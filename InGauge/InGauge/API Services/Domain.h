//
//  Domain.h
//  InGauge
//
//  Created by Mehul Patel on 12/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#ifndef Domain_h
#define Domain_h

//------------------------------------------------------

// ~~~~~~~~~>>>>>  EUROP domains

//------------------------------------------------------

// LIVE server
#define EUROP_URL                @"ec.fpg-ingauge.com"

// UAT Server
//#define EUROP_URL                @"18.195.106.186"

// QA Server
//#define EUROP_URL                @"75.101.236.193"

// svt-srv-fpg (local) server
//#define EUROP_URL                @"192.168.2.167"
//#define EUROP_URL                @"192.168.3.118"

// Bharti's Machine
//#define EUROP_URL                   @"192.168.3.132"

// Parth's Machine
//#define EUROP_URL                   @"192.168.2.139"

// Nishith's machine
//#define EUROP_URL                   @"svt-pc-84"

// Aziz's machine
//#define EUROP_URL                   @"192.168.3.175"

// Hemali's machine
//#define EUROP_URL                   @"192.168.3.101"

// Kanisha's machine
//#define EUROP_URL                   @"192.168.2.127"

// Rizwan's machine
//#define EUROP_URL                   @"192.168.2.157"

// Hardik's machine
//#define EUROP_URL                   @"192.168.2.172"

// Prerak's machine
//#define EUROP_URL                   @"192.168.2.124"

// Priyanka's machine
//#define EUROP_URL                   @"192.168.2.173"

// Vishal's machine
//#define EUROP_URL                   @"192.168.3.246"


//------------------------------------------------------

// ~~~~~~~~~>>>>>  ADMIN domains

//------------------------------------------------------

// LIVE server
#define ADMIN_URL                @"app.fpg-ingauge.com"

// UAT Server
//#define ADMIN_URL                @"uat.app.fpg-ingauge.com"

// QA Server
//#define ADMIN_URL                @"75.101.236.193"

// svt-srv-fpg (local) server
//#define ADMIN_URL                @"192.168.3.118"

//#define Bharti's Machine
//#define ADMIN_URL                   @"192.168.3.132"

//#define Parth's Machine
//#define ADMIN_URL                   @"192.168.2.139"

// Nishith's machine
//#define ADMIN_URL                   @"svt-pc-84"

// Aziz's machine
//#define ADMIN_URL                   @"192.168.3.175"

// Hemali's machine
//#define ADMIN_URL                   @"192.168.3.101"

// Kanisha's machine
//#define ADMIN_URL                   @"192.168.2.127"

// Rizwan's machine
//#define ADMIN_URL                   @"192.168.2.157"

// Hardik's machine
//#define ADMIN_URL                   @"192.168.2.172"

// Prerak's machine
//#define ADMIN_URL                   @"192.168.2.124"

// Priyanka's machine
//#define ADMIN_URL                   @"192.168.2.173"

// Vishal's machine
//#define ADMIN_URL                   @"192.168.3.246"


#endif /* FPDomain_h */
