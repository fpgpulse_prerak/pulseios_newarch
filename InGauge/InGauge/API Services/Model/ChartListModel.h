//
//  ChartListModel.h
//  IN-Gauge
//
//  Created by Mehul Patel on 29/08/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "CustomReport.h"

@interface ChartListModel : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSArray           *reportList; //CustomReport
//UPDATE: 10-Oct-2017 ~ Update filters based on dashboard (dynamic filters)
@property (nonatomic, strong) NSArray           *filters;
@property (nonatomic, strong) NSDictionary      *dimensionDisplayName;

@end
