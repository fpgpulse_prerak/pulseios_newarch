//
//  ChartListModel.m
//  IN-Gauge
//
//  Created by Mehul Patel on 29/08/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "ChartListModel.h"

@implementation ChartListModel

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id": @"customReportId"};
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    if (!isNull(array)) {
        @try {
            if ([property isEqualToString:@"customReports"]) {
                NSMutableArray *arr = [[NSMutableArray alloc] init];
                for (NSDictionary *details in array) {
                    CustomReport *model = [CustomReport new];
                    [HKJsonUtils updatePropertiesWithData:details forObject:model];
                    [arr addObject:model];
                }
                self.reportList = [NSArray arrayWithArray:arr];
                arr = nil;
            }
            
            if ([property isEqualToString:@"filters"]) {
                NSMutableArray *arr = [[NSMutableArray alloc] init];
                for (NSString *string in array) {
                    [arr addObject:string];
                }
                self.filters = [NSArray arrayWithArray:arr];
                arr = nil;
            }
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
    }
}

//---------------------------------------------------------------

- (void)handleDictionaryValue:(NSDictionary *)dictionary forProperty:(NSString *)property {
    
    //dimensionDisplayName
    if (!isNull(dictionary)) {
        if ([property isEqualToString:@"dimensionDisplayName"]) {
            self.dimensionDisplayName = dictionary;
        }
    }
}

//---------------------------------------------------------------

@end
