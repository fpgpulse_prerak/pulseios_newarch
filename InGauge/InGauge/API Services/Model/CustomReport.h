//
//  CustomReport.h
//  InGauge
//
//  Created by Mehul Patel on 01/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"

@interface CustomReport : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber          *customReportId;
@property (nonatomic, strong) NSString          *reportName;
@property (nonatomic, strong) NSString          *tableHeaders;
@property (nonatomic, strong) NSString          *prefix;
@property (nonatomic, strong) NSString          *suffix;
@property (nonatomic, strong) NSString          *username;
@property (nonatomic, strong) NSNumber          *value;
@property (nonatomic, strong) NSNumber          *dataCompareBaselineValue;
@property (nonatomic, strong) NSNumber          *dataCompareTargetValue;
@property (nonatomic, strong) NSNumber          *baseline;
@property (nonatomic, strong) NSNumber          *dataCompareValue;
@property (nonatomic, strong) NSNumber          *target;
@property (nonatomic, strong) NSString          *reportType;
@property (nonatomic, strong) NSString          *entityName;
@property (nonatomic, strong) NSString          *chartType;
@property (nonatomic, strong) NSString          *currency;
@property (nonatomic, strong) NSString          *text;
@property (nonatomic, strong) NSString          *extraInfo;
@property (nonatomic, strong) NSNumber          *isError;
@property (nonatomic, strong) NSString          *compEntityName;
@property (nonatomic, strong) NSString          *isQueryBasedReport;

@end
