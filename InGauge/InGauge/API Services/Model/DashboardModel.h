//
//  DashboardModel.h
//  InGauge
//
//  Created by Mehul Patel on 23/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"

@interface DashboardModel : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber          *dashboardId;
@property (nonatomic, strong) NSNumber          *industryId;
@property (nonatomic, strong) NSString          *activeStatus;
@property (nonatomic, strong) NSNumber          *createdById;
@property (nonatomic, strong) NSNumber          *updatedById;
@property (nonatomic, strong) NSNumber          *createdOn;
@property (nonatomic, strong) NSNumber          *updatedOn;
@property (nonatomic, strong) NSString          *name;
@property (nonatomic, strong) NSString          *title;
@property (nonatomic, strong) NSNumber          *tenantId;
@property (nonatomic, strong) NSString          *tenantName;
@property (nonatomic, strong) NSNumber          *userId;
@property (nonatomic, strong) NSString          *userFirstName;
@property (nonatomic, strong) NSString          *userLastName;
@property (nonatomic, strong) NSString          *userEmail;
@property (nonatomic, strong) NSNumber          *share;
@property (nonatomic, strong) NSString          *createdByWithDateTime;
@property (nonatomic, strong) NSString          *updatedByWithDateTime;
@property (nonatomic, strong) NSString          *reviewedByWithDateTime;
@property (nonatomic, strong) NSDate            *unameTime;
@property (nonatomic, strong) NSDate            *cnameTime;
@property (nonatomic, strong) NSNumber          *dashboardType;
@property (nonatomic, strong) NSNumber          *isMTD;
@property (nonatomic, strong) NSNumber          *hideCompareDate;
@property (nonatomic, strong) NSNumber          *hideMetricDataType;

@end
