//
//  FeedModel.h
//  InGauge
//
//  Created by Mehul Patel on 02/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "User.h"

@interface FeedModel : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber      *feedId;
@property (nonatomic, strong) User          *createdBy; //User
@property (nonatomic, strong) NSString      *createdOn;
@property (nonatomic, strong) NSNumber      *indexRelatedOnly;
@property (nonatomic, strong) NSNumber      *indexMainOnly;
//@property (nonatomic, strong) NSNumber      *tenant;

@property (nonatomic, strong) NSString      *feedDescription;
@property (nonatomic, strong) NSNumber      *customReportWidget;
@property (nonatomic, strong) NSString      *options;
@property (nonatomic, strong) NSArray       *feedComments;
@property (nonatomic, strong) NSArray       *likeUsers;
@property (nonatomic, strong) NSString      *noOfLikes;
@property (nonatomic, strong) NSString      *noOfComments;
@property (nonatomic, strong) NSNumber      *isLiked;

@property (nonatomic, strong) NSDictionary  *reportDetails;

@end
