//
//  FeedModel.m
//  InGauge
//
//  Created by Mehul Patel on 02/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "FeedModel.h"

@implementation FeedModel

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id" : @"feedId", @"description" : @"feedDescription"};
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    // User
    if (!isNull(array)) {
        if ([property isEqualToString:@"feedComments"]) {
            self.feedComments = array;
        } else if ([property isEqualToString:@"likeUsers"]) {
            self.likeUsers = array;
        }
    }
}

//---------------------------------------------------------------

- (void)handleDictionaryValue:(NSDictionary *)dictionary forProperty:(NSString *)property {
    
    // User
    if (!isNull(dictionary)) {
        if ([property isEqualToString:@"createdBy"]) {
            User *user = [User new];
            [HKJsonUtils updatePropertiesWithData:dictionary forObject:user];
            self.createdBy = user;
        }
    }
}

//---------------------------------------------------------------

@end
