//
//  AppFilter.h
//  IN-Gauge
//
//  Created by Mehul Patel on 03/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "HKJsonUtils.h"

@class GeographyModel;

@interface AppFilter : NSObject <HKJsonPropertyMappings, NSCopying>

@property (nonatomic, strong) NSNumber          *loginUserId;
@property (nonatomic, strong) NSNumber          *industryId;
@property (nonatomic, strong) NSString          *industryDescription;
@property (nonatomic, strong) NSNumber          *tenantId;
@property (nonatomic, strong) NSString          *tenantName;
@property (nonatomic, strong) NSString          *userRole;
@property (nonatomic, strong) NSNumber          *userRoleId;
@property (nonatomic, strong) NSDictionary      *geographyResponse;
@property (nonatomic, strong) NSArray           *geographyList;
@property (nonatomic, strong) NSArray           *geoItems;
@property (nonatomic, strong) NSArray           *filterItems;

- (NSDictionary *)serializeFiltersForFeedsService;
- (void) addGeoItem:(GeographyModel *)item tolevel:(NSInteger)level;
- (NSArray *) getGeoSubItems:(NSNumber *)regionId tolevel:(NSInteger)level;
- (GeographyModel *) getGeorapghyForRegionId:(NSNumber *)regionId tolevel:(NSInteger)level;

@end
