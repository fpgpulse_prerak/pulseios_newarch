//
//  AppFilter.m
//  IN-Gauge
//
//  Created by Mehul Patel on 03/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "AppFilter.h"
#import "GeographyModel.h"

@implementation AppFilter

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return nil;
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (NSDictionary *)serializeFiltersForFeedsService {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (App_Delegate.appfilter.tenantId != nil) {
        [dict setValue:App_Delegate.appfilter.tenantId forKey:@"tenantId"];
    }
    return [NSDictionary dictionaryWithDictionary:dict];
}
//---------------------------------------------------------------

#pragma mark
#pragma mark Utility methods

//---------------------------------------------------------------

- (void) addGeoItem:(GeographyModel *)item tolevel:(NSInteger)level {
    
    // This method add each region to their respected level
    // Below figure highlights the idea!
    /*
            Country  State   District  Area   City
            -----------------------------------------
  Level     |  0    |   1   |   2   |   3   |   4   |
            |       |       |       |       |       |
            -----------------------------------------
            |  C1   |   S1  |   D1  |   A1  |  c1   |
            |       |       |       |       |       |
            -----------------------------------------
            |  C1   |   S2  |   D2  |   A2  |   c2  |
            |       |       |       |       |       |
            -----------------------------------------
    */
    
    // Array of columns
    NSArray *columnArray = nil;
    if (self.geoItems.count <= level) {
        // Create new column if not exist!
        columnArray = [NSArray new];
    } else {
        // Update column
        columnArray = [self.geoItems objectAtIndex:level];
    }
    
    // Array of rows
    NSMutableArray *rowArray = columnArray ? [NSMutableArray arrayWithArray:columnArray] : [NSMutableArray new];
    // Create new row!
    [rowArray addObject:item];
    columnArray = [NSMutableArray arrayWithArray:rowArray];
    rowArray = nil;
    
    // Update the main array - Replace respective column
    NSMutableArray *tempArray = self.geoItems ? [NSMutableArray arrayWithArray:self.geoItems] : [NSMutableArray new];
    if (self.geoItems.count <= level) {
        // Add new column
        [tempArray addObject:columnArray];
    } else {
        // Replace existing column!
        [tempArray replaceObjectAtIndex:level withObject:columnArray];
    }
    self.geoItems = [NSArray arrayWithArray:tempArray];
    tempArray = nil;
}

//---------------------------------------------------------------

- (NSArray *) getGeoSubItems:(NSNumber *)regionId tolevel:(NSInteger)level {
    @try {
        NSArray *subRegions = [self.geoItems objectAtIndex:level];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.region.parentRegionId.intValue = %d", regionId.intValue];
        NSArray *items = [subRegions filteredArrayUsingPredicate:predicate];
        return items;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return nil;
    }
}

//---------------------------------------------------------------

- (GeographyModel *) getGeorapghyForRegionId:(NSNumber *)regionId tolevel:(NSInteger)level {
    @try {
        NSArray *subRegions = [self.geoItems objectAtIndex:level];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.region.regionId.intValue = %d", regionId.intValue];
        NSArray *items = [subRegions filteredArrayUsingPredicate:predicate];
        return (items.count > 0) ? items[0] : nil;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return nil;
    }
}

//---------------------------------------------------------------

#pragma mark - Object Encoding/Decoding

//---------------------------------------------------------------

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.loginUserId forKey:@"loginUserId"];
    [aCoder encodeObject:self.industryId forKey:@"industryId"];
    [aCoder encodeObject:self.industryDescription forKey:@"industryDescription"];
    [aCoder encodeObject:self.tenantId forKey:@"tenantId"];
    [aCoder encodeObject:self.tenantName forKey:@"tenantName"];
    [aCoder encodeObject:self.userRole forKey:@"userRole"];
    [aCoder encodeObject:self.userRoleId forKey:@"userRoleId"];
    [aCoder encodeObject:self.geographyResponse forKey:@"geographyResponse"];
    [aCoder encodeObject:self.geographyList forKey:@"geographyList"];
    [aCoder encodeObject:self.geoItems forKey:@"geoItems"];
    [aCoder encodeObject:self.filterItems forKey:@"filterItems"];
}

//---------------------------------------------------------------

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    self.loginUserId = [aDecoder decodeObjectForKey:@"loginUserId"];
    self.industryId = [aDecoder decodeObjectForKey:@"industryId"];
    self.industryDescription = [aDecoder decodeObjectForKey:@"industryDescription"];
    self.tenantId = [aDecoder decodeObjectForKey:@"tenantId"];
    self.tenantName = [aDecoder decodeObjectForKey:@"tenantName"];
    self.userRole = [aDecoder decodeObjectForKey:@"userRole"];
    self.userRoleId = [aDecoder decodeObjectForKey:@"userRoleId"];
    self.geographyResponse = [aDecoder decodeObjectForKey:@"geographyResponse"];
    self.geographyList = [aDecoder decodeObjectForKey:@"geographyList"];
    self.geoItems = [aDecoder decodeObjectForKey:@"geoItems"];
    self.filterItems = [aDecoder decodeObjectForKey:@"filterItems"];
    return self;
}

//---------------------------------------------------------------

- (id)copyWithZone:(NSZone *)zone {
    AppFilter *filter = [AppFilter new];
    filter.loginUserId = self.loginUserId;
    filter.industryId = self.industryId;
    filter.industryDescription = self.industryDescription;
    filter.tenantId = self.tenantId;
    filter.tenantName = self.tenantName;
    filter.userRole = self.userRole;
    filter.userRoleId = self.userRoleId;
    filter.geographyResponse = self.geographyResponse;
    filter.geographyList = self.geographyList;
    filter.geoItems = self.geoItems;
    filter.filterItems = self.filterItems;
    return filter;
}

//---------------------------------------------------------------

- (instancetype)init {
    self = [super init];
    if (self) {
    }
    return self;
}

//---------------------------------------------------------------

@end
