//
//  Filters.h
//  InGauge
//
//  Created by Mehul Patel on 31/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"

@interface Filters : NSObject <HKJsonPropertyMappings, NSCopying>

@property (nonatomic, strong) NSNumber          *regionTypeId;
@property (nonatomic, strong) NSNumber          *userId;
@property (nonatomic, strong) NSString          *orderBy;
@property (nonatomic, strong) NSString          *sort;
@property (nonatomic, strong) NSNumber          *regionId;
@property (nonatomic, strong) NSString          *activeStatus;
@property (nonatomic, strong) NSNumber          *accessible;
@property (nonatomic, strong) NSNumber          *locationId;
@property (nonatomic, strong) NSNumber          *locationGroupId;
@property (nonatomic, strong) NSNumber          *productId;
@property (nonatomic, strong) NSNumber          *locationGroupIDs;
@property (nonatomic, strong) NSDate            *startDate;
@property (nonatomic, strong) NSDate            *endDate;
@property (nonatomic, strong) NSDate            *compareStartDate;
@property (nonatomic, strong) NSDate            *compareEndDate;
@property (nonatomic, strong) NSNumber          *getContributorsOnly;
@property (nonatomic, strong) NSString          *isCompare;
@property (nonatomic, strong) NSString          *metricsDateType;
@property (nonatomic, strong) NSNumber          *month;
@property (nonatomic, strong) NSNumber          *year;

// Filter names
@property (nonatomic, strong) NSString          *regionTypeName;
@property (nonatomic, strong) NSString          *regionName;
@property (nonatomic, strong) NSString          *locationName;
@property (nonatomic, strong) NSString          *locationGroupName;
@property (nonatomic, strong) NSString          *productName;
@property (nonatomic, strong) NSString          *userName;
@property (nonatomic, strong) NSString          *metricTypeName;
@property (nonatomic, strong) NSString          *permissionName;
@property (nonatomic, strong) NSNumber          *getOperatorsOnly;
@property (nonatomic, strong) NSNumber          *getWithAllStatus;
@property (nonatomic, strong) NSString          *surveyEntityType;
@property (nonatomic, strong) NSString          *surveyEntityTypeString;
@property (nonatomic, strong) NSNumber          *decideContributorOpratorLocationLevel;

@property (nonatomic, strong) NSNumber          *performanceLeaderId;
@property (nonatomic, strong) NSNumber          *agentId;
@property (nonatomic, strong) NSNumber          *obTypeId;
@property (nonatomic, strong) NSNumber          *categoryId;
@property (nonatomic, strong) NSString          *categoryName;
@property (nonatomic, strong) NSString          *surveyStatus;

// Array of selected geography
@property (nonatomic, strong) NSArray           *selectedRegions; //GeographyModel

@property (nonatomic, strong) GeographyModel    *geoItem;

- (NSDictionary *)serializeFiltersForGoalProgress;
- (NSDictionary *)serializeFiltersForRegionService;
- (NSDictionary *)serializeFiltersForLocationService;
- (NSDictionary *)serializeFiltersForProductService;
- (NSDictionary *)serializeDashboardFiltersForUsersService;
- (NSDictionary *)serializeFiltersForChartURL;
- (NSDictionary *)serializeFiltersForChartDataURL;
- (NSDictionary *)serializeFiltersForTableDataURL;
- (NSDictionary *)serializeFiltersForLikeComment;
- (NSDictionary *)serializeFiltersForDashboardUsersService;

//--- Observation ---
- (NSDictionary *) serializeFiltersForAgentService;
- (NSDictionary *) serializeFiltersForObservationTypeService;
- (NSDictionary *) serializeFiltersForObservationCategoriesService;
- (NSDictionary *) serializeFiltersForObservationListService;
- (NSDictionary *) serializeFiltersSurveyLocationService;
- (NSDictionary *) serializeFiltersForAvailableUsersService;

//--- Goal Settings ---
- (NSDictionary *) serializeFiltersGoalSettingLocationService;
- (NSDictionary *) serializeFiltersGoalProgressUsersService;
- (NSDictionary *) serializeFiltersForPerformanceLeaderService;

- (void) copyFromFilter:(Filters *)filter;
- (void) removeFilters;
@end
