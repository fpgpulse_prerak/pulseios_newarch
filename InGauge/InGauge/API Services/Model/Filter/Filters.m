//
//  Filters.m
//  InGauge
//
//  Created by Mehul Patel on 31/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "Filters.h"

@implementation Filters

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return nil;
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) removeFilters {
    self.regionTypeId = nil;
    self.userId = nil;
    self.regionId = nil;
    self.locationId = nil;
    self.locationGroupId = nil;
    self.productId = nil;
    self.locationGroupIDs = nil;
    self.metricsDateType = nil;
}

//---------------------------------------------------------------

- (void) copyFromFilter:(Filters *)filter {
}

//---------------------------------------------------------------

- (NSDictionary *)serializeFiltersForGoalProgress {  
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (self.month != nil) {
        [dict setValue:self.month forKey:@"month"];
    }
    if (self.year != nil) {
        [dict setValue:self.year forKey:@"year"];
    }
    if (App_Delegate.appfilter.tenantId != nil) {
        [dict setValue:App_Delegate.appfilter.tenantId forKey:@"tenantId"];
    }
    if (self.locationId != nil) {
        [dict setValue:self.locationId forKey:@"tenantLocationId"];
    }
    if (self.locationGroupId != nil) {
        [dict setValue:self.locationGroupId forKey:@"locationGroupId"];
    }
    if (self.userId != nil) {
        [dict setValue:self.userId forKey:@"userId"];
    }
    if (self.productId != nil) {
        [dict setValue:self.productId forKey:@"locationGroupProductId"];
    }
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *)serializeFiltersForChartDataURL {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (App_Delegate.appfilter.tenantId != nil) {
        [dict setValue:App_Delegate.appfilter.tenantId forKey:@"tenantId"];
    }

    if (self.regionTypeId != nil) {
        [dict setValue:[self patchForDashboardChart:self.regionTypeId] forKey:@"regionTypeId"];
    } else {
        [dict setValue:[NSNumber numberWithInt:-2] forKey:@"regionTypeId"];
    }
    
    if (self.regionId != nil) {
        [dict setValue:[self patchForDashboardChart:self.regionId] forKey:@"regionId"];
    } else {
        [dict setValue:[NSNumber numberWithInt:-2] forKey:@"regionId"];
    }
    
    if (self.locationId != nil) {
        [dict setValue:[self patchForDashboardChart:self.locationId] forKey:@"tenantLocationId"];
    } else {
        [dict setValue:[NSNumber numberWithInt:-2] forKey:@"tenantLocationId"];
    }
    
    if (self.locationGroupId != nil) {
        [dict setValue:[self patchForDashboardChart:self.locationGroupId] forKey:@"locationGroupId"];
    } else {
        [dict setValue:[NSNumber numberWithInt:-2] forKey:@"locationGroupId"];
    }
    
    if (self.productId != nil) {
        [dict setValue:[self patchForDashboardChart:self.productId] forKey:@"productId"];
    } else {
        [dict setValue:[NSNumber numberWithInt:-2] forKey:@"productId"];
    }
    
    if (self.userId != nil) {
        [dict setValue:[self patchForDashboardChart:self.userId] forKey:@"userId"];
    } else {
        [dict setValue:[NSNumber numberWithInt:-2] forKey:@"userId"];
    }
    
    if (self.startDate != nil) {
        [dict setValue:[self.startDate toNSString:SERVICE_DATE_TIME_SHORT_FORMAT] forKey:KEY_DATE_FROM];
    }
    if (self.endDate != nil) {
        [dict setValue:[self.endDate toNSString:SERVICE_DATE_TIME_SHORT_FORMAT] forKey:KEY_DATE_TO];
    }
    
    if (self.isCompare != nil) {
        [dict setValue:self.isCompare forKey:@"isCompare"];
    }
    if (self.compareStartDate != nil) {
        [dict setValue:[self.compareStartDate toNSString:SERVICE_DATE_TIME_SHORT_FORMAT] forKey:KEY_DATE_COMP_FROM];
    }
    if (self.compareEndDate != nil) {
        [dict setValue:[self.compareEndDate toNSString:SERVICE_DATE_TIME_SHORT_FORMAT] forKey:KEY_DATE_COMP_TO];
    }
    if (self.metricsDateType != nil) {
        self.metricsDateType = [Helper getMetricTypeActualValue:self.metricTypeName];
        [dict setValue:self.metricsDateType forKey:@"metricsDateType"];
    }
        
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *)serializeFiltersForTableDataURL {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (App_Delegate.appfilter.tenantId != nil) {
        [dict setValue:App_Delegate.appfilter.tenantId forKey:@"tenantId"];
    }
    
    if (self.regionTypeId != nil) {
        [dict setValue:[self patchForDashboardChart:self.regionTypeId] forKey:@"regionTypeId"];
    } else {
        [dict setValue:[NSNumber numberWithInt:-2] forKey:@"regionTypeId"];
    }
    
    if (self.regionId != nil) {
        [dict setValue:[self patchForDashboardChart:self.regionId] forKey:@"regionId"];
    } else {
        [dict setValue:[NSNumber numberWithInt:-2] forKey:@"regionId"];
    }
    
    if (self.locationId != nil) {
        [dict setValue:[self patchForDashboardChart:self.locationId] forKey:@"tenantLocationId"];
    } else {
        [dict setValue:[NSNumber numberWithInt:-2] forKey:@"tenantLocationId"];
    }
    
    if (self.locationGroupId != nil) {
        [dict setValue:[self patchForDashboardChart:self.locationGroupId] forKey:@"locationGroupId"];
    } else {
        [dict setValue:[NSNumber numberWithInt:-2] forKey:@"locationGroupId"];
    }
    
    if (self.productId != nil) {
        [dict setValue:[self patchForDashboardChart:self.productId] forKey:@"productId"];
    } else {
        [dict setValue:[NSNumber numberWithInt:-2] forKey:@"productId"];
    }
    
    if (self.userId != nil) {
        [dict setValue:[self patchForDashboardChart:self.userId] forKey:@"userId"];
    } else {
        [dict setValue:[NSNumber numberWithInt:-2] forKey:@"userId"];
    }
    
    if (self.startDate != nil) {
        [dict setValue:[self.startDate toNSString:SERVICE_DATE_TIME_SHORT_FORMAT] forKey:KEY_DATE_FROM];
    }
    if (self.endDate != nil) {
        [dict setValue:[self.endDate toNSString:SERVICE_DATE_TIME_SHORT_FORMAT] forKey:KEY_DATE_TO];
    }
    if (self.isCompare != nil) {
        [dict setValue:self.isCompare forKey:@"isCompare"];
    }
    if (self.compareStartDate != nil) {
        [dict setValue:[self.compareStartDate toNSString:SERVICE_DATE_TIME_SHORT_FORMAT] forKey:KEY_DATE_COMP_FROM];
    }
    if (self.compareEndDate != nil) {
        [dict setValue:[self.compareEndDate toNSString:SERVICE_DATE_TIME_SHORT_FORMAT] forKey:KEY_DATE_COMP_TO];
    }
    if (self.metricsDateType != nil) {
        self.metricsDateType = [Helper getMetricTypeActualValue:self.metricTypeName];
        [dict setValue:self.metricsDateType forKey:@"metricsDateType"];
    }

    if (App_Delegate.appfilter.loginUserId != nil) {
        [dict setValue:App_Delegate.appfilter.loginUserId forKey:@"loggedInUserId"];
    }
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *)serializeFiltersForChartURL {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (App_Delegate.appfilter.tenantId != nil) {
        [dict setValue:App_Delegate.appfilter.tenantId forKey:@"tenantId"];
    }
    
    if (self.regionTypeId != nil) {
        [dict setValue:[self patchForDashboardChart:self.regionTypeId] forKey:@"regionTypeId"];
    } else {
        [dict setValue:[NSNumber numberWithInt:-2] forKey:@"regionTypeId"];
    }
    
    if (self.regionId != nil) {
        [dict setValue:[self patchForDashboardChart:self.regionId] forKey:@"regionId"];
    } else {
        [dict setValue:[NSNumber numberWithInt:-2] forKey:@"regionId"];
    }
    
    if (self.locationId != nil) {
        [dict setValue:[self patchForDashboardChart:self.locationId] forKey:@"tenantLocationId"];
    } else {
        [dict setValue:[NSNumber numberWithInt:-2] forKey:@"tenantLocationId"];
    }
    
    if (self.locationGroupId != nil) {
        [dict setValue:[self patchForDashboardChart:self.locationGroupId] forKey:@"locationGroupId"];
    } else {
        [dict setValue:[NSNumber numberWithInt:-2] forKey:@"locationGroupId"];
    }
    
    if (self.productId != nil) {
        [dict setValue:[self patchForDashboardChart:self.productId] forKey:@"productId"];
    } else {
        [dict setValue:[NSNumber numberWithInt:-2] forKey:@"productId"];
    }
    
    if (self.userId != nil) {
        [dict setValue:[self patchForDashboardChart:self.userId] forKey:@"userId"];
    } else {
        [dict setValue:[NSNumber numberWithInt:-2] forKey:@"userId"];
    }
    
    if (self.startDate != nil) {
        [dict setValue:[self.startDate toNSString:SERVICE_DATE_TIME_SHORT_FORMAT] forKey:KEY_DATE_FROM];
    }
    if (self.endDate != nil) {
        [dict setValue:[self.endDate toNSString:SERVICE_DATE_TIME_SHORT_FORMAT] forKey:KEY_DATE_TO];
    }
    if (self.isCompare != nil) {
        [dict setValue:self.isCompare forKey:@"isCompare"];
    }
    if (self.compareStartDate != nil) {
        [dict setValue:[self.compareStartDate toNSString:SERVICE_DATE_TIME_SHORT_FORMAT] forKey:KEY_DATE_COMP_FROM];
    }
    if (self.compareEndDate != nil) {
        [dict setValue:[self.compareEndDate toNSString:SERVICE_DATE_TIME_SHORT_FORMAT] forKey:KEY_DATE_COMP_TO];
    }
    if (self.metricsDateType != nil) {
        self.metricsDateType = [Helper getMetricTypeActualValue:self.metricTypeName];
        [dict setValue:self.metricsDateType forKey:@"metricsDateType"];
    }
    if (App_Delegate.appfilter.loginUserId != nil) {
        [dict setValue:App_Delegate.appfilter.loginUserId forKey:@"loginUser"];
    }
    if (App_Delegate.appfilter.industryId != nil) {
        [dict setValue:App_Delegate.appfilter.industryId forKey:@"industryId"];
    }
    
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *)serializeFiltersForLikeComment {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (App_Delegate.appfilter.tenantId != nil) {
        [dict setValue:App_Delegate.appfilter.tenantId forKey:@"tenantId"];
    }
    if (App_Delegate.appfilter.industryId != nil) {
        [dict setValue:App_Delegate.appfilter.industryId forKey:@"industryId"];
    }
    
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSNumber *) patchForDashboardChart:(NSNumber *)number {
    if (number.integerValue == 0) {
        return [NSNumber numberWithInteger:-2];
    }
    return number;
}

//---------------------------------------------------------------

- (NSDictionary *)serializeDashboardFiltersForUsersService {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];    
    if (self.locationGroupId != nil) {
        [dict setValue:self.locationGroupId forKey:@"locationGroupIDs"];
    }
    if (self.startDate != nil) {
        [dict setValue:[self.startDate toNSString:SERVICE_DATE_TIME_SHORT_FORMAT] forKey:@"startDate"];
    }
    if (self.endDate != nil) {
        [dict setValue:[self.endDate toNSString:SERVICE_DATE_TIME_SHORT_FORMAT] forKey:@"endDate"];
    }    
    if (self.getContributorsOnly != nil) {
        [dict setValue:self.getContributorsOnly forKey:@"getContributorsOnly"];
    }
    
//    [dict setValue:[NSNumber numberWithBool:0] forKey:@"getOperatorsOnly"];
//    [dict setValue:[NSNumber numberWithBool:0] forKey:@"getWithAllStatus"];
//    [dict setValue:[NSNumber numberWithBool:1] forKey:@"isWeightWiseUser"];
//    [dict setValue:[NSNumber numberWithBool:0] forKey:@"allowSameWeightUser"];
    
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *)serializeFiltersForDashboardUsersService {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (self.locationGroupId != nil) {
        [dict setValue:self.locationGroupId forKey:@"locationGroupID"];
    }
    
    if (self.locationId != nil) {
        [dict setValue:self.locationId forKey:@"tenantLocationID"];
    }
    
    [dict setValue:[NSNumber numberWithBool:1] forKey:@"getContributorsOnly"];
    [dict setValue:[NSNumber numberWithBool:0] forKey:@"getOperatorsOnly"];
    [dict setValue:[NSNumber numberWithBool:0] forKey:@"getWithAllStatus"];
    [dict setValue:[NSNumber numberWithBool:1] forKey:@"checkLoggedinUserOperatorContributor"];
    //    [dict setValue:[NSNumber numberWithBool:1] forKey:@"isWeightWiseUser"];
    //    [dict setValue:[NSNumber numberWithBool:0] forKey:@"allowSameWeightUser"];
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *)serializeFiltersForAvailableUsersService {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (self.locationGroupId != nil) {
        [dict setValue:self.locationGroupId forKey:@"locationGroupID"];
    }
    
    if (self.locationId != nil) {
        [dict setValue:self.locationId forKey:@"tenantLocationID"];
    }
    
    [dict setValue:[NSNumber numberWithBool:1] forKey:@"getContributorsOnly"];
    [dict setValue:[NSNumber numberWithBool:0] forKey:@"getOperatorsOnly"];
    [dict setValue:[NSNumber numberWithBool:0] forKey:@"getWithAllStatus"];
//    [dict setValue:[NSNumber numberWithBool:1] forKey:@"isWeightWiseUser"];
//    [dict setValue:[NSNumber numberWithBool:0] forKey:@"allowSameWeightUser"];
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *)serializeFiltersForProductService {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (self.locationGroupId != nil) {
        [dict setValue:self.locationGroupId forKey:@"locationGroupId"];
    }
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *)serializeFiltersForRegionService {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (self.accessible != nil) {
        [dict setValue:self.accessible forKey:@"accessible"];
    }
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *)serializeFiltersForLocationService {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
//    if (self.orderBy != nil) {
//        [dict setValue:@"" forKey:@"orderBy"];
//    }
//    if (self.sort != nil) {
//        [dict setValue:@"name" forKey:@"sort"];
//    }
    if (self.regionId != nil) {
        [dict setValue:self.regionId forKey:@"regionId"];
    }    
//    if (self.activeStatus != nil) {
//        [dict setValue:self.activeStatus forKey:@"activeStatus"];
//    }
    
    [dict setValue:KEY_ORDER_BY_VALUE forKey:@"orderBy"];
    [dict setValue:KEY_SORT_BY_ASC_VALUE forKey:@"sort"];
    [dict setValue:KEY_PARAM_CONST_ACTIVE_STATUS forKey:@"activeStatus"];

    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *) serializeFiltersSurveyLocationService {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    [dict setValue:KEY_ORDER_BY_VALUE forKey:@"orderBy"];
    [dict setValue:KEY_SORT_BY_ASC_VALUE forKey:@"sort"];
    [dict setValue:KEY_PARAM_CONST_ACTIVE_STATUS forKey:@"activeStatus"];
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Goal Settings methods

//---------------------------------------------------------------

- (NSDictionary *) serializeFiltersGoalSettingLocationService {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    [dict setValue:KEY_ORDER_BY_VALUE forKey:@"orderBy"];
    [dict setValue:KEY_SORT_BY_ASC_VALUE forKey:@"sort"];
    [dict setValue:KEY_PARAM_CONST_ACTIVE_STATUS forKey:@"activeStatus"];
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *) serializeFiltersGoalProgressUsersService {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (self.locationGroupId != nil) {
        [dict setValue:self.locationGroupId forKey:@"locationGroupIDs"];
    }
    if (self.getContributorsOnly != nil) {
        [dict setValue:self.getContributorsOnly forKey:@"getContributorsOnly"];
    }
    
    NSDate *dateFromMonthAndYear = [Helper getDateFromMonth:self.month andYear:self.year];
    // startDate - Start of month
    // endDate - End of month
    NSDate *startDate = [Helper firstDateOfMonth:dateFromMonthAndYear];
    NSDate *endDate = [Helper lastDateOfMonth:dateFromMonthAndYear];
    if (startDate != nil) {
        [dict setValue:[startDate toNSString:SERVICE_DATE_TIME_SHORT_FORMAT] forKey:@"startDate"];
    }
    if (endDate != nil) {
        [dict setValue:[endDate toNSString:SERVICE_DATE_TIME_SHORT_FORMAT] forKey:@"endDate"];
    }
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *) serializeFiltersForPerformanceLeaderService {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (self.permissionName != nil) {
        [dict setValue:self.permissionName forKey:@"permissionName"];
    }
    if (self.locationId != nil) {
        [dict setValue:self.locationId forKey:@"tenantLocationId"];
    }
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *) serializeFiltersForAgentService {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (self.permissionName != nil) {
        [dict setValue:self.permissionName forKey:@"permissionName"];
    }
    if (self.locationId != nil) {
        [dict setValue:self.locationId forKey:@"tenantLocationId"];
    }
    if (self.getContributorsOnly != nil) {
        [dict setValue:self.getContributorsOnly forKey:@"getContributorsOnly"];
    }
    if (self.getOperatorsOnly != nil) {
        [dict setValue:self.getOperatorsOnly forKey:@"getOperatorsOnly"];
    }
    if (self.getWithAllStatus != nil) {
        [dict setValue:self.getWithAllStatus forKey:@"getWithAllStatus"];
    }
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *) serializeFiltersForObservationTypeService {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (self.surveyEntityType != nil) {
        [dict setValue:self.surveyEntityTypeString forKey:@"surveyEntityType"];
    }
    if (self.locationId != nil) {
        [dict setValue:self.locationId forKey:@"tenantLocationId"];
    }
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *) serializeFiltersForObservationCategoriesService {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (self.surveyEntityType != nil) {
        [dict setValue:self.surveyEntityTypeString forKey:@"surveyEntityType"];
    }
    if (self.activeStatus != nil) {
        [dict setValue:self.activeStatus forKey:@"activeStatus"];
    }
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *) serializeFiltersForObservationListService {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (self.locationId != nil) {
        [dict setValue:@[self.locationId] forKey:@"tenantLocations"];
    }
//    if (self.surveyStatus != nil) {
//        [dict setValue:self.surveyStatus forKey:@"surveyStatus"];
//    }
    if (self.categoryId != nil) {
        [dict setValue:self.categoryId forKey:@"questionnaireSurveyCategoryId"];
    }
    if (App_Delegate.appfilter.tenantId != nil) {
        [dict setValue:App_Delegate.appfilter.tenantId forKey:@"tenantId"];
    }
    if (self.startDate != nil) {
        long long utcTimeInMilliSeconds = [Helper utcTimeIntervalForDate:self.startDate];
        [dict setValue:[NSString stringWithFormat:@"%lld", utcTimeInMilliSeconds] forKey:@"startDate"];
    }
    if (self.endDate != nil) {
        
//        // 😑 Patch: 26-Sep-2017 ~ Service has some issue from back end so I am subtracting one second from date
//        // Subtract one second from endDate to manage time zone for web application/Service
//        DLog(@"End date :%@", self.endDate);
//        NSDate *finalDate = [Helper subtractSecondsFromDate:self.endDate seconds:-1];
        long long utcTimeInMilliSeconds = [Helper utcTimeIntervalForDate:self.endDate];
        [dict setValue:[NSString stringWithFormat:@"%lld", utcTimeInMilliSeconds] forKey:@"endDate"];
    }
    if (self.surveyEntityType != nil) {
        [dict setValue:self.surveyEntityType forKey:@"surveyEntityType"];
    }
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

#pragma mark - Object Encoding/Decoding

//---------------------------------------------------------------

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.regionTypeName forKey:@"regionTypeName"];
    [aCoder encodeObject:self.regionName forKey:@"regionName"];
    [aCoder encodeObject:self.locationName forKey:@"locationName"];
    [aCoder encodeObject:self.locationGroupName forKey:@"locationGroupName"];
    [aCoder encodeObject:self.productName forKey:@"productName"];
    [aCoder encodeObject:self.userName forKey:@"userName"];
    [aCoder encodeObject:self.metricTypeName forKey:@"metricTypeName"];
    [aCoder encodeObject:self.regionTypeId forKey:@"regionTypeId"];
    [aCoder encodeObject:self.userId forKey:@"userId"];
    [aCoder encodeObject:self.orderBy forKey:@"orderBy"];
    [aCoder encodeObject:self.sort forKey:@"sort"];
    [aCoder encodeObject:self.regionId forKey:@"regionId"];
    [aCoder encodeObject:self.activeStatus forKey:@"activeStatus"];
    [aCoder encodeObject:self.accessible forKey:@"accessible"];
    [aCoder encodeObject:self.locationId forKey:@"locationId"];
    [aCoder encodeObject:self.locationGroupId forKey:@"locationGroupId"];
    [aCoder encodeObject:self.productId forKey:@"productId"];
    [aCoder encodeObject:self.locationGroupIDs forKey:@"locationGroupIDs"];
    [aCoder encodeObject:self.startDate forKey:@"startDate"];
    [aCoder encodeObject:self.endDate forKey:@"endDate"];
    [aCoder encodeObject:self.compareStartDate forKey:@"compareStartDate"];
    [aCoder encodeObject:self.compareEndDate forKey:@"compareEndDate"];
    [aCoder encodeObject:self.getContributorsOnly forKey:@"getContributorsOnly"];
    [aCoder encodeObject:self.isCompare forKey:@"isCompare"];
    [aCoder encodeObject:self.metricsDateType forKey:@"metricsDateType"];
    [aCoder encodeObject:self.month forKey:@"month"];
    [aCoder encodeObject:self.year forKey:@"year"];
    [aCoder encodeObject:self.permissionName forKey:@"permissionName"];
    [aCoder encodeObject:self.getOperatorsOnly forKey:@"getOperatorsOnly"];
    [aCoder encodeObject:self.getWithAllStatus forKey:@"getWithAllStatus"];
    [aCoder encodeObject:self.surveyEntityType forKey:@"surveyEntityType"];
    [aCoder encodeObject:self.performanceLeaderId forKey:@"performanceLeaderId"];
    [aCoder encodeObject:self.agentId forKey:@"agentId"];
    [aCoder encodeObject:self.obTypeId forKey:@"obTypeId"];
    [aCoder encodeObject:self.categoryId forKey:@"categoryId"];
    [aCoder encodeObject:self.categoryName forKey:@"categoryName"];
    [aCoder encodeObject:self.surveyStatus forKey:@"surveyStatus"];
    [aCoder encodeObject:self.surveyEntityTypeString forKey:@"surveyEntityTypeString"];
    [aCoder encodeObject:self.decideContributorOpratorLocationLevel forKey:@"decideContributorOpratorLocationLevel"];
    [aCoder encodeObject:self.geoItem forKey:@"geoItem"];
    [aCoder encodeObject:self.selectedRegions forKey:@"selectedRegions"];
}

//---------------------------------------------------------------

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    self.regionTypeName = [aDecoder decodeObjectForKey:@"regionTypeName"];
    self.regionName = [aDecoder decodeObjectForKey:@"regionName"];
    self.locationName = [aDecoder decodeObjectForKey:@"locationName"];
    self.locationGroupName = [aDecoder decodeObjectForKey:@"locationGroupName"];
    self.productName = [aDecoder decodeObjectForKey:@"productName"];
    self.userName = [aDecoder decodeObjectForKey:@"userName"];
    self.metricTypeName = [aDecoder decodeObjectForKey:@"metricTypeName"];
    
    
    self.regionTypeId = [aDecoder decodeObjectForKey:@"regionTypeId"];
    self.userId = [aDecoder decodeObjectForKey:@"userId"];
    self.orderBy = [aDecoder decodeObjectForKey:@"orderBy"];
    self.sort = [aDecoder decodeObjectForKey:@"sort"];
    self.regionId = [aDecoder decodeObjectForKey:@"regionId"];
    self.activeStatus = [aDecoder decodeObjectForKey:@"activeStatus"];
    self.accessible = [aDecoder decodeObjectForKey:@"accessible"];
    self.locationId = [aDecoder decodeObjectForKey:@"locationId"];
    self.locationGroupId = [aDecoder decodeObjectForKey:@"locationGroupId"];
    self.productId = [aDecoder decodeObjectForKey:@"productId"];
    self.locationGroupIDs = [aDecoder decodeObjectForKey:@"locationGroupIDs"];
    self.startDate = [aDecoder decodeObjectForKey:@"startDate"];
    self.endDate = [aDecoder decodeObjectForKey:@"endDate"];
    self.compareStartDate = [aDecoder decodeObjectForKey:@"compareStartDate"];
    self.compareEndDate = [aDecoder decodeObjectForKey:@"compareEndDate"];
    self.getContributorsOnly = [aDecoder decodeObjectForKey:@"getContributorsOnly"];
    self.isCompare = [aDecoder decodeObjectForKey:@"isCompare"];
    self.metricsDateType = [aDecoder decodeObjectForKey:@"metricsDateType"];
    self.month = [aDecoder decodeObjectForKey:@"month"];
    self.year = [aDecoder decodeObjectForKey:@"year"];
    self.permissionName = [aDecoder decodeObjectForKey:@"permissionName"];
    self.getOperatorsOnly = [aDecoder decodeObjectForKey:@"getOperatorsOnly"];
    self.getWithAllStatus = [aDecoder decodeObjectForKey:@"getWithAllStatus"];
    self.surveyEntityType = [aDecoder decodeObjectForKey:@"surveyEntityType"];
    self.performanceLeaderId = [aDecoder decodeObjectForKey:@"performanceLeaderId"];
    self.agentId = [aDecoder decodeObjectForKey:@"agentId"];
    self.obTypeId = [aDecoder decodeObjectForKey:@"obTypeId"];
    self.categoryId = [aDecoder decodeObjectForKey:@"categoryId"];
    self.categoryName = [aDecoder decodeObjectForKey:@"categoryName"];
    self.surveyStatus = [aDecoder decodeObjectForKey:@"surveyStatus"];
    self.surveyEntityTypeString = [aDecoder decodeObjectForKey:@"surveyEntityTypeString"];
    self.decideContributorOpratorLocationLevel = [aDecoder decodeObjectForKey:@"decideContributorOpratorLocationLevel"];
    self.geoItem = [aDecoder decodeObjectForKey:@"geoItem"];
    self.selectedRegions = [aDecoder decodeObjectForKey:@"selectedRegions"];
    return self;
}

#pragma mark - NSCopying

//---------------------------------------------------------------

- (id)copyWithZone:(NSZone *)zone {
    Filters *filter = [Filters new];
    filter.regionTypeId = self.regionTypeId;
    filter.userId = self.userId;
    filter.orderBy = self.orderBy;
    filter.sort = self.sort;
    filter.regionId = self.regionId;
    filter.activeStatus = self.activeStatus;
    filter.accessible = self.accessible;
    filter.locationId = self.locationId;
    filter.locationGroupId = self.locationGroupId;
    filter.productId = self.productId;
    filter.locationGroupIDs = self.locationGroupIDs;
    filter.startDate = self.startDate;
    filter.endDate = self.endDate;
    filter.compareStartDate = self.compareStartDate;
    filter.compareEndDate = self.compareEndDate;
    filter.getContributorsOnly = self.getContributorsOnly;
    filter.isCompare = self.isCompare;
    filter.metricsDateType = self.metricsDateType;
    filter.month = self.month;
    filter.year = self.year;
    
    filter.regionTypeName = self.regionTypeName;
    filter.regionName = self.regionName;
    filter.locationName = self.locationName;
    filter.locationGroupName = self.locationGroupName;
    filter.productName = self.productName;
    filter.userName = self.userName;
    filter.metricTypeName = self.metricTypeName;
    
    filter.permissionName = self.permissionName;
    filter.getOperatorsOnly = self.getOperatorsOnly;
    filter.getWithAllStatus = self.getWithAllStatus;
    filter.surveyEntityType = self.surveyEntityType;
    filter.performanceLeaderId = self.performanceLeaderId;
    filter.agentId = self.agentId;
    filter.obTypeId = self.obTypeId;
    filter.categoryId = self.categoryId;
    filter.categoryName = self.categoryName;
    filter.surveyStatus = self.surveyStatus;
    filter.surveyEntityTypeString = self.surveyEntityTypeString;
    filter.decideContributorOpratorLocationLevel = self.decideContributorOpratorLocationLevel;
    filter.geoItem = self.geoItem;
    filter.selectedRegions = self.selectedRegions;
    return filter;
}

//---------------------------------------------------------------

@end
