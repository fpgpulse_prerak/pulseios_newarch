//
//  FilterListDetails.h
//  IN-Gauge
//
//  Created by Mehul Patel on 01/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "DashboardModel.h"

@interface FilterListDetails : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSDictionary          *dimensionDisplayName;
@property (nonatomic, strong) NSArray               *filters;
@property (nonatomic, strong) DashboardModel        *dashboard;

@end
