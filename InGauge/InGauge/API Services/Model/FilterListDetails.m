//
//  FilterListDetails.m
//  IN-Gauge
//
//  Created by Mehul Patel on 01/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "FilterListDetails.h"

@implementation FilterListDetails

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id": @"customReportId"};
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    if (!isNull(array)) {
        @try {            
            if ([property isEqualToString:@"filters"]) {
                NSMutableArray *arr = [[NSMutableArray alloc] init];
                for (NSString *string in array) {
                    [arr addObject:string];
                    
                    
                }
                self.filters = [NSArray arrayWithArray:arr];
                arr = nil;
                
                // Add missing filters...
                // Add location group if product is there, because you can only get products from location groups
                if ([self.filters containsObject:@"Product"] && ![self.filters containsObject:@"LocationGroup"]) {
                    // Add location group below location filter
                    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.filters];
                    [tempArray addObject:@"LocationGroup"];
                    self.filters = [NSArray arrayWithArray:tempArray];
                    tempArray = nil;
                }
                
                // Same way!
                // Add location group if user is there, because you can only get users from location groups
                if ([self.filters containsObject:@"User"] && ![self.filters containsObject:@"LocationGroup"]) {
                    // Add location group below location filter
                    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.filters];
                    [tempArray addObject:@"LocationGroup"];
                    self.filters = [NSArray arrayWithArray:tempArray];
                    tempArray = nil;
                }
            }
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
    }
}

//---------------------------------------------------------------

- (void)handleDictionaryValue:(NSDictionary *)dictionary forProperty:(NSString *)property {
    
    //dimensionDisplayName
    if (!isNull(dictionary)) {
        if ([property isEqualToString:@"dimensionDisplayName"]) {
            self.dimensionDisplayName = dictionary;
        }
    }
    
    //dimensionDisplayName
    if (!isNull(dictionary)) {
        if ([property isEqualToString:@"dashboard"]) {
            DashboardModel *model = [DashboardModel new];
            [HKJsonUtils updatePropertiesWithData:dictionary forObject:model];
            self.dashboard = model;
        }
    }
}

//---------------------------------------------------------------

@end
