//
//  GeographyItems.h
//  IN-Gauge
//
//  Created by Mehul Patel on 11/10/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GeographyModel.h"

@interface GeographyItems : NSObject

@property (nonatomic, strong) NSArray       *allItems;

@end
