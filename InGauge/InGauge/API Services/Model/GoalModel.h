//
//  GoalModel.h
//  IN-Gauge
//
//  Created by Mehul Patel on 22/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "GoalProgressModel.h"
#import "Product.h"
#import "GoalWeeklyPrediction.h"

@interface GoalModel : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) GoalProgressModel                 *FpgGoalProgress;
@property (nonatomic, strong) GoalProgressModel                 *SelfGoalProgress;
@property (nonatomic, strong) GoalWeeklyPrediction              *weeklyGoalPrediction;
@property (nonatomic, strong) NSArray                           *curMonthGoalMap; // Product

//@property (nonatomic, strong) GoalSearchForm                    *searchGoalPredictionForm;

//- (Organization *)selectedOrganization;
//- (FPLocationPM *)selectedUser;
//- (GoalSettingProduct *)selectedProduct;
//- (GoalSettingDepartment *)selectedDepartment;
//- (NSString *)getOrganizationCurrency;


@end
