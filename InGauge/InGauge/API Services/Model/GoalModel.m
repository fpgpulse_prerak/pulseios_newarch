//
//  GoalModel.m
//  IN-Gauge
//
//  Created by Mehul Patel on 22/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "GoalModel.h"

@implementation GoalModel

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return nil;
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    if (!isNull(array)) {
//        @try {
//            if ([property isEqualToString:@"curMonthGoalMap"]) {
//                NSMutableArray *arr = [[NSMutableArray alloc] init];
//                for (NSDictionary *details in array) {
//                    GoalProduct *model = [GoalProduct new];
//                    [HKJsonUtils updatePropertiesWithData:details forObject:model];
//                    [arr addObject:model];
//                }
//                self.curMonthGoalMap = [NSArray arrayWithArray:arr];
//                arr = nil;
//                
//                // SORT - Ascending order
//                NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"SELF.locationGroupProductName" ascending:YES];
//                NSArray *sortedArray = [self.curMonthGoalMap sortedArrayUsingDescriptors:@[sortDescriptor1]];
//                self.curMonthGoalMap = sortedArray;
//            }
//        } @catch (NSException *exception) {
//            DLog(@"Exception :%@", exception.debugDescription);
//        }
    }
}

//---------------------------------------------------------------

- (void)handleDictionaryValue:(NSDictionary *)dictionary forProperty:(NSString *)property {
    
    //SelfGoalProgress
    if (!isNull(dictionary)) {
        if ([property isEqualToString:@"SelfGoalProgress"]) {
            GoalProgressModel *SelfGoalProgress = [GoalProgressModel new];
            [HKJsonUtils updatePropertiesWithData:dictionary forObject:SelfGoalProgress];
            self.SelfGoalProgress = SelfGoalProgress;
        }
    }
    
    //FpgGoalProgress
    if (!isNull(dictionary)) {
        if ([property isEqualToString:@"FpgGoalProgress"]) {
            GoalProgressModel *FpgGoalProgress = [GoalProgressModel new];
            [HKJsonUtils updatePropertiesWithData:dictionary forObject:FpgGoalProgress];
            self.FpgGoalProgress = FpgGoalProgress;
        }
    }
    
    //weeklyGoalPrediction
    if (!isNull(dictionary)) {
        if ([property isEqualToString:@"weeklyGoalPrediction"]) {
            GoalWeeklyPrediction *weeklyGoalPrediction = [GoalWeeklyPrediction new];
            [HKJsonUtils updatePropertiesWithData:dictionary forObject:weeklyGoalPrediction];
            self.weeklyGoalPrediction = weeklyGoalPrediction;
        }
    }
    
    //weeklyGoalPrediction
    if (!isNull(dictionary)) {
        if ([property isEqualToString:@"weeklyGoalPrediction"]) {
            GoalWeeklyPrediction *weeklyGoalPrediction = [GoalWeeklyPrediction new];
            [HKJsonUtils updatePropertiesWithData:dictionary forObject:weeklyGoalPrediction];
            self.weeklyGoalPrediction = weeklyGoalPrediction;
        }
    }
    
    if (!isNull(dictionary)) {
        @try {
            if ([property isEqualToString:@"curMonthGoalMap"]) {
                
                NSMutableArray *arr = [[NSMutableArray alloc] init];
                for (NSString *key in dictionary) {
                    Product *model = [Product new];
                    [HKJsonUtils updatePropertiesWithData:[dictionary objectForKey:key] forObject:model];
                    [arr addObject:model];
                }
                self.curMonthGoalMap = [NSArray arrayWithArray:arr];
                arr = nil;
                
                // SORT - Ascending order
                NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"locationGroupProductName" ascending:YES];
                NSArray *sortedArray = [self.curMonthGoalMap sortedArrayUsingDescriptors:@[sortDescriptor1]];
                self.curMonthGoalMap = sortedArray;
            }
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
    }
}

//---------------------------------------------------------------

@end
