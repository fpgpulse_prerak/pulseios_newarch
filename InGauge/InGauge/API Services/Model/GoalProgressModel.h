//
//  GoalProgressModel.h
//  IN-Gauge
//
//  Created by Mehul Patel on 22/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "Product.h"

@interface GoalProgressModel : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber                      *productGoal;
@property (nonatomic, strong) NSNumber                      *productIRThisMonth;
@property (nonatomic, strong) NSNumber                      *targetAchievedPer;
@property (nonatomic, strong) NSNumber                      *timeElapsed;
@property (nonatomic, strong) NSArray                       *productGoals; //GProduct
@property (nonatomic, strong) NSNumber                      *revenuetoHitGoal;
@property (nonatomic, strong) NSNumber                      *averageRevenueMTD;
@property (nonatomic, strong) NSNumber                      *avgRevenuPerUnit;
@property (nonatomic, strong) NSNumber                      *projectedRevenueEOM;
@property (nonatomic, strong) NSNumber                      *revenueToHitNextTier;
@property (nonatomic, strong) NSNumber                      *requiredNumberOfUnitsPerDay;
@property (nonatomic, strong) NSNumber                      *numberOfUnitsToHitNextTier;
@property (nonatomic, strong) NSString                      *notes;
@property (nonatomic, strong) NSString                      *verdict;
@property (nonatomic, strong) NSNumber                      *productARPDThisMonth;
@property (nonatomic, strong) NSNumber                      *productYTDIncRevenue;

- (Product *) getProduct:(NSNumber *)productId;
@end
