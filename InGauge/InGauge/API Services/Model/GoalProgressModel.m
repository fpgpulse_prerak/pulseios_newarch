//
//  GoalProgressModel.m
//  IN-Gauge
//
//  Created by Mehul Patel on 22/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "GoalProgressModel.h"
#import "AppDelegate.h"

@implementation GoalProgressModel

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return nil;
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    if (!isNull(array)) {
        
        // productGoals
        if ([property isEqualToString:@"productGoals"]) {
            NSMutableArray * arr = [[NSMutableArray alloc] init];
            for (NSDictionary * orgDict in array) {
                Product *product = [Product new];
                [HKJsonUtils updatePropertiesWithData:orgDict forObject:product];
                [arr addObject:product];
            }
            self.productGoals = [NSArray arrayWithArray:arr];
            arr = nil;
        }
    }
}

//---------------------------------------------------------------

- (Product *) getProduct:(NSNumber *)productId {
    @try {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.productId = %@", productId];
        NSArray *users = [self.productGoals filteredArrayUsingPredicate:predicate];
        if (users.count > 0) {
            return [users objectAtIndex:0];
        } else {
            return nil;
        }
    }
    @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return nil;
    }
}

//---------------------------------------------------------------

@end
