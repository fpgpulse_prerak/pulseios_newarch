//
//  GoalWeeklyPrediction.h
//  IN-Gauge
//
//  Created by Mehul Patel on 22/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"

@interface GoalWeeklyPrediction : NSObject <HKJsonPropertyMappings>

//@property (nonatomic, strong) NSArray                       *dayWiseReport; //GoalDayWiseReport
@property (nonatomic, strong) NSNumber                      *earnedRevenueWeekly;
@property (nonatomic, strong) NSNumber                      *reqRevWeeklyFPG;
@property (nonatomic, strong) NSNumber                      *reqRevWeeklySelf;
@property (nonatomic, strong) NSNumber                      *unitsRequiredPerDayFPG;
@property (nonatomic, strong) NSNumber                      *unitsRequiredPerDaySelf;
@property (nonatomic, strong) NSNumber                      *weekStartDeltaFPG;
@property (nonatomic, strong) NSNumber                      *weekStartDeltaSelf;
@property (nonatomic, strong) NSNumber                      *weeklyGoalAcheivedFPG;
@property (nonatomic, strong) NSNumber                      *weeklyGoalAcheivedSelf;

@end
