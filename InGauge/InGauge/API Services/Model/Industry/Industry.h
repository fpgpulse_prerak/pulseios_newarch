//
//  Industry.h
//  InGauge
//
//  Created by Mehul Patel on 17/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Hankai.h"

@interface Industry : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber      *industryId;
@property (nonatomic, strong) NSString      *activeStatus;
@property (nonatomic, strong) NSNumber      *indexRelatedOnly;
@property (nonatomic, strong) NSNumber      *indexMainOnly;
@property (nonatomic, strong) NSString      *name;
@property (nonatomic, strong) NSString      *industryDescription;

@end
