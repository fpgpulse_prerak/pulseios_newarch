//
//  IndustryList.h
//  InGauge
//
//  Created by Mehul Patel on 02/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "Industry.h"

@interface IndustryList : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSArray       *industryList; //Industry

@end
