//
//  Location.h
//  InGauge
//
//  Created by Mehul Patel on 31/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"

@interface Location : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber          *locationId;
@property (nonatomic, strong) NSNumber          *industryId;
@property (nonatomic, strong) NSNumber          *activeStatus;
@property (nonatomic, strong) NSNumber          *createdById;
@property (nonatomic, strong) NSNumber          *updatedById;
@property (nonatomic, strong) NSString          *adjacentProperty;
@property (nonatomic, strong) NSString          *amenitiesLevel;
@property (nonatomic, strong) NSString          *brand;
@property (nonatomic, strong) NSString          *cityLead;
@property (nonatomic, strong) NSString          *dataDeliveryMethod;
@property (nonatomic, strong) NSString          *dataStatus;
@property (nonatomic, strong) NSString          *dc1;
@property (nonatomic, strong) NSString          *dc1Email;
@property (nonatomic, strong) NSString          *dc1Phone;
@property (nonatomic, strong) NSString          *emailChampion;
@property (nonatomic, strong) NSString          *executioveLounge;
@property (nonatomic, strong) NSNumber          *force2fa;
@property (nonatomic, strong) NSString          *frontOfficeMgr;
@property (nonatomic, strong) NSString          *frontOfficeMgrEmail;
@property (nonatomic, strong) NSString          *generalManager;
@property (nonatomic, strong) NSString          *geoAddress;
@property (nonatomic, strong) NSNumber          *geoId;
@property (nonatomic, strong) NSString          *geoLocality;
@property (nonatomic, strong) NSString          *geoPostalcode;
@property (nonatomic, strong) NSString          *geoState;
@property (nonatomic, strong) NSString          *hasSpa;
@property (nonatomic, strong) NSString          *hotelAuditorMain;
@property (nonatomic, strong) NSString          *hotelAuditorMainEmail;
@property (nonatomic, strong) NSString          *hotelMetricsDataType;
@property (nonatomic, strong) NSString          *hotelMktTier;
@property (nonatomic, strong) NSString          *launchDate;
@property (nonatomic, strong) NSString          *legalName;
//@property (nonatomic, strong) NSString          *locationID;
@property (nonatomic, strong) NSString          *name;
@property (nonatomic, strong) NSString          *nearConventionCenter;
@property (nonatomic, strong) NSNumber          *noOfWorkingDaysPerMonth;
@property (nonatomic, strong) NSString          *perfomanceLeaderEmail;
@property (nonatomic, strong) NSString          *phone;
@property (nonatomic, strong) NSString          *pl;
@property (nonatomic, strong) NSString          *productMetricsValidationType;
@property (nonatomic, strong) NSString          *productUploadValidation;
@property (nonatomic, strong) NSString          *quarterback;
@property (nonatomic, strong) NSNumber          *regionId;
@property (nonatomic, strong) NSString          *regionName;
@property (nonatomic, strong) NSString          *regionCurrency;
@property (nonatomic, strong) NSNumber          *rooms;
@property (nonatomic, strong) NSNumber          *tenantId;
@property (nonatomic, strong) NSString          *tenantName;
@property (nonatomic, strong) NSString          *valuedViews;
@property (nonatomic, strong) NSString          *website;
@property (nonatomic, strong) NSNumber          *weightLastMonth;
@property (nonatomic, strong) NSNumber          *weightLastTwoMonth;
@property (nonatomic, strong) NSNumber          *weightLastThreeMonth;
@property (nonatomic, strong) NSNumber          *weightPriorYear;
@property (nonatomic, strong) NSNumber          *deemphasizeByTierOne;
@property (nonatomic, strong) NSNumber          *deemphasizeByTierTwo;
@property (nonatomic, strong) NSNumber          *deemphasizeByTierThree;
@property (nonatomic, strong) NSString          *updatedByWithDateTime;
@property (nonatomic, strong) NSString          *createdByWithDateTime;
@property (nonatomic, strong) NSString          *reviewedByWithDateTime;
@property (nonatomic, strong) NSDate            *unameTime;
@property (nonatomic, strong) NSDate            *cnameTime;

@end
