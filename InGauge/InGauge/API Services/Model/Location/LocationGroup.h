//
//  LocationGroup.h
//  InGauge
//
//  Created by Mehul Patel on 31/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"

@interface LocationGroup : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber          *_id;
@property (nonatomic, strong) NSNumber          *locationGroupID;
@property (nonatomic, strong) NSString          *locationGroupName;
@property (nonatomic, strong) NSNumber          *userID;
@property (nonatomic, strong) NSNumber          *isContributor;
@property (nonatomic, strong) NSNumber          *isOperator;
@property (nonatomic, strong) NSString          *locationGroupHierarchy;
@property (nonatomic, strong) NSNumber          *isAccessibleToUser;
@property (nonatomic, strong) NSNumber          *defaultHiddenGroup;
@property (nonatomic, strong) NSNumber          *isRetail;
@property (nonatomic, strong) NSString          *activeStatus;

// This is used to set parent location
@property (nonatomic, strong) NSString          *locationName;

@end
