//
//  LocationGroupList.m
//  InGauge
//
//  Created by Mehul Patel on 31/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "LocationGroupList.h"

@implementation LocationGroupList
//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return nil;
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    if (!isNull(array)) {
        // FPLocationPM
        @try {
            if ([property isEqualToString:MAIN_BODY]) {
                NSMutableArray *arr = [[NSMutableArray alloc] init];
                for (NSDictionary *details in array) {
                    LocationGroup *model = [LocationGroup new];
                    [HKJsonUtils updatePropertiesWithData:details forObject:model];
                    [arr addObject:model];
                }
                self.locationGroupList = [NSArray arrayWithArray:arr];
                arr = nil;
                
//                // SORT - Ascending order
//                NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"locationGroupName" ascending:YES];
//                NSArray *sortedArray = [self.locationGroupList sortedArrayUsingDescriptors:@[sortDescriptor1]];
//                self.locationGroupList = sortedArray;
            }
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
    }
}

//---------------------------------------------------------------

@end
