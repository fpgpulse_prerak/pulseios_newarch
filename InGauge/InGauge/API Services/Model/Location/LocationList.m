//
//  LocationList.m
//  InGauge
//
//  Created by Mehul Patel on 31/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "LocationList.h"

@implementation LocationList

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return nil;
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    if (!isNull(array)) {
        // FPLocationPM
        @try {
            if ([property isEqualToString:MAIN_BODY]) {
                NSMutableArray *arr = [[NSMutableArray alloc] init];
                for (NSDictionary *details in array) {
                    Location *model = [Location new];
                    [HKJsonUtils updatePropertiesWithData:details forObject:model];
                    [arr addObject:model];
                }
                self.locationList = [NSArray arrayWithArray:arr];
                arr = nil;
                
                // SORT - Ascending order
                NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
                NSArray *sortedArray = [self.locationList sortedArrayUsingDescriptors:@[sortDescriptor1]];
                self.locationList = sortedArray;
            }
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
    }
}

//---------------------------------------------------------------

- (Location *) getLocation:(NSNumber *)locationId {
    @try {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.locationId = %@", locationId];
        NSArray *users = [self.locationList filteredArrayUsingPredicate:predicate];
        if (users.count > 0) {
            return [users objectAtIndex:0];
        } else {
            return nil;
        }
    }
    @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return nil;
    }
}

//---------------------------------------------------------------

@end
