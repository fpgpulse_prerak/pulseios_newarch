//
//  EmailUser.h
//  IN-Gauge
//
//  Created by Bharat Chandera on 13/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HKJsonUtils.h"

@interface EmailUser : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSString              *email;
@property (nonatomic, strong) NSString              *firstName;
@property (nonatomic, strong) NSString              *lastName;
@property (nonatomic, strong) NSString              *name;
@property (nonatomic, strong) NSString              *roleName;
@property (nonatomic, strong) NSString              *rolePermission;

@property (nonatomic, assign) NSNumber              *activeStatus;
@property (nonatomic, strong) NSNumber              *userId;
@property (nonatomic, strong) NSNumber              *number;
@property (nonatomic, strong) NSNumber              *pulseId;
@property (nonatomic, strong) NSNumber              *roleId;
@property (nonatomic, strong) NSNumber              *isRead;
@property (nonatomic, strong) NSNumber              *isReadBySender;
@property (nonatomic, strong) NSDate                *hiredDate;

@end

