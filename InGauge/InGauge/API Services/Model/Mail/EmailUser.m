//
//  EmailUser.m
//  IN-Gauge
//
//  Created by Bharat Chandera on 13/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "EmailUser.h"
#import "Constant.h"

@implementation EmailUser

//---------------------------------------------------------------

#pragma mark
#pragma mark HKJsonPropertyMappings methods

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id" : @"userId"};
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------




@end
