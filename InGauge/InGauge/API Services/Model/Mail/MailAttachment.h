//
//  MailAttachment.h
//  IN-Gauge
//
//  Created by Mehul Patel on 27/09/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface MailAttachment : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber              *attachmentId;
@property (nonatomic, strong) NSString              *fileName;
@property (nonatomic, strong) NSString              *fileType;

@end
