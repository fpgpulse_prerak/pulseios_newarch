//
//  MailDetails.h
//  IN-Gauge
//
//  Created by Mehul Patel on 14/09/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "MailAttachment.h"
#import "UserMessageList.h"

@interface MailDetails : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber              *userMailId;
//@property (nonatomic, strong) NSNumber              *hasReplies;
@property (nonatomic, strong) NSNumber              *industryId;
//@property (nonatomic, strong) NSNumber              *isDeleted;
//@property (nonatomic, strong) NSNumber              *isRead;
@property (nonatomic, strong) NSDate                *createdOn;
@property (nonatomic, strong) NSDate                *updatedOn;
@property (nonatomic, strong) NSString              *subject;
@property (nonatomic, strong) NSString              *message;
@property (nonatomic, strong) NSArray               *userMessageRecipientList; //UserMessageList
@property (nonatomic, strong) NSArray               *mobileMailAttachmentList; //MailAttachment
@property (nonatomic, strong) NSArray               *userMessageSenderList;     //User

//@property (nonatomic, strong) NSString              *parentMessage;
//@property (nonatomic, strong) MailDetails           *parentMessageObject;
//@property (nonatomic, strong) NSString              *recipientEmail;
//@property (nonatomic, strong) NSNumber              *recipientId;
//@property (nonatomic, strong) NSString              *recipientName;
//@property (nonatomic, strong) NSString              *senderEmail;
//@property (nonatomic, strong) NSNumber              *senderId;
//@property (nonatomic, strong) NSString              *senderName;
//@property (nonatomic, strong) NSString              *lastName;
//@property (nonatomic, strong) NSNumber              *userMessageId;
//@property (nonatomic, strong) NSNumber              *readTime;
// Below value is not recieved from server
@property (nonatomic, strong) NSAttributedString    *htmlString;

- (void) convertHtmlContentToAttributedString;
- (NSArray *) getSendersEmailAddress;

@end
