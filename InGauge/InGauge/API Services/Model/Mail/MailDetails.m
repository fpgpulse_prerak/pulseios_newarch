//
//  MailDetails.m
//  IN-Gauge
//
//  Created by Mehul Patel on 14/09/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "MailDetails.h"

@implementation MailDetails

//---------------------------------------------------------------

#pragma mark
#pragma mark HKJsonPropertyMappings methods

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id" : @"userMailId"};
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void) handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    if (!isNull(array)) {
        
        //userMessageRecipientList
        if ([property isEqualToString:@"userMessageRecipientList"]) {
            NSMutableArray *arr = [[NSMutableArray alloc] init];
            for (NSDictionary *users in array) {
                UserMessageList *messageList = [UserMessageList new];
                [HKJsonUtils updatePropertiesWithData:users forObject:messageList];
                [arr addObject:messageList];
            }
            self.userMessageRecipientList = [NSArray arrayWithArray:arr];
            arr = nil;
        }
        
        // mobileMailAttachment
        if ([property isEqualToString:@"mobileMailAttachmentList"]) {
            NSMutableArray *arr = [[NSMutableArray alloc] init];
            for (NSDictionary *users in array) {
                MailAttachment *attachment = [MailAttachment new];
                [HKJsonUtils updatePropertiesWithData:users forObject:attachment];
                [arr addObject:attachment];
            }
            self.mobileMailAttachmentList = [NSArray arrayWithArray:arr];
            arr = nil;
        }
        
        // userMessageSenderList
        if ([property isEqualToString:@"userMessageSenderList"]) {
            NSMutableArray *arr = [[NSMutableArray alloc] init];
            for (NSDictionary *users in array) {
                User *user = [User new];
                [HKJsonUtils updatePropertiesWithData:users forObject:user];
                [arr addObject:user];
            }
            self.userMessageSenderList = [NSArray arrayWithArray:arr];
            arr = nil;
        }
    }
}

//---------------------------------------------------------------

- (void) handleDictionaryValue:(NSDictionary *)dictionary forProperty:(NSString *)property {
    
//    if (!isNull(dictionary)) {
//        //parentMessage
//        if ([property isEqualToString:@"parentMessageObject"]) {
//            MailDetails *userMail = [MailDetails new];
//            [HKJsonUtils updatePropertiesWithHTMLContent:dictionary forObject:userMail];
//            self.parentMessageObject = userMail;
//        }
//    }
}

//---------------------------------------------------------------

- (void) convertHtmlContentToAttributedString {
    @try {
//        NSString *whiteHtmlBodyString = [NSString stringWithFormat:@"<div id ='foo' align='justify' style='font-size:10px; font-family:%@; color:#ffffff';>%@<div>", AVENIR_FONT, self.userMessageMessage];

        NSString *whiteHtmlBodyString = [NSString stringWithFormat:@"<div id ='foo' align='justify' color:#000000';>%@<div>", self.message];
        
        NSError *error = nil;
        NSAttributedString *attrStr2 = [[NSAttributedString alloc] initWithData:[whiteHtmlBodyString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:&error];
        self.htmlString = attrStr2;
        
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception);
    }
}

//---------------------------------------------------------------

- (NSArray *) getSendersEmailAddress {
    NSMutableArray *tempArray = [NSMutableArray new];
    for (User *user in self.userMessageSenderList) {
        [tempArray addObject:user.email];
    }
    NSArray *emails = [NSArray arrayWithArray:tempArray];
    tempArray = nil;
    return emails;
}

//---------------------------------------------------------------

@end
