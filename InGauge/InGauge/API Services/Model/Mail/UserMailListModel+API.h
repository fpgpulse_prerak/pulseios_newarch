//
//  UserMailListModel+API.h
//  pulse
//
//  Created by Mehul Patel on 29/12/16.
//  Copyright © 2016 frontline performance group. All rights reserved.
//

#import "UserMailListModel.h"
#import "ServiceManager.h"
#import "UserMailRecipientList.h"



#define MAIL_RECIPIENT  @"emailId"
#define MAIL_PAYLOAD    @"payload"
#define MAIL_SUBJECT    @"subject"
#define MAIL_MESSAGE    @"message"

typedef void (^GetUserMailList)(NSError * localizedError, UserMailListModel *mailList);
typedef void (^GetUnreadMailsCount)(NSError * localizedError, NSNumber *count);
typedef void (^DeleteUserMails)(NSError * localizedError, NSDictionary *details);
typedef void (^ReplyUserMails)(NSString * localizedError, NSDictionary *details);
typedef void (^GetUserMailRecipient)(NSError * localizedError, UserMailRecipientList *recipients);

@interface UserMailListModel (API)

+ (void)getUserMailList:(NSDictionary *)dict whenFinish:(GetUserMailList)whenFinish;
+ (void)getUnreadMailsCount:(UserMailSearchForm *)searchForm whenFinish:(GetUnreadMailsCount)whenFinish;
+ (void)removeUserMails:(NSDictionary *)parameter whenFinish:(DeleteUserMails)whenFinish;
+ (void)replyUserMail:(NSDictionary *)details whenFinish:(ReplyUserMails)whenFinish;
+ (void)updateReadMail:(NSNumber *)mailId whenFinish:(ReplyUserMails)whenFinish;
+ (void)getRecipients:(GetUserMailRecipient)whenFinish;
+ (void)sendMailToRecipients:(NSDictionary *)recipientArray whenFinish:(ReplyUserMails)whenFinish;
+ (void)getMaildetails:(NSDictionary *)details whenFinish:(ReplyUserMails)whenFinish;

@end
