//
//  UserMailListModel+API.m
//  pulse
//
//  Created by Mehul Patel on 29/12/16.
//  Copyright © 2016 frontline performance group. All rights reserved.
//

#import "UserMailListModel+API.h"
#import "AppDelegate.h"
@implementation UserMailListModel (API)

//---------------------------------------------------------------

#pragma mark
#pragma mark Service methods

//---------------------------------------------------------------

+ (void)getUserMailList:(NSDictionary *)dict whenFinish:(GetUserMailList)whenFinish {
    
    DLog(@"\n\n\n\n==========>>> Get Mail list - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager postWebServiceCallwithParam:GET_MAIL_Find parameters:dict whenFinish:^(NSString *errorMessage, NSDictionary *response) {
        DLog(@"\n\n\n\n==========>>> Get Mail list - Service Call finish <<<==========\n\n\n\n");

        UserMailListModel *mailList = [UserMailListModel new];
        if (errorMessage == nil) {
            // Use JSON parse method which allows HTML tags
            if ([[response allKeys] containsObject:MAIN_BODY]) {
                NSDictionary *details = [response objectForKey:MAIN_BODY];
                [HKJsonUtils updatePropertiesWithHTMLContent:details forObject:mailList];
            }
        }
        
        if (whenFinish) {
            whenFinish(nil, mailList);
        }
    }];
}

//---------------------------------------------------------------

+ (void)getUnreadMailsCount:(UserMailSearchForm *)searchForm whenFinish:(GetUnreadMailsCount)whenFinish {
    
    [ServiceManager getWebServiceCall:GET_UNREAD_MAILS_COUNT parameters:nil whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        NSNumber *count = [NSNumber numberWithInteger:0];
        if (errorMessage == nil) {
            // update mail count
            if ([[response allKeys] containsObject:MAIN_BODY]) {
                NSDictionary *data = [response objectForKey:MAIN_BODY];
                
                if ([[data allKeys] containsObject:@"unreadCount"]) {
                   count = [data objectForKey:@"unreadCount"];
                }
            }
        }        
        if (whenFinish) {
            whenFinish(nil, count);
        }
    }];
}

//---------------------------------------------------------------

+ (void)removeUserMails:(NSDictionary *)parameter whenFinish:(DeleteUserMails)whenFinish {
    
    // Convert array to comma seperated string
    NSString *urlString = [NSString stringWithFormat:@"%@", DELETE_USER_MAIL];

    [ServiceManager putWebServiceCallwithParam:urlString parameters:parameter whenFinish:^(NSString *errorMessage, NSDictionary *response) {
        if (errorMessage == nil) {
        }
        
        if (whenFinish) {
            whenFinish(nil, response);
        }
    }];
}

//---------------------------------------------------------------

+ (void)replyUserMail:(NSDictionary *)details whenFinish:(ReplyUserMails)whenFinish {
    
    NSString *urlString = [NSString stringWithFormat:@"%@", REPLY_USER_MAIL];
    
    [ServiceManager postWebServiceCall:urlString parameters:nil withBody:details whenFinish:^(NSString *errorMessage, NSDictionary *response) {
        if (whenFinish) {
            whenFinish(errorMessage, response);
        }
    }];
}


//---------------------------------------------------------------

+ (void)updateReadMail:(NSNumber *)mailId whenFinish:(ReplyUserMails)whenFinish {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@", UPDATE_USER_MAIL_READ, mailId.stringValue];
    [ServiceManager postWebServiceCall:urlString parameters:nil withBody:nil  whenFinish:^(NSString *errorMessage, NSDictionary *response) {
        if (whenFinish) {
            whenFinish(nil, response);
        }
    }];
}

//---------------------------------------------------------------

+ (void)getRecipients:(GetUserMailRecipient)whenFinish {
//    GET_USER_MAIL_RECIPIENT
    
    NSString *stringUrl = [NSString stringWithFormat:@"%@/%d?getContributorsOnly=false&getOperatorsOnly=false&tenantLocationID=-1&getWithAllStatus=false&isWeightWiseUser=true&allowSameWeightUser=true&getPulseMailContacts=true", GET_AVAILABLE_USER_ACTION, App_Delegate.appfilter.tenantId.intValue];
    NSString *urlString = [stringUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

    DLog(@"\n\n\n\n==========>>> Get Recipients - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager getWebServiceCall:urlString parameters:nil whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get Recipients - Service Call Finish <<<==========\n\n\n\n");
        UserMailRecipientList *recipientList = nil;
        if (errorMessage == nil) {
            recipientList = [UserMailRecipientList new];
            [HKJsonUtils updatePropertiesWithData:response forObject:recipientList];
        }
        
        if (whenFinish) {
            whenFinish(nil, recipientList);
        }
    }];
}

//---------------------------------------------------------------

+ (void)sendMailToRecipients:(NSDictionary *)details whenFinish:(ReplyUserMails)whenFinish {
    
    NSString *urlString = [NSString stringWithFormat:@"%@", SEND_USER_MAIL];
    
    [ServiceManager postWebServiceCallwithParam:urlString parameters:details whenFinish:^(NSString *errorMessage, NSDictionary *response) {
        if (errorMessage == nil) {
            
        }
        
        if (whenFinish) {
            whenFinish(nil, response);
        }
    }];
}

//---------------------------------------------------------------

+ (void)getMaildetails:(NSDictionary *)details whenFinish:(ReplyUserMails)whenFinish {
    
    NSString *urlString = [NSString stringWithFormat:@"%@", GET_MAIL_Details];
    
    [ServiceManager getWebServiceCall:urlString parameters:details whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {            
        if (whenFinish) {
            whenFinish(nil, response);
        }
    }];
}

//---------------------------------------------------------------



@end
