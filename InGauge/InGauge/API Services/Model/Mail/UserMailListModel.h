//
//  UserMailListModel.h
//  pulse
//
//  Created by Mehul Patel on 29/12/16.
//  Copyright © 2016 frontline performance group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HKJsonUtils.h"
#import "UserMailModel.h"
#import "UserMailSearchForm.h"

@interface UserMailListModel : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber              *status;
@property (nonatomic, strong) NSNumber              *statusCode;
@property (nonatomic, strong) NSNumber              *total;
@property (nonatomic, strong) NSNumber              *totalInList;
@property (nonatomic, retain) NSArray               *objects; //UserMailModel

// These parameters are not from service response
//@property (nonatomic, strong) UserMailSearchForm    *searchForm;
@end
