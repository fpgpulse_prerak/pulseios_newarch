//
//  UserMailListModel.m
//  pulse
//
//  Created by Mehul Patel on 29/12/16.
//  Copyright © 2016 frontline performance group. All rights reserved.
//

#import "UserMailListModel.h"

@implementation UserMailListModel

//---------------------------------------------------------------

#pragma mark
#pragma mark HKJsonPropertyMappings methods

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return nil;
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void) handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    if (!isNull(array)) {
        
        // UserMailModel
        if ([property isEqualToString:@"data"]) {
            NSMutableArray *arr = [[NSMutableArray alloc] init];
            for (NSDictionary *mails in array) {
                UserMailModel *mailModel = [UserMailModel new];
                [HKJsonUtils updatePropertiesWithHTMLContent:mails forObject:mailModel];
                [arr addObject:mailModel];
            }
            self.objects = [NSArray arrayWithArray:arr];
            arr = nil;                       
        }
    }
}

//---------------------------------------------------------------

@end
