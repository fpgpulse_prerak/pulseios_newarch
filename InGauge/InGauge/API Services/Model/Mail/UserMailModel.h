//
//  UserMailModel.h
//  pulse
//
//  Created by Mehul Patel on 29/12/16.
//  Copyright © 2016 frontline performance group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HKJsonUtils.h"
#import "User.h"


@interface UserMailModel : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber              *userMailId;
@property (nonatomic, strong) NSNumber              *id_ur;
@property (nonatomic, strong) NSNumber              *isDeleted_ur;
@property (nonatomic, strong) NSNumber              *isDeleted_usrSender;
@property (nonatomic, strong) NSNumber              *isRead_ur;
@property (nonatomic, strong) NSNumber              *recipientId_ur;
@property (nonatomic, strong) NSNumber              *senderId;
@property (nonatomic, strong) NSNumber              *usermailId_ur;
@property (nonatomic, strong) NSNumber              *createdOn;
@property (nonatomic, strong) NSNumber              *updatedOn;

@property (nonatomic, strong) NSString              *unameTime;
@property (nonatomic, strong) NSString              *cnameTime;
@property (nonatomic, strong) NSString              *createdByWithDateTime;
@property (nonatomic, strong) NSString              *firstName;
@property (nonatomic, strong) NSString              *lastName;
@property (nonatomic, strong) NSString              *message;
@property (nonatomic, strong) NSString              *subject;
@property (nonatomic, strong) NSString              *recipient_name;

// Below value is not recieved from server
@property (nonatomic, strong) NSAttributedString    *htmlString;

- (void) convertHtmlContentToAttributedString;
- (NSString *) getFullName;

@end


/*
 cnameTime = "2017-07-17 04:55 PM";
 createdByWithDateTime = " - Jul 17, 2017 04:55:30 PM";
 createdOn = 1500310530200;
 firstName = Prerak;
 "hasReplies_ur" = 0;
 id = 4224;
 "id_ur" = 4237;
 "isDeleted_ur" = 0;
 "isDeleted_usrSender" = 0;
 "isRead_ur" = 1;
 lastName = Thakur;
 message = ABC;
 parentMessage = 0;
 "recipientId_ur" = 646;
 reviewedByWithDateTime = " - ";
 senderId = 646;
 subject = "New Mail";
 unameTime = "2017-07-17 04:55 PM";
 updatedByWithDateTime = " - Jul 17, 2017 04:55:30 PM";
 updatedOn = 1500310530200;
 "usermailId_ur" = 4224;

 */
