//
//  UserMailModel.m
//  pulse
//
//  Created by Mehul Patel on 29/12/16.
//  Copyright © 2016 frontline performance group. All rights reserved.
//

#import "UserMailModel.h"

@implementation UserMailModel

//---------------------------------------------------------------

#pragma mark
#pragma mark HKJsonPropertyMappings methods

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id" : @"userMailId"};
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void) handleDictionaryValue:(NSDictionary *)dictionary forProperty:(NSString *)property {
    
    if (!isNull(dictionary)) {
        
//        //parentMessage
//        if ([property isEqualToString:@"parentMessage"]) {
//            UserMailModel *userMail = [UserMailModel new];
//            [HKJsonUtils updatePropertiesWithHTMLContent:dictionary forObject:userMail];
//            self.parentMessage = userMail;
//        }
    }
}

//---------------------------------------------------------------

- (void) convertHtmlContentToAttributedString {
    @try {
        NSString *whiteHtmlBodyString = [NSString stringWithFormat:@"<div id ='foo' align='justify' color:#000000';>%@<div>", self.message];
        
        NSAttributedString *attrStr2 = [[NSAttributedString alloc] initWithData:[whiteHtmlBodyString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        self.htmlString = attrStr2;

    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception);
    }
}

//---------------------------------------------------------------

- (NSString *) getFullName {
    return [NSString stringWithFormat:@"%@ %@",self.firstName,self.lastName];
}


@end
