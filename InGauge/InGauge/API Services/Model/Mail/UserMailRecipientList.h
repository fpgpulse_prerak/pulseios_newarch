//
//  UserMailRecipientList.h
//  pulse
//
//  Created by Mehul Patel on 05/01/17.
//  Copyright © 2017 frontline performance group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HKJsonUtils.h"

@interface UserMailRecipientList : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSArray           *employees; //EmailUser

@end
