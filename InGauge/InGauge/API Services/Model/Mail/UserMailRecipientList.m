//
//  UserMailRecipientList.m
//  pulse
//
//  Created by Mehul Patel on 05/01/17.
//  Copyright © 2017 frontline performance group. All rights reserved.
//

#import "UserMailRecipientList.h"
#import "Constant.h"
#import "EmailUser.h"

@implementation UserMailRecipientList
//---------------------------------------------------------------

#pragma mark
#pragma mark HKJsonPropertyMappings methods

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return nil;
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void) handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    if (!isNull(array)) {
        
        // User
        if ([property isEqualToString:@"data"]) {
            NSMutableArray *arr = [[NSMutableArray alloc] init];
            for (NSDictionary *users in array) {
                EmailUser *recipient = [EmailUser new];
                [HKJsonUtils updatePropertiesWithData:users forObject:recipient];
                [arr addObject:recipient];
            }
            self.employees = [NSArray arrayWithArray:arr];
            arr = nil;
            
            // Remove login user name from recipient list
            NSPredicate *preciate = [NSPredicate predicateWithFormat:@"SELF.email = %@", [User signedInUser].email];
            NSArray *filterArray = [self.employees filteredArrayUsingPredicate:preciate];
            if (filterArray.count > 0) {
                NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.employees];
                EmailUser *user = filterArray[0];
                [tempArray removeObject:user];
                self.employees = [NSArray arrayWithArray:tempArray];
                tempArray = nil;
            }
            
            // SORT - Ascending order
            NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
            NSArray *sortedArray = [self.employees sortedArrayUsingDescriptors:@[sortDescriptor1]];
            self.employees = sortedArray;
        }
    }
}

//---------------------------------------------------------------

@end
