//
//  UserMailSearchForm.h
//  pulse
//
//  Created by Mehul Patel on 29/12/16.
//  Copyright © 2016 frontline performance group. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserMailSearchForm : NSObject

//@property (nonatomic, strong) NSDate                *fromDate;
//@property (nonatomic, strong) NSDate                *toDate;
@property (nonatomic, strong) NSString              *keywords;
@property (nonatomic, strong) NSNumber              *page;
@property (nonatomic, strong) NSNumber              *size;
@property (nonatomic, strong) NSString              *messageType;
@property (nonatomic, strong) NSString              *readStatus;

- (NSDictionary *)serialize;

@end
