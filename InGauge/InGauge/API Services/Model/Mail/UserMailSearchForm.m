//
//  UserMailSearchForm.m
//  pulse
//
//  Created by Mehul Patel on 29/12/16.
//  Copyright © 2016 frontline performance group. All rights reserved.
//

#import "UserMailSearchForm.h"

@implementation UserMailSearchForm

- (NSDictionary *)serialize {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];

//    // Start date and End date
//    if (self.fromDate != nil) {
//        [dict setValue:[self.fromDate toNSString:SERVICE_DATE_TIME_FORMAT]  forKey:@"fromDate"];
//    }
//    if (self.toDate != nil) {
//        [dict setValue:[self.toDate toNSString:SERVICE_DATE_TIME_FORMAT] forKey:@"toDate"];
//    }
    if (self.keywords != nil) {
        [dict setValue:self.keywords forKey:@"keywords"];
    }
    if (self.page != nil) {
        [dict setValue:self.page forKey:@"page"];
    }
    if (self.size != nil) {
        [dict setValue:self.size forKey:@"size"];
    }
    if (self.messageType != nil) {
        [dict setValue:self.messageType forKey:@"messageType"];
    }
    if (self.readStatus != nil) {
        [dict setValue:self.readStatus forKey:@"readStatus"];
    }
    return [NSDictionary dictionaryWithDictionary:dict];
}

@end
