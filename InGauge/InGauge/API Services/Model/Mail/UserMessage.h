//
//  UserMessage.h
//  IN-Gauge
//
//  Created by Mehul Patel on 27/09/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface UserMessage : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber              *messageId;
@property (nonatomic, strong) NSString              *createdOn;
//@property (nonatomic, strong) NSNumber              *indexMainOnly = 0;
//@property (nonatomic, strong) NSNumber              *indexRelatedOnly = 0;
@property (nonatomic, strong) NSNumber              *industryId;
@property (nonatomic, strong) NSString              *message;
@property (nonatomic, strong) NSString              *subject;
@property (nonatomic, strong) NSString              *updatedOn;
//@property (nonatomic, strong) NSNumber              *userMessageRecipientList = "<null>";
//@property (nonatomic, strong) NSNumber              *userMessageSenderList = "<null>";

@property (nonatomic, strong) NSAttributedString    *htmlString;

- (NSAttributedString *) convertHtmlContentToAttributedString;

@end
