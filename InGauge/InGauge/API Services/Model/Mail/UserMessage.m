//
//  UserMessage.m
//  IN-Gauge
//
//  Created by Mehul Patel on 27/09/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "UserMessage.h"

@implementation UserMessage

//---------------------------------------------------------------

#pragma mark
#pragma mark HKJsonPropertyMappings methods

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id" : @"messageId"};
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (NSAttributedString *) convertHtmlContentToAttributedString {
    @try {
        NSString *whiteHtmlBodyString = [NSString stringWithFormat:@"<div id ='foo' align='justify' color:#000000';>%@<div>", self.message];
        
        NSAttributedString *attrStr2 = [[NSAttributedString alloc] initWithData:[whiteHtmlBodyString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        self.htmlString = attrStr2;
        return attrStr2;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception);
        return [NSAttributedString new];
    }
}

//---------------------------------------------------------------


@end
