//
//  UserMessageList.h
//  IN-Gauge
//
//  Created by Mehul Patel on 27/09/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "UserMessage.h"
#import "MailAttachment.h"

@interface UserMessageList : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber              *userMessageId;
@property (nonatomic, strong) NSNumber              *industryId;
//@property (nonatomic, strong) NSNumber              *indexRelatedOnly": false,
//@property (nonatomic, strong) NSNumber              *indexMainOnly": false,
@property (nonatomic, strong) NSNumber              *isDeleted;
@property (nonatomic, strong) NSDate                *readTime;
@property (nonatomic, strong) NSNumber              *isRead;
@property (nonatomic, strong) NSNumber              *hasReplies;
@property (nonatomic, strong) User                  *recipient;
@property (nonatomic, strong) NSNumber              *parentMessage;
@property (nonatomic, strong) NSArray               *mobileMailAttachmentList; //MailAttachment
@property (nonatomic, strong) UserMessage           *userMessage; //UserMessage
@property (nonatomic, strong) UserMessageList       *parentMessageObject;
@property (nonatomic, strong) NSArray               *senderList; //User
@property (nonatomic, strong) User                  *sender;

@property (nonatomic, strong) NSAttributedString    *htmlString;

- (void) convertHtmlContentToAttributedString;
- (NSArray *) getSendersEmailAddress;

@end
