//
//  UserMessageList.m
//  IN-Gauge
//
//  Created by Mehul Patel on 27/09/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "UserMessageList.h"

@implementation UserMessageList

//---------------------------------------------------------------

#pragma mark
#pragma mark HKJsonPropertyMappings methods

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id" : @"userMessageId"};
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void) handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    if (!isNull(array)) {
        
        // mobileMailAttachment
        if ([property isEqualToString:@"mobileMailAttachmentList"]) {
            NSMutableArray *arr = [[NSMutableArray alloc] init];
            for (NSDictionary *detail in array) {
                MailAttachment *attachment = [MailAttachment new];
                [HKJsonUtils updatePropertiesWithData:detail forObject:attachment];
                [arr addObject:attachment];
            }
            self.mobileMailAttachmentList = [NSArray arrayWithArray:arr];
            arr = nil;
        }
        
        // senderList
        if ([property isEqualToString:@"senderList"]) {
            NSMutableArray *arr = [[NSMutableArray alloc] init];
            for (NSDictionary *users in array) {
                User *user = [User new];
                [HKJsonUtils updatePropertiesWithData:users forObject:user];
                [arr addObject:user];
            }
            self.senderList = [NSArray arrayWithArray:arr];
            arr = nil;
        }
    }
}

//---------------------------------------------------------------

- (void) handleDictionaryValue:(NSDictionary *)dictionary forProperty:(NSString *)property {
    
    if (!isNull(dictionary)) {
        //userMessage
        if ([property isEqualToString:@"userMessage"]) {
            UserMessage *userMail = [UserMessage new];
            [HKJsonUtils updatePropertiesWithHTMLContent:dictionary forObject:userMail];
            self.userMessage = userMail;
        }
        
        //userMessage
        if ([property isEqualToString:@"recipient"]) {
            User *user = [User new];
            [HKJsonUtils updatePropertiesWithData:dictionary forObject:user];
            self.recipient = user;
        }
        
        //sender
        if ([property isEqualToString:@"sender"]) {
            User *user = [User new];
            [HKJsonUtils updatePropertiesWithData:dictionary forObject:user];
            self.sender = user;
        }
        
        //parentMessageObject
        if ([property isEqualToString:@"parentMessageObject"]) {
            UserMessageList *messageList = [UserMessageList new];
            [HKJsonUtils updatePropertiesWithData:dictionary forObject:messageList];
            self.parentMessageObject = messageList;
        }
    }
}

//---------------------------------------------------------------

- (void) convertHtmlContentToAttributedString {
    @try {
        NSString *whiteHtmlBodyString = [NSString stringWithFormat:@"<div id ='foo' align='justify' color:#000000';>%@<div>", self.userMessage.message];
        
        NSAttributedString *attrStr2 = [[NSAttributedString alloc] initWithData:[whiteHtmlBodyString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        self.htmlString = attrStr2;
        
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception);
    }
}

//---------------------------------------------------------------

- (NSArray *) getSendersEmailAddress {
    NSMutableArray *tempArray = [NSMutableArray new];
    for (User *user in self.senderList) {
        if (user.email) {
            [tempArray addObject:user.email];
        }
    }
    NSArray *emails = [NSArray arrayWithArray:tempArray];
    tempArray = nil;
    return emails;
}

//---------------------------------------------------------------

@end
