//
//  Menu.h
//  IN-Gauge
//
//  Created by Mehul Patel on 24/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"

@interface Menu : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSString      *title;
@property (nonatomic, strong) NSString      *url;
@property (nonatomic, strong) NSArray       *subMenus; //Menu

@end
