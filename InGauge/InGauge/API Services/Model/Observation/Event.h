//
//  Event.h
//  IN-Gauge
//
//  Created by Mehul Patel on 13/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "QuestionAnswer.h"
#import "GreatJob.h"
#import "QuestionSection.h"

@interface Event : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber      *eventId;
@property (nonatomic, strong) NSNumber      *industryId;
@property (nonatomic, strong) NSString      *activeStatus;
@property (nonatomic, strong) NSNumber      *createdById;
@property (nonatomic, strong) NSNumber      *updatedById;
@property (nonatomic, strong) NSString      *cByIdentity;
@property (nonatomic, strong) NSString      *uByIdentity;
@property (nonatomic, strong) NSNumber      *createdOn;
@property (nonatomic, strong) NSNumber      *updatedOn;
@property (nonatomic, strong) NSString      *androidVersion;
@property (nonatomic, strong) NSNumber      *donotMailToAgent;
@property (nonatomic, strong) NSString      *endDate;
@property (nonatomic, strong) NSString      *iosVersion;
@property (nonatomic, strong) NSNumber      *isScheduled;
@property (nonatomic, strong) NSString      *note;
@property (nonatomic, strong) NSNumber      *questionnaireId;
@property (nonatomic, strong) NSString      *questionnaireName;
@property (nonatomic, strong) NSString      *recommendation;
@property (nonatomic, strong) NSNumber      *questionnaireSurveyCategoryId;
@property (nonatomic, strong) NSString      *questionnaireSurveyCategoryName;
@property (nonatomic, strong) NSNumber      *respondentId;
@property (nonatomic, strong) NSString      *respondentName;
@property (nonatomic, strong) NSNumber      *scoreVal;
@property (nonatomic, strong) NSString      *startDate;
@property (nonatomic, strong) NSString      *surveyStatus;
@property (nonatomic, strong) NSString      *strengthText;
@property (nonatomic, strong) NSString      *surveyEntityType;
@property (nonatomic, strong) NSNumber      *surveyorId;
@property (nonatomic, strong) NSString      *surveyorName;
@property (nonatomic, strong) NSNumber      *surveyTakenTime;
@property (nonatomic, strong) NSString      *symptomText;
@property (nonatomic, strong) NSNumber      *tenantLocationId;
@property (nonatomic, strong) NSString      *tenantLocationName;
@property (nonatomic, strong) NSNumber      *calendarEventId;
@property (nonatomic, strong) NSArray       *strengths;         //GreatJob
@property (nonatomic, strong) NSArray       *surveyStrengths;
@property (nonatomic, strong) NSArray       *questionAnswer;    //QuestionAnswer
@property (nonatomic, strong) NSString      *subject;
@property (nonatomic, strong) NSString      *location;
@property (nonatomic, strong) NSString      *eventDescription;
@property (nonatomic, strong) NSNumber      *tenantId;
@property (nonatomic, strong) NSString      *unameTime;
@property (nonatomic, strong) NSString      *cnameTime;
@property (nonatomic, strong) NSString      *reviewedByWithDateTime;
@property (nonatomic, strong) NSString      *createdByWithDateTime;
@property (nonatomic, strong) NSString      *updatedByWithDateTime;
@property (nonatomic, strong) NSArray       *questionnaireQuestionSections; //QuestionSection

// For new event - when you create event from mobile platform
@property (nonatomic, strong) NSArray       *oneToOneIds;
@property (nonatomic, strong) NSNumber      *donotDisplayToAgent;
@property (nonatomic, strong) NSString      *mapTo;

// Local parameter
@property (nonatomic, strong) NSNumber      *timeDifference;

- (NSDictionary *) searializeToSaveEvent;
- (NSDictionary *) searialize;

- (NSString *) getGreatJobOnString;
- (QuestionAnswer *) getQuestionAnswer:(NSNumber *)questionId;
@end
