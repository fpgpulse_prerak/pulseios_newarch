//
//  Event.m
//  IN-Gauge
//
//  Created by Mehul Patel on 13/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "Event.h"

@implementation Event

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id" : @"eventId", @"description" : @"eventDescription"};
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    if (!isNull(array)) {
        @try {
            
            // QuestionAnswer
            if ([property isEqualToString:@"questionAnswer"]) {
                NSMutableArray *arr = [[NSMutableArray alloc] init];
                for (NSDictionary *details in array) {
                    QuestionAnswer *model = [QuestionAnswer new];
                    [HKJsonUtils updatePropertiesWithData:details forObject:model];
                    [arr addObject:model];
                }
                self.questionAnswer = [NSArray arrayWithArray:arr];
                arr = nil;
            }
            
            // Strengths
            if ([property isEqualToString:@"strengths"]) {
                NSMutableArray *arr = [[NSMutableArray alloc] init];
                for (NSDictionary *details in array) {
                    GreatJob *model = [GreatJob new];
                    [HKJsonUtils updatePropertiesWithData:details forObject:model];
                    [arr addObject:model];
                }
                self.strengths = [NSArray arrayWithArray:arr];
                arr = nil;
            }
            
            // questionnaireQuestionSections
            if ([property isEqualToString:@"questionnaireQuestionSections"]) {
                NSMutableArray *arr = [[NSMutableArray alloc] init];
                for (NSDictionary *details in array) {
                    QuestionSection *questionSection = [QuestionSection new];
                    [HKJsonUtils updatePropertiesWithData:details forObject:questionSection];
                    [arr addObject:questionSection];
                }
                self.questionnaireQuestionSections = [NSArray arrayWithArray:arr];
                arr = nil;
            }            
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
    }
}

//---------------------------------------------------------------

- (void)handleDictionaryValue:(NSDictionary *)dictionary forProperty:(NSString *)property {
//    
////    questionnaireQuestionSections
//        if (!isNull(dictionary)) {
//            if ([property isEqualToString:@"questionnaireQuestionSections"]) {
//            }
//        }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Helper methods

//---------------------------------------------------------------

- (NSString *) getGreatJobOnString {
    NSString *jobString = @"";
    NSMutableString *string = [NSMutableString new];
    for (GreatJob *model in self.strengths) {
        NSString *nameString = [Helper removeWhiteSpaceAndNewLineCharacter:model.name];
        if (nameString && nameString.length > 0) {
            [string appendString:nameString];
            [string appendString:@", "];
        }
    }
    if (string.length > 2) {
        NSString *fullString = [NSString stringWithString:string];
        NSString *truncatedString = [fullString substringToIndex:[fullString length] - 2];
        jobString = truncatedString;
    }
    return jobString;
}

//---------------------------------------------------------------

- (QuestionAnswer *) getQuestionAnswer:(NSNumber *)questionId {
    @try {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.questionId.intValue = %d", questionId.intValue];
        NSArray *questions = [self.questionAnswer filteredArrayUsingPredicate:predicate];
        if (questions.count > 0) {
            return [questions objectAtIndex:0];
        } else {
            return nil;
        }
    }
    @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return nil;
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Searialize methods

//---------------------------------------------------------------

- (NSDictionary *) searializeToSaveEvent {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    
    //Note: For new event it will be nill and for edit event it will be there
    if (self.eventId != nil) {
        [dict setValue:self.eventId forKey:@"id"];
    }
    
    if (self.tenantLocationId != nil) {
        [dict setValue:self.tenantLocationId forKey:@"tenantLocationId"];
    }
    if (self.respondentId != nil) {
        [dict setValue:self.respondentId forKey:@"respondentId"];
    }
    if (self.surveyorId != nil) {
        [dict setValue:self.surveyorId forKey:@"surveyorId"];
    }
    if (self.questionnaireId != nil) {
        [dict setValue:self.questionnaireId forKey:@"questionnaireId"];
    }
    if (self.oneToOneIds != nil) {
        [dict setValue:self.oneToOneIds forKey:@"oneToOneIds"];
    }
    if (self.note != nil) {
        [dict setValue:self.note forKey:@"note"];
    }
    if (self.donotDisplayToAgent != nil) {
        [dict setValue:self.donotDisplayToAgent forKey:@"donotDisplayToAgent"];
    }
    if (self.donotMailToAgent != nil) {
        [dict setValue:self.donotMailToAgent forKey:@"donotMailToAgent"];
    }
    if (self.startDate != nil) {
        [dict setValue:self.startDate forKey:@"startDate"];
    }
    if (self.scoreVal != nil) {
        [dict setValue:self.scoreVal forKey:@"scoreVal"];
    }
    if (self.questionAnswer != nil) {
        [dict setValue:self.questionAnswer forKey:@"questionAnswer"];
    }
    if (self.mapTo != nil) {
        [dict setValue:self.mapTo forKey:@"mapTo"];
    }
    if (self.symptomText != nil) {
        [dict setValue:self.symptomText forKey:@"symptomText"];
    }
    if (self.strengthText != nil) {
        [dict setValue:self.strengthText forKey:@"strengthText"];
    }
    if (self.industryId != nil) {
        [dict setValue:self.industryId forKey:@"industryId"];
    }
    if (self.surveyEntityType != nil) {
        [dict setValue:self.surveyEntityType forKey:@"surveyEntityType"];
    }
    if (self.surveyStatus != nil) {
        [dict setValue:self.surveyStatus forKey:@"surveyStatus"];
    }
    if (self.activeStatus != nil) {
        [dict setValue:self.activeStatus forKey:@"activeStatus"];
    }
    if (self.endDate != nil) {
        [dict setValue:self.endDate forKey:@"endDate"];
    }
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *) searialize {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (self.eventId != nil) {
        [dict setValue:self.eventId forKey:@"id"];
    }
    if (self.industryId != nil) {
        [dict setValue:self.industryId forKey:@"industryId"];
    }
    if (self.activeStatus != nil) {
        [dict setValue:self.activeStatus forKey:@"activeStatus"];
    }
    if (self.createdById != nil) {
        [dict setValue:self.createdById forKey:@"createdById"];
    }
    if (self.updatedById != nil) {
        [dict setValue:self.updatedById forKey:@"updatedById"];
    }
    if (self.cByIdentity != nil) {
        [dict setValue:self.cByIdentity forKey:@"cByIdentity"];
    }
    if (self.uByIdentity != nil) {
        [dict setValue:self.uByIdentity forKey:@"uByIdentity"];
    }
    if (self.createdOn != nil) {
        [dict setValue:self.createdOn forKey:@"createdOn"];
    }
    if (self.updatedOn != nil) {
        [dict setValue:self.updatedOn forKey:@"updatedOn"];
    }
    if (self.androidVersion != nil) {
        [dict setValue:self.androidVersion forKey:@"androidVersion"];
    }
    if (self.donotMailToAgent != nil) {
        [dict setValue:self.donotMailToAgent forKey:@"donotMailToAgent"];
    }
    if (self.endDate != nil) {
        [dict setValue:self.endDate forKey:@"endDate"];
    }
    if (self.iosVersion != nil) {
        [dict setValue:self.iosVersion forKey:@"iosVersion"];
    }
    if (self.isScheduled != nil) {
        [dict setValue:self.isScheduled forKey:@"isScheduled"];
    }
    if (self.note != nil) {
        [dict setValue:self.note forKey:@"note"];
    }
    if (self.questionnaireId != nil) {
        [dict setValue:self.questionnaireId forKey:@"questionnaireId"];
    }
    if (self.questionnaireName != nil) {
        [dict setValue:self.questionnaireName forKey:@"questionnaireName"];
    }
    if (self.recommendation != nil) {
        [dict setValue:self.recommendation forKey:@"recommendation"];
    }
    if (self.questionnaireSurveyCategoryId != nil) {
        [dict setValue:self.questionnaireSurveyCategoryId forKey:@"questionnaireSurveyCategoryId"];
    }
    if (self.questionnaireSurveyCategoryName != nil) {
        [dict setValue:self.questionnaireSurveyCategoryName forKey:@"questionnaireSurveyCategoryName"];
    }
    if (self.respondentId != nil) {
        [dict setValue:self.respondentId forKey:@"respondentId"];
    }
    if (self.respondentName != nil) {
        [dict setValue:self.respondentName forKey:@"respondentName"];
    }
    if (self.scoreVal != nil) {
        [dict setValue:self.scoreVal forKey:@"scoreVal"];
    }
    if (self.startDate != nil) {
        [dict setValue:self.startDate forKey:@"startDate"];
    }
    if (self.surveyStatus != nil) {
        [dict setValue:self.surveyStatus forKey:@"surveyStatus"];
    }
    if (self.strengthText != nil) {
        [dict setValue:self.strengthText forKey:@"strengthText"];
    }
    if (self.surveyEntityType != nil) {
        [dict setValue:self.surveyEntityType forKey:@"surveyEntityType"];
    }
    if (self.surveyorId != nil) {
        [dict setValue:self.surveyorId forKey:@"surveyorId"];
    }
    if (self.surveyorName != nil) {
        [dict setValue:self.surveyorName forKey:@"surveyorName"];
    }
    if (self.surveyTakenTime != nil) {
        [dict setValue:self.surveyTakenTime forKey:@"surveyTakenTime"];
    }
    if (self.symptomText != nil) {
        [dict setValue:self.symptomText forKey:@"symptomText"];
    }
    if (self.tenantLocationId != nil) {
        [dict setValue:self.tenantLocationId forKey:@"tenantLocationId"];
    }
    if (self.calendarEventId != nil) {
        [dict setValue:self.calendarEventId forKey:@"calendarEventId"];
    }
    if (self.strengths != nil) {
        [dict setValue:self.strengths forKey:@"strengths"];
    }
    if (self.surveyStrengths != nil) {
        [dict setValue:self.surveyStrengths forKey:@"surveyStrengths"];
    }
    if (self.questionAnswer != nil) {
        [dict setValue:self.questionAnswer forKey:@"questionAnswer"];
    }
    if (self.subject != nil) {
        [dict setValue:self.subject forKey:@"subject"];
    }
    if (self.location != nil) {
        [dict setValue:self.location forKey:@"location"];
    }
    if (self.eventDescription != nil) {
        [dict setValue:self.eventDescription forKey:@"description"];
    }
    if (self.tenantId != nil) {
        [dict setValue:self.tenantId forKey:@"tenantId"];
    }
    if (self.unameTime != nil) {
        [dict setValue:self.unameTime forKey:@"unameTime"];
    }
    if (self.cnameTime != nil) {
        [dict setValue:self.cnameTime forKey:@"cnameTime"];
    }
    if (self.reviewedByWithDateTime != nil) {
        [dict setValue:self.reviewedByWithDateTime forKey:@"reviewedByWithDateTime"];
    }
    if (self.createdByWithDateTime != nil) {
        [dict setValue:self.createdByWithDateTime forKey:@"createdByWithDateTime"];
    }
    if (self.updatedByWithDateTime != nil) {
        [dict setValue:self.updatedByWithDateTime forKey:@"updatedByWithDateTime"];
    }    
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

@end
