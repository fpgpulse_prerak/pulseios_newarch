//
//  EventList.h
//  IN-Gauge
//
//  Created by Mehul Patel on 14/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "Event.h"

@interface EventList : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSArray       *eventList; //Event
@property (nonatomic, strong) NSNumber      *first;
@property (nonatomic, strong) NSNumber      *totalInList;
@property (nonatomic, strong) NSNumber      *total;
//@property (nonatomic, strong) NSNumber      *sorts;
//@property (nonatomic, strong) NSString      *updatedByWithDateTime;
//@property (nonatomic, strong) NSString      *createdByWithDateTime;
//@property (nonatomic, strong) NSString      *reviewedByWithDateTime;
//@property (nonatomic, strong) NSString      *unameTime;
//@property (nonatomic, strong) NSString      *cnameTime;

@end
