//
//  GreatJob.h
//  IN-Gauge
//
//  Created by Mehul Patel on 12/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"

@interface GreatJob : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber      *greatJobId;
@property (nonatomic, strong) NSNumber      *industryId;
//@property (nonatomic, strong) NSNumber      *activeStatus;
//@property (nonatomic, strong) NSNumber      *createdById;
//@property (nonatomic, strong) NSNumber      *updatedById;
//@property (nonatomic, strong) NSString      *cByIdentity;
//@property (nonatomic, strong) NSString      *uByIdentity;
//@property (nonatomic, strong) NSNumber      *createdOn;
//@property (nonatomic, strong) NSNumber      *updatedOn;
@property (nonatomic, strong) NSString      *name;
//@property (nonatomic, strong) NSString      *reviewedByWithDateTime;
//@property (nonatomic, strong) NSString      *updatedByWithDateTime;
//@property (nonatomic, strong) NSString      *createdByWithDateTime;
//@property (nonatomic, strong) NSString      *cnameTime;
//@property (nonatomic, strong) NSString      *unameTime;

@end
