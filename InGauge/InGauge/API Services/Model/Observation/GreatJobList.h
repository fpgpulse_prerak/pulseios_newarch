//
//  GreatJobList.h
//  IN-Gauge
//
//  Created by Mehul Patel on 12/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "GreatJob.h"

@interface GreatJobList : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSArray   *jobList; //GreatJob

- (GreatJob *) getJob:(NSNumber *)jobId;

@end
