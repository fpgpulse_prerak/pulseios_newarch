//
//  OBTypeList.h
//  IN-Gauge
//
//  Created by Mehul Patel on 10/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "ObservationType.h"

@interface OBTypeList : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSArray       *obList; //ObservationType

- (ObservationType *) getObservationType:(NSNumber *)observationTypeId;

@end
