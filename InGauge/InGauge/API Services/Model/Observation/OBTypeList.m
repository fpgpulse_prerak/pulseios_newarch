//
//  OBTypeList.m
//  IN-Gauge
//
//  Created by Mehul Patel on 10/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "OBTypeList.h"

@implementation OBTypeList

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return nil;
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    if (!isNull(array)) {
        @try {
            if ([property isEqualToString:MAIN_BODY]) {
                NSMutableArray *arr = [[NSMutableArray alloc] init];
                for (NSDictionary *details in array) {
                    ObservationType *model = [ObservationType new];
                    [HKJsonUtils updatePropertiesWithData:details forObject:model];
                    [arr addObject:model];
                }
                self.obList = [NSArray arrayWithArray:arr];
                arr = nil;
            }
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
    }
}

//---------------------------------------------------------------

- (ObservationType *) getObservationType:(NSNumber *)observationTypeId {
    @try {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.observationTypeId.intValue = %d", observationTypeId.intValue];
        NSArray *users = [self.obList filteredArrayUsingPredicate:predicate];
        if (users.count > 0) {
            return [users objectAtIndex:0];
        } else {
            return nil;
        }
    }
    @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return nil;
    }
}

//---------------------------------------------------------------

@end
