//
//  OBCategory.h
//  IN-Gauge
//
//  Created by Mehul Patel on 14/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"

@interface OBCategory : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber      *categoryId;
@property (nonatomic, strong) NSNumber      *industryId;
@property (nonatomic, strong) NSString      *activeStatus;
//@property (nonatomic, strong) NSNumber      *createdById;
//@property (nonatomic, strong) NSNumber      *updatedById;
//@property (nonatomic, strong) NSNumber      *createdOn;
//@property (nonatomic, strong) NSNumber      *updatedOn;
@property (nonatomic, strong) NSString      *name;
@property (nonatomic, strong) NSString      *surveyEntityType;
//@property (nonatomic, strong) NSString      *reviewedByWithDateTime;
//@property (nonatomic, strong) NSString      *createdByWithDateTime;
//@property (nonatomic, strong) NSString      *updatedByWithDateTime;
//@property (nonatomic, strong) NSString      *cnameTime;
//@property (nonatomic, strong) NSString      *unameTime;

@end
