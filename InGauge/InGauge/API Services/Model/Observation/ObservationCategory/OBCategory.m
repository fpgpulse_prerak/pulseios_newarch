//
//  OBCategory.m
//  IN-Gauge
//
//  Created by Mehul Patel on 14/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "OBCategory.h"

@implementation OBCategory
//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id" : @"categoryId"};
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    if (!isNull(array)) {
//        @try {
//            if ([property isEqualToString:@"questionSections"]) {
//                NSMutableArray *arr = [[NSMutableArray alloc] init];
//                for (NSDictionary *details in array) {
//                    QuestionSection *model = [QuestionSection new];
//                    [HKJsonUtils updatePropertiesWithData:details forObject:model];
//                    [arr addObject:model];
//                }
//                self.questionSections = [NSArray arrayWithArray:arr];
//                arr = nil;
//            }
//        } @catch (NSException *exception) {
//            DLog(@"Exception :%@", exception.debugDescription);
//        }
    }
}

//---------------------------------------------------------------

@end
