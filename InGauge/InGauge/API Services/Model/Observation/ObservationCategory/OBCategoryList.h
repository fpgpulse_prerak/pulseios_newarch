//
//  OBCategoryList.h
//  IN-Gauge
//
//  Created by Mehul Patel on 14/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "OBCategory.h"

@interface OBCategoryList : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSArray       *categoryList; //OBCategory

@end
