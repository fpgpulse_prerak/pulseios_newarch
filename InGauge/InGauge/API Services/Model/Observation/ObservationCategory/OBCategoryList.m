//
//  OBCategoryList.m
//  IN-Gauge
//
//  Created by Mehul Patel on 14/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "OBCategoryList.h"

@implementation OBCategoryList

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return nil;
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    if (!isNull(array)) {
        @try {
            if ([property isEqualToString:MAIN_BODY]) {
                NSMutableArray *arr = [[NSMutableArray alloc] init];
                for (NSDictionary *details in array) {
                    OBCategory *model = [OBCategory new];
                    [HKJsonUtils updatePropertiesWithData:details forObject:model];
                    [arr addObject:model];
                }
                self.categoryList = [NSArray arrayWithArray:arr];
                arr = nil;
            }
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
    }
}

//---------------------------------------------------------------

//- (GreatJob *) getJob:(NSNumber *)jobId {
//    @try {
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.greatJobId.intValue = %d", jobId.intValue];
//        NSArray *users = [self.jobList filteredArrayUsingPredicate:predicate];
//        if (users.count > 0) {
//            return [users objectAtIndex:0];
//        } else {
//            return nil;
//        }
//    }
//    @catch (NSException *exception) {
//        DLog(@"Exception :%@", exception.debugDescription);
//        return nil;
//    }
//}

//---------------------------------------------------------------

@end
