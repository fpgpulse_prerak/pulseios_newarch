//
//  ObservationType.h
//  IN-Gauge
//
//  Created by Mehul Patel on 10/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "OBCategory.h"

@interface ObservationType : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber          *observationTypeId;
@property (nonatomic, strong) NSNumber          *industryId;
//@property (nonatomic, strong) User              *createdBy;
//@property (nonatomic, strong) User              *updatedBy;
@property (nonatomic, strong) NSString          *activeStatus;
//@property (nonatomic, strong) NSDate            *createdOn;
//@property (nonatomic, strong) NSDate            *updatedOn;
//@property (nonatomic, strong) NSNumber          *indexRelatedOnly;
//@property (nonatomic, strong) NSNumber          *indexMainOnly;
@property (nonatomic, strong) OBCategory        *surveyCategory;
@property (nonatomic, strong) NSString          *surveyEntityType;
@property (nonatomic, strong) NSString          *name;
@property (nonatomic, strong) NSNumber          *isForAllTenant;
@property (nonatomic, strong) NSNumber          *isForAllLocation;
@property (nonatomic, strong) NSNumber          *oldId;

@end
