//
//  Observation.m
//  IN-Gauge
//
//  Created by Mehul Patel on 10/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "ObservationType.h"

@implementation ObservationType

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id" : @"observationTypeId"};
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void)handleDictionaryValue:(NSDictionary *)dictionary forProperty:(NSString *)property {
    
    //surveyCategory
    if (!isNull(dictionary)) {
        if ([property isEqualToString:@"surveyCategory"]) {
            OBCategory *category = [OBCategory new];
            [HKJsonUtils updatePropertiesWithData:dictionary forObject:category];
            self.surveyCategory = category;
        }
    }
}

//---------------------------------------------------------------

@end
