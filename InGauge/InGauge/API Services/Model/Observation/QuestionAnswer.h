//
//  QuestionAnswer.h
//  IN-Gauge
//
//  Created by Mehul Patel on 13/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"

@interface QuestionAnswer : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber      *questionId;
@property (nonatomic, strong) NSString      *answer;

- (NSDictionary *) searialize;
@end
