//
//  QuestionAnswer.m
//  IN-Gauge
//
//  Created by Mehul Patel on 13/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "QuestionAnswer.h"

@implementation QuestionAnswer
//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return nil;
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    if (!isNull(array)) {
    }
}

//---------------------------------------------------------------

- (NSDictionary *) searialize {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (self.questionId != nil) {
        [dict setValue:self.questionId forKey:@"questionId"];
    }
    if (self.answer != nil) {
        [dict setValue:self.answer forKey:@"answer"];
    }
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

@end
