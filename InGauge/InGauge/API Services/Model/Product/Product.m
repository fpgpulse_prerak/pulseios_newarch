//
//  Product.m
//  InGauge
//
//  Created by Mehul Patel on 31/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "Product.h"

@implementation Product
//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id" : @"productId"};
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
}

//---------------------------------------------------------------

@end
