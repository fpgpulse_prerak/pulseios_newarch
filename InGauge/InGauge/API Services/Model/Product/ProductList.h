//
//  ProductList.h
//  InGauge
//
//  Created by Mehul Patel on 31/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "Product.h"

@interface ProductList : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSArray       *productList; //Product

- (Product *) getProduct:(NSNumber *)productid;
@end
