//
//  Question.h
//  IN-Gauge
//
//  Created by Mehul Patel on 11/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"

@interface Question : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber      *questionId;
@property (nonatomic, strong) NSNumber      *industryId;
@property (nonatomic, strong) NSNumber      *activeStatus;
@property (nonatomic, strong) NSNumber      *isDeleted;
@property (nonatomic, strong) NSString      *questionText;
@property (nonatomic, strong) NSString      *surveyEntityType;
@property (nonatomic, strong) NSString      *symptom;
@property (nonatomic, strong) NSString      *questionType;
@property (nonatomic, strong) NSNumber      *weight;
@property (nonatomic, strong) NSString      *type;
@property (nonatomic, strong) NSNumber      *required;
//@property (nonatomic, strong) NSNumber      *unameTime;
//@property (nonatomic, strong) NSNumber      *cnameTime;
//@property (nonatomic, strong) NSNumber      *updatedByWithDateTime;
//@property (nonatomic, strong) NSNumber      *reviewedByWithDateTime;
//@property (nonatomic, strong) NSNumber      *createdByWithDateTime;
@end
