//
//  Question.m
//  IN-Gauge
//
//  Created by Mehul Patel on 11/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "Question.h"

@implementation Question

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id" : @"questionId"};
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    if (!isNull(array)) {
        @try {
//            if ([property isEqualToString:MAIN_BODY]) {
//                NSMutableArray *arr = [[NSMutableArray alloc] init];
//                for (NSDictionary *details in array) {
//                    ObservationType *model = [ObservationType new];
//                    [HKJsonUtils updatePropertiesWithData:details forObject:model];
//                    [arr addObject:model];
//                }
//                self.obList = [NSArray arrayWithArray:arr];
//                arr = nil;
//            }
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
    }
}

//---------------------------------------------------------------

@end
