//
//  Questionnaire.h
//  IN-Gauge
//
//  Created by Mehul Patel on 11/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "QuestionSection.h"

@interface Questionnaire : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber      *questionnaireId;
@property (nonatomic, strong) NSNumber      *industryId;
//@property (nonatomic, strong) NSNumber      *activeStatus;
//@property (nonatomic, strong) NSNumber      *createdById;
//@property (nonatomic, strong) NSNumber      *updatedById;
//@property (nonatomic, strong) NSNumber      *createdOn;
//@property (nonatomic, strong) NSNumber      *updatedOn;
//@property (nonatomic, strong) NSNumber      *isForAllTenant;
//@property (nonatomic, strong) NSNumber      *isForAllLocation;
@property (nonatomic, strong) NSString      *name;
@property (nonatomic, strong) NSNumber      *surveyCategoryId;
@property (nonatomic, strong) NSString      *surveyCategoryName;
@property (nonatomic, strong) NSString      *surveyCategoryStatus;
@property (nonatomic, strong) NSString      *surveyEntityType;
@property (nonatomic, strong) NSArray       *questionSections; //QuestionSection
//@property (nonatomic, strong) NSArray       *questionnaireTenantLocation;
//@property (nonatomic, strong) NSNumber       *createdByFirstName;
//@property (nonatomic, strong) NSNumber       *createdByLastName;
//@property (nonatomic, strong) NSNumber       *updatedByLastName;
//@property (nonatomic, strong) NSNumber       *updatedByFirstName;
//@property (nonatomic, strong) NSNumber       *unameTime;
//@property (nonatomic, strong) NSNumber       *cnameTime;
//@property (nonatomic, strong) NSNumber       *updatedByWithDateTime;
//@property (nonatomic, strong) NSNumber       *reviewedByWithDateTime;
//@property (nonatomic, strong) NSNumber       *createdByWithDateTime;

@end
