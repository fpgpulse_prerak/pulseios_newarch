//
//  Geography.h
//  IN-Gauge
//
//  Created by Mehul Patel on 11/10/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "HKJsonUtils.h"

@interface Geography : NSObject <HKJsonPropertyMappings, NSCopying>

@property (nonatomic, strong) NSNumber      *regionId;
@property (nonatomic, strong) NSNumber      *parentRegionId;
@property (nonatomic, strong) NSString      *name;
@property (nonatomic, strong) NSNumber      *activeStatus;
@property (nonatomic, strong) NSNumber      *regionTypeId;
@property (nonatomic, strong) NSString      *regionTypeName;
@property (nonatomic, strong) NSString      *regionHierarchy;
@property (nonatomic, strong) NSString      *childRegion;
@property (nonatomic, strong) NSNumber      *industryId;
@property (nonatomic, strong) NSString      *childLocation;
@property (nonatomic, strong) NSString      *childLocationName;
@property (nonatomic, strong) NSNumber      *complete;

@end
