//
//  Geography.m
//  IN-Gauge
//
//  Created by Mehul Patel on 11/10/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "Geography.h"

@implementation Geography

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id" : @"regionId"};
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

#pragma mark - Object Encoding/Decoding

//---------------------------------------------------------------

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.regionId forKey:@"regionId"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.activeStatus forKey:@"activeStatus"];
    [aCoder encodeObject:self.regionTypeId forKey:@"regionTypeId"];
    [aCoder encodeObject:self.regionTypeName forKey:@"regionTypeName"];
    [aCoder encodeObject:self.regionHierarchy forKey:@"regionHierarchy"];
    [aCoder encodeObject:self.childRegion forKey:@"childRegion"];
    [aCoder encodeObject:self.industryId forKey:@"industryId"];
    [aCoder encodeObject:self.childLocation forKey:@"childLocation"];
    [aCoder encodeObject:self.childLocationName forKey:@"childLocationName"];
    [aCoder encodeObject:self.complete forKey:@"complete"];
}

//---------------------------------------------------------------

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    self.regionId = [aDecoder decodeObjectForKey:@"regionId"];
    self.name = [aDecoder decodeObjectForKey:@"name"];
    self.activeStatus = [aDecoder decodeObjectForKey:@"activeStatus"];
    self.regionTypeId = [aDecoder decodeObjectForKey:@"regionTypeId"];
    self.regionTypeName = [aDecoder decodeObjectForKey:@"regionTypeName"];
    self.regionHierarchy = [aDecoder decodeObjectForKey:@"regionHierarchy"];
    self.childRegion = [aDecoder decodeObjectForKey:@"childRegion"];
    self.industryId = [aDecoder decodeObjectForKey:@"industryId"];
    self.childLocation = [aDecoder decodeObjectForKey:@"childLocation"];
    self.childLocationName = [aDecoder decodeObjectForKey:@"childLocationName"];
    self.complete = [aDecoder decodeObjectForKey:@"complete"];
    return self;
}

//---------------------------------------------------------------

- (id)copyWithZone:(NSZone *)zone {
    Geography *model = [Geography new];
    model.regionId = self.regionId;
    model.name = self.name;
    model.activeStatus = self.activeStatus;
    model.regionTypeId = self.regionTypeId;
    model.regionTypeName = self.regionTypeName;
    model.regionHierarchy = self.regionHierarchy;
    model.childRegion = self.childRegion;
    model.industryId = self.industryId;
    model.childLocation = self.childLocation;
    model.childLocationName = self.childLocationName;
    model.complete = self.complete;
    return model;
}

//---------------------------------------------------------------

- (instancetype)init {
    self = [super init];
    if (self) {
    }
    return self;
}

//---------------------------------------------------------------

@end
