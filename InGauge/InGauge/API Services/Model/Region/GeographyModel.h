//
//  GeographyModel.h
//  IN-Gauge
//
//  Created by Mehul Patel on 11/10/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "HKJsonUtils.h"
#import "Geography.h"

@class Geography;

@interface GeographyModel : NSObject <HKJsonPropertyMappings, NSCopying>

@property (nonatomic, strong) Geography         *region;
@property (nonatomic, strong) NSString          *regionType;
@property (nonatomic, strong) NSNumber          *level;
@property (nonatomic, strong) NSArray           *subRegions;

@end
