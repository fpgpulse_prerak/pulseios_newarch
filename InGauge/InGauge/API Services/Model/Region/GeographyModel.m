//
//  GeographyModel.m
//  IN-Gauge
//
//  Created by Mehul Patel on 11/10/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "GeographyModel.h"

@implementation GeographyModel

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return nil;
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------
//
//- (void) handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
//    if (!isNull(array)) {
//        @try {
//            NSMutableArray *list = [NSMutableArray new];
//            for (NSDictionary *details in array) {
//                Geography *model = [Geography new];
//                [HKJsonUtils updatePropertiesWithData:details forObject:model];
//                [list addObject:model];
//            }
//            
//            NSMutableArray *tempArray = self.geographyList ? [NSMutableArray arrayWithArray:self.geographyList] : [NSMutableArray new];
//            [tempArray addObject:@{property : [NSArray arrayWithArray:list]}];
//            self.geographyList = [NSArray arrayWithArray:tempArray];
//            tempArray = nil;
//            list = nil;
//        } @catch (NSException *exception) {
//            DLog(@"Exception :%@", exception.debugDescription);
//        }
//    }
//}

//---------------------------------------------------------------

#pragma mark - Object Encoding/Decoding

//---------------------------------------------------------------

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.region forKey:@"region"];
    [aCoder encodeObject:self.regionType forKey:@"regionType"];
    [aCoder encodeObject:self.level forKey:@"level"];
    [aCoder encodeObject:self.subRegions forKey:@"subRegions"];
}

//---------------------------------------------------------------

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    self.region = [aDecoder decodeObjectForKey:@"region"];
    self.regionType = [aDecoder decodeObjectForKey:@"regionType"];
    self.level = [aDecoder decodeObjectForKey:@"level"];
    self.subRegions = [aDecoder decodeObjectForKey:@"subRegions"];
    return self;
}

//---------------------------------------------------------------

- (id)copyWithZone:(NSZone *)zone {
    GeographyModel *model = [GeographyModel new];
    model.region = self.region;
    model.regionType = self.regionType;
    model.level = self.level;
    model.subRegions = self.subRegions;
    return model;
}

//---------------------------------------------------------------

- (instancetype)init {
    self = [super init];
    if (self) {
    }
    return self;
}

//---------------------------------------------------------------

@end
