//
//  Region.m
//  InGauge
//
//  Created by Mehul Patel on 31/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "Region.h"

@implementation Region

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id" : @"regionId"};
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
}

//---------------------------------------------------------------

- (void)handleDictionaryValue:(NSDictionary *)dictionary forProperty:(NSString *)property {
    //organization
    if (!isNull(dictionary)) {
        if ([property isEqualToString:@"regionType"]) {
            RegionType *regionType = [RegionType new];
            [HKJsonUtils updatePropertiesWithData:dictionary forObject:regionType];
            self.regionType = regionType;
        }
    }
}

//---------------------------------------------------------------

@end
