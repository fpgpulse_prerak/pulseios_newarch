//
//  RegionList.h
//  InGauge
//
//  Created by Mehul Patel on 31/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "Region.h"

@interface RegionList : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSArray       *regionList; //Region

@end
