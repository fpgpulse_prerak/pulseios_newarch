//
//  RegionType.h
//  InGauge
//
//  Created by Mehul Patel on 31/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"

@interface RegionType : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber          *regionTypeId;
@property (nonatomic, strong) NSNumber          *industryId;
@property (nonatomic, strong) NSString          *activeStatus;
@property (nonatomic, strong) NSNumber          *indexRelatedOnly;
@property (nonatomic, strong) NSNumber          *indexMainOnly;
@property (nonatomic, strong) NSString          *name;
@property (nonatomic, strong) NSNumber          *level;
//@property (nonatomic, strong) NSNumber          *tenant; // Custom tenant class
@property (nonatomic, strong) NSNumber          *regions;

@end
