//
//  Role.h
//  InGauge
//
//  Created by Mehul Patel on 11/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"

/**
 * 用户角色
 */
@interface Role : NSObject <HKJsonPropertyMappings, NSCopying>

@property (nonatomic, strong) NSNumber *    roleID;

@property (nonatomic, strong) NSString *    name;

@property (nonatomic, strong) NSString *    shortName;

@property (nonatomic, strong) NSString *    introduction;

@property (nonatomic, strong) NSString *    routes;

@property (nonatomic, strong) NSString *    menus;

@property (nonatomic, strong) NSString *    reports;

@property (nonatomic, strong) NSDate *      timestamp;

@property (nonatomic, strong) NSDate *      updatedTime;

@property (nonatomic, strong) NSNumber *    isSuperAdmin;

@property (nonatomic, strong) NSNumber *    weight;

@property (nonatomic, strong) NSNumber *    isPerformanceManager;

@end
