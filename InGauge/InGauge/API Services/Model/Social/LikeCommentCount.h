//
//  LikeCommentCount.h
//  
//
//  Created by Mehul Patel on 10/11/17.
//
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "HKJsonUtils.h"

@interface LikeCommentCount : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber      *comments;
@property (nonatomic, strong) NSNumber      *likes;
@property (nonatomic, strong) NSString      *sharedByComment;
@property (nonatomic, strong) NSString      *sharedByLike;

@property (nonatomic, strong) NSString      *uniqueId;
@property (nonatomic, assign) BOOL          isCallService;

@end
