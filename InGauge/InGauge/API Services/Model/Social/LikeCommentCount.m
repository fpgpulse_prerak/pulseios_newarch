//
//  LikeCommentCount.m
//  
//
//  Created by Mehul Patel on 10/11/17.
//
//

#import "LikeCommentCount.h"

@implementation LikeCommentCount

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return nil;
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

@end
