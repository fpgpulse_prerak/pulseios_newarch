//
//  TableDataDetails.h
//  IN-Gauge
//
//  Created by Mehul Patel on 19/09/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"

@interface TableDataDetails : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSDictionary      *tableHeaders;
@property (nonatomic, strong) NSArray           *tableDatas;
@property (nonatomic, strong) NSArray           *tableKeys;
@property (nonatomic, strong) NSDictionary      *socialInteractionEnabledColumns;

@end
