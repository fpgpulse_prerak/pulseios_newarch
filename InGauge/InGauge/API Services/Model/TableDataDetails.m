//
//  TableDataDetails.m
//  IN-Gauge
//
//  Created by Mehul Patel on 19/09/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "TableDataDetails.h"

@implementation TableDataDetails

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id": @"dashboardDetailId"};
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    if (!isNull(array)) {
        @try {
            if ([property isEqualToString:@"tableDatas"]) {
                NSMutableArray *arr = [[NSMutableArray alloc] init];
                for (NSDictionary *details in array) {
                    [arr addObject:details];
                }
                self.tableDatas = [NSArray arrayWithArray:arr];
                arr = nil;
            }
            
            if ([property isEqualToString:@"tableKeys"]) {
                NSMutableArray *arr = [[NSMutableArray alloc] init];
                for (NSDictionary *details in array) {
                    [arr addObject:details];
                }
                self.tableKeys = [NSArray arrayWithArray:arr];
                arr = nil;
            }
            
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
    }
}

//---------------------------------------------------------------

- (void)handleDictionaryValue:(NSDictionary *)dictionary forProperty:(NSString *)property {
    
    if (!isNull(dictionary)) {
        if ([property isEqualToString:@"tableHeaders"]) {
            self.tableHeaders = dictionary;
        }        
        if ([property isEqualToString:@"socialInteractionEnabledColumns"]) {
            self.socialInteractionEnabledColumns = dictionary;
        }
    }
}

//---------------------------------------------------------------

@end
