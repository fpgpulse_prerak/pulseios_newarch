//
//  RolesAndPermissions.h
//  IN-Gauge
//
//  Created by Mehul Patel on 12/10/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Hankai.h"

@interface RolesAndPermissions : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber                  *roleId;
@property (nonatomic, strong) NSNumber                  *activeStatus;
@property (nonatomic, strong) NSString                  *displayName;
@property (nonatomic, strong) NSString                  *name;
@property (nonatomic, strong) NSString                  *permissionType;

@end
