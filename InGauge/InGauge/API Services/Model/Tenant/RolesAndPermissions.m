//
//  RolesAndPermissions.m
//  IN-Gauge
//
//  Created by Mehul Patel on 12/10/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "RolesAndPermissions.h"

@implementation RolesAndPermissions

//---------------------------------------------------------------

#pragma mark
#pragma mark HKJsonPropertyMappings  methods

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id" : @"roleId"};
    return nil;
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return nil;
}

//---------------------------------------------------------------

- (void) encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.roleId forKey:@"roleId"];
    [encoder encodeObject:self.activeStatus forKey:@"activeStatus"];
    [encoder encodeObject:self.displayName forKey:@"displayName"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.permissionType forKey:@"permissionType"];
}

//---------------------------------------------------------------

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.roleId  = [decoder decodeObjectForKey:@"roleId"];
        self.activeStatus  = [decoder decodeObjectForKey:@"activeStatus"];
        self.displayName  = [decoder decodeObjectForKey:@"displayName"];
        self.name  = [decoder decodeObjectForKey:@"name"];
        self.permissionType  = [decoder decodeObjectForKey:@"permissionType"];
    }
    return self;
}

//---------------------------------------------------------------

@end
