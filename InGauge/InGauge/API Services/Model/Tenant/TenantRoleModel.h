//
//  TenantRoleModel.h
//  InGauge
//
//  Created by Mehul Patel on 17/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "Hankai.h"
#import "RolesAndPermissions.h"

@interface TenantRoleModel : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber          *tenantRoleId;
@property (nonatomic, strong) NSNumber          *industryId;
@property (nonatomic, strong) NSString          *activeStatus;
@property (nonatomic, strong) NSNumber          *userId;
@property (nonatomic, strong) NSString          *userName;
@property (nonatomic, strong) NSNumber          *tenantId;
@property (nonatomic, strong) NSString          *tenantName;
@property (nonatomic, strong) NSNumber          *roleId;
@property (nonatomic, strong) NSString          *roleName;
@property (nonatomic, strong) NSNumber          *roleWeight;
@property (nonatomic, strong) NSArray           *roleRolePermissions; // RolesAndPermissions
@property (nonatomic, strong) NSString          *industryName;
@property (nonatomic, strong) NSString          *industryDescription;
@property (nonatomic, strong) NSArray           *roleReports;
@property (nonatomic, strong) NSArray           *roleMetricAuditStatus;
@property (nonatomic, strong) NSArray           *roleIndustryMenus;
@property (nonatomic, strong) NSArray           *roleIndustryRoutes;
@property (nonatomic, strong) NSString          *solrTimeZone;
@property (nonatomic, strong) NSString          *updatedByWithDateTime;
@property (nonatomic, strong) NSString          *createdByWithDateTime;
@property (nonatomic, strong) NSString          *reviewedByWithDateTime;
@property (nonatomic, strong) NSString          *unameTime;
@property (nonatomic, strong) NSString          *cnameTime;


- (void) getUserRole;

@end
