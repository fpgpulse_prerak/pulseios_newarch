//
//  TenantRoleModel.m
//  InGauge
//
//  Created by Mehul Patel on 17/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "TenantRoleModel.h"


@implementation TenantRoleModel

//---------------------------------------------------------------

#pragma mark
#pragma mark HKJsonPropertyMappings  methods

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id" : @"tenantRoleId"};
    return nil;
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return nil;
}

//---------------------------------------------------------------

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    if (!isNull(array)) {
        @try {
            
            //roleRolePermissions
            if ([property isEqualToString:@"roleRolePermissions"]) {
                NSMutableArray *arr = [[NSMutableArray alloc] init];
                for (NSDictionary *details in array) {
                    RolesAndPermissions *model = [RolesAndPermissions new];
                    [HKJsonUtils updatePropertiesWithData:details forObject:model];
                    [arr addObject:model];
                }
                self.roleRolePermissions = [NSArray arrayWithArray:arr];
                arr = nil;
                
//                // SORT - Ascending order
//                NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"self.tenantName" ascending:YES];
//                NSArray *sortedArray = [self.tenantList sortedArrayUsingDescriptors:@[sortDescriptor1]];
//                self.tenantList = sortedArray;
            }
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
    }
}

//---------------------------------------------------------------

- (void) getUserRole {
    User *user = [User signedInUser];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name = %@", KEY_SYSTEM_ADMIN_ROLE];
    NSArray *array = [self.roleRolePermissions filteredArrayUsingPredicate:predicate];
    user.isSuperAdmin = array.count > 0 ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO];
    
    predicate = [NSPredicate predicateWithFormat:@"name = %@", KEY_PL_ROLE];
    array = [self.roleRolePermissions filteredArrayUsingPredicate:predicate];
    user.isPerformanceLeader = array.count > 0 ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO];
    
    predicate = [NSPredicate predicateWithFormat:@"name = %@", KEY_FRONTLINE_ASSOCIATE_ROLE];
    array = [self.roleRolePermissions filteredArrayUsingPredicate:predicate];
    user.isFrontlineAssociate = array.count > 0 ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO];
    
    // Update user details...
    [user setAsCurrentUser:YES];
}

//---------------------------------------------------------------

- (void) encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.tenantRoleId forKey:@"tenantRoleId"];
    [encoder encodeObject:self.industryId forKey:@"industryId"];
    [encoder encodeObject:self.activeStatus forKey:@"activeStatus"];
    [encoder encodeObject:self.userId forKey:@"userId"];
    [encoder encodeObject:self.userName forKey:@"userName"];
    [encoder encodeObject:self.tenantId forKey:@"tenantId"];
    [encoder encodeObject:self.tenantName forKey:@"tenantName"];
    [encoder encodeObject:self.roleId forKey:@"roleId"];
    [encoder encodeObject:self.roleName forKey:@"roleName"];
    [encoder encodeObject:self.roleWeight forKey:@"roleWeight"];
    [encoder encodeObject:self.roleRolePermissions forKey:@"roleRolePermissions"];
    [encoder encodeObject:self.industryName forKey:@"industryName"];
    [encoder encodeObject:self.industryDescription forKey:@"industryDescription"];
    [encoder encodeObject:self.roleReports forKey:@"roleReports"];
    [encoder encodeObject:self.roleMetricAuditStatus forKey:@"roleMetricAuditStatus"];
    [encoder encodeObject:self.roleIndustryMenus forKey:@"roleIndustryMenus"];
    [encoder encodeObject:self.roleIndustryRoutes forKey:@"roleIndustryRoutes"];
    [encoder encodeObject:self.solrTimeZone forKey:@"solrTimeZone"];
    [encoder encodeObject:self.updatedByWithDateTime forKey:@"updatedByWithDateTime"];
    [encoder encodeObject:self.createdByWithDateTime forKey:@"createdByWithDateTime"];
    [encoder encodeObject:self.reviewedByWithDateTime forKey:@"reviewedByWithDateTime"];
    [encoder encodeObject:self.unameTime forKey:@"unameTime"];
    [encoder encodeObject:self.cnameTime forKey:@"cnameTime"];
}

//---------------------------------------------------------------

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars        
       self.tenantRoleId  = [decoder decodeObjectForKey:@"tenantRoleId"];
       self.industryId  = [decoder decodeObjectForKey:@"industryId"];
       self.activeStatus  = [decoder decodeObjectForKey:@"activeStatus"];
       self.userId  = [decoder decodeObjectForKey:@"userId"];
       self.userName  = [decoder decodeObjectForKey:@"userName"];
       self.tenantId  = [decoder decodeObjectForKey:@"tenantId"];
       self.tenantName = [decoder decodeObjectForKey:@"tenantName"];
       self.roleId = [decoder decodeObjectForKey:@"roleId"];
       self.roleName = [decoder decodeObjectForKey:@"roleName"];
       self.roleWeight = [decoder decodeObjectForKey:@"roleWeight"];
       self.roleRolePermissions = [decoder decodeObjectForKey:@"roleRolePermissions"];
       self.industryName = [decoder decodeObjectForKey:@"industryName"];
       self.industryDescription = [decoder decodeObjectForKey:@"industryDescription"];
       self.roleReports = [decoder decodeObjectForKey:@"roleReports"];
       self.roleMetricAuditStatus = [decoder decodeObjectForKey:@"roleMetricAuditStatus"];
       self.roleIndustryMenus = [decoder decodeObjectForKey:@"roleIndustryMenus"];
       self.roleIndustryRoutes = [decoder decodeObjectForKey:@"roleIndustryRoutes"];
       self.solrTimeZone = [decoder decodeObjectForKey:@"solrTimeZone"];
       self.updatedByWithDateTime = [decoder decodeObjectForKey:@"updatedByWithDateTime"];
       self.createdByWithDateTime = [decoder decodeObjectForKey:@"createdByWithDateTime"];
       self.reviewedByWithDateTime = [decoder decodeObjectForKey:@"reviewedByWithDateTime"];
       self.unameTime = [decoder decodeObjectForKey:@"unameTime"];
       self.cnameTime = [decoder decodeObjectForKey:@"cnameTime"];
    }
    return self;
}

//---------------------------------------------------------------

@end
