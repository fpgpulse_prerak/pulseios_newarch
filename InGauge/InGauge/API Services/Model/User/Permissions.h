//
//  Permissions.h
//  IN-Gauge
//
//  Created by Mehul Patel on 31/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"

@interface Permissions : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber  *canAccessViewMail;
@property (nonatomic, strong) NSNumber  *canAccessTakeObservation;
@property (nonatomic, strong) NSNumber  *canAccessGoalProgress;
@property (nonatomic, strong) NSNumber  *canAccessEditCounterCoaching;
@property (nonatomic, strong) NSNumber  *canAccessComposeMail;
@property (nonatomic, strong) NSNumber  *canAccessEditObservation;
@property (nonatomic, strong) NSNumber  *canAccessTakeCounterCoaching;
@property (nonatomic, strong) NSNumber  *canAccessDeletMail;
@property (nonatomic, strong) NSNumber  *canAccessMailInbox;
@property (nonatomic, strong) NSNumber  *canAccessViewObservation;
@property (nonatomic, strong) NSNumber  *canAccessViewCounterCoaching;
@property (nonatomic, strong) NSNumber  *canAccessDeleteObservation;
@property (nonatomic, strong) NSNumber  *canAccessDeleteCounterCoaching;
@property (nonatomic, strong) NSNumber  *canAccessFeed;
@property (nonatomic, strong) NSNumber  *canAccessVideoLib;

@end
