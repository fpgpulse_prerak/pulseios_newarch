//
//  Permissions.m
//  IN-Gauge
//
//  Created by Mehul Patel on 31/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "Permissions.h"

@implementation Permissions

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return nil;
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------


@end
