//
//  UserList.h
//  InGauge
//
//  Created by Mehul Patel on 01/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "User.h"

@interface UserList : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSArray       *userList; //User

- (User *) getUser:(NSNumber *)userId;

@end
