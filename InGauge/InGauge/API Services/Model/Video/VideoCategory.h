//
//  VideoCategory.h
//  IN-Gauge
//
//  Created by Mehul Patel on 13/12/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"

@interface VideoCategory : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber      *categoryId;
@property (nonatomic, strong) NSNumber      *industryId;
@property (nonatomic, strong) NSString      *activeStatus;
@property (nonatomic, strong) NSDate        *createdOn;
@property (nonatomic, strong) NSDate        *updatedOn;
@property (nonatomic, strong) NSNumber      *indexRelatedOnly;
@property (nonatomic, strong) NSNumber      *indexMainOnly;
@property (nonatomic, strong) NSString      *mediaId;
@property (nonatomic, strong) NSString      *name;
@property (nonatomic, strong) NSString      *duration;
@property (nonatomic, strong) NSString      *categoryDesc;
@property (nonatomic, strong) NSString      *status;
@property (nonatomic, strong) NSString      *thumbnailURL;
@property (nonatomic, strong) NSString      *projectId;
@property (nonatomic, strong) NSString      *projectName;
@property (nonatomic, strong) NSString      *embedCode;
@property (nonatomic, strong) NSNumber      *playCount;
@property (nonatomic, strong) NSString      *section;
@property (nonatomic, strong) NSNumber      *mediaAssets;
@property (nonatomic, strong) NSNumber      *isDeleted;
@property (nonatomic, strong) NSNumber      *roleMedia;

@end
