//
//  VideoCategory.m
//  IN-Gauge
//
//  Created by Mehul Patel on 13/12/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "VideoCategory.h"

@implementation VideoCategory

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id": @"categoryId", @"description" : @"categoryDesc"};
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

@end
