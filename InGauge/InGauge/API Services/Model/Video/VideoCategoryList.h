//
//  VideoCategoryList.h
//  IN-Gauge
//
//  Created by Mehul Patel on 13/12/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "VideoCategory.h"

@interface VideoCategoryList : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSString      *categoryName;
@property (nonatomic, strong) NSArray       *categoryList; //VideoCategory

@end
