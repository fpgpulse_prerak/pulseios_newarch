//
//  VideoCategoryModel.h
//  IN-Gauge
//
//  Created by Mehul Patel on 13/12/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "VideoCategoryList.h"

@interface VideoCategoryModel : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSArray           *categoryNames; 
@property (nonatomic, strong) NSArray           *categoryItems; //VideoCategoryList
@property (nonatomic, strong) NSArray           *allVideos; //VideoCategory

@end
