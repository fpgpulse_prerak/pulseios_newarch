//
//  VideoCategoryModel.m
//  IN-Gauge
//
//  Created by Mehul Patel on 13/12/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "VideoCategoryModel.h"

@interface VideoCategoryModel ()

@end

@implementation VideoCategoryModel

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return nil;
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return SERVICE_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
//    if (!isNull(array)) {
//        @try {
//            if ([property isEqualToString:MAIN_BODY]) {
//                NSMutableArray *arr = [[NSMutableArray alloc] init];
//                for (NSDictionary *details in array) {
//                    DashboardModel *model = [DashboardModel new];
//                    [HKJsonUtils updatePropertiesWithData:details forObject:model];
//                    [arr addObject:model];
//                }
//                self.dashboardList = [NSArray arrayWithArray:arr];
//                arr = nil;
//            }
//        } @catch (NSException *exception) {
//            DLog(@"Exception :%@", exception.debugDescription);
//        }
//    }
}

//---------------------------------------------------------------

- (void)handleDictionaryValue:(NSDictionary *)dictionary forProperty:(NSString *)property {
    // User
    if (!isNull(dictionary)) {
        if ([property isEqualToString:MAIN_BODY]) {
            
            @try {
                NSMutableArray *tempAllVideos = (self.allVideos.count == 0) ? [NSMutableArray new] : [NSMutableArray arrayWithArray:self.allVideos];

                // Get all category names
                self.categoryNames = [dictionary allKeys];
                
                // Ascending order
                NSArray *sortedArray = [self.categoryNames sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
                self.categoryNames = sortedArray;
                
                NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.categoryNames];
                [tempArray insertObject:@"All Videos" atIndex:0];
                self.categoryNames = [NSArray arrayWithArray:tempArray];
                tempArray = nil;
                
                tempArray = [NSMutableArray new];
                for (NSString *key in dictionary) {
                    VideoCategoryList *object = [VideoCategoryList new];
                    object.categoryName = key;
                    [HKJsonUtils updatePropertiesWithData:dictionary forObject:object];
                    [tempArray addObject:object];
                    
//                    // Add videos to all videos
//                    [tempAllVideos addObjectsFromArray:object.categoryList];
                }
                self.categoryItems = [NSArray arrayWithArray:tempArray];
                tempArray = nil;
                
                NSPredicate *predicate = nil;
                // Get videos in order of category names
                for (NSString *categoryName in self.categoryNames) {
                    predicate = [NSPredicate predicateWithFormat:@"SELF.categoryName = %@", categoryName];
                    NSArray *filterArray = [self.categoryItems filteredArrayUsingPredicate:predicate];
                    if (filterArray.count > 0) {
                        VideoCategoryList *videoList = (VideoCategoryList *)filterArray[0];
                        // Add videos to all videos
                        [tempAllVideos addObjectsFromArray:videoList.categoryList];
                    }
                }
                // Get all videos
                self.allVideos = [NSArray arrayWithArray:tempAllVideos];
                tempAllVideos = nil;

            } @catch (NSException *exception) {
                DLog(@"Exception :%@",exception.debugDescription);
            }
        }
    }
}

//---------------------------------------------------------------

@end
