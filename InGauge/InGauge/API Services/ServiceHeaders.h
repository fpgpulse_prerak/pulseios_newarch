//
//  ServiceHeaders.h
//  IN-Gauge
//
//  Created by Mehul Patel on 29/08/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#ifndef ServiceHeaders_h
#define ServiceHeaders_h

#import "ServiceUtil.h"
#import "TenantList.h"
#import "ServicePreference.h"
#import "RegionTypeList.h"
#import "Filters.h"
#import "RegionList.h"
#import "LocationList.h"
#import "LocationGroupList.h"
#import "ProductList.h"
#import "UserList.h"
#import "IndustryList.h"
#import "FeedList.h"
#import "OBTypeList.h"
#import "Questionnaire.h"
#import "GreatJobList.h"
#import "EventList.h"
#import "OBCategoryList.h"
#import "Menu.h"
#import "Permissions.h"
#import "ChartListModel.h"
#import "TableDataDetails.h"
#import "RefineItem.h"
#import "GeographyModel.h"
#import "GeographyItems.h"
#import "FilterListDetails.h"
#import "VideoCategoryModel.h"

#endif /* ServiceHeaders_h */
