//
//  ServiceManager.h
//  InGauge
//
//  Created by Mehul Patel on 11/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceHeaders.h"

typedef void (^PostWebServiceCallFinish)(NSString *errorMessage, NSDictionary *response);
typedef void (^GetWebServiceCallFinish)(NSString *errorMessage, id response, NSNumber *status);

@interface ServiceManager : NSObject

// Common GET and POST Methods
+ (void) postWebServiceCall:action parameters:(NSDictionary *)params withBody:(NSDictionary *)bodyParams whenFinish:(PostWebServiceCallFinish)whenFinish;
+ (void) getWebServiceCall:action parameters:(NSDictionary *)params whenFinish:(GetWebServiceCallFinish)whenFinish;
+ (void) deleteWebServiceCall:action parameters:(NSDictionary *)params whenFinish:(PostWebServiceCallFinish)whenFinish;

// Login services
+ (void) callLoginWebService:(User *)user usingBlock:(void(^)(NSString *error, NSDictionary *respose))block;
// Logout
+ (void) callLogoutWebService:(void(^)(NSString *error, NSDictionary *respose))block;

// 2FA check
+ (void) callWebServiceToCheck2FA:(User *)user usingBlock:(void(^)(NSString *error, NSDictionary *respose))block;
// Verify 2FA
+ (void) callWebServiceToVerify2FACode:(NSString *)otpCode usingBlock:(void(^)(NSString *error, NSDictionary *respose))block;

// SignUp services
+ (void) callSignUpWebService:(User *)user whenFinish:(PostWebServiceCallFinish)whenFinish;

// Forgot password
+ (void) callForgotUpWebService:(NSDictionary *)user whenFinish:(PostWebServiceCallFinish)whenFinish;

+ (void) callWebServiceToGetUserId:(void(^)(BOOL isFinish))block;
// Tenant list service
+ (void) callWebServiceToGetTenantDetails:(void(^)(NSString *errorMessage))block;
// Industry list service
+ (void) callWebServiceToGetIndustryDetails;

// Chart Data
+ (void) callWebServiceToGetChartData:(NSDictionary *)details dashboardId:(NSNumber *)dashboardId usingBlock:(void(^)(NSError *error, ChartListModel *chartList))block;
// Get table data
+ (void) callWebServiceToGetTableData:(Filters *)filters reportId:(NSNumber *)reportId usingBlock:(void(^)(NSString *errorMessage, TableDataDetails *response, NSNumber *status))block;


// Filter services
+ (void) callWebServiceToGetFilterType:(NSNumber *)dashboardId usingBlock:(void(^)(FilterListDetails *filters))block;
// Region Type list
+ (void) callWebServiceToGetRegionType:(Filters *)filter usingBlock:(void(^)(RegionTypeList *regionTypeList))block;
// Region list
+ (void) callWebServiceToGetRegions:(Filters *)filter usingBlock:(void(^)(RegionList *regionList))block;
// Location list
+ (void) callWebServiceToGetLocations:(NSDictionary *)details usingBlock:(void(^)(LocationList *locationList))block;
// Tenant wise region-location list
+ (void) callWebServiceToGetTenantWiseRegionLocation:(void(^)(NSDictionary *response))block;

// Location group list
+ (void) callWebServiceToGetLocationGroup:(NSNumber *)locationId usingBlock:(void(^)(LocationGroupList *locationGroupList))block;
// Product list
+ (void) callWebServiceToGetProducts:(Filters *)filters usingBlock:(void(^)(ProductList *productList))block;
// User list
+ (void) callWebServiceToGetUsers:(Filters *)filter withDetails:(NSDictionary *)details usingBlock:(void(^)(UserList *userList))block;

+ (void) callWebServiceToGetAvailabelUsers:(Filters *)filter usingBlock:(void(^)(UserList *userList))block;
// Get all users of tenant
+ (void) getAllUsersOfTenant:(void(^)(UserList *userList))block;

// Performance Leader list
+ (void) callWebServiceToGetPerformanceLeader:(NSDictionary *)details usingBlock:(void(^)(UserList *userList))block;
// Agent list
+ (void) callWebServiceToGetAgents:(NSDictionary *)details usingBlock:(void(^)(UserList *userList))block;
// Observation Type list
+ (void) callWebServiceToGetObservationType:(NSDictionary *)details usingBlock:(void(^)(OBTypeList *obTypeList))block;
// Get questionnaire
+ (void) callWebServiceToGetQuestionnaire:(Filters *)filter usingBlock:(void(^)(Questionnaire *questionnaire))block;
// Get Great Job on list
+ (void) callWebServiceToGetGreatJobOn:(void(^)(GreatJobList *jobList))block;
// Save observation event, It can be New or Edited
+ (void) callWebServiceSaveEvent:(Event *)event usingBlock:(void(^)(NSString *errorMessage, NSDictionary *details))block;
// Get Observation categories
+ (void) callWebServiceToGetObservationCategories:(Filters *)filter usingBlock:(void(^)(OBCategoryList *categoryList))block;
// Get List of observations
+ (void) callWebServiceToGetListOfObservation:(NSDictionary *)params usingBlock:(void(^)(EventList *eventList))block;
// Get single event/observation
+ (void) callWebServiceToGetSingleObservation:(NSNumber *)eventId usingBlock:(void(^)(Event *event))block;
// Delete single event/observation
+ (void) callWebServiceToDeleteSingleObservation:(NSNumber *)eventId usingBlock:(void(^)(NSDictionary *response))block;


// Call service to get mobile keys..
+ (void)getServerMappingKeyList:(id)params usingBlock:(void(^)(NSDictionary *keys))block;
// Get method
//+ (void) getWebServiceCallMethod:action parameters:(NSDictionary *)params whenFinish:(PostWebServiceCallFinish)whenFinish;

+ (void) postWebServiceCallwithParam:action parameters:(NSDictionary *)params whenFinish:(PostWebServiceCallFinish)whenFinish;

// Get menus
+ (void) getAvailableMenu:(void(^)(NSArray *menus))block;

// Get roles and permissions
+ (void)getRolesAndPermissionsList:(id)params usingBlock:(void(^)(Permissions *details))block;

// PUT
+ (void) putWebServiceCallwithParam:action parameters:(NSDictionary *)params whenFinish:(PostWebServiceCallFinish)whenFinish;


// Download attachment
+ (void) postDownloadWebServiceCallwithURL:actionAttID parameters:(NSDictionary *)params whenFinish:(PostWebServiceCallFinish)whenFinish;

// Upload attachment
+ (void) postUploadWebServiceCallwithParam:action parameters:(NSDictionary *)params whenFinish:(PostWebServiceCallFinish)whenFinish;

// Share feeds
+ (void) callWebServiceToShareFeeds:(NSString *)action params:(NSDictionary *)params usingBlock:(void(^)(NSDictionary *response, NSError *error))block;

// Post Device Id
+ (void) postDeviceUDIDtoServer:(NSDictionary *)details;

// Like / Comment
+ (void) callWebServiceToLikeCommentReport:(NSString *)action params:(NSDictionary *)params usingBlock:(void(^)(NSDictionary *response, NSError *error))block;

// Video
//1. Get video category list
+ (void) callWebServiceToGetVideoCategoryList:(NSDictionary *)params usingBlock:(void(^)(VideoCategoryModel *model, NSString *error))block;


@end
