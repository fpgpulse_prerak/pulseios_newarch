//
//  ServiceManager.m
//  InGauge
//
//  Created by Mehul Patel on 11/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "ServiceManager.h"

static NSString *keyMappingFileName = @"FPG_KEY_MAP.json";

@implementation ServiceManager

//---------------------------------------------------------------

#pragma mark
#pragma mark COMMON methods

//---------------------------------------------------------------

+ (void) postWebServiceCall:action parameters:(NSDictionary *)params withBody:(NSDictionary *)bodyParams whenFinish:(PostWebServiceCallFinish)whenFinish {
    
    // Create manager
    AFHTTPSessionManager *manager = nil;
    // If action is login action then don't add access-token in request manager
    if ([action isEqualToString:LOGIN_ACTION]) {
        manager = [ServiceUtil requestManagerWithAccessToken:NO];
        
        // Set device id
        NSString *deviceToken = @"";
        if ([Helper getCurrentDeviceId]) {
            deviceToken = [Helper getCurrentDeviceId];
        }
        [manager.requestSerializer setValue:deviceToken forHTTPHeaderField:@"deviceId"];

    } else {
        manager = [ServiceUtil requestManagerWithAccessToken:YES];
    }
    
    NSString *urlString = [ServiceUtil addHeaderParameters:action withParams:params];
    DLog(@"urlString :%@", urlString);
    NSString *encodedUrlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [manager POST:encodedUrlString parameters:bodyParams progress:^(NSProgress * _Nonnull downloadProgress) {
        // Check progress here...
    }  success:^(NSURLSessionTask *task, id responseObject) {
        whenFinish(nil, responseObject);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSString *errorMessage = [ServiceUtil translateError:error];
        
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [operation response];
        NSInteger statusCode = [response statusCode];
        DLog(@"RESPONSE CODE: %d", (int)statusCode);
        
        if (error.code == NSURLErrorTimedOut) {
            NSString *errorMessage = @"Server is down for maintenance please try again later";
            whenFinish(errorMessage, nil);
        }
        
        @try {
            NSString* errResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
            NSDictionary *errordetails = [Helper getObjectFromJSONString:errResponse];
            errorMessage = errordetails[@"error"];
            DLog(@"JSON: %@", errordetails[@"error"]);
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
        
        whenFinish(errorMessage, nil);
    }];
}

//---------------------------------------------------------------
+ (void) postUploadWebServiceCallwithParam:action parameters:(NSDictionary *)params whenFinish:(PostWebServiceCallFinish)whenFinish {
    // Check for internet
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        whenFinish(nil, nil);
    }
    
    // Create manager
    AFHTTPSessionManager *manager = nil;
    // If action is login action then don't add access-token in request manager
    if ([action isEqualToString:LOGIN_ACTION]) {
        manager = [ServiceUtil requestManagerWithAccessToken:NO];
    } else {
        manager = [ServiceUtil requestManagerWithAccessToken:YES];
    }
    
    [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    
    NSString *urlPrefix = nil;
    if (IS_SECURE_HOST) {
        urlPrefix = [NSString stringWithFormat:@"https://%@/%@", BASE_URL, action];
    } else {
        urlPrefix = [NSString stringWithFormat:@"http://%@/%@", BASE_URL, action];
    }
    DLog(@"urlString :%@", urlPrefix);

    NSError *error = nil;
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:urlPrefix parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    NSData *data = [NSData dataWithContentsOfFile:[params valueForKey:@"url"]];
        [formData appendPartWithFileData:data name:@"file" fileName:[params valueForKey:@"fileName"] mimeType:@"image/png"];
    } error:&error];
    
    // ~~~~~ Set headers ~~~~~
    User *user = [User signedInUser];
    if (!isEmptyString(user.accessToken)) {
        NSString *authorisationBearer = [NSString stringWithFormat:@"bearer %@", user.accessToken];
        DLog(@"\n\n\n\n ˜˜˜˜˜˜˜˜ACCESS TOKEN :%@", authorisationBearer);
        [request setValue:authorisationBearer forHTTPHeaderField:KEY_ACCESS_TOKEN];
    }
    
    // Set IndustryId
    NSString *industryId = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_INDUSTRY_ID];
    if (industryId) {
        [request setValue:industryId forHTTPHeaderField:KEY_INDUSTRY_ID];
    } else {
        [request setValue:KEY_INDUSTRY_ID_VALUE forHTTPHeaderField:KEY_INDUSTRY_ID];
    }
    
    // Set Platform
    [request setValue:KEY_PLATFORM_VALUE forHTTPHeaderField:KEY_PLATFORM];
    
    // Set mobility call
    [request setValue:KEY_ISMOBILE_VALUE forHTTPHeaderField:KEY_ISMOBILE];
    
    // Set time zone
    // Get device local time zone
    [manager.requestSerializer setValue:[Helper getLocalTimeZone] forHTTPHeaderField:KEY_TIME_ZONE];
    [Helper getLocalTimeZone];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager uploadTaskWithStreamedRequest:request progress:^(NSProgress * _Nonnull uploadProgress) {
    } completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if (error) {

            NSString *errorMessage = [ServiceUtil translateError:error];
            NSHTTPURLResponse *response = (NSHTTPURLResponse *) response;

            if (error.code == NSURLErrorTimedOut) {
              NSString *errorMessage = @"Server is down for maintenance please try again later";
              whenFinish(errorMessage, nil);
            }

            @try {
                NSString* errResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
                NSDictionary *errordetails = [Helper getObjectFromJSONString:errResponse];
                errorMessage = errordetails[@"error"];
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
            }
            whenFinish(errorMessage, nil);
        } else {
            DLog(@"%@ %@", response, responseObject);
            whenFinish(nil, responseObject);
        }
    }];
    [uploadTask resume];
}

//---------------------------------------------------------------

+ (void) postDownloadWebServiceCallwithURL:actionAttID parameters:(NSDictionary *)params whenFinish:(PostWebServiceCallFinish)whenFinish {
    
    // Create manager
    AFHTTPSessionManager *manager = [ServiceUtil requestManagerWithAccessToken:YES];
    
    NSString *urlPrefix = nil;
    if (IS_SECURE_HOST) {
        urlPrefix = [NSString stringWithFormat:@"https://%@/%@%@", BASE_URL, DOWNLOAD_ATTACHMENT, actionAttID];
    } else {
        urlPrefix = [NSString stringWithFormat:@"http://%@/%@%@", BASE_URL, DOWNLOAD_ATTACHMENT, actionAttID];
    }
    DLog(@"urlString :%@", urlPrefix);

    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlPrefix]];
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        DLog(@"File downloaded to: %@", filePath);
        NSString *errorMessage = [ServiceUtil translateError:error];
        NSString *url = [[filePath absoluteString] stringByReplacingOccurrencesOfString:@"file://" withString:@""];
        whenFinish(errorMessage, @{@"UploadedFilePath":url});
    }];
    [downloadTask resume];
}

//---------------------------------------------------------------

+ (void) postWebServiceCallwithParam:action parameters:(NSDictionary *)params whenFinish:(PostWebServiceCallFinish)whenFinish {
    
    // Create manager
    AFHTTPSessionManager *manager = nil;
    // If action is login action then don't add access-token in request manager
    if ([action isEqualToString:LOGIN_ACTION]) {
        manager = [ServiceUtil requestManagerWithAccessToken:NO];
    } else {
        manager = [ServiceUtil requestManagerWithAccessToken:YES];
    }
    
    NSString *urlPrefix = nil;
    if (IS_SECURE_HOST) {
        urlPrefix = [NSString stringWithFormat:@"https://%@/%@", BASE_URL, action];
    } else {
        urlPrefix = [NSString stringWithFormat:@"http://%@/%@", BASE_URL, action];
    }
    DLog(@"urlString :%@", urlPrefix);
    
    [manager POST:urlPrefix parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
        // Check progress here...
    }  success:^(NSURLSessionTask *task, id responseObject) {
        whenFinish(nil, responseObject);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSString *errorMessage = [ServiceUtil translateError:error];
        if (error.code == NSURLErrorTimedOut) {
            NSString *errorMessage = @"Server is down for maintenance please try again later";
            whenFinish(errorMessage, nil);
        }
        @try {
            NSString* errResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
            NSDictionary *errordetails = [Helper getObjectFromJSONString:errResponse];
            errorMessage = errordetails[@"error"];
            DLog(@"JSON: %@", errordetails[@"error"]);
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
        whenFinish([ServiceUtil translateErrorCode:operation], nil);
    }];
}

//---------------------------------------------------------------

+ (void) putWebServiceCallwithParam:action parameters:(NSDictionary *)params whenFinish:(PostWebServiceCallFinish)whenFinish {
    
    // Create manager
    AFHTTPSessionManager *manager = nil;
    // If action is login action then don't add access-token in request manager
    if ([action isEqualToString:LOGIN_ACTION]) {
        manager = [ServiceUtil requestManagerWithAccessToken:NO];
    } else {
        manager = [ServiceUtil requestManagerWithAccessToken:YES];
    }
    
    NSString *urlPrefix = nil;
    if (IS_SECURE_HOST) {
        urlPrefix = [NSString stringWithFormat:@"https://%@/%@", BASE_URL, action];
    } else {
        urlPrefix = [NSString stringWithFormat:@"http://%@/%@", BASE_URL, action];
    }
    DLog(@"urlString :%@", urlPrefix);
    
    [manager PUT:urlPrefix parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        whenFinish(nil, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSString *errorMessage = [ServiceUtil translateError:error];
        if (error.code == NSURLErrorTimedOut) {
            NSString *errorMessage = @"Server is down for maintenance please try again later";
            whenFinish(errorMessage, nil);
        }
        @try {
            NSString* errResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
            NSDictionary *errordetails = [Helper getObjectFromJSONString:errResponse];
            errorMessage = errordetails[@"error"];
            DLog(@"JSON: %@", errordetails[@"error"]);
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
        whenFinish([ServiceUtil translateErrorCode:task], nil);
    }];
}

//---------------------------------------------------------------

+ (void) getWebServiceCall:action parameters:(NSDictionary *)params whenFinish:(GetWebServiceCallFinish)whenFinish {
    // Common manager
    AFHTTPSessionManager *manager = [ServiceUtil requestManagerWithAccessToken:YES];
    NSString *urlString = [ServiceUtil addHeaderParameters:action withParams:params];
    DLog(@"urlString :%@", urlString);
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        // Check progress here...
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        // Hide progress
        runOnMainQueueWithoutDeadlocking(^{
            [MBProgressHUD hideHUDForView:App_Delegate.topMostController.view animated:YES];
        });

        @try {
            NSString *errorString = nil;
            NSNumber *statusCode = [responseObject objectForKey:@"statusCode"];
            if (statusCode.intValue == 200) {
                whenFinish(errorString, responseObject, statusCode);
            } else {
                if ([responseObject isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *details = (NSDictionary *)responseObject;
                    errorString = [[details allKeys] containsObject:@"message"] ? [details objectForKey:@"message"] : nil;
                }
                whenFinish(errorString, nil, statusCode);
            }
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
            whenFinish(nil, responseObject, nil);
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        // Hide progress
        runOnMainQueueWithoutDeadlocking(^{
            [MBProgressHUD hideHUDForView:App_Delegate.topMostController.view animated:YES];
        });
        NSString *errorMessage = [ServiceUtil translateErrorCode:operation];
        NSString* errResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        DLog(@"JSON: %@", errResponse);
        whenFinish(errorMessage, nil, nil);
    }];
}

//---------------------------------------------------------------

+ (void) deleteWebServiceCall:action parameters:(NSDictionary *)params whenFinish:(PostWebServiceCallFinish)whenFinish {
    
    NSString *urlString = [ServiceUtil addHeaderParameters:action withParams:params];
    
    //Convert to NSData formate with utf8 encoding
    NSData *postData = [urlString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"DELETE"];
    
    //Pass some default parameter(like content-type etc.)
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // Set Content-Type
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // Set Authorization:bearer [And access token]
    User *user = [User signedInUser];
    if (!isEmptyString(user.accessToken)) {
        NSString *authorisationBearer = [NSString stringWithFormat:@"bearer %@", user.accessToken];
        DLog(@"\n\n\n\n ˜˜˜˜˜˜˜˜ACCESS TOKEN :%@", authorisationBearer);
        [request setValue:authorisationBearer forHTTPHeaderField:KEY_ACCESS_TOKEN];
    }
    
    // Set IndustryId
    NSString *industryId = [ServicePreference objectForKey:KEY_INDUSTRY_ID];
    if (industryId) {
        [request setValue:industryId forHTTPHeaderField:KEY_INDUSTRY_ID];
    } else {
        [request setValue:KEY_INDUSTRY_ID_VALUE forHTTPHeaderField:KEY_INDUSTRY_ID];
    }
    
    // Set Platform
    [request setValue:KEY_PLATFORM_VALUE forHTTPHeaderField:KEY_PLATFORM];
    
    // Set mobility call
    [request setValue:KEY_ISMOBILE_VALUE forHTTPHeaderField:KEY_ISMOBILE];
    
    // Set Service timeout interval
    [request setTimeoutInterval:FPServiceDefaultTimeout];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        NSDictionary *responseDetails = nil;
        if (data.length > 0 && connectionError == nil) {
            responseDetails = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
        }
        whenFinish(connectionError.debugDescription, responseDetails);
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Login methods

//---------------------------------------------------------------

+ (void) callLoginWebService:(User *)user usingBlock:(void(^)(NSString *error, NSDictionary *respose))block {
    // Check for internet
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil, nil);
    }
    
    DLog(@"\n\n\n\n==========>>> Login - Service Call Begin <<<==========\n\n\n\n");
    NSString *urlString = LOGIN_ACTION;
    NSDictionary *details = [user userParams];
    [ServiceManager postWebServiceCall:urlString parameters:nil withBody:details whenFinish:^(NSString *errorMessage, NSDictionary *response) {
        DLog(@"\n\n\n\n==========>>> Login - Service Call finish <<<==========\n\n\n\n");
        if (errorMessage) {
            [Helper showAlertWithTitle:@"" message:@"Invalid username or password" forController:App_Delegate.topMostController];
            [ServicePreference removeAllPreferences];
        } else {
            @try {
                if ([[response allKeys] containsObject:@"statusCode"]) {
                    NSNumber *statusCode = [response objectForKey:@"statusCode"];

                    if (statusCode.intValue == 200) {
                        NSDictionary *data = [response objectForKey:MAIN_BODY];
                        if (data != nil) {
                            // Save user for next login
                            // Set access token
                            user.accessToken = [data valueForKey:@"access_token"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.accessToken forKey:KEY_ACCESS_TOKEN];
                            [user setAsCurrentUser:YES];
                            
                            // Call service to get User id
                            [self callWebServiceToGetUserId:^(BOOL isFinish) {
                                block(errorMessage, response);
                            }];
                        } else {
                            block(errorMessage, response);
                        }
                    } else {
                        [Helper showAlertWithTitle:@"" message:@"Invalid username or password" forController:App_Delegate.topMostController];
                        [ServicePreference removeAllPreferences];
                        block(errorMessage, response);
                    }
                } else {
                    [Helper showAlertWithTitle:@"" message:@"Server is down for maintenance please try again later" forController:App_Delegate.topMostController];
                    block(errorMessage, response);
                }
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
                block(errorMessage, response);
            }
        }
    }];
}

//---------------------------------------------------------------

+ (void) callLogoutWebService:(void(^)(NSString *error, NSDictionary *respose))block {
    // Check for internet
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil, nil);
    }
    
    NSDictionary *parmas = @{
                                @"userId" : [User signedInUser].userID,
                                @"deviceId" : [Helper getCurrentDeviceId],
                                @"platform" : [NSNumber numberWithInt:0]
                             };
    
    NSString *urlString = LOGOUT_ACTION;
    
    DLog(@"\n\n\n\n==========>>> Logout - Service Call begin <<<==========\n\n\n\n");
    
    [ServiceManager getWebServiceCall:urlString parameters:parmas whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Logout - Service Call finish <<<==========\n\n\n\n");
        
        if (errorMessage) {
        } else {
            @try {
                NSDictionary *data = response;
                if (data != nil) {
                }
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
            }
        }
        block(errorMessage, response);
    }];
}

//---------------------------------------------------------------

+ (void) callWebServiceToCheck2FA:(User *)user usingBlock:(void(^)(NSString *error, NSDictionary *respose))block {
    // Check for internet
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil, nil);
        return;
    }

    NSDictionary *params = @{@"userId" : user.userID, @"isAuthyCheck" : @"true"};
    
    // Common manager
    AFHTTPSessionManager *manager = [ServiceUtil getSessionManager];
    NSString *urlString = [ServiceUtil addHeaderParameters:USER_CHECK_2FA withParams:params];

    DLog(@"\n\n\n\n==========>>> 2FA - Service Call begin <<<==========\n\n\n\n");
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        DLog(@"\n\n\n\n==========>>> 2FA - Service Call finish <<<==========\n\n\n\n");
        @try {
            NSNumber *statusCode = [responseObject objectForKey:@"statusCode"];
            if (statusCode.intValue == 200) {
                block(nil, responseObject);
            } else {
                block(nil, nil);
            }
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
            block(nil, responseObject);
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        if (error.code == NSURLErrorTimedOut) {
            NSString *errorMessage = @"Server is down for maintenance please try again later";
            block(errorMessage, nil);
        }
        
        NSString *errorMessage = [ServiceUtil translateErrorCode:operation];
        NSString* errResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        DLog(@"JSON: %@", errResponse);
        block(errorMessage, nil);
    }];
}

//---------------------------------------------------------------

+ (void) callWebServiceToVerify2FACode:(NSString *)otpCode usingBlock:(void(^)(NSString *error, NSDictionary *respose))block {
    // Check for internet
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil, nil);
        return;
    }
    
    // Show loading indicator
    [MBProgressHUD showHUDAddedTo:App_Delegate.topMostController.view animated:YES];
    NSDictionary *params = @{@"userId" : App_Delegate.appfilter.loginUserId};
    
    // Common manager
    AFHTTPSessionManager *manager = [ServiceUtil getSessionManager];
    // Pass OTP code in header
    DLog(@"otp code :%@", otpCode);
    [manager.requestSerializer setValue:otpCode forHTTPHeaderField:@"secureToken"];

    NSString *urlString = [ServiceUtil addHeaderParameters:USER_VERIFY_2FA withParams:params];
    DLog(@"\n\n\n\n==========>>> Verify 2FA - Service Call begin <<<==========\n\n\n\n");
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        runOnMainQueueWithoutDeadlocking(^{
            [MBProgressHUD hideHUDForView:App_Delegate.topMostController.view animated:YES];
        });
        DLog(@"\n\n\n\n==========>>> Verify 2FA - Service Call finish <<<==========\n\n\n\n");
        @try {
            NSNumber *statusCode = [responseObject objectForKey:@"statusCode"];
            if (statusCode.intValue == 200) {
                block(nil, responseObject);
            } else {
                block(nil, nil);
            }
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
            block(nil, responseObject);
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        // Hide progress
        runOnMainQueueWithoutDeadlocking(^{
            [MBProgressHUD hideHUDForView:App_Delegate.topMostController.view animated:YES];
        });
        NSString *errorMessage = @"Invalid code";
        
        if (error.code == NSURLErrorTimedOut) {
            errorMessage = @"Server is down for maintenance please try again later";
            block(errorMessage, nil);
        }
        
        errorMessage = [ServiceUtil translateErrorCode:operation];
        NSString* errResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        DLog(@"JSON: %@", errResponse);
        block(errorMessage, nil);
    }];
}

//---------------------------------------------------------------

+ (void) callSignUpWebService:(User *)user whenFinish:(PostWebServiceCallFinish)whenFinish {
    // Check for internet
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        whenFinish(nil, nil);
        return;
    }
    
    // Show loading indicator
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:App_Delegate.topMostController.view animated:YES];
    hud.label.text = @"Loading...";
    
    NSArray *arrKeylist = @[
        @"countryCode",
        @"desiredLocation",
        @"email",
        @"firstName",
        @"gender",
        @"jobTitle",
        @"lastName",
        @"number",
        @"password",
        @"phoneNumber"];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    for (int i = 0; i < arrKeylist.count; i++) {
        if ([user valueForKey:[arrKeylist objectAtIndex:i]]) {
            [dict setObject:[user valueForKey:[arrKeylist objectAtIndex:i]] forKey:[arrKeylist objectAtIndex:i]];
        }
    }
    
    NSDictionary *requestDict = [NSDictionary dictionaryWithDictionary:dict];    
    NSString *urlString = USER_SIGNUP;
    DLog(@"\n\n\n\n==========>>> SignUP - Service Call begin <<<==========\n\n\n\n");

    [ServiceManager postWebServiceCall:urlString parameters:nil withBody:requestDict whenFinish:^(NSString *errorMessage, NSDictionary *response) {
        runOnMainQueueWithoutDeadlocking(^{
            [MBProgressHUD hideHUDForView:App_Delegate.topMostController.view animated:YES];
        });
        DLog(@"\n\n\n\n==========>>> SignUP - Service Call finish <<<==========\n\n\n\n");
        if (errorMessage) {
            [Helper showAlertWithTitle:@"" message:errorMessage forController:App_Delegate.topMostController];
        } else {
            @try {
                    whenFinish(errorMessage, response);
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
            }
        }
    }];
}
//---------------------------------------------------------------

+ (void) callForgotUpWebService:(NSDictionary *)user whenFinish:(PostWebServiceCallFinish)whenFinish {
    
    // Check for internet
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        whenFinish(nil, nil);
        return;
    }
    
    // Show loading indicator
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:App_Delegate.topMostController.view animated:YES];
    hud.label.text = @"Loading...";
    DLog(@"\n\n\n\n==========>>> Forgot password - Service Call begin <<<==========\n\n\n\n");

    [ServiceManager postWebServiceCall:USER_FORGOTPASSWORD parameters:user withBody:nil whenFinish:^(NSString *errorMessage, NSDictionary *response) {
        runOnMainQueueWithoutDeadlocking(^{
            [MBProgressHUD hideHUDForView:App_Delegate.topMostController.view animated:YES];
        });
        DLog(@"\n\n\n\n==========>>> Forgot password - Service Call finish <<<==========\n\n\n\n");

        if (errorMessage) {
            [Helper showAlertWithTitle:@"" message:errorMessage forController:App_Delegate.topMostController];
        } else {
            
            @try {
                whenFinish(errorMessage, response);
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
                whenFinish(errorMessage, nil);
            }
        }
    }];
}

//---------------------------------------------------------------

+ (void) callWebServiceToGetUserId:(void(^)(BOOL isFinish))block {
    // Check for internet
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(YES);
    }
    
    NSString *urlString = GET_USERID_ACTION;
    DLog(@"\n\n\n\n==========>>> Get UserId - Service Call begin <<<==========\n\n\n\n");
    [ServiceManager getWebServiceCall:urlString parameters:nil whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get UserId - Service Call finish <<<==========\n\n\n\n");
        if (errorMessage) {
            // Show any relevant message
            block(YES);
        } else {
            @try {
                if (response!= nil) {
                    NSDictionary *details = [response objectForKey:MAIN_BODY];
                    User *user = [User new];
                    [HKJsonUtils updatePropertiesWithData:details forObject:user];
                    
                    // Get access token from user defaults
                    NSString *accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_ACCESS_TOKEN];
                    user.accessToken = accessToken;
                    [user setAsCurrentUser:YES];
                    
                    // Update app filters
                    App_Delegate.appfilter.loginUserId = [NSNumber numberWithInteger:user.userID.integerValue];
                    App_Delegate.appfilter.industryId = user.defaultIndustry;
                    [App_Delegate saveFilters];
                    
                    // NOTE: - SET INDUSTRY ID before calling any service
                    [ServicePreference setObject:user.defaultIndustry.stringValue forKey:KEY_INDUSTRY_ID];

                    // Call service to get all keys
                    [ServiceManager getServerMappingKeyList:nil usingBlock:^(NSDictionary *keys) {}];
                    
                    // Call webservice to get industry details
                    [self callWebServiceToGetIndustryDetails];

                    // Check that this user required 2FA?
                    [self callWebServiceToCheck2FA:user usingBlock:^(NSString *error, NSDictionary *respose) {
                        BOOL is2FARequired = NO;
                        if (is2FARequired) {
                            // Go to OTP verification screen
                            [App_Delegate showVerificationScreen];
                            block(YES);
                        } else {
                            [self callWebServiceToGetTenantDetails:^(NSString *errorMessage) {
                                if (errorMessage == nil) {
                                    // Good to GO....
                                    // Move to next controller
                                    [Helper hideLoading:App_Delegate.globalHud];
                                    [App_Delegate changeRootViewControllerTo:@"MainDashboard"];
                                }
                                block(YES);
                            }];
                        }
                    }];
                }
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
            }
        }
    }];
}

//---------------------------------------------------------------

+ (void) callWebServiceToGetTenantDetails:(void(^)(NSString *errorMessage))block {
    // Check for internet
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil);
    }
        
    NSString *urlString = GET_TENANT_ACTION;
    User *signInUser = [User signedInUser];
    App_Delegate.appfilter.loginUserId = [NSNumber numberWithInteger:signInUser.userID.integerValue];
    
    NSDictionary *params = nil;
    if (signInUser.userID) {
        params = @{@"userID" : signInUser.userID};
    }
    DLog(@"\n\n\n\n==========>>> Get Tenant details - Service Call begin <<<==========\n\n\n\n");
    
    [ServiceManager getWebServiceCall:urlString parameters:params whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get Tenant details - Service Call finish <<<==========\n\n\n\n");
        
        if (errorMessage) {
            [Helper showAlertWithTitle:@"" message:errorMessage forController:App_Delegate.topMostController];
            block(errorMessage);
        } else {
            @try {
                if (response != nil) {
                    TenantList *tenantList = [TenantList new];
                    [HKJsonUtils updatePropertiesWithData:response forObject:tenantList];
                    
                    if (tenantList.tenantList.count > 0) {
                        // Store tenant role to user defaults
                        [ServicePreference saveCustomObject:tenantList key:KEY_DEFAULT_TENANT_ROLE_MODEL];

                        // Set default tenant
                        if (!App_Delegate.appfilter.tenantId) {
                            TenantRoleModel *tenant = [tenantList.tenantList objectAtIndex:0];
                            App_Delegate.appfilter.tenantId = tenant.tenantId;
                            App_Delegate.appfilter.userRole = tenant.roleName;
                            App_Delegate.appfilter.tenantName = tenant.tenantName;
                            App_Delegate.appfilter.userRoleId = tenant.roleId;
                            
                            // Get user role
                            [tenant getUserRole];

                            // Update app filters
                            [App_Delegate saveFilters];
                        }
                    } else {
                        [ServicePreference saveCustomObject:nil key:KEY_DEFAULT_TENANT_ROLE_MODEL];
                        App_Delegate.appfilter.tenantId = nil;
                        App_Delegate.appfilter.userRole = nil;
                        
                        // Update app filters
                        [App_Delegate saveFilters];
                    }
                    
                    // Call service to get roles and permissions
                    [ServiceManager getRolesAndPermissionsList:nil usingBlock:^(Permissions *details) {
                        App_Delegate.userPermissions = details;
                        
                        // Call service to get tenant based region and location
                        [self callWebServiceToGetTenantWiseRegionLocation:^(NSDictionary *response) {
                            App_Delegate.appfilter.geographyResponse = response;
                            App_Delegate.appfilter.geoItems = nil;
                            // re-organize regions
                            [self reOrganizeRegionsAndLocations];
                            block(errorMessage);
                        }];
                    }];
                }
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
                block(errorMessage);
            }
        }
    }];
}

//---------------------------------------------------------------

+ (void) reOrganizeRegionsAndLocations {
    NSMutableArray *tempArray = [NSMutableArray new];
    
    if ([App_Delegate.appfilter.geographyResponse.allKeys containsObject:@"-1"]) {
        NSArray *countries = [App_Delegate.appfilter.geographyResponse objectForKey:@"-1"];
        for (NSDictionary *details in countries) {
            [tempArray addObject:[self createRegionItem:details withLevel:0]];
        }
    }
    App_Delegate.appfilter.geographyList = [NSArray arrayWithArray:tempArray];
    tempArray = nil;
}

//---------------------------------------------------------------
// Its recursion!!!!
+ (GeographyModel *) createRegionItem:(NSDictionary *)region withLevel:(NSInteger)level {
    
    @try {
        
        // Now it's time to spin your brain!
        // GeographyModel defines regions, which can be country, region, states, district, area
        // it contains sub-regions -> and that is again GeographyModel, it manage hierarchy of multiple regions!
        // Multiple regions are manage by levels - which start from 0, each additional level defines subregions
        GeographyModel *geoModel = [GeographyModel new];
        Geography *model = [Geography new];
        [HKJsonUtils updatePropertiesWithData:region forObject:model];
        geoModel.region = model;
        geoModel.regionType = model.regionTypeName;
        geoModel.level = [NSNumber numberWithInteger:level];
        
        // Add region with respected level to items array
        [App_Delegate.appfilter addGeoItem:geoModel tolevel:level];
        // Break the iteration
        if (!model.childRegion) {
            return geoModel;
        }
        
        NSMutableArray *childArray = [NSMutableArray new];
        // Get region details using id
        NSArray *subRegions = [App_Delegate.appfilter.geographyResponse objectForKey:model.regionId.stringValue];
        for (NSDictionary *subRegion in subRegions) {
            GeographyModel *geoModel = [self createRegionItem:subRegion withLevel:level + 1];
            [childArray addObject:geoModel];
        }
        
        NSArray *childs = [NSArray arrayWithArray:childArray];
        geoModel.subRegions = childs;
        childArray = nil;
        return geoModel;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@",exception.debugDescription);
        return [GeographyModel new];
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark GET INDUSTRY methods

//---------------------------------------------------------------

+ (void) callWebServiceToGetIndustryDetails {
    // Check for internet
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        return;
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%d", GET_ALL_INDUSTRY_ACTION, (int)App_Delegate.appfilter.loginUserId.intValue];
    DLog(@"\n\n\n\n==========>>> Get Industry details - Service Call begin <<<==========\n\n\n\n");
    [ServiceManager getWebServiceCall:urlString parameters:nil whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get Industry details - Service Call finish <<<==========\n\n\n\n");
        if (errorMessage) {
        } else {
            @try {
                if (response != nil) {
                    IndustryList *industryList = [IndustryList new];
                    [HKJsonUtils updatePropertiesWithData:response forObject:industryList];
                    
                    [ServicePreference saveCustomObject:industryList key:KEY_INDUSTRY_MODEL];
                    Industry *industry = [industryList.industryList objectAtIndex:0];
                    
                    NSString *industryId = [ServicePreference objectForKey:KEY_INDUSTRY_ID];
                    if (industryList.industryList.count > 0) {
                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.industryId.intValue == %d", industryId.intValue];
                        NSArray *filter = [industryList.industryList filteredArrayUsingPredicate:predicate];
                        if (filter.count > 0) {
                            Industry *filterIndustry = [filter objectAtIndex:0];
                            App_Delegate.appfilter.industryId = filterIndustry.industryId;
                            App_Delegate.appfilter.industryDescription = filterIndustry.industryDescription;
                            // Update app filters
                            [App_Delegate saveFilters];
                        }
                    }
                    
                    if (!industryId) {
                        [ServicePreference setObject:industry.industryId.stringValue forKey:KEY_INDUSTRY_ID];
                        App_Delegate.appfilter.industryId = industry.industryId;
                        App_Delegate.appfilter.industryDescription = industry.industryDescription;
                        // Update app filters
                        [App_Delegate saveFilters];
                    }
                }
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
            }
        }
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Feeds methods

//---------------------------------------------------------------

+ (void) callWebServiceToShareFeeds:(NSString *)action params:(NSDictionary *)params usingBlock:(void(^)(NSDictionary *response, NSError *error))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil, nil);
    }
    
    AFHTTPSessionManager *manager = [ServiceUtil requestManagerWithAccessToken:YES];
    DLog(@"\n\n\n\n==========>>> Share feeds - Service Call begin <<<==========\n\n\n\n");
    [manager POST:action parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        DLog(@"\n\n\n\n==========>>> Share feeds - Service Call finish <<<==========\n\n\n\n");
        block(responseObject, nil);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        DLog(@"\n\n\n\n==========>>> Share feeds - Service Call finish <<<==========\n\n\n\n");
        block(nil, error);
    }];
}

//---------------------------------------------------------------

+ (void) callWebServiceToLikeCommentReport:(NSString *)action params:(NSDictionary *)params usingBlock:(void(^)(NSDictionary *response, NSError *error))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil, nil);
    }
    
    AFHTTPSessionManager *manager = [ServiceUtil requestManagerWithAccessToken:YES];
    DLog(@"\n\n\n\n==========>>> Like Or Comment - Service Call begin <<<==========\n\n\n\n");
    [manager POST:action parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        DLog(@"\n\n\n\n==========>>> Like Or Comment - Service Call finish <<<==========\n\n\n\n");
        block(responseObject, nil);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        DLog(@"\n\n\n\n==========>>> Like Or Comment - Service Call finish <<<==========\n\n\n\n");
        block(nil, error);
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Charts methods

//---------------------------------------------------------------

+ (void) callWebServiceToGetTableData:(Filters *)filters reportId:(NSNumber *)reportId usingBlock:(void(^)(NSString *errorMessage, TableDataDetails *response, NSNumber *status))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil, nil, nil);
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%d", GET_TABLE_DATA, (int)reportId.intValue];
    NSDictionary *details = [filters serializeFiltersForTableDataURL];
    
    //UPDATE: 07-DEC- 2017 ~ IN-215 : Time zone changes, add new parameters
    // This dates needs to be in UTC format - Convert "to", "from", "compFrom", "compto" in UTC date
    // fromDateTime         - Start date in UTC
    // toDateTime           - End date in UTC
    // compFromDateTime     - Compare Start date in UTC
    // compToDateTime       - Compare End date in UTC
    
    @try {
        
        NSString *fromDate = [NSString stringWithFormat:@"%@ 00:00:00", [details objectForKey:KEY_DATE_FROM]];
        NSString *toDate = [NSString stringWithFormat:@"%@ 23:59:59", [details objectForKey:KEY_DATE_TO]];
        NSString *compFromDate = [NSString stringWithFormat:@"%@ 00:00:00", [details objectForKey:KEY_DATE_COMP_FROM]];
        NSString *compToDate = [NSString stringWithFormat:@"%@ 23:59:59", [details objectForKey:KEY_DATE_COMP_TO]];
        
        NSDate *startDate = [NSDate dateFromNSString:fromDate withFormat:SERVICE_DATE_TIME_FORMAT];
        NSDate *endDate = [NSDate dateFromNSString:toDate withFormat:SERVICE_DATE_TIME_FORMAT];
        NSDate *compStartDate = [NSDate dateFromNSString:compFromDate withFormat:SERVICE_DATE_TIME_FORMAT];
        NSDate *compEndDate = [NSDate dateFromNSString:compToDate withFormat:SERVICE_DATE_TIME_FORMAT];
        
        long long utcStartDateInMilliSeconds = [Helper utcTimeIntervalForDate:startDate];
        long long utcEndDateInMilliSeconds = [Helper utcTimeIntervalForDate:endDate];
        long long utcCompStartDateInMilliSeconds = [Helper utcTimeIntervalForDate:compStartDate];
        long long utcCompEndDateInMilliSeconds = [Helper utcTimeIntervalForDate:compEndDate];
        
        NSMutableDictionary *tempDetails = [NSMutableDictionary dictionaryWithDictionary:details];
        [tempDetails setObject:[NSNumber numberWithLongLong:utcStartDateInMilliSeconds] forKey:@"fromDateTime"];
        [tempDetails setObject:[NSNumber numberWithLongLong:utcEndDateInMilliSeconds] forKey:@"toDateTime"];
        [tempDetails setObject:[NSNumber numberWithLongLong:utcCompStartDateInMilliSeconds] forKey:@"compFromDateTime"];
        [tempDetails setObject:[NSNumber numberWithLongLong:utcCompEndDateInMilliSeconds] forKey:@"compToDateTime"];
        
        details = [NSDictionary dictionaryWithDictionary:tempDetails];
        tempDetails = nil;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
    
    DLog(@"\n\n\n\n==========>>> Get table data - Service Call begin <<<==========\n\n\n\n");

    [ServiceManager getWebServiceCall:urlString parameters:details whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get table data - Service Call finish <<<==========\n\n\n\n");
        if (response) {
            TableDataDetails *chartList = [TableDataDetails new];
            @try {
                NSDictionary *details = [response objectForKey:MAIN_BODY];
                chartList = [TableDataDetails new];
                [HKJsonUtils updatePropertiesWithData:details forObject:chartList];
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
            }
            block(nil, chartList, status);
        } else {
            block(nil, nil, status);
        }
    }];
}

//---------------------------------------------------------------

+ (void) callWebServiceToGetChartData:(NSDictionary *)details dashboardId:(NSNumber *)dashboardId usingBlock:(void(^)(NSError *error, ChartListModel *chartList))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil, nil);
    }
    
    //UPDATE: 21-Nov- 2017 ~ IN-215 : Time zone changes, add new parameters
    // This dates needs to be in UTC format - Convert "to", "from", "compFrom", "compto" in UTC date
    // fromDateTime         - Start date in UTC
    // toDateTime           - End date in UTC
    // compFromDateTime     - Compare Start date in UTC
    // compToDateTime       - Compare End date in UTC
    
    @try {
        NSDate *startDate = [NSDate dateFromNSString:[details objectForKey:KEY_DATE_FROM] withFormat:SERVICE_DATE_TIME_SHORT_FORMAT];
        NSDate *endDate = [NSDate dateFromNSString:[details objectForKey:KEY_DATE_TO] withFormat:SERVICE_DATE_TIME_SHORT_FORMAT];
        NSDate *compStartDate = [NSDate dateFromNSString:[details objectForKey:KEY_DATE_COMP_FROM] withFormat:SERVICE_DATE_TIME_SHORT_FORMAT];
        NSDate *compEndDate = [NSDate dateFromNSString:[details objectForKey:KEY_DATE_COMP_TO] withFormat:SERVICE_DATE_TIME_SHORT_FORMAT];

        long long utcStartDateInMilliSeconds = [Helper utcTimeIntervalForDate:startDate];
        long long utcEndDateInMilliSeconds = [Helper utcTimeIntervalForDate:endDate];
        long long utcCompStartDateInMilliSeconds = [Helper utcTimeIntervalForDate:compStartDate];
        long long utcCompEndDateInMilliSeconds = [Helper utcTimeIntervalForDate:compEndDate];
        
        NSMutableDictionary *tempDetails = [NSMutableDictionary dictionaryWithDictionary:details];
        [tempDetails setObject:[NSNumber numberWithLongLong:utcStartDateInMilliSeconds] forKey:@"fromDateTime"];
        [tempDetails setObject:[NSNumber numberWithLongLong:utcEndDateInMilliSeconds] forKey:@"toDateTime"];
        [tempDetails setObject:[NSNumber numberWithLongLong:utcCompStartDateInMilliSeconds] forKey:@"compFromDateTime"];
        [tempDetails setObject:[NSNumber numberWithLongLong:utcCompEndDateInMilliSeconds] forKey:@"compToDateTime"];
        
        details = [NSDictionary dictionaryWithDictionary:tempDetails];
        tempDetails = nil;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
    

    DLog(@"\n\n\n\n==========>>> Get Dashboard details - Service Call Begin <<<==========\n\n\n\n");
    NSString *urlString = [NSString stringWithFormat:@"%@/%d", GET_CHART_DATA, (int)dashboardId.intValue];

    [ServiceManager getWebServiceCall:urlString parameters:details whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get Dashboard details - Service Call finish <<<==========\n\n\n\n");
        if (response) {
            ChartListModel *chartList = [ChartListModel new];
            @try {
                NSDictionary *details = [response objectForKey:MAIN_BODY];
                chartList = [ChartListModel new];
                [HKJsonUtils updatePropertiesWithData:details forObject:chartList];
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
            }
            block(nil, chartList);
        } else {
            block(nil, nil);
        }
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Filters methods

//---------------------------------------------------------------

+ (void) callWebServiceToGetFilterType:(NSNumber *)dashboardId usingBlock:(void(^)(FilterListDetails *filters))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil);
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%d", GET_DASHBOARD_FILTERS_ACTION, (int)dashboardId.intValue];
    DLog(@"\n\n\n\n==========>>> Get Filter types - Service Call Begin <<<==========\n\n\n\n");

    [ServiceManager getWebServiceCall:urlString parameters:nil whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get Filter types - Service Call finish <<<==========\n\n\n\n");
        if (response) {
            @try {
                NSDictionary *data = [response objectForKey:MAIN_BODY];
                
                FilterListDetails *filterListDetails = [FilterListDetails new];
                [HKJsonUtils updatePropertiesWithData:data forObject:filterListDetails];
                block(filterListDetails);
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
                block(nil);
            }
        } else {
            block(nil);
        }
    }];
}

//---------------------------------------------------------------

+ (void) callWebServiceToGetRegionType:(Filters *)filter usingBlock:(void(^)(RegionTypeList *regionTypeList))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil);
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%d", GET_REGION_TYPE_ACTION, (int)App_Delegate.appfilter.tenantId.intValue];
    DLog(@"\n\n\n\n==========>>> Get Region types - Service Call Begin <<<==========\n\n\n\n");

    [ServiceManager getWebServiceCall:urlString parameters:nil whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get Region types - Service Call finish <<<==========\n\n\n\n");
        if (response) {
            @try {
                RegionTypeList *regionTypeList = [RegionTypeList new];
                [HKJsonUtils updatePropertiesWithData:response forObject:regionTypeList];
                block(regionTypeList);
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
                block(nil);
            }
        } else {
            block(nil);
        }
    }];
}

//---------------------------------------------------------------

+ (void) callWebServiceToGetRegions:(Filters *)filter usingBlock:(void(^)(RegionList *regionList))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil);
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%d/%d", GET_REGIONS_BY_TENANT_ACTION, (int)App_Delegate.appfilter.tenantId.intValue, (int)filter.regionTypeId.intValue];
    filter.accessible = [NSNumber numberWithBool:YES];
    
    NSDictionary *details = [filter serializeFiltersForRegionService];
    DLog(@"\n\n\n\n==========>>> Get Regions - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager getWebServiceCall:urlString parameters:details whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get Regions - Service Call finish <<<==========\n\n\n\n");
        if (response) {
            @try {
                RegionList *regionList = [RegionList new];
                [HKJsonUtils updatePropertiesWithData:response forObject:regionList];
                block(regionList);
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
                block(nil);
            }
        } else {
            block(nil);
        }
    }];
}


//---------------------------------------------------------------

+ (void) callWebServiceToGetLocations:(NSDictionary *)details usingBlock:(void(^)(LocationList *locationList))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil);
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%d/%d", GET_TENANT_LOCATIONS_ACTION, (int)App_Delegate.appfilter.tenantId.intValue, (int)App_Delegate.appfilter.loginUserId.intValue];
    DLog(@"\n\n\n\n==========>>> Get Locations - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager getWebServiceCall:urlString parameters:details whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get Locations - Service Call finish <<<==========\n\n\n\n");
        if (response) {
            @try {
                LocationList *locationList = [LocationList new];
                [HKJsonUtils updatePropertiesWithData:response forObject:locationList];
                block(locationList);
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
                block(nil);
            }
        } else {
            block(nil);
        }
    }];
}

//---------------------------------------------------------------

+ (void) callWebServiceToGetLocationGroup:(NSNumber *)locationId usingBlock:(void(^)(LocationGroupList *locationGroupList))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil);
    }
    
    NSDictionary *params = @{@"activeStatus" : @"Normal"};
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%d/%d", GET_LOCATION_GROUPS_ACTION, (int)locationId.intValue, (int)App_Delegate.appfilter.loginUserId.intValue];
    DLog(@"\n\n\n\n==========>>> Get Location Groups - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager getWebServiceCall:urlString parameters:params whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get Location Groups - Service Call finish <<<==========\n\n\n\n");
        if (response) {
            @try {
                LocationGroupList *locationGroupList = [LocationGroupList new];
                [HKJsonUtils updatePropertiesWithData:response forObject:locationGroupList];
                block(locationGroupList);
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
                block(nil);
            }
        } else {
            block(nil);
        }
    }];
}

//---------------------------------------------------------------

+ (void) callWebServiceToGetProducts:(Filters *)filter usingBlock:(void(^)(ProductList *productList))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil);
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@", GET_PRODUCTS_ACTION];
    NSDictionary *details = [filter serializeFiltersForProductService];
    DLog(@"\n\n\n\n==========>>> Get Products - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager getWebServiceCall:urlString parameters:details whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get Products - Service Call finish <<<==========\n\n\n\n");
        if (response) {
            @try {
                ProductList *productList = [ProductList new];
                [HKJsonUtils updatePropertiesWithData:response forObject:productList];
                block(productList);
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
                block(nil);
            }
        } else {
            block(nil);
        }
    }];
}

//---------------------------------------------------------------

+ (void) callWebServiceToGetUsers:(Filters *)filter withDetails:(NSDictionary *)details usingBlock:(void(^)(UserList *userList))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil);
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%d", GET_USERS_ACTION, (int)filter.locationId.intValue];
    DLog(@"\n\n\n\n==========>>> Get Users - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager getWebServiceCall:urlString parameters:details whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get Users - Service Call finish <<<==========\n\n\n\n");
        if (response) {
            @try {
                UserList *userList = [UserList new];
                [HKJsonUtils updatePropertiesWithData:response forObject:userList];
                block(userList);
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
                block(nil);
            }
        } else {
            block(nil);
        }
    }];
}

//---------------------------------------------------------------

+ (void) callWebServiceToGetAvailabelUsers:(Filters *)filter usingBlock:(void(^)(UserList *userList))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil);
    }
    
    NSDictionary *params = [filter serializeFiltersForDashboardUsersService];
    NSString *urlString = [NSString stringWithFormat:@"%@/%d", GET_AVAILABLE_USER_ACTION, (int) App_Delegate.appfilter.tenantId.integerValue];
    DLog(@"\n\n\n\n==========>>> Get Available user - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager getWebServiceCall:urlString parameters:params whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get Available user - Service Call finish <<<==========\n\n\n\n");
        if (response) {
            @try {
                UserList *userList = [UserList new];
                [HKJsonUtils updatePropertiesWithData:response forObject:userList];
                block(userList);
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
                block(nil);
            }
        } else {
            block(nil);
        }
    }];
}

//---------------------------------------------------------------

+ (void) getAllUsersOfTenant:(void(^)(UserList *userList))block {
    
    NSString *stringUrl = [NSString stringWithFormat:@"%@/%d?excludeSuperAdmin=false&checkAccessibility=false", GET_USER_MAIL_RECIPIENT, App_Delegate.appfilter.tenantId.intValue];
    DLog(@"\n\n\n\n==========>>> Get all users of tenant - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager getWebServiceCall:stringUrl parameters:nil whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get all users of tenant - Service Call finish <<<==========\n\n\n\n");
        UserList *userList = nil;
        if (errorMessage == nil) {
            userList = [UserList new];
            [HKJsonUtils updatePropertiesWithData:response forObject:userList];
        }
        block(userList);
    }];
}

//---------------------------------------------------------------

+ (void) callWebServiceToGetAgents:(NSDictionary *)details usingBlock:(void(^)(UserList *userList))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil);
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%d", GET_AGENTS_ACTION, (int)App_Delegate.appfilter.tenantId.integerValue];
    DLog(@"\n\n\n\n==========>>> Get Agent - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager getWebServiceCall:urlString parameters:details whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get Agent - Service Call finish <<<==========\n\n\n\n");
        if (response) {
            @try {
                UserList *userList = [UserList new];
                [HKJsonUtils updatePropertiesWithData:response forObject:userList];
                block(userList);
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
                block(nil);
            }
        } else {
            block(nil);
        }
    }];
}

//---------------------------------------------------------------

+ (void) callWebServiceToGetPerformanceLeader:(NSDictionary *)details usingBlock:(void(^)(UserList *userList))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil);
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%d", GET_PL_ACTION, (int)App_Delegate.appfilter.tenantId.integerValue];
    DLog(@"\n\n\n\n==========>>> Get Performance leader - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager getWebServiceCall:urlString parameters:details whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get Performance leader - Service Call finish <<<==========\n\n\n\n");
        if (response) {
            @try {
                UserList *userList = [UserList new];
                [HKJsonUtils updatePropertiesWithData:response forObject:userList];
                block(userList);
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
                block(nil);
            }
        } else {
            block(nil);
        }
    }];
}

//---------------------------------------------------------------

+ (void) callWebServiceToGetTenantWiseRegionLocation:(void(^)(NSDictionary *response))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil);
    }
    
    NSDictionary *details = @{
                                @"userID" : [User signedInUser].userID,
                                @"showDeactivatedLocation" : [NSNumber numberWithBool:NO]
                              };
    
    MBProgressHUD *hud = [Helper showLoading:App_Delegate.topMostController.view];
    hud.label.text = @"Loading...";
    NSString *urlString = [NSString stringWithFormat:@"%@/%d", GET_TENANT_WISE_LOCATION_ACTION, (int)App_Delegate.appfilter.tenantId.integerValue];
    DLog(@"\n\n\n\n==========>>> TenantWise Regions - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager getWebServiceCall:urlString parameters:details whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> TenantWise Regions - Service Call finish <<<==========\n\n\n\n");
        [Helper hideLoading:hud];
        if (response) {
            @try {
                NSDictionary *data = [response objectForKey:MAIN_BODY];
                App_Delegate.appfilter.geographyResponse = data;
                App_Delegate.appfilter.geoItems = nil;
                // re-organize regions
                [ServiceManager reOrganizeRegionsAndLocations];
                block(data);
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
                block(nil);
            }
        } else {
            block(nil);
        }
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Observation methods

//---------------------------------------------------------------

+ (void) callWebServiceToGetObservationType:(NSDictionary *)details usingBlock:(void(^)(OBTypeList *obTypeList))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil);
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@", GET_OBTYPE_ACTION];
    DLog(@"\n\n\n\n==========>>> Get Observation type - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager getWebServiceCall:urlString parameters:details whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get Observation type - Service Call finish <<<==========\n\n\n\n");
        if (response) {
            @try {
                OBTypeList *obTypeList = [OBTypeList new];
                [HKJsonUtils updatePropertiesWithData:response forObject:obTypeList];
                block(obTypeList);
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
                block(nil);
            }
        } else {
            block(nil);
        }
    }];
}

//---------------------------------------------------------------

+ (void) callWebServiceToGetQuestionnaire:(Filters *)filter usingBlock:(void(^)(Questionnaire *questionnaire))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil);
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%d", GET_QUESTIONNAIRE_ACTION, (int)filter.obTypeId.integerValue];
    DLog(@"\n\n\n\n==========>>> Get Questionnaire - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager getWebServiceCall:urlString parameters:nil whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get Questionnaire - Service Call finish <<<==========\n\n\n\n");
        if (response) {
            @try {
                NSDictionary *details = [response objectForKey:MAIN_BODY];
                Questionnaire *questionnaire = [Questionnaire new];
                [HKJsonUtils updatePropertiesWithData:details forObject:questionnaire];
                block(questionnaire);
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
                block(nil);
            }
        } else {
            block(nil);
        }
    }];
}

//---------------------------------------------------------------

+ (void) callWebServiceToGetGreatJobOn:(void(^)(GreatJobList *jobList))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil);
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@", GET_GREAT_JOB_ON_ACTION];
    NSDictionary *params = @{
                                @"total" : [NSNumber numberWithInt:100000],
                                @"first" : [NSNumber numberWithInteger:0]
                            };
    DLog(@"\n\n\n\n==========>>> Get Great JobOn - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager postWebServiceCallwithParam:urlString parameters:params whenFinish:^(NSString *errorMessage, NSDictionary *response) {
        DLog(@"\n\n\n\n==========>>> Get Great JobOn - Service Call finish <<<==========\n\n\n\n");
        if (response) {
            @try {
                GreatJobList *jobList = [GreatJobList new];
                [HKJsonUtils updatePropertiesWithData:response forObject:jobList];
                block(jobList);
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
                block(nil);
            }
        } else {
            block(nil);
        }
    }];
}

//---------------------------------------------------------------

+ (void) callWebServiceSaveEvent:(Event *)event usingBlock:(void(^)(NSString *errorMessage, NSDictionary *details))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil, nil);
    }
    
    NSDictionary *params = [event searializeToSaveEvent];
    NSString *urlString = [NSString stringWithFormat:@"%@", POST_CREATE_OBSERVATION_ACTION];

    // This is edit observation - use PUT method
    if (event.eventId) {
        DLog(@"\n\n\n\n==========>>> Edit event - Service Call Begin <<<==========\n\n\n\n");
        [ServiceManager putWebServiceCallwithParam:urlString parameters:params whenFinish:^(NSString *errorMessage, NSDictionary *response) {
            DLog(@"\n\n\n\n==========>>> Edit event - Service Call finish <<<==========\n\n\n\n");
            if (response) {
                @try {
                    block(errorMessage, response);
                } @catch (NSException *exception) {
                    DLog(@"Exception :%@", exception.debugDescription);
                    block(errorMessage, nil);
                }
            } else {
                block(errorMessage, nil);
            }
        }];
    } else {
        // This is new observation - use POST method
        DLog(@"\n\n\n\n==========>>> Create event - Service Call Begin <<<==========\n\n\n\n");
        [ServiceManager postWebServiceCallwithParam:urlString parameters:params whenFinish:^(NSString *errorMessage, NSDictionary *response) {
            DLog(@"\n\n\n\n==========>>> Create event - Service Call finish <<<==========\n\n\n\n");
            if (response) {
                @try {
                    block(errorMessage, response);
                } @catch (NSException *exception) {
                    DLog(@"Exception :%@", exception.debugDescription);
                    block(errorMessage, nil);
                }
            } else {
                block(errorMessage, nil);
            }
        }];
    }
}

//---------------------------------------------------------------

+ (void) callWebServiceToGetObservationCategories:(Filters *)filter usingBlock:(void(^)(OBCategoryList *categoryList))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil);
    }
    
    // surveyEntityType=Observation | CounterCoaching // activeStatus=Normal // industryId=1
    
    NSDictionary *params = [filter serializeFiltersForObservationCategoriesService];
    DLog(@"\n\n\n\n==========>>> Get Observation categories - Service Call Begin <<<==========\n\n\n\n");
    NSString *urlString = [NSString stringWithFormat:@"%@", GET_OB_CATEGORIES_ACTION];
    [ServiceManager getWebServiceCall:urlString parameters:params whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get Observation categories - Service Call finish <<<==========\n\n\n\n");
        if (response) {
            @try {
                OBCategoryList *categoryList = [OBCategoryList new];
                [HKJsonUtils updatePropertiesWithData:response forObject:categoryList];
                block(categoryList);
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
                block(nil);
            }
        } else {
            block(nil);
        }
    }];
}

//---------------------------------------------------------------

+ (void) callWebServiceToGetListOfObservation:(NSDictionary *)params usingBlock:(void(^)(EventList *eventList))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil);
    }
    
    // UPDATE: 21-Sep-2017 ~ Sonal call is removed, and call from database
//    NSString *urlString = [NSString stringWithFormat:@"%@", POST_FIND_OB_ACTION];
    
    NSDictionary *queryParam = @{@"tenantId" : App_Delegate.appfilter.tenantId,
                                 @"userId" : App_Delegate.appfilter.loginUserId};
    
    NSString *urlString = [NSString stringWithFormat:@"%@", POST_FIND_OB_ACTION];
    DLog(@"\n\n\n\n==========>>> Get Survey list - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager postWebServiceCall:urlString parameters:queryParam withBody:params whenFinish:^(NSString *errorMessage, NSDictionary *response) {
        DLog(@"\n\n\n\n==========>>> Get Survey list - Service Call finish <<<==========\n\n\n\n");
        if (response) {
            @try {
                NSDictionary *data = [response objectForKey:MAIN_BODY];
                EventList *eventList = [EventList new];
                [HKJsonUtils updatePropertiesWithData:data forObject:eventList];
                block(eventList);
                
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
                block(nil);
            }
        } else {
            block(nil);
        }
    }];
}

//---------------------------------------------------------------

+ (void) callWebServiceToGetSingleObservation:(NSNumber *)eventId usingBlock:(void(^)(Event *event))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil);
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%d", GET_SINGLE_OBSERVATION_ACTION, eventId.intValue];
    DLog(@"\n\n\n\n==========>>> Get Single survey - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager getWebServiceCall:urlString parameters:nil whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get Single survey - Service Call finish <<<==========\n\n\n\n");
        if (response) {
            @try {
                Event *event = [Event new];
                NSDictionary *data = [response objectForKey:MAIN_BODY];
                [HKJsonUtils updatePropertiesWithData:data forObject:event];
                block(event);
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
                block(nil);
            }
        } else {
            block(nil);
        }
    }];
}

//---------------------------------------------------------------

+ (void) callWebServiceToDeleteSingleObservation:(NSNumber *)eventId usingBlock:(void(^)(NSDictionary *response))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil);
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%d", DELETE_SINGLE_OBSERVATION_ACTION, eventId.intValue];
    DLog(@"\n\n\n\n==========>>> Remove survey - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager deleteWebServiceCall:urlString parameters:nil whenFinish:^(NSString *errorMessage, NSDictionary *response) {
        DLog(@"\n\n\n\n==========>>> Remove survey - Service Call finish <<<==========\n\n\n\n");
        if (response) {
            @try {
                block(response);
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
                block(nil);
            }
        } else {
            block(nil);
        }
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Menu methods

//---------------------------------------------------------------

+ (void) getAvailableMenu:(void(^)(NSArray *menus))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil);
    }
    
    // Menu Url + tenant id + login user id
    NSString *urlString = [NSString stringWithFormat:@"%@/%d/%d", GET_MENU_DETAILS, (int)App_Delegate.appfilter.tenantId.intValue, (int)App_Delegate.appfilter.loginUserId.intValue];
    DLog(@"\n\n\n\n==========>>> Get menus - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager getWebServiceCall:urlString parameters:nil whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get menus - Service Call finish <<<==========\n\n\n\n");
        if (response) {
            @try {
                NSMutableArray *tempArray = [NSMutableArray new];
                NSDictionary *data = [response objectForKey:MAIN_BODY];
                for (NSDictionary *menuDict in data) {
                    Menu *menu = [Menu new];
                    [HKJsonUtils updatePropertiesWithData:menuDict forObject:menu];
                    [tempArray addObject:menu];
                }
                NSArray *menuArray = [NSArray arrayWithArray:tempArray];
                tempArray = nil;
                block(menuArray);
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
                block(nil);
            }
        } else {
            block(nil);
        }
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Mobile keys methods

//---------------------------------------------------------------

+ (void) getServerMappingKeyList:(id)params usingBlock:(void(^)(NSDictionary *keys))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil);
    }

    // Common manager
    AFHTTPSessionManager *manager = [ServiceUtil requestManagerWithAccessToken:NO];

    NSString *urlString = [ServiceUtil addHeaderParameters:GET_MOBILE_KEYS_ACTION withParams:params];
    DLog(@"\n\n\n\n==========>>> Get Server keys - Service Call Begin <<<==========\n\n\n\n");

    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        DLog(@"\n\n\n\n==========>>> Get Server keys - Service Call finish <<<==========\n\n\n\n");

        if (responseObject) {
            NSDictionary *details = responseObject[@"data"];
            App_Delegate.keyMapping = details;
            
            if (details && details.count > 0) {
                // if keys available write it to disk
                [Helper writeFileToDocumentDirectory:keyMappingFileName andFileData:responseObject];
            } else {
                NSDictionary *keys = [Helper readFileFromDocumentDirectory:keyMappingFileName];
                App_Delegate.keyMapping = keys[@"data"];
            }
        } else {
            NSDictionary *keys = [Helper readFileFromDocumentDirectory:keyMappingFileName];
            App_Delegate.keyMapping = keys[@"data"];
        }
        block(responseObject);
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        DLog(@"\n\n\n\n==========>>> Get Server keys - Service Call finish <<<==========\n\n\n\n");
        
        NSDictionary *keys = [Helper readFileFromDocumentDirectory:keyMappingFileName];
        App_Delegate.keyMapping = keys[@"data"];
        
        if (error.code == NSURLErrorTimedOut) {
            block(nil);
        }
        block(nil);
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Roles and Permissions methods

//---------------------------------------------------------------

+ (void)getRolesAndPermissionsList:(id)params usingBlock:(void(^)(Permissions *details))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        block(nil);
    }
    MBProgressHUD *hud = [Helper showLoading:App_Delegate.topMostController.view];
    hud.label.text = @"Loading...";

    NSString *urlString = [NSString stringWithFormat:@"%@/%d/%d", GET_PERMISSIONS_ACTION, (int)App_Delegate.appfilter.tenantId.intValue, (int)App_Delegate.appfilter.loginUserId.intValue];
    DLog(@"\n\n\n\n==========>>> Roles and Permissions - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager getWebServiceCall:urlString parameters:nil whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Roles and Permissions - Service Call finish <<<==========\n\n\n\n");
        [Helper hideLoading:hud];
        @try {
            Permissions *permissions = [Permissions new];
            DLog(@"Permissions :%@", response);
            if (response) {
                NSDictionary *details = response[@"data"];
                [HKJsonUtils updatePropertiesWithData:details forObject:permissions];
            }
            block(permissions);
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
            block(nil);
        }
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Send device token methods

//---------------------------------------------------------------

+ (void) postDeviceUDIDtoServer:(NSDictionary *)details {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        return;
    }
    
    // Background service - DO NOT Show loading indicators here
    DLog(@"\n\n\n\n==========>>> Post device UDID to server - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager postWebServiceCall:POST_DEVICE_ID_ACTION parameters:nil withBody:details whenFinish:^(NSString *errorMessage, NSDictionary *response) {
        DLog(@"\n\n\n\n==========>>> Post device UDID to server - Service Call finish <<<==========\n\n\n\n");
        @try {
            if (response) {
                DLog(@"response");
            }
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
    }];
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark Video methods

//---------------------------------------------------------------

+ (void) callWebServiceToGetVideoCategoryList:params usingBlock:(void(^)(VideoCategoryModel *model, NSString *error))block {
    // Check for internet connnection
    if (![Helper checkForConnection:App_Delegate.topMostController]) {
        return;
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@", GET_VIDEO_CAREGORY];
    DLog(@"\n\n\n\n==========>>> Get Video categories - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager getWebServiceCall:urlString parameters:params whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Get Video categories - Service Call finish <<<==========\n\n\n\n");
        @try {
            VideoCategoryModel *model = [VideoCategoryModel new];
            if (response) {
                [HKJsonUtils updatePropertiesWithData:response forObject:model];
            }
            block(model, errorMessage);
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
            block(nil, errorMessage);
        }
    }];
}

//---------------------------------------------------------------

@end
