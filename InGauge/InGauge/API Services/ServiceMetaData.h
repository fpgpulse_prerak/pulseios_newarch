//
//  ServiceMetaData.h
//  InGauge
//
//  Created by Mehul Patel on 11/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

//Application ID
#define APP_ID                      @"1256920868"
#define KEY_DOMAIN                  @"DOMAIN_URL"

#define SERVER_IP                   [[NSUserDefaults standardUserDefaults] objectForKey:KEY_DOMAIN]
#define BASE_URL                    ([NSString stringWithFormat:@"%@/pulse", SERVER_IP])

#define HOST                        ([NSString stringWithFormat:@"http://%@/api", BASE_URL])
#define SECURE_HOST                 ([NSString stringWithFormat:@"https://%@/api", BASE_URL])

#define DASHBOARD_CHART_URL         [NSString stringWithFormat:@"http://%@:3000", SERVER_IP]
#define FEEDS_CHART_URL             [NSString stringWithFormat:@"http://%@:3000", SERVER_IP]
#define PLAY_VIDEO_URL              [NSString stringWithFormat:@"http://%@:3000", SERVER_IP]


#define IS_SECURE_HOST              0

#define MAIN_BODY                   @"data"

// SERVICE ACTION CONSTANTS
// LOGIN
//#define LOGIN_ACTION                @"oauth/token"
#define LOGOUT_ACTION               @"api/user/secure/logout"
#define GET_USERID_ACTION           @"api/user/secure/getByToken"
#define GET_TENANT_ACTION           @"api/userTenantRole/secure/getUserTenantRole"
#define USER_SIGNUP                 @"api/user/signUp"
#define USER_FORGOTPASSWORD         @"api/user/forgot_password"
#define USER_CHECK_2FA              @"api/user/is2FARequired"
#define USER_VERIFY_2FA             @"api/user/verifySecure2FAToken"

//UPDATE: 09-Oct-2017 ~ Login service changes
#define LOGIN_ACTION                @"api/user/authenticateUser"

//TENANT
#define KEY_DEFAULT_TENANT_ROLE_MODEL @"DefaultTenantRoleModel"

// DASHBOARD
#define GET_DASHBOARD_DEFAULT_FILTERS_ACTION    @"api/util/getDefaultFilters"
#define GET_DASHBOARD_LIST_ACTION       @"api/dashboard/secure/findSpecial"
#define GET_DASHBOARD_DETAILS_ACTION    @"api/dashboard/secure"
#define NOTIFICATION_RELOAD_DASHBOARD_LIST @"kNotificationReloadDashboardList"
#define GET_CHART_DATA                  @"api/dashboard/secure/m"
#define GET_DASHBOARD_FILTERS_ACTION    @"api/dashboard/secure/m/filters"
#define GET_TABLE_DATA                  @"api/util/secure/getTableData"

//FILTERS
#define GET_REGION_TYPE_ACTION          @"api/regionType/secure/getRegionTypeByTenantId"
#define GET_REGIONS_BY_TENANT_ACTION    @"api/region/secure/getRegionByTenantRegionType"
#define GET_TENANT_LOCATIONS_ACTION     @"api/tenantLocation/secure/getAccessibleTenantLocation"
#define GET_LOCATION_GROUPS_ACTION      @"api/locationGroup/secure/getDashboardLocationGroupList"
#define GET_PRODUCTS_ACTION             @"api/locationGroup/secure/assignedProduct"
#define GET_USERS_ACTION                @"api/user/secure/getDashboardUserList"
#define GET_AGENTS_ACTION               @"api/user/secure/getUsersForSurvey"
#define GET_PL_ACTION                   @"api/user/secure/getUsersWithPermissionForSurvey"
#define GET_OBTYPE_ACTION               @"api/questionnaire/secure/getQuestionnaire"
#define GET_TENANT_WISE_LOCATION_ACTION @"api/tenantLocation/secure/getTenantWiseRegionLocation"
#define GET_AVAILABLE_USER_ACTION       @"api/user/secure/getAvailableUsers"

// Observation
#define GET_QUESTIONNAIRE_ACTION        @"api/questionnaire/secure/m"
#define GET_GREAT_JOB_ON_ACTION         @"api/oneToOneType/secure/m/find"
#define POST_CREATE_OBSERVATION_ACTION  @"api/survey/secure"
#define GET_OB_CATEGORIES_ACTION        @"api/surveyCategory/secure/getSurveyCategory"

// UPDATE: 21-Sep-2017 ~ Sonal call is removed, and call from database
//#define POST_FIND_OB_ACTION             @"api/survey/secure/m/find"
#define POST_FIND_OB_ACTION             @"api/survey/secure/getSurveys"

#define GET_SINGLE_OBSERVATION_ACTION   @"api/survey/secure"
#define DELETE_SINGLE_OBSERVATION_ACTION   @"api/survey/secure"

// PREFERENCES
#define GET_ALL_PREFERENCES_ACTION      @"api/preference/secure/getAllPreferences"

// GET MOBILE KEYS
#define GET_MOBILE_KEYS_ACTION          @"api/preference/getMobileKeys"

//INDUSTRY
//#define GET_ALL_INDUSTRY_ACTION         @"api/industry/getIndustry"
// UPDATE: 25-Sep-2017 ~ Get industry access changed
#define GET_ALL_INDUSTRY_ACTION         @"api/industry/getAccessibleIndustry"

#define KEY_INDUSTRY_MODEL              @"IndustryModel"

//USER
#define GET_USER_AVATAR                 @"api/user/avatar"

//FEEDS
#define GET_FEEDS_ACTION                @"api/feed/secure/fetchFeeds"

//FEEDS Like
#define USERFEEDLIKE                    @"api/feed/secure/likeFeed"

//Comment Add
#define ADDCOMMENT                      @"api/feedComment/secure/m"

// Goal
#define GET_GOAL_DETAILS                @"api/goal/goalProgress"
// Menu
#define GET_MENU_DETAILS                @"api/menu/secure/getMenuList"

// Permissions
#define GET_PERMISSIONS_ACTION          @"api/util/fetchPermission"

// Send DeviceId
#define POST_DEVICE_ID_ACTION           @"api/user/secure/getDeviceID"


// Pulse Mail
#define GET_UNREAD_MAILS_COUNT          @"api/userMessage/getUnReadCount"
#define DELETE_USER_MAIL                @"api/userMessage/deleteEmails"
#define REPLY_USER_MAIL                 @"api/userMessage/replyFrom"
#define UPDATE_USER_MAIL_READ           @"userMessage/updateReadTime/"
#define GET_USER_MAIL_RECIPIENT         @"api/user/secure/getUsersFromTenant"
#define SEND_USER_MAIL                  @"api/userMessage/secure/saveMessage"
#define GET_PULSE_USER_MAILS            @"userMessage/search"
#define NOTIFY_UPDATE_MAILS             @"kNotifyUpdateMails"
#define GET_MAIL_Find                   @"api/userMessage/secure/m/find"
#define GET_MAIL_Details                @"api/userMessage/getMailDetails"
#define DOWNLOAD_ATTACHMENT             @"api/userMessage/attachment/"
#define UPLOAD_ATTACHMENT               @"api/userMessage/uploadFile"

// Video
#define GET_VIDEO_CAREGORY              @"api/media/secure/getAllMedia"

