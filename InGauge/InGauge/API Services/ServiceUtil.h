//
//  ServiceUtil.h
//  InGauge
//
//  Created by Mehul Patel on 11/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceMetaData.h"
#import <AFNetworking/AFNetworking.h>
#import "User.h"
#import "Hankai.h"

#define FPServiceDefaultTimeout     60

extern NSString * const FPServiceErrorDomain;


typedef enum {
    FPServiceErrorsAuthenticationFailed,
    FPServiceErrorsAccessDenied
} FPServiceErrors;

@interface ServiceUtil : NSObject

+ (AFHTTPSessionManager *) requestManagerWithAccessToken:(BOOL)isAccessTokenRequired;
+ (void)setAccessToken:(AFHTTPSessionManager *)request;
+ (NSString *)translateError:(NSError *)error;
+ (NSString *) translateErrorCode:(NSURLSessionTask *)operation;
+ (AFHTTPSessionManager *)form:(NSDictionary *)params;

+ (AFHTTPSessionManager *) getSessionManager;
+ (AFHTTPSessionManager *)post:(NSDictionary *)params;

//+ (AFHTTPSessionManager *)postEntity:(NSString *)action entity:(NSDictionary *)entity isSecure:(BOOL)isSecure;
//
//+ (AFHTTPSessionManager *)postData:(NSData *)data action:(NSString *)action isSecure:(BOOL)isSecure;

+ (AFHTTPSessionManager *)get:(NSDictionary *)params;

+ (NSString *)addHeaderParameters:(NSString *)action withParams:(NSDictionary *)params;
+ (NSString *)addHeaderParametersWithOutBaseURL:(NSString *)baseURLString withParams:(NSDictionary *)params;

@end
