//
//  ServiceUtil.m
//  InGauge
//
//  Created by Mehul Patel on 11/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "ServiceUtil.h"
#import <AFNetworkActivityLogger/AFNetworkActivityLogger.h>
#import "NSString+HTML.h"

#define FP_ERROR_DETAILS            @"errorDetails"

NSString * const FPServiceErrorDomain = @"FPServiceErrorDomain";


@implementation ServiceUtil

//---------------------------------------------------------------

#pragma mark
#pragma mark Request helper methods

//---------------------------------------------------------------

+ (AFHTTPSessionManager *) requestManagerWithAccessToken:(BOOL)isAccessTokenRequired {
    
    AFHTTPSessionManager *manager = [self getSessionManager];
    
    // Set Content-Type
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // Set Authorization:bearer [And access token]
    if (isAccessTokenRequired) {
        [self setAccessToken:manager];
    }
    
    // Set IndustryId
    NSString *industryId = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_INDUSTRY_ID];
    if (industryId) {
        [manager.requestSerializer setValue:industryId forHTTPHeaderField:KEY_INDUSTRY_ID];
    } else {
        [manager.requestSerializer setValue:KEY_INDUSTRY_ID_VALUE forHTTPHeaderField:KEY_INDUSTRY_ID];
    }
    
    // Set Platform
    [manager.requestSerializer setValue:KEY_PLATFORM_VALUE forHTTPHeaderField:KEY_PLATFORM];
    
    // Set mobility call
    [manager.requestSerializer setValue:KEY_ISMOBILE_VALUE forHTTPHeaderField:KEY_ISMOBILE];
    
    // Set time zone
    // Get device local time zone
    [manager.requestSerializer setValue:[Helper getLocalTimeZone] forHTTPHeaderField:KEY_TIME_ZONE];
    
    return manager;
}

//---------------------------------------------------------------

+ (AFHTTPSessionManager *) getSessionManager {
    [[AFNetworkActivityLogger sharedLogger] startLogging];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    
    // Set Service timeout interval
    [manager.requestSerializer setTimeoutInterval:FPServiceDefaultTimeout];
    return manager;
}

//---------------------------------------------------------------

+ (NSString *)addHeaderParameters:(NSString *)action withParams:(NSDictionary *)params {
    NSString *urlPrefix = nil;
    @try {
        
        if (IS_SECURE_HOST) {
            urlPrefix = [NSString stringWithFormat:@"https://%@/%@", BASE_URL, action];
        } else {
            urlPrefix = [NSString stringWithFormat:@"http://%@/%@", BASE_URL, action];
        }
        
        NSString *urlString = nil;
        if (params.count > 0) {
            NSMutableString * body = [NSMutableString string];
            
            for (NSString * key in params) {
                NSString *appendString = [NSString stringWithFormat:@"%@", params[key]];

//                id value = params[key];
//                if ([value isKindOfClass:[NSNumber class]]) {
//                    // Check that NSNumber is boolean or not?
//                    if (value == (void*)kCFBooleanFalse || value == (void*)kCFBooleanTrue) {
//                        //Do not convert here
//                    } else {
//                        appendString = [self convertZeroToMinusTwo:appendString];
//                    }
//                } else {
//                    appendString = [self convertZeroToMinusTwo:appendString];
//                }
                
                // Append parameter
                [body appendFormat:@"%@=%@&", key, appendString];
            }
            
            [body deleteCharactersInRange:NSMakeRange(body.length-1, 1)];
            urlString = [NSString stringWithString:body];
            urlString = [NSString stringWithFormat:@"%@?%@", urlPrefix, urlString];
            body = nil;
        } else {
            urlString = urlPrefix;
        }
        
        return urlString;
        
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription)
        return urlPrefix;
    }
}

//---------------------------------------------------------------

// This is a patch to convert all ZEROs values to -2
+ (NSString *) convertZeroToMinusTwo:(NSString *)param {
    if ([param isEqualToString:@"0"]) {
        return @"-2";
    }
    return param;
}

//---------------------------------------------------------------

+ (NSString *)addHeaderParametersWithOutBaseURL:(NSString *)baseURLString withParams:(NSDictionary *)params {
    NSString *urlPrefix = baseURLString;
    @try {
        NSString *urlString = nil;
        if (params.count > 0) {
            NSMutableString * body = [NSMutableString string];            
            for (NSString * key in params) {
                NSString *appendString = [NSString stringWithFormat:@"%@", params[key]];
                
//                id value = params[key];
//                if ([value isKindOfClass:[NSNumber class]]) {
//                    // Check that NSNumber is boolean or not?
//                    if (value == (void*)kCFBooleanFalse || value == (void*)kCFBooleanTrue) {
//                        //Do not convert here
//                    } else {
//                        appendString = [self convertZeroToMinusTwo:appendString];
//                    }
//                } else {
//                    appendString = [self convertZeroToMinusTwo:appendString];
//                }
                
                // Append parameter
                [body appendFormat:@"%@=%@&", key, appendString];
            }
            
            [body deleteCharactersInRange:NSMakeRange(body.length-1, 1)];
            urlString = [NSString stringWithString:body];
            urlString = [NSString stringWithFormat:@"%@?%@", urlPrefix, urlString];
            body = nil;
        } else {
            urlString = urlPrefix;
        }
        
        return urlString;
        
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription)
        return urlPrefix;
    }
}

//---------------------------------------------------------------

+ (void)setAccessToken:(AFHTTPSessionManager *)request {
    User *user = [User signedInUser];    
    if (!isEmptyString(user.accessToken)) {
        NSString *authorisationBearer = [NSString stringWithFormat:@"bearer %@", user.accessToken];
        DLog(@"\n\n\n\n ˜˜˜˜˜˜˜˜ACCESS TOKEN :%@", authorisationBearer);
        [request.requestSerializer setValue:authorisationBearer forHTTPHeaderField:KEY_ACCESS_TOKEN];
    }
}

//---------------------------------------------------------------

+ (NSString *)translateError:(NSError *)error {
    NSString *errResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
    // Remove html characters    
    errResponse = [errResponse stringByConvertingHTMLToPlainText];
    return errResponse;
}

+ (NSString *) translateErrorCode:(NSURLSessionTask *)operation {
    NSHTTPURLResponse *response = (NSHTTPURLResponse *) [operation response];
    NSInteger statusCode = [response statusCode];
    DLog(@"RESPONSE CODE: %d", (int)statusCode);
    
    NSString *messageIfAny = nil;
    switch (statusCode) {
        
        // If user is unauthorized - logout immediately "As fast as you can :P"
        case 401: {
            // I'm out
            messageIfAny = @"Invalid username or password";
            [User signOut];
            [App_Delegate changeRootViewControllerTo:@"SignInTableViewController"];
            break;
        }

        case 405: {
            // Service not working!!!
            messageIfAny = @"Could not connect to server";
            break;
        }
            
        // Server break down!!!
        case 500: {
            messageIfAny = @"Server is down for maintenance please try again later";
            break;
        }
        
        // Service not availabel
        case 503:
            messageIfAny = @"Server is down for maintenance please try again later";
            break;
            
        default:
            break;
    }
    return messageIfAny;
}

//---------------------------------------------------------------

+ (NSError *)translateServiceError:(NSString *)code extraInfo:(NSDictionary *)extraInfo {
    NSMutableDictionary * userInfo = [NSMutableDictionary dictionary];
    
    NSString * desc = NSLocalizedString(code, nil);
    if (![desc isEqualToString:code]) {
        [userInfo setValue:desc forKey:NSLocalizedDescriptionKey];
    }
    if (userInfo == nil) {
        [userInfo setValue:NSLocalizedString(@"api.failed", nil) forKey:NSLocalizedDescriptionKey];
    }
    if (extraInfo.count > 0) {
        [userInfo setValue:extraInfo forKey:FP_ERROR_DETAILS];
    }
    
    return [NSError errorWithDomain:FPServiceErrorDomain code:0 userInfo:userInfo];
}

//---------------------------------------------------------------

+ (AFHTTPSessionManager *)form:(NSDictionary *)params {
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        [manager.requestSerializer setValue:@"application/x-www-form-urlencoded; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        [self setAccessToken:manager];
        [manager.requestSerializer setTimeoutInterval:FPServiceDefaultTimeout];
        
        if (params.count > 0) {
            for (NSString * key in params) {
                [manager.requestSerializer setValue:params[key] forHTTPHeaderField:key];
            }
        }
        return manager;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return nil;
    }
}

//---------------------------------------------------------------

+ (AFHTTPSessionManager *)post:(NSDictionary *)params {
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [self setAccessToken:manager];
    [manager.requestSerializer setTimeoutInterval:FPServiceDefaultTimeout];
    return manager;
}

//---------------------------------------------------------------

+ (AFHTTPSessionManager *)get:(NSDictionary *)params {
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [self setAccessToken:manager];
    [manager.requestSerializer setTimeoutInterval:FPServiceDefaultTimeout];
    return manager;
}

//---------------------------------------------------------------

@end

