//
//  AppDelegate.h
//  InGauge
//
//  Created by Mehul Patel on 24/04/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@class Filters;
@class AppFilter;
@class Permissions;
@class TenantRoleModel;
@class Industry;
@class AppManager;
@class MBProgressHUD;

#import "AppFilter.h"
#import <UserNotifications/UserNotifications.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow                  *window;
@property (nonatomic, strong) UIStoryboard              *mainStoryboard;
@property (nonatomic, strong) AppFilter                 *appfilter;
@property (nonatomic, strong) AppManager                *appManager;
@property (nonatomic, strong) NSDictionary              *keyMapping;
@property (nonatomic, strong) NSArray                   *menuArray;
@property (nonatomic, strong) Permissions               *userPermissions;
@property (nonatomic, strong) CLLocationManager         *locationManager;
@property (nonatomic, strong) MBProgressHUD             *globalHud;
@property (nonatomic) BOOL                              isNetworkAvailable;

- (void) changeRootViewControllerTo:(NSString *)viewControllerIdentifier;
- (void) changeTabBarControllerForTabIndex:(NSInteger)tabIndex;
- (UIViewController*) topMostController;
- (void) autoLoginUser;
- (void) reloadDashboardData;
- (void) saveFilters;
- (void) callServiceToPreloadMenus;
- (void) tenantChanged:(TenantRoleModel *)tenant;
- (void) industryChanged:(Industry *)industry;
- (void) showVerificationScreen;
//- (void) showGoalSettingsPage;

@end

