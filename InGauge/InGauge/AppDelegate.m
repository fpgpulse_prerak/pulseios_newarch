//
//  AppDelegate.m
//  InGauge
//
//  Created by Mehul Patel on 24/04/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "AppDelegate.h"
#import "AppFilter.h"
#import "AnalyticsLogger.h"

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#import <AFNetworkActivityLogger/AFNetworkActivityLogger.h>
#import <AFNetworkActivityLogger/AFNetworkActivityConsoleLogger.h>
#import "Reachability.h"
#import "ServiceManager.h"
#import "SlideNavigationController.h"
#import "RightMenuViewController.h"
#import <Highcharts/Highcharts.h>
#import "VerificationController.h"

@import Firebase;

@interface AppDelegate ()

@end

@implementation AppDelegate

//---------------------------------------------------------------

#pragma mark
#pragma mark UIApplication lifeCycle methods

//---------------------------------------------------------------

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Reset domain - if it is changed from "Domain.h" file
    if ([Helper isDomainChanged]) {
        [ServicePreference setObject:nil forKey:KEY_DOMAIN];
    }
    //Default base url - Admin url
    [Helper setDefaultDomain];    
        
    self.mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.isNetworkAvailable = YES;
    
    // clear old filters
    [ServicePreference saveCustomObject:nil key:KEY_DASHBOARD_FILTERS];
    
    // Location settings
//    [self locationSetup];
    
    // FireBase Analytics setup
    // FireBase will run only in release mode
//#if DEBUG == 0
    [FIRApp configure];
//#endif
    
    // Load high charts in advance -- This line is important! Do not remove/comment it
    // Other wise highcharts will not work
    [HIChartView preload];

    // Get Stored filters
    [self getStoredFilters];

    // Register for reachbility
    [self registerForNetworkReachabilityNotifications];
    
    // Register for Push Notification
    [self registerForRemoteNotifications:application];

    // Afnetowrking logger
    AFNetworkActivityConsoleLogger *testLogger = [AFNetworkActivityConsoleLogger new];
    [testLogger setLevel:AFLoggerLevelDebug];
    
    // Auto login
    // Show loading indicator
    self.globalHud = [Helper showLoading:App_Delegate.window];
    self.globalHud.label.text = @"Loading...";
    [self autoLoginUser];
    return YES;
}

//---------------------------------------------------------------

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    id presentedViewController = [window.rootViewController presentedViewController];
    NSString *className = presentedViewController ? NSStringFromClass([presentedViewController class]) : nil;
    
    if (window && [className isEqualToString:@"AVFullScreenViewController"]) {
        return UIInterfaceOrientationMaskAll;
    } else {
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
        return UIInterfaceOrientationMaskPortrait;
    }
}

//---------------------------------------------------------------

- (void)registerForRemoteNotifications:(UIApplication *)application {
    
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert) completionHandler:^(BOOL granted, NSError * _Nullable error) {
            if( !error ) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
            }
        }];
    }
    else {
        // iOS 8 or later
        // [START register_for_notifications]
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Notification delegate methods

//---------------------------------------------------------------

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}

//---------------------------------------------------------------

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    NSString * token = [NSString stringWithFormat:@"%@", deviceToken];
    //Format token as you need:
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    DLog(@"Device Token String :%@", token);
    
    NSLog(@"Device Token String :%@", token);
    // Store device token to user defaults
    [Helper setDeviceId:token];    
}

//---------------------------------------------------------------

- (void) application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    DLog(@"Remote notification error: %@", error.debugDescription);
}

//---------------------------------------------------------------

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    DLog(@"Receive Remote Notification");
    DLog(@"Remote notification info :%@", userInfo);

    if (application.applicationState == UIApplicationStateActive) {
        //app is currently active, can update badges count here
    } else if (application.applicationState == UIApplicationStateBackground) {
        //app is in background, if content-available key of your notification is set to 1, poll to your backend to retrieve data and update your interface here
    } else if (application.applicationState == UIApplicationStateInactive) {
    }
    
    @try {
        NSString *web_link = [userInfo objectForKey:@"web_link"];
        NSLog(@"%@", web_link);
        NSURL *url = [[NSURL alloc] initWithString:web_link];
        if (url) {
            [[UIApplication sharedApplication] openURL:url];
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    // Called when a notification is delivered to a foreground app.
    NSLog(@"Userinfo %@",notification.request.content.userInfo);
    completionHandler(UNNotificationPresentationOptionAlert);
}

//---------------------------------------------------------------

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    
    //Called to let your app know which action was selected by the user for a given notification.
    DLog(@"Userinfo %@",response.notification.request.content.userInfo);
    DLog(@"Receive Remote Notification");
    UIApplication *application = [UIApplication sharedApplication];
    NSLog(@"Application state :%d", (int)application.applicationState);
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Reachability methods

//---------------------------------------------------------------

- (void)registerForNetworkReachabilityNotifications {
    // Initialize Reachability
    Reachability *reachability = [Reachability reachabilityWithHostName:@"www.google.com"];
    // Start Monitoring
    [reachability startNotifier];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
}

//---------------------------------------------------------------

- (void)unsubscribeFromNetworkReachabilityNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

//---------------------------------------------------------------

- (BOOL)isNetworkReachableViaWWAN {
    return ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == ReachableViaWWAN);
}

//---------------------------------------------------------------

- (void)reachabilityDidChange:(NSNotification *)notification {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    (internetStatus == NotReachable) ? [self networkStatus:NO] : [self networkStatus:YES];
}

//---------------------------------------------------------------

- (void) networkStatus:(BOOL)isAvailable {
    self.isNetworkAvailable = isAvailable;
    
    if (isAvailable) {
    } else {
        [Helper checkForConnection:self.window.rootViewController];
    }
}

//---------------------------------------------------------------

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    [self saveFilters];
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [self saveFilters];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self saveFilters];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) locationSetup {
    _locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
        [self.locationManager requestWhenInUseAuthorization];
    } else {
        [self.locationManager startUpdatingLocation];
    }
}

//---------------------------------------------------------------

- (void) getStoredFilters {
    _appfilter = [ServicePreference customObjectWithKey:KEY_APP_FILTERS];
    if (!self.appfilter) {
        _appfilter = [AppFilter new];
    }
}

//---------------------------------------------------------------

- (void) saveFilters {
    // Store filter object in preference
    [ServicePreference saveCustomObject:self.appfilter key:KEY_APP_FILTERS];
}

//---------------------------------------------------------------

- (UIViewController*) topMostController {
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    if (topController == nil) {
        topController = self.window.rootViewController;
    }
    return topController;
}

//---------------------------------------------------------------

- (void) autoLoginUser {
    if ([User signedIn]) {
        // Analytics
        [AnalyticsLogger logSignInEvent:[User signedInUser] withSuccess:@YES];

        MBProgressHUD *hud = [Helper showLoading:self.topMostController.view];
        hud.label.text = @"Loading...";
        
        // Move to next controller
        [ServiceManager getServerMappingKeyList:nil usingBlock:^(NSDictionary *keys) {
            [ServiceManager callWebServiceToGetTenantDetails:^(NSString *errorMessage) {
                // Hide indicator
                [Helper hideLoading:hud];
                [Helper hideLoading:self.globalHud];
                if (errorMessage == nil) {
                    [App_Delegate changeRootViewControllerTo:@"MainDashboard"];
                }
            }]; 
        }];
    }
}

//---------------------------------------------------------------

- (void) callServiceToPreloadMenus {
    [ServiceManager getAvailableMenu:^(NSArray *menus) {
        self.menuArray = menus;
        
        // Add static menus
        // My Profile
        Menu *profileMenu = [Menu new];
        profileMenu.url = MENU_MY_PROFILE;
        profileMenu.title = @"My Profile";
        
        // Logout
        Menu *logoutMenu = [Menu new];
        logoutMenu.url = MENU_LOGOUT;
        logoutMenu.title = @"Logout";
        
        NSMutableArray *newArray = [NSMutableArray arrayWithArray:self.menuArray];
        [newArray addObject:profileMenu];
        [newArray addObject:logoutMenu];
        self.menuArray = [NSArray arrayWithArray:newArray];
        newArray = nil;
    }];
}

//---------------------------------------------------------------

- (void) reloadDashboardData {
    // Call webservice to get industry details
    [ServiceManager callWebServiceToGetIndustryDetails];
    [App_Delegate changeRootViewControllerTo:@"MainDashboard"];
}

//---------------------------------------------------------------

- (void) changeRootViewControllerTo:(NSString *)viewControllerIdentifier {
    //    RevealViewController
    @try {
        UIViewController *controller = (UIViewController *)[self.mainStoryboard instantiateViewControllerWithIdentifier:viewControllerIdentifier];
        SlideNavigationController *slideMenuController = [[SlideNavigationController alloc] initWithRootViewController:controller];

        if ([viewControllerIdentifier isEqualToString:@"MainDashboard"]) {
            self.window.rootViewController = slideMenuController;
            RightMenuViewController *rightMenu = (RightMenuViewController*)[self.mainStoryboard
                                                                            instantiateViewControllerWithIdentifier: @"RightMenuViewController"];            
            slideMenuController.rightMenu = rightMenu;
            slideMenuController.portraitSlideOffset = self.window.bounds.size.width - 300;
            slideMenuController.menuRevealAnimationDuration = .18;
            slideMenuController.enableSwipeGesture = NO;
            slideMenuController.portraitSlideOffset = 50;
            MBProgressHUD *hud = [MBProgressHUD HUDForView:slideMenuController.view];
            [Helper hideLoading:hud];
        } else {
            MBProgressHUD *hud = [MBProgressHUD HUDForView:slideMenuController.view];
            [Helper hideLoading:hud];
            self.window.rootViewController = slideMenuController;
        }
    }
    @catch (NSException *exception) {
        DLog(@"exception :%@",exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void) changeTabBarControllerForTabIndex:(NSInteger)tabIndex {
    UITabBarController *tabController = (UITabBarController *)[self.mainStoryboard instantiateViewControllerWithIdentifier:@"MainDashboard"];
    tabController.selectedViewController = [tabController.viewControllers objectAtIndex:tabIndex];
}

//---------------------------------------------------------------

- (void) showVerificationScreen {
    //    VerificationViewController
    @try {
        //    SignInviewController
        UIViewController * signInController = [self.mainStoryboard instantiateViewControllerWithIdentifier:@"SignInTableViewController"];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:signInController];
        [self.window setRootViewController:navigationController];
        [signInController performSegueWithIdentifier:@"showVerificationPage" sender:self];
    }
    @catch (NSException *exception) {
        DLog(@"exception :%@",exception.debugDescription);
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Filter methods

//---------------------------------------------------------------

- (void) tenantChanged:(TenantRoleModel *)tenant {
    // Analytics
    [AnalyticsLogger logEventTenantChange:tenant.tenantName];
    
    // Clear old filter details
    [ServicePreference saveCustomObject:nil key:KEY_DASHBOARD_FILTERS];
    [ServicePreference saveCustomObject:nil key:KEY_GOAL_FILTERS];
    [ServicePreference saveCustomObject:nil key:KEY_OBSERVATION_FILTERS];
    [ServicePreference saveCustomObject:nil key:KEY_STORE_FILTERS_DASHBOARD];

    self.appfilter.tenantId = tenant.tenantId;
    self.appfilter.userRole = tenant.roleName;
    self.appfilter.tenantName = tenant.tenantName;
    self.appfilter.userRoleId = tenant.roleId;
    self.menuArray = nil;

    // Get user role
    [tenant getUserRole];

    // UPDATE FILTERS
    [self saveFilters];
    
    [ServiceManager callWebServiceToGetTenantDetails:^(NSString *errorMessage) {
        [Helper hideLoading:self.globalHud];
        // Call service to get users permissions
        [self reloadDashboardData];
        // Reload menus
        [self callServiceToPreloadMenus];
    }];
}

//---------------------------------------------------------------

- (void) industryChanged:(Industry *)industry {
    
    // Analytics
    [AnalyticsLogger logEventIndustryChange:industry.industryDescription];
    
    // Clear filter
    [ServicePreference saveCustomObject:nil key:KEY_DASHBOARD_FILTERS];
    [ServicePreference saveCustomObject:nil key:KEY_GOAL_FILTERS];
    [ServicePreference saveCustomObject:nil key:KEY_OBSERVATION_FILTERS];
    [ServicePreference saveCustomObject:nil key:KEY_STORE_FILTERS_DASHBOARD];
    
    // Clear tenant
    self.appfilter.tenantId = nil;
    self.appfilter.userRole = nil;
    self.appfilter.tenantName = nil;
    self.appfilter.userRoleId = nil;
    
    // Set new industry
    self.appfilter.industryId = industry.industryId;
    self.appfilter.industryDescription = industry.industryDescription;
    [ServicePreference setObject:industry.industryId.stringValue forKey:KEY_INDUSTRY_ID];
    
    // UPDATE FILTERS
    [self saveFilters];
    
    // Show loading indicator
    [self autoLoginUser];

//    [MBProgressHUD hideHUDForView:App_Delegate.topMostController.view animated:YES];
//    
//    // Get New tenant
//    MBProgressHUD *hud = [Helper showLoading:App_Delegate.topMostController.view];
//    hud.label.text = @"Changing...";
//    [ServiceManager callWebServiceToGetTenantDetails:^(NSString *errorMessage) {
//        [Helper hideLoading:hud];
//        // Relogin with new setup
//        [self autoLoginUser];
//    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark  methods

//---------------------------------------------------------------

// this delegate is called when the app successfully finds your current location
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLGeocoder *geocoder = [CLGeocoder new];
    [geocoder reverseGeocodeLocation:newLocation
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       CLPlacemark *placemark = [placemarks objectAtIndex:0];
                       NSLog(@"placemark %@",placemark);
                       //String to hold address
                       NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                       NSLog(@"addressDictionary %@", placemark.addressDictionary);
                       
                       NSLog(@"name: %@",placemark.name);
                       NSLog(@"street: %@",placemark.thoroughfare);
                       NSLog(@"sub-street: %@",placemark.subThoroughfare);
                       NSLog(@"locality: %@",placemark.locality);  // Extract the city name
                       NSLog(@"subLocality: %@",placemark.subLocality);
                       NSLog(@"administrativeArea: %@",placemark.administrativeArea);
                       NSLog(@"postalCode: %@",placemark.postalCode);
                       NSLog(@"ISOcountryCode: %@",placemark.ISOcountryCode);
                       NSLog(@"country: %@",placemark.country);  // Give Country Name
                       NSLog(@"inlandWater: %@",placemark.inlandWater);
                       NSLog(@"areasOfInterest: %@",placemark.areasOfInterest);
                       NSLog(@"ocean: %@",placemark.ocean);
                       NSLog(@"location: %@",placemark.location);
                       
                       //Print the location to console
                       NSLog(@"I am currently at %@",locatedAt);
                       //                  _City.text=[placemark.addressDictionary objectForKey:@"City"];
                       //                  [locationManager stopUpdatingLocation];
                   }
     
     ];
}

//---------------------------------------------------------------

// this delegate method is called if an error occurs in locating your current location
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"locationManager:%@ didFailWithError:%@", manager, error);
}

//---------------------------------------------------------------

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            // do some error handling
        }
            break;
        default:{
            [self.locationManager startUpdatingLocation];
        }
            break;
    }
}

//---------------------------------------------------------------

@end
