//
//  AppManager.h
//  IN-Gauge
//
//  Created by Mehul Patel on 08/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppManager : NSObject

@property (nonatomic, strong) NSArray           *tenantUsers;

@end
