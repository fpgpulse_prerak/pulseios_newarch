//
//  BaseTableViewController.m
//  InGauge
//
//  Created by Mehul Patel on 08/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "BaseTableViewController.h"

@interface BaseTableViewController ()

@end

@implementation BaseTableViewController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------


//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------


//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
}

//---------------------------------------------------------------

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setBarTintColor:APP_RED_COLOR];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:
     @{
        NSForegroundColorAttributeName:[UIColor whiteColor],
        NSFontAttributeName:[UIFont fontWithName:AVENIR_FONT size:15]
       }
     ];
}

//---------------------------------------------------------------

@end
