//
//  BaseViewController.h
//  InGauge
//
//  Created by Mehul Patel on 08/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"

@interface BaseViewController : UIViewController

@end
