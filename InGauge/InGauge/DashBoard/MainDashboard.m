//
//  MainDashboard.m
//  InGauge
//
//  Created by Mehul Patel on 26/04/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "MainDashboard.h"
#import "Helper.h"

@interface MainDashboard ()

@end

@implementation MainDashboard

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setHidden:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
