//
//  AnalyticsLogger.h
//  IN-Gauge
//
//  Created by Mehul Patel on 22/08/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceManager.h"
#import "DashboardVC.h"
#import "FeedsViewController.h"
#import "GoalViewController.h"
#import "SurveyAgentController.h"
#import "QuestionAnswerController.h"
#import "SaveObservationController.h"
#import "MailViewController.h"
#import "ChartDetailsViewController.h"
#import "DashboardTableController.h"
#import "ShareViewController.h"

@interface AnalyticsLogger : NSObject

// Common
+ (void) logEventWithName:(NSString *)eventName;

// App-level
+ (void) logEventIndustryChange:(NSString *)industryName;
+ (void) logEventTenantChange:(NSString *)tenantName;

// SignUp
+ (void) logRegisterUserEvent:(User *)user withSuccess:(NSNumber *)isSuccess;

// Forgot password
+ (void) logForgotPasswordEvent:(NSString *)email;

// SignIn
+ (void) logSignInChangeIndustryEvent:(NSString *)industry;
+ (void) logSignInEvent:(User *)user withSuccess:(NSNumber *)isSuccess;

// Dashboard
+ (void) logDashboardFilterApplyEvent:(DashboardVC *)controller;
+ (void) logDashboardDateChangeEvent:(DashboardVC *)controller;
+ (void) logDashboardChartShareEvent:(ShareViewController *)controller reportId:(NSString *)reportId comment:(NSString *)comment;
+ (void) logDashboardSelectDashboardEvent:(DashboardVC *)controller;
+ (void) logDashboardShowReportEvent:(DashboardVC *)controller report:(CustomReport *)report;
+ (void) logDashboardTableShareEvent:(DashboardTableController *)controller reportId:(NSString *)reportId comment:(NSString *)comment;

// Feeds
+ (void) logFeedsLikeEvent:(NSString *)chartId isLike:(NSNumber *)isLike;
+ (void) logFeedsCommentEvent:(NSString *)chartId comment:(NSString *)comment;

// Goal Progress
+ (void) logGoalProgressDateChangeEvent:(GoalViewController *)controller;
+ (void) logGoalProgressFilterChangeEvent:(GoalViewController *)controller;

// Observation (Survey)
+ (void) logSurveyStartEvent:(SurveyAgentController *)controller;
+ (void) logSurveyAnswerSelectEvent:(QuestionAnswerController *)controller answer:(NSString *)answer;
+ (void) logSurveySaveEvent:(SaveObservationController *)controller;

// Mail
+ (void) logRemoveMailsEvent:(MailViewController *)controller mailId:(NSArray *)mailId;
+ (void) logEmailSentEvent:(NSDictionary *)details;
+ (void) logEmailFailedEvent:(NSDictionary *)details;
+ (void) logEmailAttachmentEvent:(NSDictionary *)details;
+ (void) logReplyMailEvent:(NSDictionary *)details;
+ (void) logFailedReplyMailEvent:(NSDictionary *)details;

// Preferences
+ (void) logPreferenceChangeIndustryEvent:(NSString *)industryName;
+ (void) logPreferenceChangeTenantEvent:(NSString *)tenantName;

// Signout
+ (void) logUserSignOutEvent;

@end
