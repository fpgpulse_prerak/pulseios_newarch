//
//  AnalyticsLogger.m
//  IN-Gauge
//
//  Created by Mehul Patel on 22/08/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

@import Firebase;

#import "AnalyticsLogger.h"

@implementation AnalyticsLogger

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

+ (NSDictionary *) addCommonParameter:(NSDictionary *)details {
    NSString *email = [self notNull:User.signedInUser.email];
    NSString *role = [self notNull:App_Delegate.appfilter.userRole];
    NSDictionary *commonDetails = @{@"email" : email, @"role" : role};
    
    if (!details) {
        return commonDetails;
    }
    
    // Add common params
    NSMutableDictionary *mDetails = [NSMutableDictionary dictionaryWithDictionary:details];
    [mDetails addEntriesFromDictionary:commonDetails];
    details = [NSDictionary dictionaryWithDictionary:mDetails];
    mDetails = nil;
    return details;
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark Application methods

//---------------------------------------------------------------

+ (void) logEventIndustryChange:(NSString *)industryName {
    NSDictionary *details = @{@"industry" : [self notNull:industryName]};
    [FIRAnalytics logEventWithName:@"industry_change" parameters:details];
}

//---------------------------------------------------------------

+ (void) logEventTenantChange:(NSString *)tenantName {
    NSDictionary *details = @{@"tenant" : [self notNull:tenantName]};
    [FIRAnalytics logEventWithName:@"tenant_change" parameters:details];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark SignUp analytics methods

//---------------------------------------------------------------

+ (void) logEventWithName:(NSString *)eventName {
    // FireBase - Analytics
    [FIRAnalytics logEventWithName:eventName parameters:[self addCommonParameter:nil]];
}

//---------------------------------------------------------------

+ (void) logRegisterUserEvent:(User *)user withSuccess:(NSNumber *)isSuccess {
    [FIRAnalytics logEventWithName:@"register_new_user" parameters:@{ @"email": [self notNull:user.email], @"success": isSuccess}];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark ForgotPassword analytics methods

//---------------------------------------------------------------

+ (void) logForgotPasswordEvent:(NSString *)email {
    NSDictionary *details = @{@"email" : [self notNull:email]};
    [FIRAnalytics logEventWithName:@"get_password" parameters:details];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark SignIn methods

//---------------------------------------------------------------

+ (void) logSignInChangeIndustryEvent:(NSString *)industry {
    NSDictionary *details = @{@"industry": [self notNull:industry]};
    [FIRAnalytics logEventWithName:@"change_indsutry" parameters:[self addCommonParameter:details]];
}

//---------------------------------------------------------------

+ (void) logSignInEvent:(User *)user withSuccess:(NSNumber *)isSuccess {
    BOOL isHotel = [Helper isHotelDomain];
    NSString *industry = isHotel ? @"Hospitality" : @"Car Rental";
    [FIRAnalytics logEventWithName:@"sign_in" parameters:@{
                                                           @"email"     : [self notNull:user.email],
                                                           @"industry"  : industry,
                                                           @"role"      : [self notNull:App_Delegate.appfilter.userRole],
                                                           @"success"   : isSuccess,
                                                           @"name"      : [self notNull:user.firstName]}];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Dashboard analytics methods

//---------------------------------------------------------------

+ (void) logDashboardFilterApplyEvent:(DashboardVC *)controller {
    Filters *filter = controller.filter;
    NSDictionary *details = @{
                                     @"dashboard"       : [self notNull:controller.selectedDashboard.name],
                                     @"geography_type"  : [self notNull:filter.regionTypeName],
                                     @"region"          : [self notNull:filter.regionName],
                                     @"location"        : [self notNull:filter.locationName],
                                     @"location_group"  : [self notNull:filter.locationGroupName],
                                     @"product"         : [self notNull:filter.productName],
                                     @"user"            : [self notNull:filter.userName],
                                     @"metric_type"     : [self notNull:filter.metricTypeName]
                            };
    // Add common params
    [FIRAnalytics logEventWithName:@"change_dashboard_filter" parameters:[self addCommonParameter:details]];
}

//---------------------------------------------------------------

+ (void) logDashboardDateChangeEvent:(DashboardVC *)controller {
    Filters *filter = controller.filter;
    NSString *startDateString = [ServicePreference objectForKey:KStartDate];
    NSString *endDateString = [ServicePreference objectForKey:KEndDate];
    NSString *compStartDateString = [ServicePreference objectForKey:KComStartDate];
    NSString *compEndDateString = [ServicePreference objectForKey:KComEndDate];
    
    NSDictionary *details = @{
                                    @"dashboard"            : [self notNull:controller.selectedDashboard.name],
                                    @"location"             : [self notNull:filter.locationName],
                                    @"start_date"           : [self notNull:startDateString],
                                    @"end_date"             : [self notNull:endDateString],
                                    @"compare_start_date"   : [self notNull:compStartDateString],
                                    @"compare_end_date"     : [self notNull:compEndDateString]
                              };
    [FIRAnalytics logEventWithName:@"change_dashboard_date" parameters:[self addCommonParameter:details]];
}

//---------------------------------------------------------------

+ (void) logDashboardChartShareEvent:(ShareViewController *)controller reportId:(NSString *)reportId comment:(NSString *)comment {
    Filters *filter = controller.filter;
    NSDictionary *details = @{
                                    @"dashboard"    : [self notNull:controller.dashboardName],
                                    @"location"     : [self notNull:filter.locationName],
                                    @"report_id"    : [self notNull:reportId],
                                    @"comment"      : [self notNull:comment]
                              };
    
    [FIRAnalytics logEventWithName:@"share_report" parameters:[self addCommonParameter:details]];
}

//---------------------------------------------------------------

+ (void) logDashboardTableShareEvent:(DashboardTableController *)controller reportId:(NSString *)reportId comment:(NSString *)comment {
    Filters *filter = controller.filter;
    NSDictionary *details = @{
                              @"dashboard"    : [self notNull:controller.dashboardName],
                              @"location"     : [self notNull:filter.locationName],
                              @"report_id"    : reportId,
                              @"comment"      : [self notNull:comment]
                              };
    
    [FIRAnalytics logEventWithName:@"share_report" parameters:[self addCommonParameter:details]];
}

//---------------------------------------------------------------

+ (void) logDashboardSelectDashboardEvent:(DashboardVC *)controller {
    Filters *filter = controller.filter;
    NSDictionary *details = @{
                                    @"dashboard"    : [self notNull:controller.selectedDashboard.name],
                                    @"location"     : [self notNull:filter.locationName],
                              };
    
    [FIRAnalytics logEventWithName:@"change_dashboard" parameters:[self addCommonParameter:details]];
}


//---------------------------------------------------------------

+ (void) logDashboardShowReportEvent:(DashboardVC *)controller report:(CustomReport *)report {
    Filters *filter = controller.filter;
    NSString *reportId = report.customReportId ? report.customReportId.stringValue : @"";
    
    NSDictionary *details = @{
                                    @"location"     : [self notNull:filter.locationName],
                                    @"report_id"    : reportId,
                                    @"dashboard"    : [self notNull:controller.selectedDashboard.name],
                                    @"report_name"  : [self notNull:report.reportName],
                                    @"entity_name"  : [self notNull:report.entityName]
                              };
    
    [FIRAnalytics logEventWithName:@"show_report" parameters:[self addCommonParameter:details]];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Feeds events methods

//---------------------------------------------------------------

+ (void) logFeedsLikeEvent:(NSString *)chartId isLike:(NSNumber *)isLike {
    NSDictionary *details = @{@"report_id" : chartId, @"isLike" : isLike};
    [FIRAnalytics logEventWithName:@"like_feed" parameters:[self addCommonParameter:details]];
}

//---------------------------------------------------------------

+ (void) logFeedsCommentEvent:(NSString *)chartId comment:(NSString *)comment {
    NSDictionary *details = @{@"report_id" : [self notNull:chartId], @"comment" : [self notNull:comment]};
    [FIRAnalytics logEventWithName:@"comment_feed" parameters:[self addCommonParameter:details]];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark GoalProgress event methods

//---------------------------------------------------------------

+ (void) logGoalProgressDateChangeEvent:(GoalViewController *)controller {
    Filters *filter = controller.filter;
    NSString *month = filter.month ? [Helper getMonthNameFromMonthNumber:filter.month.integerValue] : @"";
    NSString *year = filter.year ? filter.year.stringValue : @"";
    
    NSDictionary *details = @{
                                    @"location" : [self notNull:filter.locationName],
                                    @"user"     : [self notNull:filter.userName],
                                    @"month"    : month,
                                    @"year"     : year
                              };
    
    [FIRAnalytics logEventWithName:@"change_goal_date" parameters:[self addCommonParameter:details]];
}

//---------------------------------------------------------------

+ (void) logGoalProgressFilterChangeEvent:(GoalViewController *)controller {
    Filters *filter = controller.filter;
    NSDictionary *details = @{
                                    @"location"         : [self notNull:filter.locationName],
                                    @"location_group"   : [self notNull:filter.locationGroupName],
                                    @"product"          : [self notNull:filter.productName],
                                    @"user"             : [self notNull:filter.userName],
                              };
    
    // Add common params
    [FIRAnalytics logEventWithName:@"change_goal_filter" parameters:[self addCommonParameter:details]];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Observation event methods

//---------------------------------------------------------------

+ (void) logSurveyStartEvent:(SurveyAgentController *)controller {
    Filters *filter = controller.filter;
    NSString *surveyType = @"";
    surveyType = (controller.surveyType == OBSERVATION) ? [App_Delegate.keyMapping valueForKey:@"KEY_OB_DASHBOARD_TITLE"] : [App_Delegate.keyMapping valueForKey:@"KEY_CC_DASHBOARD_TITLE"];

    NSDictionary *details = @{
                                    @"location"     : [self notNull:filter.locationName],
                                    @"surveyor"     : [self notNull:controller.performanceLeader],
                                    @"respondent"   : [self notNull:filter.userName],
                                    @"survey_entity": [self notNull:filter.surveyEntityTypeString],
                                    @"survey_type"  : surveyType,
                                    @"date"         : [self notNull:controller.startDateString]
                              };
    
    // Add common params
    [FIRAnalytics logEventWithName:@"survey_start" parameters:[self addCommonParameter:details]];
}

//---------------------------------------------------------------

+ (void) logSurveyAnswerSelectEvent:(QuestionAnswerController *)controller answer:(NSString *)answer {
    Filters *filter = controller.filter;
    NSString *surveyType = @"";
    surveyType = (controller.dataManager.surveyType == OBSERVATION) ? [App_Delegate.keyMapping valueForKey:@"KEY_OB_DASHBOARD_TITLE"] : [App_Delegate.keyMapping valueForKey:@"KEY_CC_DASHBOARD_TITLE"];

    NSDictionary *details = @{
                                    @"location"     : [self notNull:filter.locationName],
                                    @"respondent"   : [self notNull:filter.userName],
                                    @"surveyor"     : [self notNull:filter.performanceLeaderId.stringValue],
                                    @"survey_entity": [self notNull:filter.surveyEntityTypeString],
                                    @"survey_type"  : surveyType,
                                    @"section"      : [self notNull:controller.sectionText],
                                    @"question"     : [self notNull:controller.question.questionText],
                                    @"answer"       : [self notNull:answer]
                              };
    [FIRAnalytics logEventWithName:@"select_answer" parameters:[self addCommonParameter:details]];
}

//---------------------------------------------------------------

+ (void) logSurveySaveEvent:(SaveObservationController *)controller {
    Filters *filter = controller.filter;
    Event *event = controller.dataManager.event;
    NSString *surveyType = @"";
    surveyType = (controller.dataManager.surveyType == OBSERVATION) ? [App_Delegate.keyMapping valueForKey:@"KEY_OB_DASHBOARD_TITLE"] : [App_Delegate.keyMapping valueForKey:@"KEY_CC_DASHBOARD_TITLE"];
    
    NSDictionary *details = @{
                                    @"location"     : [self notNull:event.tenantLocationId.stringValue],
                                    @"respondent"   : [self notNull:event.respondentId.stringValue],
                                    @"surveyor"     : [self notNull:event.surveyorId.stringValue],
                                    @"survey_entity": [self notNull:filter.surveyEntityType],
                                    @"survey_type"  : surveyType,
                                    @"note"         : [self notNull:event.note],
                                    @"start_date"   : [self notNull:event.startDate],
                                    @"end_date"     : [self notNull:event.endDate],
                              };
    
    [FIRAnalytics logEventWithName:@"save_survey" parameters:[self addCommonParameter:details]];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Mail event methods

//---------------------------------------------------------------

+ (void) logRemoveMailsEvent:(MailViewController *)controller mailId:(NSArray *)mailId {
    NSDictionary *details = @{
                                    @"remove_ids" : mailId,
                              };
    [FIRAnalytics logEventWithName:@"remove_mails" parameters:[self addCommonParameter:details]];
}

//---------------------------------------------------------------

+ (void) logEmailSentEvent:(NSDictionary *)details {
    [FIRAnalytics logEventWithName:@"send_mail" parameters:[self addCommonParameter:details]];
}

//---------------------------------------------------------------

+ (void) logEmailFailedEvent:(NSDictionary *)details {
    [FIRAnalytics logEventWithName:@"send_mail_failed" parameters:[self addCommonParameter:details]];
}

//---------------------------------------------------------------

+ (void) logEmailAttachmentEvent:(NSDictionary *)details {
    [FIRAnalytics logEventWithName:@"mail_attachment" parameters:[self addCommonParameter:details]];
}

//---------------------------------------------------------------

+ (void) logReplyMailEvent:(NSDictionary *)details {
    [FIRAnalytics logEventWithName:@"reply_mail" parameters:[self addCommonParameter:details]];
}

//---------------------------------------------------------------

+ (void) logFailedReplyMailEvent:(NSDictionary *)details {
    [FIRAnalytics logEventWithName:@"reply_mail_failed" parameters:[self addCommonParameter:details]];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Preference event methods

//---------------------------------------------------------------

+ (void) logPreferenceChangeIndustryEvent:(NSString *)industryName {
    NSDictionary *details = @{@"industry_name" : [self notNull:industryName]};
    [FIRAnalytics logEventWithName:@"industry_change" parameters:[self addCommonParameter:details]];
}

//---------------------------------------------------------------

+ (void) logPreferenceChangeTenantEvent:(NSString *)tenantName {
    NSDictionary *details = @{ @"tenant_name" : [self notNull:tenantName]};
    [FIRAnalytics logEventWithName:@"tenant_change" parameters:[self addCommonParameter:details]];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark SignOut methods

//---------------------------------------------------------------

+ (void) logUserSignOutEvent {
    [FIRAnalytics logEventWithName:@"sign_out" parameters:[self addCommonParameter:nil]];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark  methods

//---------------------------------------------------------------

+ (NSString *) notNull:(NSString *)string {
    return string ? string : @"";
}

//---------------------------------------------------------------

@end
