//
//  Constant.h
//  InGauge
//
//  Created by Mehul Patel on 24/04/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#ifndef Constant_h
#define Constant_h

#import "AppDelegate.h"

#define App_Delegate  ((AppDelegate *)[[UIApplication sharedApplication] delegate])

#ifdef DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define DLog(...)
#endif

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

typedef enum : NSUInteger {
    NO_SURVEY,
    PENDING_EVENT,
    OB_PENDING_EVENT_VIEW,
    OB_COMPLETED_EVENT,
    OB_COMPLETED_EVENT_VIEW,
    OB_NEW_EVENT
} EVENT_TYPE;


typedef enum : NSUInteger {
    COMPLETED,
    PENDING,
    CANCELLED
} SURVEY_STATUS;

typedef enum : NSUInteger {
    OBSERVATION,
    COACHING
} SURVEY_TYPE;

#define COMMENT_MAX_LENGTH 200

// Fonts
#define AVENIR_FONT                     @"Avenir"
#define AVENIR_FONT_HEAVY               @"Avenir-Heavy" //Avenir-Heavy
#define AVENIR_FONT_LIGHT               @"Avenir-Light"
#define AVENIR_NEXT_FONT                @"AvenirNext-Regular"
#define AVENIR_NEXT_DEMI_BOLD_FONT      @"AvenirNext-DemiBold"
#define AVENIR_NEXT_BOLD_FONT           @"AvenirNext-Bold"

//App Constant
#define HOTEL_INDUSTRY_MARKER           @"HOTEL"
#define CAR_INDUSTRY_MARKER             @"CARRENTAL"
#define SEAWORLD_INDUSTRY_MARKER        @"THEMEPARK"
#define KEY_TITLE                       @"title"
#define KEY_VALUE                       @"value"

#define KEY_DEVICE_ID                   @"keyDeviceId"
#define FP_CONST_ERRORS_KEY             @"errors"
#define FP_CONST_BODY_KEY               @"body"
#define FP_CONST_PLATFORM               @"platform"
#define DEFAULT_LOCATION                @"DEFAULT_LOCATION"
#define DEFAULT_MONTH                   @"DEFAULT_MONTH"
#define DEFAULT_YEAR                    @"DEFAULT_YEAR"
#define DEFAULT_AGENT_EMPLOYEE          @"DEFAULT_AGENT_EMPLOYEE"
#define DEFAULT_START_DATE              @"DEFAULT_START_DATE"
#define DEFAULT_END_DATE                @"DEFAULT_END_DATE"
#define DEFAULT_COMPARE_START_DATE      @"DEFAULT_COMPARE_START_DATE"
#define DEFAULT_COMPARE_END_DATE        @"DEFAULT_COMPARE_END_DATE"
#define DEFAULT_SHOW_COUNTER_COACHING_ALERT @"DEFAULT_SHOW_COUNTER_COACHING_ALERT"

#define KStartDate                      @"StartDate"
#define KEndDate                        @"EndDate"
#define KComStartDate                   @"ComStartDate"
#define KComEndDate                     @"ComEndDate"
#define KComSwitch                      @"ComSwitch"
#define KEY_TITLE                       @"title"
#define KEY_VALUE                       @"value"
#define KEY_DATE_FROM                   @"from"
#define KEY_DATE_TO                     @"to"
#define KEY_DATE_COMP_FROM              @"compFrom"
#define KEY_DATE_COMP_TO                @"compTo"

#define SURVEY_START_DATE               @"SurveyStartDate"
#define SURVEY_END_DATE                 @"SurveyEndDate"
#define SURVEY_COMP_START_DATE          @"SurveyCompStartDate"
#define SURVEY_COMP_END_DATE            @"SurveyCompEndDate"
#define SURVEY_COMP_SWITCH              @"SurveyCompSwitch"

#define KEY_ORDER_BY_VALUE              @"name"
#define KEY_SORT_BY_ASC_VALUE           @"ASC"
#define KEY_SORT_BY_DES_VALUE           @"DESC"
#define KEY_MAIL_TYPE_INBOX             @"inbox"
#define KEY_MAIL_TYPE_SENT              @"sent"
#define KEY_MAIL_TYPE_TRASH             @"trash"

#define KEY_PL_ROLE                     @"isPerformanceManager"
#define KEY_SYSTEM_ADMIN_ROLE           @"isSuperAdmin"
#define KEY_FRONTLINE_ASSOCIATE_ROLE    @"isFrontLineAssociate"

// Date Constant
#define SERVICE_DATE_TIME_FORMAT        @"yyyy-MM-dd HH:mm:ss"
#define SERVICE_DATE_TIME_SHORT_FORMAT  @"yyyy-MM-dd"
#define APP_DISPLAY_DATE_TIME_FORMAT    @"MMM dd, yyyy"

#define OB_DATE_FORMAT                  @"dd-MMM-yyyy hh:mm:ss a"
#define MAIL_DATE_TIME_FORMAT           @"dd-MMM-yyyy hh:mm:ss a"

// Color
// 46 55 68
#define APP_COLOR                       [UIColor colorWithRed:46/255.0f green:55/255.0f blue:68/255.0f alpha:1]
//#define APP_COLOR                       [UIColor colorWithRed:53/255.0f green:64/255.0f blue:81/255.0f alpha:1]
#define BACK_COLOR                      [UIColor colorWithRed:243/255.0f green:243/255.0f blue:243/255.0f alpha:1]

//(44,62,80)
#define FP_ORANGE_COLOR                 [UIColor colorWithRed:211.0/255.0f green:86.0/255.0f blue:28.0/255.0f alpha:1]
#define DEFAULT_BLUE_COLOR              [UIColor colorWithRed:0/255.0f green:122/255.0f blue:255/255.0f alpha:1]
#define FP_BORDER_COLOR                 [UIColor colorWithRed:199/255.0f green:65/255.0f blue:23/255.0f alpha:1]
#define CARD_COLOR                      [UIColor colorWithRed:23.0/255.0 green:29.0/255.0 blue:36.0/255.0 alpha:1.0]
#define FEED_CARD_COLOR                 [UIColor colorWithRed:33/255.0f green:40/255.0f blue:48/255.0f alpha:1.0]
#define GRAPHITE_SEPARATOR              [UIColor colorWithRed:48/255.0 green:53/255.0 blue:54/255.0 alpha:1]
#define SKY_BLUE_COLOR                  [UIColor colorWithRed:89/255.0 green:191/255.0 blue:200/255.0 alpha:1.0]
#define BLUE_NEON_DARK_COLOR            [UIColor colorWithRed:94/255.0f green:242/255.0f blue:253/255.0f alpha:1]
#define BLUE_NEON_LIGHT_COLOR           [UIColor colorWithRed:54/255.0f green:90/255.0f blue:110/255.0f alpha:0.2]
#define OB_ORANGE_COLOR                 [UIColor colorWithRed:252/255.0f green:179/255.0f blue:34/255.0f alpha:1]
#define OB_GREEN_COLOR                  [UIColor colorWithRed:60/255.0f green:192/255.0f blue:81/255.0f alpha:1]
#define OB_GREY_COLOR                   [UIColor colorWithRed:127/255.0f green:127/255.0f blue:127/255.0f alpha:1]
#define OB_DARK_RED                     [UIColor colorWithRed:151/255.0f green:32/255.0f blue:33/255.0f alpha:1]
#define OB_WHITE_CREAM                  [UIColor colorWithRed:249/255.0f green:249/255.0f blue:249/255.0f alpha:1]
#define TABLE_SEPERATOR_COLOR           [UIColor colorWithRed:235/255.0f green:235/255.0f blue:241/255.0f alpha:1]
#define MATERIAL_PINK_COLOR             [UIColor colorWithRed:242/255.0f green:24/255.0f blue:62/255.0f alpha:1]
#define LIGHT_BLUE_COLOR                [UIColor colorWithRed:20/255.0f green:150/255.0f blue:239/255.0f alpha:1]
#define SOCIAL_BUTTON_COLOR             [UIColor colorWithWhite:0.5 alpha:1]
#define GREEN_COLOR                     [UIColor colorWithRed:63/255.0f green:164/255.0f blue:62/255.0f alpha:1]

//Image constants
#define GREEN_ARROW_NAME                @"arrow_green"
#define RED_ARROW_NAME                  @"arrow_red"
#define DOWN_ARROW                      @"down-arrow"
#define UP_ARROW                        @"up-arrow"
#define RIGHT_ARROW                     @"right-arrow"
#define LEFT_ARROW                      @"left-arrow"
#define LIKE_IMAGE                      @"like"
#define LIKED_IMAGE                     @"liked"
#define COMMENT_IMAGE                   @"comment"
#define RADIO_ON_IMAGE                  @"radio_on"
#define RADIO_OFF_IMAGE                 @"radio_off"
#define TABLE_IMAGE                     @"table"
#define BAR_CHART_IMAGE                 @"bar-chart"
#define PIE_CHART_IMAGE                 @"pie-chart"
#define BUBBLE_CHART_IMAGE              @"bubble"
#define SPLINE_CHART_IMAGE              @"spline"
#define SHARE_ICON_IMAGE                @"share"
#define COMPOSE_MAIL_IMAGE              @"compose_mail"
#define SEND_IMAGE                      @"send-button"
#define TRASH_IMAGE                     @"remove"

#define KEY_APP_FILTERS                 @"AppFilters"
#define KEY_DASHBOARD_FILTERS           @"DashboardFilters"
#define KEY_GOAL_FILTERS                @"GoalFilters"
#define KEY_OBSERVATION_FILTERS         @"ObservationFilters"

// Dashboard Filters
#define KEY_STORE_FILTERS_DASHBOARD     @"StoredDashboardFilters"
//#define KEY_STORE_LOCATION              @"StoredLocation"
//#define KEY_STORE_LOCATION_GROUP        @"StoredLocationGroup"
//#define KEY_STORE_METRIC_TYPE           @"StoredMetricType"

#define KEY_ACCESS_TOKEN                @"Authorization"
#define KEY_INDUSTRY_ID                 @"industryId"
#define KEY_INDUSTRY_ID_VALUE           @"1"
#define KEY_PLATFORM                    @"Platform"
#define KEY_PLATFORM_VALUE              @"0"
#define KEY_ISMOBILE                    @"isMobile"
#define KEY_ISMOBILE_VALUE              @"true"
#define KEY_TIME_ZONE                   @"TimeZone"

#define kNotificationUpdateScoreOnCoaching  @"NotificationUpdateScore"
#define kUpdateObservationDashboard         @"UpdateObservationDashboard"
#define kBackToDashboardNotification        @"BackToDashboardNotification"
#define kReloadMenuNotification             @"ReloadMenuNotification"
#define kMenuUpdateMailCountNotification    @"MenuUpdateMailCountNotification"

// Survey
#define KEY_OBSERVATION_STORYBOARD_NAME @"Survey"
#define KEY_PARAM_CONST_OBSERVATION     @"Observation"
#define KEY_PARAM_CONST_COACHING        @"CounterCoaching"
#define KEY_PARAM_CONST_COMPLETED       @"Completed"
#define KEY_PARAM_CONST_ACTIVE_STATUS   @"Normal"
#define KEY_PARAM_CONST_SURVEY          @"SURVEY"

#define ANSWER_YES                      [App_Delegate.keyMapping getKeyValue:@"KEY_OB_ANS_YES"]
#define ANSWER_NO                       [App_Delegate.keyMapping getKeyValue:@"KEY_OB_ANS_NO"]
#define ANSWER_NA                       [App_Delegate.keyMapping getKeyValue:@"KEY_OB_ANS_NA"]

// Dashboard
#define KEY_DASHBOARD_TYPE              @"type"
#define KEY_DASHBOARD_LIST              @"list"
#define DBTYPE_1_TITLE                  [App_Delegate.keyMapping getKeyValue:@"KEY_DASHBOARD"]
#define DBTYPE_2_TITLE                  [App_Delegate.keyMapping getKeyValue:@"KEY_OB_TYPE2"]
#define DBTYPE_3_TITLE                  [App_Delegate.keyMapping getKeyValue:@"KEY_OB_TYPE3"]
#define KEY_BLOCK_CHART                 @"Block"
#define KEY_TABLE_CHART                 @"Table"
#define KEY_COLUMN_CHART                @"column"
#define KEY_PIE_CHART                   @"pie"
#define KEY_BUBBLE_CHART                @"bubble"
#define KEY_SPLINE_CHART                @"spline"

//menu constants
#define MENU_MY_PROFILE                 @"menu/myprofile"
#define MENU_PREFERENCES                @"menu/preferences"
#define MENU_LOGOUT                     @"menu/logout"
#define MENU_SELECT_GOAL_PROGRESS       @"/goal/goalProgress"
#define MENU_SELECT_COACHING            @"/survey/counterCoaching"
#define MENU_TAKE_COACHING              @"/survey/counterCoaching/add"
#define MENU_COACHING_DASHBOARD         @"/survey/counterCoaching/dashboard"
#define MENU_SELECT_OBSERVATION         @"/survey/observation"
#define MENU_TAKE_OBSERVATION           @"/survey/observation/add"
#define MENU_OBSERVATION_DASHBOARD      @"/survey/observation/dashboard"
#define MENU_SELECT_MAIL                @"/in-gauge-mail/mail"
#define MENU_MAIL_INBOX                 @"/in-gauge-mail/mail/inbox"
#define MENU_MAIL_SENT                  @"/in-gauge-mail/mail/sent"
#define MENU_MAIL_TRASH                 @"/in-gauge-mail/mail/trash"

// Share
#define ENTITY_TYPE                     @"entityType"
#define ENTITY_ID                       @"entityId"
#define DATA_START_DATE                 @"dataStartDate"
#define DATA_END_DATE                   @"dataEndDate"
#define CONCAT_REPORT_NAME              @"concatWithReportName"

#endif /* Constant_h */


