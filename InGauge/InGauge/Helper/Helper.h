//
//  Helper.h
//  InGauge
//
//  Created by Mehul Patel on 24/04/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Constant.h"
#import "NSDate+LangExt.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "NSString+LangExt.h"
#import "NSObject+LangExt.h"
#import "User.h"
#import "HKJsonUtils.h"
#import <SMFloatingLabelTextField/SMFloatingLabelTextField.h>
#import "NSDictionary+MapKey.h"
#import "NSString+Contains.h"

@interface Helper : NSObject

typedef enum : NSUInteger {
    RegionTypeFilter = 0,
    RegionFilter = 1,
    LocationFilter = 2,
    LocationGroupFilter= 3,
    ProductFilter = 4,
    UserFilter = 5,
    MetricTypeFilter = 6,
    PerformanceLeaderFilter = 7,
    AgentFilter = 8,
    ObservationTypeFilter = 9,
    GreatJobFilter = 10,
    ObservationCategory = 11,
    ObservationStatus = 12,
    IndustryFilter = 13,
    TenantFilter = 14,
    GeographyType = 15
} FilterType;

+ (void) removeCookiesCachesFromDevice;
+ (BOOL) checkForConnection:(UIViewController *)controller;

// App methods
+ (void) setDeviceId:(NSString *)deviceId;
+ (NSString *) getCurrentDeviceId;
+ (void) applyMailControllerNavigationStyle;
+ (void) applyBorderSettings:(UIView *)view;
+ (NSString *) getApplicationVersion;
+ (NSString *) getApplicationBundleId;
+ (void) tapOnControlAnimation:(UIView *)control forScale:(CGFloat)scale forCompletionBlock:(void(^)(BOOL finished))block;
+ (void) applyTheme;
+ (NSString *) openFileFromBundle:(NSString *)fileName ofType:(NSString *)fileType;
+ (UIColor *)colorFromRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue;
+ (void) addTopLine:(UIView *)view withColor:(UIColor *)color;
+ (void) addBottomLine:(UIView *)view withColor:(UIColor *)color;
+ (UIView *) createNoAccessView:(UIView *)superView withMessage:(NSString *)message;
+ (NSString *) getMetricType:(NSString *)hotelType;
+ (NSString *) getMetricTypeActualValue:(NSString *)metricType;
+ (NSString *) documentDirectory;

// Loading methods
+ (MBProgressHUD *) showLoading:(UIView *)view;
+ (void) hideLoading:(MBProgressHUD *)hud;

//---------------------------------------
// String
//---------------------------------------
// Get local / region currency symbol
+ (NSString *) getLocalCurrencySymbol:(NSNumber *)number;

// Get formatter decimal string based on local region. e.g India - 20,000 US - 200,000
+ (NSString *) getDecimalString:(NSNumber *)numberValue;

// Removes zeros from decimal places, e.g 10.00 => 10
+ (NSString *) getDecimalWithRemovingExtraZero:(NSNumber *)numberValue;

// Get stored currency signs for different countries
+ (NSString *) getCurrencySign:(NSString *)currencyName;

// Remoev space and new line characters from string
+ (NSString *) removeWhiteSpaceAndNewLineCharacter:(NSString *)rawString;
+ (NSAttributedString *) getJustifyiedText:(NSString *)text;
+ (NSString *)decodeStringForHtmlCharacters:(NSString *)text;
+ (NSString *)platformString;
+ (NSString *) getStringFromMutableAttributedString:(NSString *)checkString;
+ (BOOL) validateEmail:(NSString *)strEmail;
+ (BOOL) validatePhone:(NSString *)strPhone;

// Get Number value from any string
+ (long) getNumbersFromString:(NSString*)string;

// Get count of characters in label
+ (NSInteger) getNumberOfCharactersInLabel:(UILabel *)label;

// Get count of truncated characters in label
+ (NSInteger) getNumberOfTruncatedCharactersInLabel:(UILabel *)label;

// Date
+ (NSString *) utcDateFromTimeInterval:(long long)dateTimeStamp;
+ (NSDate *) convertUTCTimeToLocalDate:(NSString *)dateString;
+ (NSDate *) lastDateOfCurrentMonth;
+ (NSDate *) lastDateOfMonth:(NSDate *)date;
+ (NSDate *) firstDateOfMonth:(NSDate *)date;
+ (NSDate *) getCurrentDateTimeInGMT;
+ (NSDate *) getDateFromMonth:(NSNumber *)month andYear:(NSNumber *)year;
+ (NSDate *) subtractSecondsFromDate:(NSDate *)date seconds:(NSInteger)seconds;

+ (NSString *) getCurrentDateInGMT;
+ (NSString *) getGMTTimeStampFromLocalDate:(NSDate *)localDate;
+ (NSString *) monthNameString:(NSDate *)date;
+ (NSString *) yearString:(NSDate *)date;
+ (NSString *) getMonthNameFromMonthNumber:(NSInteger)month;
+ (NSString *) convertLocalTimeToUTC:(NSDate *)date;

+ (NSInteger) getMonthFromDate:(NSDate *)date;
+ (NSInteger) getYearFromDate:(NSDate *)date;
+ (NSInteger) getDaysFromDate:(NSDate *)start toDate:(NSDate *)end;
+ (NSInteger) daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
+ (NSInteger) hoursBetweenDate:(NSDate *)fromDateTime andDateTime:(NSDate *)toDateTime;
+ (long long) utcTimeIntervalForDate:(NSDate *)date;
+ (BOOL) isDatesValid:(NSDate *)startDate endDate:(NSDate *)endDate;
+ (NSString *) getLocalTimeZone;

// Number
+ (double) validateInfinte:(double)value;

// Alert
+ (void) showAlertWithTitle:(NSString *)title message:(NSString *)message forController:(UIViewController *)controller;
+ (void) showAlertWithTitle:(NSString *)title message:(NSString *)message forController:(UIViewController *)controller withAccessoryView:(UIView *)accessoryView;

// JSON
+ (NSString *) getJSONString:(id)object;
+ (id) getObjectFromJSONString:(NSString *)jsonString;

//--------------------------------------------------------------------
//*_*_*_*_*_*_*_*_*_*_*_*_* USER PREFERENCES *_*_*_*_*_*_*_*_*_*_*_*_*
//--------------------------------------------------------------------

/* SERVER CONTENT KEYS */
+ (void) writeFileToDocumentDirectory:(NSString *)fileName andFileData:(NSDictionary *)details;
+ (NSDictionary *) readFileFromDocumentDirectory:(NSString *)fileName;

// Domain helper 
+ (void) loginToAdmin;
+ (void) loginToEuropCar;
+ (void) setDefaultDomain;
+ (BOOL) isHotelDomain;
+ (BOOL) isDomainChanged;

void runOnMainQueueWithoutDeadlocking(void (^block)(void));

@end
