//
//  Helper.h
//  InGauge
//
//  Created by Mehul Patel on 24/04/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//


#import "Helper.h"
#include <sys/types.h>
#include <sys/sysctl.h>
#include <math.h>
#import "Domain.h"
#import "ServiceMetaData.h"

@import WebKit;

@implementation Helper

//---------------------------------------------------------------

#pragma mark
#pragma mark Internet Connection methods

//---------------------------------------------------------------

+ (BOOL) checkForConnection:(UIViewController *)controller {
    if (App_Delegate.isNetworkAvailable) {
        return YES;
    } else {
        [self showAlertWithTitle:@"" message:@"No Internet Connection" forController:controller];
        return NO;
    }
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark  methods

//---------------------------------------------------------------

+ (MBProgressHUD *) showLoading:(UIView *)view {
    MBProgressHUD *hud  = [MBProgressHUD showHUDAddedTo:view animated:YES];
//    hud.contentColor = MATERIAL_PINK_COLOR;
//    hud.bezelView.backgroundColor = [UIColor whiteColor];
//    hud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
//    hud.backgroundView.color = [UIColor colorWithWhite:0 alpha:0.3f];
    return hud;
}

//---------------------------------------------------------------

+ (void) hideLoading:(MBProgressHUD *)hud {
    runOnMainQueueWithoutDeadlocking(^{
        [hud hideAnimated:YES];
    });
}

//---------------------------------------------------------------

#pragma mark
#pragma mark APP methods

//---------------------------------------------------------------

+ (void) removeCookiesCachesFromDevice {
    // Remove webView cache before doing anything
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
        // Optional data
        NSSet *websiteDataTypes = [NSSet setWithArray:@[
                                                        WKWebsiteDataTypeDiskCache,
                                                        WKWebsiteDataTypeOfflineWebApplicationCache,
                                                        WKWebsiteDataTypeMemoryCache,
                                                        WKWebsiteDataTypeLocalStorage,
                                                        WKWebsiteDataTypeCookies,
                                                        WKWebsiteDataTypeSessionStorage,
                                                        WKWebsiteDataTypeIndexedDBDatabases,
                                                        WKWebsiteDataTypeWebSQLDatabases
                                                        ]];
        // All kinds of data
        // Date from
        NSDate *dateFrom = [NSDate dateWithTimeIntervalSince1970:0];
        // Execute
        [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:websiteDataTypes modifiedSince:dateFrom completionHandler:^{
            // Done
        }];
    } else {
        // Remove the basic cache
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        // Delete system cookie store in the app
        NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        for (NSHTTPCookie *cookie in storage.cookies) {
            [storage deleteCookie:cookie];
        }
    }
}

//---------------------------------------------------------------

+ (void) setDeviceId:(NSString *)deviceId {
    [[NSUserDefaults standardUserDefaults] setObject:deviceId ? deviceId : nil forKey:KEY_DEVICE_ID];
}

//---------------------------------------------------------------

+ (NSString *) getCurrentDeviceId {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:KEY_DEVICE_ID]) {
        
        NSString *deviceId = [defaults objectForKey:KEY_DEVICE_ID];
        if (deviceId.length > 0) {
            return deviceId;
        } else {
            return @"";
        }
    } else {
        return @"";
    }
}

//---------------------------------------------------------------

+ (UIView *) createNoAccessView:(UIView *)superView withMessage:(NSString *)message {
    UIView *noAccessView = [[UIView alloc] init];
    [noAccessView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [superView addSubview:noAccessView];
    [noAccessView setHidden:YES];
    [noAccessView setBackgroundColor:[UIColor clearColor]];
    
    UILabel *label = [[UILabel alloc] init];
    label.font = [UIFont fontWithName:AVENIR_FONT_HEAVY size:14];
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByTruncatingTail;
    label.textAlignment = NSTextAlignmentCenter;
    [label setTranslatesAutoresizingMaskIntoConstraints:NO];
    [noAccessView addSubview:label];
    
    label.text = message;
    
    NSDictionary *views = @{@"noAccessView" : noAccessView, @"label" : label};
    [superView addConstraint:[NSLayoutConstraint constraintWithItem:noAccessView
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:superView
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1 constant:-24]];
    [superView addConstraint:[NSLayoutConstraint constraintWithItem:noAccessView
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:superView
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1 constant:0]];
    
    [superView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[noAccessView]|" options:0 metrics:nil views:views]];
    [noAccessView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[label]-20-|" options:0 metrics:nil views:views]];
    [noAccessView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[label]|" options:0 metrics:nil views:views]];
    
    return noAccessView;
}

//---------------------------------------------------------------

+ (void) applyBorderSettings:(UIView *)view {
    view.layer.cornerRadius = 0.0f;
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(1, 4);
    view.layer.shadowOpacity = 1;
    view.layer.shadowRadius = 1.0;
}

//---------------------------------------------------------------

+ (NSString *) getApplicationVersion {
    @try {
        return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return @"";
    }
}

//---------------------------------------------------------------

+ (NSString *) getApplicationBundleId {
    @try {
        return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return @"";
    }
}

//---------------------------------------------------------------

+ (void) tapOnControlAnimation:(UIView *)control forScale:(CGFloat)scale forCompletionBlock:(void(^)(BOOL finished))block {
    // Animation start
    control.transform = CGAffineTransformMakeScale(scale, scale);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        control.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        block(YES);
    }];
}

//---------------------------------------------------------------

+ (void) applyTheme {
    @try {
        id appearance = [UINavigationBar appearance];
        [appearance setTranslucent:YES];
        
        // Shadow is available from iOS 7.1
        NSShadow *shadow = [NSShadow new] ;
        //    [shadow setShadowColor:[UIColor clearColor]];
        
        [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
        [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
        
        
        NSDictionary *titleAttributes = @{
                                           NSForegroundColorAttributeName: [UIColor blackColor],
                                           NSShadowAttributeName: shadow,
                                           NSFontAttributeName: [UIFont fontWithName:AVENIR_FONT size:14]
                                           };
        [appearance setTitleTextAttributes:titleAttributes];
        
        /*
         导航栏按钮
         */
        appearance = [UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil];
        [appearance setTitleTextAttributes:@{
                                             NSFontAttributeName: [UIFont fontWithName:AVENIR_FONT size:14],
                                             NSShadowAttributeName: shadow,
                                             }
                                  forState:UIControlStateNormal];
    }
    @catch (NSException *exception) {
        DLog(@"exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

+ (NSString *) openFileFromBundle:(NSString *)fileName ofType:(NSString *)fileType {
    @try {
        NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:fileType];
        NSError *error = nil;
        NSData *jsonData = [NSData dataWithContentsOfFile:filePath options:kNilOptions error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        return (error == nil) ? jsonString : nil;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return nil;
    }
}

//---------------------------------------------------------------

+ (UIColor *)colorFromRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue {
    return [UIColor colorWithRed:red / 255.0 green:green / 255.0 blue:blue / 255.0 alpha:1.0];
}

//---------------------------------------------------------------

+ (void) addTopLine:(UIView *)view withColor:(UIColor *)color {
    // Add line
    UIView *lineView = [[UIView alloc] init];
    [lineView setBackgroundColor:color];
    [lineView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [view addSubview:lineView];
    
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[lineView]|" options: 0 metrics:nil views:@{@"lineView" : lineView}]];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[lineView(1)]" options: 0 metrics:nil views:@{@"lineView" : lineView}]];
}

//---------------------------------------------------------------

+ (void) addBottomLine:(UIView *)view withColor:(UIColor *)color {
    // Add line
    UIView *lineView = [[UIView alloc] init];
    [lineView setBackgroundColor:color];
    [lineView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [view addSubview:lineView];
    
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[lineView]|" options: 0 metrics:nil views:@{@"lineView" : lineView}]];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[lineView(1)]|" options: 0 metrics:nil views:@{@"lineView" : lineView}]];
}

//---------------------------------------------------------------

void runOnMainQueueWithoutDeadlocking(void (^block)(void)) {
    if ([NSThread isMainThread])
    {
        block();
    }
    else
    {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
}

//---------------------------------------------------------------

+ (NSString *) getMetricType:(NSString *)hotelType {
    NSString *displayString = [App_Delegate.keyMapping getKeyValue:@"KEY_METRIC_TYPE_ARRIVAL"];
    if ([hotelType isEqualToString:@"Arrival"]) {
        displayString = [App_Delegate.keyMapping getKeyValue:@"KEY_METRIC_TYPE_ARRIVAL"];
    } else if ([hotelType isEqualToString:@"Departure"]) {
        displayString = [App_Delegate.keyMapping getKeyValue:@"KEY_METRIC_TYPE_DEPARTURE"];
    } else if ([hotelType isEqualToString:@"Daily"]) {
        displayString = [App_Delegate.keyMapping getKeyValue:@"KEY_METRIC_TYPE_DAILY"];
    }
    return displayString;
}

//---------------------------------------------------------------

+ (NSString *) getMetricTypeActualValue:(NSString *)metricType {
    NSString *valueString = @"Departure";
    if ([metricType isEqualToString:[App_Delegate.keyMapping getKeyValue:@"KEY_METRIC_TYPE_ARRIVAL"]]) {
        valueString = @"Arrival";
    } else if ([metricType isEqualToString:[App_Delegate.keyMapping getKeyValue:@"KEY_METRIC_TYPE_DEPARTURE"]]) {
        valueString = @"Departure";
    } else if ([metricType isEqualToString:[App_Delegate.keyMapping getKeyValue:@"KEY_METRIC_TYPE_DAILY"]]) {
        valueString = @"Daily";
    }
    return valueString;
}

//---------------------------------------------------------------

+ (NSString *) documentDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject].path;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UINavigationBar methods

//---------------------------------------------------------------

+ (void) applyMailControllerNavigationStyle {
    @try {
        id appearance = [UINavigationBar appearance];
        
        //**** This must be required
        [appearance setTranslucent:NO];
        
        NSShadow *shadow = [NSShadow new];
        [shadow setShadowColor:[UIColor whiteColor]];
        
        NSDictionary *titleAttributes = @{
                                           NSForegroundColorAttributeName: [UIColor blackColor],
                                           NSShadowAttributeName: shadow,
                                           NSFontAttributeName: [UIFont fontWithName:AVENIR_FONT size:14]
                                           };
        [appearance setTitleTextAttributes:titleAttributes];
        
        appearance = [UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil];
        [appearance setTitleTextAttributes:@{
                                             NSFontAttributeName: [UIFont fontWithName:AVENIR_FONT size:14],
                                             NSForegroundColorAttributeName: [UIColor colorWithRed:0.8f green:0.0353f blue:0.0353f alpha:1],
                                             NSShadowAttributeName: shadow,
                                             }
                                  forState:UIControlStateNormal];
        [appearance setTintColor:[UIColor colorWithRed:0.8f green:0.0353f blue:0.0353f alpha:1]];
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark TEXT formatting methods

//---------------------------------------------------------------

+ (NSString *) getLocalCurrencySymbol:(NSNumber *)number {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    return [formatter stringFromNumber:number];
}

//---------------------------------------------------------------

+ (NSString *) getDecimalString:(NSNumber *)numberValue {
    @try {
        if (numberValue == nil) {
            return @"0.00";
        }
        
//        // Manually inserting zero
//        if (numberValue.floatValue == 0) {
//            return @"0";
//        }
        
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setRoundingMode:NSNumberFormatterRoundHalfEven];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
        [formatter setMaximumFractionDigits:2];
        [formatter setMinimumFractionDigits:2];
        
        NSString *decimalString = [formatter stringFromNumber:numberValue];
        
        // Check last two decimal places are zero if 'YES' then remove last two zeros
//        NSArray *arr = [decimalString componentsSeparatedByString:@"."];
//        int tempInt = [[arr lastObject] intValue];
//        
//        if (tempInt == 0) {
//            NSString *finalString = [NSString stringWithFormat:@"%@", [arr firstObject]];
//            decimalString = finalString;
//        }
        return decimalString;
    } @catch (NSException *exception) {
        DLog(@"exception :%@",exception.debugDescription);
        return [NSString stringWithFormat:@"%0.2f", numberValue.doubleValue];
    }
}

+ (NSString *) getDecimalWithRemovingExtraZero:(NSNumber *)numberValue {
    @try {
        if (numberValue == nil) {
            return @"0.00";
        }
        
        // Manually inserting zero
        if (numberValue.floatValue == 0) {
            return @"0";
        }
        
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setRoundingMode:NSNumberFormatterRoundHalfEven];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
        [formatter setMaximumFractionDigits:2];
        [formatter setMinimumFractionDigits:2];
        
        NSString *decimalString = [formatter stringFromNumber:numberValue];
        
//     Check last two decimal places are zero if 'YES' then remove last two zeros
        NSArray *arr = [decimalString componentsSeparatedByString:@"."];
        int tempInt = [[arr lastObject] intValue];

        if (tempInt == 0) {
            NSString *finalString = [NSString stringWithFormat:@"%@", [arr firstObject]];
            decimalString = finalString;
        }
        return decimalString;
    } @catch (NSException *exception) {
        DLog(@"exception :%@",exception.debugDescription);
        return [NSString stringWithFormat:@"%0.2f", numberValue.doubleValue];
    }
}

//---------------------------------------------------------------

+ (NSString *) getCurrencySign:(NSString *)currencyName {
    if (!currencyName) {
        return @"";
    }
    
    NSString *currencySign = currencyName;
    
    // US Dollar
    if ([currencyName isEqualToString:@"USD"]) {
        currencySign = @"$";
    }
    // EURO
    if ([currencyName isEqualToString:@"EUR"]) {
        currencySign = @"€";
    }
    // Pound
    if ([currencyName isEqualToString:@"GBP"]) {
        currencySign = @"£";
    }
    // INDIAN RUPEE
    if ([currencyName isEqualToString:@"INR"]) {
        currencySign = @"₹";
    }
    // Australian dollar
    if ([currencyName isEqualToString:@"AUD"]) {
        currencySign = @"$";
    }
    // Canadian dollar
    if ([currencyName isEqualToString:@"CAD"]) {
        currencySign = @"$";
    }
    // United Arab Emirates dirham
    if ([currencyName isEqualToString:@"AED"]) {
        currencySign = @"AED";
    }
    // Malaysian ringgit
    if ([currencyName isEqualToString:@"MYR"]) {
        currencySign = @"MYR";
    }
    // Swiss currency
    if ([currencyName isEqualToString:@"CHF"]) {
        currencySign = @"₣";
    }
    // Chinese Yuan / Japanease Yen
    if ([currencyName isEqualToString:@"CNY"]) {
        currencySign = @"¥";
    }
    // Thai Baht
    if ([currencyName isEqualToString:@"THB"]) {
        currencySign = @"฿";
    }
    // Brazilian currency
    if ([currencyName isEqualToString:@"BRL"]) {
        currencySign = @"BRL";
    }
    
    //UPDATE: Added new currency signs - 13 Feb 2017
    //  Maxico City Currency
    if ([currencyName isEqualToString:@"MXN"]) {
        currencySign = @"MXN";
    }
    //  Curacao City Currency
    if ([currencyName isEqualToString:@"ANG"]) {
        currencySign = @"ANG";
    }
    //  Curacao City Currency
    if ([currencyName isEqualToString:@"NLG"]) {
        currencySign = @"NLG";
    }
    return currencySign;
}

//---------------------------------------------------------------

+ (NSString *) removeWhiteSpaceAndNewLineCharacter:(NSString *)rawString {
    
    if (rawString == nil) {
        return @"";
    }
    rawString = [rawString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    // Remove "\t" (TAB CHARACHTER)
    rawString = [rawString stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    
    return rawString;
}

//---------------------------------------------------------------

+ (NSAttributedString *) getJustifyiedText:(NSString *)text {
    @try {
        NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
        paragraph.alignment = NSTextAlignmentJustified;
        
        NSDictionary *attrubutes = @{
                                     NSParagraphStyleAttributeName : paragraph,
                                     NSBaselineOffsetAttributeName : [NSNumber numberWithFloat:0]
                                     };
        return [[NSAttributedString alloc] initWithString:text attributes:attrubutes];
    } @catch (NSException *exception) {
        DLog(@"exception :%@", exception.debugDescription);
        return [[NSAttributedString alloc] initWithString:@""];
    }
}

//---------------------------------------------------------------

+ (NSString *)decodeStringForHtmlCharacters:(NSString *)text {
    NSData *stringData = [text dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary *options = @{ NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType,
                               NSCharacterEncodingDocumentAttribute :@(NSUTF8StringEncoding) };
    
    NSAttributedString *decodedString;
    decodedString = [[NSAttributedString alloc] initWithData:stringData
                                                     options:options
                                          documentAttributes:NULL
                                                       error:NULL];
    NSString *normalString = decodedString.string;
    decodedString = nil;
    stringData = nil;
    return normalString;
}

//---------------------------------------------------------------

+ (NSString *)platformString {
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithUTF8String:machine];
    free(machine);
    
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone 6s Plus";
    if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone 6s";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini (GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (GSM)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air (WiFi)";
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPad mini 2G (WiFi)";
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPad mini 2G (Cellular)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    return platform;
}

//---------------------------------------------------------------

+ (BOOL) validateEmail:(NSString *)strEmail {
    NSString *emailRegex = @"^[_A-Za-z0-9-\\'\\+]+(\\.[_A-Za-z0-9-\\'\\+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:strEmail];
}

//---------------------------------------------------------------

+ (BOOL) validatePhone:(NSString *)strPhone {
    NSString *phoneRegex = @"^+(?:[0-9] ?){6,14}[0-9]$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:strPhone];
}

//---------------------------------------------------------------

+ (NSString *) getStringFromMutableAttributedString:(NSString *)checkString {
    NSString *amountValueString = checkString;
    @try {
        if ([checkString isKindOfClass:[NSMutableAttributedString class]]) {
            NSMutableAttributedString *mutableString = (NSMutableAttributedString *)checkString;
            amountValueString = [mutableString string];
            amountValueString = [NSString stringWithFormat:@"%@", checkString];
            
            NSRange fullRange = NSMakeRange(0, checkString.length);
            NSAttributedString *attStr = [mutableString attributedSubstringFromRange:fullRange];
            NSDictionary *documentAttributes = @{NSDocumentTypeDocumentAttribute:NSPlainTextDocumentType};
            
            NSData *textData = [attStr dataFromRange:fullRange documentAttributes:documentAttributes error:NULL];
            amountValueString = [[NSString alloc] initWithData:textData encoding:NSUTF8StringEncoding];
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return amountValueString;
    }
    return amountValueString;
}

//---------------------------------------------------------------

+ (long) getNumbersFromString:(NSString*)string {
    NSArray *numberArray = [string componentsSeparatedByCharactersInSet:
                      [[NSCharacterSet decimalDigitCharacterSet] invertedSet]];
    NSString *returnString = [numberArray componentsJoinedByString:@""];
    return [returnString integerValue];
}

//---------------------------------------------------------------

+ (NSInteger) getNumberOfCharactersInLabel:(UILabel *)label {
    if (label.text.length == 0)
        return 0;
    
    @try {
        NSUInteger numberOfCharsInLabel = NSNotFound;
        NSString *labelText = label.text;
        for (int i = (int)labelText.length; i >= 0; i--) {
            NSString *substring = [labelText substringToIndex:i];
            NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:substring
                                                                                 attributes:@{ NSFontAttributeName : label.font }];
            CGSize size = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
            CGRect textFrame = [attributedText boundingRectWithSize:size
                                                            options:NSStringDrawingUsesLineFragmentOrigin
                                                            context:nil];
            
            if (CGRectGetHeight(textFrame) <= CGRectGetHeight(label.frame)) {
                numberOfCharsInLabel = i;
                break;
            }
        }
        
        if (numberOfCharsInLabel == NSNotFound) {
            // TODO: Handle this case.
            return 0;
        }
        return numberOfCharsInLabel;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return 0;
    }
}

//---------------------------------------------------------------

+ (NSInteger) getNumberOfTruncatedCharactersInLabel:(UILabel *)label {
    if (label.text.length == 0)
        return 0;

    @try {
        NSUInteger numberOfCharsInLabel = NSNotFound;
        NSUInteger numberOfCharactersThatTruncated = NSNotFound;
        
        NSString *labelText = label.text;
        for (int i = (int)labelText.length; i >= 0; i--) {
            NSString *substring = [labelText substringToIndex:i];
            NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:substring
                                                                                 attributes:@{ NSFontAttributeName : label.font }];
            CGSize size = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
            CGRect textFrame = [attributedText boundingRectWithSize:size
                                                            options:NSStringDrawingUsesLineFragmentOrigin
                                                            context:nil];
            
            if (CGRectGetHeight(textFrame) <= CGRectGetHeight(label.frame)) {
                numberOfCharsInLabel = i;
                numberOfCharactersThatTruncated = labelText.length - numberOfCharsInLabel;
                break;
            }
        }
        
        if (numberOfCharactersThatTruncated == NSNotFound) {
            return 0;
        }
        return numberOfCharactersThatTruncated;
        
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return 0;
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark NUMBER validation methods

//---------------------------------------------------------------

+ (double) validateInfinte:(double)value {
    if (isnan(value)) {
        return 0.0f;
    }
    if (isinf(value)) {
        return 0.0f;
    }
    return value;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark DATE methods

//---------------------------------------------------------------

+ (NSString *) utcDateFromTimeInterval:(long long)dateTimeStamp {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(dateTimeStamp / 1000.0)];
    return [date toNSString:OB_DATE_FORMAT];
}

//---------------------------------------------------------------

+ (NSString *) convertLocalTimeToUTC:(NSDate *)date {
    @try {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:SERVICE_DATE_TIME_FORMAT];
        NSTimeZone *gmtTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        [dateFormatter setTimeZone:gmtTimeZone];
        NSString *dateFromString = [dateFormatter stringFromDate:date];
        return dateFromString;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return nil;
    }
}

//---------------------------------------------------------------

+ (long long) utcTimeIntervalForDate:(NSDate *)date {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *sourceTimeZone = [NSTimeZone systemTimeZone];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    [dateFormatter setTimeZone:sourceTimeZone];
    [dateFormatter setDateFormat:SERVICE_DATE_TIME_FORMAT];
    NSString *dateFromString = [dateFormatter stringFromDate:date];
    
    NSDate *currentDate = [dateFormatter dateFromString:dateFromString];
    return (long long)([currentDate timeIntervalSince1970] * 1000.0);
}

//---------------------------------------------------------------

+ (BOOL) isDatesValid:(NSDate *)startDate endDate:(NSDate *)endDate {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger comps = (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay);
    NSDateComponents *date1Components = [calendar components:comps fromDate: startDate];
    NSDateComponents *date2Components = [calendar components:comps fromDate: endDate];
    startDate = [calendar dateFromComponents:date1Components];
    endDate = [calendar dateFromComponents:date2Components];
    
    NSComparisonResult result = [startDate compare:endDate];
    if (result == NSOrderedAscending) {
        return YES;
    } else if (result == NSOrderedDescending) {
        return NO;
    }  else {
        // Same date
        return YES;
    }
}

//---------------------------------------------------------------

+ (NSDate *) convertUTCTimeToLocalDate:(NSString *)dateString {
    @try {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:SERVICE_DATE_TIME_FORMAT];
        NSTimeZone *gmtTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        [dateFormatter setTimeZone:gmtTimeZone];
        NSDate *dateFromString = [dateFormatter dateFromString:dateString];
        return dateFromString;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return nil;
    }
}

//---------------------------------------------------------------

+ (NSDate *) lastDateOfCurrentMonth {
    NSDate *curDate = [NSDate date];
    NSCalendar  *calendar = [NSCalendar currentCalendar];
    NSDateComponents    *comps = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday fromDate:curDate]; // Get necessary date components
    // set last of month
    [comps setMonth:[comps month]+1];
    [comps setDay:0];
    NSDate *tDateMonth = [calendar dateFromComponents:comps];
    return tDateMonth;
}

//---------------------------------------------------------------

+ (NSDate *) lastDateOfMonth:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitWeekOfMonth|NSCalendarUnitWeekday fromDate:date]; // Get necessary date components
    // set last of month
    [comps setMonth:[comps month]+1];
    [comps setDay:0];
    NSDate *tDateMonth = [calendar dateFromComponents:comps];
    return tDateMonth;
}

//---------------------------------------------------------------

+ (NSDate *) firstDateOfMonth:(NSDate *)date {
    NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth) fromDate:date];
    components.day = 1;
    NSDate *dayOneInCurrentMonth = [[NSCalendar currentCalendar] dateFromComponents:components];
    return dayOneInCurrentMonth;
}

//---------------------------------------------------------------

+ (NSDate *) getCurrentDateTimeInGMT {
    NSString *dateString = [[NSDate date] toNSString:SERVICE_DATE_TIME_FORMAT];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat =  SERVICE_DATE_TIME_FORMAT; //@"yyyy-MM-dd HH:mm:ss";
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    return [dateFormatter dateFromString:dateString];
}

//---------------------------------------------------------------

+ (NSDate *) getDateFromMonth:(NSNumber *)month andYear:(NSNumber *)year {
    NSDate *arbitraryDate = [NSDate date];
    NSDateComponents *comp = [[NSCalendar currentCalendar] components:(NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:arbitraryDate];
    [comp setMonth:month.integerValue];
    [comp setYear:year.integerValue];
    NSDate *resultDate = [[NSCalendar currentCalendar] dateFromComponents:comp];
    return resultDate;
}

//---------------------------------------------------------------

+ (NSDate *) subtractSecondsFromDate:(NSDate *)date seconds:(NSInteger)seconds {
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setSecond:seconds];
    NSDate *finalDate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:date options:0];
    return finalDate;
}

//---------------------------------------------------------------

+ (NSString *) getCurrentDateInGMT {
    @try {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:SERVICE_DATE_TIME_FORMAT];
        NSTimeZone *gmtTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        [dateFormatter setTimeZone:gmtTimeZone];
        NSString *dateFromString = [dateFormatter stringFromDate:[NSDate date]];
        return dateFromString;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return nil;
    }
}

//---------------------------------------------------------------

+ (NSString *) getGMTTimeStampFromLocalDate:(NSDate *)localDate {
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:SERVICE_DATE_TIME_FORMAT];
    
    NSTimeInterval timeZoneOffset = [[NSTimeZone defaultTimeZone] secondsFromGMT]; // You could also use the systemTimeZone method
    NSTimeInterval gmtTimeInterval = [localDate timeIntervalSinceReferenceDate] - timeZoneOffset;
    NSDate *gmtDate = [NSDate dateWithTimeIntervalSinceReferenceDate:gmtTimeInterval];
    NSString *gmtTimeStamp = [formatter stringFromDate:gmtDate];
    return gmtTimeStamp;
}

//---------------------------------------------------------------

+ (NSInteger) getMonthFromDate:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:date];
    return [components month]; //gives you month
}

//---------------------------------------------------------------

+ (NSString *) monthNameString:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [NSLocale currentLocale];
    [dateFormatter setDateFormat:@"MMMM"];
    return [dateFormatter stringFromDate:date];
}

//---------------------------------------------------------------

+ (NSString *) yearString:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [NSLocale currentLocale];
    [dateFormatter setDateFormat:@"yyyy"];
    return [dateFormatter stringFromDate:date];
}

//---------------------------------------------------------------

+ (NSInteger) getYearFromDate:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:date];
    return [components year]; //gives you year
}

//---------------------------------------------------------------

+ (NSString *) getMonthNameFromMonthNumber:(NSInteger)month {
    
    NSString *dateString = [NSString stringWithFormat: @"%d", (int)month];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM"];
    dateFormatter.locale = [NSLocale currentLocale];
    NSDate* myDate = [dateFormatter dateFromString:dateString];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMMM"];
    formatter.locale = [NSLocale currentLocale];    
    NSString *stringFromDate = [formatter stringFromDate:myDate];
    return stringFromDate;
}

//---------------------------------------------------------------

+ (NSInteger) getDaysFromDate:(NSDate *)start toDate:(NSDate *)end {
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:start toDate:end options:0];
    return components.day;
}

//---------------------------------------------------------------

+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime {
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}

//---------------------------------------------------------------

+ (NSInteger) hoursBetweenDate:(NSDate *)fromDateTime andDateTime:(NSDate *)toDateTime {
    NSTimeInterval distanceBetweenDates = [fromDateTime timeIntervalSinceDate:toDateTime];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = ceilf(distanceBetweenDates / secondsInAnHour);
    return hoursBetweenDates;
}

//---------------------------------------------------------------

+ (NSString *) getLocalTimeZone {
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    DLog(@"timeZone abbreviation :%@", timeZone.abbreviation);
    return timeZone.abbreviation;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark ALERT methods

//---------------------------------------------------------------

+ (void) showAlertWithTitle:(NSString *)title message:(NSString *)message forController:(UIViewController *)controller {
    @try {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [controller presentViewController:alertController animated:YES completion:nil];
    } @catch (NSException *exception) {
        DLog(@"exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

+ (void) showAlertWithTitle:(NSString *)title message:(NSString *)message forController:(UIViewController *)controller withAccessoryView:(UIView *)accessoryView {
    @try {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [alertController.view addSubview:accessoryView];
        [controller presentViewController:alertController animated:YES completion:nil];
    } @catch (NSException *exception) {
        DLog(@"exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark JSON parsing methods

//---------------------------------------------------------------

+ (NSString *) getJSONString:(id)object {
    NSString *jsonString = @"";
    @try {
        NSError *error = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        jsonString = jsonData ? [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding] : @"";
        return jsonString;
    }
    @catch (NSException *exception) {
        return jsonString;
        DLog(@"exception :%@",exception.debugDescription);
    }
}

//---------------------------------------------------------------

+ (id) getObjectFromJSONString:(NSString *)jsonString {
    @try {
        NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error = nil;
        id object = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
        return object;
    } @catch (NSException *exception) {
        DLog(@"exception :%@",exception.debugDescription);
    }
}

////---------------------------------------------------------------
//
//#pragma mark
//#pragma mark Internet Connection methods
//
////---------------------------------------------------------------
//
//+ (BOOL) checkForConnection:(UIViewController *)controller {
//    if (AppDelegate.isNetworkAvailable) {
//        return YES;
//    } else {
//        [self showAlertWithTitle:@"" message:@"No Internet Connection" forController:controller];
//        return NO;
//    }
//}

//---------------------------------------------------------------

#pragma mark
#pragma mark Prefrences methods

//---------------------------------------------------------------
#pragma mark — Key file
//---------------------------------------------------------------

+ (void) writeFileToDocumentDirectory:(NSString *)fileName andFileData:(NSDictionary *)details {
    @try {
        // Build the path, and create if needed.
        NSString *filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *fileAtPath = [filePath stringByAppendingPathComponent:fileName];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
            [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
        }
        NSString *fileData = [Helper getJSONString:details];
        [[fileData dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
        DLog(@"\n\n~~~*_*_*_*_*_*_*_* Key Synchronization completed *_*_*_*_*_*_*_*~~~\n\n");
    } @catch (NSException *exception) {
        DLog(@"\n\n~~~*_*_*_*_*_*_*_* Key Synchronization FAILED!!!! *_*_*_*_*_*_*_*~~~\n\n");
        DLog(@"Exception :%@",exception.debugDescription);
    }
}

//---------------------------------------------------------------

+ (NSDictionary *) readFileFromDocumentDirectory:(NSString *)fileName {
    @try {
        NSString *filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *fileAtPath = [filePath stringByAppendingPathComponent:fileName];
        
        NSError *error = nil;
        NSData *jsonData = [NSData dataWithContentsOfFile:fileAtPath options:kNilOptions error:&error];
        
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSDictionary *details = [Helper getObjectFromJSONString:jsonString];
        return details;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@",exception.debugDescription);
        return nil;
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Domain methods

//---------------------------------------------------------------

+ (void) loginToAdmin {
    DLog(@"\n\n\n^^^^^^ :::: :::: ^^^^^^\n\n\n%@", ADMIN_URL);
    [[NSUserDefaults standardUserDefaults] setObject:ADMIN_URL forKey:KEY_DOMAIN];
}

//---------------------------------------------------------------

+ (void) loginToEuropCar {
    DLog(@"\n\n\n^^^^^^ :::: :::: ^^^^^^\n\n\n%@", EUROP_URL);
    [[NSUserDefaults standardUserDefaults] setObject:EUROP_URL forKey:KEY_DOMAIN];
}

//---------------------------------------------------------------

+ (void) setDefaultDomain {
    id object = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_DOMAIN];
    if (object) {
        [[NSUserDefaults standardUserDefaults] setObject:object forKey:KEY_DOMAIN];
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:ADMIN_URL forKey:KEY_DOMAIN];
    }
}

//---------------------------------------------------------------

+ (BOOL) isHotelDomain {
    id object = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_DOMAIN];
    
    if ([object isEqualToString:ADMIN_URL]) {
        return YES;
    } else {
        return NO;
    }
}


//---------------------------------------------------------------

+ (BOOL) isDomainChanged {
    id object = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_DOMAIN];
    
    BOOL isChange = YES;
    if ([object isEqualToString:ADMIN_URL]) {
        isChange = NO;
    } else if ([object isEqualToString:EUROP_URL]) {
        isChange = NO;
    } else {
        isChange = YES;
    }
    return isChange;
}

//---------------------------------------------------------------

@end
