//
//  LIRButtonView.h
//  InGauge
//
//  Created by Mehul Patel on 23/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"

@interface LIRButtonView : UIView

@property (nonatomic, strong) UILabel                       *label;
@property (nonatomic, strong) UIButton                      *button;
@property (nonatomic, strong) UIImageView                   *image;

// Defines image height - Default is 20
@property (nonatomic, assign) NSNumber                      *imageSize;

- (instancetype) initWithImageSize:(NSNumber *)imageSize;
@end
