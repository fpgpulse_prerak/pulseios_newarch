//
//  LIRButtonView.m
//  InGauge
//
//  Created by Mehul Patel on 23/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "LIRButtonView.h"

@implementation LIRButtonView

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc {
    // Remove delegates, mutable array and observers here!
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (void) buttonTapped:(id) sender {
//    [self.delegate buttonEventDetected:self.filterType sender:sender];
    
    self.transform = CGAffineTransformMakeScale(0.8, 0.8);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {}];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) layoutSetup {
    self.backgroundColor = [UIColor clearColor];
    
    _label = [[UILabel alloc] init];
    self.label.backgroundColor = [UIColor clearColor];
    self.label.textColor = SOCIAL_BUTTON_COLOR;
    self.label.font = [UIFont fontWithName:AVENIR_FONT size:14];
    [self.label setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:self.label];
    
    _button = [[UIButton alloc] init];
    self.button.backgroundColor = [UIColor clearColor];
    [self.button setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.button addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.button];
    
    _image = [[UIImageView alloc] init];
    self.image.backgroundColor = [UIColor clearColor];
//    self.image.tintColor = [UIColor blackColor];
    self.image.image = [UIImage imageNamed:DOWN_ARROW]; //
    [self.image setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:self.image];
    
//    self.backgroundColor = [UIColor redColor];
//        self.label.backgroundColor = [UIColor yellowColor];
    //    self.button.backgroundColor = [UIColor blueColor];
//        self.image.backgroundColor = [UIColor brownColor];
    
    self.label.text = @"Share";
    self.image.image = [UIImage imageNamed:SHARE_ICON_IMAGE];
    
    NSDictionary *metrics = @{@"imageSize" : self.imageSize ? self.imageSize : @20};
    // Constraints
    NSDictionary *views = @{@"label" : self.label, @"button" : self.button, @"image" : self.image};
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[image(imageSize)]-10-[label]-5-|" options:0 metrics:metrics views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[button]|" options:0 metrics:metrics views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[label]|" options:0 metrics:metrics views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[button]|" options:0 metrics:metrics views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[image(imageSize)]" options:0 metrics:metrics views:views]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.image
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1
                                                      constant:0]];
    
    [self.label setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    [self.label setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];

    // Disable views
//    [self setUserInteractionEnabled:NO];
//    [self.label setEnabled:NO];
//    [self.button setEnabled:NO];
//    [self.image setTintColor:[UIColor grayColor]];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Init methods

//---------------------------------------------------------------

- (instancetype) init {
    self = [super init];
    [self layoutSetup];
    return self;
}

//---------------------------------------------------------------

- (instancetype) initWithImageSize:(NSNumber *)imageSize {
    self.imageSize = imageSize;
    self = [super init];
    [self layoutSetup];
    return self;
}

//---------------------------------------------------------------

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    return self;
}

//---------------------------------------------------------------

@end
