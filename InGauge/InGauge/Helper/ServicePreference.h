//
//  ServicePreference.h
//  SuperTeam
//
//  Created by Admin on 02/02/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServicePreference : NSObject
+(void)setObject:(id)object forKey:(NSString *)key;
+(void)setBoolObject:(BOOL)value forKey:(NSString *)key;
+(id)objectForKey:(NSString *)key;
+(void)setDouble:(double)value forKey:(NSString *)key;
+(double)doubleForKey:(NSString *)key;
+(BOOL)getBoolobjectForKey:(NSString *)key;


+ (void) saveCustomObject:(id)object key:(NSString *)key;
+ (id) customObjectWithKey:(NSString *)key;
+ (void) removeAllPreferences;

@end
