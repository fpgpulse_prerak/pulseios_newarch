//
//  ServicePreference.m
//  SuperTeam
//
//  Created by Admin on 02/02/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ServicePreference.h"
#import "Constant.h"

@implementation ServicePreference

+(void)setObject:(id)object forKey:(NSString *)key{
    NSUserDefaults *standardefualts = [NSUserDefaults standardUserDefaults];
    [standardefualts setObject:object forKey:key];
    [standardefualts synchronize];
}

+(id)objectForKey:(NSString *)key {
    NSUserDefaults *standardefualts = [NSUserDefaults standardUserDefaults];
    return [standardefualts objectForKey:key];
}

+(void)setDouble:(double)value forKey:(NSString *)key{
    NSUserDefaults *standardefualts = [NSUserDefaults standardUserDefaults];
    [standardefualts setDouble:value forKey:key];
    [standardefualts synchronize];
}

+(double)doubleForKey:(NSString *)key{
    NSUserDefaults *standardefualts = [NSUserDefaults standardUserDefaults];
    return [[standardefualts objectForKey:key] doubleValue];
}

+(void)setBoolObject:(BOOL)value forKey:(NSString *)key {
    NSUserDefaults *standardefualts = [NSUserDefaults standardUserDefaults];
    [standardefualts setBool:value forKey:key];
    [standardefualts synchronize];
}

+(BOOL)getBoolobjectForKey:(NSString *)key {
    NSUserDefaults *standardefualts = [NSUserDefaults standardUserDefaults];
    return [[standardefualts objectForKey:key] boolValue];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark  methods

//---------------------------------------------------------------

+ (void) saveCustomObject:(id)object key:(NSString *)key {
    NSData *data = nil;
    if (object == nil) {
        data = nil;
    } else {
        data = [NSKeyedArchiver archivedDataWithRootObject:object];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:data forKey:key];
    [defaults synchronize];
}

//---------------------------------------------------------------

+ (id) customObjectWithKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:key];
    id object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}

//---------------------------------------------------------------

+ (void) removeAllPreferences {    
    [self saveCustomObject:nil key:KEY_APP_FILTERS];
    [self saveCustomObject:nil key:KEY_DASHBOARD_FILTERS];
    [self saveCustomObject:nil key:KEY_GOAL_FILTERS];
    [self setObject:nil forKey:DEFAULT_SHOW_COUNTER_COACHING_ALERT];
    [self saveCustomObject:nil key:KEY_STORE_FILTERS_DASHBOARD];
    
    // Remove date preferences
    [ServicePreference setObject:nil forKey:KStartDate];
    [ServicePreference setObject:nil forKey:KEndDate];
    [ServicePreference setObject:nil forKey:KComStartDate];
    [ServicePreference setObject:nil forKey:KComEndDate];
    [ServicePreference setObject:nil forKey:KComSwitch];

    // Reset domain
    if ([Helper isDomainChanged]) {
        [self setObject:nil forKey:KEY_DOMAIN];
        [Helper setDefaultDomain];
    }
}

//---------------------------------------------------------------

@end
