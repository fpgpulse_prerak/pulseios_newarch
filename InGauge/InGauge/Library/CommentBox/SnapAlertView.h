//
//  SnapAlertView.h
//  DynamicsDemo
//
//  Created by Mehul Patel on 30/04/15.
//  Copyright (c) 2015 synoverge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNPPopupController.h"
#import <UITextView+Placeholder.h>

@protocol SnapAlertDelegate <NSObject>

- (void) dismissAlert:(id)sender;
- (void) postComment:(NSString *)commentString;

@end

@interface SnapAlertView : UIView

@property (nonatomic, weak) id <SnapAlertDelegate>  delegate;

@property (nonatomic, strong) UILabel               *titleLabel;
@property (nonatomic, strong) UILabel               *line;
@property (nonatomic, strong) UITextView            *commentText;
@property (nonatomic, strong) UIButton              *postButton;
@property (nonatomic, strong) UIButton              *cancelButton;

- (void) resignTextViewResponder;
@end
