//
//  SnapAlertView.m
//  DynamicsDemo
//
//  Created by Mehul Patel on 30/04/15.
//  Copyright (c) 2015 synoverge. All rights reserved.
//

#import "SnapAlertView.h"

@interface SnapAlertView() <UITextViewDelegate>

@end

@implementation SnapAlertView

//---------------------------------------------------------------

#pragma mark
#pragma mark  methods

//---------------------------------------------------------------

- (void) dealloc {
    self.commentText.delegate = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (void) postButtonTapped:(id)sender {
    
    // Remove extra padding
    NSString *commentString = [self.commentText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    [self.delegate postComment:commentString];
}

//---------------------------------------------------------------

- (void) cancelButtonTapped:(id)sender {
    self.commentText.text = @"";
    [self.delegate dismissAlert:sender];
}

//---------------------------------------------------------------

- (void) doneButtonTapped:(id)sender {
    [self.commentText resignFirstResponder];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom  methods

//---------------------------------------------------------------

- (void) layoutSetup {
    
    _titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont fontWithName:AVENIR_NEXT_DEMI_BOLD_FONT size:12];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.textColor = [UIColor blackColor];
    self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.text = @"POST COMMENT";
    [self.titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:self.titleLabel];

    _line = [[UILabel alloc] init];
    self.line.backgroundColor = [UIColor darkGrayColor];
    self.line.lineBreakMode = NSLineBreakByWordWrapping;
    self.line.textAlignment = NSTextAlignmentRight;
    [self.line setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:self.line];
    
    _commentText = [[UITextView alloc] init];
    [self.commentText setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.commentText setFont:[UIFont fontWithName:AVENIR_NEXT_FONT size:14]];
    self.commentText.textColor = [UIColor blackColor];
    self.commentText.backgroundColor = [UIColor colorWithRed:250/255.0f green:250/255.0f blue:250/255.0f alpha:1];
    self.commentText.delegate = self;
    self.commentText.keyboardType = UIKeyboardTypeAlphabet;
    self.commentText.placeholder = @"Write comment";

    [self addSubview:self.commentText];
    
    _postButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.postButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.postButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.postButton.titleLabel setFont:[UIFont fontWithName:AVENIR_NEXT_DEMI_BOLD_FONT size:15]];
    [self.postButton setTitle:@"POST" forState:UIControlStateNormal];
    [self.postButton setBackgroundColor:MATERIAL_PINK_COLOR];
    [self.postButton addTarget:self action:@selector(postButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    self.postButton.enabled = NO;
    [self addSubview:self.postButton];

    _cancelButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.cancelButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [self.cancelButton.titleLabel setFont:[UIFont fontWithName:AVENIR_NEXT_DEMI_BOLD_FONT size:15]];
    [self.cancelButton setBackgroundColor:[UIColor grayColor]];
    [self.cancelButton addTarget:self action:@selector(cancelButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.cancelButton];
    
//    self.titleLabel.backgroundColor = [UIColor brownColor];
//    self.commentText.backgroundColor = [UIColor redColor];
//    self.postButton.backgroundColor = [UIColor blueColor];
//    self.cancelButton.backgroundColor = [UIColor yellowColor];
    
    // Constrainst
    NSDictionary *views = @{
                                @"titleLabel" : self.titleLabel,
                                @"commentText" : self.commentText,
                                @"doneButton" : self.postButton,
                                @"cancelButton" : self.cancelButton,
                                @"line" : self.line,
                            };
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[titleLabel]-15-[line(1.5)]-20-[commentText]-65-|" options: 0 metrics:nil views:views]];

    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[titleLabel]-|" options: 0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[commentText]-10-|" options: 0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-60-[line]-60-|" options: 0 metrics:nil views:views]];

    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cancelButton][doneButton]|" options: 0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[doneButton(45)]|" options: 0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[cancelButton(45)]|" options: 0 metrics:nil views:views]];

    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.cancelButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.postButton attribute:NSLayoutAttributeWidth multiplier:1 constant:0]];
}

//---------------------------------------------------------------

- (void) resignTextViewResponder {
    self.commentText.text = @"";
    [self.commentText resignFirstResponder];    
}

//---------------------------------------------------------------

#pragma mark
#pragma mark  methods

//---------------------------------------------------------------

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    [self.commentText resignFirstResponder];
    return YES;
}

//---------------------------------------------------------------

- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {

    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
//    newString = [newString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    if (newString.length >= COMMENT_MAX_LENGTH) {
        return NO;
    }
    
    if (newString.length > 0) {
        self.postButton.enabled = YES;
    } else {
        self.postButton.enabled = NO;
    }
    return YES;

}

//---------------------------------------------------------------

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    self.postButton.enabled = NO;
    return YES;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Init methods

//---------------------------------------------------------------

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        // Initial setup
        self.backgroundColor = [UIColor whiteColor];
        self.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.layer.borderWidth = 1.0;
        
//        self.layer.cornerRadius = 10;
//        self.layer.shadowColor = [UIColor blackColor].CGColor;
//        self.layer.shadowOffset = CGSizeMake(0, 5);
//        self.layer.shadowOpacity = 0.5;
//        self.layer.shadowRadius = 10.0f;
        
        [self layoutSetup];
        
        UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
        [keyboardToolbar setBarStyle:UIBarStyleDefault];
        [keyboardToolbar setTranslucent:YES];
//        [keyboardToolbar setBarTintColor:[UIColor blackColor]];
//        [keyboardToolbar setTintColor:[UIColor whiteColor]];
        [keyboardToolbar sizeToFit];

        
        UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                          initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                          target:nil action:nil];
        UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                          initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                          target:self action:@selector(doneButtonTapped:)];
        
        [doneBarButton setTitleTextAttributes:@{NSFontAttributeName : [UIFont fontWithName:AVENIR_NEXT_FONT size:14]} forState:UIControlStateNormal];

        keyboardToolbar.items = @[flexBarButton, doneBarButton];
        self.commentText.inputAccessoryView = keyboardToolbar;
    }
    return self;
}

//---------------------------------------------------------------

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
