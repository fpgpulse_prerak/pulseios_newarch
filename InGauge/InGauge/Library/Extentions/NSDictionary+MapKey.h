//
//  NSDictionary+MapKey.h
//  pulse
//
//  Created by Mehul Patel on 22/01/16.
//  Copyright © 2016 frontline performance group. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (MapKey)

- (id) getKeyValue:(id)aKey;

@end
