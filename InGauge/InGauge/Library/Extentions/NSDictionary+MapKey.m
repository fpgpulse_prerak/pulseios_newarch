//
//  NSDictionary+MapKey.m
//  pulse
//
//  Created by Mehul Patel on 22/01/16.
//  Copyright © 2016 frontline performance group. All rights reserved.
//

#import "NSDictionary+MapKey.h"

@implementation NSDictionary (MapKey)

- (id) getKeyValue:(id)aKey {
    @try {
        if ([self objectForKey:aKey]) {
            return [self objectForKey:aKey];
        }
        return @"";
    } @catch (NSException *exception) {
        NSLog(@"Exception :%@", exception.debugDescription);
        return @"";
    }
}

@end
