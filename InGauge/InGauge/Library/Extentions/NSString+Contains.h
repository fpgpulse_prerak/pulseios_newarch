//
//  NSString+Contains.h
//  pulse
//
//  Created by Mehul Patel on 05/02/16.
//  Copyright © 2016 frontline performance group. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Contains)

- (BOOL)myContainsString:(NSString*)other;

@end
