//
//  NSString+Contains.m
//  pulse
//
//  Created by Mehul Patel on 05/02/16.
//  Copyright © 2016 frontline performance group. All rights reserved.
//

#import "NSString+Contains.h"

@implementation NSString (Contains)

- (BOOL)myContainsString:(NSString*)other {
    NSRange range = [self rangeOfString:other];
    return range.length != 0;
}

@end