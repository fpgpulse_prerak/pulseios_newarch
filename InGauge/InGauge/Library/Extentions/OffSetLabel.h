//
//  OffSetLabel.h
//  IN-Gauge
//
//  Created by Mehul Patel on 22/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OffSetLabel : UILabel

@property (nonatomic, assign) UIEdgeInsets edgeInsets;

@end
