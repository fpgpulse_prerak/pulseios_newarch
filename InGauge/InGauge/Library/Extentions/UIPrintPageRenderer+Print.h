//
//  UIPrintPageRenderer+Print.h
//  IN-Gauge
//
//  Created by Mehul Patel on 07/09/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIPrintPageRenderer (Print)

- (NSData*) printToPDF;

@end
