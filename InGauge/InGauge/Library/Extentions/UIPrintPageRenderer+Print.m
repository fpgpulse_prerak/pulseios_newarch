//
//  UIPrintPageRenderer+Print.m
//  IN-Gauge
//
//  Created by Mehul Patel on 07/09/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "UIPrintPageRenderer+Print.h"

@implementation UIPrintPageRenderer (Print)

- (NSData*) printToPDF {
//    [self doNotRasterizeSubviews:self.view];
    
    NSMutableData *pdfData = [NSMutableData data];
    
    UIGraphicsBeginPDFContextToData( pdfData, CGRectZero, nil );
    
    [self prepareForDrawingPages: NSMakeRange(0, self.numberOfPages)];
    
    CGRect bounds = UIGraphicsGetPDFContextBounds();
    
    for ( int i = 0 ; i < self.numberOfPages ; i++ )
    {
        UIGraphicsBeginPDFPage();
        
        [self drawPageAtIndex: i inRect: bounds];
    }
    
    UIGraphicsEndPDFContext();
    
    return pdfData;
}

@end
