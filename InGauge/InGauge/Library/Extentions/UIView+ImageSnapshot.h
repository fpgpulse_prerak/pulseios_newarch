//
//  UIView+ImageSnapshot.h
//  IN-Gauge
//
//  Created by Mehul Patel on 05/09/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ImageSnapshot)

- (UIImage*) imageSnapshot:(CGRect)frame;
- (UIImage *) renderWithSubframe:(CGSize)size;

@end
