//
//  UIView+ImageSnapshot.m
//  IN-Gauge
//
//  Created by Mehul Patel on 05/09/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "UIView+ImageSnapshot.h"

@implementation UIView (ImageSnapshot)

- (UIImage*)imageSnapshot:(CGRect)frame {
    UIGraphicsBeginImageContextWithOptions(self.bounds.size,
                                           NO, 0.0);
    [self drawViewHierarchyInRect:frame afterScreenUpdates:NO];    
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (UIImage *) renderWithSubframe:(CGSize)size {
    
    //	Begin the image context
    UIGraphicsBeginImageContextWithOptions(self.bounds.size,	//	same size as our view
                                           NO,		//	transparent background (!opaque)
                                           0);		//	screen scale, 0 = main screen scale
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    //	Translate to our active region, for example, where the content currently is in a scroll view
    CGContextTranslateCTM(ctx, -self.bounds.origin.x, -self.bounds.origin.y);
    
    //	Draw the view's layer
    [self.layer renderInContext:ctx];
    
    //	Grab the image
    UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
    
    //	End the image context (and clean up)
    UIGraphicsEndImageContext();
    return image;
}

@end
