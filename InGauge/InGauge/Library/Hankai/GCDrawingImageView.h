//
//  GCDrawingImageView.h
//  ILovePostcard
//
//  Created by Han Kai on 7/3/13.
//  Copyright (c) 2013 iiseeuu Network Technology Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
    An ImageView that supports drawing images with finger.
    
    Reference: http://tonyngo.net/2011/09/smooth-line-drawing-in-ios/
 
    
 */

@interface GCDrawingImageView : UIImageView

@property (nonatomic, assign) CGFloat       lineWidth;

@property (nonatomic, strong) UIColor *     drawnLineColor;

@property (nonatomic, strong) UIColor *     drawingLineColor;

@end
