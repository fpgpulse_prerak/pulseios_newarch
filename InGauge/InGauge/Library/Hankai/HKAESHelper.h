
#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCryptor.h>

@interface HKAESHelper : NSObject

/*
     根据AES算法对数据进行加密或解密
     
     key				密匙
     keySize			密匙长度（CommonCryptor.h line 184）
     operation		标识是加密还是解密
 */

+ (NSData *)cryptData:(NSData *)data withKey:(NSString *)key keySize:(size_t)keySize operation:(CCOperation)operation;

@end
