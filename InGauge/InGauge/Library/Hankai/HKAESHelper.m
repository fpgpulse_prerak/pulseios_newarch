
#import "HKAESHelper.h"
#import <CommonCrypto/CommonDigest.h>

@implementation HKAESHelper

+ (NSData *)cryptData:(NSData *)data withKey:(NSString *)key keySize:(size_t)keySize operation:(CCOperation)operation {
    // 'key' will be null-padded
	char keyPtr[keySize+1]; // room for terminator (unused)
	bzero(keyPtr, sizeof(keyPtr)); // fill with zeroes (for padding)
	
	// fetch key data
	[key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
	NSUInteger dataLength = [data length];
	
	//See the doc: For block ciphers, the output size will always be less than or
	//equal to the input size plus the size of one block.
	//That's why we need to add the size of one block here
	size_t bufferSize = dataLength + kCCBlockSizeAES128;
	void *buffer = malloc(bufferSize);
    
	size_t dataLen = 0;
	CCCryptorStatus cryptStatus = CCCrypt(operation, kCCAlgorithmAES128, kCCOptionPKCS7Padding,
                                          keyPtr, keySize,
                                          nil /* initialization vector (optional) */,
                                          [data bytes], dataLength, /* input */
                                          buffer, bufferSize, /* output */
                                          &dataLen);
    NSData * retData = nil;
    
	if (cryptStatus == kCCSuccess) {
		//the returned NSData takes ownership of the buffer and will free it on deallocation
        //so, just don't free the buffer.
		retData = [NSData dataWithBytesNoCopy:buffer length:dataLen];
	} else {
        //If cryption failed, we need to free the buffer because no pointer is pointing to the buffer after return.
        free(buffer);
    }
    
    return retData;
}

@end
