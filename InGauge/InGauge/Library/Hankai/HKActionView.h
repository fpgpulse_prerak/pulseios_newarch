//
//  HKActionView.h
//
//  Created by Han Kai on 6/13/13.
//  Copyright (c) 2013 iiseeuu Network Technology Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const HKActionViewDidDismiss;
extern NSString * const HKActionViewWillAppear;

typedef enum {
    HKActionViewAnimationVerticalMove, //default
    HKActionViewAnimationFade
} HKActionViewAnimation;

@interface HKActionView : UIView

@property (nonatomic, assign, readonly)    BOOL         isShown;

//用于控制action view的纵向位置，在弹出action view时会加上这个额外的偏移量。主要为了解决不同iOS版本和硬件差异
+ (void)setExtraYOffset:(CGFloat)value;

- (void)showInView:(UIView *)parentView modal:(BOOL)isModal animation:(HKActionViewAnimation)animation;

- (void)showInView:(UIView *)parentView modal:(BOOL)isModal;

- (void)showInView:(UIView *)parentView;

- (void)dismiss;

@end
