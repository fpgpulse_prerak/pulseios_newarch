//
//  HKAudioStreaming.h
//
//  Created by Han Kai on 13-12-26.
//  Copyright (c) 2013 HanKai. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * 音频流播放器接口
 */
@protocol HKAudioStreamer <NSObject>



@end


/**
 * 音频流播放任务
 */
@interface HKAudioStreamingJob : NSObject

@end


@interface HKAudioStreamingJobManager : NSObject

@end
