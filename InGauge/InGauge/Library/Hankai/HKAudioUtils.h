//
//  ILPAudioUtils.h
//
//  Created by Han Kai on 6/27/13.
//  Copyright (c) 2013 HanKai. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^HKTimeDidChange)(NSTimeInterval currentTime);//时间变化

typedef void (^HKPowerDidChange)(float left, float right);//音量变化

typedef void (^HKRecordingDidFinish)(NSTimeInterval duration);//录音结束

typedef void (^HKPlayingDidFinish)();//播放结束

typedef void (^HKAudioErrorDidOccur)(NSError * error);//出现错误


@interface HKAudioUtils : NSObject

@property (nonatomic, strong) NSString *                    audioFilePath;

@property (nonatomic, assign) NSTimeInterval                maxDuration;

@property (nonatomic, assign) BOOL                          meteringEnabled;

@property (nonatomic, strong) HKTimeDidChange               recorderTimeDidChange;

@property (nonatomic, strong) HKTimeDidChange               playerTimeDidChange;

@property (nonatomic, strong) HKPowerDidChange              powerDidChange;//只在录音时有用

@property (nonatomic, strong) HKRecordingDidFinish          recordingDidFinish;

@property (nonatomic, strong) HKPlayingDidFinish            playingDidFinish;

@property (nonatomic, strong) HKAudioErrorDidOccur          errorDidOccur;

@property (nonatomic, readonly) BOOL                        isRecording;

@property (nonatomic, readonly) BOOL                        isPlaying;//可观察


+ (id)sharedInstance;

- (void)startRecording;

- (void)stopRecording;

- (void)startPlaying;

- (void)stopPlaying;

- (void)deleteRecording;

@end
