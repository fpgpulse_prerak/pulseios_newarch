

#import <Foundation/Foundation.h>

@interface HKBase64 : NSObject

+ (NSString *)encode:(NSData *)data;

+ (NSData *)decode:(NSString *)string;

@end
