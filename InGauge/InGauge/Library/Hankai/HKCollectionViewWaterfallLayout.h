//
//  HKCollectionViewWaterfallLayout.h
//  NailSocial
//
//  Created by Han Kai on 8/15/13.
//  Copyright (c) 2013 unknown. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HKCollectionViewWaterfallLayout;

@protocol HKCollectionViewDelegateWaterfallLayout <UICollectionViewDelegate>

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(HKCollectionViewWaterfallLayout *)layout
 heightForItemAtIndexPath:(NSIndexPath *)indexPath;

@optional

//只支持一个header
- (CGFloat)heightOfHeaderOfCollectionView:(UICollectionView *)collectionView;

@end

/*
    瀑布流式布局，目前只能支持单个 section。
 */

@interface HKCollectionViewWaterfallLayout : UICollectionViewLayout

@property (nonatomic, weak) IBOutlet id<HKCollectionViewDelegateWaterfallLayout> delegate;

@property (nonatomic, assign) NSUInteger        numberOfColumns;        //根据列数量均分得到列宽度

@property (nonatomic, assign) CGFloat           horizontalMinSpacing;   //水平最小间隔

@property (nonatomic, assign) CGFloat           verticalMinSpacing;     //垂直最小间隔

@property (nonatomic, readonly) CGFloat         columnWidth;

@end
