//
//  HKFileSystem.h
//  Hankai
//
//  Created by 韩凯 on 4/13/14.
//  Copyright (c) 2014 Hankai. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    HKFileSizeUnitByte,     //byte
    HKFileSizeUnitKiloByte, //KB
    HKFileSizeUnitMegaByte, //MB
    HKFileSizeUnitGigaByte, //GB
    HKFileSizeUnitTeraByte  //TB
} HKFileSizeUnit;

@interface HKFileSystem : NSObject

+ (unsigned long long int)sizeOfItem:(NSString *)path;

+ (void)cleanCaches;

@end


#define HKCacheSize         ([HKFileSystem sizeOfItem:AppCachesDirectory])