//
//  HKFileSystem.m
//  Hankai
//
//  Created by 韩凯 on 4/13/14.
//  Copyright (c) 2014 Hankai. All rights reserved.
//

#import "HKFileSystem.h"
#import "HKVars.h"

@implementation HKFileSystem

+ (unsigned long long int)sizeOfItem:(NSString *)path {
    unsigned long long int size = 0;
    NSFileManager * fm = [NSFileManager defaultManager];
    BOOL isDir = NO;
    if ([fm fileExistsAtPath:path isDirectory:&isDir] && isDir) {
        NSString * cacheDir = AppCachesDirectory;
        NSArray * treeNodes = [fm subpathsAtPath:cacheDir];
        NSEnumerator * enumerator = [treeNodes objectEnumerator];
        
        NSString * node = nil;
        unsigned long long s = 0;
        while (node = [enumerator nextObject]) {
            NSError * error = nil;
            NSDictionary * attributes = [fm attributesOfItemAtPath:[cacheDir stringByAppendingPathComponent:node] error:&error];
            if (error == nil) {
                s += [attributes fileSize];
            }
        }
        size = s;
    } else {
        NSError * error = nil;
        NSDictionary * attributes = [fm attributesOfItemAtPath:path error:&error];
        unsigned long long s = [attributes fileSize];
        if (error == nil) {
            size = s;
        }
    }
    return size;
}

+ (void)cleanCaches {
    NSFileManager * fm = [NSFileManager defaultManager];
    NSError * error = nil;
    NSString * dir = AppCachesDirectory;
    NSArray * items = [fm contentsOfDirectoryAtPath:dir error:&error];
    if (error == nil) {
        for (NSString * item in items) {
            error = nil;
            [fm removeItemAtPath:[dir stringByAppendingPathComponent:item] error:&error];
            if (error != nil) {
                NSLog(@"%@", error);
            }
        }
    } else {
        NSLog(@"%@", error);
    }
}

@end
