//
//  HKFilterManager.h
//  Hankai
//
//  Created by Han Kai on 6/28/13.
//  Copyright (c) 2013 iiseeuu Network Technology Co., Ltd. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//过滤器组件，可以实现后，向管理器注册新的过滤器
@protocol HKFilterComponent <NSObject>

- (UIImage *)filterImage:(UIImage *)srcImage;

//返回支持的滤镜名称
- (NSArray *)supportedFilters;

- (BOOL)canProcessImageWithFilterNamed:(NSString *)name;

@optional

//可选的，仅在一个滤镜组件支持多种滤镜的时候需要使用到，用以确定当前需要用何种滤镜效果
@property (nonatomic, strong) NSString *    filterName;

@end

//图片滤镜管理器，在管理器初始化时，会自动注册内建的滤镜组件
@interface HKFilterManager : NSObject {
    NSMutableArray *    _filters;
}

+ (NSArray *)availableFilterNames;

+ (BOOL)isFilterAvailable:(NSString *)name;

+ (void)registerFilter:(id<HKFilterComponent>)filter;

+ (void)removeFilter:(id<HKFilterComponent>)filter;

//filterName就是注册时的组件名称
+ (UIImage *)processImage:(UIImage *)srcImage withFilterNamed:(NSString *)filterName;

@end
