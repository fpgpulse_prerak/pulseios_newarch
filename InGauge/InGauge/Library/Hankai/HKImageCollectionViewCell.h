//
//  HKImageCollectionViewCell.h
//  Hankai
//
//  Created by 韩凯 on 4/7/14.
//  Copyright (c) 2014 Hankai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HKImageCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) IBOutlet UIImageView *    imageView;

@end
