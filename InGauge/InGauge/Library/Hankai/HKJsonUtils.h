//
//  HKJsonUtils.h
//  Hankai
//
//  Created by 韩凯 on 4/12/14.
//  Copyright (c) 2014 Hankai. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HKJsonPropertyMappings <NSObject>

/**
 * 为属性设置别名，键为别名，值为属性名
 */
- (NSDictionary *)jsonPropertyMappings;

/**
 * 遇到日期类型时，将使用指定日期格式来解析
 */
- (NSString *)dateFormat;

@optional

/**
 * 处理无法自动注入属性值得类型
 */
- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property;

- (void)handleDictionaryValue:(NSDictionary *)dictionary forProperty:(NSString *)property;

@end

@interface HKJsonUtils : NSObject

/**
 * 为支持属性别名的类型自动注入属性值，只支持JSON基本类型（true, false, String, number, null）的注入
 * object的属性必须全是非基础类型（如：int, enum, long 只能用NSNumber代替）
 * object: JSON数据对应的根对象，由于没有泛型支持，无法处理数组类型，即：数组中包含字典
 */
+ (void)updatePropertiesWithData:(NSDictionary *)data forObject:(NSObject<HKJsonPropertyMappings> *)object;

/* 
 Below method allows to parse JSON content with HTML Tags
 */
+ (void)updatePropertiesWithHTMLContent:(NSDictionary *)data forObject:(NSObject<HKJsonPropertyMappings> *)object;
@end
