//
//  HKJsonUtils.m
//  Hankai
//
//  Created by 韩凯 on 4/12/14.
//  Copyright (c) 2014 Hankai. All rights reserved.
//

#import "HKJsonUtils.h"
#import "NSString+HTML.h"
#import "HKReflection.h"
#import "NSDate+LangExt.h"
#import "NSString+LangExt.h"

/**
 * JSON 类型：object, array, number, string, boolean, null
 */
@implementation HKJsonUtils


/**
 * 当遇到数组类型时，尝试检查数组元素是否是基础JSON类型并尝试转换
 */
+ (NSArray *)parseJsonArrays:(NSArray *)array {
    NSMutableArray * ma = [NSMutableArray array];
    for (id object in array) {
        if ([object isKindOfClass:[NSNumber class]] ||
            [object isKindOfClass:[NSString class]] ||
            [object isKindOfClass:[NSNull class]]) {
            [ma addObject:object];
        } else if ([object isKindOfClass:[NSArray class]]) {
            [ma addObject:[self parseJsonArrays:object]];
        }
    }
    return [NSArray arrayWithArray:ma];
}

// Parse JSON without removing HTML Tags
+ (void)updatePropertiesWithHTMLContent:(NSDictionary *)data forObject:(NSObject<HKJsonPropertyMappings> *)object {
    //得到属性映射表
    if ([object conformsToProtocol:@protocol(HKJsonPropertyMappings)] &&
        [object respondsToSelector:@selector(jsonPropertyMappings)] &&
        [object respondsToSelector:@selector(dateFormat)]) {
        NSDictionary * mappings = [object jsonPropertyMappings];
        
        //遍历数据字典
        for (NSString * alias in data) {
            NSString * propertyName = mappings[alias];
            if (propertyName == nil) {
                propertyName = alias;//如果没有设置别名，则默认别名和属性名相同
            }
            id value = data[alias];
            if ([value isKindOfClass:[NSNumber class]] || //true, false, number
                [value isKindOfClass:[NSString class]] ||
                //TODO: 此处NSNull值会直接传递给实体属性，可能造成后续使用时类型适配!
                [value isKindOfClass:[NSNull class]]) {
                
                //如果是JSON基本数据类型，且类型兼容，则直接赋值
                NSString * type = [HKReflection getTypeOfProperty:propertyName ofObject:object];
                if (!isEmptyString(type)) {
                    Class clazz = NSClassFromString(type);
                    
                    //如果类型不符，额外检查是否是日期类型，如果是，则进行日期解析
                    if (![value isKindOfClass:clazz]) {
                        if (clazz == [NSDate class]) {
                            if ([object respondsToSelector:@selector(dateFormat)]) {
                                NSString * fmt = [object dateFormat];
                                value = [NSDate dateFromNSString:value withFormat:fmt];
                            }
                        }
                    }
                    @try {
                        [object setValue:value forKey:propertyName];
                    } @catch (NSException *exception) {
                        NSLog(@"Exception :%@", exception.debugDescription);
                    }
                }
            } else if ([value isKindOfClass:[NSDictionary class]]) {
                if ([object respondsToSelector:@selector(handleDictionaryValue:forProperty:)]) {
                    [object handleDictionaryValue:value forProperty:propertyName];
                }
            } else if ([value isKindOfClass:[NSArray class]]) {
                if ([object respondsToSelector:@selector(handleArrayValue:forProperty:)]) {
                    [object handleArrayValue:value forProperty:propertyName];
                }
            }
        }
    }
}

+ (void)updatePropertiesWithData:(NSDictionary *)data forObject:(NSObject<HKJsonPropertyMappings> *)object {
    //得到属性映射表
    if ([object conformsToProtocol:@protocol(HKJsonPropertyMappings)] &&
        [object respondsToSelector:@selector(jsonPropertyMappings)] &&
        [object respondsToSelector:@selector(dateFormat)]) {
        NSDictionary * mappings = [object jsonPropertyMappings];
        
        //遍历数据字典
        for (NSString * alias in data) {
            NSString * propertyName = mappings[alias];
            if (propertyName == nil) {
                propertyName = alias;//如果没有设置别名，则默认别名和属性名相同
            }
            id value = data[alias];
            if ([value isKindOfClass:[NSNumber class]] || //true, false, number
                [value isKindOfClass:[NSString class]] ||
                //TODO: 此处NSNull值会直接传递给实体属性，可能造成后续使用时类型适配!
                [value isKindOfClass:[NSNull class]]) {
                
                // UPDATE: 21 Oct 2016 - Added code to remove HTML characters from string
                // Remove percent escape
                if ([value isKindOfClass:[NSString class]]) {
                    [value stringByRemovingPercentEncoding];
                    value = [value stringByConvertingHTMLToPlainText];
                }
                
                //如果是JSON基本数据类型，且类型兼容，则直接赋值
                NSString * type = [HKReflection getTypeOfProperty:propertyName ofObject:object];
                if (!isEmptyString(type)) {
                    Class clazz = NSClassFromString(type);
                    
                    //如果类型不符，额外检查是否是日期类型，如果是，则进行日期解析
                    if (![value isKindOfClass:clazz]) {
                        if (clazz == [NSDate class]) {
                            if ([object respondsToSelector:@selector(dateFormat)]) {
                                NSString * fmt = [object dateFormat];
                                value = [NSDate dateFromNSString:value withFormat:fmt];
                            }
                        }
                    }
                    @try {
                        [object setValue:value forKey:propertyName];
                    } @catch (NSException *exception) {
                        NSLog(@"Exception :%@", exception.debugDescription);
                    }
                }
            } else if ([value isKindOfClass:[NSDictionary class]]) {
                if ([object respondsToSelector:@selector(handleDictionaryValue:forProperty:)]) {
                    [object handleDictionaryValue:value forProperty:propertyName];
                }
            } else if ([value isKindOfClass:[NSArray class]]) {
                if ([object respondsToSelector:@selector(handleArrayValue:forProperty:)]) {
                    [object handleArrayValue:value forProperty:propertyName];
                }
            }
        }
    }
}

@end
