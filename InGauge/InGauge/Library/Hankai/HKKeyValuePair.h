//
//  HKKeyValuePair.h
//  NailSocial
//
//  Created by Han Kai on 8/20/13.
//  Copyright (c) 2013 unknown. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HKKeyValuePair : NSObject

@property (nonatomic, strong) NSString *    key;

@property (nonatomic, strong) NSString *    value;

@end
