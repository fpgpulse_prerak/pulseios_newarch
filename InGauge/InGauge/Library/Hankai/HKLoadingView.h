//
//  HKLoadingView.h
//  Hankai
//
//  Created by 韩凯 on 4/13/14.
//  Copyright (c) 2014 Hankai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HKLoadingView : UIView

+ (void)presentInWindowWithText:(NSString *)text;

+ (void)presentInView:(UIView *)container withText:(NSString *)text;

+ (void)dismiss;

@end
