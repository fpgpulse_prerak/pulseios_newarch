

#import <Foundation/Foundation.h>

@interface HKMD5Helper : NSObject

//计算文件MD5
+ (NSString *)md5OfContentsOfFile:(NSString *)path;

//计算字符串MD
+ (NSString *)md5OfNSString:(NSString *)string withEncoding:(NSStringEncoding)encoding;

//计算数据的MD5
+ (NSString *)md5OfNSData:(NSData *)data;

@end
