
#import "HKMD5Helper.h"
#import <CommonCrypto/CommonDigest.h>

@implementation HKMD5Helper

+ (NSString *)md5OfContentsOfFile:(NSString *)path {
    return nil;
}

+ (NSString *)md5OfNSString:(NSString *)string withEncoding:(NSStringEncoding)encoding {
    NSData * data = [string dataUsingEncoding:encoding];
    return [self md5OfNSData:data];
}

+ (NSString *)md5OfNSData:(NSData *)data {
    if (data.length == 0) {
        return nil;
    } else {
        CC_MD5_CTX md5;
        CC_MD5_Init(&md5);
        CC_MD5_Update(&md5, [data bytes], (CC_LONG)[data length]);
        unsigned char result[CC_MD5_DIGEST_LENGTH];
        CC_MD5_Final(result, &md5);
        
        NSString* hash = [NSString stringWithFormat:
                          @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                          result[0], result[1], result[2], result[3],
                          result[4], result[5], result[6], result[7],
                          result[8], result[9], result[10], result[11],
                          result[12], result[13], result[14], result[15]];
        return hash;
    }
}

@end
