//
//  HKMath.h
//  Hankai
//
//  Created by 韩凯 on 10/14/14.
//  Copyright (c) 2014 Hankai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HKMath : NSObject

+ (float)roundFloat:(float)value fractions:(NSInteger)fractions;

@end
