//
//  HKMath.m
//  Hankai
//
//  Created by 韩凯 on 10/14/14.
//  Copyright (c) 2014 Hankai. All rights reserved.
//

#import "HKMath.h"

@implementation HKMath

+ (float)roundFloat:(float)value fractions:(NSInteger)fractions
{
    float flt = (fractions > 0) ? powf(10, fractions) : 1;
    float retVal = floorf(value * flt + 0.5) / flt;
    return retVal;
}

@end
