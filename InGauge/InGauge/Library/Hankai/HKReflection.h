//
//  HKReflection.h
//  Hankai
//
//  Created by 韩凯 on 4/12/14.
//  Copyright (c) 2014 Hankai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface HKReflection : NSObject

/**
 * 获得属性的类型
 */
+ (NSString *)getTypeOfProperty:(objc_property_t)property;

+ (NSString *)getTypeOfProperty:(NSString *)propertyName ofClass:(Class)clazz;

+ (NSString *)getTypeOfProperty:(NSString *)propertyName ofObject:(id)object;

@end
