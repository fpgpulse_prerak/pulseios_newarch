//
//  HKReflection.m
//  Hankai
//
//  Created by 韩凯 on 4/12/14.
//  Copyright (c) 2014 Hankai. All rights reserved.
//

#import "HKReflection.h"

@implementation HKReflection

+ (NSString *)getTypeOfProperty:(objc_property_t)property {
    const char * attributes = property_getAttributes(property);
    char buffer[1 + strlen(attributes)];
    strcpy(buffer, attributes);
    char * state = buffer, * attribute;
    while ((attribute = strsep(&state, ",")) != NULL) {
        if (attribute[0] == 'T' && attribute[1] == '@') {
            NSData * data = [NSData dataWithBytes:(attribute + 3) length:strlen(attribute) - 4];
            return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
    }
    return nil;
}

+ (NSString *)getTypeOfProperty:(NSString *)propertyName ofClass:(Class)clazz {
    unsigned int numberOfProperties = 0;
    objc_property_t * properties = nil;
    
    Class currentClass = clazz;
    
    //遍历当前类的继承链及各类的属性，找到目标属性
    while (YES) {
        //得到当前类的属性列表
        properties = class_copyPropertyList(currentClass, &numberOfProperties);
        
        //遍历当前类的属性列表
        for (int i=0; i<numberOfProperties; i++) {
            objc_property_t property = properties[i];
            NSString * name = [[NSString alloc] initWithUTF8String:property_getName(property)];
            if ([name isEqualToString:propertyName]) {
                return [self getTypeOfProperty:property];
            }
        }
        free(properties);
        currentClass = [currentClass superclass];
        if (currentClass == [NSObject class]) {
            break;
        }
    }
    return nil;
}

+ (NSString *)getTypeOfProperty:(NSString *)propertyName ofObject:(id)object {
    Class clazz = [object class];
    return [self getTypeOfProperty:propertyName ofClass:clazz];
}

@end
