//
//  HKTextViewViewController.h
//  Hankai
//
//  Created by 韩 凯 on 9/1/13.
//  Copyright (c) 2013 HanKai. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HKTextViewViewController;

@protocol HKTextViewViewControllerDelegate <NSObject>

- (void)textViewController:(HKTextViewViewController *)controller didFinishEditingWithText:(NSString *)text;

@end

@interface HKTextViewViewController : UITableViewController <UITextViewDelegate> {
    IBOutlet UITextView *   textView;
    IBOutlet UILabel *      lenthLabel;
}

@property (nonatomic, assign) NSInteger     maxLength;

@property (nonatomic, assign) BOOL          allowWhitespace;

@property (nonatomic, strong) NSString *    initialText;

@property (nonatomic, assign) IBOutlet id<HKTextViewViewControllerDelegate> delegate;


- (IBAction)finishEditing:(id)sender;


@end
