//
//  HKTextViewViewController.m
//  Hankai
//
//  Created by 韩 凯 on 9/1/13.
//  Copyright (c) 2013 HanKai. All rights reserved.
//

#import "HKTextViewViewController.h"
#import "UIView+GUIExt.h"

@interface HKTextViewViewController ()

@end

@implementation HKTextViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    textView.delegate = self;
    lenthLabel.radius = 4.0f;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSInteger left = self.maxLength - self.initialText.length;
    
    if (left >= 0) {
        textView.text = self.initialText;
        lenthLabel.text = [NSString stringWithFormat:@"%ld", (long)left];
    } else {
        textView.text = nil;
        lenthLabel.text = [NSString stringWithFormat:@"%ld", (long)self.maxLength];
    }
    
    [textView becomeFirstResponder];
}

#pragma mark - Events

- (void)finishEditing:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textViewController:didFinishEditingWithText:)]) {
        [self.delegate textViewController:self didFinishEditingWithText:textView.text];
    }
}


#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
}

- (void)textViewDidEndEditing:(UITextView *)textView {

}

- (BOOL)textView:(UITextView *)tv shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSMutableString * textToBe = [NSMutableString stringWithString:textView.text];
    [textToBe replaceCharactersInRange:range withString:text];
    if (textToBe.length > self.maxLength) {
        return NO;
    }
    lenthLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)(self.maxLength - textToBe.length)];
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView {

}

- (void)textViewDidChangeSelection:(UITextView *)textView {
    
}


@end
