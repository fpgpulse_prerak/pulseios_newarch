

/*
    Frequently used directories
 */
#define AppDocumentDirectory    [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]

#define AppLibraryDirectory     [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject]

#define AppCachesDirectory      [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject]

#define AppTempDirectory        NSTemporaryDirectory()

#define AppPreferenceDirectory  [AppLibraryDirectory stringByAppendingPathComponent:@"Preferences"]

#define AppCookieDirectory      [AppLibraryDirectory stringByAppendingPathComponent:@"Cookies"]


/*
    Application Meta Data
 */

#define MainBundleName          [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"]

#define DevelopmentRegion       [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDevelopmentRegion"]

/*
 For more info about version and other keys, please refer to "Recommended Info.plist Keys" section of link below:
 https://developer.apple.com/library/ios/documentation/General/Reference/InfoPlistKeyReference/
 */
//localizable key for Cocoa applications.
#define BundleShortVersion      [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]
//for both iOS based applications and Cocoa applications
#define BundleVersion           [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]



/*
    Languages
 */

#define SystemLanguage          [[NSLocale preferredLanguages] objectAtIndex:0]



/*
    URL templates
 */

#define AppDetailsUrlTemplate(appId)    [NSString stringWithFormat:@"itms://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=%@&mt=8", #appId]

#define AppReviewsUrlTemplate(appId)    [NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@", #appId]






