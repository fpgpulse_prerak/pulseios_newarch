
#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCryptor.h>

@interface NSData (HKExt)

/*
    根据AES算法对数据进行加密
 
    key				密匙
    keySize			密匙长度（CommonCryptor.h line 184）
    operation		标识是加密还是解密
 */

- (NSData*)aesCryptWithKey:(NSString *)key keySize:(size_t)keySize operation:(CCOperation)operation;


- (NSString *)md5Encrypt;

@end
