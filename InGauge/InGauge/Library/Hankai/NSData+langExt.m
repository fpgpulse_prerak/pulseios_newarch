

#import "NSData+LangExt.h"
#import "HKAESHelper.h"
#import "HKMD5Helper.h"

@implementation NSData (HKExt)

- (NSData *)aesCryptWithKey:(NSString *)key keySize:(size_t)keySize operation:(CCOperation)operation {
    return [HKAESHelper cryptData:self withKey:key keySize:keySize operation:operation];
}

- (NSString *)md5Encrypt {
    return [HKMD5Helper md5OfNSData:self];
}

@end
