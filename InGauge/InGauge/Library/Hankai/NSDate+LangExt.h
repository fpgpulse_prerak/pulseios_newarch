

#import <Foundation/Foundation.h>

#pragma mark - Parse the date compare results

#define EarlierThan(result) ({__typeof__(result) __r = (result); (__r == NSOrderedAscending);})
#define EarlierThanOrEqualTo(result) ({__typeof__(result) __r = (result); (__r == NSOrderedAscending) || (__r == NSOrderedSame);})
#define LaterThan(result) ({__typeof__(result) __r = (result); (__r == NSOrderedDescending);})
#define LaterThanOrEqualTo(result) ({__typeof__(result) __r = (result); (__r == NSOrderedDescending) || (__r == NSOrderedSame);})
#define Equal(result) (#result == NSOrderedSame)


static NSInteger const SecondsPerMinute    = 60;
static NSInteger const SecondsPerHour      = SecondsPerMinute * 60;
static NSInteger const SecondsPerDay       = SecondsPerHour * 24;


#pragma mark - Frequently used date format

#define Default_Date_Format             @"yyyy-MM-dd"
#define Date_Format1                    @"yyyy/MM/dd EE" //... 周二
#define Date_Format2                    @"yyyy/MM/dd EEEE" //... 星期三
#define Date_Format3                    @"yyyy/MM/dd"

#define Default_Date_Time_Format        @"yyyy-MM-dd hh:mm:ss"//12小时制
#define Date_Time_Format1               @"yyyy-MM-dd HH:mm"//24小时制

#define Default_Time_Format             @"hh:mm:ss"
#define Time_Format1                    @"hh:mm" //12-hour
#define Time_Format2                    @"HH:mm" //24-hour



//Date parts
typedef enum {
    NSDatePart  = 0x01,
    NSTimePart  = 0x10
} NSDateCompareMode;

//Range mode. Used to check if a date is in some range
typedef enum {
    NSDateRangeClosed,      //closed interval                   <=> []
    NSDateRangeOpen,        //open interval                     <=> ()
    NSDateRangeLeftClosed,  //Left-closed right-open interval   <=> [)
    NSDateRangeRightClosed  //Left-open right-closed interval   <=> (]
} NSDateRangeMode;

typedef struct {
    NSInteger year;
    NSInteger month;
    NSInteger day;
    NSInteger hour;
    NSInteger minute;
    NSInteger second;
} NSDateInfo;



@interface NSDate (DateParts)

@property (nonatomic, readonly) NSInteger year;

@property (nonatomic, readonly) NSInteger month;

@property (nonatomic, readonly) NSInteger day;

@property (nonatomic, readonly) NSInteger hour;

@property (nonatomic, readonly) NSInteger minute;

@property (nonatomic, readonly) NSInteger second;

@end


@interface NSDate (DateCompare)

//判断date和当前实例是否是同一天
- (BOOL)isSameDay:(NSDate *)date;

//比较日期的日期部分、时间部分或日期和时间部分
- (NSComparisonResult)compare:(NSDate *)other mode:(NSDateCompareMode)mode;

//检查日期是否出于某个日期范围内
- (BOOL)betweensDate:(NSDate *)date1 andDate:(NSDate *)date2 mode:(NSDateCompareMode)mode range:(NSDateRangeMode)range;

@end


@interface NSDate (Calculation)

/*
    日期计算
 */

//计算当前实例距离指定 日期的天数，正数表示在指定日期之后，负数表示在指定日期之前
- (NSInteger)timeIntervalInDaysSinceDate:(NSDate *)date;

//计算当前实例距离当前日期的天数，正数表示在当前日期之后，负数表示在当前日期之前
- (NSInteger)timeIntervalInDaysSinceNow;

@end


@interface NSDate (Generation)

/*
    日期创建
 */

- (NSDate *)dateByAddingDays:(NSInteger)days;

+ (NSArray *)datesBetweenDate:(NSDate *)date1 andDate:(NSDate *)date2 range:(NSDateRangeMode)range;

+ (NSDate *)dateFromDateInfo:(NSDateInfo)dateInfo;

- (NSDateInfo)dateInfo;

//根据字符串和日期格式来构造日期对象，使用当前系统时区
+ (NSDate *)dateFromNSString:(NSString *)string withFormat:(NSString *)format;

+ (NSDate *)dateFromNSString:(NSString *)string withFormat:(NSString *)format timeZone:(NSTimeZone *)timeZone;


@end


@interface NSDate (Format)

/*
    日期格式化
 
    Format: 日期格式仅仅是影响日期各分量的具体呈现方式
 
    Locale：日期的格式会根据Locale来调整以适应当地的阅读习惯，基本是改变各日期分量的排放位置
 
    TimeZone: 表示日期所属时区
 */

- (NSString *)toNSString:(NSString *)format locale:(NSLocale *)locale timeZone:(NSTimeZone *)zone;

- (NSString *)toNSString:(NSString *)format;

@end
