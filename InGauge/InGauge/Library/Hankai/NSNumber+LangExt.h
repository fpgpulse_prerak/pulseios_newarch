//
//  NSNumber+LangExt.h
//  Hankai
//
//  Created by 韩凯 on 9/7/14.
//  Copyright (c) 2014 Hankai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (LangExt)

- (NSString *)toNSStringWithMaxInteger:(NSInteger)maxInteger maxFraction:(NSInteger)maxFraction;

- (NSString *)toNSStringWithMaxFraction:(NSInteger)maxFractionDigits;

@end
