//
//  NSNumber+LangExt.m
//  Hankai
//
//  Created by 韩凯 on 9/7/14.
//  Copyright (c) 2014 Hankai. All rights reserved.
//

#import "NSNumber+LangExt.h"

@implementation NSNumber (LangExt)

- (NSString *)toNSStringWithMaxInteger:(NSInteger)maxIntegerDigits maxFraction:(NSInteger)maxFractionDigits {
    NSNumberFormatter * fmt = [NSNumberFormatter new];
    fmt.maximumFractionDigits = maxFractionDigits;
    fmt.maximumIntegerDigits = maxIntegerDigits;
    NSString * formatted = [fmt stringFromNumber:self];
    return formatted;
}

- (NSString *)toNSStringWithMaxFraction:(NSInteger)maxFractionDigits {
    NSNumberFormatter * fmt = [NSNumberFormatter new];
    fmt.maximumFractionDigits = maxFractionDigits;
    NSString * formatted = [fmt stringFromNumber:self];
    return formatted;
}

@end
