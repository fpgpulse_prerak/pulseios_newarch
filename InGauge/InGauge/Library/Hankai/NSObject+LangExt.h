//
//  NSObject+LangExt.h
//  
//
//  Created by John Han on 3/28/13.
//
//

#import <Foundation/Foundation.h>

@interface NSObject (LangExt)


@end

BOOL isNull(id obj);

// min <= num < max
NSInteger randomInteger(NSInteger min, NSInteger max);

#define HKRDM(min, max)     (randomInteger(min, max))