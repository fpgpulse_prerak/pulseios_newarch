//
//  NSObject+LangExt.m
//  
//
//  Created by John Han on 3/28/13.
//
//

#import "NSObject+LangExt.h"

@implementation NSObject (LangExt)



@end


BOOL isNull(id obj) {
    if (obj == nil || obj == [NSNull null]) {
        return YES;
    }
    return NO;
}

NSInteger randomInteger(NSInteger min, NSInteger max) {
    return (NSInteger)(arc4random() % (max - min) + min);
}