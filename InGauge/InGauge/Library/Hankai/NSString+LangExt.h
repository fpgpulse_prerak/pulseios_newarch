
#import <Foundation/Foundation.h>

@interface NSString (LangExt)

// Check nil value - if nil return empty string
- (NSString *) notNull;

//去掉空格和换行
- (NSString *)trim;

//用正则表达式来分割字符串, 例如: @"aa;bb,cc" 用 @";,"(分号或逗号) 分割后得到[aa, bb, cc].
- (NSArray *)splitWithRegex:(NSString *)expression;

//先编码，后加密
- (NSString *)md5EncryptedStringWithEncoding:(NSStringEncoding)encoding;

//AES加密/解密，先加密后编码
- (NSString*)aesEncryptWithBase64EncodingByKey:(NSString*)key;

- (NSString*)aesDecryptWithBase64EncodingWithKey:(NSString*)key;

//url encode
- (NSString *)urlEncodedUTF8String;

- (NSString *)urlDecodedUtf8String;

//check if string has no contents
+ (BOOL)isEmpty:(NSString *)string;

//generate random UUID
+ (NSString *)randomUUID;

- (id)jsonObject:(NSError **)error;

- (BOOL)matchesRegex:(NSString *)regex;

- (BOOL)containsRegex:(NSString *)regex;

- (BOOL)isEmail;

/**
 * 将字符串转换为带有千分符的数字，默认千分符为逗号，保留1位小数.
 * 如果不是一个数字，则返回原始字符串
 */
- (NSString *)asGroupedNumber;


@end

BOOL isEmptyString(NSString * str);


NSString * NSLocalizedStringWithFormat(NSString * format, ...);//deprecated, use MYLocalizedStringWithValueFormat instead

//支持本地化字符串格式化，既string table中得值可以是一个带占位符的字符串
NSString * NSLocalizedStringWithValueFormat(NSString * format, ...);

//支持键格式化，会用格式化后的键去获取本地化字符串
NSString * NSLocalizedStringWithKeyFormat(NSString * format, ...);
