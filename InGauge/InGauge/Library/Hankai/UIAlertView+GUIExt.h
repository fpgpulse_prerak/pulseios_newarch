//
//  UIAlertView+GUIExt.h
//  ILovePostcard
//
//  Created by Han Kai on 6/29/13.
//  Copyright (c) 2013 iiseeuu Network Technology Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (GUIExt)

/*
    使用 error 中的 userInfo 中的信息来生成警告框
 
    title = nil;
    message = NSLocalizedDescriptionKey + NSLocalizedDescriptionKey + NSLocalizedFailureReasonErrorKey;
    buttons = NSLocalizedRecoveryOptionsErrorKey，如果没有，则默认会添加一个 OK 按钮，需要在主工程中本地化 "OK"键
 */
+ (UIAlertView *)alertViewWithError:(NSError *)error;

//弹出一个用于显示信息的警告框，只有一个按钮
+ (void)presentAlertWithMessage:(NSString *)msg buttonTitle:(NSString *)title;

+ (void)presentAlertWithMessage:(NSString *)msg buttonTitle:(NSString *)title delegate:(id)delegate;

@end
