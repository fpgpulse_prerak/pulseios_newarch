//
//  UIAlertView+GUIExt.m
//  ILovePostcard
//
//  Created by Han Kai on 6/29/13.
//  Copyright (c) 2013 iiseeuu Network Technology Co., Ltd. All rights reserved.
//

#import "UIAlertView+GUIExt.h"

@implementation UIAlertView (GUIExt)

+ (UIAlertView *)alertViewWithError:(NSError *)error {
    NSMutableString * msg = [NSMutableString string];
    
    NSString * desc = error.userInfo[NSLocalizedDescriptionKey];
    NSString * reason = error.userInfo[NSLocalizedFailureReasonErrorKey];
    NSString * suggestion = error.userInfo[NSLocalizedRecoverySuggestionErrorKey];
    
    if (desc != nil) {
        [msg appendFormat:@"%@", desc];
    }
    
    if (reason != nil) {
        if (desc != nil) {
            [msg appendFormat:@", %@", reason];
        } else {
            [msg appendFormat:@"%@", reason];
        }
    }
    
    if (suggestion != nil) {
        if (desc != nil || reason != nil) {
            [msg appendFormat:@", %@", suggestion];
        } else {
            [msg appendFormat:@"%@", suggestion];
        }
    }
    
    if (msg.length == 0) {
        return nil;
    }
    
    NSArray * options = error.userInfo[NSLocalizedRecoveryOptionsErrorKey];
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@""
                                                     message:msg
                                                    delegate:nil
                                           cancelButtonTitle:nil
                                           otherButtonTitles:nil];
    if ([options count] == 0) {
        [alert addButtonWithTitle:NSLocalizedString(@"ok", nil)];
    } else {
        for (NSString * opt in options) {
            [alert addButtonWithTitle:opt];
        }
    }
    
    return alert;
}

+ (void)presentAlertWithMessage:(NSString *)msg buttonTitle:(NSString *)title {
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@""
                                                     message:msg
                                                    delegate:nil
                                           cancelButtonTitle:title
                                           otherButtonTitles:nil];
    [alert show];
}

+ (void)presentAlertWithMessage:(NSString *)msg buttonTitle:(NSString *)title delegate:(id)delegate {
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@""
                                                     message:msg
                                                    delegate:delegate
                                           cancelButtonTitle:title
                                           otherButtonTitles:nil];
    [alert show];
}

@end
