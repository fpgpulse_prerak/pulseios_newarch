//
//  UIColor+LangExt.h
//  ILovePostcard
//
//  Created by Han Kai on 7/29/13.
//  Copyright (c) 2013 iiseeuu Network Technology Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (LangExt)

//由形如：#1213A2 这样的颜色码字符串构成颜色，实际不要求字符串具有特定格式
//解析将会把所有数字解析为一个16进制整数然后分别取出R,G,B值
//颜色值必须满6位，不足6位应补齐
+ (UIColor *)colorFromHexString:(NSString *)hexString;

@end
