//
//  UIColor+LangExt.m
//  ILovePostcard
//
//  Created by Han Kai on 7/29/13.
//  Copyright (c) 2013 iiseeuu Network Technology Co., Ltd. All rights reserved.
//

#import "UIColor+LangExt.h"

@implementation UIColor (LangExt)

+ (UIColor *)colorFromRGBHex:(UInt32)hex {
    int r = (hex >> 16) & 0xFF;
    int g = (hex >> 8) & 0xFF;
    int b = (hex) & 0xFF;
    
    return [UIColor colorWithRed:r / 255.0f
                           green:g / 255.0f
                            blue:b / 255.0f
                           alpha:1.0f];
}

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    NSScanner * scanner = [NSScanner scannerWithString:hexString];
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
    unsigned int hexNum;
    if (![scanner scanHexInt:&hexNum]) {
        return nil;
    }
    return [self colorFromRGBHex:hexNum];
}

@end
