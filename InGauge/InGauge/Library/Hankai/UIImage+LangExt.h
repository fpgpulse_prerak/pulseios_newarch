//
//  UIImage+LangExt.h
//  ILovePostcard
//
//  Created by Han Kai on 6/26/13.
//  Copyright (c) 2013 iiseeuu Network Technology Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (LangExt)

//将原图中位于 rect 内的部分剪切出来作为新图片，不会破坏原图片
- (UIImage *)clipImageInRect:(CGRect)rect
                     newSize:(CGSize)newSize
        orientationSensitive:(BOOL)orientationSensitive;

- (UIImage *)clipImageInRect:(CGRect)rect
                     newSize:(CGSize)newSize
        orientationSensitive:(BOOL)orientationSensitive
                scaleToFill:(BOOL)scaleToFill;//新图是否需要按原图宽高比进行等比缩放来完全满足新尺寸

//将图片裁成圆形
- (UIImage *)round;

//对图片进行缩放，如果新老尺寸相同，则直接返回原图
- (UIImage *)scaleToSize:(CGSize)newSize;

- (UIImage *)filteredImage:(NSString *)filterName;

- (CIImage *)toCIImage;

//angle 角度，正值顺时针旋转，负值逆时针
- (UIImage *)rotateWithAngle:(CGFloat)angle;

- (UIImage *)fixOrientation;

@end
