//
//  UIImageView+GUIExt.h
//  ILovePostcard
//
//  Created by Han Kai on 6/13/13.
//  Copyright (c) 2013 iiseeuu Network Technology Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (GUIExt)

- (void)setImageWithTransition:(UIImage *)image;

@end
