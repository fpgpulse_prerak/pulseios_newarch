//
//  UIImageView+GUIExt.m
//  ILovePostcard
//
//  Created by Han Kai on 6/13/13.
//  Copyright (c) 2013 iiseeuu Network Technology Co., Ltd. All rights reserved.
//

#import "UIImageView+GUIExt.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIImageView (GUIExt)

#pragma mark - Public

- (void)setImageWithTransition:(UIImage *)image {
    self.image = image;
    
    CATransition * transition = [CATransition animation];
    transition.duration = 1.0f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [self.layer addAnimation:transition forKey:nil];
}

@end
