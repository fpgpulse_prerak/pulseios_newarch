//
//  UINavigationController+GUIExt.h
//  ILovePostcard
//
//  Created by Han Kai on 6/24/13.
//  Copyright (c) 2013 iiseeuu Network Technology Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (GUIExt)

- (void)setNavigationBarHidden:(BOOL)hidden withFadeAnimation:(BOOL)animated;

@end
