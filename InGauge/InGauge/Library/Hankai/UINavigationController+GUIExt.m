//
//  UINavigationController+GUIExt.m
//  ILovePostcard
//
//  Created by Han Kai on 6/24/13.
//  Copyright (c) 2013 iiseeuu Network Technology Co., Ltd. All rights reserved.
//

#import "UINavigationController+GUIExt.h"

@implementation UINavigationController (GUIExt)

- (void)setNavigationBarHidden:(BOOL)hidden withFadeAnimation:(BOOL)animated {
    if (animated) {
        [UIView animateWithDuration:0.8f
                         animations:^{
                             self.navigationBar.alpha = hidden ? 0.0f : 1.0f;
                         }
                         completion:^(BOOL finished) {
                             [self setNavigationBarHidden:self.navigationBar.alpha == 0.0f ? YES : NO];
                         }];
    } else {
        [self setNavigationBarHidden:hidden];
    }
}

@end
