//
//  UISearchBar+GUIExt.h
//  Hankai
//
//  Created by 韩凯 on 9/7/14.
//  Copyright (c) 2014 Hankai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISearchBar (GUIExt)

- (void)enableKeybordSearchForEmptyFields;

@end
