//
//  UISearchBar+GUIExt.m
//  Hankai
//
//  Created by 韩凯 on 9/7/14.
//  Copyright (c) 2014 Hankai. All rights reserved.
//

#import "UISearchBar+GUIExt.h"

@implementation UISearchBar (GUIExt)

- (void)enableKeybordSearchForEmptyFields {
    UITextField *searchBarTextField = nil;
    BOOL before7_0 = [[[UIDevice currentDevice] systemVersion] floatValue] < 7.0f;
    
    NSArray * views = before7_0 ? self.subviews : [[self.subviews objectAtIndex:0] subviews];
    for (UIView * subview in views) {
        if ([subview isKindOfClass:[UITextField class]]) {
            searchBarTextField = (UITextField *)subview;
            break;
        }
    }
    searchBarTextField.enablesReturnKeyAutomatically = NO;
}

@end
