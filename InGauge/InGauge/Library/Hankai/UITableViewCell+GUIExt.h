
#ifndef __MAC_OS_X_VERSION_MIN_REQUIRED

#import <UIKit/UIKit.h>

@interface UITableViewCell (GUIExt)

//use tag to identify cell in xib when xib contains multiple cells.
+ (id)cellFromNib:(NSString *)nibName tag:(NSInteger)tag;

@end

#endif