
#import "UITableViewCell+GUIExt.h"

#ifndef __MAC_OS_X_VERSION_MIN_REQUIRED

@implementation UITableViewCell (GUIExt)

+ (id)cellFromNib:(NSString *)nibName tag:(NSInteger)tag {
    NSArray * objs = nil;
    @try {
        objs = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    } @catch (NSException *exception) {
        NSLog(@"Failed to load nib with name %@ due to %@", nibName, exception.reason);
        return nil;
    }
    
    UITableViewCell * cell = nil;
    if ([objs count] == 1) {//only one top-level object found, just check its type, it must be UITableViewCell or its subclass.
        if (![[objs objectAtIndex:0] isKindOfClass:[self class]]) {
            NSLog(@"Failed to load cell from nib %@. Only one top level object found, but the object is not UITableViewCell or its subclass", nibName);
            assert(NO);
        } else {
            cell = (UITableViewCell *)[objs objectAtIndex:0];
            if (cell.tag != tag) {
                NSLog(@"Only one cell was found and returned, but the tag is not equal to %ld", (long)tag);
            }
        }
    } else if ([objs count] > 1) {//multiple top-level objects, in this case, do filter with tag.
        NSPredicate * pre = [NSPredicate predicateWithFormat:@"tag=%@", [NSNumber numberWithInteger:tag]];
        NSArray * results = [objs filteredArrayUsingPredicate:pre];
        if ([results count] == 1) {
            cell = [results lastObject];
        } else if ([results count] > 1) {
            NSLog(@"Multiple objects found while loading objects from nib %@ with tag %ld", nibName, (long)tag);
        } else {
            NSLog(@"Cell not found in nib %@ with tag %ld.", nibName, (long)tag);
        }
    } else {//no objects, in this case, this is an empty nib.
        NSLog(@"Cannot load cell from an empty nib %@", nibName);
    }
    
    return cell;
}


@end

#endif