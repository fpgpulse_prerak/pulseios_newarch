//
//  UIView+GUIExt.h
//
//  Created by Han Kai on 6/5/13.
//  Copyright (c) 2013 iiseeuu Network Technology Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^HKViewTapped)(void);

@interface UIView (GUIExt) <CAAnimationDelegate>

@property (nonatomic, assign) CGFloat   radius;

@property (nonatomic, assign) CGFloat   borderWidth;

@property (nonatomic, assign) UIColor * borderColor;

+ (void)fillRoundRectangleInRect:(CGRect)rect withRadius:(CGFloat)radius;

+ (void) fillGradientInRect:(CGRect)rect withColors:(NSArray*)colors angle:(CGFloat)angle;

- (void)blinkBorderWithColor:(UIColor *)color duration:(CFTimeInterval)duration;

/*
    duration: 单次旋转动画播放时间
    rotations: 单次旋转角度
    repeatCount 设为 HUGE_VALF，无限播放
 */
- (void)startSpinWithDuration:(NSTimeInterval)duration rotations:(CGFloat)rotations repeatCount:(float)repeatCount;

- (void)stopSpin;


- (void)addFullFillConstraints;

//使视图在父视图中居中显示
- (void)centerAligned;

- (void)whenTapped:(HKViewTapped)action;


@end
