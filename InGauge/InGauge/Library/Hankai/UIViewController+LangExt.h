//
//  UIViewController+LangExt.h
//  Hankai
//
//  Created by Han Kai on 13-12-31.
//  Copyright (c) 2013年 HanKai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (LangExt)

@property (nonatomic, readonly) UIViewController *      topViewController;

- (IBAction)popViewController;

@end
