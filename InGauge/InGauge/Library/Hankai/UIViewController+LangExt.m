//
//  UIViewController+LangExt.m
//  Hankai
//
//  Created by Han Kai on 13-12-31.
//  Copyright (c) 2013年 HanKai. All rights reserved.
//

#import "UIViewController+LangExt.h"

@implementation UIViewController (LangExt)

- (UIViewController *)topViewController {
    UIViewController * top = self;
    while (top.presentedViewController != nil) {
        top = top.presentedViewController;
    }
    return top;
}

- (void)popViewController {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
