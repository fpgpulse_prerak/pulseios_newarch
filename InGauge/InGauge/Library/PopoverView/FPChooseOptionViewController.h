//
//  FPChooseOptionViewController.h
//  pulse
//
//  Created by 韩凯 on 1/25/15.
//  Copyright (c) 2015 frontline performance group. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FPChooseOptionViewController;

/**
 * 列表选择控制器
 */
@protocol FPChooseOptionViewControllerDelegate <NSObject>

//获取可供选择的选项
- (NSArray *)availableOptionsForController:(FPChooseOptionViewController *)controller;

//由于是通用的列表选项，因此需要通过该函数获取需要显示的文本
- (NSString *)chooseOptionViewController:(FPChooseOptionViewController *)controller textForOptionAtIndex:(NSInteger)index;

//判断某个选项是否应该被标记为已选中，例如：编辑选项时，弹出列表后，需要将原先选择的选项标记为已选中（显示一个check mark）
- (BOOL)chooseOptionViewController:(FPChooseOptionViewController *)controller shouldMarkOptionAsSelected:(id)option;

@optional

//当用户选择了某个选项后，触发该委托函数
- (void)chooseOptionViewController:(FPChooseOptionViewController *)controller didSelectOption:(id)option;

- (BOOL)chooseOptionViewController:(FPChooseOptionViewController *)controller shouldDisableOption:(id)option;

@end



typedef enum {
    FPOptionTypeHotel,
    FPOptionTypeGroup,
    FPOptionTypeReport,
    FPOptionTypeUser,
    FPOptionTypeProduct,
    FPOptionTypeEmployee,
    FPOptionTypeMonth,
    FPOptionTypeYear,
    FPOptionTypeOther,
    FPOptionPerformanceLeader,
    FPOptionStatus,
    FPOptionObservationType,
    FPOptionCounterCoachingType,
    FPOptionTypeTime,
    FPOptionCoachingType,
    FPOptionDepartmentType,
    FPOptionProductType,
    FPOptionCountryCode,
    FPOptionClientProfileOption,
    FPOptionPulseMailRecipients,
    FPOptionTestEnvironment
} FPOptionType;

/**
 *  通用的列表选择控制器，例如：选择集团、酒店、报告等。
 *  配合 HKActionView 弹出供用户选择
 */
@interface FPChooseOptionViewController : UITableViewController

@property (nonatomic, assign) id<FPChooseOptionViewControllerDelegate>      delegate;

//记录列表选取器的选项类型，因为同一个控制器可能复用该列表选取器
@property (nonatomic, assign) FPOptionType                                  optionType;
@property (nonatomic, strong) NSArray                                       *selectedOptions;

- (IBAction)cancel;

- (IBAction)done;

- (void) initialSetup;
- (void) hideSearchBar:(BOOL)isHide;
- (void) searchTextFromList:(NSString *)searchText;

// New methods created to improve performance - 12 Aug 2016
- (void) showLocationLoadingIndicator;
- (void) hideLocationLoadingIndicator;
- (void) reloadOptions;

@end
