//
//  AHTagTableViewCell.h
//  AutomaticHeightTagTableViewCell
//
//  Created by WEI-JEN TU on 2016-07-16.
//  Copyright © 2016 Cold Yam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AHTagsLabel.h"

@protocol AHTagCellDelegate <NSObject>

- (void) tapOnTag:(AHTag *)tag;

@end

@interface AHTagTableViewCell : UITableViewCell

@property (nonatomic, weak) id <AHTagCellDelegate> delegate;
@property (nonatomic, weak) IBOutlet AHTagsLabel *label;

@end
