//
//  AHTagTableViewCell.m
//  AutomaticHeightTagTableViewCell
//
//  Created by WEI-JEN TU on 2016-07-16.
//  Copyright © 2016 Cold Yam. All rights reserved.
//

#import "AHTagTableViewCell.h"

@interface AHTagTableViewCell() <AHTagDelegate>

@end

@implementation AHTagTableViewCell 


- (void)awakeFromNib {
    [super awakeFromNib];
    self.label.delegate = self;
    // Initialization code
}

- (void)tapOnTag:(AHTag *)tag {
    [self.delegate tapOnTag:tag];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
