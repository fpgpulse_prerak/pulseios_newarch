//
//  AHTagView.h
//  AutomaticHeightTagTableViewCell
//
//  Created by WEI-JEN TU on 2016-07-16.
//  Copyright © 2016 Cold Yam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"

@interface AHTagView : UIView

@property (nonatomic, strong) UIView  *cardView;
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, copy, readonly) UIImage *image;
@property (nonatomic, copy, readonly) UIImageView *removeImage;
@end
