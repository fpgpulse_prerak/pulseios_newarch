//
//  AHTagView.m
//  AutomaticHeightTagTableViewCell
//
//  Created by WEI-JEN TU on 2016-07-16.
//  Copyright © 2016 Cold Yam. All rights reserved.
//

#import "AHTagView.h"

@implementation AHTagView {
    UILabel *_label;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setupViews];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setupViews {
    self.backgroundColor = [UIColor whiteColor];
    
    _cardView = [[UIView alloc] init];
    self.cardView.translatesAutoresizingMaskIntoConstraints = NO;
    self.cardView.backgroundColor = APP_COLOR;
    [self addSubview:self.cardView];

    _label = [UILabel new];
    _label.textColor = [UIColor whiteColor];
    _label.translatesAutoresizingMaskIntoConstraints = NO;
//    _label.layer.cornerRadius = 8;
    _label.layer.masksToBounds = YES;
    [self.cardView addSubview:_label];
    
    _removeImage = [[UIImageView alloc] init];
    self.removeImage.translatesAutoresizingMaskIntoConstraints = NO;
    self.removeImage.image = [UIImage imageNamed:@"cross"];
    self.removeImage.tintColor = [UIColor whiteColor];
    [self.cardView addSubview:self.removeImage];
    
    self.cardView.layer.masksToBounds = YES;
    self.cardView.layer.cornerRadius = 8;
    [self setupConstraints];
}

- (void) setupConstraints {
    // Constraints
    NSDictionary *views = @{@"cardView" : self.cardView, @"label" : _label, @"removeImage" : self.removeImage};
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-2-[cardView]-2-|" options: 0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-3-[cardView(25)]-3-|" options: 0 metrics:nil views:views]];

    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[label]-0-[removeImage(10)]-6-|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[label]|" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[removeImage(10)]" options: 0 metrics:nil views:views]];
    
//    [self.cardView addConstraint:[NSLayoutConstraint constraintWithItem:self.removeImage
//                                                        attribute:NSLayoutAttributeCenterY
//                                                        relatedBy:NSLayoutRelationEqual
//                                                           toItem:self.cardView
//                                                        attribute:NSLayoutAttributeCenterY
//                                                       multiplier:1.0
//                                                         constant:0]];
//    
//    [self.cardView addConstraint:[NSLayoutConstraint constraintWithItem:self.removeImage
//                                                              attribute:NSLayoutAttributeHeight
//                                                              relatedBy:NSLayoutRelationEqual
//                                                                 toItem:self.cardView
//                                                              attribute:NSLayoutAttributeHeight
//                                                             multiplier:1.0
//                                                               constant:10]];

}

/*- (void)setupConstraints {
    NSMutableArray<NSLayoutConstraint *> *constraints = [NSMutableArray new];
    
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-4.0-[_label]-4.0-|"
                                                                             options:NSLayoutFormatDirectionLeadingToTrailing
                                                                             metrics:nil
                                                                               views:@{@"_label": _label}]];
    
    [constraints addObject:[NSLayoutConstraint constraintWithItem:_label
                                                        attribute:NSLayoutAttributeHeight
                                                        relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                           toItem:nil
                                                        attribute:NSLayoutAttributeHeight
                                                       multiplier:1.0
                                                         constant:24.0]];
    
    [constraints addObject:[NSLayoutConstraint constraintWithItem:_label
                                                        attribute:NSLayoutAttributeTop
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self
                                                        attribute:NSLayoutAttributeTop
                                                       multiplier:1.0
                                                         constant:6.0]];
    
    [constraints addObject:[NSLayoutConstraint constraintWithItem:self
                                                        attribute:NSLayoutAttributeBottom
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:_label
                                                        attribute:NSLayoutAttributeBottom
                                                       multiplier:1.0
                                                         constant:6.0]];
    
    [NSLayoutConstraint activateConstraints:constraints];
} */

#pragma mark - Getter

- (UIImage *)image {
    UIGraphicsBeginImageContextWithOptions(self.frame.size, NO, 0.0);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
