//
//  AHTagsLabel.h
//  AutomaticHeightTagTableViewCell
//
//  Created by WEI-JEN TU on 2016-07-16.
//  Copyright © 2016 Cold Yam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AHTag.h"

@protocol AHTagDelegate <NSObject>

- (void) tapOnTag:(AHTag *)tag;

@end

@interface AHTagsLabel : UILabel
@property (nonatomic, weak) id <AHTagDelegate> delegate;
@property (nonatomic, strong) NSArray<AHTag *> *tags;
@property (nonatomic, assign) BOOL  isShow;

@end
