//
//  YBHud.h
//  YBHud
//
//  Created by Yahya on 03/02/17.
//  Copyright © 2017 yahya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DGActivityIndicatorView.h"

@interface YBHud : NSObject

//Public Methods
-(id)initWithHudType:(DGActivityIndicatorAnimationType)HUDType andText:(NSString *)text;

-(void)showInView:(UIView *)view;
-(void)showInView:(UIView *)view animated:(BOOL)shouldAnimate;
-(void)dismiss;
-(void)dismissAnimated:(BOOL)shouldAnimate;

//Public Properties
@property (nonatomic, strong) UIColor *tintColor; // Tint Color of the Loader and Text (Optional) (Default : White)
@property (nonatomic, strong) UIColor *hudColor;  // Background color of HUD (Optional) (Default : Black)
@property (nonatomic) CGFloat dimAmount;          // Dim Amount of HUD (Optional) (Default : 0.7)
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) UIFont *textFont;
@property (nonatomic, strong) UIView *blockView;
@property (nonatomic, strong) DGActivityIndicatorView *indicator;
@property (nonatomic, strong) UILabel *textLabel;

//USER INTERACTION
@property (nonatomic) BOOL UserInteractionDisabled;
- (void) setIndicatorType:(int)type;


@end
