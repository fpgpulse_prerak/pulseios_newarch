//
//  YBHud.m
//  YBHud
//
//  Created by Yahya on 03/02/17.
//  Copyright © 2017 yahya. All rights reserved.
//

#import "YBHud.h"
#import "DGActivityIndicatorView.h"

#define kAnimateDuration    0.25

#define kIndicatorWidth     64
#define kIndicatorHeight    68.5714

#define kTransformScaleDown     CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001)
#define kTransformScaleOriginal CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0)

@implementation YBHud

-(id)initWithHudType:(DGActivityIndicatorAnimationType)HUDType andText:(NSString *)text{
    if (self=[super init]){
        
        _tintColor = [UIColor whiteColor];
        _textFont = [UIFont boldSystemFontOfSize:18];
        _text = text;
        
        self.indicator = [[DGActivityIndicatorView alloc]
                     initWithType :  HUDType
                     tintColor    :  _tintColor];
    }
    return self;
}

-(void)showInView:(UIView *)view{
    [self showInView:view animated:NO];
}

-(void)showInView:(UIView *)view animated:(BOOL)shouldAnimate{
    
    _blockView = [[UIView alloc] initWithFrame:[view bounds]];
    
    if(_UserInteractionDisabled) self.blockView.userInteractionEnabled = NO;
    
    UIColor *hudColor = (_hudColor == nil) ? [UIColor blackColor] : _hudColor;
    if(_dimAmount == 0){ _dimAmount = 0.7; }
    
    hudColor = [hudColor colorWithAlphaComponent:_dimAmount];
    
    [self.blockView setBackgroundColor:hudColor];
    
    if(_tintColor != nil){ self.indicator.tintColor = _tintColor; }
    
    self.indicator.frame = CGRectMake(0, 0, kIndicatorWidth, kIndicatorHeight);
    self.indicator.center = self.blockView.center;
    [self.indicator startAnimating];
    [self.blockView addSubview:self.indicator];
    
    if(_text.length > 0){
        CGRect labelFrame = CGRectMake(8, kIndicatorHeight + 4, self.blockView.frame.size.width - 16, 0);
        self.textLabel = [[UILabel alloc] initWithFrame:labelFrame];
        self.textLabel.numberOfLines = 0;
        self.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.textLabel.textAlignment = NSTextAlignmentCenter;
        self.textLabel.textColor = _tintColor;
        self.textLabel.font = _textFont;
        self.textLabel.text = _text;
        [self.blockView addSubview:self.textLabel];
    }
    
    if(shouldAnimate){

        self.indicator.transform = kTransformScaleDown;
        if(self.textLabel){
            self.textLabel.transform = kTransformScaleDown;
        }
        
        self.blockView.alpha=0.3;
        [UIView animateWithDuration:kAnimateDuration
                animations:^{

                    self.indicator.transform = kTransformScaleOriginal;
                    if(self.textLabel){
                        self.textLabel.transform = kTransformScaleOriginal;
                    }
                    
                    self.blockView.alpha = 1.0;
                    [view addSubview:self.blockView];
                } completion:^(BOOL finished) { }];
    }
    
    
    else{
        [view addSubview:self.blockView];
    }
    
    /*Add Constraints*/
    self.blockView.translatesAutoresizingMaskIntoConstraints = false;
    
    NSLayoutConstraint *blockTrailing =[NSLayoutConstraint
                                   constraintWithItem:self.blockView
                                   attribute:NSLayoutAttributeTrailing
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:view
                                   attribute:NSLayoutAttributeTrailing
                                   multiplier:1.0f
                                   constant:0.f];
    
    NSLayoutConstraint *blockLeading = [NSLayoutConstraint
                                   constraintWithItem:self.blockView
                                   attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:view
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0f
                                   constant:0.f];
    
    NSLayoutConstraint *blockBottom = [NSLayoutConstraint
                                  constraintWithItem:self.blockView
                                  attribute:NSLayoutAttributeBottom
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:view
                                  attribute:NSLayoutAttributeBottom
                                  multiplier:1.0f
                                  constant:0.f];
    
    NSLayoutConstraint *blockTop = [NSLayoutConstraint
                               constraintWithItem:self.blockView
                               attribute: NSLayoutAttributeTop
                               relatedBy: NSLayoutRelationEqual
                               toItem: view
                               attribute: NSLayoutAttributeTop
                               multiplier: 1.0f
                               constant: 0.f];
    
    [view addConstraints:@[blockTrailing, blockLeading, blockBottom, blockTop]];
    
    
    self.indicator.translatesAutoresizingMaskIntoConstraints = false;
    
    NSLayoutConstraint *indicatorWidth = [NSLayoutConstraint
                                 constraintWithItem:self.indicator
                                 attribute:NSLayoutAttributeWidth
                                 relatedBy:NSLayoutRelationEqual
                                 toItem:nil
                                 attribute:NSLayoutAttributeNotAnAttribute
                                 multiplier:1.0f
                                 constant:kIndicatorWidth];
    
    NSLayoutConstraint *indicatorHeight = [NSLayoutConstraint
                                  constraintWithItem:self.indicator
                                  attribute:NSLayoutAttributeHeight
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:nil
                                  attribute:NSLayoutAttributeNotAnAttribute
                                  multiplier:1.0f
                                  constant:kIndicatorHeight];
    
    [[self.indicator.centerXAnchor constraintEqualToAnchor:self.blockView.centerXAnchor] setActive:YES];
    
    [[self.indicator.centerYAnchor constraintEqualToAnchor:self.blockView.centerYAnchor] setActive:YES];
    
    [self.indicator addConstraints:@[indicatorWidth, indicatorHeight]];
    
    
    if(self.textLabel){
        
        self.textLabel.translatesAutoresizingMaskIntoConstraints = false;
        
        NSLayoutConstraint *labelTop = [NSLayoutConstraint
                                        constraintWithItem:self.textLabel
                                        attribute:NSLayoutAttributeTop
                                        relatedBy:NSLayoutRelationEqual
                                        toItem:self.indicator
                                        attribute:NSLayoutAttributeTop
                                        multiplier:1.0f
                                        constant:kIndicatorHeight + 4];
        
        NSLayoutConstraint *labelTrailing = [NSLayoutConstraint
                                             constraintWithItem:self.textLabel
                                             attribute:NSLayoutAttributeTrailing
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.blockView
                                             attribute:NSLayoutAttributeTrailing
                                             multiplier:1.0f constant:-8 ];
        
        NSLayoutConstraint *labelLeading = [NSLayoutConstraint
                                            constraintWithItem:self.textLabel
                                            attribute:NSLayoutAttributeLeading
                                            relatedBy:NSLayoutRelationEqual
                                            toItem:self.blockView
                                            attribute:NSLayoutAttributeLeading
                                            multiplier:1.0f
                                            constant:8];
    
        
        [[self.textLabel.centerXAnchor constraintEqualToAnchor:self.blockView.centerXAnchor] setActive:YES];

        [self.blockView addConstraints:@[labelTop, labelTrailing, labelLeading ]];
    
    }
    
}

-(void)dismiss{
    [self dismissAnimated:NO];
}

-(void)dismissAnimated:(BOOL)shouldAnimate{
    if(shouldAnimate){
        [UIView animateWithDuration:kAnimateDuration
                         animations:^{
                             
                             self.indicator.transform = kTransformScaleDown;
                             if(self.textLabel){
                                 self.textLabel.transform = kTransformScaleDown;
                             }
                             
                             self.blockView.alpha = 0.0;
                         }
                         completion:^(BOOL finished){
                             [self.blockView removeFromSuperview];
                         }];
    }else{
        [self.blockView removeFromSuperview];
    }
}

- (void) setIndicatorType:(int)type {
    self.indicator.type = type;
}
@end
