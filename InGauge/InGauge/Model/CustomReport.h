//
//  CustomReport.h
//  InGauge
//
//  Created by Mehul Patel on 01/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"

@interface CustomReport : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber          *customReportId;
//@property (nonatomic, strong) NSNumber          *industryId;
//@property (nonatomic, strong) NSString          *activeStatus;
//@property (nonatomic, strong) NSNumber          *createdById;
//@property (nonatomic, strong) NSNumber          *updatedById;
//@property (nonatomic, strong) NSNumber          *createdOn;
//@property (nonatomic, strong) NSNumber          *updatedOn;
//@property (nonatomic, strong) NSNumber          *tenantId;
//@property (nonatomic, strong) NSString          *tenantName;
//@property (nonatomic, strong) NSString          *reportName;
//@property (nonatomic, strong) NSNumber          *queryBasedReport;
////@property (nonatomic, strong) NSNumber          *tenantReports;
////@property (nonatomic, strong) NSNumber          *dimensions;
//@property (nonatomic, strong) NSString          *groupBy;
//@property (nonatomic, strong) NSNumber          *customReportWidgetId;
//@property (nonatomic, strong) NSString          *customReportWidgetChartType;
//@property (nonatomic, strong) NSNumber          *customReportWidgetInverted;
//@property (nonatomic, strong) NSNumber          *customReportWidgetPolar;
//@property (nonatomic, strong) NSString          *customReportWidgetCompareType;
//@property (nonatomic, strong) NSString          *customReportWidgetDisplayType;
//@property (nonatomic, strong) NSNumber          *avgColumns;
//@property (nonatomic, strong) NSNumber          *ranking;
//@property (nonatomic, strong) NSNumber          *isDynamicHeader;
//@property (nonatomic, strong) NSDate            *cnameTime;
//@property (nonatomic, strong) NSDate          *unameTime;
//@property (nonatomic, strong) NSDate          *reviewedByWithDateTime;
//@property (nonatomic, strong) NSDate          *createdByWithDateTime;
//@property (nonatomic, strong) NSDate          *updatedByWithDateTime;

@end
