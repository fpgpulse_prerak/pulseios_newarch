//
//  CustomReport.m
//  InGauge
//
//  Created by Mehul Patel on 01/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "CustomReport.h"

@implementation CustomReport

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id": @"customReportId"};
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return FP_CONST_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

@end
