//
//  DashboardDetails.h
//  InGauge
//
//  Created by Mehul Patel on 01/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "DashboardSection.h"

@interface DashboardDetails : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber          *dashboardDetailId;
@property (nonatomic, strong) NSNumber          *industryId;
@property (nonatomic, strong) NSString          *activeStatus;
@property (nonatomic, strong) NSNumber          *createdById;
@property (nonatomic, strong) NSNumber          *updatedById;
@property (nonatomic, strong) NSNumber          *createdOn;
@property (nonatomic, strong) NSNumber          *updatedOn;
@property (nonatomic, strong) NSString          *name;
@property (nonatomic, strong) NSArray           *dashboardSections; // DashboardSection
@property (nonatomic, strong) NSNumber          *tenantId;
@property (nonatomic, strong) NSString          *tenantName;
@property (nonatomic, strong) NSNumber          *userId;
@property (nonatomic, strong) NSString          *userFirstName;
@property (nonatomic, strong) NSString          *userLastName;
@property (nonatomic, strong) NSString          *userEmail;
@property (nonatomic, strong) NSNumber          *share;
//@property (nonatomic, strong) NSNumber          *roleRights;
@property (nonatomic, strong) NSNumber          *rank;
//@property (nonatomic, strong) NSNumber          *filters;
@property (nonatomic, strong) NSDate            *cnameTime;
@property (nonatomic, strong) NSDate            *unameTime;
@property (nonatomic, strong) NSString          *reviewedByWithDateTime;
@property (nonatomic, strong) NSString          *createdByWithDateTime;
@property (nonatomic, strong) NSString          *updatedByWithDateTime;

// - Below fields are custom fields not received in response
@property (nonatomic, strong) NSArray           *reportIds;

@end
