//
//  DashboardDetails.m
//  InGauge
//
//  Created by Mehul Patel on 01/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "DashboardDetails.h"

@implementation DashboardDetails

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id": @"dashboardDetailId"};
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return FP_CONST_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    if (!isNull(array)) {
        @try {
            if ([property isEqualToString:@"dashboardSections"]) {
                NSMutableArray *arr = [[NSMutableArray alloc] init];
                NSMutableArray *tempArray = [[NSMutableArray alloc] init];
                
                for (NSDictionary *details in array) {
                    DashboardSection *model = [DashboardSection new];
                    [HKJsonUtils updatePropertiesWithData:details forObject:model];
                    [arr addObject:model];
                    [tempArray addObjectsFromArray:model.reportIds];
                }
                self.dashboardSections = [NSArray arrayWithArray:arr];
                self.reportIds = [NSArray arrayWithArray:tempArray];
                arr = nil;
                tempArray = nil;
            }
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
    }
}

//---------------------------------------------------------------


@end
