//
//  DashboardListModel.h
//  InGauge
//
//  Created by Mehul Patel on 23/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DashboardModel.h"
#import "Helper.h"

@interface DashboardListModel : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSArray       *dashboardList; //DashboardModel

@end
