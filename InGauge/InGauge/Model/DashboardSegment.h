//
//  DashboardSegment.h
//  InGauge
//
//  Created by Mehul Patel on 01/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "CustomReport.h"

@interface DashboardSegment : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber          *dashboardSegmentId;
@property (nonatomic, strong) NSString          *activeStatus;
@property (nonatomic, strong) NSNumber          *segmentIndex;
@property (nonatomic, strong) NSString          *title;
@property (nonatomic, strong) NSArray           *customReports; //CustomReport
@property (nonatomic, strong) NSString          *cnameTime;
@property (nonatomic, strong) NSString          *unameTime;
@property (nonatomic, strong) NSString          *reviewedByWithDateTime;
@property (nonatomic, strong) NSString          *createdByWithDateTime;
@property (nonatomic, strong) NSString          *updatedByWithDateTime;

// - Below fields are custom fields not received in response
@property (nonatomic, strong) NSArray           *reportIds;

@end
