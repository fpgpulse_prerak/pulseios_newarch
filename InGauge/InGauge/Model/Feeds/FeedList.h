//
//  FeedList.h
//  InGauge
//
//  Created by Mehul Patel on 02/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "FeedModel.h"

@interface FeedList : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSArray       *feedList; //FeedModel

@end
