//
//  Filters.h
//  InGauge
//
//  Created by Mehul Patel on 31/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"

@interface Filters : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber          *tenantId;
@property (nonatomic, strong) NSNumber          *regionTypeId;
@property (nonatomic, strong) NSNumber          *userId;
@property (nonatomic, strong) NSString          *orderBy;
@property (nonatomic, strong) NSString          *sort;
@property (nonatomic, strong) NSNumber          *regionId;
@property (nonatomic, strong) NSString          *activeStatus;
@property (nonatomic, strong) NSNumber          *accessible;
@property (nonatomic, strong) NSNumber          *locationId;
@property (nonatomic, strong) NSNumber          *locationGroupId;
@property (nonatomic, strong) NSNumber          *productId;

@property (nonatomic, strong) NSNumber          *locationGroupIDs;
@property (nonatomic, strong) NSDate            *startDate;
@property (nonatomic, strong) NSDate            *endDate;
@property (nonatomic, strong) NSDate            *compareStartDate;
@property (nonatomic, strong) NSDate            *compareEndDate;

@property (nonatomic, strong) NSNumber          *getContributorsOnly;
@property (nonatomic, strong) NSString          *isCompare;
@property (nonatomic, strong) NSString          *metricsDateType;
@property (nonatomic, strong) NSNumber          *loginUser;
@property (nonatomic, strong) NSNumber          *industryId;

@property (nonatomic, strong) NSString          *userRole;
@property (nonatomic, strong) NSString          *industryDescription;
@property (nonatomic, strong) NSString          *tenantName;
@property (nonatomic, strong) NSString          *metricType;

- (NSDictionary *)serializeFiltersForRegionService;
- (NSDictionary *)serializeFiltersForLocationService;
- (NSDictionary *)serializeFiltersForProductService;
- (NSDictionary *)serializeFiltersForUsersService;
- (NSDictionary *)serializeFiltersForChartURL;
- (NSDictionary *)serializeFiltersForFeedsService;

- (void) copyFromFilter:(Filters *)filter;
@end
