//
//  Filters.m
//  InGauge
//
//  Created by Mehul Patel on 31/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "Filters.h"

@implementation Filters

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return nil;
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return FP_CONST_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) copyFromFilter:(Filters *)filter {
}

//---------------------------------------------------------------

- (NSDictionary *)serializeFiltersForFeedsService {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (self.tenantId != nil) {
        [dict setValue:self.tenantId forKey:@"tenantId"];
    }
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *)serializeFiltersForChartURL {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (self.tenantId != nil) {
        [dict setValue:self.tenantId forKey:@"tenantId"];
    }
    if (self.regionTypeId != nil) {
        [dict setValue:self.regionTypeId forKey:@"regionTypeId"];
    }
    if (self.regionId != nil) {
        [dict setValue:self.regionId forKey:@"regionId"];
    }
    if (self.locationId != nil) {
        [dict setValue:self.locationId forKey:@"tenantLocationId"];
    }
    if (self.locationGroupId != nil) {
        [dict setValue:self.locationGroupId forKey:@"locationGroupId"];
    }
    if (self.productId != nil) {
        [dict setValue:self.productId forKey:@"productId"];
    }
    if (self.userId != nil) {
        [dict setValue:self.userId forKey:@"userId"];
    }
    if (self.startDate != nil) {
        [dict setValue:[self.startDate toNSString:FP_CONST_DATE_FORMAT] forKey:@"from"];
    }
    if (self.endDate != nil) {
        [dict setValue:[self.endDate toNSString:FP_CONST_DATE_FORMAT] forKey:@"to"];
    }
    if (self.isCompare != nil) {
        [dict setValue:self.isCompare forKey:@"isCompare"];
    }
    if (self.compareStartDate != nil) {
        [dict setValue:[self.compareStartDate toNSString:FP_CONST_DATE_FORMAT] forKey:@"compFrom"];
    }
    if (self.compareEndDate != nil) {
        [dict setValue:[self.compareEndDate toNSString:FP_CONST_DATE_FORMAT] forKey:@"compTo"];
    }
    if (self.metricsDateType != nil) {
        [dict setValue:self.metricsDateType forKey:@"metricsDateType"];
    }
    if (self.loginUser != nil) {
        [dict setValue:self.loginUser forKey:@"loginUser"];
    }
    if (self.industryId != nil) {
        [dict setValue:self.industryId forKey:@"industryId"];
    }

    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *)serializeFiltersForUsersService {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];    
    if (self.locationGroupId != nil) {
        [dict setValue:self.locationGroupId forKey:@"locationGroupIDs"];
    }
    if (self.startDate != nil) {
        [dict setValue:[self.startDate toNSString:FP_CONST_DATE_FORMAT] forKey:@"startDate"];
    }
    if (self.endDate != nil) {
        [dict setValue:[self.endDate toNSString:FP_CONST_DATE_FORMAT] forKey:@"endDate"];
    }    
    if (self.getContributorsOnly != nil) {
        [dict setValue:self.getContributorsOnly forKey:@"getContributorsOnly"];
    }
    
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *)serializeFiltersForProductService {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (self.locationGroupId != nil) {
        [dict setValue:self.locationGroupId forKey:@"locationGroupId"];
    }
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *)serializeFiltersForRegionService {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (self.accessible != nil) {
        [dict setValue:self.accessible forKey:@"accessible"];
    }
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *)serializeFiltersForLocationService {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (self.orderBy != nil) {
        [dict setValue:self.orderBy forKey:@"orderBy"];
    }
    if (self.sort != nil) {
        [dict setValue:self.sort forKey:@"sort"];
    }
    if (self.regionId != nil) {
        [dict setValue:self.regionId forKey:@"regionId"];
    }
    if (self.activeStatus != nil) {
        [dict setValue:self.activeStatus forKey:@"activeStatus"];
    }
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

@end
