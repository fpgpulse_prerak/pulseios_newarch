//
//  Industry.m
//  InGauge
//
//  Created by Mehul Patel on 17/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "Industry.h"

@implementation Industry

//---------------------------------------------------------------

#pragma mark
#pragma mark HKJsonPropertyMappings  methods

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id" : @"industryId",@"description":@"industryDescription"};
    return nil;
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return nil;
}

//---------------------------------------------------------------

- (void) encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.industryId forKey:@"industryId"];
    [encoder encodeObject:self.activeStatus forKey:@"activeStatus"];
    [encoder encodeObject:self.indexRelatedOnly forKey:@"indexRelatedOnly"];
    [encoder encodeObject:self.indexMainOnly forKey:@"indexMainOnly"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.industryDescription forKey:@"industryDescription"];
}

//---------------------------------------------------------------

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.industryId  = [decoder decodeObjectForKey:@"industryId"];
        self.activeStatus  = [decoder decodeObjectForKey:@"activeStatus"];
        self.indexRelatedOnly  = [decoder decodeObjectForKey:@"indexRelatedOnly"];
        self.indexMainOnly  = [decoder decodeObjectForKey:@"indexMainOnly"];
        self.name  = [decoder decodeObjectForKey:@"name"];
        self.industryDescription  = [decoder decodeObjectForKey:@"industryDescription"];
    }
    return self;
}

//---------------------------------------------------------------

@end
