//
//  Product.h
//  InGauge
//
//  Created by Mehul Patel on 31/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"

@interface Product : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber          *productId;
@property (nonatomic, strong) NSNumber          *industryId;
@property (nonatomic, strong) NSNumber          *activeStatus;
@property (nonatomic, strong) NSNumber          *createdById;
@property (nonatomic, strong) NSNumber          *updatedById;
@property (nonatomic, strong) NSNumber          *createdOn;
@property (nonatomic, strong) NSNumber          *updatedOn;
@property (nonatomic, strong) NSNumber          *availableDate;
@property (nonatomic, strong) NSNumber          *groupProductCategoryId;
@property (nonatomic, strong) NSString          *groupProductCategoryName;
@property (nonatomic, strong) NSString          *incentiveDiscount;
@property (nonatomic, strong) NSNumber          *isExcludedProduct;
@property (nonatomic, strong) NSNumber          *isGoalExcludedProduct;
@property (nonatomic, strong) NSNumber          *isIndividualUpsell;
@property (nonatomic, strong) NSNumber          *isMajor;
@property (nonatomic, strong) NSNumber          *isRecurring;
@property (nonatomic, strong) NSString          *measurementUnit;
@property (nonatomic, strong) NSString          *name;
@property (nonatomic, strong) NSString          *productAvailability;
@property (nonatomic, strong) NSNumber          *productCost;
@property (nonatomic, strong) NSString          *productType;
@property (nonatomic, strong) NSNumber          *thumbnailImage;
@property (nonatomic, strong) NSNumber          *locationGroupId;
@property (nonatomic, strong) NSString          *locationGroupName;
@property (nonatomic, strong) NSNumber          *tenantProductId;
@property (nonatomic, strong) NSString          *tenantProductName;
@property (nonatomic, strong) NSNumber          *locationGroupTenantLocationId;
@property (nonatomic, strong) NSString          *locationGroupTenantLocationName;
@property (nonatomic, strong) NSNumber          *locationGroupGroupTypeId;
@property (nonatomic, strong) NSString          *locationGroupGroupTypeName;
@property (nonatomic, strong) NSNumber          *locationGroupTenantLocationTenantId;
@property (nonatomic, strong) NSNumber          *locationGroupTenantProductId;
@property (nonatomic, strong) NSString          *updatedByWithDateTime;
@property (nonatomic, strong) NSString          *createdByWithDateTime;
@property (nonatomic, strong) NSString          *reviewedByWithDateTime;
@property (nonatomic, strong) NSDate            *unameTime;
@property (nonatomic, strong) NSDate            *cnameTime;

@end
