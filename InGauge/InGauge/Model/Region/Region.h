//
//  Region.h
//  InGauge
//
//  Created by Mehul Patel on 31/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "RegionType.h"

@interface Region : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber      *regionId;
@property (nonatomic, strong) NSNumber      *industryId;
@property (nonatomic, strong) NSString      *createdBy;
@property (nonatomic, strong) NSString      *updatedBy;
@property (nonatomic, strong) NSString      *activeStatus;
@property (nonatomic, strong) NSDate        *createdOn;
@property (nonatomic, strong) NSDate        *updatedOn;
@property (nonatomic, strong) NSNumber      *indexRelatedOnly;
@property (nonatomic, strong) NSNumber      *indexMainOnly;
//@property (nonatomic, strong) NSNumber      *tenant;    /// Tenant custom class
@property (nonatomic, strong) RegionType    *regionType;
@property (nonatomic, strong) NSString      *name;
@property (nonatomic, strong) NSNumber      *oldId;
@property (nonatomic, strong) NSString      *regionHierarchy;
@property (nonatomic, strong) NSString      *currency;
@property (nonatomic, strong) NSString      *country;
@property (nonatomic, strong) NSString      *dataUtilityEnable;

@end
