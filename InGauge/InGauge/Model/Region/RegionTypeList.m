//
//  RegionTypeList.m
//  InGauge
//
//  Created by Mehul Patel on 31/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "RegionTypeList.h"

@implementation RegionTypeList

//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return nil;
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return FP_CONST_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    if (!isNull(array)) {
        // FPLocationPM
        @try {
            if ([property isEqualToString:MAIN_BODY]) {
                NSMutableArray *arr = [[NSMutableArray alloc] init];
                for (NSDictionary *details in array) {
                    RegionType *model = [RegionType new];
                    [HKJsonUtils updatePropertiesWithData:details forObject:model];
                    [arr addObject:model];
                }
                self.regionTypeList = [NSArray arrayWithArray:arr];
                arr = nil;
            }
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
    }
}

//---------------------------------------------------------------

@end
