//
//  Role.m
//  InGauge
//
//  Created by Mehul Patel on 11/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "Role.h"

@implementation Role

#pragma mark - HKJsonPropertyMappings

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id": @"roleID",
             @"description": @"introduction"};
}

- (NSString *)dateFormat
{
    return FP_CONST_DATE_TIME_FORMAT;
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    Role *role = [Role new];
    role.roleID = self.roleID;
    role.name = self.name;
    role.shortName = self.shortName;
    role.introduction = self.introduction;
    role.routes = self.routes;
    role.menus = self.menus;
    role.reports = self.reports;
    role.timestamp = self.timestamp;
    role.updatedTime = self.updatedTime;
    role.isSuperAdmin = self.isSuperAdmin;
    role.weight = self.weight;
    role.isPerformanceManager = self.isPerformanceManager;
    return role;
}

@end
