//
//  TenantList.m
//  InGauge
//
//  Created by Mehul Patel on 02/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "TenantList.h"

@implementation TenantList
//---------------------------------------------------------------

#pragma mark
#pragma mark - HKJsonPropertyMappings

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return nil;
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return FP_CONST_DATE_TIME_FORMAT;
}

//---------------------------------------------------------------

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    if (!isNull(array)) {
        @try {
            if ([property isEqualToString:MAIN_BODY]) {
                NSMutableArray *arr = [[NSMutableArray alloc] init];
                for (NSDictionary *details in array) {
                    TenantRoleModel *model = [TenantRoleModel new];
                    [HKJsonUtils updatePropertiesWithData:details forObject:model];
                    [arr addObject:model];
                }
                self.tenantList = [NSArray arrayWithArray:arr];
                arr = nil;
            }
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
    }
}

//---------------------------------------------------------------

- (void) encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.tenantList forKey:@"tenantList"];
}

//---------------------------------------------------------------

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.tenantList  = [decoder decodeObjectForKey:@"tenantList"];
    }
    return self;
}

//---------------------------------------------------------------

@end
