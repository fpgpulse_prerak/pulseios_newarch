//
//  TenantModel.h
//  InGauge
//
//  Created by Mehul Patel on 17/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TenantRoleModel.h"
#import "Hankai.h"

@interface TenantModel : NSObject <HKJsonPropertyMappings>

@property (nonatomic,assign) NSInteger *_id;
@property (nonatomic,assign) NSInteger *industryId;
@property (nonatomic,retain) NSString *activeStatus;
@property (nonatomic,assign) NSInteger *userId;
@property (nonatomic,retain) NSString *userName;
@property (nonatomic,retain) NSString *tenantName;
@property (nonatomic,assign) NSInteger *roleId;
@property (nonatomic,retain) NSString *roleName;
@property (nonatomic,assign) NSInteger *roleWeight;
@property (nonatomic,retain) NSArray *roleRolePermissions;


@end
