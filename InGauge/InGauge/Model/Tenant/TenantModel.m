//
//  TenantModel.m
//  InGauge
//
//  Created by Mehul Patel on 17/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "TenantModel.h"


@implementation TenantModel

//---------------------------------------------------------------

#pragma mark
#pragma mark HKJsonPropertyMappings  methods

//---------------------------------------------------------------

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id" : @"_id"};
    return nil;
}

//---------------------------------------------------------------

- (NSString *)dateFormat {
    return nil;
}

//---------------------------------------------------------------

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property {
    
    if (!isNull(array)) {
        
        // listLeaveBalance  //ListLeaveBalance
        if ([property isEqualToString:@"listLeaveBalance"]) {
            NSMutableArray * arr = [[NSMutableArray alloc] init];
            for (NSDictionary *orgDict in array) {
                TenantRoleModel *tenantRoleModel = [TenantRoleModel new];
                [HKJsonUtils updatePropertiesWithData:orgDict forObject:tenantRoleModel];
                [arr addObject:tenantRoleModel];
            }
            self.roleRolePermissions = [NSArray arrayWithArray:arr];
            arr = nil;
        }
    }
}


//---------------------------------------------------------------

- (void)handleDictionaryValue:(NSDictionary *)dictionary forProperty:(NSString *)property {
    if (!isNull(dictionary)) {
        
        //        //incentive
        //        if ([property isEqualToString:@"incentive"]) {
        //            FPIncentive *incentive = [FPIncentive new];
        //            [HKJsonUtils updatePropertiesWithData:dictionary forObject:incentive];
        //            self.incentive = incentive;
        //        }
        //
        //        //searchEmployeeDashBoardForm
        //        if ([property isEqualToString:@"searchForm"]) {
        //            FPAgentSearchForm *form = [FPAgentSearchForm new];
        //            [HKJsonUtils updatePropertiesWithData:dictionary forObject:form];
        //            self.searchForm = form;
        //
        //            //            self.performanceSummary.currency = [self getOrganizationCurrency];
        //        }
        //
        //        //performanceSummary
        //        if ([property isEqualToString:@"performanceSummary"]) {
        //            FPAgentPerformanceSummary *performSummary = [FPAgentPerformanceSummary new];
        //            [HKJsonUtils updatePropertiesWithData:dictionary forObject:performSummary];
        //            self.performanceSummary = performSummary;
        //            self.performanceSummary.socialFeeds = self.feeds;
        //            // Reset social values
        //            [self.performanceSummary setSocialFeedValueToPerformanceMetrics];
        //        }
    }
}

@end
