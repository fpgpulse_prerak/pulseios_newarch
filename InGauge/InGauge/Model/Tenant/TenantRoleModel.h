//
//  TenantRoleModel.h
//  InGauge
//
//  Created by Mehul Patel on 17/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Hankai.h"

@interface TenantRoleModel : NSObject <HKJsonPropertyMappings>

@property (nonatomic, strong) NSNumber          *tenantRoleId;
@property (nonatomic, strong) NSNumber          *industryId;
@property (nonatomic, strong) NSString          *activeStatus;
@property (nonatomic, strong) NSNumber          *userId;
@property (nonatomic, strong) NSString          *userName;
@property (nonatomic, strong) NSNumber          *tenantId;
@property (nonatomic, strong) NSString          *tenantName;
@property (nonatomic, strong) NSNumber          *roleId;
@property (nonatomic, strong) NSString          *roleName;
@property (nonatomic, strong) NSNumber          *roleWeight;
@property (nonatomic, strong) NSArray           *roleRolePermissions;
@property (nonatomic, strong) NSString          *industryName;
@property (nonatomic, strong) NSString          *industryDescription;
@property (nonatomic, strong) NSArray           *roleReports;
@property (nonatomic, strong) NSArray           *roleMetricAuditStatus;
@property (nonatomic, strong) NSArray           *roleIndustryMenus;
@property (nonatomic, strong) NSArray           *roleIndustryRoutes;
@property (nonatomic, strong) NSString          *solrTimeZone;
@property (nonatomic, strong) NSString          *updatedByWithDateTime;
@property (nonatomic, strong) NSString          *createdByWithDateTime;
@property (nonatomic, strong) NSString          *reviewedByWithDateTime;
@property (nonatomic, strong) NSString          *unameTime;
@property (nonatomic, strong) NSString          *cnameTime;


//"roleRolePermissions": [
//                        {
//                            "id": 2,
//                            "activeStatus": "Normal",
//                            "displayName": "Can Change Contributor",
//                            "name": "canChangeContributor",
//                            "permissionType": "ROLE",
//                            "updatedByWithDateTime": " - ",
//                            "createdByWithDateTime": " - ",
//                            "reviewedByWithDateTime": " - ",
//                            "unameTime": "-",
//                            "cnameTime": "-"
//                        },
//                        ],

@end
