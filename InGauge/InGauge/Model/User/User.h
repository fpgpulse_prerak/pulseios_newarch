//
//  User.h
//  InGauge
//
//  Created by Mehul Patel on 11/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HKJsonUtils.h"
#import "Helper.h"
#import "Role.h"
#import "ServiceMetaData.h"

typedef enum {
    FPGenderMale        =   0,
    FPGenderFemale      =   1,
    FPGenderNotSpecify  =   2
} FPGender;

@class FPRole;

@interface User : NSObject <HKJsonPropertyMappings, NSCopying>

// Old fields
@property (nonatomic, strong) NSNumber              *userID;
@property (nonatomic, strong) NSString              *number;
@property (nonatomic, strong) NSString              *email;
@property (nonatomic, strong) NSString              *password;      // Password is stored only MD5 encrypted
@property (nonatomic, strong) NSString              *firstName;
@property (nonatomic, strong) NSString              *middleName;
@property (nonatomic, strong) NSString              *lastName;
@property (nonatomic, strong) NSString              *gender;        // FPGender
@property (nonatomic, strong) NSString              *phoneNumber;
@property (nonatomic, strong) NSDate                *hiredDate;
@property (nonatomic, strong) NSString              *fpgLevel;
@property (nonatomic, strong) NSNumber              *status;        // FPStatus
@property (nonatomic, strong) NSDate                *timestamp;
@property (nonatomic, strong) NSDate                *updatedTime;
@property (nonatomic, strong) NSString              *accessToken;
@property (nonatomic, strong) NSNumber               *tokenExpiry;
@property (nonatomic, strong) NSDate                *signedInTime;
@property (nonatomic, strong) NSArray               *roles;         // FPRole
@property (nonatomic, readonly) FPRole              *highestWeightRole;
@property (nonatomic, readonly) NSString            *avatarUrl;

// New fields
@property (nonatomic, strong) NSNumber              *isContributor;
@property (nonatomic, strong) NSNumber              *isFPGMember;
@property (nonatomic, strong) NSNumber              *isOperator;
@property (nonatomic, strong) NSNumber              *is2FARequired;

@property (nonatomic, strong) NSString              *activeStatus;
@property (nonatomic, strong) NSDate                *createdOn;
@property (nonatomic, strong) NSDate                *updatedOn;
@property (nonatomic, strong) NSNumber              *indexRelatedOnly;
@property (nonatomic, strong) NSNumber              *indexMainOnly;
@property (nonatomic, strong) NSString              *salt;
@property (nonatomic, strong) NSNumber              *defaultIndustry;
@property (nonatomic, strong) NSString              *jobTitle;
@property (nonatomic, strong) NSString              *linkedinId;
@property (nonatomic, strong) NSString              *linkedinImage;
@property (nonatomic, strong) NSString              *pulseId;
@property (nonatomic, strong) NSNumber              *isAuthyAttempted;
@property (nonatomic, strong) NSString              *workAge;
@property (nonatomic, strong) NSString              *name;
@property (nonatomic, strong) NSString              *birthMonth;
@property (nonatomic, strong) NSString              *birthDay;

- (NSString *)getFullName;
- (BOOL) isSuperAdmin;
- (BOOL) isPerformanceManager;
+ (BOOL) isLoginUserCanSeePerformanceLeader;

- (void)setAsCurrentUser:(BOOL)cacheEabled;
+ (BOOL)signedIn;
+ (User *)signedInUser;
+ (void)signOut;


- (NSDictionary *)serialize;

- (NSDictionary *) userParams;

@end


extern NSString * const FPUserDidSignInNotification;

extern NSString * const FPUserDidSignOutNotification;

extern NSString * const FPAuthenticationFailedNotification;

extern NSString * const FPPermissionErrorNotification;
