//
//  User.m
//  InGauge
//
//  Created by Mehul Patel on 11/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "User.h"
#import "NSObject+LangExt.h"
#import "NSString+LangExt.h"
#import "ServicePreference.h"

#define AppCachesDirectory      [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject]
#define FPUserInfoCachePath     [AppCachesDirectory stringByAppendingPathComponent:@"CurrentUser"]


@implementation User

- (NSString *)getFullName {
    NSString *firstName = [Helper removeWhiteSpaceAndNewLineCharacter:self.firstName];
    NSString *lastName = [Helper removeWhiteSpaceAndNewLineCharacter:self.lastName];
    return [NSString stringWithFormat:@"%@ %@", firstName, lastName];
}

- (BOOL) isSuperAdmin {
    @try {
        User *loginUser = [User signedInUser];
        Role *role = [loginUser.roles objectAtIndex:0];
        if (role) {
            return role.isSuperAdmin.boolValue;
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription)
        return NO;
    }
    return NO;
}

- (BOOL) isPerformanceManager {
    @try {
        User *loginUser = [User signedInUser];
        Role *role = [loginUser.roles objectAtIndex:0];
        if (role) {
            return role.isPerformanceManager.boolValue;
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription)
        return NO;
    }
    return NO;
}

+ (BOOL) isLoginUserCanSeePerformanceLeader {
    /* IF USER = SUPER ADMIN & NOT PERFORMANCE MANAGER
     OR USER = NOT PERFORMANCE MANAGER
     THAN ONLY HE/SHE CAN SEE PERFORMANCE LEADERS
     */
    
    if (([[User signedInUser] isSuperAdmin] && ![[User signedInUser] isPerformanceManager]) || ![[User signedInUser] isPerformanceManager]) {
        return YES;
    } else {
        return NO;
    }
}

- (Role *) highestWeightRole {
    if (self.roles != nil && self.roles.count > 0) {
        Role * role = nil;
        for (Role * r in self.roles) {
            if (role == nil) {
                role = r;
            } else {
                if (r.weight.integerValue > role.weight.integerValue) {
                    role = r;
                }
            }
        }
        return role;
    }
    return nil;
}

- (NSString *)avatarUrl {
    if (self.email != nil) {
        return [NSString stringWithFormat:@"%@/avatar", FP_HTTP_URL(FP_SYSTEM_USERS)];
    } else {
        return nil;
    }
}

- (instancetype)init {
    self = [super init];
    if (self != nil) {
        self.gender = @"Male";
    }
    return self;
}

#pragma mark - HKJsonPropertyMappings

- (NSDictionary *)jsonPropertyMappings {
    return @{@"id": @"userID"};
}

- (NSString *)dateFormat
{
    return FP_CONST_DATE_TIME_FORMAT;
}

- (void)handleArrayValue:(NSArray *)array forProperty:(NSString *)property
{
    if ([property isEqualToString:@"roles"]) {
        if (!isNull(array)) {
            NSMutableArray * arr = [NSMutableArray array];
            for (NSDictionary * dict in array) {
                Role * role = [Role new];
                [HKJsonUtils updatePropertiesWithData:dict forObject:role];
                [arr addObject:role];
            }
            self.roles = [NSArray arrayWithArray:arr];
            arr = nil;
        }
    }
}

#pragma mark - Public

static User * currentUser = nil;

- (void)setAsCurrentUser:(BOOL)cacheEabled
{
    if (self.email != nil) {
        currentUser = self;
        if (cacheEabled) {
            BOOL succeeded = [NSKeyedArchiver archiveRootObject:self toFile:FPUserInfoCachePath];
            assert(succeeded);
        }
    } else {
        DLog(@"Failed to set current user, identifier is null.");
    }
}

+ (BOOL)signedIn
{
    return [self signedInUser] != nil;
}

+ (User *)signedInUser {
    if (currentUser == nil) {
        @try {
            User * user = [NSKeyedUnarchiver unarchiveObjectWithFile:FPUserInfoCachePath];
            if (user.email != nil && !isEmptyString(user.password)) {
                currentUser = user;
            }
        } @catch (NSException *exception) {
            DLog(@"Failed to unarchive cached user information. %@", exception);
        }
    }
    
    return currentUser;
}

+ (void)signOut {
    // Remove menu preference
//    [Helper clearPrefrencesAfterLogout];
//    [[ServerDataManager sharedManager] setMenuItems:nil];
//    [[ServerDataManager sharedManager] setMenuURL:nil];
    
    currentUser = nil;
    [[NSFileManager defaultManager] removeItemAtPath:FPUserInfoCachePath error:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:FPUserDidSignOutNotification object:nil];
    
    // Remove all user defauls
    [ServicePreference removeAllPreferences];
}

- (NSDictionary *)serialize
{
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    if (self.userID != nil) {
        [dict setValue:self.userID forKey:@"id"];
    }
    if (self.number != nil) {
        [dict setValue:self.number forKey:@"number"];
    }
    if (!isEmptyString(self.email)) {
        [dict setValue:self.email forKey:@"email"];
    }
    if (!isEmptyString(self.password)) {
        [dict setValue:self.password forKey:@"password"];
    }
    if (!isEmptyString(self.firstName)) {
        [dict setValue:self.firstName forKey:@"firstName"];
    }
    if (!isEmptyString(self.middleName)) {
        [dict setValue:self.middleName forKey:@"middleName"];
    }
    if (!isEmptyString(self.lastName)) {
        [dict setValue:self.lastName forKey:@"lastName"];
    }
    if (self.gender != nil) {
        [dict setValue:self.gender forKey:@"gender"];
    }
    if (!isEmptyString(self.phoneNumber)) {
        [dict setValue:self.phoneNumber forKey:@"phoneNumber"];
    }
    if (self.hiredDate != nil) {
        [dict setValue:[self.hiredDate toNSString:FP_CONST_DATE_TIME_FORMAT] forKey:@"hiredDate"];
    }
    if (!isEmptyString(self.fpgLevel)) {
        [dict setValue:self.fpgLevel forKey:@"fpgLevel"];
    }
    if (self.status != nil) {
        [dict setValue:self.status forKey:@"status"];
    }
    
    if (self.isContributor != nil) {
        [dict setValue:self.isContributor forKey:@"isContributor"];
    }
    
    if (self.isFPGMember != nil) {
        [dict setValue:self.isFPGMember forKey:@"isFPGMember"];
    }
    
    if (self.isOperator != nil) {
        [dict setValue:self.isOperator forKey:@"isOperator"];
    }
    return [NSDictionary dictionaryWithDictionary:dict];
}

//---------------------------------------------------------------

- (NSDictionary *) userParams {
    NSMutableDictionary *mdetails = [NSMutableDictionary dictionary];
    if (!isEmptyString(self.email)) {
        [mdetails setValue:self.email forKey:@"username"];
    }
    
    if (!isEmptyString(self.password)) {
        [mdetails setValue:self.password forKey:@"password"];
    }
    
    [mdetails setValue:@"restapp" forKey:@"client_id"];
    [mdetails setValue:@"restapp" forKey:@"client_secret"];
    [mdetails setValue:@"password" forKey:@"grant_type"];

    NSDictionary *details = [NSDictionary dictionaryWithDictionary:mdetails];
    mdetails = nil;
    return details;
}

//---------------------------------------------------------------


#pragma mark - Object Encoding/Decoding

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.userID forKey:@"id"];
    [aCoder encodeObject:self.number forKey:@"number"];
    [aCoder encodeObject:self.email forKey:@"email"];
    [aCoder encodeObject:self.password forKey:@"password"];
    [aCoder encodeObject:self.firstName forKey:@"firstName"];
    [aCoder encodeObject:self.middleName forKey:@"middleName"];
    [aCoder encodeObject:self.lastName forKey:@"lastName"];
    [aCoder encodeObject:self.gender forKey:@"gender"];
    [aCoder encodeObject:self.phoneNumber forKey:@"phoneNumber"];
    [aCoder encodeObject:self.hiredDate forKey:@"hiredDate"];
    [aCoder encodeObject:self.fpgLevel forKey:@"fpgLevel"];
    [aCoder encodeObject:self.status forKey:@"status"];
    [aCoder encodeObject:self.timestamp forKey:@"timestamp"];
    [aCoder encodeObject:self.updatedTime forKey:@"updatedTime"];
    [aCoder encodeObject:self.accessToken forKey:@"accessToken"];
    [aCoder encodeObject:self.tokenExpiry forKey:@"tokenExpiry"];
    [aCoder encodeObject:self.signedInTime forKey:@"signedInTime"];
    [aCoder encodeObject:self.isContributor forKey:@"isContributor"];
    [aCoder encodeObject:self.isFPGMember forKey:@"isFPGMember"];
    [aCoder encodeObject:self.isOperator forKey:@"isOperator"];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    self.userID = [aDecoder decodeObjectForKey:@"id"];
    self.number = [aDecoder decodeObjectForKey:@"number"];
    self.email = [aDecoder decodeObjectForKey:@"email"];
    self.password = [aDecoder decodeObjectForKey:@"password"];
    self.firstName = [aDecoder decodeObjectForKey:@"firstName"];
    self.middleName = [aDecoder decodeObjectForKey:@"middleName"];
    self.lastName = [aDecoder decodeObjectForKey:@"lastName"];
    self.gender = [aDecoder decodeObjectForKey:@"gender"];
    self.phoneNumber = [aDecoder decodeObjectForKey:@"phoneNumber"];
    self.hiredDate = [aDecoder decodeObjectForKey:@"hiredDate"];
    self.fpgLevel = [aDecoder decodeObjectForKey:@"fpgLevel"];
    self.status = [aDecoder decodeObjectForKey:@"status"];
    self.timestamp = [aDecoder decodeObjectForKey:@"timestamp"];
    self.updatedTime = [aDecoder decodeObjectForKey:@"updatedTime"];
    self.accessToken = [aDecoder decodeObjectForKey:@"accessToken"];
    self.tokenExpiry = [aDecoder decodeObjectForKey:@"tokenExpiry"];
    self.signedInTime = [aDecoder decodeObjectForKey:@"signedInTime"];
    self.isContributor = [aDecoder decodeObjectForKey:@"isContributor"];
    self.isFPGMember = [aDecoder decodeObjectForKey:@"isFPGMember"];
    self.isOperator = [aDecoder decodeObjectForKey:@"isOperator"];
    return self;
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    User * user = [User new];
    user.userID = self.userID;
    user.number = self.number;
    user.email = self.email;
    user.password = self.password;
    user.firstName = self.firstName;
    user.middleName = self.middleName;
    user.lastName = self.lastName;
    user.gender = self.gender;
    user.phoneNumber = self.phoneNumber;
    user.hiredDate = self.hiredDate;
    user.fpgLevel = self.fpgLevel;
    user.status = self.status;
    user.timestamp = self.timestamp;
    user.updatedTime = self.updatedTime;
    user.accessToken = self.accessToken;
    user.tokenExpiry = self.tokenExpiry;
    user.signedInTime = self.signedInTime;
    user.roles = self.roles;
    user.isContributor = self.isContributor;
    user.isFPGMember = self.isFPGMember;
    user.isOperator = self.isOperator;
    return user;
    
}

@end


NSString * const FPUserDidSignInNotification            = @"FPUserDidSignInNotification";

NSString * const FPUserDidSignOutNotification           = @"FPUserDidSignOutNotification";

NSString * const FPAuthenticationFailedNotification     = @"FPAuthenticationFailedNotification";

NSString * const FPPermissionErrorNotification          = @"FPPermissionErrorNotification";

