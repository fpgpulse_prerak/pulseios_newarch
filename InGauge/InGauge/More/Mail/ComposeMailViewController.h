//
//  ComposeMailViewController.h
//  pulse
//
//  Created by Mehul Patel on 04/01/17.
//  Copyright © 2017 frontline performance group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewController.h"

@interface ComposeMailViewController : BaseTableViewController

@property (nonatomic, strong) NSArray                     *seletedRecipientArray;
@property (nonatomic, strong) NSArray                     *userArray;
@property (nonatomic, strong) NSString                    *seletedSubject;
@property (nonatomic, strong) NSString                    *senderID;
@property (nonatomic, strong) NSNumber                    *replyMailID;

@end
