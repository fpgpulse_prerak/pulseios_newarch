//
//  ComposeMailViewController.m
//  pulse
//
//  Created by Mehul Patel on 04/01/17.
//  Copyright © 2017 frontline performance group. All rights reserved.
//

#import "ComposeMailViewController.h"
#import "AnalyticsLogger.h"
#import "FPChooseOptionViewController.h"
#import "WYPopoverController.h"
#import "UserMailListModel+API.h"
#import "RecipientCell.h"
#import "LPlaceholderTextView.h"
#import "EmailUser.h"
#import <MessageUI/MessageUI.h>

#define kButtonTag                  1234
#define kFooterHeight               250.0f
#define kRecipientHeaderHeight      44.0f
#define kSubjectHeaderHeight        50.0f
#define kAttachmentHeaderHeight     70.0f

@interface ComposeMailViewController () <FPChooseOptionViewControllerDelegate, WYPopoverControllerDelegate, UITextViewDelegate, UISearchBarDelegate,MFMailComposeViewControllerDelegate,UIDocumentMenuDelegate, UIDocumentPickerDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) FPChooseOptionViewController      *chooseOptionViewController;
@property (nonatomic, strong) WYPopoverController               *wyPopoverController;
@property (nonatomic, strong) LPlaceholderTextView              *textView;

@property (nonatomic, strong) UITextField                       *subjectText;
@property (nonatomic, strong) UIView                            *footerView;
@property (nonatomic, strong) UIButton                          *recipientButton;
@property (nonatomic, strong) UISearchBar                       *searchBar;
@property (nonatomic, strong) NSArray                           *selectedUsers;
@property (nonatomic, strong) NSArray                           *searchArray;
@property (nonatomic, strong) NSMutableArray                    *recipientArray;
@property (nonatomic, strong) NSMutableArray                    *attachmentArray;

@end

@implementation ComposeMailViewController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
    self.recipientArray = nil;
    self.attachmentArray = nil;
    
    self.wyPopoverController.delegate = nil;
    self.wyPopoverController = nil;
    self.chooseOptionViewController.delegate = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (void) sendButtonTapped:(id)sender {
    
    // Validations
    [self resignAllResponders];
    
    // 1. Recipents
    if (self.recipientArray.count == 0) {
        // Recipients are required
        [Helper showAlertWithTitle:@"" message:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_RECIPIENTS_REQUIRED"] forController:self];
        return;
    }
    
    // 2. Empty Subject
    NSString *subjectString = [Helper removeWhiteSpaceAndNewLineCharacter:self.subjectText.text];
    if (subjectString.length == 0) {
        [Helper showAlertWithTitle:@"" message:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_SUBJECT_REQUIRED"] forController:self];
        return;
    }
    
    // 3. Empty message
    NSString *messageString = [Helper removeWhiteSpaceAndNewLineCharacter:self.textView.text];
    if (messageString.length == 0) {
        [Helper showAlertWithTitle:@"" message:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_MESSAGE_REQUIRED"] forController:self];
        return;
    }
    
    NSDictionary *details = @{
                                MAIL_RECIPIENT : self.recipientArray,
                                MAIL_SUBJECT : subjectString, MAIL_MESSAGE : messageString
                              };
    
    if (self.seletedRecipientArray.count > 0) {
        details = @{
                        MAIL_SUBJECT : subjectString, MAIL_MESSAGE : messageString, @"replyId" : self.replyMailID
                    };
    }
    
    
    if (self.attachmentArray.count > 0) {
        [self uploadAttachmentFile:details];
    } else {
        // Send message service
        [self callWebServiceToSendMailOrReplyMail:details];
    }
}

//---------------------------------------------------------------

- (IBAction) cancelButtonTapped:(id) sender {
    [self resignAllResponders];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_DISCARD_MESSAGE"] preferredStyle:UIAlertControllerStyleAlert];
    
    // Cancel action
    UIAlertAction *actionNo = [UIAlertAction actionWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_NO"] style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:actionNo];
    
    // Yes action
    UIAlertAction *actionYes = [UIAlertAction actionWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_YES"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.subjectText.text = @"";
        self.textView.text = @"";
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alertController addAction:actionYes];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

//---------------------------------------------------------------

- (void) closeButtonTapped:(UIButton *)sender {
    RecipientCell *cell = (RecipientCell *)sender.superview.superview;
    [Helper tapOnControlAnimation:cell.crossImage forScale:0.5 forCompletionBlock:^(BOOL finished) {}];
    @try {
        NSInteger index = sender.tag - kButtonTag;
        NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.selectedUsers];
        [tempArray removeObjectAtIndex:index];
        self.selectedUsers = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
        
        // Remove recipient element
        [self.recipientArray removeObjectAtIndex:index];
        
        // update section
        NSRange range = NSMakeRange(1, 1);
        NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
        [self.tableView reloadSections:section withRowAnimation:UITableViewRowAnimationFade];
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void) attachmentButtonTapped:(id)sender  {
    [self.view endEditing:YES];
    
    // All file type to acsses
    NSArray *types = @[(NSString*)kUTTypeImage, (NSString*)kUTTypeSpreadsheet, (NSString*)kUTTypePresentation, (NSString*)kUTTypeDatabase, (NSString*)kUTTypeFolder, (NSString*)kUTTypeZipArchive, (NSString*)kUTTypeVideo];
    
    UIDocumentMenuViewController *documentController = [[UIDocumentMenuViewController alloc]initWithDocumentTypes:types inMode:UIDocumentPickerModeImport];
    documentController.delegate = self;

    [documentController addOptionWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_GALLERY"] image:nil order:UIDocumentMenuOrderFirst handler:^{
        [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }];
    
    // Create Custom option to display Camera
    [documentController addOptionWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_CAMERA"] image:nil order:UIDocumentMenuOrderFirst handler:^{
        //Call when user select the option
        BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
        if (hasCamera) {
            [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
        } else {
            [Helper showAlertWithTitle:@"" message:[App_Delegate.keyMapping getKeyValue:@"KEY_CAMERA_NOT_AVAILABLE"] forController:self];
        }
    }];
    
    //present the document menu view
    [self presentViewController:documentController animated:YES completion:nil];
}

//---------------------------------------------------------------

- (void) deleteButtonTapped:(UIButton *)sender {
    [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];
    @try {
        if (self.attachmentArray.count > 0) {
            [self.attachmentArray removeObjectAtIndex:sender.tag];
            [self.tableView reloadData];
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WEBSERVICE methods

//---------------------------------------------------------------

- (void) callWebServiceToGetRecipientList {
    // Check for internet
    if (![Helper checkForConnection:self]) {
        return;
    }
    
    [self.chooseOptionViewController showLocationLoadingIndicator];
    [UserMailListModel getRecipients:^(NSError *localizedError, UserMailRecipientList *recipients) {
        
        [self.chooseOptionViewController hideLocationLoadingIndicator];
        self.userArray = recipients.employees;
        [self.chooseOptionViewController reloadOptions];
    }];
}

//---------------------------------------------------------------

- (void) callWebServiceToSendMailOrReplyMail:(NSDictionary *)details {
    
    // Check for internet
    if (![Helper checkForConnection:self]) {
        return;
    }
    
    // Check if it is reply or new mail!
    if (self.replyMailID) {
        MBProgressHUD *hud = [Helper showLoading:self.navigationController.view];
        [UserMailListModel replyUserMail:details whenFinish:^(NSString *localizedError, NSDictionary *details) {
            [Helper hideLoading:hud];
            BOOL isSucess = YES;
            if (details) {
                NSNumber *statusCode = [details objectForKey:@"statusCode"];
                if (statusCode.intValue == 200) {
                    isSucess = YES;
                } else {
                    isSucess = NO;
                }
            } else {
                isSucess = NO;
            }
            
            if (isSucess) {
                [self resignAllResponders];
                [self.navigationController popViewControllerAnimated:YES];
                
                // Showing alert for success
                dispatch_async(dispatch_get_main_queue(), ^{
                    [Helper showAlertWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL"] message:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_SENT_SUCCESS"] forController:self];
                    //Analytics
                    [AnalyticsLogger logReplyMailEvent:details];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [Helper showAlertWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL"] message:@"Failed to reply mail!" forController:self];
                    //Analytics
                    [AnalyticsLogger logFailedReplyMailEvent:details];
                });
            }
        }];
    
    }
    // Send new mail!
    else {
        MBProgressHUD *hud = [Helper showLoading:self.navigationController.view];
        [UserMailListModel sendMailToRecipients:details whenFinish:^(NSString *localizedError, NSDictionary *details) {
            [Helper hideLoading:hud];
            BOOL isSucess = YES;
            if (details) {
                NSNumber *statusCode = [details objectForKey:@"statusCode"];
                if (statusCode.intValue == 200) {
                    isSucess = YES;
                } else {
                    isSucess = NO;
                }
            } else {
                isSucess = NO;
            }

            if (isSucess) {
                [self resignAllResponders];
                [self.navigationController popViewControllerAnimated:YES];
                
                // Showing alert for success
                dispatch_async(dispatch_get_main_queue(), ^{
                    [Helper showAlertWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL"] message:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_SENT_SUCCESS"] forController:self];
                    //Analytics
                    [AnalyticsLogger logEmailSentEvent:details];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [Helper showAlertWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL"] message:@"Failed to send mail!" forController:self];
                    //Analytics
                    [AnalyticsLogger logEmailFailedEvent:details];
                });
            }
        }];
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) uploadAttachmentFile:(NSDictionary *)details {
    
    NSMutableDictionary *tempDetails = [[NSMutableDictionary alloc] initWithDictionary:details];
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];

    MBProgressHUD *hud = [Helper showLoading:self.navigationController.view];
    for (NSDictionary *dict in self.attachmentArray) {
        [ServiceManager postUploadWebServiceCallwithParam:UPLOAD_ATTACHMENT parameters:dict whenFinish:^(NSString *errorMessage, NSDictionary *response) {
            [Helper hideLoading:hud];
            if (errorMessage == nil) {
                //Analytics
                [AnalyticsLogger logEmailAttachmentEvent:dict];
                
                [tempArray addObject:[[response objectForKey:MAIN_BODY] objectForKey:@"mailAttachmentID"]];
                if (self.attachmentArray.count == tempArray.count) {
                    [tempDetails setObject:tempArray forKey:@"mailFileList"];
                    [self callWebServiceToSendMailOrReplyMail:tempDetails];
                }
            }
        }];
    }
    tempArray = nil;
    tempDetails = nil;
}

//---------------------------------------------------------------

- (void) showPopoverController:(UIViewController *)controller fromControl:(UIView *)control {
    
    if ([self.wyPopoverController isPopoverVisible]) {
        [self.wyPopoverController dismissPopoverAnimated:YES];
    }
    
    @try {
        // Hide searchbar for scrollView
        if ([control isKindOfClass:[UISearchBar class]] || [control isKindOfClass:NSClassFromString(@"UISearchBarTextField")]) {
            [self.chooseOptionViewController hideSearchBar:YES];
        } else {
            [self.chooseOptionViewController hideSearchBar:NO];
        }
    }
    @catch (NSException *exception) {
        [self.chooseOptionViewController hideSearchBar:YES];
    }
    
    self.wyPopoverController.passthroughViews = @[control];
    self.wyPopoverController.popoverLayoutMargins = UIEdgeInsetsMake(10, 10, 10, 10);
    self.wyPopoverController.wantsDefaultContentAppearance = NO;
    [self.wyPopoverController presentPopoverFromRect:control.bounds
                                              inView:control
                            permittedArrowDirections:WYPopoverArrowDirectionUp
                                            animated:YES
                                             options:WYPopoverAnimationOptionFadeWithScale];
}

//---------------------------------------------------------------

- (void) addTableFooterView {
    // Table footer view
    _footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, kFooterHeight)];
    self.footerView.backgroundColor = [UIColor whiteColor];
    
    // Message label
    UILabel *messageLabel = [[UILabel alloc] init];
    messageLabel.translatesAutoresizingMaskIntoConstraints = NO;
    messageLabel.textColor = [UIColor lightGrayColor];
    messageLabel.font = [UIFont fontWithName:AVENIR_FONT size:14];
    messageLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.footerView addSubview:messageLabel];
    
    _textView = [[LPlaceholderTextView alloc] init];
    self.textView.delegate = self;
    self.textView.translatesAutoresizingMaskIntoConstraints = NO;
    self.textView.textColor = [UIColor blackColor];
    self.textView.backgroundColor = [UIColor clearColor];
    self.textView.font = [UIFont fontWithName:AVENIR_FONT size:16];
    self.textView.placeholderText = [App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_WRITE_MESSAGE"];
    self.textView.placeholderColor = [UIColor lightGrayColor];
    [self.footerView addSubview:self.textView];
    
    messageLabel.text = [App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_MESSAGE_TITLE"];
    
    NSDictionary *views = @{
                            @"messageLabel" : messageLabel,
                            @"textView" : self.textView,
                            };
    [self.footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[messageLabel]-60-|" options: 0 metrics:nil views:views]];
    [self.footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-9-[textView]-0-|" options: 0 metrics:nil views:views]];
    [self.footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[messageLabel]-5-[textView]-|" options: 0 metrics:nil views:views]];
    
    [self addBottomLine:self.footerView];
    
    self.tableView.tableFooterView = self.footerView;
}

//---------------------------------------------------------------

- (void) addTopLine:(UIView *)headerView {
    // Add line
    UIView *lineView = [[UIView alloc] init];
    [lineView setBackgroundColor:TABLE_SEPERATOR_COLOR];
    [lineView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [headerView addSubview:lineView];
    
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[lineView]|" options: 0 metrics:nil views:@{@"lineView" : lineView}]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[lineView(0.5)]" options: 0 metrics:nil views:@{@"lineView" : lineView}]];
}

//---------------------------------------------------------------

- (void) addBottomLine:(UIView *)headerView {
    // Add line
    UIView *lineView = [[UIView alloc] init];
    [lineView setBackgroundColor:TABLE_SEPERATOR_COLOR];
    [lineView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [headerView addSubview:lineView];
    
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[lineView]|" options: 0 metrics:nil views:@{@"lineView" : lineView}]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[lineView(0.5)]-0-|" options: 0 metrics:nil views:@{@"lineView" : lineView}]];
}

//---------------------------------------------------------------

- (UIView *) createRecipientHeader {
    // Table Header view
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, kRecipientHeaderHeight)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    // Reply label
    UILabel *toLabel = [[UILabel alloc] init];
    toLabel.translatesAutoresizingMaskIntoConstraints = NO;
    toLabel.textColor = [UIColor lightGrayColor];
    toLabel.font = [UIFont fontWithName:AVENIR_FONT size:14];
    toLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [headerView addSubview:toLabel];
    
    _searchBar = [[UISearchBar alloc] init];
    self.searchBar.translatesAutoresizingMaskIntoConstraints = NO;
    self.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    self.searchBar.delegate = self;

    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setDefaultTextAttributes:@{
                                                                                                 NSForegroundColorAttributeName:[UIColor blackColor],
                                                                                                 NSFontAttributeName : [UIFont fontWithName:AVENIR_FONT size:14]
                                                                                                 }];
    [self.searchBar setReturnKeyType:UIReturnKeyDone];
    [headerView addSubview:self.searchBar];
    
    if (self.seletedRecipientArray.count > 0) {
        [self.searchBar setUserInteractionEnabled:NO];
    }
    
    toLabel.text = [App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_TO_TITLE"];
    self.searchBar.placeholder = [App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_SEARCH_RECIPIENTS"];
    
    NSDictionary *views = @{
                                @"toLabel" : toLabel,
                                @"searchBar" : self.searchBar,
                            };
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-14-[toLabel]-5-[searchBar]-|" options: 0 metrics:nil views:views]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[toLabel]|" options: 0 metrics:nil views:views]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[searchBar]|" options: 0 metrics:nil views:views]];
    return headerView;
}

//---------------------------------------------------------------

- (UIView *) createSubjectHeader {
    
    // Table Header view
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, kSubjectHeaderHeight)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    // Subject label
    UILabel *subjectLabel = [[UILabel alloc] init];
    subjectLabel.translatesAutoresizingMaskIntoConstraints = NO;
    subjectLabel.textColor = [UIColor lightGrayColor];
    subjectLabel.font = [UIFont fontWithName:AVENIR_FONT size:14];
    subjectLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [headerView addSubview:subjectLabel];
    
    if (_subjectText == nil) {
        _subjectText = [[UITextField alloc] init];
        self.subjectText.translatesAutoresizingMaskIntoConstraints = NO;
        self.subjectText.backgroundColor = [UIColor clearColor];
        [self.subjectText setFont:[UIFont fontWithName:AVENIR_FONT size:14]];
        [self.subjectText setTextColor:[UIColor blackColor]];
    }
    [headerView addSubview:self.subjectText];
    
    if (self.seletedSubject != nil) {
        self.subjectText.text = self.seletedSubject;
        self.subjectText.enabled = NO;
    }
    
    subjectLabel.text = [App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_SUBJECT_TITLE"];
    
    NSDictionary *views = @{ @"subjectLabel" : subjectLabel, @"subjectText" : self.subjectText};
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-14-[subjectLabel]-|" options: 0 metrics:nil views:views]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-14-[subjectText]-5-|" options: 0 metrics:nil views:views]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[subjectLabel(20)]-0-[subjectText]|" options: 0 metrics:nil views:views]];
    
    [self addTopLine:headerView];
    [self addBottomLine:headerView];
    return headerView;
}

//---------------------------------------------------------------

- (UIView *) createAttachmentView {
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, kAttachmentHeaderHeight)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    if (self.attachmentArray.count > 0) {
        UILabel *subjectLabel = [[UILabel alloc] initWithFrame:CGRectMake(14, 0, self.tableView.frame.size.width, 15)];
        subjectLabel.textColor = [UIColor lightGrayColor];
        subjectLabel.font = [UIFont fontWithName:AVENIR_FONT size:9];
        subjectLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        subjectLabel.text = [NSString stringWithFormat:@"%@ (%lu)",[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_ATTACHMENTS"],(unsigned long)self.attachmentArray.count];
        [headerView addSubview:subjectLabel];
        
        UIScrollView * attchScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(14, 15, self.tableView.frame.size.width - 15, 60)];
        
        [headerView addSubview:attchScroll];
        
        int X = 10;
        int Y = 5;
        
        for (int i = 0; i < self.attachmentArray.count; i++) {
            
            UIButton *documentButton = [[UIButton alloc] initWithFrame:CGRectMake(X, Y, 35, 35)];
            [documentButton setBackgroundImage:[UIImage imageNamed:@"doc_icon"] forState:UIControlStateNormal];
            [documentButton setTitle:[[NSString stringWithFormat:@"%@",[[self.attachmentArray objectAtIndex:i] valueForKey:@"fileType"]] uppercaseString] forState:UIControlStateNormal];
            documentButton.titleLabel.font = [UIFont fontWithName:AVENIR_FONT_HEAVY size:10.0];
            documentButton.tag = i;
            [attchScroll addSubview:documentButton];
            
            UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(X, 42, 40, 20)];
            nameLabel.font = [UIFont fontWithName:AVENIR_FONT_LIGHT size:6.5];
            nameLabel.text = [NSString stringWithFormat:@"%@",[[self.attachmentArray objectAtIndex:i] valueForKey:@"fileName"]];
            nameLabel.numberOfLines = 0;
            nameLabel.textAlignment = NSTextAlignmentCenter;
            [attchScroll addSubview:nameLabel];
            
            UIButton *deleteButton = [[UIButton alloc] initWithFrame:CGRectMake(25, 20, 15, 15)];
            [deleteButton setImage:[UIImage imageNamed:TRASH_IMAGE] forState:UIControlStateNormal];
            [deleteButton addTarget:self action:@selector(deleteButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            deleteButton.tag = i;
            deleteButton.tintColor = APP_COLOR;
            [documentButton addSubview:deleteButton];
            
            X += 60;
        }
        [attchScroll setContentSize:CGSizeMake(X, attchScroll.frame.size.height)];
    }
    return headerView;
}

//---------------------------------------------------------------
- (void) resignAllResponders {
    [self.subjectText resignFirstResponder];
    [self.textView resignFirstResponder];
    [self.searchBar resignFirstResponder];
    if (self.wyPopoverController.isPopoverVisible) {
        [self.wyPopoverController dismissPopoverAnimated:YES];
    }
}

//---------------------------------------------------------------

- (void) showPopover:(UISearchBar *)sender {
    [self.subjectText resignFirstResponder];
    [self.textView resignFirstResponder];
    
    self.chooseOptionViewController.view.frame = CGRectMake(0, 0, self.view.bounds.size.width - 40, self.view.bounds.size.height/2 - 40);
    self.chooseOptionViewController.optionType = FPOptionCoachingType;
    
    @try {
        UIView *popoverDirectionView = sender;
        // Set the return key and keyboard appearance of the search bar
        for (UIView *searchBarSubview in [sender subviews]) {
            for (UIView *innerView in [searchBarSubview subviews]) {
                if ([innerView isKindOfClass:NSClassFromString(@"UISearchBarTextField")]) {
                    popoverDirectionView = innerView;
                }
            }
        }
        [self showPopoverController:self.chooseOptionViewController fromControl:popoverDirectionView];
    } @catch (NSException *exception) {
        [self showPopoverController:self.chooseOptionViewController fromControl:sender];
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

//---------------------------------------------------------------

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0: return self.seletedSubject == nil ? kRecipientHeaderHeight : 0; break;
        case 1: return 0; break;
        case 2: return kSubjectHeaderHeight; break;
        case 3: return self.attachmentArray.count > 0 ? kAttachmentHeaderHeight: 0; break;
        default: return 0; break;
    }
    return 0;
}

//---------------------------------------------------------------

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40.0f;
}

//---------------------------------------------------------------

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (section == 1) ? self.selectedUsers.count : 0;
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"MailContentCellIdentifier%d%d", (int)indexPath.section, (int)indexPath.row];
    RecipientCell *cell = (RecipientCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[RecipientCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    @try {
        // Recipient name
        cell.recipientLabel.text = [self.selectedUsers objectAtIndex:indexPath.row];
        [cell.closeButton addTarget:self action:@selector(closeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        cell.closeButton.tag = kButtonTag + indexPath.row;
        
        if (self.seletedRecipientArray.count > 0) {
            cell.closeButton.hidden = YES;
            cell.crossImage.hidden = YES;
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception);
    }
    return cell;
}

//---------------------------------------------------------------

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0: return [self createRecipientHeader]; break;
        case 1: return nil; break;
        case 2: return [self createSubjectHeader]; break;
        case 3: return [self createAttachmentView]; break;
        default: return nil; break;
    }
    return nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark FPChooseOptionViewControllerDelegate methods

//---------------------------------------------------------------

- (NSArray *)availableOptionsForController:(FPChooseOptionViewController *)controller {
    NSArray *dataArray = self.userArray;
    NSString *searchText = [Helper removeWhiteSpaceAndNewLineCharacter:self.searchBar.text];
    if (searchText.length > 0) {
        dataArray = self.searchArray;
    }
    
    if (controller.optionType == FPOptionCoachingType) {
        return dataArray;
    }
    return nil;
}

//---------------------------------------------------------------

- (NSString *)chooseOptionViewController:(FPChooseOptionViewController *)controller textForOptionAtIndex:(NSInteger)index {
    
    NSArray *dataArray = self.userArray;
    NSString *searchText = [Helper removeWhiteSpaceAndNewLineCharacter:self.searchBar.text];
    if (searchText.length > 0) {
        dataArray = self.searchArray;
    }
    
    if (controller.optionType == FPOptionCoachingType) {
        EmailUser *user = dataArray[index];
        NSString *usernameAndEmailString = [NSString stringWithFormat:@"%@ [%@]", [user name], user.email];
        return usernameAndEmailString;
    }
    return nil;
}

//---------------------------------------------------------------

- (BOOL)chooseOptionViewController:(FPChooseOptionViewController *)controller shouldMarkOptionAsSelected:(id)option {
    return NO;
}

//---------------------------------------------------------------

- (BOOL)chooseOptionViewController:(FPChooseOptionViewController *)controller shouldDisableOption:(id)option {
    BOOL isReadOnly = NO;
    if (controller.optionType == FPOptionCoachingType) {
        EmailUser *selectedUser = (EmailUser *)option;
        NSString *selectedUserName = [NSString stringWithFormat:@"%@ [%@]", [selectedUser name], selectedUser.email];
        
        for (NSString *name in self.selectedUsers) {
            if ([name isEqualToString:selectedUserName]) {
                isReadOnly = YES;
            }
        }
    }
    return isReadOnly;
}

//---------------------------------------------------------------

- (void)chooseOptionViewController:(FPChooseOptionViewController *)controller didSelectOption:(id)option {
    self.searchBar.text = @"";
//    self.userArray = nil;
    [self resignAllResponders];
    
    if (controller.optionType == FPOptionCoachingType) {
        EmailUser *selectedUser = (EmailUser *)option;
        NSString *selectedUserName = [NSString stringWithFormat:@"%@ [%@]", [selectedUser name], selectedUser.email];
        NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.selectedUsers];
        BOOL isContainsObject = NO;
        for (NSString *name in tempArray) {
            if ([selectedUserName isEqualToString:name]) {
                isContainsObject = YES;
                break;
            }
        }
        
        if (!isContainsObject) {
            [tempArray addObject:selectedUserName];
            // Add user id to recipient list
            [self.recipientArray addObject:selectedUser.userId];
        }
        self.selectedUsers = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
        
        // Reload only second section
        @try {
            NSRange range = NSMakeRange(1, 1);
            NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
            [self.tableView reloadSections:section withRowAnimation:UITableViewRowAnimationFade];
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITextView delegate methods

//---------------------------------------------------------------

- (void) textViewDidBeginEditing:(UITextView *)textView {
    [UIView animateWithDuration:0.3 animations:^{
        [self.tableView setContentOffset:CGPointMake(0, self.tableView.contentOffset.y + kFooterHeight + 100)];
        [self.tableView scrollRectToVisible:[self.tableView convertRect:self.tableView.tableFooterView.bounds fromView:self.tableView.tableFooterView] animated:NO];
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WYPopoverControllerDelegate methods

//---------------------------------------------------------------

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)popoverController {
    @try {
        if ([self.searchBar isFirstResponder]) {
            [self.searchBar resignFirstResponder];
        }
        return YES;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return YES;
    }
}

//---------------------------------------------------------------

- (void)popoverControllerDidPresentPopover:(WYPopoverController *)popoverController {
}

//---------------------------------------------------------------

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)popoverController {
    if ([self.searchBar isFirstResponder]) {
        [self.searchBar resignFirstResponder];
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UISearchBar delegate methods

//---------------------------------------------------------------

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    if (!self.wyPopoverController.isPopoverVisible) {
        [self showPopover:self.searchBar];
    }
    return YES;
}

//---------------------------------------------------------------

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSMutableString *textToBe = [NSMutableString stringWithString:searchBar.text];
    [textToBe replaceCharactersInRange:range withString:text];
    if (textToBe.length > 60) {
        return NO;
    }
    return YES;
}

//---------------------------------------------------------------

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    @try {
        if (isEmptyString(searchText)) {
            self.searchArray = self.userArray;
        } else {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.name CONTAINS[cd] %@", searchText];
            NSArray *filterArray = [self.userArray filteredArrayUsingPredicate:predicate];
            self.searchArray = filterArray;
            [self.chooseOptionViewController reloadOptions];
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

//---------------------------------------------------------------

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    return YES;
}

//---------------------------------------------------------------

- (void) searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UIDocumentPickerViewController methods

//---------------------------------------------------------------

- (void)documentMenu:(UIDocumentMenuViewController *)documentMenu didPickDocumentPicker:(UIDocumentPickerViewController *)documentPicker {
    documentPicker.delegate = self;
    [self presentViewController:documentPicker animated:YES completion:nil];
}

//---------------------------------------------------------------

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url {
    if (controller.documentPickerMode == UIDocumentPickerModeImport) {
        
        // Condition called when user download the file
        // NSData of the content that was downloaded - Use this to upload on the server or save locally in directory
        NSData *fileData = [NSData dataWithContentsOfURL:url];
        NSString *fileName = [url lastPathComponent];
        NSString *fileExtension = [url pathExtension];
        NSString *url = [self saveDocInLocal:fileData FileName:fileName];
        
        NSDictionary *docDict = @{
                                    @"fileName":fileName,
                                    @"fileType": fileExtension,
                                    @"url" : [NSString stringWithFormat:@"%@",url]
                                  };
        
        [self.attachmentArray addObject:docDict];
        [self.tableView reloadData];
        
        //Showing alert for success
        dispatch_async(dispatch_get_main_queue(), ^{
//            [Helper showAlertWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL"] message:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_ATTACHMENTS_SUCCESS"] forController:self];
        });
 
    } else if (controller.documentPickerMode == UIDocumentPickerModeExportToService) {
        // Called when user uploaded the file - Display success alert
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [Helper showAlertWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL"] message:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_UPLOAD_SUCCESS"] forController:self];
//        });
    }
}

//---------------------------------------------------------------

- (void)documentMenuWasCancelled:(UIDocumentMenuViewController *)documentMenu {
}

//---------------------------------------------------------------

- (void)documentPickerWasCancelled:(UIDocumentPickerViewController *)controller {
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UIImagePickerController delegate methods

//---------------------------------------------------------------

- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    imagePickerController.sourceType = sourceType;
    imagePickerController.delegate = self;
    imagePickerController.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie,(NSString *)kUTTypeVideo,(NSString *)kUTTypeImage,nil];
    imagePickerController.allowsEditing = YES;
    
    if (sourceType == UIImagePickerControllerSourceTypeCamera) {
        imagePickerController.showsCameraControls = YES;
    }
    
    UIViewController *topC = App_Delegate.window.rootViewController.topViewController;
    [topC presentViewController:imagePickerController animated:YES completion:nil];
}

//---------------------------------------------------------------

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSString *fileExtension = @"PNG";
    NSString *urlStr = @"";
    NSString *fileName = @"";
    
    if ([info[UIImagePickerControllerMediaType] isEqual:@"public.image"]) {
        UIImage *editedImage = info[UIImagePickerControllerEditedImage];
        fileName = [NSString stringWithFormat:@"attach_%f.PNG",[[NSDate date] timeIntervalSince1970]];
        urlStr = [self saveImage:editedImage ImageName:fileName];
    } else {
        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        fileExtension = [videoURL pathExtension];
        fileName = [NSString stringWithFormat:@"attach_%f.%@",[[NSDate date] timeIntervalSinceNow],fileExtension];
        urlStr = [self saveDocInLocal:[NSData dataWithContentsOfURL:videoURL] FileName:fileName];
    }
    
    NSDictionary *docDict = @{
                                @"fileName":fileName,
                                @"fileType": fileExtension,
                                @"url" : [NSString stringWithFormat:@"%@", urlStr]
                              };
    
    [self.attachmentArray addObject:docDict];
    [self.tableView reloadData];
    
    //Showing alert for success
    dispatch_async(dispatch_get_main_queue(), ^{
//        [Helper showAlertWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL"] message:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_ATTACHMENTS_SUCCESS"] forController:self];
    });
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
//---------------------------------------------------------------

- (NSString *)saveImage:(UIImage *)selectedImage ImageName:(NSString *)imageName {
    NSString *imagePath = @"";
    if (selectedImage != nil) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",imageName]];
        NSData* data = UIImagePNGRepresentation(selectedImage);
        [data writeToFile:imagePath atomically:YES];
    }
    return imagePath;
}

//---------------------------------------------------------------

- (NSString *)saveDocInLocal:(NSData *)selectedDocData FileName:(NSString *)fileName {
    
    NSString *path = @"";
    if (selectedDocData != nil) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSError *error = nil;
        path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",fileName]];
        [selectedDocData writeToFile:path options:NSDataWritingAtomic error:&error];
    }
    return path;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    
    //Analytics
    [AnalyticsLogger logEventWithName:@"compose_mail"];
    
    self.title = [App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_COMPOSE"];
    self.selectedUsers = nil;
    
    _recipientArray = [NSMutableArray new];
    _attachmentArray = [NSMutableArray new];
    
    // Init option controller
    _chooseOptionViewController = [[FPChooseOptionViewController alloc] init];
    self.chooseOptionViewController.delegate = self;
    [self.chooseOptionViewController hideSearchBar:YES];
    [self.chooseOptionViewController initialSetup];

    // popover
    _wyPopoverController = [[WYPopoverController alloc] initWithContentViewController:self.chooseOptionViewController];
    self.wyPopoverController.delegate = self;

    [self addTableFooterView];
    
    self.tableView.estimatedRowHeight = 100;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Navigation items
    UIBarButtonItem *attachmentButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"attachment"] style:UIBarButtonItemStylePlain target:self action:@selector(attachmentButtonTapped:)];
    
    UIBarButtonItem *sendButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:SEND_IMAGE] style:UIBarButtonItemStylePlain target:self action:@selector(sendButtonTapped:)];
    self.navigationItem.rightBarButtonItems = @[sendButton,attachmentButton];
    
    if (self.seletedRecipientArray.count > 0) {
        self.selectedUsers = [NSArray arrayWithArray:self.seletedRecipientArray];
        [self.recipientArray addObject:self.senderID];
        self.title = [App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_REPLY_TITLE"];
    } else {
        
        // Create new mail
        if (self.userArray.count == 0) {
            // Call service to get recipients
            [self callWebServiceToGetRecipientList];
        }
    }
}

//---------------------------------------------------------------

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setToolbarHidden:NO animated:YES];
    [self.navigationController.toolbar setBarTintColor:APP_COLOR];
    [self.navigationController.toolbar setBackgroundColor:APP_COLOR];
    [self.navigationController.toolbar setTintColor:[UIColor whiteColor]];
    [self.navigationController.toolbar setTranslucent:YES];
}

//---------------------------------------------------------------

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.subjectText resignFirstResponder];
    [self.textView resignFirstResponder];
    [self.searchBar resignFirstResponder];
    [self.navigationController setToolbarHidden:YES animated:YES];
}

//---------------------------------------------------------------

- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

//---------------------------------------------------------------

@end
