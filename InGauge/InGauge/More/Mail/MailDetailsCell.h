//
//  MailDetailsCell.h
//  IN-Gauge
//
//  Created by Bharat Chandera on 11/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MailDetailsCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIView             *mainView;
@property (nonatomic, weak) IBOutlet UIScrollView       *attchScroll;
@property (nonatomic, weak) IBOutlet UIImageView        *imgUserPic;
@property (nonatomic, weak) IBOutlet UIButton           *btnMore;
@property (nonatomic, weak) IBOutlet UILabel            *lblSenderName;
@property (nonatomic, weak) IBOutlet UILabel            *lblReceiverName;
@property (nonatomic, weak) IBOutlet UILabel            *lblEmailSubject;
@property (nonatomic, weak) IBOutlet UILabel            *lblEmailDate;
@property (nonatomic, weak) IBOutlet UILabel            *lblEmailContent;
@property (nonatomic, weak) IBOutlet UIButton           *imgAttachment;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *emailContentHeight;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *scrollHeight;
@property (nonatomic, assign) BOOL                      isOpen;

@end
