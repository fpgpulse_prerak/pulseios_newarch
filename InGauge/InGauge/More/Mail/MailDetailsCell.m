//
//  MailDetailsCell.m
//  IN-Gauge
//
//  Created by Bharat Chandera on 11/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "MailDetailsCell.h"

@implementation MailDetailsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.imgUserPic.layer.cornerRadius = 22;
    self.imgUserPic.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
