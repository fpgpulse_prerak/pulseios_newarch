//
//  MailDetailsController.h
//  IN-Gauge
//
//  Created by Bharat Chandera on 10/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserMailModel.h"

@interface MailDetailsController : UITableViewController

@property (nonatomic, strong) UserMailModel         *mailModel;
@property (nonatomic, strong) NSString              *mailType;

@end
