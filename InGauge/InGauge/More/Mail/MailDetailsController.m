//
//  MailDetailsController.m
//  IN-Gauge
//
//  Created by Bharat Chandera on 10/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "MailDetailsController.h"
#import "MailDetailsCell.h"
#import "AppDelegate.h"
#import "UserMailListModel+API.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ComposeMailViewController.h"
#import "AnalyticsLogger.h"
#import <QuickLook/QuickLook.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "NSString+HTML.h"
#import "MailDetails.h"
#import "PreviewController.h"

@interface MailDetailsController ()

@property (nonatomic, strong) UIView                *noRecordsView;
@property (nonatomic, strong) PreviewController     *previewController;
@property (nonatomic, strong) NSURL                 *url;
@property (nonatomic, strong) NSArray               *dataArray;
@property (nonatomic, strong) MailDetails           *mailDetails;

@end

@implementation MailDetailsController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc {
    // Remove delegates, mutable array and observers here!
    self.previewController.dataSource = nil;
    self.previewController.delegate = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (void) replyButtonTapped:(id)sender {
    if (self.dataArray.count > 0) {
        UserMessageList *mailDetail = [self.dataArray firstObject];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ComposeMailViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"ComposeMailViewController"];
        controller.seletedSubject = self.mailDetails.subject;
        controller.seletedRecipientArray = @[mailDetail.sender.email];
        controller.senderID = mailDetail.sender.userID.stringValue;
        controller.replyMailID = mailDetail.userMessageId;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

//---------------------------------------------------------------

- (void) documentButtonTapped:(UIButton *)sender {
    [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];
    
    @try {
        UserMessageList *emailDetails   = [self.dataArray objectAtIndex:sender.superview.tag];
        MailAttachment *attchment       = [emailDetails.mobileMailAttachmentList objectAtIndex:sender.tag];
        
        // Remove extention
        NSString *downLoadFileName = [NSString stringWithFormat:@"%@", attchment.fileName];
        NSString *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        NSString *FilePath = [documentsPath stringByAppendingPathComponent:downLoadFileName];
        
        // Create file path to upload document
        self.url = [NSURL fileURLWithPath:FilePath isDirectory:NO];

        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.previewController];
        [navigationController.navigationBar setBarTintColor:APP_COLOR];
        [navigationController.navigationBar setTintColor:[UIColor whiteColor]];
        navigationController.modalPresentationStyle = UIModalPresentationFullScreen;
        self.previewController.fileURL = self.url;
        [self.navigationController pushViewController:self.previewController animated:YES];
    } @catch (NSException *exception) {
        DLog(@"Exception :%@",exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void) moreButtonTapped:(UIButton *)sender {
    [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];

    MailDetailsCell *cell = (MailDetailsCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    UserMessageList *mailDetails = [self.dataArray objectAtIndex:sender.tag];
    UserMessage *message = mailDetails.userMessage;
    
    [self.tableView beginUpdates];
    if (cell.isOpen) {
        
        // Remove html tags
        NSString *userMessage = [message.message stringByConvertingHTMLToPlainText];
        cell.lblEmailContent.text = userMessage;
        
        cell.isOpen = NO;
        [cell.btnMore setTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_MORE"] forState:UIControlStateNormal];
        cell.emailContentHeight.constant = 20;
        cell.scrollHeight.constant = 0;
        [cell updateFocusIfNeeded];
    } else {
        // Set attributed text
        cell.lblEmailContent.attributedText = [message convertHtmlContentToAttributedString];
        
        // Calculate dynamic label height
        CGRect rect = [cell.lblEmailContent.attributedText boundingRectWithSize:CGSizeMake(cell.lblEmailContent.frame.size.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        
        [cell.btnMore setTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_LESS"] forState:UIControlStateNormal];
        cell.emailContentHeight.constant = rect.size.height + 10;
        if (cell.attchScroll.subviews.count != 2) {
            cell.scrollHeight.constant = 70;
        }
        [cell updateFocusIfNeeded];
        cell.isOpen = YES;
    }
    [self.tableView endUpdates];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WEBSERVICE methods

//---------------------------------------------------------------

- (void) callWebServiceToGetMailDetails {
    
    // Check for internet
    if (![Helper checkForConnection:self]) {
        return;
    }
    
    NSDictionary *params = @{ @"id" : self.mailModel.userMailId, @"type" :self.mailType};
    MBProgressHUD *hud = [Helper showLoading:self.view];
    [UserMailListModel getMaildetails:params whenFinish:^(NSString *localizedError, NSDictionary *details) {
        [Helper hideLoading:hud];
        
        if (details) {
            @try {
                self.mailDetails = [MailDetails new];
                NSDictionary *mailDetail = details[MAIN_BODY];
                [HKJsonUtils updatePropertiesWithHTMLContent:mailDetail forObject:self.mailDetails];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    // Update menu count
                    [[NSNotificationCenter defaultCenter] postNotificationName:kMenuUpdateMailCountNotification object:nil];
                                    
                    [self createMailChaining];
                });
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
            }
        }
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) createMailChaining {
    MBProgressHUD *hud = [Helper showLoading:self.navigationController.view];
    // Now this is interesting !!!! 🤔
    // We are now creating queue to iterate mail threads!
//    dispatch_queue_t myQueue = dispatch_queue_create("mail_chain_queue",NULL);
//    dispatch_async(myQueue, ^{
        // Set title of controller
    self.title = self.mailModel.subject;
        // Perform long running process
    NSMutableArray *tempArray = [NSMutableArray new];
    UserMessageList *list = self.mailDetails.userMessageRecipientList.count > 0 ? self.mailDetails.userMessageRecipientList[0] : nil;
    
    if (!list) {
        return;
    }
    tempArray = [self addMailObject:tempArray forMailObject:list];
    self.dataArray = [NSArray arrayWithArray:tempArray];
    tempArray = nil;
    
    
    // Please note that you can only reply if below condition is satisfied
    if (self.dataArray.count > 0) {
        // Check that loginUserId == recipientId && hasReplies == false
        UserMessageList *mailDetail = [self.dataArray objectAtIndex:0];
        if ((mailDetail.recipient.userID.intValue == [User signedInUser].userID.intValue) && (mailDetail.hasReplies.boolValue == NO)) {
            // You can reply now!
        } else {
            // You can not reply
            self.navigationItem.rightBarButtonItem = nil;
        }
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        // Update the UI
        [Helper hideLoading:hud];
        [self.tableView reloadData];
    });
//    });
}

//---------------------------------------------------------------

- (NSMutableArray *) addMailObject:(NSMutableArray *)tempArray forMailObject:(UserMessageList *)mailList {
    // This take hell of time to run, screw NSAttributedString! 😪
    [tempArray addObject:mailList];
    [mailList.userMessage convertHtmlContentToAttributedString];
    
    // Break condition to stop recursion
    if (mailList.parentMessageObject) {
        tempArray = [self addMailObject:tempArray forMailObject:mailList.parentMessageObject];
    }
    return tempArray;
}

//---------------------------------------------------------------

- (void) createAttachmentView:(NSArray *)attachmentArray Cell:(MailDetailsCell *)cell {
    if (cell.attchScroll.subviews.count == 2) {
        int X = 10;
        int Y = 5;
        
        for (int index = 0; index < attachmentArray.count; index++) {
            MailAttachment *attachment = [attachmentArray objectAtIndex:index];
            UIButton *btnDoct = [[UIButton alloc] initWithFrame:CGRectMake(X, Y, 40, 40)];
            [btnDoct setBackgroundImage:[UIImage imageNamed:@"doc_icon"] forState:UIControlStateNormal];
            [btnDoct setTitle:[[NSString stringWithFormat:@"%@", attachment.fileType] uppercaseString] forState:UIControlStateNormal];
            btnDoct.titleLabel.font = [UIFont fontWithName:AVENIR_NEXT_DEMI_BOLD_FONT size:8.0];
            [btnDoct addTarget:self action:@selector(documentButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            btnDoct.tag = index;
            [cell.attchScroll addSubview:btnDoct];
            
            UILabel *docName = [[UILabel alloc] initWithFrame:CGRectMake(X, 42, 40, 25)];
            docName.font = [UIFont fontWithName:AVENIR_FONT_LIGHT size:7.5];
            docName.text = [NSString stringWithFormat:@"%@", attachment.fileName];
            docName.numberOfLines = 0;
            docName.lineBreakMode = NSLineBreakByTruncatingTail;
            docName.textAlignment = NSTextAlignmentCenter;
            [cell.attchScroll addSubview:docName];
            
            X += 60;
            
            [ServiceManager postDownloadWebServiceCallwithURL:[NSString stringWithFormat:@"%d", (int)attachment.attachmentId.intValue] parameters:nil whenFinish:^(NSString *errorMessage, NSDictionary *response) {
            }];
        }
        [cell.attchScroll setContentSize:CGSizeMake(X, cell.attchScroll.frame.size.height)];
    }
}

//---------------------------------------------------------------

- (NSString *) utcDateFromIntervalForDate:(long long)dateTimeStamp {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(dateTimeStamp / 1000.0)];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:MAIL_DATE_TIME_FORMAT];
    return [dateFormatter stringFromDate:date];
}

//---------------------------------------------------------------

- (CGFloat)getLabelHeight:(UILabel*)label {
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    return size.height;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

//---------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return self.arrMails.count;
    return self.dataArray.count;
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MailDetailsCell *cell = (MailDetailsCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.btnMore.tag = indexPath.row;
    [cell.btnMore addTarget:self action:@selector(moreButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    @try {
        UserMessageList *mailDetails = [self.dataArray objectAtIndex:indexPath.row];

        // Download user profile
        NSString *action = [NSString stringWithFormat:@"%@/%d/%@", GET_USER_AVATAR, App_Delegate.appfilter.industryId.intValue, mailDetails.sender.userID];
        NSString *imageURLString = [ServiceUtil addHeaderParameters:action withParams:nil];
        [cell.imgUserPic sd_setImageWithURL:[NSURL URLWithString:imageURLString] placeholderImage:[UIImage imageNamed:@"defaultimg"] options:SDWebImageRefreshCached];

        UserMessage *message = mailDetails.userMessage;
        NSString *subjectString = message.subject;
        
        // Time
        cell.lblEmailDate.text = [[Helper convertUTCTimeToLocalDate:message.createdOn] toNSString:MAIL_DATE_TIME_FORMAT];
        
        //Sender Name
        cell.lblSenderName.text = [mailDetails.sender getFullName];
        
        // Receiver Name
        cell.lblReceiverName.text = (mailDetails.recipient) ? [NSString stringWithFormat:@"To: %@", [mailDetails.recipient getFullName]] : @"";

        // Subject
        cell.lblEmailSubject.text = subjectString;
        
        CGFloat lblHeight = [self getLabelHeight:cell.lblEmailContent];
        cell.btnMore.hidden = (lblHeight < 30) ? YES : NO;
        
        // Email attachment
        cell.imgAttachment.hidden = YES;
        if (mailDetails.mobileMailAttachmentList.count > 0) {
            cell.attchScroll.tag = indexPath.row;
            cell.imgAttachment.hidden = NO;
            cell.btnMore.hidden = NO;
            [cell.btnMore setTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_MORE"] forState:UIControlStateNormal];
            [self createAttachmentView:mailDetails.mobileMailAttachmentList Cell:cell];
        }
        
        //First cell should be open
        if (indexPath.row == 0) {
            cell.lblEmailContent.attributedText = [message convertHtmlContentToAttributedString];
            CGRect rect = [cell.lblEmailContent.attributedText boundingRectWithSize:CGSizeMake(cell.lblEmailContent.frame.size.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];

            [cell.btnMore setTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_LESS"] forState:UIControlStateNormal];
            cell.emailContentHeight.constant = rect.size.height + 10;
            if (cell.attchScroll.subviews.count != 2) {
                cell.scrollHeight.constant = 70;
            }
            [cell.lblEmailContent layoutIfNeeded];
            cell.isOpen = YES;
        }
        
        // Set email details based on button title
        if ([cell.btnMore.titleLabel.text isEqualToString:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_LESS"]]) {
            cell.lblEmailContent.attributedText = [message convertHtmlContentToAttributedString];
        } else {
            // Remove html tags
            NSString *userMessage = [message.message stringByConvertingHTMLToPlainText];
            cell.lblEmailContent.text = userMessage;
        }
        
        //-----------------
        // MAIL TYPE - SENT
        //-----------------
        if ([self.mailType isEqualToString:KEY_MAIL_TYPE_SENT]) {
            cell.lblSenderName.text = [mailDetails.sender getFullName];
            cell.lblReceiverName.text = (mailDetails.recipient) ? [NSString stringWithFormat:@"To: %@", [mailDetails.recipient getFullName]] : @"";
        }
        
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
    return cell;
}

//---------------------------------------------------------------

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    MailDetailsCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [self moreButtonTapped:cell.btnMore];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View LifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    
    //Analytics
    [AnalyticsLogger logEventWithName:@"reply_mail"];
    
    _previewController = [[PreviewController alloc] init];
    self.previewController.currentPreviewItemIndex = 0;

    UIBarButtonItem *replyButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemReply target:self action:@selector(replyButtonTapped:)];
    self.navigationItem.rightBarButtonItem = replyButton;
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _noRecordsView = [Helper createNoAccessView:self.tableView withMessage:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_NOT_FOUND"]];
    [self callWebServiceToGetMailDetails];
}

//---------------------------------------------------------------

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

//---------------------------------------------------------------

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    UIToolbar *toolbar = [self.navigationController.view viewWithTag:20125];
    [toolbar removeFromSuperview];
}

//---------------------------------------------------------------

@end
