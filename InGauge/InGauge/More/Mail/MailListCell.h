//
//  MailListCell.h
//  IN-Gauge
//
//  Created by Bharat Chandera on 11/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MailListCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView        *imgUserPic;
@property (nonatomic, weak) IBOutlet UIImageView        *imgAttachment;
@property (nonatomic, weak) IBOutlet UILabel            *lblSenderName;
@property (nonatomic, weak) IBOutlet UILabel            *lblEmailSubject;
@property (nonatomic, weak) IBOutlet UILabel            *lblEmailDate;
@property (nonatomic, weak) IBOutlet UILabel            *lblEmailContent;
@property (nonatomic, weak) IBOutlet UIView             *unReadView;

@end
