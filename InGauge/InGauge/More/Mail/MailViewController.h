//
//  MailViewController.h
//  IN-Gauge
//
//  Created by Bharat Chandera on 06/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface MailViewController : BaseViewController

@property (nonatomic, strong) NSString                  *mailType;

@end
