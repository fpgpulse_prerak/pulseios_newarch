//
//  MailViewController.m
//  IN-Gauge
//
//  Created by Bharat Chandera on 06/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "MailViewController.h"
#import "MailListCell.h"
#import "MailDetailsController.h"
#import "ComposeMailViewController.h"
#import "UserMailListModel+API.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "LGRefreshView.h"
#import "AnalyticsLogger.h"
#import "NSString+HTML.h"
#import <MJRefresh.h>

#define kOptionButtonTag        5656
#define kFloatingButtonTag      1221

@interface MailViewController () 

@property (nonatomic, weak) IBOutlet UIButton           *optionButton;
@property (nonatomic, weak) IBOutlet UISearchBar        *mailSearchBar;
@property (nonatomic, weak) IBOutlet UIView             *viewSearchBar;
@property (nonatomic, weak) IBOutlet UITableView        *tableView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *trailingConstraint;

@property (nonatomic, strong) LGRefreshView             *refreshView;
@property (nonatomic, strong) MBProgressHUD             *hud;
@property (nonatomic, strong) UIView                    *noRecordsView;
@property (nonatomic, strong) UIButton                  *flotingButton;
@property (nonatomic, strong) NSMutableArray            *arrMailList;
@property (nonatomic, strong) NSArray                   *userArray;

@property (nonatomic) NSInteger                         pageNo;
@property (nonatomic) NSInteger                         pageSize;
@property (nonatomic, assign) NSInteger                 seletedOpIndex;
@property (nonatomic) BOOL                              isFirstLoad;
@property (nonatomic) BOOL                              shouldBeginEditing;

@end

@implementation MailViewController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc {
    // Remove delegates, mutable array and observers here!
    [self.arrMailList removeAllObjects];
    self.arrMailList = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) optionCliked:(UIButton *)sender {
    if (sender.tag == kOptionButtonTag) {
        sender.tag = kOptionButtonTag + 1;
        [self createBottomView:NO];
    } else {
        sender.tag = kOptionButtonTag;
        [self createBottomView:YES];
    }
}

//---------------------------------------------------------------

- (void) segmentValueChanged:(UISegmentedControl *)segment {
    
    // clear previous text
    self.mailSearchBar.text = @"";
    [self.mailSearchBar resignFirstResponder];
    
    self.seletedOpIndex = segment.selectedSegmentIndex;
    if (segment.selectedSegmentIndex == 0) {
        [self.optionButton setTitle:@"All" forState:UIControlStateNormal];
    } else if(segment.selectedSegmentIndex == 1) {
        [self.optionButton setTitle:@"Read" forState:UIControlStateNormal];
    } else {
        [self.optionButton setTitle:@"UnRead" forState:UIControlStateNormal];
    }
    
    // Update list
    [self reloadMailList];
    self.optionButton.tag = kOptionButtonTag;
    [self createBottomView:YES];
}

//---------------------------------------------------------------

- (void) btnFlotingClicked:(UIButton *)sender {
    [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];
    if (sender.tag == 1220) {
        // Delete button
        // Call service to remove mails
        [self callWebServiceToRemoveMails:self.tableView.indexPathsForSelectedRows];
    } else {
        // Compose button
        ComposeMailViewController *composeMailVC = [App_Delegate.mainStoryboard instantiateViewControllerWithIdentifier:@"ComposeMailViewController"];
        composeMailVC.userArray = self.userArray;
        [self.navigationController pushViewController:composeMailVC animated:YES];
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WEBSERVICE methods

//---------------------------------------------------------------

- (void) callWebServiceToGetMailList {
    // Check for internet
    if (![Helper checkForConnection:self]) {
        return;
    }
    
    if (self.pageNo == 0) {
        // Remove all data if page index is zero
        [self.arrMailList removeAllObjects];
    }
    
    // Clear searching
    if (self.mailSearchBar.text == nil) {
        self.mailSearchBar.text = @"";
    }
    
    @try {
        NSArray *itemArray = [NSArray arrayWithObjects:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_ALL"], [App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_READ"], [App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_UNREAD"], nil];
        NSString *status = [itemArray objectAtIndex:self.seletedOpIndex];
        NSDictionary *solrInput = @{
                                        @"first" : [NSNumber numberWithInteger:self.arrMailList.count],
                                        @"total" : [NSNumber numberWithInteger:self.pageSize],
                                        @"sortBy" : @[@"createdOn"],
                                        @"sortOrder" : @[@"DESC"],
                                        @"searchText" : self.mailSearchBar.text
                                    };
        
        NSDictionary *jsonRequest = @{
                                        @"recipientId":App_Delegate.appfilter.loginUserId,
                                        @"senderId":App_Delegate.appfilter.loginUserId,
                                        @"type":self.mailType,
                                        @"mailReadStatus":[status lowercaseString],
                                        @"solrInput":solrInput
                                      };
        
        
        // Show loading on first load & reload
        BOOL isShowLoading = NO;
        if (self.isFirstLoad) {
            isShowLoading = YES;
        }
        // Clear table editing
        if (self.tableView.editing) {
            self.tableView.editing = NO;
            isShowLoading = YES;
        }
        
        if (isShowLoading) {
            self.hud = [Helper showLoading:self.view];
        }

        [UserMailListModel getUserMailList:jsonRequest whenFinish:^(NSError *localizedError, UserMailListModel *mailList) {
            if (isShowLoading) {
                [Helper hideLoading:self.hud];
            }
            self.isFirstLoad = NO;
            
            if (localizedError == nil) {
                NSArray *arrMail = mailList.objects;
                [self.arrMailList addObjectsFromArray:arrMail];
                self.noRecordsView.hidden = (self.arrMailList.count > 0) ? YES : NO;
                
                // Hide load more if you get all records
                self.tableView.mj_footer.hidden = (self.arrMailList.count >= mailList.total.intValue) ? YES : NO;
                [self.tableView reloadData];
            }
        }];
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void) callWebServiceToGetUnreadCount {
    if ([self.mailType isEqual:KEY_MAIL_TYPE_INBOX]) {
        [UserMailListModel getUnreadMailsCount:nil whenFinish:^(NSError *localizedError, NSNumber *count) {
            if (localizedError == nil) {
                NSString *countString = [NSString stringWithFormat:@"(%d)", (int)count.intValue];
                if (count.intValue == 0) {
                    countString = @"";
                }
                self.title = [NSString stringWithFormat:@"%@ %@",[self.mailType capitalizedString], countString];
            }
        }];
    }
}

//---------------------------------------------------------------

- (void) callWebServiceToRemoveMails:(NSArray *)indexPathsArray {
    
    // Validation
    if (indexPathsArray.count == 0) {
        [Helper showAlertWithTitle:@"" message:@"There is/are no any message(s) selected to remove" forController:self];
        return;
    }
    
    // Check for internet
    if (![Helper checkForConnection:self]) {
        return;
    }

    @try {
        NSMutableArray *tempArray = [NSMutableArray new];
        for (int i = 0; i < indexPathsArray.count; i++) {
            NSIndexPath *indexpath = (NSIndexPath *)[indexPathsArray objectAtIndex:i];
            NSString *mailId = [NSString stringWithFormat:@"%@",[[self.arrMailList objectAtIndex:indexpath.row] userMailId]];
            [tempArray addObject:mailId];
        }
        NSArray *mailIdArray = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
        
        NSDictionary *jsonRequest = @{
                                        @"ids" : mailIdArray,
                                        @"type" : self.mailType
                                      };

        MBProgressHUD *hud = [Helper showLoading:self.navigationController.view];
        [UserMailListModel removeUserMails:jsonRequest whenFinish:^(NSError *localizedError, NSDictionary *details) {
            [Helper hideLoading:hud];
            
            if (localizedError == nil) {
                if ([details[@"statusCode"] integerValue] == 200) {
                    
                    // update read count if unread mail is removed
                    [self callWebServiceToGetUnreadCount];
                    
                    // remove mail from datasource
                    for (int i = 0; i < indexPathsArray.count; i++) {
                        NSIndexPath *indexpath = (NSIndexPath *)[indexPathsArray objectAtIndex:i];
                        [self.arrMailList removeObjectAtIndex:(indexpath.row - i != 0 ? 1 : 0 )];
                    }
                    
                    // remove cell with animations
                    [self.tableView beginUpdates];
                    [self.tableView deleteRowsAtIndexPaths:indexPathsArray withRowAnimation:UITableViewRowAnimationAutomatic];
                    [self.tableView endUpdates];
                    
                    self.editButtonItem.title = [App_Delegate.keyMapping getKeyValue:@"KEY_DELETE"];
                    [self.flotingButton setImage:[UIImage imageNamed:COMPOSE_MAIL_IMAGE] forState:UIControlStateNormal];
                    self.flotingButton.tag = kFloatingButtonTag;
                    
                    //Analytics
                    [AnalyticsLogger logRemoveMailsEvent:self mailId:mailIdArray];
                }
            }
            
            [UIView animateWithDuration:0.5 animations:^{
                self.tableView.editing = NO;
            }];
        }];
    } @catch (NSException *exception) {
        DLog(@"Exception :%@",exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void) callWebServiceToGetRecipientListInBackground {
    [UserMailListModel getRecipients:^(NSError *localizedError, UserMailRecipientList *recipients) {
        self.userArray = recipients.employees;
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) createBottomView:(BOOL)isLoad {
    if (isLoad) {
        UIView *bottomView = [self.view viewWithTag:15253];
        [UIView animateWithDuration:0.4 delay:0.0 options:(UIViewAnimationCurveEaseInOut|UIViewAnimationOptionAllowUserInteraction) animations:^{
            bottomView.frame = CGRectMake(10, self.view.frame.size.height + 60, self.view.frame.size.width - 100, 44);
        } completion:^(BOOL finished) {
            [bottomView removeFromSuperview];
        }];
        
    } else {
        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(10, self.view.frame.size.height + 60, self.view.frame.size.width - 100, 44)];
        bottomView.backgroundColor = APP_COLOR;
        bottomView.tag = 15253;
        bottomView.layer.cornerRadius = 6;
        
        NSArray *itemArray = [NSArray arrayWithObjects: @"All", @"Read", @"UnRead", nil];
        UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:itemArray];
        segmentedControl.frame = CGRectMake(8, 8, bottomView.bounds.size.width - 16, 30);
        [segmentedControl addTarget:self action:@selector(segmentValueChanged:) forControlEvents: UIControlEventValueChanged];
        segmentedControl.selectedSegmentIndex = self.seletedOpIndex;
        [segmentedControl setTintColor:[UIColor whiteColor]];
        [bottomView addSubview:segmentedControl];
        [self.view addSubview:bottomView];
        [self.view bringSubviewToFront:bottomView];
        
        [UIView animateWithDuration:0.5 delay:0.0 options:(UIViewAnimationCurveEaseInOut|UIViewAnimationOptionAllowUserInteraction) animations:^{
            bottomView.frame = CGRectMake(10, self.view.frame.size.height - 70, self.view.frame.size.width - 100, 44);
        } completion:^(BOOL finished) {}];
    }
}

//---------------------------------------------------------------

- (void) setEditing:(BOOL)editing animated:(BOOL)animated {
    // Make sure you call super first
    [super setEditing:editing animated:animated];
    [UIView animateWithDuration:0.5 animations:^{
        self.tableView.editing = editing;
    }];
    
    NSString *title = @"";
    UIImage *image = nil;
    if (editing) {
        title = [App_Delegate.keyMapping getKeyValue:@"KEY_CANCEL"];
        image = [UIImage imageNamed:TRASH_IMAGE];
        self.flotingButton.tag = 1220;
        self.optionButton.tag = kOptionButtonTag;
        [self createBottomView:YES];
    } else {
        title = [App_Delegate.keyMapping getKeyValue:@"KEY_DELETE"];
        image = [UIImage imageNamed:COMPOSE_MAIL_IMAGE];
        self.flotingButton.tag = kFloatingButtonTag;
    }
 
    // Button flip animation
    [UIView transitionWithView:self.flotingButton duration:0.5 options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
        self.editButtonItem.title = title;
        [self.flotingButton setImage:image forState:UIControlStateNormal];
    } completion:nil];
}

//---------------------------------------------------------------

- (NSString *) utcDateFromIntervalForDate:(long long)dateTimeStamp {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(dateTimeStamp / 1000.0)];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:MAIL_DATE_TIME_FORMAT];
    return [dateFormatter stringFromDate:date];
}

//---------------------------------------------------------------

- (void) createPullToRefresh {
    __weak typeof(self) wself = self;
    _refreshView = [LGRefreshView refreshViewWithScrollView:self.tableView refreshHandler:^(LGRefreshView *refreshView) {
        if (wself) {
            __strong typeof(wself) self = wself;
            
            //TODO: Call API
            [self reloadMailList];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void) {
                [self.refreshView endRefreshing];
            });
        }
    }];
    _refreshView.tintColor = APP_COLOR;
}

//---------------------------------------------------------------

- (void) reloadMailList {
    // start paging from first page
    self.pageNo = 0;
    self.isFirstLoad = YES;
    
    // Call service to get fresh list!
    [self callWebServiceToGetMailList];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

//---------------------------------------------------------------

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrMailList.count;
}

//---------------------------------------------------------------

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MailListCell *cell = (MailListCell *) [tableView dequeueReusableCellWithIdentifier:@"MailCell" forIndexPath:indexPath];
    
    @try {
        UserMailModel *mailModel = [self.arrMailList objectAtIndex:indexPath.row];
        
        // Download user profile
        NSString *action = [NSString stringWithFormat:@"%@/%d/%d", GET_USER_AVATAR, App_Delegate.appfilter.industryId.intValue, mailModel.senderId.intValue];
        NSString *imageURLString = [ServiceUtil addHeaderParameters:action withParams:nil];
        [cell.imgUserPic sd_setImageWithURL:[NSURL URLWithString:imageURLString] placeholderImage:[UIImage imageNamed:@"defaultimg"] options:SDWebImageRefreshCached];
        
        NSString *subjectString = [mailModel.subject stringByConvertingHTMLToPlainText];
        NSString *messageString = [mailModel.message stringByConvertingHTMLToPlainText];
        
        // Time
        cell.lblEmailDate.text = [self utcDateFromIntervalForDate:mailModel.createdOn.longLongValue];
        
        //Sender Name
        cell.lblSenderName.text = [NSString stringWithFormat:@"%@ %@",mailModel.firstName,mailModel.lastName];
        
        //Email Details
        cell.lblEmailSubject.text = subjectString;
        cell.lblEmailContent.text = messageString;
        
        cell.imgAttachment.hidden = YES;
        cell.unReadView.backgroundColor = [UIColor whiteColor];
        
        // Show/hide read and unread mail details
        if (mailModel.isRead_ur.boolValue == NO && [self.mailType isEqual:KEY_MAIL_TYPE_INBOX]) {
            cell.unReadView.layer.cornerRadius = 5;
            cell.unReadView.backgroundColor = [UIColor colorWithRed:0.00 green:0.48 blue:1.00 alpha:1.0];
            
            // If user is same as sender than show as read message
            User *user = [User signedInUser];
            if (user.userID.intValue == mailModel.senderId.intValue) {
                cell.unReadView.backgroundColor = [UIColor whiteColor];
            }
        }
        
        // Change name if mailType is "sent"
        if ([self.mailType isEqualToString:KEY_MAIL_TYPE_SENT]) {
            cell.lblSenderName.text = [NSString stringWithFormat:@"%@", mailModel.recipient_name];
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
    return cell;
}

//---------------------------------------------------------------

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    @try {
        if (!tableView.isEditing) {
            UserMailModel *mailModel = [self.arrMailList objectAtIndex:indexPath.row];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            MailDetailsController *controller = [storyboard instantiateViewControllerWithIdentifier:@"MailDetailsController"];
            controller.mailModel = mailModel;
            controller.mailType = self.mailType;
            [self.navigationController pushViewController:controller animated:YES];
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return (App_Delegate.userPermissions.canAccessDeletMail.boolValue && ![self.mailType isEqualToString:KEY_MAIL_TYPE_TRASH]);
}

//---------------------------------------------------------------

- (void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Call service to remove mails
        [self callWebServiceToRemoveMails:@[indexPath]];
    }
}

//---------------------------------------------------------------

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.tableView.isEditing == YES) {
        return;
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UISearchBar delegate methods

//---------------------------------------------------------------

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self reloadMailList];
    [self.view endEditing:YES];
}

//---------------------------------------------------------------

- (void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if (![searchBar isFirstResponder]) {
        self.shouldBeginEditing = NO;
    }
    
    NSString *trimText = [Helper removeWhiteSpaceAndNewLineCharacter:searchText];
    if ([trimText length] == 0) {
        self.shouldBeginEditing = NO;
        [self.mailSearchBar resignFirstResponder];
        [self reloadMailList];
    }
}

//---------------------------------------------------------------

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)bar {
    // reset the shouldBeginEditing BOOL ivar to YES, but first take its value and use it to return it from the method call
    BOOL boolToReturn = self.shouldBeginEditing;
    self.shouldBeginEditing = YES;
    return boolToReturn;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View LifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    self.title = [self.mailType capitalizedString];
    
    // Pagination settings
    self.pageNo = 0;
    self.pageSize = 20;
    self.isFirstLoad = YES;
    self.shouldBeginEditing = YES;
    _hud = [MBProgressHUD new];
    _arrMailList = [NSMutableArray new];
    
    // table settings
    self.tableView.estimatedRowHeight = UITableViewAutomaticDimension;
    self.tableView.rowHeight = 75.0;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.tableView.mj_footer endRefreshing];
            self.pageNo = self.pageNo + 1;
            [self callWebServiceToGetMailList];
        });
    }];
    
    //Analytics
    NSString *checkMailString = [NSString stringWithFormat:@"check_mail_%@", self.mailType];
    [AnalyticsLogger logEventWithName:checkMailString];
    
    _noRecordsView = [Helper createNoAccessView:self.tableView withMessage:[App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_NOT_FOUND"]];
    if (self.mailType == nil) {
        self.mailType = KEY_MAIL_TYPE_INBOX;
    }
    
    if (App_Delegate.userPermissions.canAccessDeletMail.boolValue && ![self.mailType isEqualToString:KEY_MAIL_TYPE_TRASH]) {
        self.navigationItem.rightBarButtonItem = self.editButtonItem;
        self.editButtonItem.title = [App_Delegate.keyMapping getKeyValue:@"KEY_DELETE"];
        self.tableView.allowsMultipleSelectionDuringEditing = YES;
    } else {
    }
    
    // Hide option button for sent mail type
    if ([self.mailType isEqualToString:KEY_MAIL_TYPE_SENT]) {
        self.optionButton.hidden = YES;
        self.trailingConstraint.constant = 0.5;
        [self.mailSearchBar layoutIfNeeded];
    } else {
        self.trailingConstraint.constant = 83;
        [self.mailSearchBar layoutIfNeeded];
        self.optionButton.hidden = NO;
    }
    self.optionButton.hidden = ([self.mailType isEqualToString:KEY_MAIL_TYPE_SENT]) ? YES : NO;
    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setDefaultTextAttributes:@{
                                                                                                 NSForegroundColorAttributeName:[UIColor blackColor],
                                                                                                 NSFontAttributeName : [UIFont fontWithName:AVENIR_FONT size:14]
                                                                                                 }];
    
    // Pull to refresh ----
    [self createPullToRefresh];
    
    if (App_Delegate.userPermissions.canAccessComposeMail.boolValue) {
        _flotingButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 60, self.view.frame.size.height - 120, 50, 50)];
        [self.flotingButton setImage:[UIImage imageNamed:COMPOSE_MAIL_IMAGE] forState:UIControlStateNormal];
        [self.flotingButton setBackgroundColor:APP_COLOR];
        [self.flotingButton setImageEdgeInsets:UIEdgeInsetsMake(15, 15, 15, 15)];
        [self.flotingButton setContentMode:UIViewContentModeCenter];
        self.flotingButton.tag = kFloatingButtonTag;
        self.flotingButton.layer.cornerRadius = 25;
        self.flotingButton.tintColor = [UIColor whiteColor];
        self.flotingButton.hidden = NO;
        self.flotingButton.layer.masksToBounds = YES;
        [self.flotingButton addTarget:self action:@selector(btnFlotingClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.flotingButton];
        [self.view bringSubviewToFront:self.flotingButton];
    }
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];    
    self.mailSearchBar.placeholder = [App_Delegate.keyMapping getKeyValue:@"KEY_MAIL_SEARCH"];
    
    // Call Background service to get recipients
    [self callWebServiceToGetRecipientListInBackground];
}

//---------------------------------------------------------------

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Update unread mail count
    [self callWebServiceToGetUnreadCount];
    [self callWebServiceToGetMailList];
}

//---------------------------------------------------------------

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.mailSearchBar resignFirstResponder];
    [self createBottomView:YES];
}

//---------------------------------------------------------------

@end
