//
//  PreviewController.h
//  IN-Gauge
//
//  Created by Mehul Patel on 01/12/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <QuickLook/QuickLook.h>
#import "BaseViewController.h"

@class PreviewController;

typedef void(^FilePreviewCustomizeBlock)(PreviewController * previewer);

@interface PreviewController : QLPreviewController <QLPreviewControllerDataSource, QLPreviewControllerDelegate, UIDocumentInteractionControllerDelegate>

/// You may remove the action button if you don't want it.
@property (nonatomic, assign) BOOL showActionButton;

/// You may add a custom right bar button item instead of using the print button.
@property (nonatomic, strong) UIBarButtonItem * rightBarButtonItem;

/// A custom tint color for the nav bar. Be aware that if this view is pushed, 
/// then this will change the color of all nav bars in the navigation stack.
@property (nonatomic, strong) UIColor * navBarTintColor;

/// A custom tint color for the tool bar.
@property (nonatomic, strong) UIColor * toolBarTintColor;

/// A block for customizing the appearance of the RBFilePreviewer.
@property (nonatomic, copy) FilePreviewCustomizeBlock block;

@property (nonatomic, strong) NSURL                 *fileURL;
/**
 * Convenience method if you just want to preview on file;
 */
- (id)initWithFile:(id<QLPreviewItem>)file;

/**
 *  Creates a file previewer from the given files. The array must not be empty.
 *
 *  @param theFiles An array of QLPreviewItems.
 *
 *  @return self
 */
- (id)initWithFiles:(NSArray *)theFiles;

/**
 *  This should be called before creating RBFileViewer. Quick Look is not 
 *  supported on pre-4.0 devices. Returns YES if Quick Look is supported.
 *
 *  @return YES if Quick Look is supported.
 */
+ (BOOL)isFilePreviewingSupported;

/**
 * Dismisses the file previewer. Takes into account if the view was presented modally.
 */
- (IBAction)dismissView:(id)sender;


@end
