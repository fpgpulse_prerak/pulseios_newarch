//
//  PreviewController.m
//  IN-Gauge
//
//  Created by Mehul Patel on 01/12/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "PreviewController.h"

@interface PreviewController ()

@property (nonatomic, strong) NSArray               *files;
@property (nonatomic, strong) UIToolbar             *toolbar;
@property (nonatomic, strong) UIBarButtonItem       *leftButton;
@property (nonatomic, strong) UIBarButtonItem       *rightButton;

@end

@implementation PreviewController

//---------------------------------------------------------------

#pragma mark -
#pragma mark  methods

//---------------------------------------------------------------

- (void)dealloc {
    [self setDataSource:nil];
    [self setDelegate:nil];
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark Init methods

//---------------------------------------------------------------

- (id) initWithFile:(id<QLPreviewItem>)file {
    PreviewController *result = [self initWithFiles:[NSArray arrayWithObject:file]];
    [result setCurrentPreviewItemIndex:0];
    return result;
}

//---------------------------------------------------------------

- (id) initWithFiles:(NSArray *)files  {
    NSAssert([files count] > 0, @"Empty file array.");
    
    if ((self = [super init])) {
        [self setShowActionButton:YES];
        [self setFiles:files];
        [self setDataSource:self];
        [self setDelegate:self];
        [self setCurrentPreviewItemIndex:0];
    }
    return self;
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark Action methods

//---------------------------------------------------------------

- (void) dismissView:(id)sender {
    [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
}

//---------------------------------------------------------------

- (void) showPreviousDocument:(id)sender {
    if ([self currentPreviewItemIndex] - 1 < 0)
        return;
    [self setCurrentPreviewItemIndex:[self currentPreviewItemIndex] - 1];
}

//---------------------------------------------------------------

- (void) showNextDocument:(id)sender {
    if ([self currentPreviewItemIndex] >= [[self files] count])
        return;
    [self setCurrentPreviewItemIndex:[self currentPreviewItemIndex] + 1];
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) setCurrentPreviewItemIndex:(NSInteger)index {
    NSInteger max = [[self files] count];
    if (index < 0 || index >= max)
        return;
    
    [super setCurrentPreviewItemIndex:index];
    self.showActionButton = [[[self.files objectAtIndex:index] previewItemURL] isFileURL];
    [self updateArrows];
    [self removeActionButtonIfApplicable];
}

//---------------------------------------------------------------

- (void) updateArrows {
    NSInteger index = [self currentPreviewItemIndex];
    NSInteger max = [[self files] count];
    
    [[self rightButton] setEnabled:(index < max - 1)];
    [[self leftButton] setEnabled:(index != 0)];
}

//---------------------------------------------------------------

- (void) removeActionButtonIfApplicable  {
    // Replaces the action button if desired.
    if ([self rightBarButtonItem])
        self.navigationItem.rightBarButtonItem = self.rightBarButtonItem;
    // Hides the action button if not wanted.
    else if (!self.showActionButton)
        [self.navigationItem setRightBarButtonItem:nil animated:NO];
}

//---------------------------------------------------------------

- (void) addToolbarIfApplicable {
    // Adds a toolbar to the view so it's available to both pushed views and modal views.
    if (![self toolbar] && [[self files] count] > 1) {
        const CGFloat kStandardHeight = 44.0f;
        CGFloat superViewWidth = self.view.frame.size.width;
        CGFloat superViewHeight = self.view.frame.size.height;
        CGRect frame = CGRectMake(0, superViewHeight - kStandardHeight, superViewWidth, kStandardHeight);
        
        UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:frame];
        UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithImage:[self leftArrow]
                                                                  style:UIBarButtonItemStylePlain
                                                                 target:self
                                                                 action:@selector(showPreviousDocument:)];
        
        UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithImage:[self rightArrow]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(showNextDocument:)];
        
        UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                        target:nil
                                                                                        action:nil];
        
        [toolbar setItems:[NSArray arrayWithObjects:flexibleSpace, left, flexibleSpace, right, flexibleSpace, nil]];
        [self setLeftButton:left];
        [self setRightButton:right];
        [[self view] addSubview:toolbar];
        [self setToolbar:toolbar];
        
        [self updateArrows];
        if (nil != self.toolBarTintColor)
            [[self toolbar] setTintColor:[self toolBarTintColor]];
    }
}

//---------------------------------------------------------------

+ (BOOL)isFilePreviewingSupported {
    return (nil != NSClassFromString(@"QLPreviewController"));
}

//---------------------------------------------------------------

#pragma mark - Images

//---------------------------------------------------------------

- (UIImage *)leftArrow {
    return [UIImage imageNamed:LEFT_ARROW];
}

//---------------------------------------------------------------

- (UIImage *)rightArrow {
   return [UIImage imageNamed:RIGHT_ARROW];
}

//---------------------------------------------------------------

#pragma mark - QLPreviewControllerDataSource methods.

- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller {
//    return [[self files] count];
    return 1;
}

//---------------------------------------------------------------

- (id<QLPreviewItem>)previewController:(QLPreviewController *)controller
                    previewItemAtIndex:(NSInteger)index {
//    id<QLPreviewItem> result = [[self files] objectAtIndex:index];
//    self.showActionButton = [result.previewItemURL isFileURL];
//    return result;
    id<QLPreviewItem> result = self.fileURL;
    if (![QLPreviewController canPreviewItem:result]) {
        [Helper showAlertWithTitle:@"" message:@"File not supported!" forController:self];
    }
    return result;
}

//---------------------------------------------------------------

#pragma mark - QLPreviewControllerDelegate methods

//---------------------------------------------------------------

- (BOOL) previewController:(QLPreviewController *)controller shouldOpenURL:(NSURL *)url forPreviewItem:(id<QLPreviewItem>)item {
    if ([QLPreviewController canPreviewItem:item]) {
        return YES;
    } else {
        [Helper showAlertWithTitle:@"" message:@"File not supported!" forController:self];
        return NO;
    }
}

//---------------------------------------------------------------

- (void)previewControllerWillDismiss:(QLPreviewController *)controller {
}

//---------------------------------------------------------------

- (void)previewControllerDidDismiss:(QLPreviewController *)controller {
}

//---------------------------------------------------------------

#pragma mark - UIDocumentInteractionControllerDelegate

//---------------------------------------------------------------

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)interactionController {
    return [self parentViewController];
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark View life cycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    NSAssert(self.navigationController, @"FilePreviewer must be in a nav controller.");
    if (nil != self.navBarTintColor)
        self.navigationController.navigationBar.tintColor = self.navBarTintColor;    
    [self setDataSource:self];
    [self setDelegate:self];
}

//---------------------------------------------------------------

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [self removeActionButtonIfApplicable];
    
    // Overrides the original done button if the previewer was presented modally.
//    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissView:)];
//    self.navigationItem.leftBarButtonItem = doneButton;
//    [self.navigationItem setLeftBarButtonItem:doneButton animated:YES];
//    self.leftButton = doneButton;
//    [self addToolbarIfApplicable];
    
    // Allows further customization via block.
//    if (self.block)
//        self.block(self);
}

//---------------------------------------------------------------

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//---------------------------------------------------------------

@end
