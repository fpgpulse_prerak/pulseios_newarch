//
//  RecipientCell.h
//  pulse
//
//  Created by Mehul Patel on 06/01/17.
//  Copyright © 2017 frontline performance group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecipientCell : UITableViewCell

@property (nonatomic, strong) UILabel               *recipientLabel;
@property (nonatomic, strong) UIImageView           *crossImage;
@property (nonatomic, strong) UIButton              *closeButton;

@end
