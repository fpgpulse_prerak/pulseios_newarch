//
//  RecipientCell.m
//  pulse
//
//  Created by Mehul Patel on 06/01/17.
//  Copyright © 2017 frontline performance group. All rights reserved.
//

#import "RecipientCell.h"
#import "Constant.h"

@implementation RecipientCell

//---------------------------------------------------------------

#pragma mark
#pragma mark Init methods

//---------------------------------------------------------------

- (instancetype) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = BACK_COLOR;
    
    _recipientLabel = [[UILabel alloc] init];
    self.recipientLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.recipientLabel.font = [UIFont fontWithName:AVENIR_FONT size:12];
    self.recipientLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.recipientLabel.textColor = [UIColor blackColor];
    self.recipientLabel.numberOfLines = 0;
    [self.contentView addSubview:self.recipientLabel];
    
    _crossImage = [UIImageView new];
    self.crossImage.translatesAutoresizingMaskIntoConstraints = NO;
    self.crossImage.image = [UIImage imageNamed:@"cross"];
    self.crossImage.tintColor = APP_COLOR;
    [self.contentView addSubview:self.crossImage];
    
    _closeButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.closeButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.closeButton.backgroundColor = [UIColor clearColor];
    self.closeButton.tintColor = APP_COLOR;
//    [self.closeButton setImage:[UIImage imageNamed:@"cross"] forState:UIControlStateNormal];
    
//    self.closeButton.backgroundColor = [UIColor blueColor];
//    self.recipientLabel.backgroundColor = [UIColor redColor];
    
    [self.contentView addSubview:self.closeButton];
    
    NSDictionary *views = @{
                                @"recipientLabel" : self.recipientLabel,
                                @"closeButton" : self.closeButton,
                                @"crossImage" : self.crossImage
                            };
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[recipientLabel]-40-|" options:0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[recipientLabel]|" options:0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[closeButton(60)]|" options:0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[closeButton(40)]" options:0 metrics:nil views:views]];

    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[crossImage(10)]-20-|" options:0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[crossImage(10)]" options:0 metrics:nil views:views]];

    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.closeButton
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1
                                                                  constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.crossImage
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1
                                                                  constant:0]];
    return self;
}

//---------------------------------------------------------------

- (void) layoutSubviews {
    [super layoutSubviews];    
}

//---------------------------------------------------------------

@end
