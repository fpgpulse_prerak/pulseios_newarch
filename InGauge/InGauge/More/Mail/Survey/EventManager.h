//
// EventManager.h
//  pulse
//
//  Created by Mehul Patel on 15/12/15.
//  Copyright © 2015 frontline performance group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EventKit/EventKit.h>
#import "Event.h"

#define OB_CALENDAR_SAVE_EVENTS @"KeyOB_CALENDAR_SAVE_EVENTS"

@interface EventManager : NSObject

// The database with calendar events and reminders
@property (nonatomic, strong) EKEventStore      *eventStore;

// Selected calendar identifier
@property (nonatomic, strong) NSString          *selectedCalendarIdentifier;

// The data source for the table view
@property (strong, nonatomic) NSMutableArray    *todoItems;

// The data source's start date
@property (strong, nonatomic) NSDate            *startDate;

// The data source's end date
@property (strong, nonatomic) NSDate            *endDate;


// Indicates whether app has access to event store.
@property (nonatomic) BOOL                      eventsAccessGranted;

- (void) saveAllEvents:(NSArray *)events withBlock:(void(^)(BOOL isFinish))block;
- (void) removeAllEvents:(NSArray *)events withBlock:(void(^)(BOOL isFinish))block;;
- (void) removeEvent:(Event *)event;

@end
