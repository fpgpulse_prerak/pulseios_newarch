//
//  QuestionAnswerController.h
//  IN-Gauge
//
//  Created by Mehul Patel on 11/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "DataManager.h"

@protocol QuestionAnswerDelegate <NSObject>

- (void) nextQuestion:(NSString *)answer currentQueId:(NSNumber *)questionId;

@end

@interface QuestionAnswerController : BaseViewController

@property (nonatomic, weak) id <QuestionAnswerDelegate> questionAnswerDelegate;

@property (nonatomic, strong) Filters                   *filter;
@property (nonatomic, strong) DataManager               *dataManager;
@property (strong, nonatomic) NSString                  *sectionText;
@property (strong, nonatomic) Question                  *question;
@property (nonatomic, strong) NSString                  *answerString;
@property (nonatomic, strong) NSString                  *segIndexString;
@property (nonatomic, strong) NSString                  *queIndexString;
@property (assign, nonatomic) NSInteger                 index;

@end
