//
//  SaveObservationController.h
//  IN-Gauge
//
//  Created by Mehul Patel on 12/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewController.h"
#import "DataManager.h"

@interface SaveObservationController : BaseTableViewController

@property (nonatomic, strong) Filters                   *filter;
@property (nonatomic, strong) DataManager               *dataManager;

@end
