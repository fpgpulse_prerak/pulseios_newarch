//
//  SurveyAgentController.h
//  IN-Gauge
//
//  Created by Mehul Patel on 07/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "DataManager.h"

@interface SurveyAgentController : BaseViewController

@property (nonatomic, strong) Filters                   *filter;
@property (nonatomic, strong) Event                     *event;
@property (nonatomic, assign) EVENT_TYPE                eventType;
@property (nonatomic, assign) SURVEY_TYPE               surveyType;

// Use below Variables for Analytics only
@property (nonatomic, strong) NSString                  *performanceLeader;
@property (nonatomic, strong) NSString                  *startDateString;
@property (nonatomic, strong) NSString                  *endDateString;
@end
