//
//  SurveyAgentController.m
//  IN-Gauge
//
//  Created by Mehul Patel on 07/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "SurveyAgentController.h"
#import "FilterItemCell.h"
#import "FilterDetailViewController.h"
#import "QuestionContainer.h"
#import "SurveyTabController.h"
#import "IQActionSheetPickerView.h"
#import "AnalyticsLogger.h"

@interface SurveyAgentController () <FilterDetailDelegate, UITableViewDelegate, UITableViewDataSource, IQActionSheetPickerViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView        *tableView;
@property (nonatomic, weak) IBOutlet UIButton           *startButton;
@property (nonatomic, weak) IBOutlet UIButton           *viewButton;
@property (nonatomic, strong) NSMutableArray            *dataArray;
@property (nonatomic, strong) DataManager               *dataManager;

@property (nonatomic, strong) UserList                  *performanceLeaderList;
@property (nonatomic, strong) UserList                  *agentList;
@property (nonatomic, strong) LocationList              *locationList;
@property (nonatomic, strong) OBTypeList                *obTypeList;
@property (nonatomic, strong) UIButton                  *getStartedButton;

@property (nonatomic, strong) NSString                  *locationKey;
@property (nonatomic, strong) NSString                  *plKey;
@property (nonatomic, strong) NSString                  *agentKey;
@property (nonatomic, strong) NSString                  *observationTypeKey;
@property (nonatomic, strong) NSString                  *scoreKey;
@property (nonatomic, strong) NSString                  *plNotFoundKey;
@property (nonatomic, strong) NSString                  *agentNotFoundKey;
@property (nonatomic, strong) NSString                  *typeNotFoundKey;
@property (nonatomic, assign) BOOL                      isReloadQuestions;

@end

@implementation SurveyAgentController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
    self.tableView.dataSource = nil;
    self.tableView.delegate = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kBackToDashboardNotification object:nil];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) getStartedButtonTapped:(UIButton *)sender {
    //Analytics
    [AnalyticsLogger logSurveyStartEvent:self];
    
    //animation
    [Helper tapOnControlAnimation:sender forScale:0.2 forCompletionBlock:^(BOOL finished) {
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WEBSERVICE methods

//---------------------------------------------------------------

- (void) callWebServiceToGetEvent {
    
    MBProgressHUD *hud = [Helper showLoading:self.view];
    [ServiceManager callWebServiceToGetSingleObservation:self.event.eventId usingBlock:^(Event *event) {
        [Helper hideLoading:hud];
        self.dataManager.event = event;
        [self setTitleArrayForEditEvent:self.dataManager.event];        
        [self.tableView reloadData];
    }];
}

//---------------------------------------------------------------

- (void) callFilterWebServices {
    self.filter.accessible  = [NSNumber numberWithBool:NO];
    
    // Reset all filters
    self.filter.locationId  = [NSNumber numberWithInt:0];
    self.filter.userId      = [NSNumber numberWithInt:0];
    self.filter.orderBy     = KEY_ORDER_BY_VALUE;
    self.filter.sort        = KEY_SORT_BY_ASC_VALUE;
    self.filter.activeStatus = KEY_PARAM_CONST_ACTIVE_STATUS;
    
    // Reset default names
    self.filter.locationName = @"";
    self.filter.locationGroupName = @"";
    self.filter.productName = @"";
    self.filter.userName = @"";
    
    // Call location service
    [self getTenantLocations:^(BOOL isFinish) {
        // Call performance leader service
        
        //UPDATE: 26-Sep-2017 ~ If user is Performance leader than show PL = Login User
        if ([User signedInUser].isPerformanceLeader.boolValue) {
            
            // Skip Performance leader service call
            self.filter.performanceLeaderId = [User signedInUser].userID;
            NSDictionary *details = @{
                                      KEY_TITLE : self.plKey,
                                      KEY_VALUE : [[User signedInUser] getFullName],
                                      };
            [self.dataArray replaceObjectAtIndex:1 withObject:details];
            [self reloadTableWithAnimation:1];

            // Call Agent service
            [self getAgents:^(BOOL isFinish) {
                // Call obsrevation type service
                [self getObservationType:^(BOOL isFinish) {
                }];
            }];
        } else {
            [self getPerformanceLeader:^(BOOL isFinish) {
                // Call Agent service
                [self getAgents:^(BOOL isFinish) {
                    // Call obsrevation type service
                    [self getObservationType:^(BOOL isFinish) {
                    }];
                }];
            }];
        }
        
    }];
}

//---------------------------------------------------------------

- (void) getTenantLocations:(void(^)(BOOL isFinish))block {
    self.filter.orderBy = KEY_ORDER_BY_VALUE;
    self.filter.sort = KEY_SORT_BY_ASC_VALUE;
    self.filter.activeStatus = KEY_PARAM_CONST_ACTIVE_STATUS;

    // Get locations
    MBProgressHUD *hud = [Helper showLoading:self.view];
    NSDictionary *details = [self.filter serializeFiltersGoalSettingLocationService];
    [ServiceManager callWebServiceToGetLocations:details usingBlock:^(LocationList *locationList) {
        [Helper hideLoading:hud];
        self.locationList = locationList;
        
        @try {
            if (self.locationList.locationList.count > 0) {
                // First location
                Location *location = [self.locationList.locationList objectAtIndex:0];
                self.filter.locationId = location.locationId;
                self.filter.locationName = location.name;
            } else {
                // Remove filter
                self.filter.locationId = [NSNumber numberWithInt:0];
                self.filter.locationName = @"";
            }
            
            // For Pending event - ONLY
            if (self.eventType == PENDING_EVENT) {
                self.filter.locationId = self.dataManager.event.tenantLocationId;
                self.filter.locationName = self.dataManager.event.tenantLocationName;
            }
            NSDictionary *details = @{
                                      KEY_TITLE : self.locationKey,
                                      KEY_VALUE : self.filter.locationName,
                                      };
            [self.dataArray replaceObjectAtIndex:0 withObject:details];
        } @catch (NSException *exception) {
            DLog(@"Exception :%@",exception.debugDescription);
        }
//        [self.tableView reloadData];
        [self reloadTableWithAnimation:0];
        block(YES);
    }];
}

//---------------------------------------------------------------

- (void) getPerformanceLeader:(void(^)(BOOL isFinish))block {
    self.filter.permissionName = @"'isPerformanceManager','isAvailableForTakeObservation'";
    self.filter.getContributorsOnly = [NSNumber numberWithBool:YES];
    self.filter.getOperatorsOnly = [NSNumber numberWithBool:NO];
    self.filter.getWithAllStatus = [NSNumber numberWithBool:NO];
    
    MBProgressHUD *hud = [Helper showLoading:self.view];
    // Get Users list
    NSDictionary *details = [self.filter serializeFiltersForPerformanceLeaderService];
    [ServiceManager callWebServiceToGetPerformanceLeader:details usingBlock:^(UserList *userList) {
        [Helper hideLoading:hud];
        self.performanceLeaderList = userList;
        
        @try {
            NSString *leaderName = @"";
            if (self.performanceLeaderList.userList.count > 0) {
                User *user = [self.performanceLeaderList.userList objectAtIndex:0];
                self.filter.performanceLeaderId = user.userID;
                leaderName = user.name;
            }
            else {
                // Remove filter
                self.filter.performanceLeaderId = nil;
            }
  
            // For Pending event - ONLY
            if (self.eventType == PENDING_EVENT) {
                self.filter.performanceLeaderId = self.dataManager.event.surveyorId;
                leaderName = self.dataManager.event.surveyorName;
            }
            self.performanceLeader = leaderName;
            NSDictionary *details = @{
                                      KEY_TITLE : self.plKey,
                                      KEY_VALUE : leaderName,
                                      };
            [self.dataArray replaceObjectAtIndex:1 withObject:details];
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
        //        [self.tableView reloadData];
        [self reloadTableWithAnimation:1];
        block(YES);
    }];
}

//---------------------------------------------------------------

- (void) getAgents:(void(^)(BOOL isFinish))block {
    self.filter.permissionName = @"'isAvailableForObservation'";
    self.filter.getContributorsOnly = [NSNumber numberWithBool:YES];
    self.filter.getOperatorsOnly = [NSNumber numberWithBool:NO];
    self.filter.getWithAllStatus = [NSNumber numberWithBool:NO];
    self.filter.decideContributorOpratorLocationLevel = [NSNumber numberWithBool:NO];
    
    MBProgressHUD *hud = [Helper showLoading:self.view];
    // Get Users list
    NSDictionary *details = [self.filter serializeFiltersForAgentService];
    [ServiceManager callWebServiceToGetAgents:details usingBlock:^(UserList *userList) {
        [Helper hideLoading:hud];
        self.agentList = userList;
        @try {
            NSString *agentName = @"";
            if (self.agentList.userList.count > 0) {
                User *user = [self.agentList.userList objectAtIndex:0];
                self.filter.agentId = user.userID;
                agentName = user.name;
            }
            else {
                // Remove filter
                self.filter.agentId = nil;
            }
            
            // For Pending event - ONLY
            if (self.eventType == PENDING_EVENT) {
                self.filter.agentId = self.dataManager.event.respondentId;
                agentName = self.dataManager.event.respondentName;
            }
            
            NSDictionary *details = @{
                                      KEY_TITLE : self.agentKey,
                                      KEY_VALUE : agentName,
                                      };
            [self.dataArray replaceObjectAtIndex:2 withObject:details];
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }

//        [self.tableView reloadData];
        
        [self reloadTableWithAnimation:2];
        block(YES);
    }];
}

//---------------------------------------------------------------

- (void) getObservationType:(void(^)(BOOL isFinish))block {
    
    self.filter.surveyEntityTypeString = self.dataManager.surveyEntityTypeString;
    
    MBProgressHUD *hud = [Helper showLoading:self.view];
    // Get Users list
    
    NSDictionary *details = [self.filter serializeFiltersForObservationTypeService];
    [ServiceManager callWebServiceToGetObservationType:details usingBlock:^(OBTypeList *obTypeList) {
        [Helper hideLoading:hud];
        self.obTypeList = obTypeList;
        
        @try {
            NSString *observationType = @"";
            if (self.obTypeList.obList.count > 0) {
                ObservationType *obType = [self.obTypeList.obList objectAtIndex:0];
                self.filter.obTypeId = obType.observationTypeId;
                
                NSString *sureveyCategoryName = [Helper removeWhiteSpaceAndNewLineCharacter:obType.surveyCategory.name];
                sureveyCategoryName = sureveyCategoryName.length > 0 ? [NSString stringWithFormat:@"(%@)", sureveyCategoryName] : @"";
                observationType = [NSString stringWithFormat:@"%@ %@", obType.name, sureveyCategoryName];

            } else {
                // Remove filter
                self.filter.obTypeId = nil;
            }
            
            // For Pending event - ONLY
            if (self.eventType == PENDING_EVENT) {
                self.filter.obTypeId = self.dataManager.event.questionnaireId;
                NSString *sureveyCategoryName = [Helper removeWhiteSpaceAndNewLineCharacter:self.dataManager.event.questionnaireSurveyCategoryName];
                sureveyCategoryName = sureveyCategoryName.length > 0 ? [NSString stringWithFormat:@"(%@)", sureveyCategoryName] : @"";
                if (self.dataManager.event.questionnaireName.length > 0) {
                    observationType = [NSString stringWithFormat:@"%@ %@", self.dataManager.event.questionnaireName, sureveyCategoryName];
                } else {
                    observationType = [NSString stringWithFormat:@"%@", sureveyCategoryName];
                }
            }
            
            NSDictionary *details = @{
                                      KEY_TITLE : self.observationTypeKey,
                                      KEY_VALUE : observationType,
                                      };
            [self.dataArray replaceObjectAtIndex:3 withObject:details];
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
//        [self.tableView reloadData];
        
        [self reloadTableWithAnimation:3];
        block(YES);
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) reloadTableWithAnimation:(NSInteger)index {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

//---------------------------------------------------------------

- (void) createTitleArray {
    
    switch (self.eventType) {
        case OB_NEW_EVENT : case PENDING_EVENT : {
            //Set titles
            [self setTitleArrayForNewEvent];
            break;
        }
        case OB_COMPLETED_EVENT : case OB_COMPLETED_EVENT_VIEW: {
            //Set titles
            [self setTitleArrayForEditEvent:nil];
            break;
        }
        default:
            break;
    }
}

//---------------------------------------------------------------

- (void) setTitleArrayForNewEvent {
    
    // Pending Event
    NSString *dateTimeString = [SurveyHelper getCurrentTime];
//    if (self.eventType == PENDING_EVENT) {
////        self.event.startDate = self.dataManager.event.startDate;
//        dateTimeString = [SurveyHelper getLocalDateInStringConvertFromUtcTimeStamp:self.event.startDate.doubleValue];
////        cell.cardView.scheduledOnLabel.text = [SurveyHelper getLocalDateInStringConvertFromUtcTimeStamp:event.startDate.doubleValue];
//
//    }
    self.startDateString = dateTimeString;
    
    NSArray *array = @[ @{
                   KEY_TITLE : self.locationKey,
                   KEY_VALUE : @"",
                   },
               @{
                   KEY_TITLE : self.plKey,
                   KEY_VALUE : @"",
                   },
               @{
                   KEY_TITLE : self.agentKey,
                   KEY_VALUE : @"",
                   },
               @{
                   KEY_TITLE : self.observationTypeKey,
                   KEY_VALUE : @"",
                   },
               @{
                   KEY_TITLE : [App_Delegate.keyMapping getKeyValue:@"KEY_OB_START_DATE"],
                   KEY_VALUE : dateTimeString,
                   }];
    _dataArray = [NSMutableArray new];
    self.dataArray = [NSMutableArray arrayWithArray:array];
}

//---------------------------------------------------------------

- (void) setTitleArrayForEditEvent:(Event *)event {
    
    NSString *locationName = @"";
    NSString *performanceLeader = @"";
    NSString *agentName = @"";
    NSString *observationType = @"";
    NSString *startDate = @"";
    NSString *endDate = @"";
    NSString *agentScore = @"";
    NSString *greatJobOn = @"";
    NSString *areaOfImproveMent = @"";
    NSString *opportunity = @"";
    
    if (event) {
        // Location
        locationName = event.tenantLocationName ? event.tenantLocationName : locationName;
        // Performance leader
        performanceLeader = event.surveyorName ? event.surveyorName : performanceLeader;
        // Get selected agent
        agentName = event.respondentName ? event.respondentName : agentName;

        // Observation Type
        observationType = [SurveyHelper createObservationTypeString:event];
        
        // Calculate time difference in seconds
        //*** Important: Save time difference for future use
        self.dataManager.event.timeDifference = [NSNumber numberWithDouble:[SurveyHelper calculateTimeDifference:event]];
        
        // Start Date
        startDate = [SurveyHelper getStartDateStringWithFormat:event];
        
        // End date
        endDate = [SurveyHelper getEndDateStringWithTimeDifference:event];
        
        // Agent Score
        if (event.scoreVal) {
            agentScore = [NSString stringWithFormat:@"%@%%", [Helper getDecimalWithRemovingExtraZero:event.scoreVal]];
        }
        
        // Great Job on
        greatJobOn = [event getGreatJobOnString];
        
        // Area of improvement
        if (event.note) {
            areaOfImproveMent = [Helper decodeStringForHtmlCharacters:event.note];
        }
        
        // Opportunity
        opportunity =  [Helper decodeStringForHtmlCharacters:[SurveyHelper getOpportunitiesString:event.symptomText]];
        
        // Update filters
        self.filter.locationId = event.tenantLocationId;
        self.filter.agentId = event.respondentId;
        self.filter.performanceLeaderId = event.surveyorId;
        self.filter.obTypeId = event.questionnaireId;
    }
    self.performanceLeader = performanceLeader;
    
    self.startDateString = startDate;
    
    NSArray *array = @[ @{
                            KEY_TITLE : self.locationKey,
                            KEY_VALUE : locationName,
                            },
                        @{
                            KEY_TITLE : self.plKey,
                            KEY_VALUE : performanceLeader
                            },
                        @{
                            KEY_TITLE : self.agentKey,
                            KEY_VALUE : agentName,
                            },
                        @{
                            KEY_TITLE : self.observationTypeKey,
                            KEY_VALUE : observationType,
                            },
                        @{
                            KEY_TITLE : [App_Delegate.keyMapping getKeyValue:@"KEY_OB_START_DATE"],
                            KEY_VALUE : startDate,
                            },
                        @{
                            KEY_TITLE : [App_Delegate.keyMapping getKeyValue:@"KEY_OB_END_DATE"],
                            KEY_VALUE : endDate,
                            },
                        @{
                            KEY_TITLE : self.scoreKey,
                            KEY_VALUE : agentScore,
                            },
                        @{
                            KEY_TITLE : [App_Delegate.keyMapping getKeyValue:@"KEY_OB_GREAT_JOB_ON"],
                            KEY_VALUE : greatJobOn,
                            },
                        @{
                            KEY_TITLE : [App_Delegate.keyMapping getKeyValue:@"KEY_OB_AREA_OF_IMPROV"],
                            KEY_VALUE : areaOfImproveMent,
                            },
                        @{
                            KEY_TITLE : [App_Delegate.keyMapping getKeyValue:@"KEY_OB_OPPORTUNITY"],
                            KEY_VALUE : opportunity,
                            },
                        ];
    _dataArray = [NSMutableArray new];
    self.dataArray = [NSMutableArray arrayWithArray:array];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

//---------------------------------------------------------------

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"FilterItemCell%d%d", (int)indexPath.section, (int)indexPath.row];
    
    FilterItemCell *cell = (FilterItemCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[FilterItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    NSDictionary *details = [self.dataArray objectAtIndex:indexPath.row];
    cell.itemLabel.text = [details objectForKey:KEY_TITLE];
    cell.selectedItemLabel.text = [details objectForKey:KEY_VALUE];
    
    switch (self.eventType) {
        case OB_NEW_EVENT : case PENDING_EVENT: {
            
            switch (indexPath.row) {
                case 1: {
                    if ([User signedInUser].isPerformanceLeader.boolValue) {
                        cell.userInteractionEnabled = NO;
                        cell.arrowImage.hidden = YES;
                    } else {
                        cell.userInteractionEnabled = YES;
                        cell.arrowImage.hidden = NO;
                    }
                    break;
                }
                case 4:
                    cell.arrowImage.hidden = YES;
                    break;
                default:
                    cell.arrowImage.hidden = NO;
                    break;
            }
            
            break;
        }
        case OB_COMPLETED_EVENT: {
            if (indexPath.row == 2 || indexPath.row == 4) {
                cell.arrowImage.hidden = NO;
            } else {
                cell.arrowImage.hidden = YES;
            }
            break;
        }
        case OB_COMPLETED_EVENT_VIEW: {
            cell.arrowImage.hidden = YES;
            break;
        }
        default:
            break;
    }
    
    return cell;
}

//---------------------------------------------------------------

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FilterDetailViewController *controller = [App_Delegate.mainStoryboard instantiateViewControllerWithIdentifier:@"FilterDetailViewController"];
    controller.filter = self.filter;
    controller.delegate = self;
   
    if (self.eventType == OB_NEW_EVENT || self.eventType == PENDING_EVENT) {
            switch (indexPath.row) {
                case 0:
                    controller.itemId = LocationFilter;
                    break;
                case 1:
                    controller.itemId = PerformanceLeaderFilter;
                    break;
                case 2:
                    controller.itemId = AgentFilter;
                    break;
                case 3:
                    controller.itemId = ObservationTypeFilter;
                    break;
                case 4:{
                    return;
                    break;
                }
                default:
                    return;
                    break;
            }
            
            controller.locationList = self.locationList;
            controller.performanceLeaderList = self.performanceLeaderList;
            controller.agentList = self.agentList;
            controller.obTypeList = self.obTypeList;
            [self.navigationController pushViewController:controller animated:YES];
    }
    else if (self.eventType == OB_COMPLETED_EVENT) {
            switch (indexPath.row) {
                case 2: {
                    controller.itemId = AgentFilter;
                    controller.agentList = self.agentList;
                    [self.navigationController pushViewController:controller animated:YES];
                    break;
                }
                case 4: {
                    // Start date and time
                    IQActionSheetPickerView *picker = [[IQActionSheetPickerView alloc] initWithTitle:nil delegate:self];
                    picker.delegate = self;
                    [picker setTag:7];
                    [picker setActionSheetPickerStyle:IQActionSheetPickerStyleDateTimePicker];
                    [picker show];
                    break;
                }
                default:
                    return;
                    break;
            }
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark FilterDetailView delegate methods

//---------------------------------------------------------------

- (void) reloadFilters:(FilterType)filterType withFilter:(Filters *)filter {
    
    // If filter changed reload questions
    self.isReloadQuestions = YES;
    self.filter = filter;
    
    NSDictionary *details = @{
                              KEY_TITLE : self.locationKey,
                              KEY_VALUE : self.filter.locationName,
                              };
    [self.dataArray replaceObjectAtIndex:0 withObject:details];

    switch (filterType) {
        case LocationFilter: {
            
            //UPDATE: 26-Sep-2017 ~ If user is Performance leader than show PL = Login User
            if (![User signedInUser].isPerformanceLeader.boolValue) {
                [self getPerformanceLeader:^(BOOL isFinish) {
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    [self.tableView reloadData];
                }];
            }
            
            [self getAgents:^(BOOL isFinish) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self.tableView reloadData];
            }];
            [self getObservationType:^(BOOL isFinish) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self.tableView reloadData];
            }];
            break;
        }
            
        case PerformanceLeaderFilter: {
            User *user = [self.performanceLeaderList getUser:self.filter.performanceLeaderId];
            NSString *userName = user.name ? user.name : @"";
            NSDictionary *details = @{
                                      KEY_TITLE : self.plKey,
                                      KEY_VALUE : userName,
                                      };
            [self.dataArray replaceObjectAtIndex:1 withObject:details];
            [self.tableView reloadData];
            break;
        }
        case AgentFilter: {
//Agent
            User *user = [self.agentList getUser:self.filter.agentId];
            NSString *userName = user.name ? user.name : @"";
            NSDictionary *details = @{
                                      KEY_TITLE : self.agentKey,
                                      KEY_VALUE : userName,
                                      };
            [self.dataArray replaceObjectAtIndex:2 withObject:details];
            [self.tableView reloadData];
            break;
        }
        case ObservationTypeFilter: {
            ObservationType *type = [self.obTypeList getObservationType:self.filter.obTypeId];
            
            NSString *sureveyCategoryName = [Helper removeWhiteSpaceAndNewLineCharacter:type.surveyCategory.name];
            sureveyCategoryName = sureveyCategoryName.length > 0 ? [NSString stringWithFormat:@"(%@)", sureveyCategoryName] : @"";
            NSString *observationType = @"";
            if (type.name.length > 0) {
                observationType = [NSString stringWithFormat:@"%@ %@", type.name, sureveyCategoryName];
            } else {
                observationType = [NSString stringWithFormat:@"%@", sureveyCategoryName];
            }
            
            NSDictionary *details = @{
                                      KEY_TITLE : self.observationTypeKey,
                                      KEY_VALUE : observationType,
                                      };
            [self.dataArray replaceObjectAtIndex:3 withObject:details];
            [self.tableView reloadData];
            break;
        }
        default:
            break;
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark IQActionSheetPickerView methods

//---------------------------------------------------------------

- (void) actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray *)titles {
}

//---------------------------------------------------------------

- (void) actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectDate:(NSDate *)date {
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSDate *startTime = date;
        NSDate *endTime = [startTime dateByAddingTimeInterval:self.dataManager.event.timeDifference.doubleValue];
        
        long long utcStartTimeInMilliSeconds = [Helper utcTimeIntervalForDate:startTime];
        long long utcEndTimeInMilliSeconds = [Helper utcTimeIntervalForDate:endTime];

        self.dataManager.event.startDate = [NSString stringWithFormat:@"%lld", utcStartTimeInMilliSeconds];
        self.dataManager.event.endDate =  [NSString stringWithFormat:@"%lld", utcEndTimeInMilliSeconds];
                
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setTitleArrayForEditEvent:self.dataManager.event];
            [self.tableView reloadData];
        });
    });
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    
    self.isReloadQuestions = YES;
    // table settings
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 50;
    
    // Start Button title
    [self.startButton setTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_OB_GET_STARTED"] forState:UIControlStateNormal];
    self.startButton.hidden = YES;
    self.viewButton.hidden = NO;
    [self.view bringSubviewToFront:self.startButton];
    
    // Get filters from stored
    _filter = [ServicePreference customObjectWithKey:KEY_OBSERVATION_FILTERS];
    if (!_filter) {
        _filter = [Filters new];
    }
    
    _dataManager = [DataManager new];    
    self.dataManager.event = [Event new];
    self.dataManager.eventType = self.eventType;
    self.dataManager.surveyType = self.surveyType;
    
    self.locationKey = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_LOCATION"];
    if (self.surveyType == OBSERVATION) {
        self.title = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_OBSERVE_AGENT"];
        self.dataManager.surveyEntityTypeString = KEY_PARAM_CONST_OBSERVATION;
        self.filter.surveyEntityTypeString = KEY_PARAM_CONST_OBSERVATION;

        self.plKey = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_PL"];
        self.agentKey = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_AGENT"];
        self.observationTypeKey = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_OBTYPE"];
        self.scoreKey = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_AGENT_SCORE"];
        self.plNotFoundKey = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_NO_PL_FOUND"];
        self.agentNotFoundKey = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_NO_AGENT_FOUND"];
        self.typeNotFoundKey = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_NO_OBTYPE_FOUND"];
        self.filter.surveyEntityType = @"0";
    } else {
        self.title = [App_Delegate.keyMapping getKeyValue:@"KEY_CC_COACH_TEAM_MEMBER"];
        self.dataManager.surveyEntityTypeString = KEY_PARAM_CONST_COACHING;
        self.filter.surveyEntityTypeString = KEY_PARAM_CONST_COACHING;
        
        self.plKey = [App_Delegate.keyMapping getKeyValue:@"KEY_CC_COACH"];
        self.agentKey = [App_Delegate.keyMapping getKeyValue:@"KEY_CC_TEAM_MEMBER"];
        self.observationTypeKey = [App_Delegate.keyMapping getKeyValue:@"KEY_CC_COACHING_TYPE"];
        self.scoreKey = [App_Delegate.keyMapping getKeyValue:@"KEY_CC_TEAM_MEMBER_SCORE"];
        self.plNotFoundKey = [App_Delegate.keyMapping getKeyValue:@"KEY_CC_NO_COACH_FOUND"];
        self.agentNotFoundKey = [App_Delegate.keyMapping getKeyValue:@"KEY_CC_NO_TEAM_MEMBER_FOUND"];
        self.typeNotFoundKey = [App_Delegate.keyMapping getKeyValue:@"KEY_CC_NO_COACHINGTYPE_FOUND"];
        self.filter.surveyEntityType = @"1";
    }
    
    // Set Titles based on event type
    [self createTitleArray];
    
    switch (self.eventType) {
        case OB_NEW_EVENT: {
            self.startButton.hidden = NO;
            self.viewButton.hidden = YES;
            // Call filter services
            [self callFilterWebServices];
            break;
        }
        case OB_COMPLETED_EVENT: {
            [self.viewButton setTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_OB_EDIT_SURVEY"] forState:UIControlStateNormal];
            // Call only agent service
            [self getAgents:^(BOOL isFinish) {
                // Call service to get event
                [self callWebServiceToGetEvent];
            }];
            break;
        }
        case OB_COMPLETED_EVENT_VIEW: {
            [self.viewButton setTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_OB_VIEW_SURVEY"] forState:UIControlStateNormal];
            
            // Call service to get event
            [self callWebServiceToGetEvent];
            break;
        }
        case PENDING_EVENT: {
            self.startButton.hidden = NO;
            self.viewButton.hidden = YES;
            // Call filter services
            self.dataManager.event = self.event;
            [self callFilterWebServices];
            break;
        }
        default: break;
    }
}

//---------------------------------------------------------------

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.dataManager.isCompleted) {
        self.dataManager.isCompleted = NO;
        [self.navigationController popViewControllerAnimated:YES];
    }
}

//---------------------------------------------------------------

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    // Save filters
    [ServicePreference saveCustomObject:self.filter key:KEY_OBSERVATION_FILTERS];
}

//---------------------------------------------------------------

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showQuestions"]) {
        UITabBarController *tabBarController = [segue destinationViewController];                
        
        if (self.eventType == OB_NEW_EVENT || self.eventType == PENDING_EVENT) {
            // Start Date and Time - Only for new event or pending event
            long long utcTimeInMilliSeconds = [Helper utcTimeIntervalForDate:[NSDate date]];
            self.dataManager.event.startDate = [NSString stringWithFormat:@"%lld", utcTimeInMilliSeconds];
            
            // Clear previous records
            self.dataManager.event.note = @"";
            // Reloading table to reflect start date
            [self.tableView reloadData];
        }
        
        if (tabBarController.viewControllers.count > 0) {
            QuestionContainer *controller = (QuestionContainer *)[[tabBarController viewControllers] objectAtIndex:0];
            controller.filter = self.filter;
            controller.dataManager = self.dataManager;
            controller.isReloadQuestions = self.isReloadQuestions;
            
            SurveyTabController *tabBar = (SurveyTabController*)controller.parentViewController;
            tabBar.filter = self.filter;
            tabBar.dataManager = self.dataManager;
            
            // Next time if filter is not changed - do not reload questions
            self.isReloadQuestions = NO;
        }
    }
}

//---------------------------------------------------------------

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"showQuestions"]) {
        // Validations
        // 1. Check for performance leader
        if (!self.filter.performanceLeaderId) {
            [Helper showAlertWithTitle:@"" message:self.plNotFoundKey forController:self];
            return NO;
        }
        
        // 2. Check for agent
        if (!self.filter.performanceLeaderId) {
            [Helper showAlertWithTitle:@"" message:self.agentNotFoundKey forController:self];
            return NO;
        }
        
        // 3. Check for Observation type
        if (!self.filter.performanceLeaderId) {
            [Helper showAlertWithTitle:@"" message:self.typeNotFoundKey forController:self];
            return NO;
        }
    }
    return YES;
}

//---------------------------------------------------------------

@end
