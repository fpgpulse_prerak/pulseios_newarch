//
//  SurveyCard.h
//  IN-Gauge
//
//  Created by Mehul Patel on 14/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "Helper.h"

@interface SurveyCard : UIView

@property (nonatomic, strong) UIImageView   *locationImageView;
@property (nonatomic, strong) UIImageView   *timerImageView;
@property (nonatomic, strong) UILabel       *statusLabel;
@property (nonatomic, strong) UILabel       *agentNameLabel;
@property (nonatomic, strong) UILabel       *performanceLeaderLabel;
@property (nonatomic, strong) UILabel       *observationTypeLabel;
@property (nonatomic, strong) UILabel       *scoreLabel;
@property (nonatomic, strong) UILabel       *seperationLine;
@property (nonatomic, strong) UILabel       *locationLabel;
@property (nonatomic, strong) UILabel       *scheduledOnLabel;

@end
