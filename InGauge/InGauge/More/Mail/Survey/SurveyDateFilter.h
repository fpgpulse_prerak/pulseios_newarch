//
//  SurveyDateFilter.h
//  IN-Gauge
//
//  Created by Mehul Patel on 04/09/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "Helper.h"
#import "AppDelegate.h"
#import <RMDateSelectionViewController/RMDateSelectionViewController.h>
#import "BaseTableViewController.h"

@protocol DateFilterDelegate <NSObject>

- (void) reloadSurveyDetailsFromDateFilter;

@end

@interface SurveyDateFilter : BaseTableViewController

@property (nonatomic, weak) IBOutlet UILabel    *startDate;
@property (nonatomic, weak) IBOutlet UILabel    *endDate;
@property (nonatomic, weak) IBOutlet UILabel    *comStartDate;
@property (nonatomic, weak) IBOutlet UILabel    *comEndDate;
@property (nonatomic, weak) IBOutlet UISwitch   *comSwitch;
// Lable
@property (nonatomic, weak) IBOutlet UILabel    *lblstartDate;
@property (nonatomic, weak) IBOutlet UILabel    *lblendDate;
@property (nonatomic, weak) IBOutlet UILabel    *lblcomStartDate;
@property (nonatomic, weak) IBOutlet UILabel    *lblcomEndDate;
@property (nonatomic, weak) IBOutlet UILabel    *lblcomSwitch;


@property (nonatomic, weak) id <DateFilterDelegate>     delegate;
@property (nonatomic, assign) BOOL                      isShowCompareDate;
@property (nonatomic, assign) BOOL                      removeDateLimit;

@end
