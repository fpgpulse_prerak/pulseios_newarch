//
//  SurveyHelper.h
//  IN-Gauge
//
//  Created by Mehul Patel on 17/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Event.h"
#import "BaseViewController.h"

@interface SurveyHelper : NSObject

// Date calculations and conversion
+ (NSString *) getCurrentTime;
+ (NSDate *) getLocalDateConvertFromUtcTimeStamp:(long long)timeStamp;
+ (NSString *) getLocalDateInStringConvertFromUtcTimeStamp:(long long)timeStamp;
+ (NSTimeInterval) calculateTimeDifference:(Event *)event;
+ (NSString *) getEndDateStringWithTimeDifference:(Event *)event;
+ (NSString *) getStartDateStringWithFormat:(Event *)event;

// String calculations and formatting
+ (NSString *) getOpportunitiesString:(NSString *)symptomText;
+ (NSString *) createObservationTypeString:(Event *)event;
+ (void) showAlert:(EVENT_TYPE)eventType forController:(BaseViewController *)controller;

@end
