//
//  SurveyHelper.m
//  IN-Gauge
//
//  Created by Mehul Patel on 17/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "SurveyHelper.h"

@implementation SurveyHelper

//---------------------------------------------------------------

+ (NSString *) getCurrentTime {
    // get current date/time
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    [dateFormatter setDateFormat:OB_DATE_FORMAT];
    NSString *currentTime = [dateFormatter stringFromDate:today];
    return currentTime;
}

//---------------------------------------------------------------

+ (NSDate *) getLocalDateConvertFromUtcTimeStamp:(long long)timeStamp {
    // Convert UTC Milliseconds to UTC date string
    NSString *utcDateString = [Helper utcDateFromTimeInterval:timeStamp];
    // Convert UTC date to Local Date
    NSDate *localDate = [NSDate dateFromNSString:utcDateString withFormat:OB_DATE_FORMAT];
    return localDate;
}

//---------------------------------------------------------------

+ (NSString *) getLocalDateInStringConvertFromUtcTimeStamp:(long long)timeStamp {
    NSDate *localDate = [self getLocalDateConvertFromUtcTimeStamp:timeStamp];
    // Convert date to string
    NSString *localDateInString = [localDate toNSString:OB_DATE_FORMAT];
    return localDateInString;
}

//---------------------------------------------------------------

+ (NSTimeInterval) calculateTimeDifference:(Event *)event {
    NSDate *localStartDate = [self getLocalDateConvertFromUtcTimeStamp:event.startDate.doubleValue];
    NSDate *localEndDate = [self getLocalDateConvertFromUtcTimeStamp:event.endDate.doubleValue];
    
    // Calculate time difference in seconds
    NSTimeInterval timeDifference = [localEndDate timeIntervalSinceDate:localStartDate];
    return timeDifference;
}


//---------------------------------------------------------------

+ (NSString *) getStartDateStringWithFormat:(Event *)event {
    // Convert UTC date to local time
    NSDate *localStartDate = [self getLocalDateConvertFromUtcTimeStamp:event.startDate.doubleValue];
    
    // Get strings from date
    NSString *startDateString = [localStartDate toNSString:OB_DATE_FORMAT];
    
    // Start date
    NSString *startDate = startDateString ? startDateString : @"";
    return startDate;
}

//---------------------------------------------------------------

+ (NSString *) getEndDateStringWithTimeDifference:(Event *)event {
    // Convert UTC date to local time
    NSDate *localStartDate = [self getLocalDateConvertFromUtcTimeStamp:event.startDate.doubleValue];
    NSDate *localEndDate = [self getLocalDateConvertFromUtcTimeStamp:event.endDate.doubleValue];
    
    // Calculate minutes
    NSDateComponents *components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate: localStartDate toDate:localEndDate options: 0];
    //        NSInteger hour = [components hour];
    NSInteger minutes = [components minute];
    NSInteger seconds = [components second];
    
    // Get strings from date
    NSString *endDateString = [localEndDate toNSString:OB_DATE_FORMAT];
    
    NSString *endDate = endDateString;
    
    NSString *formateString = [NSString stringWithFormat:@"%@ (%02d:%02d Minutes)", endDateString, (int)minutes, (int)seconds];
    
    // End date
    endDate = formateString ? formateString : @"";
    return endDate;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark  methods

//---------------------------------------------------------------

+ (NSString *) getOpportunitiesString:(NSString *)symptomText {
    // Symptomtext - Opportunities
    if (symptomText) {
        // Remove " &&& " from symptomText string
        NSString *truncatedString = [symptomText stringByReplacingOccurrencesOfString:@" &&& " withString:@", "];
        
        // THIS IS SERVER SPECIFIED FUNCTIONALITY
        // IF USER IS SUPER ADMIN THEN SHOW ALL OPPORTUNITIES
        // ELSE ANY RANDOM TWO FROM A LIST
        if ([[User signedInUser] isSuperAdmin].boolValue) {
            return truncatedString;
        } else {
            NSArray *stringArray = [symptomText componentsSeparatedByString:@" &&& "];
            if (stringArray.count > 2) {
                NSString *randomString1 = [stringArray objectAtIndex:arc4random_uniform((int)stringArray.count)];
                NSString *randomString2 = [stringArray objectAtIndex:arc4random_uniform((int)stringArray.count)];
                return [NSString stringWithFormat:@"%@, %@",randomString1, randomString2];
            } else {
                return truncatedString;
            }
        }
    } else {
        return @"";
    }
}

//---------------------------------------------------------------

+ (NSString *) createObservationTypeString:(Event *)event {
    NSString *questionnaireSurveyCategoryName = [Helper removeWhiteSpaceAndNewLineCharacter:event.questionnaireSurveyCategoryName];
    questionnaireSurveyCategoryName = questionnaireSurveyCategoryName.length > 0 ? [NSString stringWithFormat:@"(%@)", questionnaireSurveyCategoryName] : @"";
    NSString *observationType = [NSString stringWithFormat:@"%@ %@", event.questionnaireName, questionnaireSurveyCategoryName];
    return observationType;
}

//---------------------------------------------------------------

+ (void) showAlert:(EVENT_TYPE)eventType forController:(BaseViewController *)controller {
    // View event dont have information alert
    if (eventType == OB_COMPLETED_EVENT_VIEW) {
        return;
    }
    
    NSNumber *isShowInfoAlert = [ServicePreference objectForKey:DEFAULT_SHOW_COUNTER_COACHING_ALERT];
    BOOL isShow = NO;
    if (isShowInfoAlert == nil) {
        isShow = YES;
    } else {
        isShow = !isShowInfoAlert.boolValue;
    }
    if (isShow) {
        NSString *title = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_SELECT_TYPE_INFO"];
        NSString *message = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_INFO_MESSAGE"];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_TITLE_OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
        [alertController addAction:okAction];
        
        UIAlertAction *dontShowAction = [UIAlertAction actionWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_OB_INFO_DECLINED"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [ServicePreference setObject:[NSNumber numberWithBool:YES] forKey:DEFAULT_SHOW_COUNTER_COACHING_ALERT];
        }];
        [alertController addAction:dontShowAction];
        
        [controller presentViewController:alertController animated:YES completion:nil];
    }
}

//---------------------------------------------------------------

@end
