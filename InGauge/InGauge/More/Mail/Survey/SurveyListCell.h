//
//  SurveyListCell.h
//  IN-Gauge
//
//  Created by Mehul Patel on 14/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "SurveyCard.h"

@interface SurveyListCell : UITableViewCell

@property (nonatomic, strong) SurveyCard              *cardView;
@property (nonatomic, strong) UIView                    *bottomBar;
@property (nonatomic, strong) UIButton                  *removeButton;
@property (nonatomic, strong) UIButton                  *editButton;
@property (nonatomic, strong) UIButton                  *viewButton;
@property (nonatomic, strong) UILabel                   *line;
@property (nonatomic, strong) NSLayoutConstraint        *constraint;

@end
