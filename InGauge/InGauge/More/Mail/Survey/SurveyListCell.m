//
//  SurveyListCell.m
//  IN-Gauge
//
//  Created by Mehul Patel on 14/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "SurveyListCell.h"

@implementation SurveyListCell

//---------------------------------------------------------------

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    // Intial setup
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
    
    
    // Card view
    _cardView = [[SurveyCard alloc] init];
    self.cardView.backgroundColor = [UIColor whiteColor];
    [self.cardView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.contentView addSubview:self.cardView];
    
    // Constraints
    NSDictionary *views = @{@"cardView" : self.cardView};
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[cardView]-10-|" options: 0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[cardView]-5-|" options: 0 metrics:nil views:views]];
    
    [self createBottomBar];
    
    // Drop shadow
    self.cardView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    self.cardView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    self.cardView.layer.shadowRadius = 0.5f;
    self.cardView.layer.shadowOpacity = 2.0f;

    return self;
}

//---------------------------------------------------------------

- (void) createBottomBar {
    _bottomBar = [[UIView alloc] init];
    [self.bottomBar setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.bottomBar setBackgroundColor: [UIColor whiteColor]];
    [self.cardView addSubview:self.bottomBar];
    
    UIEdgeInsets insets = UIEdgeInsetsMake(8, 8, 8, 8);
    
    // Seperation line label
    _line = [[UILabel alloc] init];
    self.line.font = [UIFont fontWithName:AVENIR_FONT size:12];
    self.line.backgroundColor = TABLE_SEPERATOR_COLOR;
    self.line.lineBreakMode = NSLineBreakByWordWrapping;
    self.line.textAlignment = NSTextAlignmentRight;
    [self.line setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.bottomBar addSubview:self.line];
    
    _removeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.removeButton setImage:[UIImage imageNamed:TRASH_IMAGE] forState:UIControlStateNormal];
    [self.removeButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.removeButton setImageEdgeInsets:insets];
    self.removeButton.tintColor = APP_COLOR;
    [self.bottomBar addSubview:self.removeButton];
    
    _editButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.editButton setImage:[UIImage imageNamed:@"pencil"] forState:UIControlStateNormal];
    [self.editButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.editButton setImageEdgeInsets:insets];
    [self.bottomBar addSubview:self.editButton];
    
    _viewButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.viewButton setImage:[UIImage imageNamed:@"eye"] forState:UIControlStateNormal];
    [self.viewButton setImageEdgeInsets:insets];
    [self.viewButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.bottomBar addSubview:self.viewButton];
    
    //    [self.removeButton setBackgroundColor:[UIColor redColor]];
    //    [self.editButton setBackgroundColor:[UIColor blueColor]];
    //    [self.viewButton setBackgroundColor:[UIColor yellowColor]];
    
    // Constraints
    NSDictionary *views = @{@"line" : self.line, @"bottomBar" : self.bottomBar, @"editButton" : self.editButton, @"removeButton" : self.removeButton, @"viewButton" : self.viewButton};
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[bottomBar]|" options: 0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[bottomBar(35)]|" options: 0 metrics:nil views:views]];
    
    [self.bottomBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[line]|" options: 0 metrics:nil views:views]];
    
    [self.bottomBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[line(1)]" options: 0 metrics:nil views:views]];
    
    [self.bottomBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[removeButton(35)]" options: 0 metrics:nil views:views]];
    [self.bottomBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[editButton(35)]" options: 0 metrics:nil views:views]];
    [self.bottomBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[viewButton(35)]" options: 0 metrics:nil views:views]];
    
    [self.bottomBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[editButton(35)]-10-[viewButton(35)]-10-[removeButton(35)]-10-|" options: 0 metrics:nil views:views]];
    
}

//---------------------------------------------------------------

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.bottomBar removeConstraints:[self.bottomBar constraints]];
    
//    self.viewButton.backgroundColor = [UIColor redColor];
//    self.removeButton.backgroundColor = [UIColor redColor];
//    self.editButton.backgroundColor = [UIColor redColor];
    
    NSDictionary *views = @{@"line" : self.line, @"bottomBar" : self.bottomBar, @"editButton" : self.editButton, @"removeButton" : self.removeButton, @"viewButton" : self.viewButton};
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[bottomBar]|" options: 0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[bottomBar(35)]|" options: 0 metrics:nil views:views]];
    
    [self.bottomBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[line]|" options: 0 metrics:nil views:views]];
    
    [self.bottomBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[line(1)]" options: 0 metrics:nil views:views]];
    
    [self.bottomBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[removeButton(35)]" options: 0 metrics:nil views:views]];
    [self.bottomBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[editButton(35)]" options: 0 metrics:nil views:views]];
    [self.bottomBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[viewButton(35)]" options: 0 metrics:nil views:views]];
    
    if (self.viewButton.hidden) {
        [self.bottomBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[editButton(35)]-10-[removeButton(35)]-10-|" options: 0 metrics:nil views:views]];
    } else {
        [self.bottomBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[editButton(35)]-10-[viewButton(35)]-10-[removeButton(35)]-10-|" options: 0 metrics:nil views:views]];
    }
}

//---------------------------------------------------------------

@end
