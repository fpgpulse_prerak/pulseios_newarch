//
//  SurveyListController.m
//  IN-Gauge
//
//  Created by Mehul Patel on 14/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "SurveyListController.h"
#import "SLButton.h"
#import "SurveyDateFilter.h"
#import "SurveyListCell.h"
#import "SurveyAgentController.h"
#import "LGRefreshView.h"
#import "EventManager.h"
#import "FilterController.h"
#import <MJRefresh.h>

#define SURVEY_STATUS_ALL       [App_Delegate.keyMapping getKeyValue:@"KEY_OB_STATUS_ALL"]
#define SURVEY_STATUS_COMPLETED [App_Delegate.keyMapping getKeyValue:@"KEY_OB_STATUS_COMPLETED"]
#define SURVEY_STATUS_PENDING   [App_Delegate.keyMapping getKeyValue:@"KEY_OB_STATUS_PENDING"]
#define SURVEY_STATUS_CANCELLED [App_Delegate.keyMapping getKeyValue:@"KEY_OB_STATUS_CANCELLED"]

#define REMOVE_BUTTON_TAG   1000
#define EDIT_BUTTON_TAG     5000
#define VIEW_BUTTON_TAG     10000

#define TEXT_STATUS_COMPLETED   @"COMPLETED"
#define TEXT_STATUS_PENDING     @"PENDING"
#define TEXT_STATUS_CANCELLED   @"CANCELLED"

@interface SurveyListController() <UITableViewDelegate, UITableViewDataSource, DateFilterDelegate, FilterDelegate>

@property (nonatomic, weak) IBOutlet UITableView        *tableView;
@property (nonatomic, weak) IBOutlet SLButton           *filterButton;
@property (nonatomic, weak) IBOutlet UIButton           *selectionButton;
@property (nonatomic, weak) IBOutlet UILabel            *dateLabel;
@property (nonatomic, weak) IBOutlet UIView             *filterView;
@property (nonatomic, weak) IBOutlet UIBarButtonItem    *takeObButton;

@property (nonatomic, strong) UIView                    *noRecordsView;
@property (nonatomic, strong) LGRefreshView             *refreshView;
@property (nonatomic, strong) LocationList              *locationList;
@property (nonatomic, strong) OBCategoryList            *categoryList;
@property (nonatomic, strong) Filters                   *filter;
@property (nonatomic, strong) EventList                 *eventList;
@property (nonatomic, strong) EventManager              *eventManager;

@property (nonatomic, strong) NSArray                   *statusArray;
@property (nonatomic, strong) NSArray                   *dataArray;
@property (nonatomic, strong) NSIndexPath               *selectedIndexPath;

@property (nonatomic) NSInteger                         pageNo;
@property (nonatomic) NSInteger                         pageSize;
@property (nonatomic) BOOL                              isFirstLoad;
@property (nonatomic) BOOL                              isCellExpand;

@end

@implementation SurveyListController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUpdateObservationDashboard object:nil];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) filterButtonTapped:(id)sender {
    // Location
    RefineItem *item1 = [RefineItem new];
    item1.itemId = self.filter.locationId;
    item1.title = [App_Delegate.keyMapping getKeyValue:@"KEY_LOCATION"];
    item1.displayName = self.filter.locationName;
    item1.subItems = self.locationList.locationList;
    item1.filterType = LocationFilter;
    
    // Survey Category
    RefineItem *item2 = [RefineItem new];
    item2.itemId = self.filter.categoryId;
    item2.title = @"Category"; //[App_Delegate.keyMapping getKeyValue:@"KEY_LOCATION"];
    item2.displayName = self.filter.categoryName;
    item2.subItems = self.categoryList.categoryList;
    item2.filterType = ObservationCategory;
    
    // Survey Status
    RefineItem *item3 = [RefineItem new];
    item3.itemId = nil;
    item3.title = @"Status"; //[App_Delegate.keyMapping getKeyValue:@"KEY_LOCATION"];
    item3.displayName = self.filter.surveyStatus;
    item3.subItems = self.statusArray;
    item3.filterType = ObservationStatus;
    
    FilterController *controller = [App_Delegate.mainStoryboard instantiateViewControllerWithIdentifier:@"FilterController"];
    controller.delegate = self;
    controller.filterItems = @[item1, item2, item3];
    Filters *tempFilters = self.filter.copy;
    controller.filter = tempFilters;
    controller.openFrom = OpenFromSurveyList;
    [self presentViewController:controller animated:YES completion:^{
    }];
}

//---------------------------------------------------------------

- (void) removeButtonTapped:(UIButton *)sender {
    @try {
        [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];
        
        NSInteger index = sender.tag - REMOVE_BUTTON_TAG;
        Event *event = (Event *)[self.dataArray objectAtIndex:index];
        NSString *title = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_DELETE_TITLE"];
        NSString *message = @"";
        
        if (self.surveyType == OBSERVATION) {
            message = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_DELETE_MESSAGE"];
        } else {
            message = [App_Delegate.keyMapping getKeyValue:@"KEY_CC_DELETE_MESSAGE"];
        }
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *yesAction = [UIAlertAction actionWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_YES"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            // Call service to remove observation
            MBProgressHUD *hud = [Helper showLoading:self.view];
            [ServiceManager callWebServiceToDeleteSingleObservation:event.eventId usingBlock:^(NSDictionary *response) {
                [Helper hideLoading:hud];
                if (response) {
                    [self reloadSurveyListDetails];
                }
            }];
        }];
        [alertController addAction:yesAction];
        
        UIAlertAction *noAction = [UIAlertAction actionWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_NO"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alertController addAction:noAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------
    
- (void) editButtonTapped:(UIButton *)sender {
    @try {
        [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];
        
        NSInteger index = sender.tag - EDIT_BUTTON_TAG;
        Event *event = [self.dataArray objectAtIndex:index];
        
        EVENT_TYPE eventType = OB_COMPLETED_EVENT;
        if ([event.surveyStatus isEqualToString:SURVEY_STATUS_PENDING]) {
            eventType = PENDING_EVENT;
        }
        
        // Open container view
        UIStoryboard *observationStoryboard = [UIStoryboard storyboardWithName:KEY_OBSERVATION_STORYBOARD_NAME bundle:[NSBundle mainBundle]];
        SurveyAgentController *controller = [observationStoryboard instantiateViewControllerWithIdentifier:@"SurveyAgentController"];
        controller.eventType = eventType;
        controller.event = event;
        controller.surveyType = self.surveyType;
        [self.navigationController pushViewController:controller animated:YES];
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------
        
- (void) viewButtonTapped:(UIButton *)sender {
    @try {
        [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];
        
        NSInteger index = sender.tag - VIEW_BUTTON_TAG;
        Event *event = [self.dataArray objectAtIndex:index];
        // Open container view
        UIStoryboard *observationStoryboard = [UIStoryboard storyboardWithName:KEY_OBSERVATION_STORYBOARD_NAME bundle:[NSBundle mainBundle]];
        SurveyAgentController *controller = [observationStoryboard instantiateViewControllerWithIdentifier:@"SurveyAgentController"];
        controller.eventType = OB_COMPLETED_EVENT_VIEW;
        controller.event = event;
        controller.surveyType = self.surveyType;
        [self.navigationController pushViewController:controller animated:YES];
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WEB SERVICE methods

//---------------------------------------------------------------

- (void) callFilterWebServicesInBackground {

    // Show loading on filter button
    [self.filterButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [self.filterButton showLoading];
    self.filter.accessible      = [NSNumber numberWithBool:NO];
    self.filterButton.enabled   = NO;
    
    // Reset all filters
    self.filter.locationId      = [NSNumber numberWithInt:0];
    self.filter.orderBy         = KEY_ORDER_BY_VALUE;
    self.filter.sort            = KEY_SORT_BY_ASC_VALUE;
    self.filter.activeStatus    = KEY_PARAM_CONST_ACTIVE_STATUS;
    self.filter.locationName    = @"";
    
    // Call service to get locations
    MBProgressHUD *hud = [Helper showLoading:self.view];
    
    NSDictionary *details = [self.filter serializeFiltersGoalSettingLocationService];
    [ServiceManager callWebServiceToGetLocations:details usingBlock:^(LocationList *locationList) {
        [Helper hideLoading:hud];
        self.locationList = locationList;
        
        //***NOTE: Add static location "All locations" to get all details
        Location *allLocations = [Location new];
        allLocations.name = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_ALL_LOCATION"];
        allLocations.locationId = [NSNumber numberWithInt:-1];
        
        NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.locationList.locationList];
        [tempArray insertObject:allLocations atIndex:0];
        self.locationList.locationList = [NSArray arrayWithArray:tempArray];
        tempArray = nil;

        if (self.locationList.locationList.count > 0) {
            // First location
            Location *location = [self.locationList.locationList objectAtIndex:0];
            self.filter.locationId = location.locationId;
            self.filter.locationName = location.name;
        }
                
        // Call service to get Observation categories
        MBProgressHUD *hud = [Helper showLoading:self.view];
        [ServiceManager callWebServiceToGetObservationCategories:self.filter usingBlock:^(OBCategoryList *categoryList) {
            [Helper hideLoading:hud];
            self.categoryList = categoryList;
            
            //***NOTE: Add static category "All Categories" to get all details
            OBCategory *category = [OBCategory new];
            category.name = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_ALL_CATEGORIES"];
            // UPDATE: 21-Sep-2017 ~ Use 0 for all categories
            // For all categories
            category.categoryId = [NSNumber numberWithInt:0];
            
            NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.categoryList.categoryList];
            [tempArray insertObject:category atIndex:0];
            self.categoryList.categoryList = [NSArray arrayWithArray:tempArray];
            tempArray = nil;
            
            if (self.categoryList.categoryList.count > 0) {
                // First location group
                OBCategory *category = [self.categoryList.categoryList objectAtIndex:0];
                self.filter.categoryId = category.categoryId;
                self.filter.categoryName = category.name;
            }
            
            // Enable filterbutton
            [self.filterButton setImage:[UIImage imageNamed:@"filter"] forState:UIControlStateNormal];
            [self.filterButton hideLoading];
            
            // Call Observation list service
            [self callWebServiceToGetObservationList];
        }];
    }];
}

//---------------------------------------------------------------

- (void) callWebServiceToGetObservationList {
    MBProgressHUD *hud = [Helper showLoading:self.view];
    
    // Update end date to match current time
    [self appendCurrentTimeInEndDate];
    
    NSDictionary *params = [self.filter serializeFiltersForObservationListService];
    
    NSString *surveyStatus = @"-1";
    if ([self.filter.surveyStatus isEqualToString:SURVEY_STATUS_ALL]) {
        surveyStatus = @"-1";
    } else if ([self.filter.surveyStatus isEqualToString:SURVEY_STATUS_COMPLETED]) {
        surveyStatus = @"0";
    } else if ([self.filter.surveyStatus isEqualToString:SURVEY_STATUS_PENDING]) {
        surveyStatus = @"1";
    } else if ([self.filter.surveyStatus isEqualToString:SURVEY_STATUS_CANCELLED]) {
        surveyStatus = @"2";
    }
    
    NSMutableDictionary *bodyDetails = [NSMutableDictionary dictionaryWithDictionary:params];
    [bodyDetails setObject:[NSNumber numberWithInteger:self.pageNo] forKey:@"first"];
    [bodyDetails setObject:[NSNumber numberWithInteger:self.pageSize] forKey:@"total"];
    [bodyDetails setObject:@"" forKey:@"searchKeywords"];
//    [bodyDetails setObject:@[@"updatedBy", @"updatedOn"] forKey:@"sortBy"];
    [bodyDetails setObject:@[@"startDate"] forKey:@"sortBy"];
    [bodyDetails setObject:surveyStatus forKey:@"surveyStatus"];
    [bodyDetails setObject:KEY_SORT_BY_DES_VALUE forKey:@"sortOrder"];
    
    params = [NSDictionary dictionaryWithDictionary:bodyDetails];
    bodyDetails = nil;
    
    // Remove old details from array
    if (self.pageNo == 0) {
        self.dataArray = nil;
    }
    
    [ServiceManager callWebServiceToGetListOfObservation:params usingBlock:^(EventList *eventList) {
        [Helper hideLoading:hud];
        self.eventList = eventList;
        // Add Events to device calendar - in Background
        [self performSelectorInBackground:@selector(storeEventsToLocalCalendar) withObject:self];
        
        NSMutableArray *tempArray = [NSMutableArray new];
        [tempArray addObjectsFromArray:self.dataArray];
        [tempArray addObjectsFromArray:self.eventList.eventList];
        
        self.dataArray = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
        [self.tableView reloadData];
        
        if (self.dataArray.count > 0) {
            self.noRecordsView.hidden = YES;
        } else {
            self.noRecordsView.hidden = NO;
        }

        // Hide load more if you get all records
        if (self.dataArray.count == self.eventList.total.intValue) {
            self.tableView.mj_footer.hidden = YES;
        } else {
            self.tableView.mj_footer.hidden = NO;
        }
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) dateSettings {
    // Default date if network is not there
    NSDate *endDate = [NSDate date];
    NSDate *startDate = [Helper firstDateOfMonth:endDate];

    self.filter.startDate = startDate;
    self.filter.endDate = endDate;
    
    // Set dates from Calendar syncronization
    self.eventManager.startDate = startDate;
    self.eventManager.endDate = endDate;
    
    NSString *startDateString =  [self.filter.startDate toNSString:APP_DISPLAY_DATE_TIME_FORMAT];
    NSString *endDateString =  [self.filter.endDate toNSString:APP_DISPLAY_DATE_TIME_FORMAT];
    self.dateLabel.text = [NSString stringWithFormat:@"%@ ~ %@", startDateString, endDateString];
    
    [ServicePreference setObject:startDateString forKey:SURVEY_START_DATE];
    [ServicePreference setObject:endDateString forKey:SURVEY_END_DATE];
}

//---------------------------------------------------------------

- (void) appendCurrentTimeInEndDate {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:(NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:self.filter.endDate];
    
    // 😑 Patch: 26-Sep-2017 ~ Service has some issue from back end so I am subtracting one second from date
    // Subtract one second from endDate to manage time zone for web application/Service
    NSString *stringDate = @"23:59:59";
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"HH:mm:ss";
    NSDate *timeDate = [dateFormatter dateFromString:stringDate];
    
    // Split this one in components as well but take the time part this time
    NSDateComponents *timeComponents = [[NSCalendar currentCalendar] components:(NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:timeDate];
    
    // Do some merging between the two date components
    dateComponents.hour = timeComponents.hour;
    dateComponents.minute = timeComponents.minute;
    dateComponents.second = timeComponents.second;
    
    // Extract the NSDate object again
    NSDate *restulDate = [calendar dateFromComponents:dateComponents];
    
    // Check if this was what you where looking for
    self.filter.endDate = restulDate;
}

//---------------------------------------------------------------

- (void) createPullToRefresh {
    // Pull to refresh ----
    __weak typeof(self) wself = self;
    _refreshView = [LGRefreshView refreshViewWithScrollView:self.tableView refreshHandler:^(LGRefreshView *refreshView) {
        if (wself) {
            __strong typeof(wself) self = wself;
            
            // Call Dashboard chart service
            [self reloadSurveyListDetails];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void) {
                [self.refreshView endRefreshing];
            });
        }
    }];
    _refreshView.tintColor = APP_COLOR;
}

//---------------------------------------------------------------

// Edit Survey!
- (SurveyListCell *) validationForEditEvents:(SurveyListCell *)cell forEvent:(Event *)event {
    
    // Bypass for system admin
    if ([User signedInUser].isSuperAdmin.boolValue) {
        return cell;
    }
    
    // 1. Check permission to edit events
    cell.editButton.hidden = NO;
    if (self.surveyType == OBSERVATION) {
        if (!App_Delegate.userPermissions.canAccessEditObservation.boolValue) {
            cell.editButton.hidden = YES;
        }
    } else {
        if (!App_Delegate.userPermissions.canAccessEditCounterCoaching.boolValue) {
            cell.editButton.hidden = YES;
        }
    }
    
    // 2. Performance leader can not view other user's events
    cell.editButton.hidden = [self isHideViewForPerformanceLeader:event.surveyorId.intValue];

    return cell;
}

//---------------------------------------------------------------

// View Survey!
- (SurveyListCell *) validationForViewEvents:(SurveyListCell *)cell forEvent:(Event *)event {
    
    // 1. HIDE VIEW BUTTON IN PENDING EVENTS
    if ([event.surveyStatus isEqualToString:SURVEY_STATUS_PENDING]) {
        [cell.viewButton setHidden:YES];
        return cell;
    }
    
    // Bypass for system admin
    if ([User signedInUser].isSuperAdmin.boolValue) {
        return cell;
    }

    // 2. Check permission to view events
    cell.viewButton.hidden = NO;
    if (self.surveyType == OBSERVATION) {
        if (!App_Delegate.userPermissions.canAccessViewObservation.boolValue) {
            cell.viewButton.hidden = YES;
        }
    } else {
        if (!App_Delegate.userPermissions.canAccessViewCounterCoaching.boolValue) {
            cell.viewButton.hidden = YES;
        }
    }
    return cell;
}

//---------------------------------------------------------------

// Remove Survey!
- (SurveyListCell *) validationForRemoveEvents:(SurveyListCell *)cell forEvent:(Event *)event {
    // Bypass for system admin
    if ([User signedInUser].isSuperAdmin.boolValue) {
        return cell;
    }
    
    // 1. Check permission to remove events
    cell.removeButton.hidden = YES;
    if (self.surveyType == OBSERVATION) {
        if (!App_Delegate.userPermissions.canAccessDeleteObservation.boolValue) {
            cell.removeButton.hidden = YES;
        }
    } else {
        if (!App_Delegate.userPermissions.canAccessDeleteCounterCoaching.boolValue) {
            cell.removeButton.hidden = YES;
        }
    }
    
    // 2. Performance leader can not remove other user's events
    if (event.surveyorId.intValue == [User signedInUser].userID.intValue) {
        cell.removeButton.hidden = YES;
    }
        
    cell.removeButton.hidden = [self isHideViewForPerformanceLeader:event.surveyorId.intValue];
    
    return cell;
}

//---------------------------------------------------------------

- (BOOL) isHideViewForPerformanceLeader:(int)leaderId {
    User *user = [User signedInUser];
    return (leaderId != user.userID.intValue) ? YES : NO;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

//---------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"SurveyListCell%d%d",(int)indexPath.section, (int)indexPath.row];
    
    SurveyListCell *cell = (SurveyListCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[SurveyListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    [cell.removeButton addTarget:self action:@selector(removeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [cell.editButton addTarget:self action:@selector(editButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [cell.viewButton addTarget:self action:@selector(viewButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    // Define tags
    [cell.removeButton setTag:REMOVE_BUTTON_TAG + indexPath.row];
    [cell.editButton setTag:EDIT_BUTTON_TAG + indexPath.row];
    [cell.viewButton setTag:VIEW_BUTTON_TAG + indexPath.row];
    [cell.removeButton setHidden:NO];
    [cell.viewButton setHidden:NO];
    [cell.editButton setHidden:NO];
    
    if (self.dataArray.count > 0) {
        Event *event = [self.dataArray objectAtIndex:indexPath.row];
        if (event) {
            //Status line color
            UIColor *color = [UIColor grayColor];
            if ([event.surveyStatus isEqualToString:SURVEY_STATUS_PENDING]) {
                color = OB_ORANGE_COLOR;
            } else if ([event.surveyStatus isEqualToString:SURVEY_STATUS_COMPLETED]) {
                color = OB_GREEN_COLOR;
            } else if ([event.surveyStatus isEqualToString:SURVEY_STATUS_CANCELLED]) {
                color = OB_GREY_COLOR;
            }
            
            cell.cardView.statusLabel.backgroundColor = color;
            
            // Status
            cell.cardView.statusLabel.text = [event.surveyStatus uppercaseString];
            
            // Agent
            cell.cardView.agentNameLabel.text = event.respondentName;
            
            // Performance leader
            cell.cardView.performanceLeaderLabel.text = event.surveyorName;
            
            // Observation type
            NSString *sureveyCategoryName = [Helper removeWhiteSpaceAndNewLineCharacter:event.questionnaireSurveyCategoryName];
            sureveyCategoryName = sureveyCategoryName.length > 0 ? [NSString stringWithFormat:@"(%@)", sureveyCategoryName] : @"";
            NSString *observationType = @"";
            if (event.questionnaireName.length > 0) {
                observationType = [NSString stringWithFormat:@"%@ %@", event.questionnaireName, sureveyCategoryName];
            } else {
                observationType = [NSString stringWithFormat:@"%@", sureveyCategoryName];
            }
            cell.cardView.observationTypeLabel.text = observationType;
            
            // Score
            if (event.scoreVal) {
                cell.cardView.scoreLabel.text = [NSString stringWithFormat:@"%0.2f %%", event.scoreVal.floatValue];
            } else {
                cell.cardView.scoreLabel.text = @"";
            }
            
            // location
            // Get organization name from ID.
            cell.cardView.locationLabel.text = event.tenantLocationName;
            
            // scheduled on - Start Date
            cell.cardView.scheduledOnLabel.text = [SurveyHelper getLocalDateInStringConvertFromUtcTimeStamp:event.startDate.doubleValue];
            
            // ~~~~ VALIDATIONS
            // 1. For view events
            // 2. For edit events
            // 3. For remove events
            // 4. For cancel event there is no action
            
            // 1. Validation for view events
            cell = [self validationForViewEvents:cell forEvent:event];
            
            // 2. Validation for edit events
            cell = [self validationForEditEvents:cell forEvent:event];

            // 3. Validation for edit events
            cell = [self validationForRemoveEvents:cell forEvent:event];
        
            // 4. HIDE ALL ACTIONS IF EVENT IS CANCELLED
            if ([event.surveyStatus isEqualToString:SURVEY_STATUS_CANCELLED]) {
                cell.editButton.hidden = YES;
                cell.viewButton.hidden = YES;
                cell.removeButton.hidden = YES;
            }
        }
    }
    return cell;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark FilterDelegate methods

//---------------------------------------------------------------

- (void) reloadItemsBasedOnFilters:(Filters *)filter withFilterItems:(NSArray *)filterItems {
    self.filter = filter;
    [self reloadSurveyListDetails];
}

//---------------------------------------------------------------

- (void) updateFilterArray:(NSArray *)updatedArray forFilterType:(FilterType)filterType{}

//---------------------------------------------------------------

#pragma mark
#pragma mark DateFilter methods

//---------------------------------------------------------------

- (void) reloadSurveyDetailsFromDateFilter {
    
    self.pageNo = 0;
    
    NSString *startDateString = [ServicePreference objectForKey:SURVEY_START_DATE];
    NSString *endDateString = [ServicePreference objectForKey:SURVEY_END_DATE];
    
    NSDate *startDate = [NSDate dateFromNSString:startDateString withFormat:APP_DISPLAY_DATE_TIME_FORMAT];
    NSDate *endDate = [NSDate dateFromNSString:endDateString withFormat:APP_DISPLAY_DATE_TIME_FORMAT];
    
    
    // 😑 Patch: 26-Sep-2017 ~ Service has some issue from back end so I am subtracting one second from date
    // Subtract one second from endDate to manage time zone for web application/Service
    DLog(@"End date :%@", endDate);
//    NSDate *finalDate = [Helper subtractSecondsFromDate:endDate seconds:-1];

    NSString *strFilterStartDate = [startDate toNSString:SERVICE_DATE_TIME_SHORT_FORMAT];
    NSString *strFilterEndDate = [endDate toNSString:SERVICE_DATE_TIME_SHORT_FORMAT];
    
    self.filter.startDate = [NSDate dateFromNSString:strFilterStartDate withFormat:SERVICE_DATE_TIME_SHORT_FORMAT];
    self.filter.endDate = [NSDate dateFromNSString:strFilterEndDate withFormat:SERVICE_DATE_TIME_SHORT_FORMAT];
    
    // Update date label
    self.dateLabel.text = [NSString stringWithFormat:@"%@ ~ %@", startDateString, endDateString];

    [self reloadSurveyListDetails];
}

//---------------------------------------------------------------

- (void) reloadSurveyListDetails {
    
    // Set dates from Calendar syncronization
    self.eventManager.startDate = self.filter.startDate;
    self.eventManager.endDate = self.filter.endDate;
    
    // Update page number and remove old data
    self.pageNo = 0;
    self.dataArray = nil;
    self.dataArray = [NSArray new];
    
    // Call service to get observation list
    [self callWebServiceToGetObservationList];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark NSNotificationCenter methods

//---------------------------------------------------------------

- (void) updateFromSaveSurvey {
    self.pageNo = 0;
    [self callWebServiceToGetObservationList];
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark EKEvent & EKCalendar methods (Device calendar)

//---------------------------------------------------------------

- (void) requestAccessToEvents {
    [self.eventManager.eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (error == nil) {
            // Store the returned granted value.
            self.eventManager.eventsAccessGranted = granted;
        }
        else{
            // In case of error, just log its description to the debugger.
            DLog(@"%@", [error localizedDescription]);
        }
    }];
}

//---------------------------------------------------------------

- (void) storeEventsToLocalCalendar {
    // Check that user allowed to access calendar
    if (self.eventManager.eventsAccessGranted) {
        // Remove all events first
        [self.eventManager removeAllEvents:self.eventList.eventList withBlock:^(BOOL isFinish) {
            // Start synchronization of events.
            [self.eventManager saveAllEvents:self.eventList.eventList withBlock:^(BOOL isFinish) {
            }];
        }];
    }
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:nil];
    self.navigationItem.backBarButtonItem = backItem;
    
    // Table Settings
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 180.0;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.tableView.mj_footer endRefreshing];
            self.pageNo = self.pageNo + self.pageSize;
            [self callWebServiceToGetObservationList];
        });
    }];

    // Pagination settings
    self.pageNo = 0;
    self.pageSize = 20;
    self.isFirstLoad = YES;
    self.isCellExpand = NO;
    _selectedIndexPath = [NSIndexPath indexPathForRow:-1 inSection:-1];

    _noRecordsView = [Helper createNoAccessView:self.tableView withMessage:[App_Delegate.keyMapping getKeyValue:@"KEY_OB_NO_SURVEY"]];
    _eventManager = [[EventManager alloc] init];
    // Request access to events.
    [self performSelector:@selector(requestAccessToEvents) withObject:nil afterDelay:0.4];
    
    // Get filters from stored
    _filter = [Filters new];
    
    // surveyEntityType = "0" | "1" : "Observation" | "Counter Coaching"
    
    if (self.surveyType == OBSERVATION) {
        self.filter.surveyEntityType = @"0"; // This parameter will be used in get observation list service
        self.filter.surveyEntityTypeString = KEY_PARAM_CONST_OBSERVATION; // This will be used in get categories list service
        self.title = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_DASHBOARD_TITLE"];
        // Check user permission to take observation
        if (!App_Delegate.userPermissions.canAccessTakeObservation.boolValue) {
            [self.navigationItem setRightBarButtonItem:nil animated:YES];
        }
    } else {
        self.filter.surveyEntityType = @"1";
        self.filter.surveyEntityTypeString = KEY_PARAM_CONST_COACHING;
        self.title = [App_Delegate.keyMapping getKeyValue:@"KEY_CC_DASHBOARD_TITLE"];
        // Check user permission to take observation
        if (!App_Delegate.userPermissions.canAccessTakeCounterCoaching.boolValue) {
            [self.navigationItem setRightBarButtonItem:nil animated:YES];
        }
    }
    
    // Create PULL-TO-REFRESH CONTROL
    [self createPullToRefresh];
    
    // Create Status array
    self.statusArray = @[
                            SURVEY_STATUS_ALL,
                            SURVEY_STATUS_COMPLETED,
                            SURVEY_STATUS_PENDING,
                            SURVEY_STATUS_CANCELLED
                         ];
    self.filter.surveyStatus = SURVEY_STATUS_ALL;
    [Helper addBottomLine:self.filterView withColor:TABLE_SEPERATOR_COLOR];

    // Date settings
    [self dateSettings];
    
    // Background service call
    [self callFilterWebServicesInBackground];
    
    // Add notification observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFromSaveSurvey) name:kUpdateObservationDashboard object:nil];
}

//---------------------------------------------------------------
// In a Storyboard-based application, you will often want to do a little preparation before navigation
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showObserveAgent"]) {
        // Go to Observation Storyboard
        SurveyAgentController *controller = (SurveyAgentController *)[segue destinationViewController];
        controller.eventType = OB_NEW_EVENT;
        controller.surveyType = self.surveyType;
    }
    
    // Date filter
    if ([segue.identifier isEqualToString:@"showSurveyDateFilter"]) {
        SurveyDateFilter *controller = (SurveyDateFilter *)[segue destinationViewController];
        controller.isShowCompareDate = NO;
        controller.delegate = self;
        controller.removeDateLimit = YES;
    }
}

//---------------------------------------------------------------

@end
