//
//  SurveyTabController.m
//  IN-Gauge
//
//  Created by Mehul Patel on 12/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "SurveyTabController.h"

@interface SurveyTabController ()

@end

@implementation SurveyTabController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
