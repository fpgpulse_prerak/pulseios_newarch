//
//  MenuCell.h
//  pulse
//
//  Created by Mehul Patel on 19/01/16.
//  Copyright © 2016 frontline performance group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "Menu.h"

@interface MenuCell : UITableViewCell

@property (strong, nonatomic) UILabel       *titleLabel;
@property (strong, nonatomic) UILabel       *badgeLabel;
@property (strong, nonatomic) UIImageView   *icon;
@property (strong, nonatomic) UIImageView   *arrow;
@property (strong, nonatomic) Menu          *menu;

@end
