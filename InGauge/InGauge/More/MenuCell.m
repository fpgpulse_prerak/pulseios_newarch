//
//  MenuCell.m
//  pulse
//
//  Created by Mehul Patel on 19/01/16.
//  Copyright © 2016 frontline performance group. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

//---------------------------------------------------------------

#pragma mark
#pragma mark Init methods

//---------------------------------------------------------------

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self setBackgroundColor:[UIColor whiteColor]];
    
    _titleLabel = [[UILabel alloc] init];
    [self.titleLabel setFont:[UIFont fontWithName:AVENIR_NEXT_FONT size:14]];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.opaque = NO;
    self.titleLabel.textColor = APP_COLOR;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];

    _badgeLabel = [[UILabel alloc] init];
    [self.badgeLabel setFont:[UIFont fontWithName:AVENIR_NEXT_FONT size:10]];
    self.badgeLabel.backgroundColor = APP_COLOR;
    self.badgeLabel.textColor = [UIColor whiteColor];
    self.badgeLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.badgeLabel];
    [self.badgeLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.badgeLabel.hidden = YES;
    self.badgeLabel.layer.masksToBounds = YES;
    self.badgeLabel.layer.cornerRadius = 10;
    
    _icon = [[UIImageView alloc] init];
    [self.icon setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.icon.tintColor = APP_COLOR;
    [self.contentView addSubview:self.icon];

    _arrow = [[UIImageView alloc] init];
    [self.arrow setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.arrow.tintColor = [UIColor blackColor];
    self.arrow.alpha = 0.8;
    [self.contentView addSubview:self.arrow];
    
//    [self.titleLabel setBackgroundColor:[UIColor redColor]];
//    [self.icon setBackgroundColor:[UIColor orangeColor]];
//    [self.arrow setBackgroundColor:[UIColor yellowColor]];
//    [self.arrow setBackgroundColor:[UIColor yellowColor]];
    
//    self.badgeLabel.text = @"1";
    // Constraints
    NSDictionary *views = @{@"titleLabel" : self.titleLabel, @"icon" : self.icon, @"arrow" : self.arrow, @"badgeLabel" : self.badgeLabel};
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-17-[icon(20)]-10-[titleLabel]-5-[badgeLabel(25)]" options:0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[titleLabel]|" options:0 metrics:nil views:views]];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[icon(20)]" options:0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[badgeLabel(20)]" options:0 metrics:nil views:views]];

    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[arrow(12)]-15-|" options:0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[arrow(12)]" options:0 metrics:nil views:views]];

    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.icon
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.arrow
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.badgeLabel
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1 constant:0]];
    return self;
}

//---------------------------------------------------------------

- (void) layoutSubviews {
    [super layoutSubviews];
    float indentPoints = self.indentationLevel * self.indentationWidth;
    self.contentView.frame = CGRectMake(indentPoints, self.contentView.frame.origin.y, self.contentView.frame.size.width - indentPoints, self.contentView.frame.size.height);
}

//---------------------------------------------------------------
//
//- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
//    [super setHighlighted:highlighted animated:animated];
//    if (self.highlighted) {
//        self.transform = CGAffineTransformMakeScale(0.8, 0.8);
//        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
//            self.transform = CGAffineTransformIdentity;
//        } completion:^(BOOL finished){
//        }];
//    }
//}

//---------------------------------------------------------------

@end
