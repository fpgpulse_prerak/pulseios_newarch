//
//  MoreViewController.m
//  InGauge
//
//  Created by Mehul Patel on 17/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "MoreViewController.h"
#import "AppDelegate.h"
#import "SurveyAgentController.h"
#import "SurveyListController.h"
#import "MenuCell.h"
#import "MailViewController.h"
#import "UserMailListModel+API.h"
#import "AnalyticsLogger.h"

@interface MoreViewController ()

@property (nonatomic, strong) NSArray               *menuArray;
@property (nonatomic, strong) NSArray               *dataArray;
@property (nonatomic, strong) NSMutableArray        *dynamicDataArray;
@property (nonatomic, strong) NSNumber              *mailCount;
@property (nonatomic, strong) NSIndexPath           *selectedIndexPath;
@property (nonatomic) NSInteger                     menuCount;

@end

@implementation MoreViewController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc {
    // Remove delegates, mutable array and observers here!
    _dynamicDataArray = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kMenuUpdateMailCountNotification object:nil];    
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) changeIndustryTenantButtonTapped:(id)sender {
    [self changeTenantAndIndustry];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WEBSERVICE methods

//---------------------------------------------------------------

- (void) callWebServiceToGetUnreadCount {
    [UserMailListModel getUnreadMailsCount:nil whenFinish:^(NSError *localizedError, NSNumber *count) {
        // update mail count
        self.mailCount = count;
        [self.tableView reloadData];
    }];
}

//---------------------------------------------------------------

- (void) callWebSerivceToGetMenus {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ServiceManager getAvailableMenu:^(NSArray *menus) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        self.menuArray = menus;
        
        // Add static menus
        // My Profile
        Menu *profileMenu = [Menu new];
        profileMenu.url = MENU_MY_PROFILE;
        profileMenu.title = [App_Delegate.keyMapping getKeyValue:@"KEY_MY_PROFILE"];

        // Logout
        Menu *logoutMenu = [Menu new];
        logoutMenu.url = MENU_LOGOUT;
        logoutMenu.title = @"Logout";
        
        NSMutableArray *newArray = [NSMutableArray arrayWithArray:self.menuArray];
        [newArray addObject:profileMenu];
//        [newArray addObject:prefMenu];
        [newArray addObject:logoutMenu];
        
        self.menuArray = [NSArray arrayWithArray:newArray];
        newArray = nil;
        [self createMenuListFile];
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) createMenuListFile {
    @try {
        NSMutableArray *tempArray = [NSMutableArray new];
        for (Menu *menu in self.menuArray) {
            [tempArray addObject:[self createMenuElementLevel:menu forMenuIndex:0]];
        }
        NSArray *menuArray = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
        
        self.dataArray = menuArray;
        _dynamicDataArray = [[NSMutableArray alloc] init] ;
        [self.dynamicDataArray addObjectsFromArray:self.dataArray];
        
        // Table data reload animation
        [UIView transitionWithView: self.tableView
                          duration: 0.35f
                           options: UIViewAnimationOptionTransitionCrossDissolve
                        animations: ^(void) {
                            [self.tableView reloadData];
                        } completion: nil];
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (NSDictionary *) createMenuElementLevel:(Menu *)menu forMenuIndex:(NSInteger)index {
    @try {
        // Now it's time to spin your brain!
        NSMutableDictionary *menuDetails = [NSMutableDictionary new];
        [menuDetails setObject:menu.title forKey:@"name"];
        [menuDetails setObject:[NSNumber numberWithInteger:index] forKey:@"level"];
        [menuDetails setObject:menu forKey:@"menu"];
        
        // Break the iteration
        if (!menu.subMenus || menu.subMenus.count == 0) {
            NSDictionary *details = [NSDictionary dictionaryWithDictionary:menuDetails];
            menuDetails = nil;
            return details;
        }
        
        NSMutableArray *childArray = [NSMutableArray new];
        for (Menu *childMenu in menu.subMenus) {
            if (childMenu.subMenus && childMenu.subMenus.count > 0) {
                NSDictionary *childDetails = [self createMenuElementLevel:childMenu forMenuIndex:index + 1];
                [childArray addObject:childDetails];
            } else {
                if (childMenu.title) {
                    NSDictionary *item = @{@"menu" : childMenu, @"name" : childMenu.title, @"level" : [NSNumber numberWithInteger:index + 1]};
                    [childArray addObject:item];
                }
            }
        }
        
        NSArray *childs = [NSArray arrayWithArray:childArray];
        [menuDetails setObject:childs forKey:@"Objects"];
        NSDictionary *details = [NSDictionary dictionaryWithDictionary:menuDetails];
        
        childArray = nil;
        menuDetails = nil;
        return details;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return [NSDictionary new];
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (self.dynamicDataArray.count > 0) ? self.dynamicDataArray.count : 0;
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"MenuCell";
    
    MenuCell *cell = (MenuCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[MenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    
    cell.indentationWidth = 20;
    cell.titleLabel.text = [[self.dynamicDataArray objectAtIndex:indexPath.row] valueForKey:@"name"];
    [cell setIndentationLevel:[[[self.dynamicDataArray objectAtIndex:indexPath.row] valueForKey:@"level"] intValue]];
    
    Menu *menu = [[self.dynamicDataArray objectAtIndex:indexPath.row] valueForKey:@"menu"];
    cell.menu = menu;
    if (menu.subMenus && menu.subMenus.count > 0) {
        [cell.arrow setImage:[UIImage imageNamed:RIGHT_ARROW]];
    } else {
        [cell.arrow setImage:nil];
    }
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    
    // Pulse Mail
    if ([cell.menu.url isEqualToString:MENU_SELECT_MAIL] || [cell.menu.url isEqualToString:MENU_MAIL_INBOX]) {
        if (self.mailCount.intValue != 0) {
            cell.badgeLabel.text = [NSString stringWithFormat:@"%d", self.mailCount.intValue];
            cell.badgeLabel.hidden = NO;
        } else {
            cell.badgeLabel.hidden = YES;
        }
    } else {
        cell.badgeLabel.hidden = YES;
    }
    
    [self selectCell:cell];
    
    NSDictionary *d = [self.dynamicDataArray objectAtIndex:indexPath.row];
    if ([d valueForKey:@"Objects"]) {
        NSArray *ar = [d valueForKey:@"Objects"];
        BOOL isAlreadyInserted = NO;
        
        for (NSDictionary *dInner in ar) {
            NSInteger index = [self.dynamicDataArray indexOfObjectIdenticalTo:dInner];
            isAlreadyInserted = (index > 0 && index != NSIntegerMax);
            if (isAlreadyInserted) {
                break;
            }
        }
        [cell.arrow setImage:(isAlreadyInserted) ? [UIImage imageNamed:DOWN_ARROW] : [UIImage imageNamed:RIGHT_ARROW]];
    }
    return cell;
}

//---------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    MenuCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    NSDictionary *d = [self.dynamicDataArray objectAtIndex:indexPath.row];
    if ([d valueForKey:@"Objects"]) {
        NSArray *ar = [d valueForKey:@"Objects"];
        BOOL isAlreadyInserted = NO;
        
        for (NSDictionary *dInner in ar) {
            NSInteger index = [self.dynamicDataArray indexOfObjectIdenticalTo:dInner];
            isAlreadyInserted = (index > 0 && index != NSIntegerMax);
            if (isAlreadyInserted) {
                break;
            }
        }
        
        if (isAlreadyInserted) {
            MenuCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            
            if (cell.menu.subMenus && cell.menu.subMenus.count > 0) {
                [cell.arrow setImage:[UIImage imageNamed:RIGHT_ARROW]];
            } else {
                [cell.arrow setImage:nil];
            }
            
            [self miniMizeFirstsRows:ar];
        } else {
            
            MenuCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            
            if (cell.menu.subMenus && cell.menu.subMenus.count > 0) {
                [cell.arrow setImage:[UIImage imageNamed:DOWN_ARROW]];
            } else {
                [cell.arrow setImage:nil];
            }
            NSUInteger count = indexPath.row + 1;
            NSMutableArray *arCells = [NSMutableArray array];
            for (NSDictionary *dInner in ar) {
                [arCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
                [self.dynamicDataArray insertObject:dInner atIndex:count++];
            }
            NSArray *newCells = [NSArray arrayWithArray:arCells];
            arCells = nil;
            [self.tableView insertRowsAtIndexPaths:newCells withRowAnimation:UITableViewRowAnimationLeft];
        }
    }
    // Open menu url
    [self openViewControllerForMenu:cell.menu];
}

//---------------------------------------------------------------

- (void) openViewControllerForMenu:(Menu *)menu {
    // Goal Progress
    if ([menu.url isEqualToString:MENU_SELECT_GOAL_PROGRESS]) {
        [self performSegueWithIdentifier:@"showGoalProgress" sender:nil];
    }
    
    //Coaching - Take Coaching
    if ([menu.url isEqualToString:MENU_TAKE_COACHING]) {
        // Go to Observation Storyboard
        UIStoryboard *observationStoryboard = [UIStoryboard storyboardWithName:KEY_OBSERVATION_STORYBOARD_NAME bundle:[NSBundle mainBundle]];
        SurveyAgentController *controller = [observationStoryboard instantiateViewControllerWithIdentifier:@"SurveyAgentController"];
        controller.eventType = OB_NEW_EVENT;
        controller.surveyType = COACHING;
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    //Coaching - Dashboard
    if ([menu.url isEqualToString:MENU_COACHING_DASHBOARD]) {
        UIStoryboard *observationStoryboard = [UIStoryboard storyboardWithName:KEY_OBSERVATION_STORYBOARD_NAME bundle:[NSBundle mainBundle]];
        SurveyListController *controller = [observationStoryboard instantiateViewControllerWithIdentifier:@"SurveyListController"];
        controller.surveyType = COACHING;
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    //Observation - Observe Agent
    else if ([menu.url isEqualToString:MENU_TAKE_OBSERVATION]) {
        // Go to Observation Storyboard
        UIStoryboard *observationStoryboard = [UIStoryboard storyboardWithName:KEY_OBSERVATION_STORYBOARD_NAME bundle:[NSBundle mainBundle]];
        SurveyAgentController *controller = [observationStoryboard instantiateViewControllerWithIdentifier:@"SurveyAgentController"];
        controller.eventType = OB_NEW_EVENT;
        controller.surveyType = OBSERVATION;
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    //Observation - Dashboard
    else if ([menu.url isEqualToString:MENU_OBSERVATION_DASHBOARD]) {
        UIStoryboard *observationStoryboard = [UIStoryboard storyboardWithName:KEY_OBSERVATION_STORYBOARD_NAME bundle:[NSBundle mainBundle]];
        SurveyListController *controller = [observationStoryboard instantiateViewControllerWithIdentifier:@"SurveyListController"];
        controller.surveyType = OBSERVATION;
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    // Mail - Inbox
    else if ([menu.url isEqualToString:MENU_MAIL_INBOX]) {
        [self performSegueWithIdentifier:@"showPulseMailInbox" sender:nil];
    }
    
    // Mail - Sent
    else if ([menu.url isEqualToString:MENU_MAIL_SENT]) {
        [self performSegueWithIdentifier:@"showPulseMailSent" sender:nil];
    }
    
    // Mail - Trash
    else if ([menu.url isEqualToString:MENU_MAIL_TRASH]) {
        [self performSegueWithIdentifier:@"showPulseMailTrash" sender:nil];
    }
    
    // My Profile
    else if ([menu.url isEqualToString:MENU_MY_PROFILE]) {
        [self performSegueWithIdentifier:@"showMyProfile" sender:nil];
    }
    
    // Logout
    else if ([menu.url isEqualToString:MENU_LOGOUT]) {
        [self btnLogoutClicked];
    }
}

//---------------------------------------------------------------

- (void) miniMizeFirstsRows:(NSArray*)ar {
    for (NSDictionary *dInner in ar) {
        NSUInteger indexToRemove = [self.dynamicDataArray indexOfObjectIdenticalTo:dInner];
        
        NSArray *arInner = [dInner valueForKey:@"Objects"];
        if (arInner && [arInner count] > 0) {
            [self miniMizeFirstsRows:arInner];
        }
        
        if ([self.dynamicDataArray indexOfObjectIdenticalTo:dInner] != NSNotFound) {
            [self.dynamicDataArray removeObjectIdenticalTo:dInner];
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:
                                                    [NSIndexPath indexPathForRow:indexToRemove inSection:0]
                                                    ]
                                  withRowAnimation:UITableViewRowAnimationLeft];
        }
    }
}

//---------------------------------------------------------------

- (void) selectCell:(MenuCell *)cell {
    cell.icon.image = nil;
    
    // Goal Progress
    if ([cell.menu.url isEqualToString:MENU_SELECT_GOAL_PROGRESS]) {
        cell.icon.image = [UIImage imageNamed:@"goal"];
    }
    
    // Coaching Parent
    else if ([cell.menu.url isEqualToString:MENU_SELECT_COACHING]) {
        cell.icon.image = [UIImage imageNamed:@"observation"];
    }
    
    //Coaching - Take Coaching
    else if ([cell.menu.url isEqualToString:MENU_TAKE_COACHING]) {
        cell.icon.image = [UIImage imageNamed:@"take_ob"];
    }
    
    //Coaching - Dashboard
    else if ([cell.menu.url isEqualToString:MENU_COACHING_DASHBOARD]) {
        cell.icon.image = [UIImage imageNamed:@"ob_dashboard"];
    }
    
    // Observation Parent
    else if ([cell.menu.url isEqualToString:MENU_SELECT_OBSERVATION]) {
        cell.icon.image = [UIImage imageNamed:@"observation"];
    }
    
    //Observation - Observe Agent
    else if ([cell.menu.url isEqualToString:MENU_TAKE_OBSERVATION]) {
        cell.icon.image = [UIImage imageNamed:@"take_ob"];
    }
    
    //Observation - Dashboard
    else if ([cell.menu.url isEqualToString:MENU_OBSERVATION_DASHBOARD]) {
        cell.icon.image = [UIImage imageNamed:@"ob_dashboard"];
    }
    
    // Mail Parent
    else if ([cell.menu.url isEqualToString:MENU_SELECT_MAIL]) {
        cell.icon.image = [UIImage imageNamed:@"mail"];
    }
    
    // Mail - Inbox
    else if ([cell.menu.url isEqualToString:MENU_MAIL_INBOX]) {
        cell.icon.image = [UIImage imageNamed:@"inbox"];
    }
    
    // Mail - Sent
    else if ([cell.menu.url isEqualToString:MENU_MAIL_SENT]) {
        cell.icon.image = [UIImage imageNamed:@"send-button"];
    }
    
//     Mail - Trash
    else if ([cell.menu.url isEqualToString:MENU_MAIL_TRASH]) {
        cell.icon.image = [UIImage imageNamed:TRASH_IMAGE];
    }
    
    // My Profile
    else if ([cell.menu.url isEqualToString:MENU_MY_PROFILE]) {
        cell.icon.image = [UIImage imageNamed:@"profile"];
    }
    
    // Logout
    else if ([cell.menu.url isEqualToString:MENU_LOGOUT]) {
        cell.icon.image = [UIImage imageNamed:@"logout"];
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark PreferenceDelegate methods

//---------------------------------------------------------------

- (void) btnLogoutClicked {
    
    //KEY_LOGOUT_ALERT
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Logout"  message:[App_Delegate.keyMapping getKeyValue:@"KEY_LOGOUT_ALERT"]  preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_YES"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        //Analytics
        [AnalyticsLogger logUserSignOutEvent];
        
        // Call service to logout
        [ServiceManager callLogoutWebService:^(NSString *error, NSDictionary *respose) {
            [User signOut];
        }];
        [App_Delegate changeRootViewControllerTo:@"SignInTableViewController"];
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_NO"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Notification methods

//---------------------------------------------------------------

- (void) reloadMenus {
    [self callWebSerivceToGetMenus];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View LifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    
    self.mailCount = [NSNumber numberWithInt:0];
    self.menuCount = 0;
    
    self.title = [App_Delegate.keyMapping getKeyValue:@"KEY_MORE"]; //KEY_MORE
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    // Get menus
    [self callWebSerivceToGetMenus];
    
    // Add notification observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callWebServiceToGetUnreadCount) name:kMenuUpdateMailCountNotification object:nil];

    [self callWebServiceToGetUnreadCount];
}

//---------------------------------------------------------------

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
        
    if ([segue.identifier isEqualToString:@"showLoginView"]) {
        [User signOut];
    } else if ([segue.identifier isEqualToString:@"showPulseMailInbox"]) {
        MailViewController *controller = (MailViewController *)[segue destinationViewController];
        controller.mailType = KEY_MAIL_TYPE_INBOX;
    } else if ([segue.identifier isEqualToString:@"showPulseMailSent"]) {
        MailViewController *controller = (MailViewController *)[segue destinationViewController];
        controller.mailType = KEY_MAIL_TYPE_SENT;
    } else if ([segue.identifier isEqualToString:@"showPulseMailTrash"]) {
        MailViewController *controller = (MailViewController *)[segue destinationViewController];
        controller.mailType = KEY_MAIL_TYPE_TRASH;
    }
}

//---------------------------------------------------------------

@end
