//
//  FPChooseOptionViewController.m
//  pulse
//
//  Created by 韩凯 on 1/25/15.
//  Copyright (c) 2015 frontline performance group. All rights reserved.
//

#import "FPChooseOptionViewController.h"
#import "NSString+Contains.h"
#import "Constant.h"

@interface FPChooseOptionViewController () <UISearchBarDelegate> {
    UISearchBar             *optionSearchBar;
    NSString                *selectedTextOption; //记录用户选择的列表项对应的文本
    NSMutableDictionary     *optionsDictionary;  //所有可选项，键为选项的文本，值为选项实例
    NSArray                 *textOptions;        //所有可选项对应的文本列表，例如：酒店的名称
    NSMutableArray          *textSearchResults;  //根据关键字搜索后的结果文本列表
}

@property (nonatomic, strong) UIActivityIndicatorView       *indicatorView;

@end

@implementation FPChooseOptionViewController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management  methods

//---------------------------------------------------------------

- (void) dealloc {
    textSearchResults = nil;
    optionsDictionary = nil;
    optionSearchBar.delegate = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (void)cancel {
    if (self.navigationController != nil) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

//---------------------------------------------------------------

- (void)done {
    @try {
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(chooseOptionViewController:didSelectOption:)]) {
            id option = optionsDictionary[selectedTextOption];
            [self.delegate chooseOptionViewController:self didSelectOption:option];
            
            if (self.navigationController != nil) {
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) initialSetup {
    
    // This will scroll tableView to TOP
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    
    if (self.optionType == FPOptionTypeGroup) {
        self.title = NSLocalizedString(@"groups", nil);
    } else if (self.optionType == FPOptionTypeHotel) {
        self.title = NSLocalizedString(@"hotels", nil);
    } else if (self.optionType == FPOptionTypeReport) {
        self.title = NSLocalizedString(@"reports", nil);
    } else if (self.optionType == FPOptionTypeUser) {
        self.title = NSLocalizedString(@"users", nil);
    } else if (self.optionType == FPOptionTypeOther) {
        self.title = NSLocalizedString(@"options", nil);
    } else if (self.optionType == FPOptionTypeEmployee) {
        self.title = NSLocalizedString(@"Employee", nil);
    } else if (self.optionType == FPOptionTypeMonth) {
        self.title = NSLocalizedString(@"Month", nil);
    } else if (self.optionType == FPOptionTypeYear) {
        self.title = NSLocalizedString(@"Year", nil);
    } else {
        self.title = nil;
    }
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        self.tableView.separatorInset = UIEdgeInsetsZero;
    }
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    optionSearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    optionSearchBar.delegate = self;
    self.tableView.tableHeaderView = optionSearchBar;
    [optionSearchBar setReturnKeyType:UIReturnKeyDone];
    [optionSearchBar setSearchBarStyle:UISearchBarStyleMinimal];
    [optionSearchBar setKeyboardAppearance:UIKeyboardAppearanceDark];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setDefaultTextAttributes:@{
                                                                                                 NSFontAttributeName: [UIFont fontWithName:AVENIR_FONT size:12],}];

    [self reloadOptions];
}

//---------------------------------------------------------------

- (void) hideSearchBar:(BOOL)isHide {
    if (isHide) {
        optionSearchBar.hidden = YES;
        self.tableView.tableHeaderView = nil;
    } else {
        optionSearchBar.hidden = NO;
        self.tableView.tableHeaderView = optionSearchBar;
//        [self.tableView reloadData];
    }
}

//---------------------------------------------------------------

- (void) searchTextFromList:(NSString *)searchText {
    @try {
        if (isEmptyString(searchText)) {
            textSearchResults = [NSMutableArray arrayWithArray:textOptions];
        } else {
            searchText = [[searchText trim] lowercaseString];
            NSMutableArray *tempArray = [NSMutableArray array];
            for (NSString *optionText in textOptions) {
                if ([[[optionText trim] lowercaseString] myContainsString:searchText]) {
                    [tempArray addObject:optionText];
                }
            }
            textSearchResults = tempArray;
            tempArray = nil;
        }
        [self.tableView reloadData];
    }
    @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void) showLocationLoadingIndicator {
    if (self.indicatorView) {
        [self.indicatorView removeFromSuperview];
        self.indicatorView = nil;
    }
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.indicatorView.frame = CGRectMake(self.view.center.x - 24, 150, 24, 24);
    [self.indicatorView startAnimating];
    [self.view addSubview:self.indicatorView];
}

//---------------------------------------------------------------

- (void) hideLocationLoadingIndicator {
    [self.indicatorView stopAnimating];
    [self.indicatorView removeFromSuperview];
    _indicatorView = nil;
    [self reloadOptions];
}

//---------------------------------------------------------------

- (void) reloadOptions {
    @try {
        selectedTextOption = nil;
        optionSearchBar.text = nil;
        optionsDictionary = [NSMutableDictionary dictionary];
        textSearchResults = [NSMutableArray array];
        
        NSArray *options = [self.delegate availableOptionsForController:self];
        for (int i = 0; i < options.count; i++) {
            NSString *text = [self.delegate chooseOptionViewController:self textForOptionAtIndex:i];
            [optionsDictionary setValue:options[i] forKey:text];
            [textSearchResults addObject:text];
        }
        textOptions = [NSArray arrayWithArray:textSearchResults];
        [self.tableView reloadData];
    }
    @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableViewDataSource methods

//---------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [textSearchResults count];
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"OptionCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        cell.textLabel.font = [UIFont fontWithName:AVENIR_FONT size:12];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSString *text = textSearchResults[indexPath.row];
    cell.textLabel.text = text;
    
    if (isEmptyString(selectedTextOption)) {
        id option = optionsDictionary[text];
        BOOL selected = [self.delegate chooseOptionViewController:self shouldMarkOptionAsSelected:option];
        if (selected) {
            selectedTextOption = text;
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    } else if ([selectedTextOption isEqualToString:text]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    if (self.optionType == FPOptionCoachingType) {
        id option = optionsDictionary[text];
        BOOL isAlreadySelected = [self.delegate chooseOptionViewController:self shouldDisableOption:option];
        if (isAlreadySelected) {
            cell.userInteractionEnabled = NO;
            cell.textLabel.textColor = [UIColor lightGrayColor];
        } else {
            cell.userInteractionEnabled = YES;
            cell.textLabel.textColor = [UIColor blackColor];
        }
    }
    return cell;
}

//---------------------------------------------------------------

- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 0)];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableViewDelegate methods

//---------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    @try {
        // For coaching screen if option is selected then it should be not selected again!
        if (self.optionType == FPOptionCoachingType) {
            NSString * text = textSearchResults[indexPath.row];
            id option = optionsDictionary[text];
            BOOL isAlreadySelected = [self.delegate chooseOptionViewController:self shouldDisableOption:option];
            if (isAlreadySelected) {
                return;
            }
        }
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        selectedTextOption = textSearchResults[indexPath.row];
        [self.tableView reloadData];
        
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(chooseOptionViewController:didSelectOption:)]) {
            id option = optionsDictionary[selectedTextOption];
            [self.delegate chooseOptionViewController:self didSelectOption:option];
        }
    }
    @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UISearchBarDelegate methods

//---------------------------------------------------------------

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSMutableString *textToBe = [NSMutableString stringWithString:searchBar.text];
    [textToBe replaceCharactersInRange:range withString:text];
    if (textToBe.length > 60) {
        return NO;
    }
    return YES;
}

//---------------------------------------------------------------

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self searchTextFromList:searchText];
}

//---------------------------------------------------------------

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

//---------------------------------------------------------------

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    return YES;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View LifeCycle methods

//---------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"back", nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"done", nil) style:UIBarButtonItemStylePlain target:self action:@selector(done)];
    
    if (self.optionType == FPOptionTypeGroup) {
        self.title = NSLocalizedString(@"groups", nil);
    } else if (self.optionType == FPOptionTypeHotel) {
        self.title = NSLocalizedString(@"hotels", nil);
    } else if (self.optionType == FPOptionTypeReport) {
        self.title = NSLocalizedString(@"reports", nil);
    } else if (self.optionType == FPOptionTypeUser) {
        self.title = NSLocalizedString(@"users", nil);
    } else if (self.optionType == FPOptionTypeOther) {
        self.title = NSLocalizedString(@"options", nil);
    } else if (self.optionType == FPOptionTypeEmployee) {
        self.title = NSLocalizedString(@"Employee", nil);
    } else if (self.optionType == FPOptionTypeMonth) {
        self.title = NSLocalizedString(@"Month", nil);
    } else if (self.optionType == FPOptionTypeYear) {
        self.title = NSLocalizedString(@"Year", nil);
    } else {
        self.title = nil;
    }
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        self.tableView.separatorInset = UIEdgeInsetsZero;
    }
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    optionSearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    optionSearchBar.delegate = self;
    [optionSearchBar setReturnKeyType:UIReturnKeyDone];
    [optionSearchBar setSearchBarStyle:UISearchBarStyleMinimal];
    [optionSearchBar setKeyboardAppearance:UIKeyboardAppearanceDark];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setDefaultTextAttributes:@{                                                                                                 NSFontAttributeName: [UIFont fontWithName:AVENIR_FONT size:12],}];
    self.tableView.tableHeaderView = optionSearchBar;
}

//---------------------------------------------------------------

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self reloadOptions];
}

//---------------------------------------------------------------

@end
