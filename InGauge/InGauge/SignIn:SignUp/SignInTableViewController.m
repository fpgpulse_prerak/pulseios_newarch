//
//  SignInTableViewController.m
//  InGauge
//
//  Created by Mehul Patel on 24/04/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "SignInTableViewController.h"
#import "AppDelegate.h"
#import "ServiceManager.h"
#import "User.h"
#import "NSString+LangExt.h"

@interface SignInTableViewController () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UIButton           *signInButton;
@property (nonatomic, weak) IBOutlet UITextField        *userNameText;
@property (nonatomic, weak) IBOutlet UITextField        *passwordText;

@end

@implementation SignInTableViewController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    self.userNameText.delegate = nil;
    self.passwordText.delegate = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) signInButtonTapped:(UIButton *)sender {
    
    // Animation
    [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];

    // Resign all responder
    [self.userNameText resignFirstResponder];
    [self.passwordText resignFirstResponder];

    // Validation
    NSString *userNameString = [Helper removeWhiteSpaceAndNewLineCharacter:self.userNameText.text];
    NSString *passwordString = [Helper removeWhiteSpaceAndNewLineCharacter:self.passwordText.text];
    
    if (userNameString.length == 0) {
        [self.userNameText becomeFirstResponder];
        [Helper showAlertWithTitle:@"" message:@"Please enter your email!" forController:self];
        return;
    }
    
    if (passwordString.length == 0) {
        [self.passwordText becomeFirstResponder];
        [Helper showAlertWithTitle:@"" message:@"Please enter your password!" forController:self];
        return;
    }
    
    if (![Helper validateEmail:userNameString]) {
        [self.userNameText becomeFirstResponder];
        [Helper showAlertWithTitle:@"" message:@"Invalid email address!" forController:self];
        return;
    }
    
    // Web Service call
    [self callWebServiceForUserLogin];
//    [App_Delegate changeRootViewControllerTo:@"MainDashboard"];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WEBSERVICE methods

//---------------------------------------------------------------

- (void) callWebServiceForUserLogin {
    // Check for internet
    
    // Show loading indicator
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.label.text = @"Signing in...";

    __block User * user = [User new];
    user.email = [Helper removeWhiteSpaceAndNewLineCharacter:self.userNameText.text];
    // MD5 encrypt password
    user.password = [self.passwordText.text md5EncryptedStringWithEncoding:NSUTF8StringEncoding];
    
    NSString *urlString = @"oauth/token";
    ///oauth/token?client_id=restapp&client_secret=restapp&grant_type=password&username=<emailId>&password=<md5 password>
    // Access Service manager
//    [ServiceManager postWebServiceCall:urlString parameters:[user userParams] whenFinish:^(NSString *errorMessage, NSDictionary *response) {
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
//        [Helper showAlertWithTitle:@"" message:errorMessage forController:self];
//    }];
 
    [App_Delegate changeRootViewControllerTo:@"MainDashboard"];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

//---------------------------------------------------------------

#pragma mark
#pragma mark UITextField delegate methods

//---------------------------------------------------------------

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    
    self.userNameText.delegate = self;
    self.passwordText.delegate = self;
    
    self.userNameText.text = @"prerak@fpgpulse.com";
    self.passwordText.text = @"123456";
    
    // Button border
    self.signInButton.layer.cornerRadius = 6.0f;
    self.signInButton.layer.masksToBounds = YES;
    self.signInButton.backgroundColor = APP_RED_COLOR;
}

//---------------------------------------------------------------

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBarTintColor:APP_RED_COLOR];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{
       NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:AVENIR_FONT size:15]
       }
     ];
}

//---------------------------------------------------------------

@end
