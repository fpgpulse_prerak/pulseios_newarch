//
//  SignUpController.h
//  InGauge
//
//  Created by Mehul Patel on 10/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewController.h"

#define PHONE_MAX_LENGTH 15
#define PASS_MIN_LENGHT 8

@interface SignUpController : BaseTableViewController

@end
