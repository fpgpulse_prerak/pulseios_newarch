//
//  SignUpController.m
//  InGauge
//
//  Created by Mehul Patel on 10/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "SignUpController.h"
#import "NSString+LangExt.h"

@interface SignUpController () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField        *firstNameText;
@property (nonatomic, weak) IBOutlet UITextField        *lastNameText;
@property (nonatomic, weak) IBOutlet UITextField        *countryCodeText;
@property (nonatomic, weak) IBOutlet UITextField        *phoneText;
@property (nonatomic, weak) IBOutlet UITextField        *empIdText;
@property (nonatomic, weak) IBOutlet UITextField        *jobTitleText;
@property (nonatomic, weak) IBOutlet UITextField        *organizationText;
@property (nonatomic, weak) IBOutlet UITextField        *emailText;
@property (nonatomic, weak) IBOutlet UITextField        *passwordText;
@property (nonatomic, weak) IBOutlet UITextField        *confirmPassText;
@property (nonatomic, weak) IBOutlet UIButton           *createAccountButton;

@property (nonatomic, strong) UITextField               *selectedTextField;


@end

@implementation SignUpController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    self.firstNameText.delegate = nil;
    self.lastNameText.delegate = nil;
    self.countryCodeText.delegate = nil;
    self.phoneText.delegate = nil;
    self.empIdText.delegate = nil;
    self.jobTitleText.delegate = nil;
    self.organizationText.delegate = nil;
    self.emailText.delegate = nil;
    self.passwordText.delegate = nil;
    self.confirmPassText.delegate = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) backButtonTapped:(id)sender {
    [self.emailText resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
}

//---------------------------------------------------------------

- (IBAction) createAccountButtonTapped:(id)sender {
    
    [self.selectedTextField resignFirstResponder];
    
    // Animation
    [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];

    // *** 1. Empty field validation
    if (![self validateTextFieldsForEmptyString]) {
        return;
    }

    // *** 2. Email validation
    NSString *emailString = [Helper removeWhiteSpaceAndNewLineCharacter:self.emailText.text];
    if (![Helper validateEmail:emailString]) {
        [self.emailText becomeFirstResponder];
        [Helper showAlertWithTitle:@"" message:NSLocalizedStringWithFormat(@"signup.alert.invalidemail") forController:self];
        return;
    }
    
    // *** 3. Password validation - Minimum lenght must be 8 characters
    NSString *passwordString = self.passwordText.text;
    if (passwordString.length < PASS_MIN_LENGHT) {
        [Helper showAlertWithTitle:@"" message:NSLocalizedStringWithFormat(@"signup.alert.passoword.minlength") forController:self];
        return;
    }

    // *** 4. Password comparison
    NSString *confirmPassString = self.confirmPassText.text;
    if (![passwordString isEqualToString:confirmPassString]) {
        [Helper showAlertWithTitle:@"" message:NSLocalizedStringWithFormat(@"signup.alert.passwordnotmatch") forController:self];
        return;
    }
    
    // Register account to server
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (BOOL) validateTextFieldsForEmptyString {
    NSString *emailString = [Helper removeWhiteSpaceAndNewLineCharacter:self.emailText.text];
    NSString *passString = [Helper removeWhiteSpaceAndNewLineCharacter:self.passwordText.text];
    NSString *confirmPassString = [Helper removeWhiteSpaceAndNewLineCharacter:self.confirmPassText.text];
    NSString *firstNameString = [Helper removeWhiteSpaceAndNewLineCharacter:self.firstNameText.text];
    NSString *lastNameString = [Helper removeWhiteSpaceAndNewLineCharacter:self.lastNameText.text];
    NSString *phoneString = [Helper removeWhiteSpaceAndNewLineCharacter:self.phoneText.text];
    NSString *jobTitleString = [Helper removeWhiteSpaceAndNewLineCharacter:self.jobTitleText.text];
    NSString *empIdString = [Helper removeWhiteSpaceAndNewLineCharacter:self.empIdText.text];
    NSString *organizationString = [Helper removeWhiteSpaceAndNewLineCharacter:self.organizationText.text];
    
    NSString *message = nil;
    BOOL isValid = YES;
    // First Name
    if (firstNameString.length == 0) {
        message = NSLocalizedStringWithFormat(@"signup.firstname.required");
        isValid = NO;
    }
    // Last Name
    else if (lastNameString.length == 0) {
        message = NSLocalizedStringWithFormat(@"signup.lastname.required");
        isValid = NO;
    }
    // Email
    else if (emailString.length == 0) {
        message = NSLocalizedStringWithFormat(@"signup.email.required");
        isValid = NO;
    }
    // Job Title
    else if (jobTitleString.length == 0) {
        message = NSLocalizedStringWithFormat(@"signup.jobtitle.required");
        isValid = NO;
    }
    // Employee ID
    else if (empIdString.length == 0) {
        message = NSLocalizedStringWithFormat(@"signup.empid.required");
        isValid = NO;
    }
    // Organization
    else if (organizationString.length == 0) {
        message = NSLocalizedStringWithFormat(@"signup.organization.required");
        isValid = NO;
    }
    // Cell/Mobile
    else if (phoneString.length == 0) {
        message = NSLocalizedStringWithFormat(@"signup.phone.required");
        isValid = NO;
    }
    // Password
    else if (passString.length == 0) {
        message = NSLocalizedStringWithFormat(@"signup.password.required");
        isValid = NO;
    }
    // Confirm Password
    else if (confirmPassString.length == 0) {
        message = NSLocalizedStringWithFormat(@"signup.repassword.required");
        isValid = NO;
    }
    if (message) {
        [Helper showAlertWithTitle:@"" message:message forController:self];
    }
    return isValid;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITextField delegate methods

//---------------------------------------------------------------

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    self.selectedTextField = textField;
    return YES;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    
    // Delegation
    self.firstNameText.delegate = self;
    self.lastNameText.delegate = self;
    self.countryCodeText.delegate = self;
    self.phoneText.delegate = self;
    self.empIdText.delegate = self;
    self.jobTitleText.delegate = self;
    self.organizationText.delegate = self;
    self.emailText.delegate = self;
    self.passwordText.delegate = self;
    self.confirmPassText.delegate = self;
    
    // Button border
    self.createAccountButton.layer.cornerRadius = 6.0f;
    self.createAccountButton.layer.masksToBounds = YES;
    self.createAccountButton.backgroundColor = APP_RED_COLOR;
}

//---------------------------------------------------------------

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
}

//---------------------------------------------------------------

@end

