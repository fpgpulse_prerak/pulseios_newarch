//
//  BaseTabBarController.h
//  IN-Gauge
//
//  Created by Mehul Patel on 12/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "ServiceManager.h"

@interface BaseTabBarController : UITabBarController

- (UIImage *)imageFromColor:(UIColor *)color;

@end
