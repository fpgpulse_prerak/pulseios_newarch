//
//  BaseTabBarController.m
//  IN-Gauge
//
//  Created by Mehul Patel on 12/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "BaseTabBarController.h"

@interface BaseTabBarController ()

@end

@implementation BaseTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    UITabBar *tabBar = [self tabBar];
    [tabBar setBackgroundImage:[self imageFromColor:APP_COLOR]];
    tabBar.tintColor = [UIColor whiteColor];
    tabBar.translucent = NO;
}

//---------------------------------------------------------------

- (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
