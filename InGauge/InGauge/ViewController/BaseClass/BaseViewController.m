//
//  BaseViewController.m
//  InGauge
//
//  Created by Mehul Patel on 08/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "BaseViewController.h"
#import "FilterController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------


//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) changeTenantAndIndustry {
    Industry *filterIndustry = nil;
    IndustryList *industryList = [ServicePreference customObjectWithKey:KEY_INDUSTRY_MODEL];
    if (industryList.industryList.count > 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.industryId.intValue == %d", App_Delegate.appfilter.industryId.intValue];
        NSArray *filter = [industryList.industryList filteredArrayUsingPredicate:predicate];
        if (filter.count > 0) {
            filterIndustry = [filter objectAtIndex:0];
            App_Delegate.appfilter.industryDescription = filterIndustry.industryDescription;
        }
    }
    
    TenantRoleModel *filterTenant = nil;
    TenantList *tenantList = [ServicePreference customObjectWithKey:KEY_DEFAULT_TENANT_ROLE_MODEL];
    if (tenantList.tenantList.count > 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.tenantId.intValue == %d", App_Delegate.appfilter.tenantId.intValue];
        NSArray *filter = [tenantList.tenantList filteredArrayUsingPredicate:predicate];
        if (filter.count > 0) {
            filterTenant = [filter objectAtIndex:0];
        }
    }
    
    // Industry
    RefineItem *industry = [RefineItem new];
    industry.itemId = filterIndustry.industryId;
    industry.title = [App_Delegate.keyMapping getKeyValue:@"KEY_INDUSTRY"];
    industry.displayName = filterIndustry.industryDescription;
    industry.subItems = industryList.industryList;
    industry.filterType = IndustryFilter;
    
    // Tenant
    RefineItem *tenant = [RefineItem new];
    tenant.itemId = filterTenant.tenantId;
    tenant.title = [App_Delegate.keyMapping getKeyValue:@"KEY_TENANT"];
    tenant.displayName = filterTenant.tenantName;
    tenant.subItems = tenantList.tenantList;
    tenant.filterType = TenantFilter;
    
    FilterController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterController"];
    controller.openFrom = OpenFromPreference;
    controller.filterItems = @[industry, tenant];
    [self presentViewController:controller animated:YES completion:^{
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
}

//---------------------------------------------------------------

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setBarTintColor:APP_COLOR];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:
        @{
            NSForegroundColorAttributeName:[UIColor whiteColor],
            NSFontAttributeName:[UIFont fontWithName:AVENIR_FONT size:14]
         }
     ];
}

//---------------------------------------------------------------

- (void) didReceiveMemoryWarning {
    // Remove all cache data - If you receive memory warnings
    [Helper removeCookiesCachesFromDevice];
}

//---------------------------------------------------------------

@end
