//
//  AddColumnsController.h
//  IN-Gauge
//
//  Created by Mehul Patel on 18/09/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "DataManager.h"

@protocol AddColumnsDelegate <NSObject>

- (void) selectedItems:(NSArray *)items;

@end

@interface AddColumnsController : BaseViewController

@property (nonatomic, weak) id <AddColumnsDelegate>   delegate;

@property (nonatomic, strong) NSArray                   *columnList;

@property (nonatomic, assign) FilterType                itemId;
@property (nonatomic, strong) Filters                   *filter;
@property (nonatomic, strong) DataManager               *dataManager;
@property (nonatomic, strong) NSArray                   *multiSelectionArray;

@end
