//
//  AddColumnsController.m
//  IN-Gauge
//
//  Created by Mehul Patel on 18/09/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "AddColumnsController.h"
#import "FilterItemCell.h"

@interface AddColumnsController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (nonatomic, weak) IBOutlet UISearchBar        *searchBar;
@property (nonatomic, weak) IBOutlet UITableView        *tableView;

@property (nonatomic, strong) UIView                    *noRecordsView;
@property (nonatomic, strong) NSArray                   *dataArray;
@property (nonatomic, strong) NSArray                   *searchArray;

@end

@implementation AddColumnsController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

//- (IBAction) backButtonTapped:(id)sender {
//    [self.navigationController popViewControllerAnimated:YES];
//}

//---------------------------------------------------------------

- (IBAction) doneButtonTapped:(id)sender {
    if (self.multiSelectionArray.count == 0) {
        // Show alert to select atleast one column
        [Helper showAlertWithTitle:@"Required!" message:@"You need to select atleast one column" forController:self];
        return;
    }
        
    [self.delegate selectedItems:self.multiSelectionArray];
    [self.navigationController popViewControllerAnimated:YES];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (BOOL) isSelected:(NSString *)job {
    // Remove tag from datasource
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF = %@", job];
    NSArray *tags = [self.multiSelectionArray filteredArrayUsingPredicate:predicate];
    if (tags.count > 0) {
        return YES;
    } else
        return NO;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *searchText = [Helper removeWhiteSpaceAndNewLineCharacter:self.searchBar.text];
    if (searchText.length > 0) {
        return self.searchArray.count;
    } else {
        return self.dataArray.count;
    }
}

//---------------------------------------------------------------

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"FilterItemCell%d%d", (int)indexPath.section, (int)indexPath.row];
    
    FilterItemCell *cell = (FilterItemCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[FilterItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tick"]];

    NSArray *dataSource = self.dataArray;
    NSString *searchText = [Helper removeWhiteSpaceAndNewLineCharacter:self.searchBar.text];
    if (searchText.length > 0) {
        dataSource = self.searchArray;
    }
    
    switch (self.itemId) {
        case GreatJobFilter: {
            NSString *job = [dataSource objectAtIndex:indexPath.row];
            cell.itemLabel.text = [Helper removeWhiteSpaceAndNewLineCharacter:job];
            break;
        }
        default: {
            cell.itemLabel.text = [dataSource objectAtIndex:indexPath.row];
            break;
        }
    }
    cell.arrowImage.hidden = YES;
    cell.selectedItemLabel.text = @"";
    cell.selectedItemLabel.hidden = YES;
    
    // Show selected object
    NSString *object = [dataSource objectAtIndex:indexPath.row];
    if ([self isSelected:object]) {
        cell.accessoryView.hidden = NO;
    } else {
        cell.accessoryView.hidden = YES;
    }
    return cell;
}

//---------------------------------------------------------------

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    NSArray *dataSource = self.dataArray;
    NSString *searchText = [Helper removeWhiteSpaceAndNewLineCharacter:self.searchBar.text];
    if (searchText.length > 0) {
        dataSource = self.searchArray;
    }
    
    @try {
        id object = [dataSource objectAtIndex:indexPath.row];
        NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.multiSelectionArray];
        // Update selection
        if (!cell.accessoryView.hidden) {
            cell.accessoryView.hidden = YES;
            // Remove selected object
            [tempArray removeObject:object];
        } else {
            // Check condition User can only select 3 types
            if (self.multiSelectionArray.count >= 3) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    // Show alert for information
                    [Helper showAlertWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_OB_SELECT_TYPE_INFO"] message:[App_Delegate.keyMapping getKeyValue:@"KEY_OB_SELECT_TYPE_MSG"] forController:self];
                    [tableView deselectRowAtIndexPath:indexPath animated:YES];
                });
            } else {
                cell.accessoryView.hidden = NO;
                // Add selected object
                [tempArray addObject:object];
            }
        }
        
        self.multiSelectionArray = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UISearch delegate methods

//---------------------------------------------------------------

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    return YES;
}

//---------------------------------------------------------------

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return YES;
}

//---------------------------------------------------------------

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    @try {
        if (isEmptyString(searchText)) {
            self.searchArray = self.dataArray;
        } else {
            searchText = [[searchText trim] lowercaseString];
            NSMutableArray *tempArray = [NSMutableArray array];
            for (id option in self.dataArray) {
                
                NSString *optionText = @"";
                switch (self.itemId) {
                    case GreatJobFilter: {
//                        NSString *job = option;
                        optionText = option;
                        break;
                    }
                    default: {
                        optionText = option;
                        break;
                    }
                }
                
                if ([[optionText lowercaseString] myContainsString:searchText]) {
                    [tempArray addObject:option];
                }
            }
            self.searchArray = [NSArray arrayWithArray:tempArray];
            tempArray = nil;
        }
        [self.tableView reloadData];
    }
    @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

//---------------------------------------------------------------

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    return YES;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    self.title = @"Select columns";
    
    // Search Bar UI
    self.searchBar.delegate = self;
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setFont:[UIFont fontWithName:AVENIR_FONT size:12]];
    [self.searchBar setReturnKeyType:UIReturnKeyDone];
    [self.searchBar setKeyboardAppearance:UIKeyboardAppearanceDefault];
    
    // TableView settings
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.allowsMultipleSelection = YES;
    
    _noRecordsView = [Helper createNoAccessView:self.view withMessage:[App_Delegate.keyMapping getKeyValue:@"KEY_NO_DATA"]];
    
    switch (self.itemId) {
        case GreatJobFilter: self.dataArray = self.columnList; break;
        default:
            break;
    }
    
    if (self.dataArray.count > 0) {
        [self.noRecordsView setHidden:YES];
        [self.tableView setHidden:NO];
    } else {
        [self.noRecordsView setHidden:NO];
        [self.tableView setHidden:YES];
    }
}

//---------------------------------------------------------------

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

//---------------------------------------------------------------

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

//---------------------------------------------------------------

@end
