//
//  ChartCell.h
//  IN-Gauge
//
//  Created by Mehul Patel on 23/08/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"

@interface ChartCell : UICollectionViewCell

@property (nonatomic, strong) UIView            *cardView;
@property (nonatomic, strong) UIImageView       *imageView;
@property (nonatomic, strong) UILabel           *label;

@end
