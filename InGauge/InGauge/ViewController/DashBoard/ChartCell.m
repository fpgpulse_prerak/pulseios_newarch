//
//  ChartCell.m
//  IN-Gauge
//
//  Created by Mehul Patel on 23/08/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "ChartCell.h"

@implementation ChartCell

//---------------------------------------------------------------

#pragma mark
#pragma mark Init methods

//---------------------------------------------------------------

- (id) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    self.backgroundColor = [UIColor whiteColor];
    
    _cardView = [[UIView alloc] init];
    self.cardView.translatesAutoresizingMaskIntoConstraints = NO;
    self.cardView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.cardView];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cardView]-1-|" options: 0 metrics:nil views:@{@"cardView" : self.cardView}]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cardView]-1-|" options: 0 metrics:nil views:@{@"cardView" : self.cardView}]];
    
    _imageView = [[UIImageView alloc] init];
    self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.cardView addSubview:self.imageView];
    
    UIColor *graphiteColor = [UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1];
    _label = [[UILabel alloc] init];
    self.label.translatesAutoresizingMaskIntoConstraints = NO;
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.font = [UIFont fontWithName:AVENIR_NEXT_DEMI_BOLD_FONT size:12];
    self.label.textColor = graphiteColor;
    self.label.numberOfLines = 0;
    [self.cardView addSubview:self.label];
    
    NSDictionary *views = @{@"imageView" : self.imageView, @"label" : self.label};
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[imageView(50)]" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[label]-15-|" options: 0 metrics:nil views:views]];

    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-40-[imageView(50)]-20-[label]-30-|" options: 0 metrics:nil views:views]];

    [self.cardView addConstraint:[NSLayoutConstraint constraintWithItem:self.imageView
                                                                 attribute:NSLayoutAttributeCenterX
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.cardView
                                                                 attribute:NSLayoutAttributeCenterX
                                                                multiplier:1
                                                                  constant:0]];
    return self;
}

//---------------------------------------------------------------

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

//---------------------------------------------------------------

@end
