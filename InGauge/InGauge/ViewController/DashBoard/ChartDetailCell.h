//
//  ChartDetailCell.h
//  IN-Gauge
//
//  Created by Mehul Patel on 24/08/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LIRButtonView.h"

@import WebKit;

@interface ChartDetailCell : UITableViewCell

@property (nonatomic, strong) WKWebView                     *chartControl;
@property (nonatomic, strong) NSLayoutConstraint            *webViewHeightConstraint;
@property (nonatomic, strong) UIActivityIndicatorView       *activity;

@end
