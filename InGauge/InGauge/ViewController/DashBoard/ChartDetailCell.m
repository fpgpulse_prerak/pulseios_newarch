//
//  ChartDetailCell.m
//  IN-Gauge
//
//  Created by Mehul Patel on 24/08/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "ChartDetailCell.h"

@implementation ChartDetailCell

//---------------------------------------------------------------

#pragma mark
#pragma mark Init methods

//---------------------------------------------------------------

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    [self setupLayout];
    return self;
}

//---------------------------------------------------------------

- (void) setupLayout {
    self.backgroundColor = [UIColor whiteColor];
    
    _chartControl = [[WKWebView alloc] init];
    self.chartControl.translatesAutoresizingMaskIntoConstraints = NO;
    [self.chartControl.scrollView setScrollEnabled:NO];
    [self.chartControl.scrollView setBounces:NO];
    [self.contentView addSubview:self.chartControl];
    self.chartControl.hidden = YES;
    
    _activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [self.activity setColor:[UIColor grayColor]];
    self.activity.translatesAutoresizingMaskIntoConstraints = NO;
    [self.activity setHidesWhenStopped:YES];
    [self.contentView addSubview:self.activity];
    [self.activity startAnimating];

    NSDictionary *views = @{@"webView" : self.chartControl};
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[webView]|" options: 0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[webView]|" options: 0 metrics:nil views:views]];
    
    // WebView height constraints = this will allow dynamic height for chart
    _webViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.chartControl
                                                            attribute:NSLayoutAttributeHeight
                                                            relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                               toItem:nil
                                                            attribute:NSLayoutAttributeNotAnAttribute
                                                           multiplier:1.0
                                                             constant:200];
    self.webViewHeightConstraint.priority = 900;
    [self.contentView addConstraint:self.webViewHeightConstraint];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.activity
                                                              attribute:NSLayoutAttributeCenterX
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.contentView
                                                              attribute:NSLayoutAttributeCenterX
                                                             multiplier:1.0
                                                               constant:0]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.activity
                                                              attribute:NSLayoutAttributeCenterY
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.contentView
                                                              attribute:NSLayoutAttributeCenterY
                                                             multiplier:1.0
                                                               constant:0]];
}

//---------------------------------------------------------------

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.contentView layoutIfNeeded];
}

//---------------------------------------------------------------

@end
