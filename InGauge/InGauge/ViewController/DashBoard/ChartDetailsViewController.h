//
//  ChartDetailsViewController.h
//  IN-Gauge
//
//  Created by Mehul Patel on 24/08/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewController.h"

@interface ChartDetailsViewController : BaseTableViewController

@property (nonatomic, strong) Filters           *filter;
@property (nonatomic, strong) CustomReport      *report;
@property (nonatomic, strong) NSArray           *locationArray;
@property (nonatomic, strong) NSArray           *userArray;
@property (nonatomic, strong) NSString          *dashboardName;

@end
