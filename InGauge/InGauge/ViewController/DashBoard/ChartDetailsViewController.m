//
//  ChartDetailsViewController.m
//  IN-Gauge
//
//  Created by Mehul Patel on 24/08/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "ChartDetailsViewController.h"
#import "ChartDetailCell.h"
#import "LIRButtonView.h"
//#import "SharedPoppupViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "UIView+ImageSnapshot.h"
#import "UIPrintPageRenderer+Print.h"
#import "LGRefreshView.h"
#import "AnalyticsLogger.h"
#import "ShareViewController.h"

#define kPaperSizeLetter CGSizeMake(612,792)
#define kPaperSizeA4 CGSizeMake(595.2, 800)


@interface ChartDetailsViewController () <WKNavigationDelegate, UIWebViewDelegate>

@property (nonatomic, weak) IBOutlet UIView                     *bottomView;
@property (nonatomic, strong) LIRButtonView                     *shareButton;
@property (nonatomic, strong) LIRButtonView                     *likeButton;
@property (nonatomic, strong) LIRButtonView                     *commentButton;
@property (nonatomic, strong) UIWebView                         *tempWebView;
@property (nonatomic, strong) LGRefreshView                     *refreshView;
@property (nonatomic, strong) MBProgressHUD                     *hud;
@property (nonatomic, assign) BOOL                              isReloadChart;

@end

@implementation ChartDetailsViewController

//---------------------------------------------------------------

#pragma mark -
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
    self.tempWebView.delegate = nil;
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark Action methods

//---------------------------------------------------------------

- (void) likeButtonTapped:(UIButton *)sender {
    [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];
    ShareViewController *controller = [self getShareController];
    controller.shareReportFor = ShareReportForLike;
    [self.navigationController pushViewController:controller animated:YES];
}

//---------------------------------------------------------------

- (void) commentButtonTapped:(UIButton *)sender {
    [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];
    ShareViewController *controller = [self getShareController];
    controller.shareReportFor = ShareReportForComment;
    [self.navigationController pushViewController:controller animated:YES];
}

//---------------------------------------------------------------

- (void) shareButtonTapped:(UIButton *)sender {
    [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];
    ShareViewController *controller = [self getShareController];
    controller.shareReportFor = ShareReportForShare;
    [self.navigationController pushViewController:controller animated:YES];
}

//---------------------------------------------------------------

- (void) actionButtonTapped:(id)sender {
    UIActivityViewController* activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[[self getChartImage]]  applicationActivities:nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.hud = [Helper showLoading:self.view];
        [self presentViewController:activityViewController animated:YES completion:^{
            [Helper hideLoading:self.hud];
        }];
    });
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) createBottomView {
    // Share button
    _shareButton = [[LIRButtonView alloc] init];
    self.shareButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.shareButton.button addTarget:self action:@selector(shareButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    self.shareButton.label.text = @"";
    self.shareButton.button.tintColor = LIGHT_BLUE_COLOR;
    [self.bottomView addSubview:self.shareButton];
   
    // Like button
    _likeButton = [[LIRButtonView alloc] init];
    self.likeButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.likeButton.label.text = @"";
    [self.likeButton.button addTarget:self action:@selector(likeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    self.likeButton.image.image = [UIImage imageNamed:LIKE_IMAGE];
    self.likeButton.image.tintColor = GREEN_COLOR;
    [self.bottomView addSubview:self.likeButton];
    
    // Share button
    _commentButton = [[LIRButtonView alloc] init];
    self.commentButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.commentButton.label.text = @"";
    self.commentButton.image.image = [UIImage imageNamed:COMMENT_IMAGE];
    self.commentButton.image.tintColor = SOCIAL_BUTTON_COLOR;
    [self.commentButton.button addTarget:self action:@selector(commentButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomView addSubview:self.commentButton];
    
    // Constraints
    NSDictionary *views = @{@"shareButton" : self.shareButton, @"likeButton" : self.likeButton, @"commentButton" : self.commentButton};
    
    [self.bottomView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[shareButton]|" options: 0 metrics:nil views:views]];
    [self.bottomView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[likeButton]|" options: 0 metrics:nil views:views]];
    [self.bottomView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[commentButton]|" options: 0 metrics:nil views:views]];
    
    [self.bottomView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[shareButton]" options: 0 metrics:nil views:views]];
    [self.bottomView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[likeButton]-10-[commentButton]-|" options: 0 metrics:nil views:views]];
}

//---------------------------------------------------------------

- (void) createPullToRefresh {
    __weak typeof(self) wself = self;
    _refreshView = [LGRefreshView refreshViewWithScrollView:self.tableView refreshHandler:^(LGRefreshView *refreshView) {
        if (wself) {
            __strong typeof(wself) self = wself;
            self.isReloadChart = YES;
            
            [self enableDisableSocialButtons:NO];
            [self.tableView reloadData];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void) {
                [self.refreshView endRefreshing];
            });
        }
    }];
    _refreshView.tintColor = APP_COLOR;
}

//---------------------------------------------------------------

- (NSString *) getTemporaryFolderPath {
    NSString *filePath = nil;
    @try {
        NSFileManager *fileManager = [NSFileManager new];
        NSError *error = nil;
        NSURL *docsurl = [fileManager URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:&error];
        if (!error) {
            NSURL *tempFolderURL = [docsurl URLByAppendingPathComponent:@"TempCharts"];
            BOOL isCreated = [fileManager createDirectoryAtURL:tempFolderURL withIntermediateDirectories:YES attributes:nil error:&error];
            if (isCreated) {
                // Directory created - return destination path
                filePath = tempFolderURL.path;
            }
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        filePath = nil;
    }
    return filePath;
}

//---------------------------------------------------------------

- (void) removeFilesFromTempFolder {
    @try {
        __block NSError *error;
        // Get document directory path
        NSString *tempFolderPath = [[Helper documentDirectory] stringByAppendingPathComponent:@"TempCharts"];
        
        // Get all files at temp folders
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSArray *files = [fileManager contentsOfDirectoryAtPath:tempFolderPath error:&error];
        
        // Iterate and remove files
        [files enumerateObjectsUsingBlock:^(NSString *fileName, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *filePath = [tempFolderPath stringByAppendingPathComponent:fileName];
            BOOL isRemoved = [fileManager removeItemAtPath:filePath error:&error];
            
            // Check if any file is not removed
            if (!isRemoved) {
                DLog(@"File: %@ [%@] not removed", fileName, filePath);
            }
        }];
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (UIImage *) getChartImage {
    ChartDetailCell *cell = (ChartDetailCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    UIImage *chartImage = [cell.chartControl.scrollView imageSnapshot:cell.chartControl.scrollView.frame];
    return chartImage;
}

//---------------------------------------------------------------

- (void) enableDisableSocialButtons:(BOOL)isEnable {
    self.shareButton.button.enabled = isEnable;
    self.likeButton.button.enabled = isEnable;
    self.commentButton.button.enabled = isEnable;
    self.navigationItem.rightBarButtonItem.enabled = isEnable;
}

//---------------------------------------------------------------

- (ShareViewController *) getShareController {
    ShareViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareViewController"];
    controller.report = self.report;
    controller.filter = self.filter;
    controller.chartImage = [self getChartImage];
    controller.dashboardName = self.dashboardName;
    return controller;
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        ChartDetailCell *cell = (ChartDetailCell *)[super tableView:tableView cellForRowAtIndexPath:indexPath];
        
        NSString *baseUrlString = [NSString stringWithFormat:@"%@/htmlReport", DASHBOARD_CHART_URL];
        NSString *urlString = [NSString stringWithFormat:@"%@/%d", baseUrlString, self.report.customReportId.intValue];
        NSDictionary *params = [self.filter serializeFiltersForChartURL];
        
        // If reportType is block then append `widgetType`
        //widgetType=column
        if ([self.report.reportType isEqualToString:KEY_BLOCK_CHART]) {
        NSMutableDictionary *tempDetails = [NSMutableDictionary dictionaryWithDictionary:params];
            [tempDetails setObject:@"column" forKey:@"widgetType"];
            params = [NSDictionary dictionaryWithDictionary:tempDetails];
            tempDetails = nil;
        }
        
        NSString *chartUrlString = [ServiceUtil addHeaderParametersWithOutBaseURL:urlString withParams:params];
        NSString* encodedUrl = [chartUrlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:encodedUrl]];
        DLog(@"\n\n\n\n *** encodedURL :%@", encodedUrl);
        cell.chartControl.navigationDelegate = self;
        if (self.isReloadChart) {
            [cell.activity startAnimating];
            [self enableDisableSocialButtons:NO];
            [cell.chartControl loadRequest:theRequest];
        }
        return cell;
    } else {
        UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
        return cell;
    }
}

//---------------------------------------------------------------

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    // NOTE: **DO NOT** remove This code, it is useful
    // Explanation: WKWebView has an issue while render long pages,
    // below code will render remaining page if its not fully reder in webView
    for (UITableViewCell *cell in self.tableView.visibleCells) {
        if ([cell isKindOfClass:[ChartDetailCell class]]) {
            ChartDetailCell *chartDetailsCell = (ChartDetailCell *)cell;
            [chartDetailsCell.chartControl setNeedsLayout];
        }
    }
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark WKNavigationDelegate methods

//---------------------------------------------------------------

- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    NSString *javascript = @"var meta = document.createElement('meta');meta.setAttribute('name', 'viewport');meta.setAttribute('content', 'width=device-width, initial-scale=0.0, maximum-scale=0.0, user-scalable=no');document.getElementsByTagName('head')[0].appendChild(meta);";
    [webView evaluateJavaScript:javascript completionHandler:nil];
}

//---------------------------------------------------------------

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    // Only make changes if current view controller is visible and active
    if (self.isViewLoaded && self.view.window) {
        // viewController is visible
        [webView evaluateJavaScript:@"document.getElementsByTagName('html')[0].offsetHeight;" completionHandler:^(id response, NSError * _Nullable error) {
            
            UIView *contentView = webView.superview;
            ChartDetailCell *cell = (ChartDetailCell *)contentView.superview;
            
            CGFloat height = [NSString stringWithFormat:@"%@", response].floatValue;
            [UIView animateWithDuration:0.5 animations:^{
                self.isReloadChart = NO;
                cell.chartControl.hidden = NO;
                [cell.activity stopAnimating];
                cell.webViewHeightConstraint.constant = height;
                [cell.chartControl layoutIfNeeded];
            } completion:^(BOOL finished) {
                [self enableDisableSocialButtons:YES];
                // reload table
                [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewAutomaticDimension];
            }];
        }];
    }
}

//---------------------------------------------------------------

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    DLog(@"WEBVIEW FAILED: %s. Error %@",__func__,error);
}

//---------------------------------------------------------------

- (void)webViewWebContentProcessDidTerminate:(WKWebView *)webView {
    DLog(@"WEBVIEW TERMINTATED:");
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark UIWebView delegate methods

//---------------------------------------------------------------

- (BOOL)webView:(UIWebView *)theWebView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    theWebView.scalesPageToFit = YES;
    return YES;
}

//---------------------------------------------------------------

- (void) webViewDidFinishLoad:(UIWebView *)webView {
    if (webView.isLoading)
        return;
    
    // Hide loading indicator    
    [Helper hideLoading:self.hud];
    
    UIPrintPageRenderer *render = [[UIPrintPageRenderer alloc] init];
    [render addPrintFormatter:webView.viewPrintFormatter startingAtPageAtIndex:0];

    float topPadding = 10.0f;
    float bottomPadding = 10.0f;
    float leftPadding = 10.0f;
    float rightPadding = 10.0f;
    CGRect printableRect = CGRectMake(leftPadding,
                                      topPadding,
                                      kPaperSizeA4.width - leftPadding - rightPadding,
                                      kPaperSizeA4.height - topPadding - bottomPadding);
    CGRect paperRect = CGRectMake(0, 0, kPaperSizeA4.width, kPaperSizeA4.height);
    
    [render setValue:[NSValue valueWithCGRect:paperRect] forKey:@"paperRect"];
    [render setValue:[NSValue valueWithCGRect:printableRect] forKey:@"printableRect"];
    
    NSData *pdfData = [render printToPDF];
    
    BOOL isFileCreated = YES;
    if (pdfData) {
        // Get temporary folder path
        NSString *filePath = [self getTemporaryFolderPath];
        NSString *pdfFilePath = [NSString stringWithFormat:@"%@/%@.pdf", filePath, self.report.reportName];
        
        // Check if filePath exist?
        if (filePath) {
            // Write webView data to temporary file
            [pdfData writeToFile:pdfFilePath atomically:YES];
            
            // Remove temproray webView
            [self.tempWebView removeFromSuperview];
            
            // Open Activity controller
            NSURL *fileUrl = [NSURL fileURLWithPath:pdfFilePath isDirectory:NO];
            NSArray *activityItems = @[fileUrl];
            UIActivityViewController* activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems  applicationActivities:nil];
            [self presentViewController:activityViewController animated:YES completion:^{}];
        } else {
            isFileCreated = NO;
        }
    } else {
        isFileCreated = NO;
        [self.tempWebView removeFromSuperview];
        DLog(@"PDF couldnot be created");
    }
    
    // if file is not created show alert
    [Helper showAlertWithTitle:@"Error" message:@"Sharing is not available right now, Reload page or Restart the application" forController:self];
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];    
    self.title = self.report.reportName;

    self.isReloadChart = YES;
    self.hud = [MBProgressHUD HUDForView:self.view];
    
    // Table settings
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 200;
    
    // Share Navigation button
    UIBarButtonItem *actionButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(actionButtonTapped:)];
    self.navigationItem.rightBarButtonItem = actionButton;
    self.navigationItem.rightBarButtonItem.enabled = NO;

    [self createBottomView];
    [self createPullToRefresh];
    
    // This is temporary webView it will not visible anywhere
    // Create webview - Temporary!
    _tempWebView = [[UIWebView alloc] init];
    self.tempWebView.delegate = self;
    self.tempWebView.hidden = YES;
}

//---------------------------------------------------------------

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.isReloadChart) {
        [self.tableView reloadData];
    }
}

//---------------------------------------------------------------

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    // Remove files from temporary folder - MUST DO THIS!!!!!
    [self removeFilesFromTempFolder];
}

//---------------------------------------------------------------

@end
