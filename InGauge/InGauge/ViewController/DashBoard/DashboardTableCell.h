//
//  DashboardTableCell.h
//  IN-Gauge
//
//  Created by Mehul Patel on 15/09/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "SocialButton.h"

@interface DashboardTableCell : UITableViewCell

@property (nonatomic, strong) UIView                *cardView;
@property (nonatomic, strong) UILabel               *titleLabel;
@property (nonatomic, strong) UILabel               *valueLabel;
@property (nonatomic, strong) UIStackView           *stackView;
@property (nonatomic, strong) UIView                *socialView;
@property (nonatomic, strong) SocialButton          *likeView;
@property (nonatomic, strong) SocialButton          *commentView;
@property (nonatomic, strong) NSIndexPath           *indexPath;
@property (nonatomic, assign) BOOL                  isAlreadyLike;

@end
