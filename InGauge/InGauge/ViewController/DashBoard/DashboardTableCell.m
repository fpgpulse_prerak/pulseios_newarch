//
//  DashboardTableCell.m
//  IN-Gauge
//
//  Created by Mehul Patel on 15/09/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "DashboardTableCell.h"

@implementation DashboardTableCell

//---------------------------------------------------------------

#pragma mark
#pragma mark Init methods

//---------------------------------------------------------------

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = BACK_COLOR;
    
    _titleLabel = [[UILabel alloc] init];
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.titleLabel.font = [UIFont fontWithName:AVENIR_NEXT_FONT size:12];
    self.titleLabel.numberOfLines = 0;
    
    _valueLabel = [[UILabel alloc] init];
    self.valueLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.valueLabel.textAlignment = NSTextAlignmentCenter;
    self.valueLabel.font = [UIFont fontWithName:AVENIR_NEXT_FONT size:12];
    self.valueLabel.numberOfLines = 0;
    
    // Social View
    _socialView = [[UIView alloc] init];
    self.socialView.translatesAutoresizingMaskIntoConstraints = NO;
    
    // Stack View
    _stackView = [[UIStackView alloc] init];
    self.stackView.translatesAutoresizingMaskIntoConstraints = NO;
    self.stackView.axis = UILayoutConstraintAxisHorizontal;
    self.stackView.distribution = UIStackViewDistributionEqualSpacing;
    self.stackView.alignment = UIStackViewAlignmentFill;
    self.stackView.spacing = 5;
    
    [self.stackView addArrangedSubview:self.titleLabel];
    [self.stackView addArrangedSubview:self.valueLabel];
    [self.stackView addArrangedSubview:self.socialView];
    [self.contentView addSubview:self.stackView];
    
//    self.titleLabel.backgroundColor = [UIColor redColor];
//    self.valueLabel.backgroundColor = [UIColor blueColor];
//    self.socialView.backgroundColor = [UIColor greenColor];

    // Layout for Stack View
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[stackView]|" options: 0 metrics:nil views:@{@"stackView" : self.stackView}]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[stackView]|" options: 0 metrics:nil views:@{@"stackView" : self.stackView}]];
        
    [self.stackView addConstraint:[NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:50]];
    
    // add subviews
    [self createSocialView];
    return self;
}

//---------------------------------------------------------------

- (void) createSocialView {
    // Like button
    UIFont *font = [UIFont fontWithName:AVENIR_FONT size:10];
    _likeView = [[SocialButton alloc] init];
    self.likeView.translatesAutoresizingMaskIntoConstraints = NO;
    self.likeView.label.text = @"100";
    self.likeView.label.font = font;
    self.likeView.image.image = [UIImage imageNamed:LIKE_IMAGE];
    self.likeView.image.tintColor = GREEN_COLOR;
    [self.socialView addSubview:self.likeView];
    
    // comment button
    _commentView = [[SocialButton alloc] init];
    self.commentView.translatesAutoresizingMaskIntoConstraints = NO;
    self.commentView.label.text = @"20";
    self.commentView.label.font = font;
    self.commentView.image.image = [UIImage imageNamed:COMMENT_IMAGE];
    self.commentView.image.tintColor = SOCIAL_BUTTON_COLOR;
    [self.socialView addSubview:self.commentView];
    
    // Constraints
    NSDictionary *views = @{ @"commentView" : self.commentView, @"likeView" : self.likeView};
    
    [self.socialView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[likeView]-[commentView]-|" options: 0 metrics:nil views:views]];
    [self.socialView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[commentView]|" options: 0 metrics:nil views:views]];
    [self.socialView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[likeView]|" options: 0 metrics:nil views:views]];
}

//---------------------------------------------------------------

@end
