//
//  DashboardTableController.h
//  IN-Gauge
//
//  Created by Mehul Patel on 15/09/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewController.h"

@interface DashboardTableController : UITableViewController

@property (nonatomic, strong) Filters           *filter;
@property (nonatomic, strong) CustomReport      *report;
@property (nonatomic, strong) NSString          *location;
@property (nonatomic, strong) NSString          *dashboardName;
@property (nonatomic, assign) BOOL              isMTD;

@end
