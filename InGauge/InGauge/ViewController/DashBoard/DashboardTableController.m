//
//  DashboardTableController.m
//  IN-Gauge
//
//  Created by Mehul Patel on 15/09/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "DashboardTableController.h"
#import "DashboardTableCell.h"
#import "AddColumnsController.h"
//#import "SharedPoppupViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "AnalyticsLogger.h"
#import "UIPrintPageRenderer+Print.h"
#import "LGRefreshView.h"
#import "CNPPopupController.h"
#import "SnapAlertView.h"
#import "LikeCommentCount.h"

@import WebKit;

#define LIKE_TAG                1000
#define COMMENT_TAG             5000
#define LIKE_IMAGE_TAG          3000
#define LIKE_LABEL_TAG          10000
#define COMMENT_LABEL_TAG       20000

#define kPaperSizeLetter CGSizeMake(612,792)
#define kPaperSizeA4 CGSizeMake(595.2, 800)

#define kHeaderHeight       50
#define kLabelTag           1000
#define kHeaderTag          2000

@interface DashboardTableController () <UITabBarDelegate, UITableViewDelegate, AddColumnsDelegate, WKNavigationDelegate, UIWebViewDelegate, SnapAlertDelegate, CNPPopupControllerDelegate>

@property (nonatomic, weak) IBOutlet UIView                     *headerView;
@property (nonatomic, strong) UILabel                           *header1;
@property (nonatomic, strong) UILabel                           *header2;
@property (nonatomic, strong) UILabel                           *header3;

@property (nonatomic, strong) CNPPopupController                *popupController;
@property (nonatomic, strong) SnapAlertView                     *alertView;
@property (nonatomic, strong) MBProgressHUD                     *hud;
@property (nonatomic, strong) TableDataDetails                  *tableDetails;
@property (nonatomic, strong) UIView                            *noRecordsView;
@property (nonatomic, strong) NSArray                           *multiSelection;
@property (nonatomic, strong) NSArray                           *columnList;
@property (nonatomic, strong) NSArray                           *dataArray;
@property (nonatomic, strong) UIWebView                         *tempWebView;
@property (nonatomic, strong) WKWebView                         *tableWebView;
@property (nonatomic, strong) LGRefreshView                     *refreshView;
@property (nonatomic, strong) NSLayoutConstraint                *webViewHeightConstraint;
@property (nonatomic, strong) NSMutableArray                    *arrayForBool;
@property (nonatomic, strong) NSString                          *commentString;
@property (nonatomic, strong) NSIndexPath                       *commentIndexPath;
@property (nonatomic, strong) NSArray                           *likeCommentArray;

@end

@implementation DashboardTableController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    self.tempWebView.delegate = nil;
    self.tableWebView.navigationDelegate = nil;
    [self.tempWebView removeFromSuperview];
    [self.tableWebView removeFromSuperview];
    self.arrayForBool = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (void) likeButtonTapped:(UIButton *)sender {
    SocialButton *socialButton = (SocialButton *)sender.superview;
    UIView *socialView = socialButton.superview;
    UIView *contentView = socialView.superview;
    DashboardTableCell *cell = (DashboardTableCell *)contentView.superview.superview;
    
    if (cell.isAlreadyLike) {
        // Stop service call - if alreay liked! --> Early exit!
        [Helper showAlertWithTitle:@"Like!" message:@"You have already Liked this KPI." forController:self];
        return;
    }
    
    NSDictionary *details = [self createServiceRequestForType:0 withIndexPath:cell.indexPath];
    [self callWebServiceToLikeCommentReport:details forIndexPath:cell.indexPath];
}

//---------------------------------------------------------------

- (void) commentButtonTapped:(UIButton *)sender {
    SocialButton *socialButton = (SocialButton *)sender.superview;
    UIView *socialView = socialButton.superview;
    UIView *contentView = socialView.superview;
    DashboardTableCell *cell = (DashboardTableCell *)contentView.superview.superview;
    self.commentIndexPath = cell.indexPath;
    [self showAlert:self];
}

////---------------------------------------------------------------
//
//- (void) backButtonTapped:(id)sender {
//    [self.navigationController popViewControllerAnimated:YES];
//}

//---------------------------------------------------------------

- (void) addMoreColumnsTapped:(id)sender {
    [self performSegueWithIdentifier:@"showAddMoreView" sender:self];
}

//---------------------------------------------------------------

- (void) shareButtonTapped:(id)sender {
    //UPDATE: 26-Sep-2017 ~ Now table will be not shared to feeds anymore
    // Create URL and Load it to webView
    NSString *baseUrlString = [NSString stringWithFormat:@"%@/htmlReport", DASHBOARD_CHART_URL];
    NSString *urlString = [NSString stringWithFormat:@"%@/%d", baseUrlString, self.report.customReportId.intValue];
    NSDictionary *params = [self.filter serializeFiltersForChartURL];
    
    NSString *chartUrlString = [ServiceUtil addHeaderParametersWithOutBaseURL:urlString withParams:params];
    NSString* encodedUrl = [chartUrlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:encodedUrl]];
    DLog(@"\n\n\n\n *** encodedURL :%@", encodedUrl);
    self.hud = [Helper showLoading:self.view];

    self.navigationItem.rightBarButtonItem.enabled = NO;
    [self.tableWebView loadRequest:theRequest];
}

//---------------------------------------------------------------

- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer {
    
    UIView *headerView = gestureRecognizer.view;
    NSInteger index = gestureRecognizer.view.tag;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:index];
    NSDictionary *tableData = [self.tableDetails.tableDatas objectAtIndex:indexPath.section];
    
    NSMutableArray *tempArray = self.dataArray.count > 0 ? [NSMutableArray arrayWithArray:self.dataArray] : [NSMutableArray new];
    NSMutableArray *objArray = [NSMutableArray new];
    for (NSString *column in self.columnList) {
        NSString *key = [self convertObjectToString:[self.tableDetails.tableHeaders objectForKey:column]];
        NSString *value = @"";
        if ([[tableData allKeys] containsObject:key]) {
            value = [tableData valueForKey:key];
        }

        NSDictionary *details = @{KEY_TITLE : column, KEY_VALUE : value};
        [objArray addObject:details];
    }
    
    // Replace or insert array
    if (self.dataArray.count <= index) {
        // add object
        [tempArray addObject:[NSArray arrayWithArray:objArray]];
    } else {
        // Update object
        [tempArray replaceObjectAtIndex:index withObject:[NSArray arrayWithArray:objArray]];
    }
    objArray = nil;
    
    self.dataArray = [NSArray arrayWithArray:tempArray];
    tempArray = nil;
    
    
    // Animation
    headerView.transform = CGAffineTransformMakeScale(0.8, 0.8);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        headerView.transform = CGAffineTransformIdentity;
    } completion:nil];
    
    if (indexPath.row == 0) {
        BOOL collapsed = [[self.arrayForBool objectAtIndex:indexPath.section] boolValue];
        for (int i = 0; i < [self.tableDetails.tableDatas count]; i++) {
            if (indexPath.section == i) {
                [self.arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!collapsed]];
            }
        }
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:index] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WEB SERVICE methods

//---------------------------------------------------------------

- (void) callWebServiceToGetTableData {
    
    // Show progress
    self.hud = [Helper showLoading:self.view];
    [ServiceManager callWebServiceToGetTableData:self.filter reportId:self.report.customReportId usingBlock:^(NSString *errorMessage, TableDataDetails *response, NSNumber *status) {
        [Helper hideLoading:self.hud];
        [self.navigationItem.rightBarButtonItems enumerateObjectsUsingBlock:^(UIBarButtonItem *barButton, NSUInteger idx, BOOL * _Nonnull stop) {
            barButton.enabled = YES;
        }];
        
        self.noRecordsView.hidden = YES;
        
        if (response.tableKeys.count > 0) {
            self.tableDetails = response;
            self.columnList = self.tableDetails.tableKeys.count > 0 ? self.tableDetails.tableKeys : [NSArray new];
            
            // Set expand/close status
            NSMutableArray *tempArray = [NSMutableArray new];
            for (int i = 0; i < [self.tableDetails.tableDatas count]; i++) {
                [self.arrayForBool addObject:[NSNumber numberWithBool:NO]];
                [tempArray addObject:[NSArray new]];
            }
            self.dataArray = [NSArray arrayWithArray:tempArray];
            tempArray = nil;
            
            @try {
                if (self.columnList.count <= 3) {
                    self.multiSelection = self.columnList;
                } else {
                    self.multiSelection = [self.columnList subarrayWithRange:NSMakeRange(0, 3)];
                }
                
                // Set first 3 column
                if (self.multiSelection.count == 3) {
                    self.header1.text = [self.multiSelection objectAtIndex:0];
                    self.header2.text = [self.multiSelection objectAtIndex:1];
                    self.header3.text = [self.multiSelection objectAtIndex:2];
                    
                    self.header1.hidden = NO;
                    self.header2.hidden = NO;
                    self.header3.hidden = NO;
                }
                
                if (self.multiSelection.count == 2) {
                    self.header1.text = [self.multiSelection objectAtIndex:0];
                    self.header2.text = [self.multiSelection objectAtIndex:1];
                    self.header1.hidden = NO;
                    self.header2.hidden = NO;
                }
                
                if (self.multiSelection.count == 1) {
                    self.header1.text = [self.multiSelection objectAtIndex:0];
                    self.header1.hidden = NO;
                }
                
                // Show no data to display
                if (response.tableDatas.count == 0) {
                    [self.noRecordsView removeFromSuperview];
                    _noRecordsView = nil;
                    _noRecordsView = [Helper createNoAccessView:self.tableView withMessage:@"No data to display"];
                    self.noRecordsView.hidden = NO;
                }
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
            }
        } else {
            self.navigationItem.rightBarButtonItems = nil;
            self.noRecordsView.hidden = NO;
        }
        
        // Show error message - Report loading error!
        if (status.intValue == 500) {
            self.noRecordsView.hidden = YES;
            UIView *errorView = [Helper createNoAccessView:self.tableView withMessage:@"There is some error in report data. Please contact to system administrator."];
            errorView.hidden = NO;
        }
        [self.tableView reloadData];
    }];
}

//---------------------------------------------------------------

- (void) callWebServiceToLikeCommentReport:(NSDictionary *)details forIndexPath:(NSIndexPath *)indexPath {
    
    NSMutableDictionary *filterParams = [[self.filter serializeFiltersForLikeComment] mutableCopy];
    User *user = [User signedInUser];
    if (!isEmptyString(user.accessToken)) {
        NSString *authorisationBearer = [NSString stringWithFormat:@"%@", user.accessToken];
        [filterParams setObject:[NSString stringWithFormat:@"%@",authorisationBearer] forKey:@"authorizationId"];
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/likeCommentReport/%d", DASHBOARD_CHART_URL, self.report.customReportId.intValue];
    NSString *chartUrlString = [ServiceUtil addHeaderParametersWithOutBaseURL:urlString withParams:filterParams];
    NSString* encodedUrl = [chartUrlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    // Analytics
//    [AnalyticsLogger logDashboardChartShareEvent:self reportId:self.report.customReportId.stringValue comment:self.commentString];
    
    // Start progress
    self.hud = [Helper showLoading:self.view];
    [ServiceManager callWebServiceToShareFeeds:encodedUrl params:details usingBlock:^(NSDictionary *response, NSError *error) {
        [Helper hideLoading:self.hud];
        if (response != nil) {
            if ([response[@"statusCode"] integerValue] == 200) {
                @try {
                    // Update social count array
                    NSString *uniqueId = [NSString stringWithFormat:@"%d_%d", (int)indexPath.section, (int)indexPath.row];
                    NSNumber *socIntType = [[details allKeys] containsObject:@"socIntType"] ? [details objectForKey:@"socIntType"] : nil;
                    LikeCommentCount *model = [LikeCommentCount new];
//                    model.isCallService = NO;
                    model.uniqueId = uniqueId;
                    
                    if (socIntType) {
                        // replace like and comment count
                        NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.likeCommentArray];
                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.uniqueId = %@", uniqueId];
                        NSArray *filters = [self.likeCommentArray filteredArrayUsingPredicate:predicate];
                        // update existing model
                        if (filters.count > 0) {
                            LikeCommentCount *filterModel = (LikeCommentCount *)filters[0];
                            NSInteger index = [self.likeCommentArray indexOfObject:filterModel];
                            if (socIntType.intValue == 0) {
                                filterModel.likes = [NSNumber numberWithInt:(filterModel.likes.intValue + 1)];
                                filterModel.sharedByLike = [User signedInUser].userID.stringValue;
                            } else {
                                filterModel.comments = [NSNumber numberWithInt:(filterModel.comments.intValue + 1)];
                                filterModel.sharedByComment = [User signedInUser].userID.stringValue;
                            }
                            // Replace old model with updated model
                            model = filterModel;
                            [tempArray replaceObjectAtIndex:index withObject:filterModel];
                        } else {
                            // Add new model
                            if (socIntType.intValue == 0) {
                                model.likes = [NSNumber numberWithInt:1];
                                model.sharedByLike = [User signedInUser].userID.stringValue;
                            } else {
                                model.comments = [NSNumber numberWithInt:1];
                                model.sharedByComment = [User signedInUser].userID.stringValue;
                            }
                            [tempArray addObject:model];
                        }
                        
                        self.likeCommentArray = [NSArray arrayWithArray:tempArray];
                        tempArray = nil;
                        
                        // Reload cell on main thread
                        dispatch_async(dispatch_get_main_queue(), ^{
                            // Set counts
                            DashboardTableCell *cell = (DashboardTableCell *)[self.tableView cellForRowAtIndexPath:indexPath];
                            cell.likeView.label.text = [NSString stringWithFormat:@"%d", (int)model.likes.intValue];
                            cell.commentView.label.text = [NSString stringWithFormat:@"%d", (int)model.comments.intValue];
                            
                            // Update model
                            if (socIntType.intValue == 0) {
                                cell.isAlreadyLike = YES;
                                cell.likeView.image.image = [UIImage imageNamed:LIKED_IMAGE];
                            }
                        });
                    }
                } @catch (NSException *exception) {
                    DLog(@"Exception :%@", exception.debugDescription);
                }
            }
        }
    }];
}

//---------------------------------------------------------------

- (void) callWebServiceToGetLikeAndCommentCount:(NSDictionary *)details forIndexPath:(DashboardTableCell *)cell {
    NSMutableDictionary *filterParams = [[self.filter serializeFiltersForLikeComment] mutableCopy];
    User *user = [User signedInUser];
    if (!isEmptyString(user.accessToken)) {
        NSString *authorisationBearer = [NSString stringWithFormat:@"%@", user.accessToken];
        [filterParams setObject:[NSString stringWithFormat:@"%@",authorisationBearer] forKey:@"authorizationId"];
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/likeCommentReportCount/%d", DASHBOARD_CHART_URL, self.report.customReportId.intValue];
    NSString *chartUrlString = [ServiceUtil addHeaderParametersWithOutBaseURL:urlString withParams:filterParams];
    NSString* encodedUrl = [chartUrlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    // show button progress
    [self showSocialButtons:NO forCell:cell];
    
    [ServiceManager callWebServiceToShareFeeds:encodedUrl params:details usingBlock:^(NSDictionary *response, NSError *error) {

        LikeCommentCount *model = [LikeCommentCount new];
        model.isCallService = NO;

        if (response != nil) {
            if ([response[@"statusCode"] integerValue] == 200) {
                NSDictionary *data = [[response allKeys] containsObject:MAIN_BODY] ? [response objectForKey:MAIN_BODY] : nil;
                if (data) {
                    [HKJsonUtils updatePropertiesWithData:data forObject:model];
                }
            }
        }
        
        @try {
            // Unique id
            NSString *uniqueId = [NSString stringWithFormat:@"%d_%d", (int)cell.indexPath.section, (int)cell.indexPath.row];
            model.uniqueId = uniqueId;
            
            // replace like and comment count
            NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.likeCommentArray];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.uniqueId = %@", uniqueId];
            NSArray *filters = [self.likeCommentArray filteredArrayUsingPredicate:predicate];
            if (filters.count > 0) {
                LikeCommentCount *filterModel = (LikeCommentCount *)filters[0];
                NSInteger index = [self.likeCommentArray indexOfObject:filterModel];
                filterModel.likes = model.likes;
                filterModel.comments = model.comments;
                filterModel.isCallService = NO;
                [tempArray replaceObjectAtIndex:index withObject:model];
            } else {
                // Add model
                [tempArray addObject:model];
            }
            self.likeCommentArray = [NSArray arrayWithArray:tempArray];
            tempArray = nil;
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
            // Reload cell on main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showSocialButtons:YES forCell:cell];
            });

        }
        
        // Reload cell on main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            // Set counts
            cell.likeView.label.text = [NSString stringWithFormat:@"%d", (int)model.likes.intValue];
            cell.commentView.label.text = [NSString stringWithFormat:@"%d", (int)model.comments.intValue];
            cell.isAlreadyLike = ![self checkUserCanLike:model];
            [self showSocialButtons:YES forCell:cell];
        });
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) showSocialButtons:(BOOL)isShow forCell:(DashboardTableCell *)cell {
    
    cell.likeView.label.hidden = !isShow;
    cell.commentView.label.hidden = !isShow;

    if (isShow) {
        cell.likeView.image.image = [UIImage imageNamed:LIKE_IMAGE];
        cell.commentView.image.image = [UIImage imageNamed:COMMENT_IMAGE];
        [cell.likeView.button hideLoading];
        [cell.commentView.button hideLoading];
        
        // Show if already like
        cell.likeView.image.image = cell.isAlreadyLike ? [UIImage imageNamed:LIKED_IMAGE] : [UIImage imageNamed:LIKE_IMAGE];
    } else {
        cell.likeView.image.image = nil;
        cell.commentView.image.image = nil;
        [cell.likeView.button showLoading];
        [cell.commentView.button showLoading];
    }
}

//---------------------------------------------------------------

- (NSDictionary *) createServiceRequestForType:(int)type withIndexPath:(NSIndexPath *)indexPath {
    /*
     1. socIntType       : Like = 0, Comment = 1
     2. entityType       : from response you will get ()
     3. entityId         : from response
     4. entityName       : from response
     5. value            : selected row value
     6. reportId         : Take tableRow title, use this title as key and get value from tableHeaders
                           But, if we get 'socIntTableType' in response then concat with 'concatWithReportName' + 'tableHeaders[key]'
     
     if we get 'socIntTableType' in response then
     concat with 'concatWithReportName' + 'tableHeaders[value]'
     
     7. reportName       : Take tableRow title, use this title as key and get value from tableHeaders
                           But, if we get 'socIntTableType' in response then concat with 'concatWithReportName' + 'tableHeaders[key]'
     
     8. dataStartDate    : Start Date for column which is not compared
     9. dataEndDate      : End Date for column which is not compared
     10. compDataStartDate: Start Date for column which is compared
     11. compDataEndDate : End Date for column which is compared
     12. dataColumnStartDate :
     13. dataColumnEndDate :
     Above both dates are special dates, in web some report does not contains date range, that time they use this dates.
     Identify these dates in reponse when ever you received it, it will be like below example
     'dataStartDate' + 'tableHeaders[key]'
     
     NOTE: For dates fields there will be only two fields attached in request query from above four!
     */
    
    // This is for like
    NSNumber *socIntType = [NSNumber numberWithInt:type];
    NSDictionary *selectedData = [self.tableDetails.tableDatas objectAtIndex:indexPath.section];
    
    NSNumber *entityId = [[selectedData allKeys] containsObject:@"entityId"] ? [selectedData objectForKey:@"entityId"] : nil;
    NSString *entityType = [[selectedData allKeys] containsObject:@"entityType"] ? [selectedData objectForKey:@"entityType"] : @"";
    
    // Early exit if not going well!😎
    if (!entityId || !entityType) {
        // show an alert
        return nil;
    }
    
    // Straight comes from response
    NSString *entityName = [[selectedData allKeys] containsObject:@"entityName"] ? [selectedData objectForKey:@"entityName"] : @"";
    
    // Get value of selected row
    
    NSArray *objectArray = [self.dataArray objectAtIndex:indexPath.section];
    NSDictionary *rowDetails = [objectArray objectAtIndex:indexPath.row];
    NSString *key = [rowDetails objectForKey:KEY_TITLE];
    NSString *value = [[rowDetails allKeys] containsObject:KEY_VALUE] ? [rowDetails objectForKey:KEY_VALUE] : @"";
    
    /* Report Id - please read above */
    NSString *reportName = [[self.tableDetails.tableHeaders allKeys] containsObject:key] ? key : @"";
    NSString *reportId = [[self.tableDetails.tableHeaders allKeys] containsObject:key] ? [self.tableDetails.tableHeaders objectForKey:key]: @"";
    
    if ([[selectedData allKeys] containsObject:CONCAT_REPORT_NAME]) {
        reportId = [NSString stringWithFormat:@"%@ %@", [selectedData objectForKey:CONCAT_REPORT_NAME], reportId];
        reportName = [NSString stringWithFormat:@"%@ %@", [selectedData objectForKey:CONCAT_REPORT_NAME], reportName];
    }
    
    // Dates
    // Default dates
    NSString *startDateFieldName = DATA_START_DATE;
    NSString *endDateFieldName = DATA_END_DATE;
    NSString *startDate = [[selectedData allKeys] containsObject:DATA_START_DATE] ? [selectedData objectForKey:DATA_START_DATE] : @"";
    NSString *endDate = [[selectedData allKeys] containsObject:DATA_END_DATE] ? [selectedData objectForKey:DATA_END_DATE] : @"";
    
    // Special dates
    NSString *startDateCheck = [NSString stringWithFormat:@"%@%@", DATA_START_DATE, [self.tableDetails.tableHeaders objectForKey:key]];
    NSString *endDateCheck = [NSString stringWithFormat:@"%@%@", DATA_END_DATE, [self.tableDetails.tableHeaders objectForKey:key]];
    if ([[selectedData allKeys] containsObject:startDateCheck]) {
        startDateFieldName = @"dataColumnStartDate";
        startDate = [selectedData objectForKey:startDateCheck];
    }
    if ([[selectedData allKeys] containsObject:endDateCheck]) {
        endDateFieldName = @"dataColumnEndDate";
        endDate = [selectedData objectForKey:endDateCheck];
    }

    NSDictionary *details = @{
                                @"socIntType"       : socIntType,
                                @"entityType"       : entityType,
                                @"entityId"         : entityId,
                                @"entityName"       : entityName,
                                @"value"            : value,
                                @"reportId"         : reportId,
                                @"reportName"       : reportName,
                                startDateFieldName  : startDate,
                                endDateFieldName    : endDate,
                                @"comment"          : self.commentString ? self.commentString : @"",
                              };
    
    // Compare dates
    // if compare date is there than send compare date too
    NSString *compareStartDate = [[selectedData allKeys] containsObject:@"compDataStartDate"] ? [selectedData objectForKey:@"compDataStartDate"] : nil;
    NSString *compareEndDate = [[selectedData allKeys] containsObject:@"compDataEndDate"] ? [selectedData objectForKey:@"compDataEndDate"] : nil;
    
    if (compareStartDate && compareEndDate) {
        // Add two more params...
        NSMutableDictionary *tempDict = [NSMutableDictionary dictionaryWithDictionary:details];
        [tempDict setObject:compareStartDate forKey:@"compDataStartDate"];
        [tempDict setObject:compareEndDate forKey:@"compDataEndDate"];
        details = [NSDictionary dictionaryWithDictionary:tempDict];
        tempDict = nil;
    }
    return details;
}

//---------------------------------------------------------------

- (void) shareReportAsPDF {
    
    // Get HTML from WKWebView
    [self.tableWebView evaluateJavaScript:@"document.documentElement.outerHTML.toString()" completionHandler:^(NSString *htmlString, NSError * _Nullable error) {
        // Update html file before going to further
        // Hey! its another patch from me :)
        // Well, we are facing problem while creating pdf file from this html.
        // The html string contains hell of height with it, which gives extra blank pages in pdf file
        // E.g. <html style="height: 15290px;"> (You can check put debug at this place and check this)
        // Now I am doing one hack here to remove ("style=") word from html string to overcome from this issue
        
        //**** ---- Removing height parameter from html string -----***//
        NSString *filterHtmlString = [htmlString stringByReplacingOccurrencesOfString:@"<html style=\"height:" withString:@"<html"];
        
        // Create webview - Temporary!
        [self.tempWebView removeFromSuperview];
        self.tempWebView.frame = self.view.frame;
        [self.tempWebView loadHTMLString:filterHtmlString baseURL:nil];
        [self.tableView addSubview:self.tempWebView];
        
        // Show loading indicator on main thread, to avoid dead lock
        self.hud = [Helper showLoading:self.view];
    }];
}

//---------------------------------------------------------------

- (UILabel *) addLabelsToStack {
    UILabel *label = [[UILabel alloc] init];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:AVENIR_NEXT_FONT size:12];
    label.numberOfLines = 0;
    return label;
}

//---------------------------------------------------------------

- (void) addRemoveLabelsFromStackView:(DashboardTableCell *)cell forDetail:(NSDictionary *)detail {
    
    // Check that StackView's subViews and column list are same or not?
    if (cell.stackView.subviews.count == self.multiSelection.count) {
        // Count is same not need to add/remove views
        for (int index = 0; index < self.multiSelection.count; index++) {
            NSInteger tag = kLabelTag + index;
            UILabel *label = [cell.stackView viewWithTag:tag];
            
            NSString *key = [self.multiSelection objectAtIndex:index];
            // Set text
            NSString *value = @"";
            if ([[detail allKeys] containsObject:key]) {
                value = [detail objectForKey:key];
            }
            label.text = value;
        }
    } else {
        // Remove all views from subView
        [[cell.stackView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];

        // Add label to StackView...
        for (int index = 0; index < self.multiSelection.count; index++) {
            UILabel *label = [self addLabelsToStack];
            label.tag = kLabelTag + index;

            NSString *key = [self.multiSelection objectAtIndex:index];
            // set text
            NSString *value = @"";
            if ([[detail allKeys] containsObject:key]) {
                value = [detail objectForKey:key];
            }
            label.text = value;
            [cell.stackView addArrangedSubview:label];
        }
    }
}

//---------------------------------------------------------------

- (void) createPullToRefresh {
    __weak typeof(self) wself = self;
    _refreshView = [LGRefreshView refreshViewWithScrollView:self.tableView refreshHandler:^(LGRefreshView *refreshView) {
        if (wself) {
            __strong typeof(wself) self = wself;
            [self.navigationItem.rightBarButtonItems enumerateObjectsUsingBlock:^(UIBarButtonItem *barButton, NSUInteger idx, BOOL * _Nonnull stop) {
                barButton.enabled = NO;
            }];
            
            // Call service
            [self callWebServiceToGetTableData];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void) {
                [self.refreshView endRefreshing];
            });
        }
    }];
    _refreshView.tintColor = APP_COLOR;
}

//---------------------------------------------------------------

- (NSString *) getTemporaryFolderPath {
    NSString *filePath = nil;
    @try {
        NSFileManager *fileManager = [NSFileManager new];
        NSError *error = nil;
        NSURL *docsurl = [fileManager URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:&error];
        if (!error) {
            NSURL *tempFolderURL = [docsurl URLByAppendingPathComponent:@"TempCharts"];
            BOOL isCreated = [fileManager createDirectoryAtURL:tempFolderURL withIntermediateDirectories:YES attributes:nil error:&error];
            if (isCreated) {
                // Directory created - return destination path
                filePath = tempFolderURL.path;
            }
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        filePath = nil;
    }
    return filePath;
}

//---------------------------------------------------------------

- (void) removeFilesFromTempFolder {
    @try {
        __block NSError *error;
        // Get document directory path
        NSString *tempFolderPath = [[Helper documentDirectory] stringByAppendingPathComponent:@"TempCharts"];
        
        // Get all files at temp folders
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSArray *files = [fileManager contentsOfDirectoryAtPath:tempFolderPath error:&error];
        
        // Iterate and remove files
        [files enumerateObjectsUsingBlock:^(NSString *fileName, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *filePath = [tempFolderPath stringByAppendingPathComponent:fileName];
            BOOL isRemoved = [fileManager removeItemAtPath:filePath error:&error];
            
            // Check if any file is not removed
            if (!isRemoved) {
                DLog(@"File: %@ [%@] not removed", fileName, filePath);
            }
        }];
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (NSString *) convertObjectToString:(id)object {
    if ([object isKindOfClass:[NSNumber class]]) {
        NSNumber *number = (NSNumber *)object;
        return number.stringValue;
    }
    return object;
}

//---------------------------------------------------------------

- (void) createHeaderView {
    UIFont *font = [UIFont fontWithName:AVENIR_NEXT_DEMI_BOLD_FONT size:12];
    self.headerView.backgroundColor = [UIColor colorWithRed:210/225.0 green:210/225.0 blue:210/225.0 alpha:1];
    
    _header1 = [[UILabel alloc] init];
    self.header1.translatesAutoresizingMaskIntoConstraints = NO;
    self.header1.textAlignment = NSTextAlignmentCenter;
    self.header1.font = font;
    self.header1.numberOfLines = 0;
    
    _header2 = [[UILabel alloc] init];
    self.header2.translatesAutoresizingMaskIntoConstraints = NO;
    self.header2.textAlignment = NSTextAlignmentCenter;
    self.header2.font = font;
    self.header2.numberOfLines = 0;
    
    _header3 = [[UILabel alloc] init];
    self.header3.translatesAutoresizingMaskIntoConstraints = NO;
    self.header3.textAlignment = NSTextAlignmentCenter;
    self.header3.font = font;
    self.header3.numberOfLines = 0;
    
    // Stack View
    UIStackView *stackView = [[UIStackView alloc] init];
    stackView.axis = UILayoutConstraintAxisHorizontal;
    stackView.distribution = UIStackViewDistributionFill;
    stackView.alignment = UIStackViewAlignmentFill;
    stackView.spacing = 5;
    [stackView addArrangedSubview:self.header1];
    [stackView addArrangedSubview:self.header2];
    [stackView addArrangedSubview:self.header3];
    
    stackView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.headerView addSubview:stackView];
    
    NSDictionary *views = @{@"stackView" : stackView};
    // Constraints for Stack View
    [self.headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[stackView]|" options: 0 metrics:nil views:views]];
    [self.headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[stackView]|" options: 0 metrics:nil views:views]];
}

//---------------------------------------------------------------

- (BOOL) checkUserCanLike:(LikeCommentCount *)model {
    BOOL canLike = YES;
    if (model.sharedByLike) {
        NSArray *userIds = [model.sharedByLike componentsSeparatedByString:@","];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.intValue = %d", [User signedInUser].userID.intValue];
        NSArray *filters = [userIds filteredArrayUsingPredicate:predicate];
        if (filters.count > 0) {
            canLike = NO;
        }
    }
    return canLike;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return (self.multiSelection.count > 0) ? self.tableDetails.tableDatas.count : 0;
}

//---------------------------------------------------------------

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    @try {
        if ([[self.arrayForBool objectAtIndex:section] boolValue]) {
            NSArray *objArray = [self.dataArray objectAtIndex:section];
            return objArray.count;
        }
        else {
            return 0;
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kHeaderHeight;
}

//---------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

//---------------------------------------------------------------

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"DashboardTableCell%d%d", (int)indexPath.section, (int)indexPath.row];
    DashboardTableCell *cell = (DashboardTableCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[DashboardTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    // This will be used in like and comment row identification
    cell.indexPath = indexPath;
    
    // Get details
    NSArray *objectArray = [self.dataArray objectAtIndex:indexPath.section];
    NSDictionary *tableData = [objectArray objectAtIndex:indexPath.row];

//    NSString *title = [tableData objectForKey:KEY_TITLE];
    NSString *title = [Helper removeWhiteSpaceAndNewLineCharacter:[tableData objectForKey:KEY_TITLE]];
    NSString *value = [Helper removeWhiteSpaceAndNewLineCharacter:[tableData objectForKey:KEY_VALUE]];

    cell.titleLabel.text = title;
    cell.valueLabel.text = value;
    
    cell.likeView.button.tag = LIKE_TAG + indexPath.row;
    cell.commentView.button.tag = COMMENT_TAG + indexPath.row;
    [cell.likeView.button addTarget:self action:@selector(likeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [cell.commentView.button addTarget:self action:@selector(commentButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.likeView.label.text = @"0";
    cell.commentView.label.text = @"0";
    cell.isAlreadyLike = NO;
    
    // Show like & comment for only those fields which are available for social interaction
    BOOL isShowSocialButtons = NO;
    BOOL isCallService = YES;
    
    NSString *columnValue = [self convertObjectToString:[self.tableDetails.tableHeaders objectForKey:title]];
    if ([[self.tableDetails.socialInteractionEnabledColumns allKeys] containsObject:columnValue]) {
        NSNumber *value = [self.tableDetails.socialInteractionEnabledColumns objectForKey:columnValue];
        isShowSocialButtons = value.boolValue;
        isCallService = isShowSocialButtons; // Call service only for those columns which are social enable
    }
    
    // Also check that if reportName or value is empty than hide social buttons;
    if (title.length == 0 || value.length == 0) {
        isShowSocialButtons = NO;
    }
    
    cell.likeView.hidden = !isShowSocialButtons;
    cell.commentView.hidden = !isShowSocialButtons;
    
    NSString *uniqueId = [NSString stringWithFormat:@"%d_%d", (int)cell.indexPath.section, (int)cell.indexPath.row];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.uniqueId = %@", uniqueId];
    NSArray *filters = [self.likeCommentArray filteredArrayUsingPredicate:predicate];
    if (filters.count > 0) {
        LikeCommentCount *model = (LikeCommentCount *)filters[0];
        isCallService = model.isCallService;
        // Set counts
        cell.likeView.label.text = [NSString stringWithFormat:@"%d", (int)model.likes.intValue];
        cell.commentView.label.text = [NSString stringWithFormat:@"%d", (int)model.comments.intValue];
        
        // Check that user can like or not?
        cell.isAlreadyLike = ![self checkUserCanLike:model];
        cell.likeView.image.image = cell.isAlreadyLike ? [UIImage imageNamed:LIKED_IMAGE] : [UIImage imageNamed:LIKE_IMAGE];
    }
    
    if (isCallService) {
        // Call service in background to get like and comment count
        NSDictionary *details = [self createServiceRequestForType:0 withIndexPath:indexPath];
        [self callWebServiceToGetLikeAndCommentCount:details forIndexPath:cell];
    }
    return cell;
}

//---------------------------------------------------------------

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kHeaderHeight)];
    headerView.backgroundColor = [UIColor whiteColor];
    headerView.tag = section;
    
    [Helper addTopLine:headerView withColor:TABLE_SEPERATOR_COLOR];
    [Helper addBottomLine:headerView withColor:TABLE_SEPERATOR_COLOR];
    // Drop shadow
    headerView.layer.shadowColor = TABLE_SEPERATOR_COLOR.CGColor;
    headerView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    headerView.layer.shadowRadius = 0.5f;
    headerView.layer.shadowOpacity = 2.0f;
    
    UIFont *font = [UIFont fontWithName:AVENIR_NEXT_FONT size:12];
    // Column1
    UILabel *header1 = [[UILabel alloc] init];
    header1.translatesAutoresizingMaskIntoConstraints = NO;
    header1.textAlignment = NSTextAlignmentCenter;
    header1.font = font;
    header1.numberOfLines = 0;
    // Column2
    UILabel *header2 = [[UILabel alloc] init];
    header2.translatesAutoresizingMaskIntoConstraints = NO;
    header2.textAlignment = NSTextAlignmentCenter;
    header2.font = font;
    header2.numberOfLines = 0;
    // Column3
    UILabel *header3 = [[UILabel alloc] init];
    header3.translatesAutoresizingMaskIntoConstraints = NO;
    header3.textAlignment = NSTextAlignmentCenter;
    header3.font = font;
    header3.numberOfLines = 0;

    // Stack View
    UIStackView *stackView = [[UIStackView alloc] init];
    stackView.axis = UILayoutConstraintAxisHorizontal;
    stackView.distribution = UIStackViewDistributionFill;
    stackView.alignment = UIStackViewAlignmentFill;
    stackView.spacing = 5;
    [stackView addArrangedSubview:header1];
    [stackView addArrangedSubview:header2];
    [stackView addArrangedSubview:header3];
    
    stackView.translatesAutoresizingMaskIntoConstraints = NO;
    [headerView addSubview:stackView];
    
    // Arrow Image
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.translatesAutoresizingMaskIntoConstraints = NO;
    imageView.tintColor = [UIColor blackColor];
    [imageView setBackgroundColor:[UIColor clearColor]];
    imageView.alpha = 0.8;
    [headerView addSubview:imageView];
    
    // set image
    NSNumber *isExpand = [self.arrayForBool objectAtIndex:section];
    imageView.image = (isExpand.boolValue) ? [UIImage imageNamed:DOWN_ARROW] : [UIImage imageNamed:RIGHT_ARROW];

    NSDictionary *views = @{@"stackView" : stackView, @"imageView" : imageView};
    // Constraints for Stack View
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[stackView]-[imageView(12)]-10-|" options: 0 metrics:nil views:views]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[stackView]|" options: 0 metrics:nil views:views]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[imageView(12)]" options: 0 metrics:nil views:views]];

    // Get details
    NSDictionary *tableData = [self.tableDetails.tableDatas objectAtIndex:section];
    header1.hidden = NO;
    header2.hidden = NO;
    header3.hidden = NO;

    NSString *value1 = @"";
    NSString *value2 = @"";
    NSString *value3 = @"";

    if (self.multiSelection.count == 2) {
        header3.hidden = YES;
        NSString *key1 = [self convertObjectToString:[self.tableDetails.tableHeaders objectForKey:[self.multiSelection objectAtIndex:0]]];
        NSString *key2 = [self.tableDetails.tableHeaders objectForKey:[self.multiSelection objectAtIndex:1]];

        if ([[tableData allKeys] containsObject:key1]) {
            value1 = [tableData objectForKey:key1];
        }
        if ([[tableData allKeys] containsObject:key2]) {
            value2 = [tableData objectForKey:key2];
        }
    } else if (self.multiSelection.count == 1) {
        header2.hidden = YES;
        header3.hidden = YES;
        NSString *key1 = [self convertObjectToString:[self.tableDetails.tableHeaders objectForKey:[self.multiSelection objectAtIndex:0]]];
        if ([[tableData allKeys] containsObject:key1]) {
            value1 = [tableData objectForKey:key1];
        }
    } else if (self.multiSelection.count == 0) {
        header1.hidden = YES;
        header2.hidden = YES;
        header3.hidden = YES;
    } else {
        NSString *key1 = [self convertObjectToString:[self.tableDetails.tableHeaders objectForKey:[self.multiSelection objectAtIndex:0]]];
        NSString *key2 = [self convertObjectToString:[self.tableDetails.tableHeaders objectForKey:[self.multiSelection objectAtIndex:1]]];
        NSString *key3 = [self convertObjectToString:[self.tableDetails.tableHeaders objectForKey:[self.multiSelection objectAtIndex:2]]];

        if ([[tableData allKeys] containsObject:key1]) {
            value1 = [tableData valueForKey:key1];
        }
        if ([[tableData allKeys] containsObject:key2]) {
            value2 = [tableData valueForKey:key2];
        }
        if ([[tableData allKeys] containsObject:key3]) {
            value3 = [tableData valueForKey:key3];
        }
    }

    header1.text = value1;
    header2.text = value2;
    header3.text = value3;
    
    /********** Add UITapGestureRecognizer to SectionView   **************/
    UITapGestureRecognizer *headerTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
    [headerView addGestureRecognizer:headerTapped];
    return headerView;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark AddColumnsDelegate delegate methods

//---------------------------------------------------------------

- (void) selectedItems:(NSArray *)items {
    
    // Remove all previous objects
    [self.arrayForBool removeAllObjects];
    for (int i = 0; i < [self.tableDetails.tableDatas count]; i++) {
        [self.arrayForBool addObject:[NSNumber numberWithBool:NO]];
    }

    // Reset like and comment count
    self.likeCommentArray = [NSArray new];

    self.multiSelection = items;
    
    // Set headers
    if (self.multiSelection.count == 2) {
        self.header1.text = [self.multiSelection objectAtIndex:0];
        self.header2.text = [self.multiSelection objectAtIndex:1];
        
        self.header3.hidden = YES;
    } else if (self.multiSelection.count == 1) {
        self.header1.text = [self.multiSelection objectAtIndex:0];
        
        self.header2.hidden = YES;
        self.header3.hidden = YES;
    } else if (self.multiSelection.count == 0) {
        self.header1.hidden = YES;
        self.header2.hidden = YES;
        self.header3.hidden = YES;
    } else {
        self.header1.text = [self.multiSelection objectAtIndex:0];
        self.header2.text = [self.multiSelection objectAtIndex:1];
        self.header3.text = [self.multiSelection objectAtIndex:2];
    }
    
    [self.tableView reloadData];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark SnapAlert methods

//---------------------------------------------------------------

- (void) createAlertView {
    
    CGFloat alertWidth = 300;
    CGFloat alertHeight = 310;
    
    _alertView = [[SnapAlertView alloc] initWithFrame:CGRectMake(0, 0, alertWidth, alertHeight)];
    [self.alertView setDelegate:self];
    [self.view addSubview:self.alertView];
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[self.alertView]];
    self.popupController.theme = [self customTheme];
    self.popupController.theme.popupStyle = CNPPopupStyleCentered;
    self.popupController.delegate = self;
}

//---------------------------------------------------------------

- (CNPPopupTheme *)customTheme {
    CNPPopupTheme *defaultTheme = [[CNPPopupTheme alloc] init];
    defaultTheme.backgroundColor = [UIColor clearColor];
//    defaultTheme.cornerRadius = 4.0f;
    defaultTheme.popupContentInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    defaultTheme.popupStyle = CNPPopupStyleCentered;
    defaultTheme.presentationStyle = CNPPopupPresentationStyleSlideInFromBottom;
    defaultTheme.dismissesOppositeDirection = NO;
    defaultTheme.maskType = CNPPopupMaskTypeDimmed;
    defaultTheme.shouldDismissOnBackgroundTouch = NO;
    defaultTheme.movesAboveKeyboard = YES;
    defaultTheme.contentVerticalPadding = 0;
    defaultTheme.maxPopupWidth = 300;
    return defaultTheme;
}

//---------------------------------------------------------------

- (void) showAlert:(id)sender {
    if (self.alertView == nil) {
        [self createAlertView];
    }
    [self.popupController presentPopupControllerAnimated:YES];
}

//---------------------------------------------------------------

- (void) dismissButtonTapped:(id)sender {
    [self dismissAlert:sender];
}

//---------------------------------------------------------------

- (void) dismissAlert:(id)sender {
    if (self.alertView) {
        [self.alertView resignTextViewResponder];
        [self.popupController dismissPopupControllerAnimated:YES];
    }
}

//---------------------------------------------------------------

- (void)popupControllerDidPresent:(CNPPopupController *)controller {
    [self.alertView.commentText becomeFirstResponder];
}

//---------------------------------------------------------------

- (void) postComment:(NSString *)commentString {
    [self dismissAlert:nil];
    self.commentString = commentString ? commentString : @"";
    NSDictionary *details = [self createServiceRequestForType:1 withIndexPath:self.commentIndexPath];
    [self callWebServiceToLikeCommentReport:details forIndexPath:self.commentIndexPath];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WKNavigationDelegate methods

//---------------------------------------------------------------

- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    NSString *javascript = @"var meta = document.createElement('meta');meta.setAttribute('name', 'viewport');meta.setAttribute('content', 'width=device-width, initial-scale=0.0, maximum-scale=0.0, user-scalable=no');document.getElementsByTagName('head')[0].appendChild(meta);";
    [webView evaluateJavaScript:javascript completionHandler:nil];
}

//---------------------------------------------------------------

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
    // Only make changes if current view controller is visible and active
    
    if (self.isViewLoaded && self.view.window) {
        // viewController is visible
        [webView evaluateJavaScript:@"document.getElementsByClassName('x_panel')[0].offsetHeight;" completionHandler:^(id response, NSError * _Nullable error) {
            [Helper hideLoading:self.hud];
            CGFloat height = [NSString stringWithFormat:@"%@", response].floatValue;
            [UIView animateWithDuration:0.5 animations:^{
                self.webViewHeightConstraint.constant = height;
                [self.tableWebView layoutIfNeeded];
            } completion:^(BOOL finished) {
                self.navigationItem.rightBarButtonItem.enabled = YES;

                // Share as PDF
                [self shareReportAsPDF];
            }];
        }];
    }
}

//---------------------------------------------------------------

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    DLog(@"WEBVIEW FAILED: %s. Error %@",__func__,error);
}

//---------------------------------------------------------------

- (void)webViewWebContentProcessDidTerminate:(WKWebView *)webView {
    DLog(@"WEBVIEW TERMINTATED:");
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UIWebView delegate methods

//---------------------------------------------------------------

- (BOOL)webView:(UIWebView *)theWebView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    theWebView.scalesPageToFit = YES;
    return YES;
}

//---------------------------------------------------------------

- (void) webViewDidFinishLoad:(UIWebView *)webView {
    if (webView.isLoading)
        return;
    
    // Hide loading indicator
    [Helper hideLoading:self.hud];
    UIPrintPageRenderer *render = [[UIPrintPageRenderer alloc] init];
    [render addPrintFormatter:webView.viewPrintFormatter startingAtPageAtIndex:0];
    
    float topPadding = 10.0f;
    float bottomPadding = 10.0f;
    float leftPadding = 10.0f;
    float rightPadding = 10.0f;
    CGRect printableRect = CGRectMake(leftPadding,
                                      topPadding,
                                      kPaperSizeA4.width - leftPadding - rightPadding,
                                      kPaperSizeA4.height - topPadding - bottomPadding);
    CGRect paperRect = CGRectMake(0, 0, kPaperSizeA4.width, kPaperSizeA4.height);
    
    [render setValue:[NSValue valueWithCGRect:paperRect] forKey:@"paperRect"];
    [render setValue:[NSValue valueWithCGRect:printableRect] forKey:@"printableRect"];
    
    NSData *pdfData = [render printToPDF];
    
    BOOL isFileCreated = YES;
    if (pdfData) {
        // Get temporary folder path
        NSString *filePath = [self getTemporaryFolderPath];
        NSString *pdfFilePath = [NSString stringWithFormat:@"%@/%@.pdf", filePath, self.report.reportName];
        
        // Check if filePath exist?
        if (filePath) {
            // Write webView data to temporary file
            [pdfData writeToFile:pdfFilePath atomically:YES];
            
            // Remove temproray webView
            [self.tempWebView removeFromSuperview];
            
            // Open Activity controller
            NSURL *fileUrl = [NSURL fileURLWithPath:pdfFilePath isDirectory:NO];
            NSArray *activityItems = @[fileUrl];
            UIActivityViewController* activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems  applicationActivities:nil];
            [self presentViewController:activityViewController animated:YES completion:^{}];
        } else {
            isFileCreated = NO;
        }
        
    } else {
        isFileCreated = NO;
        [self.tempWebView removeFromSuperview];
        DLog(@"PDF couldnot be created");
    }
    
    // if file is not created show alert
    [Helper showAlertWithTitle:@"Error" message:@"Sharing is not available right now, Reload page or Restart the application" forController:self];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    self.title = self.report.reportName;

    // Dynamic table height
    self.tableView.estimatedRowHeight = 60;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _arrayForBool = [NSMutableArray new];
    _noRecordsView = [Helper createNoAccessView:self.tableView withMessage:[App_Delegate.keyMapping getKeyValue:@"KEY_NO_DATA_DASHBOARD"]];

    // create headerView
    [self createHeaderView];
    
    // Add more button and Share button
    UIBarButtonItem *shareButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(shareButtonTapped:)];
    
    UIBarButtonItem *addMoreColumnItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addMoreColumnsTapped:)];
    self.navigationItem.rightBarButtonItems = @[shareButton, addMoreColumnItem];

    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setBarTintColor:APP_COLOR];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes: @{
       NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:AVENIR_FONT size:12]
       }
     ];
    
    // This is temporary webView it will not visible anywhere
    // Create webview - Temporary!
    _tempWebView = [[UIWebView alloc] init];
    self.tempWebView.delegate = self;
    self.tempWebView.hidden = YES;

    _tableWebView = [[WKWebView alloc] init];
    self.tableWebView.navigationDelegate = self;
    self.tableWebView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.tableWebView.scrollView setScrollEnabled:NO];
    [self.tableWebView.scrollView setBounces:NO];
    self.tableWebView.hidden = YES;
    [self.tableView addSubview:self.tableWebView];
    
    NSDictionary *views = @{@"webView" : self.tableWebView};
    
    [self.tableView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[webView]|" options: 0 metrics:nil views:views]];
    [self.tableView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[webView]|" options: 0 metrics:nil views:views]];
    
    // WebView height constraints = this will allow dynamic height for chart
    _webViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.tableWebView
                                                            attribute:NSLayoutAttributeHeight
                                                            relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                               toItem:nil
                                                            attribute:NSLayoutAttributeNotAnAttribute
                                                           multiplier:1.0
                                                             constant:200];
    self.webViewHeightConstraint.priority = 900;
    [self.tableView addConstraint:self.webViewHeightConstraint];
    
    // Call service to get data
    [self callWebServiceToGetTableData];
}

//---------------------------------------------------------------

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    // Remove files from temporary folder
    [self removeFilesFromTempFolder];
}

//---------------------------------------------------------------

// In a Storyboard-based application, you will often want to do a little preparation before navigation
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showAddMoreView"]) {
        // Show list of great jobs on
//        UINavigationController *navController =  (UINavigationController *)[segue destinationViewController];
        AddColumnsController *controller = (AddColumnsController *)[segue destinationViewController];
        controller.filter = self.filter;
        controller.delegate = self;
        controller.itemId = GreatJobFilter;
        controller.columnList = self.columnList;
        controller.multiSelectionArray = self.multiSelection;
    }
}

//---------------------------------------------------------------

@end
