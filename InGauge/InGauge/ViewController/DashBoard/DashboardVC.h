//
//  DashboardVC.h
//  InGauge
//
//  Created by Mehul Patel on 19/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "DashboardModel.h"

@interface DashboardVC : BaseViewController

@property (nonatomic, strong) Filters                   *filter;
@property (nonatomic, strong) DashboardModel            *selectedDashboard;

@end
