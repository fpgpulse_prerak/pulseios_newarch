//
//  DashboardVC.m
//  InGauge
//
//  Created by Mehul Patel on 19/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "DashboardVC.h"
#import "DashboardDetails.h"
#import "DateFilterVC.h"
#import "SLButton.h"
#import "SlideNavigationController.h"
#import "LGRefreshView.h"
#import "ChartCell.h"
#import "ChartDetailsViewController.h"
#import "BlockCell.h"
#import "AnalyticsLogger.h"
#import "DashboardTableController.h"
#import "FilterController.h"
#import "NTMonthYearPicker.h"

#define LOCATION_FILTER                 @"TenantLocation"
#define LOCATION_GROUP_FILTER           @"LocationGroup"
#define PRODUCT_FILTER                  @"Product"
#define USER_FILTER                     @"User"
#define SHARE_TAG                       9000

static NSString *chartCellIdentifier    = @"ChartCell";
static NSString *blockCellIdentifier    = @"BlockCell";

@interface DashboardVC () <DateFilterDelegate, UICollectionViewDelegate, UICollectionViewDataSource, FilterDelegate>

@property (nonatomic, weak) IBOutlet UIView             *topView;
@property (nonatomic, weak) IBOutlet UILabel            *dateText;
@property (nonatomic, weak) IBOutlet UILabel            *comDateText;
@property (nonatomic, weak) IBOutlet UILabel            *monthYearLabel;
@property (nonatomic, weak) IBOutlet SLButton           *filterButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem    *menuButton;
@property (nonatomic, weak) IBOutlet UICollectionView   *collectionView;

@property (nonatomic, strong) UIView                    *noRecordsView;
@property (nonatomic, strong) UILabel                   *navigationLabel;
@property (nonatomic, strong) UILabel                   *topLabel;
@property (nonatomic, strong) LGRefreshView             *refreshView;
@property (nonatomic, strong) NTMonthYearPicker         *datePickerView;
@property (nonatomic, strong) DashboardListModel        *dashboardList;
@property (nonatomic, strong) RegionTypeList            *regionTypeList;
@property (nonatomic, strong) RegionList                *regionList;
@property (nonatomic, strong) LocationList              *locationList;
@property (nonatomic, strong) LocationGroupList         *locationGroupList;
@property (nonatomic, strong) ProductList               *productList;
@property (nonatomic, strong) UserList                  *userList;
@property (nonatomic, strong) DashboardDetails          *dbDetails;
@property (nonatomic, strong) CustomReport              *selectedReport;
@property (nonatomic, strong) FilterListDetails         *filterDetails;
@property (nonatomic, strong) NSArray                   *chartArray;
@property (nonatomic, strong) NSArray                   *filterArray;
@property (nonatomic, strong) NSArray                   *filterTypes;
@property (nonatomic, strong) NSString                  *selectedDashboardType;

@property (nonatomic, assign) BOOL                      isRegion;
@property (nonatomic, assign) BOOL                      isLocation;
@property (nonatomic, assign) BOOL                      isShowGoalProducts;
@property (nonatomic, assign) BOOL                      isCallServiceBeforeFilters;
@property (nonatomic, assign) BOOL                      isProductCallFinish;
@property (nonatomic, assign) BOOL                      isUserCallFinish;
@end

@implementation DashboardVC

//---------------------------------------------------------------

#pragma mark -
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc {
    // Remove delegates, mutable array and observers here!
    self.collectionView.dataSource = nil;
    self.collectionView.delegate = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SlideNavigationControllerDidClose object:nil];
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark FilterDelegate methods

//---------------------------------------------------------------

- (void) reloadItemsBasedOnFilters:(Filters *)filter withFilterItems:(NSArray *)filterItems {
    // Update stored filters
    [ServicePreference saveCustomObject:filter key:KEY_STORE_FILTERS_DASHBOARD];

    // Update filters, UI and call service
    self.filter = filter;
    self.filterArray = filterItems;
    [self updateTopLabel];
    [self reloadDashboardDetails];
    
    // Analytics
    [AnalyticsLogger logDashboardFilterApplyEvent:self];
}

//---------------------------------------------------------------

- (void) updateFilterArray:(NSArray *)updatedArray forFilterType:(FilterType)filterType {
    switch (filterType) {
        case LocationFilter: { self.locationList.locationList = updatedArray; break; }
        case LocationGroupFilter: { self.locationGroupList.locationGroupList = updatedArray; break; }
        case ProductFilter: { self.productList.productList = updatedArray; break; }
        case UserFilter: { self.userList.userList = updatedArray; break; }
        default: break;
    }
}

//---------------------------------------------------------------

- (void) dashboardChange {
    self.isCallServiceBeforeFilters = NO;
    if (self.isCallServiceBeforeFilters) {
        [self callWebServiceToGetDashboardDetails];
        [self callWebServiceToGetFiltersInBackground:YES];
    } else {
        [self callWebServiceToGetFiltersInBackground:NO];
    }
}

//---------------------------------------------------------------

- (void) reloadDashboardDetails {
    // Set detes to filters
    self.filter.startDate = [NSDate dateFromNSString:[ServicePreference objectForKey:KStartDate] withFormat:SERVICE_DATE_TIME_SHORT_FORMAT];
    self.filter.endDate = [NSDate dateFromNSString:[ServicePreference objectForKey:KEndDate] withFormat:SERVICE_DATE_TIME_SHORT_FORMAT];
    self.filter.compareStartDate = [NSDate dateFromNSString:[ServicePreference objectForKey:KComStartDate] withFormat:SERVICE_DATE_TIME_SHORT_FORMAT];
    self.filter.compareEndDate = [NSDate dateFromNSString:[ServicePreference objectForKey:KComEndDate] withFormat:SERVICE_DATE_TIME_SHORT_FORMAT];
    self.filter.isCompare = [ServicePreference objectForKey:KComSwitch];
    
    // Change date label
    self.dateText.text = [NSString stringWithFormat:@"%@ To %@", [ServicePreference objectForKey:KStartDate],[ServicePreference objectForKey:KEndDate]];
    self.comDateText.text = ([[ServicePreference objectForKey:KComSwitch] isEqualToString:@"true"]) ? [NSString stringWithFormat:@"%@ To %@", [ServicePreference objectForKey:KComStartDate],[ServicePreference objectForKey:KComEndDate]] : @"";
    
    // Call service to get dashboard details
    [self callWebServiceToGetDashboardDetails];
    
    // Analytics
    [AnalyticsLogger logDashboardDateChangeEvent:self];
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark NotificationCenter methods

//---------------------------------------------------------------

- (void) realoadDashboardList {
    // Call background services
    [self callFilterWebServicesInBackground];
    [self reloadDashboardDetails];
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) tenantChangeButtonTapped:(id)sender {
    [self changeTenantAndIndustry];
}

//---------------------------------------------------------------

- (IBAction) openMenu:(id)sender {
    [[SlideNavigationController sharedInstance] toggleRightMenu];
}

//---------------------------------------------------------------

- (IBAction) filterButtonTapped:(UIButton *)sender {
    if (self.filterButton.isEnabled) {
        FilterController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterController"];
        controller.delegate = self;
        
        // NOTE: Here I encounterd with "Shallow" copy problem of collection, because of that
        // array of this class is modified by next class.
        // To over come from this issue --> Use "Deep" copy
        // You can find out more --https://stackoverflow.com/questions/184710/what-is-the-difference-between-a-deep-copy-and-a-shallow-copy
        NSArray *deepCopyArray = [[NSArray alloc] initWithArray:self.filterArray copyItems:YES];
        controller.filterItems = deepCopyArray;
        Filters *tempFilters = self.filter.copy;
        controller.filter = tempFilters;
        controller.openFrom = OpenFromDashboard;
        controller.isShowGoalProducts = self.isShowGoalProducts;
        [self presentViewController:controller animated:YES completion:^{}];
    }
}

//---------------------------------------------------------------

- (IBAction) monthAndYearSelectionButtonTapped:(id)sender {
    if (self.filterDetails.dashboard.isMTD.boolValue) {
        // Show month date picker
        [self showDatePicker];
    } else {
        DateFilterVC *controller = (DateFilterVC *)[self.storyboard instantiateViewControllerWithIdentifier:@"DateFilterVC"];
        controller.isShowCompareDate = YES;
        controller.delegate = self;
        controller.isShowCompareDate = !self.filterDetails.dashboard.hideCompareDate.boolValue;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark WEB SERVICE methods

//---------------------------------------------------------------

- (void) callWebServiceToGetDashboardList {
    // Check for internet connnection
    if (![Helper checkForConnection:self]) {
        return;
    }
    
    MBProgressHUD *hud = [Helper showLoading:self.navigationController.view];
    @try {
        NSDictionary *params = @{
                                 @"tenantId" : App_Delegate.appfilter.tenantId,
                                 @"userId" : [User signedInUser].userID,
                                 @"getSection" : [NSNumber numberWithBool:NO],
                                 @"roleId" : App_Delegate.appfilter.userRoleId
                                 };
        
        NSString *urlString = [NSString stringWithFormat:@"%@", GET_DASHBOARD_LIST_ACTION];
        DLog(@"\n\n\n\n==========>>> Get Dashboard list - Service Call Begin <<<==========\n\n\n\n");
        [ServiceManager getWebServiceCall:urlString parameters:params whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
            DLog(@"\n\n\n\n==========>>> Get Dashboard list - Service Call finish <<<==========\n\n\n\n");
            [Helper hideLoading:hud];
            
            _dashboardList = [DashboardListModel new];
            @try {
                if (response) {
                    [HKJsonUtils updatePropertiesWithData:response forObject:self.dashboardList];
                }
                
                if (self.dashboardList != nil && self.dashboardList.dashboardList.count > 0) {
                    [self.noRecordsView setHidden:YES];
                    self.filterButton.enabled = YES;
                    // Create dashboard list array
                    [self dashboardListSettings];
                    
                    // Call service to get filters
                    if (self.isCallServiceBeforeFilters) {
                        [self callWebServiceToGetDashboardDetails];
                        [self callWebServiceToGetFiltersInBackground:YES];
                    } else {
                        [self callWebServiceToGetFiltersInBackground:NO];
                    }
                } else {
                    self.filterButton.enabled = NO;
                    [self.noRecordsView setHidden:NO];
                }
                
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
            }
        }];
    } @catch (NSException *exception) {
        [Helper hideLoading:hud];
        DLog(@"Exception :%@", exception.debugDescription);
        
        if (self.dashboardList.dashboardList.count > 0) {
            [self.noRecordsView setHidden:YES];
        } else {
            [self.noRecordsView setHidden:NO];
        }
    }
}

//---------------------------------------------------------------

- (void) callWebServiceToGetFiltersInBackground:(BOOL)isBackground {
    MBProgressHUD *hud = nil;
    if (!isBackground) {
        hud = [Helper showLoading:self.view];
    }
    [ServiceManager callWebServiceToGetFilterType:self.selectedDashboard.dashboardId usingBlock:^(FilterListDetails *filterDetail) {
        if (!isBackground) {
            [Helper hideLoading:hud];
        }
        self.filterDetails = filterDetail;
        self.filterTypes = filterDetail.filters;
        
        // Decide that whether to show Month-Date picker or Date range
        self.comDateText.hidden = NO;
        self.dateText.hidden = NO;
        self.monthYearLabel.hidden = YES;
        self.filter.month = nil;
        self.filter.year = nil;

        if (self.filterDetails.dashboard.isMTD.boolValue) {
            // Show month date picker
            self.comDateText.hidden = YES;
            self.dateText.hidden = YES;
            self.monthYearLabel.hidden = NO;
            
            // Get month and year
            NSDate *currentDate = [NSDate date];
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:currentDate];
            self.filter.month = [NSNumber numberWithInteger:components.month];
            self.filter.year = [NSNumber numberWithInteger:components.year];

            // Check if defaults are there...
            Filters *storedFilters = [ServicePreference customObjectWithKey:KEY_STORE_FILTERS_DASHBOARD];
            self.filter.month = (storedFilters.month) ? storedFilters.month : self.filter.month;
            self.filter.year = (storedFilters.year) ? storedFilters.year : self.filter.year;
            
            // Get month and year
            NSString *month = [NSString stringWithFormat:@"%@: %@",[App_Delegate.keyMapping getKeyValue:@"KEY_TITLE_MONTH"], [Helper getMonthNameFromMonthNumber:self.filter.month.integerValue]];
            NSString *year = [NSString stringWithFormat:@"%@: %@",[App_Delegate.keyMapping getKeyValue:@"KEY_TITLE_YEAR"] , self.filter.year];
            self.monthYearLabel.text = [NSString stringWithFormat:@"%@    |    %@", month, year];
        }
        
        self.comDateText.text = [NSString stringWithFormat:@"%@ To %@", [ServicePreference objectForKey:KComStartDate],[ServicePreference objectForKey:KComEndDate]];

        // Hide compare date if not avaialable in filters
        if (self.filterDetails.dashboard.hideCompareDate.boolValue) {
            self.comDateText.text = @"";
        }
        // Call Dashboard Chart detail web service
        [self createFilters];
    }];
}

//---------------------------------------------------------------

- (void) callWebServiceToGetDashboardDetails {
    // Check for internet connnection
    if (![Helper checkForConnection:self]) {
        return;
    }
    
    NSString *startDateString = [ServicePreference objectForKey:KStartDate];
    NSString *endDateString = [ServicePreference objectForKey:KEndDate];
    NSString *compareStartDateString = [ServicePreference objectForKey:KComStartDate];
    NSString *compareEndDateString = [ServicePreference objectForKey:KComEndDate];
    
    if ([[ServicePreference objectForKey:KComSwitch] isEqualToString:@"false"]) {
        compareStartDateString = startDateString;
        compareEndDateString = endDateString;
    }
    
    self.filter.startDate = [NSDate dateFromNSString:startDateString withFormat:APP_DISPLAY_DATE_TIME_FORMAT];
    self.filter.endDate = [NSDate dateFromNSString:endDateString withFormat:APP_DISPLAY_DATE_TIME_FORMAT];
    self.filter.compareStartDate = [NSDate dateFromNSString:compareStartDateString withFormat:APP_DISPLAY_DATE_TIME_FORMAT];
    self.filter.compareEndDate = [NSDate dateFromNSString:compareEndDateString withFormat:APP_DISPLAY_DATE_TIME_FORMAT];
    self.filter.isCompare = [ServicePreference objectForKey:KComSwitch];

    // Store in dashboard filters
    [ServicePreference saveCustomObject:self.filter key:KEY_STORE_FILTERS_DASHBOARD];
    
    NSDictionary *details = [self.filter serializeFiltersForChartDataURL];

    // change dates from MTD dashboard
    if (self.filterDetails.dashboard.isMTD.boolValue) {
        NSMutableDictionary *tempDetails = [NSMutableDictionary dictionaryWithDictionary:details];
        // Change start and end date to selected picker
        NSDate *dateFromMonthAndYear = [Helper getDateFromMonth:self.filter.month andYear:self.filter.year];
        // startDate - Start of month
        // endDate - End of month
        NSDate *startDate = [Helper firstDateOfMonth:dateFromMonthAndYear];
        NSDate *endDate = [Helper lastDateOfMonth:dateFromMonthAndYear];
        if (startDate != nil) {
            [tempDetails setValue:[startDate toNSString:SERVICE_DATE_TIME_SHORT_FORMAT] forKey:KEY_DATE_FROM];
        }
        if (endDate != nil) {
            [tempDetails setValue:[endDate toNSString:SERVICE_DATE_TIME_SHORT_FORMAT] forKey:KEY_DATE_TO];
        }
        
        // update details
        details = [NSDictionary dictionaryWithDictionary:tempDetails];
        tempDetails = nil;
    }
    
    // Hide compare details
    if (self.filterDetails.dashboard.hideCompareDate.boolValue) {
        NSMutableDictionary *tempDetails = [NSMutableDictionary dictionaryWithDictionary:details];
        [tempDetails setObject:@"false" forKey:@"isCompare"];
        // update details
        details = [NSDictionary dictionaryWithDictionary:tempDetails];
        tempDetails = nil;
    }
    
    MBProgressHUD *hud = [Helper showLoading:self.view];
    [ServiceManager callWebServiceToGetChartData:details dashboardId:self.selectedDashboard.dashboardId usingBlock:^(NSError *error, ChartListModel *chartList) {
        [Helper hideLoading:hud];

        self.chartArray = chartList.reportList;
        self.noRecordsView.hidden = (chartList.reportList.count > 0) ? YES : NO;
        [self.collectionView reloadData];
    }];
}

//---------------------------------------------------------------

- (void) callFilterWebServicesInBackground {
    
    [self.filterButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [self.filterButton showLoading];
    
    self.filter.accessible = [NSNumber numberWithBool:NO];
    self.filterButton.enabled = NO;
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    //UPDATE: 10-Oct-2017 ~ Call services based on filters
    Filters *storedFilters = [ServicePreference customObjectWithKey:KEY_STORE_FILTERS_DASHBOARD];

    BOOL isBack = (self.isCallServiceBeforeFilters) ? YES : NO;
    
    // Reset filters
    [self resetFilterIds];
    
    if (self.isRegion) {
        // Get Region type
        [self getRegionType:storedFilters isBackground:isBack];
    } else if (self.isLocation) {
        self.filter.regionId = nil;
        // Get Locations
        [self getLocations:storedFilters isBackground:isBack];
    } else {
        [self getRegionType:storedFilters isBackground:isBack];
    }
}

//---------------------------------------------------------------

- (void) getRegionType:(Filters *)storedFilters isBackground:(BOOL)isBackground {
    MBProgressHUD *hud = nil;
    if (!isBackground) {
        hud = [Helper showLoading:self.view];
    }
    
    [ServiceManager callWebServiceToGetRegionType:self.filter usingBlock:^(RegionTypeList *regionTypeList) {
        if (!isBackground) {
            [Helper hideLoading:hud];
        }
        
        self.regionTypeList = regionTypeList;
        // Get regions
        if (self.regionTypeList.regionTypeList.count > 0) {                    
            NSMutableArray *geoArray = [NSMutableArray new];
            // Add geography types
            for (int index = 0; index < self.regionTypeList.regionTypeList.count; index++) {
                RegionType *regionType = [self.regionTypeList.regionTypeList objectAtIndex:index];
                RefineItem *item = [RefineItem new];
                item.itemId = regionType.regionTypeId;
                item.regionTypeId = regionType.regionTypeId;
                item.title = regionType.name;
                item.subItems = [App_Delegate.appfilter.geoItems objectAtIndex:regionType.level.integerValue];
                item.isEnable = NO;
                
                if (regionType.level.integerValue == 0) {
                    if (item.subItems.count > 0) {
                        GeographyModel *model = (GeographyModel *)item.subItems[0];
                        item.displayName = model.region.name;
                        item.regionTypeId = regionType.regionTypeId;
                        self.filter.regionId = model.region.regionId;
                        self.filter.regionName = model.region.name;
                        self.filter.regionTypeId = regionType.regionTypeId;
                        self.filter.regionTypeName = regionType.name;
                    }
                }
                
                // update levels
                if (storedFilters.selectedRegions.count > regionType.level.integerValue) {
                    GeographyModel *model = (GeographyModel *)[storedFilters.selectedRegions objectAtIndex:regionType.level.integerValue];
                    item.displayName = model.region.name;
                    item.regionTypeId = regionType.regionTypeId;
                    self.filter.regionId = model.region.regionId;
                    self.filter.regionName = model.region.name;
                    self.filter.regionTypeId = regionType.regionTypeId;
                    self.filter.regionTypeName = regionType.name;
                } else {
                    // Get sub-regions
                    if (regionType.level.integerValue > 0) {
                        NSArray *subRegions = [App_Delegate.appfilter getGeoSubItems:self.filter.regionId tolevel:index];
                        item.subItems = subRegions;                        
                    }
                }
                
                // First two items must be enable for selection, first is always selected!
                if (index == 0 || index == 1) {
                    item.isEnable = YES;
                }
                item.filterType = GeographyType;
                [geoArray addObject:item];
            }
            
            // Update filter array - Please do not remove geoArray
            //NOTE: Here two mutable array are required to maintaing filter order
            NSMutableArray *tempArray = self.filterArray ? [NSMutableArray arrayWithArray:self.filterArray] : [NSMutableArray new];
            [tempArray replaceObjectsInRange:NSMakeRange(0,0)
                            withObjectsFromArray:geoArray];
            self.filterArray = [NSArray arrayWithArray:tempArray];
            tempArray = nil;
            geoArray = nil;
        }
                
        // Check if location filter is available
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF = %@", LOCATION_FILTER];
        NSArray *array = [self.filterTypes filteredArrayUsingPredicate:predicate1];
        
        // Set default metric type - when locations are not available
        // UPDATE: 18 Dec 2017 ~ Default metric is now "Departure"
        self.filter.metricTypeName = [Helper getMetricType:@"Departure"];
        // Update metric type name here...
        RefineItem *mtericItem = [self.filterArray lastObject];
        if (mtericItem && mtericItem.filterType == MetricTypeFilter) {
            mtericItem.displayName = self.filter.metricTypeName;
            [self updateFilterItem:mtericItem];
        }
        
        if (array.count > 0) {
            // Call service to get locations
            [self getLocations:storedFilters isBackground:isBackground];
        } else {
            self.filter.locationId = nil;
            self.filter.locationName = @"";
            
            [self updateTopLabel];
            // Get dashboard details
            [self earlyExitAndGetDashboardDetails];
        }
    }];
}

//---------------------------------------------------------------

- (void) getLocations:(Filters *)storedFilters isBackground:(BOOL)isBackground {
    MBProgressHUD *hud = nil;
    if (!isBackground) {
        hud = [Helper showLoading:self.view];
    }

    NSDictionary *details = [self.filter serializeFiltersForLocationService];
    [ServiceManager callWebServiceToGetLocations:details usingBlock:^(LocationList *locationList) {
        if (!isBackground) {
            [Helper hideLoading:hud];
        }
        self.locationList = locationList;
        
        // Get Location
        if (self.locationList.locationList.count > 0) {
            // First location
            Location *location = [self.locationList.locationList objectAtIndex:0];
            
            // check if location exist in current location array?
            NSPredicate *locPredicate = [NSPredicate predicateWithFormat:@"SELF.locationId.intValue = %d", storedFilters.locationId.intValue];
            NSArray *filterLocation = [self.locationList.locationList filteredArrayUsingPredicate:locPredicate];
            if (filterLocation.count > 0) {
                location = (Location *)filterLocation[0];
            }
            
            self.filter.locationId = location.locationId;
            self.filter.locationName = location.name;
            self.filter.metricTypeName = [Helper getMetricType:location.hotelMetricsDataType];
            
            // Update metric type name here...
            RefineItem *mtericItem = [self.filterArray lastObject];
            if (mtericItem && mtericItem.filterType == MetricTypeFilter) {
                mtericItem.displayName = self.filter.metricTypeName;
                [self updateFilterItem:mtericItem];
            }
        }
        
        // update filter item
        RefineItem *item = [RefineItem new];
        item.itemId = self.filter.locationId;
        item.displayName = self.filter.locationName;
        item.subItems = self.locationList.locationList;
        item.filterType = LocationFilter;
        [self updateFilterItem:item];
        
        // Update top details
        [self updateTopLabel];

        // Check if location group filter is available
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF = %@", LOCATION_GROUP_FILTER];
        NSArray *array = [self.filterTypes filteredArrayUsingPredicate:predicate1];

        if (array.count > 0) {
            // Call service to get location group
            [self getLocationGroup:storedFilters isBackground:isBackground];
        } else {
            self.filter.locationGroupId = nil;
            // Get dashboard details
            [self earlyExitAndGetDashboardDetails];
        }
    }];
}

//---------------------------------------------------------------

- (void) getLocationGroup:(Filters *)storedFilters isBackground:(BOOL)isBackground {
    MBProgressHUD *hud = nil;
    if (!isBackground) {
        hud = [Helper showLoading:self.view];
    }

    [ServiceManager callWebServiceToGetLocationGroup:self.filter.locationId usingBlock:^(LocationGroupList *locationGroupList) {
        if (!isBackground) {
            [Helper hideLoading:hud];
        }
        
        self.locationGroupList = locationGroupList;
        // Get location group
        if (self.locationGroupList.locationGroupList.count > 0) {
            // First location group
            LocationGroup *locationGroup = [self.locationGroupList.locationGroupList objectAtIndex:0];
            self.filter.locationGroupId = locationGroup.locationGroupID;
            self.filter.getContributorsOnly = [NSNumber numberWithBool:YES];
            self.filter.locationGroupName = locationGroup.locationGroupName;
        }
        
        // update filter item
        RefineItem *item = [RefineItem new];
        item.itemId = self.filter.locationGroupId;
        item.displayName = self.filter.locationGroupName;
        item.subItems = self.locationGroupList.locationGroupList;
        item.filterType = LocationGroupFilter;
        [self updateFilterItem:item];
        
        // Check if product filter is available
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF = %@", PRODUCT_FILTER];
        NSArray *array1 = [self.filterTypes filteredArrayUsingPredicate:predicate1];
        
        self.isProductCallFinish = YES;
        self.isUserCallFinish = YES;
        self.filter.productId = nil;
        self.filter.userId = nil;
        
        if (array1.count > 0) {
            self.isProductCallFinish = NO;
            // Get Product list
            [self getProducts:storedFilters isBackground:isBackground];
        }
        
        // Check if user filter is available
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"SELF = %@", USER_FILTER];
        NSArray *array2 = [self.filterTypes filteredArrayUsingPredicate:predicate2];
        if (array2.count > 0) {
            self.isUserCallFinish = NO;
            // Get Product list
            [self getUsers:storedFilters isBackground:isBackground];
        }
        
        if (array1.count == 0 && array2.count == 0) {
            // Get dashboard details
            [self earlyExitAndGetDashboardDetails];
        }
    }];
}

//---------------------------------------------------------------

- (void) getProducts:(Filters *)storedFilters isBackground:(BOOL)isBackground {
    MBProgressHUD *hud = nil;
    if (!isBackground) {
        hud = [Helper showLoading:self.navigationController.view];
    }

    [ServiceManager callWebServiceToGetProducts:self.filter usingBlock:^(ProductList *productList) {
        if (!isBackground) {
            [Helper hideLoading:hud];
        }
        self.isProductCallFinish = YES;
        self.productList = productList;
        
        // If there is CustomGoalFilter than show "Incremental Revenue" and "Upsell products"
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF = %@", @"CustomGoalFilter"];
        NSArray *array = [self.filterTypes filteredArrayUsingPredicate:predicate];
        
        self.isShowGoalProducts = NO;
        if (array.count > 0) {
            self.isShowGoalProducts = YES;
            NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.productList.productList];
            
            //***NOTE: Add static product "Incremental Revenue" to get all details
            Product *incrementalRevenueProduct = [Product new];
            incrementalRevenueProduct.tenantProductName = [App_Delegate.keyMapping getKeyValue:@"KEY_TITLE_ARPD"];                    incrementalRevenueProduct.productId = [NSNumber numberWithInt:-1];
            [tempArray insertObject:incrementalRevenueProduct atIndex:0];
            
            // Only for Hospitality industry
            if (App_Delegate.appfilter.industryId.integerValue == 1) {
                //UPDATE:08 Sep 2017 - Add Second static product "Upsell Products" (https://fpgpulse.atlassian.net/browse/PA-126)
                Product *upsellProduct = [Product new];
                upsellProduct.tenantProductName = [App_Delegate.keyMapping getKeyValue:@"KEY_UPSELL_PRODUCTS"];
                upsellProduct.productId = [NSNumber numberWithInt:-2];
                [tempArray insertObject:upsellProduct atIndex:1];
            }
            self.productList.productList = [NSArray arrayWithArray:tempArray];
            tempArray = nil;
        }
        
        if (self.productList.productList.count > 0) {
            Product *product = [self.productList.productList objectAtIndex:0];
            self.filter.productId = product.productId;
            self.filter.productName = product.tenantProductName;
        }
        
        // update filter item
        RefineItem *item = [RefineItem new];
        item.itemId = self.filter.productId;
        item.displayName = self.filter.productName;
        item.subItems = self.productList.productList;
        item.filterType = ProductFilter;
        [self updateFilterItem:item];

        // update filter button status
        [self updateUI];
        
        if (!self.isCallServiceBeforeFilters) {
            if (self.isUserCallFinish) {
                self.isCallServiceBeforeFilters = YES;
                [self callWebServiceToGetDashboardDetails];
            }
        }
    }];
}

//---------------------------------------------------------------

- (void) getUsers:(Filters *)storedFilters isBackground:(BOOL)isBackground {
    MBProgressHUD *hud = nil;
    if (!isBackground) {
        hud = [Helper showLoading:self.view];
    }
    
    // call service to get available users
    [ServiceManager callWebServiceToGetAvailabelUsers:self.filter usingBlock:^(UserList *userList) {
        if (!isBackground) {
            [Helper hideLoading:hud];
        }
        self.isUserCallFinish = YES;
        self.userList = userList;
        
        // First User
        if (self.userList.userList.count > 0) {
            User *user = [self.userList.userList objectAtIndex:0];
            self.filter.userId = user.userID;
            self.filter.userName = user.name;
        }

        // update filter button status
        [self updateUI];
        
        // update filter item
        RefineItem *item = [RefineItem new];
        item.itemId = self.filter.userId;
        item.displayName = self.filter.userName;
        item.subItems = self.userList.userList;
        item.filterType = UserFilter;
        [self updateFilterItem:item];
        
        if (!self.isCallServiceBeforeFilters) {
            if (self.isProductCallFinish) {
                self.isCallServiceBeforeFilters = YES;
                [self callWebServiceToGetDashboardDetails];
            }
        }
    }];
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark Custom methods

//---------------------------------------------------------------

- (BOOL) isCompareValue {
    if (self.filterDetails.dashboard.isMTD.boolValue) {
        return NO;
    }
    return ([self.filter.isCompare isEqualToString:@"true"]) ? YES : NO;
}

//---------------------------------------------------------------

- (void) showDatePicker {
    if (!self.datePickerView) {
        _datePickerView = [[NTMonthYearPicker alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 240)];
    }
    
    NSDate *dateFromMonthAndYear = [Helper getDateFromMonth:self.filter.month andYear:self.filter.year];
    self.datePickerView.date = dateFromMonthAndYear;
    
    NSString *title = UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation) ? @"\n\n\n\n\n\n\n\n\n" : @"\n\n\n\n\n\n\n\n\n\n\n\n" ;
    
    UIAlertController* datePickerContainer = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [datePickerContainer.view addSubview:self.datePickerView];
    
    [datePickerContainer addAction:[UIAlertAction actionWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_TITLE_CANCEL"] style:UIAlertActionStyleCancel handler:^(UIAlertAction* action) {
    }]];
    
    [datePickerContainer addAction:[UIAlertAction actionWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_SELECT"] style:UIAlertActionStyleDefault handler:^(UIAlertAction* action) {
        // Change month and year filter
        self.filter.month = [NSNumber numberWithInteger:[Helper getMonthFromDate:self.datePickerView.date]];
        self.filter.year = [NSNumber numberWithInteger:[Helper getYearFromDate:self.datePickerView.date]];

        NSString *month = [NSString stringWithFormat:@"%@: %@",[App_Delegate.keyMapping getKeyValue:@"KEY_TITLE_MONTH"], [Helper getMonthNameFromMonthNumber:self.filter.month.integerValue]];
        NSString *year = [NSString stringWithFormat:@"%@: %@",[App_Delegate.keyMapping getKeyValue:@"KEY_TITLE_YEAR"] , self.filter.year];
        
        self.monthYearLabel.text = [NSString stringWithFormat:@"%@    |    %@", month, year];
        
        // Call service
        [self callWebServiceToGetDashboardDetails];
    }]];
    
    [self presentViewController:datePickerContainer animated:YES completion:nil];
}

//---------------------------------------------------------------

- (void) dateSettings {
    // Set default date : Current month and year
    NSDate *endDate = [NSDate date];
    NSDate *startDate = [Helper firstDateOfMonth:endDate];
    NSString *startDateString = [startDate toNSString:APP_DISPLAY_DATE_TIME_FORMAT];
    NSString *endDateString = [endDate toNSString:APP_DISPLAY_DATE_TIME_FORMAT];
    
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setMonth:-1]; // note that I'm setting it to -1
    NSDate *compareEndDate = [[NSCalendar currentCalendar] dateByAddingComponents:offsetComponents toDate:endDate options:0];
    
    NSDateComponents *dayComponents = [[NSDateComponents alloc] init];
    [dayComponents setDay:-30];
    NSDate *compareStartDate = [[NSCalendar currentCalendar] dateByAddingComponents:offsetComponents toDate:startDate options:0];
    NSString *compStartDateString = [compareStartDate toNSString:APP_DISPLAY_DATE_TIME_FORMAT];
    NSString *compEndDateString = [compareEndDate toNSString:APP_DISPLAY_DATE_TIME_FORMAT];

    // preference dates
    NSString *prefStartDate = [ServicePreference objectForKey:KStartDate];
    NSString *prefEndDate = [ServicePreference objectForKey:KEndDate];
    
    if (prefStartDate == nil) {
        self.dateText.text = [NSString stringWithFormat:@"%@ To %@", startDateString,endDateString];
        [ServicePreference setObject:startDateString forKey:KStartDate];
        [ServicePreference setObject:endDateString forKey:KEndDate];
        [ServicePreference setObject:compStartDateString forKey:KComStartDate];
        [ServicePreference setObject:compEndDateString forKey:KComEndDate];
        [ServicePreference setObject:@"true" forKey:KComSwitch];
    } else {
        
        // if "startDate" is first day of month and "endDate" is yesterday than change "endDate" to current day
        NSDateFormatter *formatter = [NSDateFormatter new];
        formatter.dateFormat = APP_DISPLAY_DATE_TIME_FORMAT;
        NSDateComponents *dayComponents2 = [[NSDateComponents alloc] init];
        [dayComponents2 setHour:-24];
        [dayComponents2 setMinute:0];
        [dayComponents2 setSecond:0];

        NSDate *yesterday = [[NSCalendar currentCalendar] dateByAddingComponents:dayComponents2 toDate:[NSDate date] options:0];
        NSString *yesterdayString = [formatter stringFromDate:yesterday];
        NSDate *firstDayOfMonth = [Helper firstDateOfMonth:[NSDate date]];
        NSString *firstDayString = [formatter stringFromDate:firstDayOfMonth];

        if ([prefEndDate isEqualToString:yesterdayString] && [prefStartDate isEqualToString:firstDayString]) {
            // change end date
            prefEndDate = [formatter stringFromDate:[NSDate date]];
            [ServicePreference setObject:prefEndDate forKey:KEndDate];
        }
        
        // Set date from preference
        self.dateText.text = [NSString stringWithFormat:@"%@ To %@", prefStartDate, prefEndDate];
        
        if ([[ServicePreference objectForKey:KComSwitch] isEqualToString:@"true"]) {
            self.comDateText.text = [NSString stringWithFormat:@"%@ To %@", [ServicePreference objectForKey:KComStartDate],[ServicePreference objectForKey:KComEndDate]];
        } else {
            self.comDateText.text = @"";
        }
    }
    
    // Always show compare on
    [ServicePreference setObject:@"true" forKey:KComSwitch];
    
    // Set detes to filters
    self.filter.startDate = [NSDate dateFromNSString:prefStartDate withFormat:SERVICE_DATE_TIME_SHORT_FORMAT];
    self.filter.endDate = [NSDate dateFromNSString:prefEndDate withFormat:SERVICE_DATE_TIME_SHORT_FORMAT];
    self.filter.compareStartDate = [NSDate dateFromNSString:[ServicePreference objectForKey:KComStartDate] withFormat:SERVICE_DATE_TIME_SHORT_FORMAT];
    self.filter.compareEndDate = [NSDate dateFromNSString:[ServicePreference objectForKey:KComEndDate] withFormat:SERVICE_DATE_TIME_SHORT_FORMAT];
    self.filter.isCompare = [ServicePreference objectForKey:KComSwitch];
}

//---------------------------------------------------------------

- (void) resetFilterIds {
    self.filter.regionTypeId = nil;
    self.filter.regionId = nil;
    self.filter.locationId = nil;
    self.filter.locationGroupId = nil;
    self.filter.productId = nil;
    self.filter.userId = nil;
    
    self.filter.locationName = @"";
    self.filter.locationGroupName = @"";
    self.filter.productName = @"";
    self.filter.userName = @"";
}

//---------------------------------------------------------------

- (void) earlyExitAndGetDashboardDetails {
    
    // update UI
    [self updateUI];
    
    // Call service to get details
    if (!self.isCallServiceBeforeFilters) {
        self.isCallServiceBeforeFilters = YES;
        [self callWebServiceToGetDashboardDetails];
    }
}

//---------------------------------------------------------------

- (void) updateUI {
    self.filterButton.enabled = YES;
    self.navigationItem.rightBarButtonItem.enabled = YES;
    
    [self.filterButton setImage:[UIImage imageNamed:@"filter"] forState:UIControlStateNormal];
    [self.filterButton hideLoading];
    
    // If there is no dashboard than disable filter button
    if (self.dashboardList != nil && self.dashboardList.dashboardList.count > 0) {
        self.filterButton.enabled = YES;
    } else {
        self.filterButton.enabled = NO;
    }
    
    // Update top labels
    [self updateTopLabel];
}

//---------------------------------------------------------------

- (void) dashboardListSettings {
    @try {
        // Create section to seperate dashboard items
        NSMutableArray *tempType1Array = [NSMutableArray new]; // Normal Dashboard
        NSMutableArray *tempType2Array = [NSMutableArray new]; // How am I doing
        NSMutableArray *tempType3Array = [NSMutableArray new]; // How am I improve
        
        for (DashboardModel *dashboard in self.dashboardList.dashboardList) {
            // Check for dashboard type
            if (dashboard.dashboardType.intValue == 1) {
                // Normal dashboard list
                [tempType1Array addObject:dashboard];
            }
            else if (dashboard.dashboardType.intValue == 2) {
                // "How am i doing" dashboard list
                [tempType2Array addObject:dashboard];
            }
            else if (dashboard.dashboardType.intValue == 3) {
                // "How am i improve" dashboard list
                [tempType3Array addObject:dashboard];
            }
            
            // Can be more dashbaord types in future.......
        }
        
        // create mapping
        NSDictionary *type1Details = @{ KEY_DASHBOARD_TYPE :  @"1", KEY_DASHBOARD_LIST : [NSArray arrayWithArray:tempType1Array]};
        NSDictionary *type2Details = @{ KEY_DASHBOARD_TYPE :  @"2", KEY_DASHBOARD_LIST : [NSArray arrayWithArray:tempType2Array]};
        NSDictionary *type3Details = @{ KEY_DASHBOARD_TYPE :  @"3", KEY_DASHBOARD_LIST : [NSArray arrayWithArray:tempType3Array]};
        // Manage order to show in table
        // 2-3-1
        NSMutableArray *tempArray = [NSMutableArray new];
        if (tempType2Array.count > 0) {
            [tempArray addObject:type2Details];
        }
        if (tempType3Array.count > 0) {
            [tempArray addObject:type3Details];
        }
        if (tempType1Array.count > 0) {
            [tempArray addObject:type1Details];
        }
        
        NSArray *dashboardList = [NSArray arrayWithArray:tempArray];
        
        // Selected dashboard
        NSDictionary *details = [dashboardList objectAtIndex:0];
        NSArray *dbList = [details objectForKey:KEY_DASHBOARD_LIST];
        _selectedDashboard = (DashboardModel *)[dbList objectAtIndex:0];
        [SlideNavigationController sharedInstance].selectedDashboard = _selectedDashboard;
        [SlideNavigationController sharedInstance].dashboardList = dashboardList;
        
        // Select Dashboard type
        NSString *type = [details objectForKey:KEY_DASHBOARD_TYPE];
        if ([type isEqualToString:@"1"]) {
            self.selectedDashboardType = DBTYPE_1_TITLE;
        } else if ([type isEqualToString:@"2"]) {
            self.selectedDashboardType = DBTYPE_2_TITLE;
        } else if ([type isEqualToString:@"3"]) {
            self.selectedDashboardType = DBTYPE_3_TITLE;
        }
        [SlideNavigationController sharedInstance].selectedDashboardType = self.selectedDashboardType;
        
        // Update navigation title based on selected dashboard
        [self updateTopLabel];
        
        tempArray = nil;
        tempType1Array = nil;
        tempType2Array = nil;
        tempType3Array = nil;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void) createPullToRefresh {
    // Pull to refresh ----
    __weak typeof(self) wself = self;
    _refreshView = [LGRefreshView refreshViewWithScrollView:self.collectionView refreshHandler:^(LGRefreshView *refreshView) {
        if (wself) {
            __strong typeof(wself) self = wself;
                            
            // Call Dashboard chart service
            [self reloadDashboardDetails];
                            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void) {
                [self.refreshView endRefreshing];
            });
        }
    }];
    _refreshView.tintColor = APP_COLOR;
}

//---------------------------------------------------------------

- (void) sideMenuClosed {
    if (_selectedDashboard.dashboardId.intValue != [SlideNavigationController sharedInstance].selectedDashboard.dashboardId.intValue) {
        
        // Scroll collectionView to top
        if (self.chartArray.count > 0) {
            [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
        }
        
        //Change Dashboard - Bharat
        _selectedDashboard = [SlideNavigationController sharedInstance].selectedDashboard;
        
        // Set selected dashbaord
        self.selectedDashboardType = [SlideNavigationController sharedInstance].selectedDashboardType;

        // Clear filter
        self.filterArray = nil;
        self.isRegion = NO;
        self.isLocation = NO;
        self.filter.locationId = nil;
        self.filter.productId = nil;
        self.filter.userId = nil;
        self.filter.locationName = @"";
        self.filter.productName = @"";
        self.filter.userName = @"";
        
        // hide all progress views
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        
        //Set dashboard title
        [self updateTopLabel];
        
        //API Call
        // Call Dashboard Chart detail web service
        [self dashboardChange];
        
        // Analytics
        [AnalyticsLogger logDashboardSelectDashboardEvent:self];
    }
}

//---------------------------------------------------------------

- (void) setupNotificationsForDashboard {
     // Side Menu - Notifications
    // This will be post from SlideNavigationController class when Side menu close
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sideMenuClosed) name:SlideNavigationControllerDidClose object:nil];
}

//---------------------------------------------------------------

- (void) setTopNavBarTitle {
    
    CGFloat width = self.view.frame.size.width - 100;
    UIView *navBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - 15, 44)];
    _navigationLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, width, 20)];
//    self.navigationLabel.text = [App_Delegate.keyMapping getKeyValue:@"KEY_DASHBOARD"];
    self.navigationLabel.textAlignment = NSTextAlignmentCenter;
    self.navigationLabel.font = [UIFont fontWithName:AVENIR_FONT size:14.0f];
    self.navigationLabel.textColor = [UIColor whiteColor];
//    self.navigationLabel.backgroundColor = [UIColor blueColor];
    [navBarView addSubview:self.navigationLabel];
    
    _topLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 25, width, 15)];
    self.topLabel.textAlignment = NSTextAlignmentCenter;
    self.topLabel.font = [UIFont fontWithName:AVENIR_FONT size:12.0f];
    self.topLabel.textColor = [UIColor whiteColor];
//    self.topLabel.backgroundColor = [UIColor greenColor];
    [navBarView addSubview:self.topLabel];
    
    // Add button to open filter
    UIButton *openFilterButton = [UIButton buttonWithType:UIButtonTypeSystem];
    openFilterButton.frame = CGRectMake(0, 0, self.view.frame.size.width - 70, 44);
    [openFilterButton addTarget:self action:@selector(filterButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [navBarView addSubview:openFilterButton];
//    [navBarView setBackgroundColor:[UIColor redColor]];
    self.navigationItem.titleView = navBarView;
}

//---------------------------------------------------------------

- (void) updateTopLabel {    
    NSString *dashboardName = [Helper removeWhiteSpaceAndNewLineCharacter: self.selectedDashboard.title];
    NSString *locationName = [Helper removeWhiteSpaceAndNewLineCharacter: [self getGeographyName]];
    NSString *userName = [Helper removeWhiteSpaceAndNewLineCharacter: [self getUserNameOrProductName]];

    if (locationName.length > 0) {
        userName = userName.length > 0 ? [NSString stringWithFormat:@"- %@", userName] : @"";
    }
    self.topLabel.text = [NSString stringWithFormat:@"%@ %@", locationName, userName];
    
    // update navigation label
//    self.navigationLabel.text = self.selectedDashboardType;
    self.navigationLabel.text = dashboardName;
}

//---------------------------------------------------------------

- (NSString *) appendPrefixOrSuffixToString:(CustomReport *)report isCompareValue:(BOOL)isCompareValue {
    @try {
        NSString *valueString = @"−";
        NSNumber *value = isCompareValue ? report.dataCompareValue : report.value;
        if (value) {
            valueString = [Helper getDecimalWithRemovingExtraZero:value];
        }
        
        if ([report.prefix isKindOfClass:[NSString class]]) {
            if (report.prefix && report.prefix.length > 0) {
                NSString *currecny = [Helper getCurrencySign:report.prefix];
                valueString = [NSString stringWithFormat:@"%@%@", currecny, valueString];
            } else if (report.suffix && report.suffix.length > 0) {
                valueString = [NSString stringWithFormat:@"%@%@", valueString, report.suffix];
            } else {
                valueString = [NSString stringWithFormat:@"%@", valueString];
            }
        } else {
            valueString = [NSString stringWithFormat:@"%@", valueString];
        }
        
        // Show extra info if it is available
        if (report.extraInfo) {
            valueString = [NSString stringWithFormat:@"%@ %@", valueString, report.extraInfo];
        }

        return valueString;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return @"";
    }
}

//---------------------------------------------------------------

- (NSMutableAttributedString *) appendArrowToString:(CustomReport *)report valueString:(NSString *)string {
    @try {
        
        double actual = [Helper validateInfinte:report.value.doubleValue];
        double compare = [Helper validateInfinte:report.dataCompareValue.doubleValue];
        
        NSString *arrowString = @"↑";
        UIColor *arrowColor = OB_GREEN_COLOR;
        if ([self isCompareValue]) {
            // Set image
            if (actual < compare) {
                arrowString = @"↓";
                arrowColor = [UIColor redColor];
            }
            else if (actual > compare) {
                arrowString = @"↑";
                arrowColor = OB_GREEN_COLOR;
            }
        }
        string = [string stringByAppendingString:arrowString];
        
        // Apply Color
        NSRange range = [string rangeOfString:arrowString];
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:string];
        [attrString addAttribute:NSForegroundColorAttributeName value:arrowColor range:range];
        return attrString;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return [[NSMutableAttributedString alloc] initWithString:string];
    }
}

//---------------------------------------------------------------

- (void) createFilters {
    
    NSMutableArray *tempArray = [NSMutableArray new];
    self.isRegion = NO;
    self.isLocation = NO;
    
    if ([self createFilterItem:GeographyType]) {
        self.isRegion = YES;
    }
    
    if ([self createFilterItem:LocationFilter]) {
        self.isLocation = YES;
        [tempArray addObject:[self createFilterItem:LocationFilter]];
    }
    if ([self createFilterItem:LocationGroupFilter]) {
        self.isLocation = YES;
        
        // Add if location is not there
        if (![self isLocationFilterAvailable:tempArray]) {
            [tempArray addObject:[self createNewLocationFilter]];
        }
        [tempArray addObject:[self createFilterItem:LocationGroupFilter]];
    }
    if ([self createFilterItem:UserFilter]) {
        self.isLocation = YES;
        // Add if location is not there
        if (![self isLocationFilterAvailable:tempArray]) {
            [tempArray addObject:[self createNewLocationFilter]];
        }
        
        [tempArray addObject:[self createFilterItem:UserFilter]];
    }
    if ([self createFilterItem:ProductFilter]) {
        self.isLocation = YES;
        // Add if location is not there
        if (![self isLocationFilterAvailable:tempArray]) {
            [tempArray addObject:[self createNewLocationFilter]];
        }
        [tempArray addObject:[self createFilterItem:ProductFilter]];
    }
    
    // Metric type
    NSArray *metricArray = @[[App_Delegate.keyMapping getKeyValue:@"KEY_METRIC_TYPE_ARRIVAL"],
                             [App_Delegate.keyMapping getKeyValue:@"KEY_METRIC_TYPE_DEPARTURE"],
                             [App_Delegate.keyMapping getKeyValue:@"KEY_METRIC_TYPE_DAILY"]
                             ];
    RefineItem *item7 = [RefineItem new];
    item7.itemId = nil;
    item7.title = [App_Delegate.keyMapping getKeyValue:@"KEY_METRIC_TYPE"];
    item7.displayName = self.filter.metricTypeName;
    item7.subItems = metricArray;
    item7.filterType = MetricTypeFilter;
    
    [tempArray addObject:item7];
    self.filterArray = [NSArray arrayWithArray:tempArray];
    tempArray = nil;
    
    // Call background services - filter services
    [self callFilterWebServicesInBackground];
}

//---------------------------------------------------------------

- (RefineItem *) createNewLocationFilter {
    RefineItem *item = [RefineItem new];
    item.itemId = self.filter.locationId;
    item.title = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_LOCATION"];
    item.displayName = @"";
    item.subItems = nil;
    item.filterType = LocationFilter;
    return item;
}

//---------------------------------------------------------------

- (BOOL)isLocationFilterAvailable:(NSArray *)tempArray {
    // Add location if not there
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.filterType = %d", LocationFilter];
    NSArray *remainingArray = [tempArray filteredArrayUsingPredicate:predicate];
    if (remainingArray.count > 0) {
        return YES;
    } else {
        return NO;
    }
}

//---------------------------------------------------------------

- (RefineItem *) createFilterItem:(FilterType)filterType {
    RefineItem *item = nil;
    NSString *searchString = @"";
    NSNumber *itemId = nil;
    NSString *displayName = @"";
    NSArray *subItems = nil;
    
    switch (filterType) {
        case GeographyType: {
            searchString = @"Region";
            break;
        }
            
        case LocationFilter: {
            searchString = LOCATION_FILTER;
            itemId = self.filter.locationId;
            displayName = self.filter.locationName;
            subItems = self.locationList.locationList;
            
            break;
        }
        case LocationGroupFilter: {
            searchString = LOCATION_GROUP_FILTER;
            itemId = self.filter.locationGroupId;
            displayName = self.filter.locationGroupName;
            subItems = self.locationGroupList.locationGroupList;
            break;
        }
        case UserFilter: {
            searchString = USER_FILTER;
            itemId = self.filter.userId;
            displayName = self.filter.userName;
            subItems = self.userList.userList;
            break;
        }
        case ProductFilter: {
            searchString = PRODUCT_FILTER;
            itemId = self.filter.productId;
            displayName = self.filter.productName;
            subItems = self.productList.productList;
            break;
        }
        default:
            break;
    }
    
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF = %@", searchString];
    NSArray *array = [self.filterTypes filteredArrayUsingPredicate:predicate1];
    if (array.count > 0) {
        NSString *title = @"";
        NSString *filter = [array objectAtIndex:0];
        if ([[self.filterDetails.dimensionDisplayName allKeys] containsObject:filter]) {
            title = [self.filterDetails.dimensionDisplayName objectForKey:filter];
            // Create filter item
            item = [RefineItem new];
            item.itemId = itemId;
            item.title = title;
            item.displayName = displayName;
            item.subItems = subItems;
            item.filterType = filterType;
        } else {
            item = nil;
        }
    }
    return item;
}

//---------------------------------------------------------------

- (void) updateFilterItem:(RefineItem *)item {
    @try {
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF.filterType == %d", item.filterType];
        NSArray *array = [self.filterArray filteredArrayUsingPredicate:predicate1];
        if (array.count > 0) {
            RefineItem *prevItem = (RefineItem *)[array objectAtIndex:0];
            NSInteger index = [self.filterArray indexOfObject:prevItem];
            
            // Update with new item
            item.title = prevItem.title;
            
            NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.filterArray];
            [tempArray replaceObjectAtIndex:index withObject:item];
            self.filterArray = [NSArray arrayWithArray:tempArray];
            tempArray = nil;
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void) removeFilterItem:(RefineItem *)item {
    @try {
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF.filterType == %d", item.filterType];
        NSArray *array = [self.filterArray filteredArrayUsingPredicate:predicate1];
        if (array.count > 0) {
            RefineItem *prevItem = (RefineItem *)[array objectAtIndex:0];
            NSInteger index = [self.filterArray indexOfObject:prevItem];
            
            // Update with new item
            item.title = prevItem.title;
            
            NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.filterArray];
            [tempArray removeObjectAtIndex:index];
            self.filterArray = [NSArray arrayWithArray:tempArray];
            tempArray = nil;
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (NSString *) getGeographyName {
    @try {
        NSString *geoLocation = @"";
        // Get geography name first
        NSNumber *geoId = self.filter.regionId;
        if (geoId != nil) {
            geoLocation = self.filter.regionName;
        }
        
        NSPredicate *locPredicate = [NSPredicate predicateWithFormat:@"SELF = %@", LOCATION_FILTER];
        NSArray *locationArray = [self.filterTypes filteredArrayUsingPredicate:locPredicate];
        // If location is available than get location name
        if (locationArray.count > 0) {
            geoLocation = self.filter.locationName;
        }
        return geoLocation;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return @"";
    }
}

//---------------------------------------------------------------

- (NSString *) getUserNameOrProductName {
    @try {
        NSString *userName = @"";
        // Get geography name first
        // If user filter is there than only show user name
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF.filterType == %d", UserFilter];
        NSArray *array1 = [self.filterArray filteredArrayUsingPredicate:predicate1];

        if (self.filter.userId != nil && array1.count > 0) {
            userName = self.filter.userName;
        }
        
        // If user filter is there than only show user name
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"SELF.filterType == %d", ProductFilter];
        NSArray *array2 = [self.filterArray filteredArrayUsingPredicate:predicate2];
        
        if (array1.count == 0 && array2.count > 0) {
            userName = self.filter.productName;
        }
        return userName;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return @"";
    }
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark UICollectionView datasource methods

//---------------------------------------------------------------

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

//---------------------------------------------------------------

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.chartArray.count;
}

//---------------------------------------------------------------

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"CustomReport"];
    UICollectionViewCell *mainCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CustomReport" forIndexPath:indexPath];
    
    CustomReport *report = (CustomReport *)[self.chartArray objectAtIndex:indexPath.row];
    
    //UPDATE:17-Oct-2017 ~ if there is an error in report than show message
    if (report.isError.boolValue) {
        UIView *cardView = [[UIView alloc] init];
        cardView.translatesAutoresizingMaskIntoConstraints = NO;
        cardView.backgroundColor = [UIColor whiteColor];
        [mainCell.contentView addSubview:cardView];
        
        [mainCell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cardView]-1-|" options: 0 metrics:nil views:@{@"cardView" : cardView}]];
        [mainCell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cardView]-1-|" options: 0 metrics:nil views:@{@"cardView" : cardView}]];
        
        UILabel *label = [[UILabel alloc] init];
        label.translatesAutoresizingMaskIntoConstraints = NO;
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont fontWithName:AVENIR_NEXT_DEMI_BOLD_FONT size:12];
        label.textColor = GRAPHITE_SEPARATOR;
        label.numberOfLines = 0;
        [cardView addSubview:label];
        NSDictionary *views = @{@"label" : label};
        
        label.text = @"There is some error in report data. Please contact to system administrator.";
        
        [cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-2-[label]-2-|" options: 0 metrics:nil views:views]];
        [cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[label]-2-|" options: 0 metrics:nil views:views]];
        return mainCell;
    } else {
        if ([report.reportType isEqualToString:KEY_BLOCK_CHART]) {
            [collectionView registerClass:[BlockCell class] forCellWithReuseIdentifier:blockCellIdentifier];
            BlockCell *cell = (BlockCell *)[collectionView dequeueReusableCellWithReuseIdentifier:blockCellIdentifier forIndexPath:indexPath];
            
            
            //UPDATE: 24-Nov-2016 : IN-243 - Pre-Configured blocks will not show details, as after converting them to chart we are getting zero value
            cell.noVisibleImage.hidden = (report.isQueryBasedReport.boolValue && [report.reportType isEqualToString:KEY_BLOCK_CHART]) ? NO : YES;
            @try {
                BOOL isCompare = [self isCompareValue];
                
                // Value
                NSString *valueString = @"−";
                // --> Attach - Suffix or Prefix
                valueString = [self appendPrefixOrSuffixToString:report isCompareValue:NO];
                
                // Compare Value Label
                NSString *compareValueString = @"−";
                
                // --> Attach - Suffix or Prefix
                compareValueString = [self appendPrefixOrSuffixToString:report isCompareValue:YES];
                
                // Append Arrow
                if (isCompare) {
                    
                    double actual = [Helper validateInfinte:report.value.doubleValue];
                    double compare = [Helper validateInfinte:report.dataCompareValue.doubleValue];
                    
                    NSString *arrowString = @"↑";
                    UIColor *arrowColor = OB_GREEN_COLOR;
                    if ([self isCompareValue]) {
                        // Set image
                        if (actual < compare) {
                            arrowString = @"↓";
                            arrowColor = [UIColor redColor];
                            cell.valueLabel.textColor = arrowColor;
                            cell.compareValueLabel.textColor = OB_GREEN_COLOR;
                        }
                        else if (actual > compare) {
                            arrowString = @"↑";
                            arrowColor = OB_GREEN_COLOR;
                            cell.valueLabel.textColor = arrowColor;
                            cell.compareValueLabel.textColor = [UIColor redColor];
                        } else if (actual == compare) {
                            cell.valueLabel.textColor = arrowColor;
                            cell.compareValueLabel.textColor = [UIColor redColor];
                        }
                    }
                    valueString = [valueString stringByAppendingString:arrowString];
                    
                    // Apply Color
                    NSRange range = [valueString rangeOfString:arrowString];
                    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:valueString];
                    [attrString addAttribute:NSForegroundColorAttributeName value:arrowColor range:range];
                    
                    cell.valueLabel.attributedText = attrString; //[self appendArrowToString:report valueString:valueString];
                } else {
                    cell.valueLabel.text = valueString;
                    cell.valueLabel.textColor = OB_GREEN_COLOR;
                }
                
                // ReportName
                cell.reportNameLabel.text = report.reportName ? [report.reportName uppercaseString] : @"";
                
                // --> No arrows for compare value
                
                // Show for compare values only
                if (isCompare) {
                    cell.compareValueLabel.text = compareValueString;
                    cell.compareValueLabel.hidden = NO;
                } else {
                    cell.compareValueLabel.hidden = YES;
                }
                
                // Entity Name
                NSString *entityName = report.entityName ? report.entityName : @"";
                NSString *compEntityName = report.compEntityName ? report.compEntityName : @"";
                if (compEntityName.length > 0) {
                    cell.entityName.text = [NSString stringWithFormat:@"%@ / %@(Compare)", entityName, compEntityName];
                } else {
                    cell.entityName.text = entityName;
                }
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
            }
            
            mainCell = cell;
        }
        else {
            [collectionView registerClass:[ChartCell class] forCellWithReuseIdentifier:chartCellIdentifier];
            ChartCell *cell = (ChartCell *)[collectionView dequeueReusableCellWithReuseIdentifier:chartCellIdentifier forIndexPath:indexPath];
            
            @try {
                // ReportName
                cell.label.text = report.reportName;
                // Report Type - Table
                if ([report.reportType isEqualToString:KEY_TABLE_CHART]) {
                    cell.imageView.image = [UIImage imageNamed:TABLE_IMAGE];
                } else {
                    
                    //*** Check for chart types - change images
                    // 1. Column
                    if ([report.chartType isEqualToString:KEY_COLUMN_CHART]) {
                        cell.imageView.image = [UIImage imageNamed:BAR_CHART_IMAGE];
                    }
                    // 2. Pie
                    else if ([report.chartType isEqualToString:KEY_PIE_CHART]) {
                        cell.imageView.image = [UIImage imageNamed:PIE_CHART_IMAGE];
                    }
                    // 3. Bubble
                    else if ([report.chartType isEqualToString:KEY_BUBBLE_CHART]) {
                        cell.imageView.image = [UIImage imageNamed:BUBBLE_CHART_IMAGE];
                    }
                    // 4. Spline
                    else if ([report.chartType isEqualToString:KEY_SPLINE_CHART]) {
                        cell.imageView.image = [UIImage imageNamed:SPLINE_CHART_IMAGE];
                    } else {
                        cell.imageView.image = [UIImage imageNamed:BAR_CHART_IMAGE];
                    }
                }
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
            }
            mainCell = cell;
        }
    }
    
    return mainCell;
}

//---------------------------------------------------------------

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 1;
}

//---------------------------------------------------------------

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 1;
}

//---------------------------------------------------------------

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //If you want your cell should be square in size the return the equal height and width, and make sure you deduct the Section Inset from it.
    return CGSizeMake((self.view.frame.size.width/2) - 0.5, 180);
}

//---------------------------------------------------------------

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // Animation
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    CustomReport *report = [self.chartArray objectAtIndex:indexPath.row];
    if (report.isError.boolValue) {
        return;
    }
    
    //UPDATE: 24-Nov-2016 : Pre-Configured blocks will not show details, as after converting them to chart we are getting zero value
    if (report.isQueryBasedReport.boolValue && [report.reportType isEqualToString:KEY_BLOCK_CHART]) {
        return;
    }
    
    cell.transform = CGAffineTransformMakeScale(0.5, 0.5);
    [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        cell.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        self.selectedReport = report;
        
        // Check for internet connnection
        if (![Helper checkForConnection:self]) {
            return;
        }

        // Analytics
        [AnalyticsLogger logDashboardShowReportEvent:self report:report];
        
        if ([report.reportType isEqualToString:KEY_TABLE_CHART]) {
            // Show table
            [self performSegueWithIdentifier:@"showTableDetails" sender:self];
        } else {
            // Show chart
            [self performSegueWithIdentifier:@"showChartDetails" sender:self];
        }
    }];
}
//---------------------------------------------------------------

#pragma mark -
#pragma mark SideMenu delegate methods

//---------------------------------------------------------------

- (BOOL) slideNavigationControllerShouldDisplayRightMenu {
    return YES;
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark View LifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    [self hidesBottomBarWhenPushed];
    [self setTopNavBarTitle];
    [Helper addBottomLine:self.topView withColor:TABLE_SEPERATOR_COLOR];

    // Check if feed access is there
    if (!App_Delegate.userPermissions.canAccessFeed.boolValue) {
        NSMutableArray *tabbarViewControllers = [NSMutableArray arrayWithArray: [self.tabBarController viewControllers]];
        [tabbarViewControllers removeObjectAtIndex:1];
        [self.tabBarController setViewControllers:tabbarViewControllers];
    }
    
    // Check if Video access is there
    if (!App_Delegate.userPermissions.canAccessVideoLib.boolValue) {
        NSMutableArray *tabbarViewControllers = [NSMutableArray arrayWithArray: [self.tabBarController viewControllers]];
        [tabbarViewControllers removeObjectAtIndex:2];
        [self.tabBarController setViewControllers:tabbarViewControllers];
    }
    
    self.monthYearLabel.hidden = YES;
    self.isRegion = NO;
    self.isLocation = NO;
    self.filterButton.enabled = NO;
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.alwaysBounceVertical = YES;
    
    _noRecordsView = [Helper createNoAccessView:self.collectionView withMessage:[App_Delegate.keyMapping getKeyValue:@"KEY_NO_DATA_DASHBOARD"]];

    // Get filters from stored
    _filter = [Filters new];
    Filters *storedFilters = [ServicePreference customObjectWithKey:KEY_STORE_FILTERS_DASHBOARD];
    if (storedFilters) {
        self.filter = storedFilters;
    }
    
    // Default metric type
    // UPDATE: 18 Dec 2017 ~ Default metric is now "Departure", previously it was "Arrival"
    self.filter.metricsDateType = self.filter.metricTypeName ? self.filter.metricTypeName : @"Departure";
    
    // Side menu settings
    [self.menuButton setTarget:[SlideNavigationController sharedInstance]];
    [self.menuButton setAction:@selector(toggleRightMenu)];
   
    // Date settings
    [self dateSettings];
    
    // Get dashboard list
    [self callWebServiceToGetDashboardList];
    
    // Write all notification code in below method
    [self setupNotificationsForDashboard];
    
    // Create PULL-TO-REFRESH CONTROL
    [self createPullToRefresh];
}

//---------------------------------------------------------------

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    // Close side menu if its open!
    if ([[SlideNavigationController sharedInstance] isMenuOpen]) {
        [[SlideNavigationController sharedInstance] closeMenuWithCompletion:^{
        }];
    }
}

//---------------------------------------------------------------
// In a Storyboard-based application, you will often want to do a little preparation before navigation
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {    
    NSDate *dateFromMonthAndYear = [Helper getDateFromMonth:self.filter.month andYear:self.filter.year];
    NSDate *startDate = [Helper firstDateOfMonth:dateFromMonthAndYear];
    NSDate *endDate = [Helper lastDateOfMonth:dateFromMonthAndYear];

    // Open Charts
    if ([segue.identifier isEqualToString:@"showChartDetails"]) {
        ChartDetailsViewController *controller = (ChartDetailsViewController *)[segue destinationViewController];
        controller.filter = self.filter;
        controller.locationArray = self.locationList.locationList;
        controller.userArray = self.userList.userList;
        controller.report = self.selectedReport;
        
        // Hide compare details
        if (self.filterDetails.dashboard.hideCompareDate.boolValue) {
            controller.filter.isCompare = @"false";
        }
        // Change dates to MTD
        if (self.filterDetails.dashboard.isMTD.boolValue) {
            controller.filter.isCompare = @"false";
            controller.filter.startDate = startDate;
            controller.filter.endDate = endDate;
        }
    }
    
    // Open table
    if ([segue.identifier isEqualToString:@"showTableDetails"]) {
        DashboardTableController *controller = (DashboardTableController *)[segue destinationViewController];
        controller.filter = self.filter;
        controller.report = self.selectedReport;
        controller.isMTD = self.filterDetails.dashboard.isMTD.boolValue;
        
        // Hide compare details
        if (self.filterDetails.dashboard.hideCompareDate.boolValue) {
            controller.filter.isCompare = @"false";
        }
        // Change dates to MTD
        if (self.filterDetails.dashboard.isMTD.boolValue) {
            controller.filter.isCompare = @"false";
            controller.filter.startDate = startDate;
            controller.filter.endDate = endDate;
        }
    }
}

//---------------------------------------------------------------

@end
