//
//  BlockCell.h
//  IN-Gauge
//
//  Created by Mehul Patel on 29/08/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"

@interface BlockCell : UICollectionViewCell

@property (nonatomic, strong) UIView            *cardView;
@property (nonatomic, strong) UILabel           *reportNameLabel;
@property (nonatomic, strong) UILabel           *valueLabel;
@property (nonatomic, strong) UILabel           *compareValueLabel;
@property (nonatomic, strong) UILabel           *entityName;
@property (nonatomic, strong) UIImageView       *noVisibleImage;

@end
