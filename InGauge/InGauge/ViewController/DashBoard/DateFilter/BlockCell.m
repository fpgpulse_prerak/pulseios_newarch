//
//  BlockCell.m
//  IN-Gauge
//
//  Created by Mehul Patel on 29/08/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "BlockCell.h"

@implementation BlockCell

#pragma mark
#pragma mark Init methods

//---------------------------------------------------------------

- (id) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    self.backgroundColor = [UIColor whiteColor];
    
    _cardView = [[UIView alloc] init];
    self.cardView.translatesAutoresizingMaskIntoConstraints = NO;
    self.cardView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.cardView];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cardView]-1-|" options: 0 metrics:nil views:@{@"cardView" : self.cardView}]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cardView]-1-|" options: 0 metrics:nil views:@{@"cardView" : self.cardView}]];
    
    // Graphite Color
//    UIColor *labelColor = [UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1];
    UIColor *labelColor = GRAPHITE_SEPARATOR;

    _reportNameLabel = [[UILabel alloc] init];
    self.reportNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.reportNameLabel.textAlignment = NSTextAlignmentCenter;
    self.reportNameLabel.font = [UIFont fontWithName:AVENIR_NEXT_DEMI_BOLD_FONT size:12];
    self.reportNameLabel.textColor = labelColor;
    self.reportNameLabel.numberOfLines = 0;
    [self.cardView addSubview:self.reportNameLabel];
    
    _valueLabel = [[UILabel alloc] init];
    self.valueLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.valueLabel.textAlignment = NSTextAlignmentCenter;
    self.valueLabel.font = [UIFont fontWithName:AVENIR_FONT_HEAVY size:20];
    self.valueLabel.textColor = OB_GREEN_COLOR;
    self.valueLabel.numberOfLines = 0;
    [self.cardView addSubview:self.valueLabel];
    
    _compareValueLabel = [[UILabel alloc] init];
    self.compareValueLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.compareValueLabel.textAlignment = NSTextAlignmentCenter;
    self.compareValueLabel.font = [UIFont fontWithName:AVENIR_FONT_HEAVY size:16];
    self.compareValueLabel.textColor = OB_GREEN_COLOR;
    self.compareValueLabel.numberOfLines = 0;
    [self.cardView addSubview:self.compareValueLabel];
    
    _entityName = [[UILabel alloc] init];
    self.entityName.translatesAutoresizingMaskIntoConstraints = NO;
    self.entityName.textAlignment = NSTextAlignmentCenter;
    self.entityName.font = [UIFont fontWithName:AVENIR_NEXT_FONT size:12];
    self.entityName.textColor = labelColor;
    self.entityName.numberOfLines = 0;
    [self.cardView addSubview:self.entityName];
    
    _noVisibleImage = [[UIImageView alloc] init];
    self.noVisibleImage.translatesAutoresizingMaskIntoConstraints = NO;
    self.noVisibleImage.hidden = NO;
    self.noVisibleImage.image = [UIImage imageNamed:@"not-visible"];
    self.noVisibleImage.backgroundColor = [UIColor clearColor];
    [self.cardView bringSubviewToFront:self.noVisibleImage];
    [self.cardView addSubview:self.noVisibleImage];
    
//    self.reportNameLabel.backgroundColor = [UIColor redColor];
//    self.valueLabel.backgroundColor = [UIColor yellowColor];
//    self.compareValueLabel.backgroundColor = [UIColor greenColor];
//    self.entityName.backgroundColor = [UIColor blueColor];
//    self.maskLabel.backgroundColor = [UIColor brownColor];
    
    NSDictionary *views = @{@"reportNameLabel" : self.reportNameLabel, @"valueLabel" : self.valueLabel, @"entityName" : self.entityName, @"compareValueLabel" : self.compareValueLabel, @"noVisibleImage" : self.noVisibleImage};
    
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-2-[reportNameLabel]-2-|" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-2-[valueLabel]-2-|" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-2-[compareValueLabel]-2-|" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-2-[entityName]-2-|" options: 0 metrics:nil views:views]];
    
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[reportNameLabel]" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[entityName]-10-|" options: 0 metrics:nil views:views]];

    [self.cardView addConstraint:[NSLayoutConstraint constraintWithItem:self.valueLabel
                                                              attribute:NSLayoutAttributeCenterY
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.cardView
                                                              attribute:NSLayoutAttributeCenterY
                                                             multiplier:1 constant:0]];
    
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[valueLabel]-0-[compareValueLabel]" options: 0 metrics:nil views:views]];

    // Content Hugging priority - This will remove extra padding (height) of label
    [self.reportNameLabel setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisVertical];
    [self.valueLabel setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisVertical];
    [self.compareValueLabel setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisVertical];
    [self.entityName setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisVertical];

    // Image constraints
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[noVisibleImage(20)]-5-|" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[noVisibleImage(20)]-10-|" options: 0 metrics:nil views:views]];

    return self;
}

//---------------------------------------------------------------

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

//---------------------------------------------------------------

- (void) layoutSubviews {
    [super layoutSubviews];
}

//---------------------------------------------------------------

@end
