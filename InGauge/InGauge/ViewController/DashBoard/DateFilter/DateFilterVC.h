//
//  DateFilterVC.h
//  
//
//  Created by Bharat Chandera on 31/05/17.
//
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "AppDelegate.h"
#import <RMDateSelectionViewController/RMDateSelectionViewController.h>
#import "BaseTableViewController.h"

@protocol DateFilterDelegate <NSObject>

- (void) reloadDashboardDetails;

@end

@interface DateFilterVC : BaseTableViewController

@property (nonatomic, weak) IBOutlet UILabel    *startDate;
@property (nonatomic, weak) IBOutlet UILabel    *endDate;
@property (nonatomic, weak) IBOutlet UILabel    *comStartDate;
@property (nonatomic, weak) IBOutlet UILabel    *comEndDate;
@property (nonatomic, weak) IBOutlet UISwitch   *comSwitch;
// Lable
@property (nonatomic, weak) IBOutlet UILabel    *lblstartDate;
@property (nonatomic, weak) IBOutlet UILabel    *lblendDate;
@property (nonatomic, weak) IBOutlet UILabel    *lblcomStartDate;
@property (nonatomic, weak) IBOutlet UILabel    *lblcomEndDate;
@property (nonatomic, weak) IBOutlet UILabel    *lblcomSwitch;


@property (nonatomic, weak) id <DateFilterDelegate>     delegate;
@property (nonatomic, assign) BOOL                      isShowCompareDate;
@property (nonatomic, assign) BOOL                      removeDateLimit;

@end
