//
//  DateFilterVC.m
//
//
//  Created by Bharat Chandera on 31/05/17.
//
//

#import "DateFilterVC.h"
#import "ServicePreference.h"

@interface DateFilterVC ()

@property (nonatomic, strong) NSString      *localStartDate;
@property (nonatomic, strong) NSString      *localEndDate;
@property (nonatomic, strong) NSString      *localCompStartDate;
@property (nonatomic, strong) NSString      *localCompEndDate;
@property (nonatomic, assign) BOOL          LocalIsCompare;

@end

@implementation DateFilterVC

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void)dealloc {
    // Remove delegates, mutable array and observers here!
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction)compareSwitch:(UISwitch *)sender {
    
    if (sender.isOn) {
        [sender setOn:NO];
        UITableViewCell *comStartDateCell = [super tableView:self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
        comStartDateCell.textLabel.textColor = [UIColor lightGrayColor];
        UITableViewCell *comEndDateCell = [super tableView:self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
        comEndDateCell.textLabel.textColor = [UIColor lightGrayColor];
    } else {
        [sender setOn:YES];
        UITableViewCell *comStartDateCell = [super tableView:self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
        comStartDateCell.textLabel.textColor = [UIColor blackColor];
        UITableViewCell *comEndDateCell = [super tableView:self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
        comEndDateCell.textLabel.textColor = [UIColor blackColor];
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (BOOL) checkIfDataChange {
    if (![self.startDate.text isEqualToString:self.localStartDate]) {
        return YES;
    } else if (![self.endDate.text isEqualToString:self.localEndDate]) {
        return YES;
    } else if (![self.comStartDate.text isEqualToString:self.localCompStartDate]) {
        return YES;
    } else if (![self.comEndDate.text isEqualToString:self.localCompEndDate]) {
        return YES;
    } else if (self.comSwitch.isOn != self.LocalIsCompare) {
        return YES;
    }
    return NO;
}

//---------------------------------------------------------------

- (void) saveDateFilters {
    
    // Only update if changed
    if ([self checkIfDataChange]) {
        // Save change values
        [ServicePreference setObject:[NSString stringWithFormat:@"%@",self.startDate.text] forKey:KStartDate];
        [ServicePreference setObject:[NSString stringWithFormat:@"%@",self.endDate.text] forKey:KEndDate];
        [ServicePreference setObject:[NSString stringWithFormat:@"%@",self.comStartDate.text] forKey:KComStartDate];
        [ServicePreference setObject:[NSString stringWithFormat:@"%@",self.comEndDate.text] forKey:KComEndDate];
        [ServicePreference setObject:(self.comSwitch.isOn ? @"true" : @"false") forKey:KComSwitch];
        
        // Set detes to filters
        [self.delegate reloadDashboardDetails];
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

//---------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.isShowCompareDate ? 5 : 2;
}

//---------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ((indexPath.row == 3 && !self.comSwitch.isOn) || (indexPath.row == 4 && !self.comSwitch.isOn) || indexPath.row == 2) {
        return;
    }
    
    // Select date and validate
    RMAction<UIDatePicker *> *selectAction = [RMAction<UIDatePicker *> actionWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_SELECT"] style:RMActionStyleDone andHandler:^(RMActionController<UIDatePicker *> *controller) {
        
        NSString *dateString = [NSString stringWithFormat:@"%@", [controller.contentView.date toNSString:APP_DISPLAY_DATE_TIME_FORMAT]];
        
        NSDate *startDate = [NSDate dateFromNSString:self.startDate.text withFormat:APP_DISPLAY_DATE_TIME_FORMAT];
        NSDate *endDate = [NSDate dateFromNSString:self.endDate.text withFormat:APP_DISPLAY_DATE_TIME_FORMAT];
        NSDate *compStartDate = [NSDate dateFromNSString:self.comStartDate.text withFormat:APP_DISPLAY_DATE_TIME_FORMAT];
        NSDate *compEndDate = [NSDate dateFromNSString:self.comEndDate.text withFormat:APP_DISPLAY_DATE_TIME_FORMAT];

        switch (indexPath.row) {
            case 0: {
                // Start date is Greater than end date
                // Change end date to last day of start date's month
                if (![Helper isDatesValid:controller.contentView.date endDate:endDate]) {
                    NSDate *nextDate = [Helper lastDateOfMonth:controller.contentView.date];
                    self.endDate.text = [NSString stringWithFormat:@"%@", [nextDate toNSString:APP_DISPLAY_DATE_TIME_FORMAT]];
                }
                self.startDate.text = dateString;
                break;
            }
                
            case 1: {
                // Date validation: End date must be greater than start date
                // Change start date to first day of end date's month
                if (![Helper isDatesValid:startDate endDate:controller.contentView.date]) {
                    NSDate *nextDate = [Helper firstDateOfMonth:controller.contentView.date];
                    self.startDate.text = [NSString stringWithFormat:@"%@", [nextDate toNSString:APP_DISPLAY_DATE_TIME_FORMAT]];
                }
                self.endDate.text = dateString;
                break;
            }
                
            case 3: {
                // Start date is Greater than end date
                // Change end date to last day of start date's month
                if (![Helper isDatesValid:controller.contentView.date endDate:compEndDate]) {
                    NSDate *nextDate = [Helper lastDateOfMonth:controller.contentView.date];
                    self.comEndDate.text = [NSString stringWithFormat:@"%@", [nextDate toNSString:APP_DISPLAY_DATE_TIME_FORMAT]];
                }
                self.comStartDate.text = dateString;
                break;
            }
                
            case 4: {
                // Date validation: End date must be greater than start date
                // Change start date to first day of end date's month
                if (![Helper isDatesValid:compStartDate endDate:controller.contentView.date]) {
                    NSDate *nextDate = [Helper firstDateOfMonth:controller.contentView.date];
                    self.comStartDate.text = [NSString stringWithFormat:@"%@", [nextDate toNSString:APP_DISPLAY_DATE_TIME_FORMAT]];
                }
                self.comEndDate.text = dateString;
                break;
            }
            default:
                break;
        }
    }];
    
    RMAction<UIDatePicker *> *cancelAction = [RMAction<UIDatePicker *> actionWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_TITLE_CANCEL"] style:RMActionStyleCancel andHandler:^(RMActionController<UIDatePicker *> *controller) {
    }];
    
    RMDateSelectionViewController *dateSelectionController = [RMDateSelectionViewController actionControllerWithStyle:RMActionControllerStyleDefault title:@"Date Filter" message:@"Please choose a date and press 'Select' or 'Cancel'." selectAction:selectAction andCancelAction:cancelAction];
    dateSelectionController.datePicker.datePickerMode = UIDatePickerModeDate;
    dateSelectionController.disableBlurEffectsForContentView = YES;
    dateSelectionController.disableBlurEffects = YES;
    
    
    // Select dates
    NSDate *selectDate = [NSDate new];
    switch (indexPath.row) {
        // Start date
        case 0: {
            selectDate = [NSDate dateFromNSString:self.startDate.text withFormat:APP_DISPLAY_DATE_TIME_FORMAT];
            dateSelectionController.datePicker.maximumDate = [NSDate date];
            break;
        }
        
        // End date
        case 1: {
            selectDate = [NSDate dateFromNSString:self.endDate.text withFormat:APP_DISPLAY_DATE_TIME_FORMAT];
            dateSelectionController.datePicker.minimumDate = [NSDate dateFromNSString:self.startDate.text withFormat:APP_DISPLAY_DATE_TIME_FORMAT];;
            dateSelectionController.datePicker.maximumDate = [NSDate date];
            break;
        }
           
        // Compare start date
        case 3: {
            selectDate = [NSDate dateFromNSString:self.comStartDate.text withFormat:APP_DISPLAY_DATE_TIME_FORMAT];
            dateSelectionController.datePicker.maximumDate = [NSDate date];
            break;
        }
           
        // Compare end date
        case 4: {
            selectDate = [NSDate dateFromNSString:self.comEndDate.text withFormat:APP_DISPLAY_DATE_TIME_FORMAT];
            dateSelectionController.datePicker.minimumDate = [NSDate dateFromNSString:self.comStartDate.text withFormat:APP_DISPLAY_DATE_TIME_FORMAT];;
            break;
        }
        default:
            break;
    }
    dateSelectionController.datePicker.date = selectDate;
    dateSelectionController.datePicker.maximumDate = [NSDate date];

    
//    if (indexPath.row == 0) {
//        dateSelectionController.datePicker.date = [NSDate dateFromNSString:self.startDate.text withFormat:APP_DISPLAY_DATE_TIME_FORMAT];
//        dateSelectionController.datePicker.maximumDate = [NSDate date];
//    } else if (indexPath.row == 1) {
//        dateSelectionController.datePicker.date = [NSDate dateFromNSString:self.endDate.text withFormat:APP_DISPLAY_DATE_TIME_FORMAT];
//        dateSelectionController.datePicker.minimumDate = [NSDate dateFromNSString:self.startDate.text withFormat:APP_DISPLAY_DATE_TIME_FORMAT];;
//        dateSelectionController.datePicker.maximumDate = [NSDate date];
//    } else if (indexPath.row == 3) {
//        dateSelectionController.datePicker.date = [NSDate dateFromNSString:self.comStartDate.text withFormat:APP_DISPLAY_DATE_TIME_FORMAT];
//        dateSelectionController.datePicker.maximumDate = [NSDate date];
//    } else if (indexPath.row == 4) {
//        dateSelectionController.datePicker.date = [NSDate dateFromNSString:self.comEndDate.text withFormat:APP_DISPLAY_DATE_TIME_FORMAT];
//        dateSelectionController.datePicker.maximumDate = [NSDate date];
//    }
    
    if (self.removeDateLimit) {
        dateSelectionController.datePicker.maximumDate = nil;
        dateSelectionController.datePicker.minimumDate = nil;
    }
    
    [self.navigationController presentViewController:dateSelectionController animated:YES completion:nil];    
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View LifeCycle methods

//---------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.title = @"CHANGE DATE";
    
    self.lblstartDate.text = [App_Delegate.keyMapping getKeyValue:@"KEY_START_DATE"];
    self.lblendDate.text = [App_Delegate.keyMapping getKeyValue:@"KEY_END_DATE"];
    self.lblcomStartDate.text = [App_Delegate.keyMapping getKeyValue:@"KEY_PREVIOUS_START_DATE"];
    self.lblcomEndDate.text = [App_Delegate.keyMapping getKeyValue:@"KEY_PREVIOUS_END_DATE"];
    self.lblcomSwitch.text = [App_Delegate.keyMapping getKeyValue:@"KEY_COMAPRE_TO"];

    if ([ServicePreference objectForKey:KStartDate] != nil) {
        self.startDate.text = [ServicePreference objectForKey:KStartDate];
        self.endDate.text   = [ServicePreference objectForKey:KEndDate];
        self.comStartDate.text = [ServicePreference objectForKey:KComStartDate];
        self.comEndDate.text   = [ServicePreference objectForKey:KComEndDate];
    } else {
        
        // Set default date : Current month and year
        NSDate *endDates = [[NSDate alloc] init];
        NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
        [offsetComponents setMonth:-1];
        NSDate *startDates = [[NSCalendar currentCalendar] dateByAddingComponents:offsetComponents toDate:endDates options:0];
        
        self.startDate.text = [startDates toNSString:APP_DISPLAY_DATE_TIME_FORMAT];
        self.endDate.text   = [endDates toNSString:APP_DISPLAY_DATE_TIME_FORMAT];
        
        NSDate *ComstartDates = [[NSCalendar currentCalendar] dateByAddingComponents:offsetComponents toDate:startDates options:0];
        
        self.comStartDate.text = [ComstartDates toNSString:APP_DISPLAY_DATE_TIME_FORMAT];
        
        [offsetComponents setMonth:-1];
        NSDate *ComendDates = [[NSCalendar currentCalendar] dateByAddingComponents:offsetComponents toDate:ComstartDates options:0];
        self.comEndDate.text   = [ComendDates toNSString:APP_DISPLAY_DATE_TIME_FORMAT];
    }
    
    if ([[ServicePreference objectForKey:KComSwitch] isEqualToString:@"true"]) {
        [self.comSwitch setOn:YES];
    } else {
        [self.comSwitch setOn:NO];
        UITableViewCell *comStartDateCell = [super tableView:self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
        comStartDateCell.textLabel.textColor = [UIColor lightGrayColor];
        UITableViewCell *comEndDateCell = [super tableView:self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
        comEndDateCell.textLabel.textColor = [UIColor lightGrayColor];
    }
    
    // Set local variables
    self.localStartDate = self.startDate.text;
    self.localEndDate = self.endDate.text;
    self.localCompStartDate = self.comStartDate.text;
    self.localCompEndDate = self.comEndDate.text;
    self.LocalIsCompare = self.comSwitch.isOn;
    
}

//---------------------------------------------------------------

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self saveDateFilters];
}

//---------------------------------------------------------------

@end
