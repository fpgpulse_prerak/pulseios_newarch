//
//  MainDashboard.h
//  InGauge
//
//  Created by Mehul Patel on 26/04/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTabBarController.h"

@interface MainDashboard : BaseTabBarController

@property (nonatomic, strong) NSDictionary          *feeds;

@end
