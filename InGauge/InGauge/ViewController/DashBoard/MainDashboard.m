//
//  MainDashboard.m
//  InGauge
//
//  Created by Mehul Patel on 26/04/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "MainDashboard.h"
#import "Helper.h"
#import "FeedsViewController.h"

@interface MainDashboard ()

@end

@implementation MainDashboard

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

//---------------------------------------------------------------

#pragma mark
#pragma mark WEBSERVICE methods

//---------------------------------------------------------------

- (void) getFeedsInBackground {
    NSDictionary *params = [App_Delegate.appfilter serializeFiltersForFeedsService];
    DLog(@"\n\n\n\n==========>>> Background Feeds - Service Call Begin <<<==========\n\n\n\n");
    [ServiceManager getWebServiceCall:GET_FEEDS_ACTION parameters:params whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        DLog(@"\n\n\n\n==========>>> Background Feeds - Service Call finish <<<==========\n\n\n\n");
        self.feeds = response;
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark SideMenu delegate methods

//---------------------------------------------------------------

- (BOOL)slideNavigationControllerShouldDisplayRightMenu {
    return YES;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setHidden:YES];
    
    UITabBar *tabBar = [self tabBar];
    [tabBar setBackgroundImage:[self imageFromColor:APP_COLOR]];
    tabBar.tintColor = [UIColor whiteColor];
    tabBar.translucent = NO;
    
    @try {
        DLog(@"%@",App_Delegate.keyMapping);
        [[tabBar.items objectAtIndex:0] setTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_DASHBOARD"]];
        [[tabBar.items objectAtIndex:1] setTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_FEEDS"]];
        [[tabBar.items objectAtIndex:2] setTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_VIDEO"]];
        [[tabBar.items objectAtIndex:3] setTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_MORE"]];
        
        // Call feeds services
//        [self getFeedsInBackground];
        
        [self performSelectorInBackground:@selector(getFeedsInBackground) withObject:nil];
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

//---------------------------------------------------------------

@end
