//
//  RightMenuViewController.m
//  InGauge
//
//  Created by Mehul Patel on 19/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "RightMenuViewController.h"
#import "SlideNavigationController.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "DashboardListModel.h"

#define HEADER_HEIGHT       30

@interface RightMenuViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView            *tableView;
@property (nonatomic, weak) IBOutlet UILabel                *lblTitle;
@property (nonatomic, weak) IBOutlet UIView                 *topBarView;

@property (nonatomic, strong) DashboardListModel            *dashboardList;
@property (nonatomic, strong) NSArray                       *dashboardArray;
@property (nonatomic, strong) UIView                        *noRecordsView;

@end

@implementation RightMenuViewController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc {
    // Remove delegates, mutable array and observers here!
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (UIView *) createNoAccessView:(UIView *)superView withMessage:(NSString *)message {
    UIView *noAccessView = [[UIView alloc] init];
    [noAccessView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [superView addSubview:noAccessView];
    [noAccessView setHidden:YES];
    [noAccessView setBackgroundColor:[UIColor clearColor]];
    
    UILabel *label = [[UILabel alloc] init];
    label.font = [UIFont fontWithName:AVENIR_FONT_HEAVY size:14];
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByTruncatingTail;
    label.textAlignment = NSTextAlignmentCenter;
    [label setTranslatesAutoresizingMaskIntoConstraints:NO];
    [noAccessView addSubview:label];
    
    label.text = message;
    
    NSDictionary *views = @{@"noAccessView" : noAccessView, @"label" : label};
    // Constraints
    [superView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-50-[noAccessView]-50-|" options:0 metrics:nil views:views]];
    [superView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-60-[noAccessView]-60-|" options:0 metrics:nil views:views]];
    
    [noAccessView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[label]|" options:0 metrics:nil views:views]];
    [noAccessView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[label]|" options:0 metrics:nil views:views]];
    
    return noAccessView;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dashboardArray.count;
}

//---------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    @try {
        NSDictionary *details = [self.dashboardArray objectAtIndex:section];
        NSArray *list = [details objectForKey:KEY_DASHBOARD_LIST];
        return list.count;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        return 0;
    }
}

//---------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return HEADER_HEIGHT;
}

//---------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1.0;
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"RightMenuCell%d%d", (int)indexPath.section, (int)indexPath.row];

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.font = [UIFont fontWithName:AVENIR_NEXT_FONT size:14.0];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    @try {
        NSDictionary *details = [self.dashboardArray objectAtIndex:indexPath.section];
        NSArray *list = [details objectForKey:KEY_DASHBOARD_LIST];
        DashboardModel *dashboard = [list objectAtIndex:indexPath.row];
        cell.textLabel.text = dashboard.title;
        cell.backgroundColor = [UIColor whiteColor];

        if (dashboard.dashboardId.intValue == [SlideNavigationController sharedInstance].selectedDashboard.dashboardId.intValue) {
            cell.textLabel.textColor = MATERIAL_PINK_COLOR;
        } else {
            cell.textLabel.textColor = [UIColor blackColor];
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
    return cell;
}

//---------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [[SlideNavigationController sharedInstance] closeMenuWithCompletion:^{
        @try {
            NSDictionary *details = [self.dashboardArray objectAtIndex:indexPath.section];
            NSArray *list = [details objectForKey:KEY_DASHBOARD_LIST];
            DashboardModel *dashboard = [list objectAtIndex:indexPath.row];
            [SlideNavigationController sharedInstance].selectedDashboard = dashboard;
            
            NSString *type = [details objectForKey:KEY_DASHBOARD_TYPE];
            if ([type isEqualToString:@"1"]) {
                [SlideNavigationController sharedInstance].selectedDashboardType = DBTYPE_1_TITLE;
            }
            else if ([type isEqualToString:@"2"]) {
                [SlideNavigationController sharedInstance].selectedDashboardType = DBTYPE_2_TITLE;
            } else if ([type isEqualToString:@"3"]) {
                [SlideNavigationController sharedInstance].selectedDashboardType = DBTYPE_3_TITLE;
            }
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
    }];
}

//---------------------------------------------------------------

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIControl alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, HEADER_HEIGHT)];
    headerView.tag = section;
    headerView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];

    // User label
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont fontWithName:AVENIR_NEXT_DEMI_BOLD_FONT size:14];
    titleLabel.numberOfLines = 0;
    titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    [titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [headerView addSubview:titleLabel];

    // Text
    @try {
        NSDictionary *details = [self.dashboardArray objectAtIndex:section];
        NSString *type = [details objectForKey:KEY_DASHBOARD_TYPE];
        if ([type isEqualToString:@"1"]) {
            titleLabel.text = DBTYPE_1_TITLE;
        }
        else if ([type isEqualToString:@"2"]) {
            titleLabel.text = DBTYPE_2_TITLE;
        } else if ([type isEqualToString:@"3"]) {
            titleLabel.text = DBTYPE_3_TITLE;
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
        titleLabel.text = @"";
    }
    
    NSDictionary *views = @{@"titleLabel" : titleLabel};
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[titleLabel]-10-|" options: 0 metrics:nil views:views]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[titleLabel]|" options: 0 metrics:nil views:views]];
    return headerView;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Notification methods

//---------------------------------------------------------------

- (void) sideMenuOpen {
    // Show message no records found
    if (self.dashboardArray.count > 0) {
        [self.noRecordsView setHidden:YES];
        [self.tableView setHidden:NO];
    } else {
        [self.noRecordsView setHidden:NO];
        [self.tableView setHidden:YES];
    }
    
    [self.tableView reloadData];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UIView LifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
	[super viewDidLoad];
    
    self.title = [App_Delegate.keyMapping getKeyValue:@"KEY_DB_DASHBOARD_LIST"];    
    self.dashboardArray = [[SlideNavigationController sharedInstance] dashboardList];
    self.lblTitle.text = [App_Delegate.keyMapping getKeyValue:@"KEY_DASHBOARD"];
    _noRecordsView = [self createNoAccessView:self.view withMessage:[App_Delegate.keyMapping getKeyValue:@"KEY_NO_DASHBOARD"]];

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.topBarView.backgroundColor = APP_COLOR;

    id <SlideNavigationContorllerAnimator> revealAnimator;
    CGFloat animationDuration = .22;
    revealAnimator = [[SlideNavigationContorllerAnimatorScaleAndFade alloc] initWithMaximumFadeAlpha:.6 fadeColor:[UIColor blackColor] andMinimumScale:.8];
    [SlideNavigationController sharedInstance].menuRevealAnimationDuration = animationDuration;
    [SlideNavigationController sharedInstance].menuRevealAnimator = revealAnimator;
    
//    [[NSNotificationCenter defaultCenter] addObserverForName:SlideNavigationControllerDidOpen object:nil queue:nil usingBlock:^(NSNotification *note) {
//    }];
    
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [Helper hideLoading:hud];
    
    MBProgressHUD *global = [MBProgressHUD HUDForView:App_Delegate.window];
    [Helper hideLoading:global];
    
    [Helper hideLoading:App_Delegate.globalHud];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sideMenuOpen) name:SlideNavigationControllerDidOpen object:nil];
}

//---------------------------------------------------------------

@end
