//
//  SharedPoppupViewController.h
//  IN-Gauge
//
//  Created by Bharat Chandera on 21/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SharedPoppupViewController;

@protocol SharedPoppupDelegate<NSObject>

@optional
- (void)closeSharePopupWindow;
- (void)shareFeed:(SharedPoppupViewController *)secondDetailViewController;

@end

@interface SharedPoppupViewController : UIViewController

@property (nonatomic, weak) id <SharedPoppupDelegate>       delegate;
@property (nonatomic, weak) IBOutlet UITextView             *shareMsgText;
@property (nonatomic, strong) NSString                      *placeHolderString;
@property (nonatomic, assign) int                           btnShareTag;

@end

