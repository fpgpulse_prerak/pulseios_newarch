//
//  SharedPoppupViewController.m
//  IN-Gauge
//
//  Created by Bharat Chandera on 21/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "SharedPoppupViewController.h"
#import "AppDelegate.h"

@interface SharedPoppupViewController () <UITextViewDelegate>

@property (nonatomic, weak) IBOutlet UIImageView    *closeImage;
@property (nonatomic, weak) IBOutlet UIImageView    *sendImage;
@property (nonatomic, weak) IBOutlet UILabel        *lblTitle;

@end

@implementation SharedPoppupViewController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc {
    // Remove delegates, mutable array and observers here!
    self.shareMsgText.delegate = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) shareButtonCliecked:(id)sender {
    [Helper tapOnControlAnimation:self.sendImage forScale:0.5 forCompletionBlock:^(BOOL finished) {
    }];
    if (self.delegate && [self.delegate respondsToSelector:@selector(shareFeed:)]) {
        [self.delegate shareFeed:self];
    }
}

//---------------------------------------------------------------

- (IBAction) closeButtonCliecked:(id)sender {
    [Helper tapOnControlAnimation:self.closeImage forScale:0.5 forCompletionBlock:^(BOOL finished) {
    }];
    if (self.delegate && [self.delegate respondsToSelector:@selector(closeSharePopupWindow)]) {
        [self.delegate closeSharePopupWindow];
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITextView delegate methods

//---------------------------------------------------------------

- (void) textViewDidBeginEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:self.placeHolderString]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}

//---------------------------------------------------------------

- (void)textViewDidEndEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]) {
        textView.text = self.placeHolderString;
        textView.textColor = [UIColor lightGrayColor]; //optional
    }
    [textView resignFirstResponder];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View LifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];

    // Title
    self.lblTitle.text = [App_Delegate.keyMapping getKeyValue:@"KEY_SHARE_TITLE"];
    
    // TextView settings
    self.shareMsgText.delegate = self;
    self.shareMsgText.textColor = [UIColor lightGrayColor];
    self.placeHolderString = [App_Delegate.keyMapping getKeyValue:@"KEY_SHARE_HINT"];
    self.shareMsgText.text = self.placeHolderString;
}

//---------------------------------------------------------------

@end
