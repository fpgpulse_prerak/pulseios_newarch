//
//  SocialButton.h
//  IN-Gauge
//
//  Created by Mehul Patel on 06/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "SLButton.h"

@interface SocialButton : UIView

@property (nonatomic, strong) UILabel                       *label;
@property (nonatomic, strong) SLButton                      *button;
@property (nonatomic, strong) UIImageView                   *image;

@end
