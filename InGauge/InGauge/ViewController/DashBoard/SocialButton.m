//
//  SocialButton.m
//  IN-Gauge
//
//  Created by Mehul Patel on 06/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "SocialButton.h"

@implementation SocialButton

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc {
    // Remove delegates, mutable array and observers here!
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (void) buttonTapped:(id) sender {
    //    [self.delegate buttonEventDetected:self.filterType sender:sender];
    
    self.transform = CGAffineTransformMakeScale(0.8, 0.8);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {}];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) layoutSetup {
    self.backgroundColor = [UIColor clearColor];
    
    _label = [[UILabel alloc] init];
    self.label.backgroundColor = [UIColor clearColor];
    self.label.textColor = [UIColor blackColor];
    self.label.font = [UIFont fontWithName:AVENIR_FONT size:14];
    [self.label setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:self.label];
    
    _button = [[SLButton alloc] init];
    self.button.backgroundColor = [UIColor clearColor];
    [self.button setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.button addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.button];
    
    _image = [[UIImageView alloc] init];
    self.image.backgroundColor = [UIColor clearColor];
    self.image.image = [UIImage imageNamed:DOWN_ARROW]; //
    [self.image setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:self.image];
    
    //    self.backgroundColor = [UIColor redColor];
    //        self.label.backgroundColor = [UIColor yellowColor];
    //    self.button.backgroundColor = [UIColor blueColor];
    //        self.image.backgroundColor = [UIColor brownColor];
    
    self.label.text = @"Share";
    self.image.image = [UIImage imageNamed:SHARE_ICON_IMAGE];
    
    // Constraints
    NSDictionary *views = @{@"label" : self.label, @"button" : self.button, @"image" : self.image};
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-2-[image(15)]-5-[label]-2-|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[button]|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[label]|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[button]|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[image(15)]" options:0 metrics:nil views:views]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.image
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1
                                                      constant:0]];
    
    [self.label setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    [self.label setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
    
    // Disable views
    //    [self setUserInteractionEnabled:NO];
    //    [self.label setEnabled:NO];
    //    [self.button setEnabled:NO];
    //    [self.image setTintColor:[UIColor grayColor]];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Init methods

//---------------------------------------------------------------

- (instancetype) init {
    self = [super init];
    [self layoutSetup];
    return self;
}

//---------------------------------------------------------------

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    return self;
}

//---------------------------------------------------------------

@end
