//
//  CommentCell.h
//  IN-Gauge
//
//  Created by Bharat Chandera on 22/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentCell : UICollectionViewCell 

@property (nonatomic, strong) UILabel               *userLabel;
@property (nonatomic, strong) UILabel               *daysLabel;
@property (nonatomic, strong) UILabel               *commentLabel;
@property (nonatomic, strong) UIImageView           *userImage;
@property (nonatomic, strong) UIView                *cardView;
@property (nonatomic, strong) UIView                *bottomView;

@property (nonatomic, strong) NSLayoutConstraint    *webViewHeightConstraint;
@property (nonatomic, strong) NSLayoutConstraint    *bottomViewHeightConstraint;


@end
