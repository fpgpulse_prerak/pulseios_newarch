//
//  CommentCell.m
//  IN-Gauge
//
//  Created by Bharat Chandera on 22/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "CommentCell.h"
#import "Helper.h"

@implementation CommentCell

// Initialize the collection cell based on the nscoder and add the tap gesture recognizer for custom animation
-(id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    return self;
}

// Initialize the collectiion cell and add the tap gesture recognizer for custom animation
-(id) init {
    self = [super init];
    return self;
}

//---------------------------------------------------------------

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor clearColor];
    
    _cardView = [[UIView alloc] init];
    self.cardView.translatesAutoresizingMaskIntoConstraints = NO;
    self.cardView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.cardView];
    
    CGFloat width = self.contentView.frame.size.width;
    NSDictionary *metrics = @{@"width" : [NSNumber numberWithFloat:width]};

    // Card view constraints
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[cardView(width)]-5-|" options: 0 metrics:metrics views:@{@"cardView" : self.cardView}]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[cardView]|" options:0 metrics:metrics views:@{@"cardView" : self.cardView}]];
    
    // User label
    _userLabel = [[UILabel alloc] init];
    self.userLabel.font = [UIFont fontWithName:AVENIR_FONT_HEAVY size:14];
    self.userLabel.numberOfLines = 0;
    self.userLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.userLabel.textAlignment = NSTextAlignmentLeft;
    [self.userLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.cardView addSubview:self.userLabel];
    
    // Days label
    _daysLabel = [[UILabel alloc] init];
    self.daysLabel.font = [UIFont fontWithName:AVENIR_FONT size:12];
    self.daysLabel.numberOfLines = 0;
    self.daysLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.daysLabel.textColor = [UIColor lightGrayColor];
    self.daysLabel.textAlignment = NSTextAlignmentRight;
    [self.daysLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.cardView addSubview:self.daysLabel];
    
    // Date label
    _commentLabel = [[UILabel alloc] init];
    self.commentLabel.font = [UIFont fontWithName:AVENIR_FONT size:12];
    self.commentLabel.numberOfLines = 0;
    self.commentLabel.textColor = [UIColor lightGrayColor];
    self.commentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.commentLabel.textAlignment = NSTextAlignmentLeft;
    [self.commentLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.cardView addSubview:self.commentLabel];
    
    // user image
    _userImage = [[UIImageView alloc] init];
    self.userImage.translatesAutoresizingMaskIntoConstraints = NO;
    [self.cardView addSubview:self.userImage];
    
    self.userImage.image = [UIImage imageNamed:@""];
    self.userImage.layer.cornerRadius = 22;
    self.userImage.clipsToBounds = YES;
    self.userImage.layer.masksToBounds = YES;
    
    self.daysLabel.text = @"10d";
    self.commentLabel.text = @"This is my first comment";
    self.userLabel.text = @"Prerak Thakur";
    //        self.userImage.backgroundColor     = [UIColor yellowColor];
    //        self.userLabel.backgroundColor     = [UIColor redColor];
    //        self.dateLabel.backgroundColor     = [UIColor brownColor];
    //        self.descLabel.backgroundColor     = [UIColor greenColor];
    
    // Constraints
    NSDictionary *views = @{@"userImage" : self.userImage, @"userLabel" : self.userLabel, @"daysLabel" : self.daysLabel, @"commentLabel" : self.commentLabel};
    //    self.userImage.hidden = YES;
    
    // User label & user image
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[userImage(44)]-8-[userLabel]-8-[daysLabel]-8-|" options: 0 metrics:metrics views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[userImage(44)]-8-[commentLabel]-8-|" options: 0 metrics:metrics views:views]];
    
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[userLabel(22)]-4-[commentLabel]-5-|" options: 0 metrics:metrics views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[userImage(44)]" options: 0 metrics:metrics views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[daysLabel]" options: 0 metrics:metrics views:views]];
    
//    [self.commentLabel setContentHuggingPriority:250 forAxis:UILayoutConstraintAxisHorizontal];
//    [self.commentLabel setContentCompressionResistancePriority:1000 forAxis:UILayoutConstraintAxisHorizontal];
    return self;
}

//---------------------------------------------------------------

- (void) layoutSubviews {
    [super layoutSubviews];
    [self.contentView layoutIfNeeded];
}

//---------------------------------------------------------------

- (void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    self.contentView.frame = bounds;
}

//---------------------------------------------------------------

@end

