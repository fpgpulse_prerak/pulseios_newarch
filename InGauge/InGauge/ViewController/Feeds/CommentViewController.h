//
//  CommentViewController.h
//  IN-Gauge
//
//  Created by Bharat Chandera on 22/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol CommentDelegate <NSObject>

- (void) updateCommentCount:(NSInteger)tag count:(NSInteger)count;
- (void) updateLikeCount:(NSInteger)tag count:(FeedModel *)model;

@end

@interface CommentViewController : BaseViewController

@property (nonatomic, weak) id <CommentDelegate>            delegate;
@property (nonatomic, strong) FeedModel                     *feedModel;
@property (nonatomic, strong) NSString                      *urlChartString;
@property (nonatomic, assign) NSInteger                     selectedTag;

@end
