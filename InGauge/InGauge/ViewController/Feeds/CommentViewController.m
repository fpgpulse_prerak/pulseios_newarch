//
//  CommentViewController.m
//  IN-Gauge
//
//  Created by Bharat Chandera on 22/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "CommentViewController.h"
#import "FeedsCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CommentCell.h"
#import "BABFrameObservingInputAccessoryView.h"
#import "AnalyticsLogger.h"

static NSString *FeedsCommentCellId = @"FeedsCommentCellId";

@interface CommentViewController () <UICollectionViewDelegate, UICollectionViewDataSource, WKNavigationDelegate, UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UICollectionView       *collectionView;
@property (nonatomic, weak) IBOutlet UIButton               *btnSendMsg;
@property (nonatomic, weak) IBOutlet UITextField            *textField;

@property (nonatomic, strong) NSArray                       *arrListGraph;
@property (nonatomic, strong) NSArray                       *dataArray;
@property (nonatomic, strong) NSArray                       *chartArray;
@property (nonatomic, strong) NSMutableArray                *isReloadArray;
@property (nonatomic, assign) BOOL                          isReload;

@end

@implementation CommentViewController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc {
    // Remove delegates and notification observers
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) commentSend:(id)sender {
    [self.textField resignFirstResponder];
    
    NSDictionary *requestDict = @{
                                  @"userId" : [App_Delegate.appfilter.loginUserId stringValue],
                                  @"mapTo" : @"FEEDCOMMENT",
                                  @"text" : self.textField.text,
                                  @"feedId" : [self.feedModel.feedId stringValue],
                                  @"industryId" : [App_Delegate.appfilter.industryId stringValue]
                                  };
    
    //Analytics
    [AnalyticsLogger logFeedsCommentEvent:self.feedModel.feedId.stringValue comment:self.textField.text];
    
    MBProgressHUD *hud = [Helper showLoading:self.view];
    [ServiceManager postWebServiceCallwithParam:ADDCOMMENT parameters:requestDict whenFinish:^(NSString *errorMessage, NSDictionary *response) {
        [Helper hideLoading:hud];
        if (errorMessage == nil) {
            self.textField.text = @"";
            self.btnSendMsg.enabled = NO;
            
            NSMutableArray *commentArray = [NSMutableArray arrayWithArray:self.feedModel.feedComments];
            [commentArray addObject:response[MAIN_BODY]];
            self.feedModel.feedComments = [NSArray arrayWithArray:commentArray];
            commentArray = nil;
            
            // update comment button
            FeedsCell *cell = ( FeedsCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            cell.commentButton.label.text = [NSString stringWithFormat:@"%d", (int)self.feedModel.feedComments.count];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update feeds
                [self.delegate updateCommentCount:self.selectedTag count:self.feedModel.feedComments.count];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.feedModel.feedComments.count inSection:0];
                [CATransaction begin];
                [CATransaction setDisableActions:YES];
                [self.collectionView performBatchUpdates:^{
                    [self.collectionView insertItemsAtIndexPaths:@[indexPath]];
                } completion:^(BOOL finished) {
                    [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:(UICollectionViewScrollPositionNone) animated:NO];
                }];
                [CATransaction commit];
            });
        }
    }];
}

//---------------------------------------------------------------

- (void) likeButtonClicked:(UIButton *)sender {
    
    if (![Helper checkForConnection:self]) {
        return;
    }
    
    LIRButtonView *likeButton = (LIRButtonView *)sender.superview;
    
    long likecount = [Helper getNumbersFromString:likeButton.label.text];
    if (sender.isSelected) {
        [sender setSelected:NO];
        likeButton.image.tintColor = SOCIAL_BUTTON_COLOR;
        self.feedModel.isLiked = [NSNumber numberWithBool:NO];
        likeButton.image.image = [UIImage imageNamed:LIKE_IMAGE];
        if (likecount > 0)
            likecount -= 1;
    } else {
        [sender setSelected:YES];
        self.feedModel.isLiked = [NSNumber numberWithBool:YES];
        likeButton.image.tintColor = GREEN_COLOR;
        likeButton.image.image = [UIImage imageNamed:LIKED_IMAGE];
        likecount += 1;
    }
    
    self.feedModel.noOfLikes = [NSString stringWithFormat:@"%ld",likecount];
    likeButton.label.text = [NSString stringWithFormat:@"%ld",likecount];
    
    MBProgressHUD *hud = [Helper showLoading:self.view];
    [ServiceManager getWebServiceCall:[NSString stringWithFormat:@"%@/%@/%@",USERFEEDLIKE,self.feedModel.feedId, App_Delegate.appfilter.loginUserId] parameters:nil whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        [Helper hideLoading:hud];

        if (response) {
            @try {
                [self.delegate updateLikeCount:self.selectedTag count:self.feedModel];
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
            }
        }
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

//---------------------------------------------------------------

#pragma mark
#pragma mark UICollectionView datasource methods

//---------------------------------------------------------------

- (NSInteger) numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

//---------------------------------------------------------------

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return (self.feedModel.feedComments.count + 1);
}

//---------------------------------------------------------------

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *collectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:FeedsCommentCellId forIndexPath:indexPath];
    
    if (indexPath.row == 0) {
        NSString *identifier = [NSString stringWithFormat:@"FeedsIdentifier_%d-%d-%d", (int)indexPath.section, (int)indexPath.row, (int)indexPath.item];
        [collectionView registerClass:[FeedsCell class] forCellWithReuseIdentifier:identifier];
        
        FeedsCell *cell = ( FeedsCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        cell.chartControl.navigationDelegate = self;
        cell.chartControl.tag = indexPath.row;
        
        cell.userLabel.text = [Helper removeWhiteSpaceAndNewLineCharacter:self.feedModel.createdBy.name];
        cell.dateLabel.text = self.feedModel.createdBy.jobTitle;
        cell.commentButton.label.text = [NSString stringWithFormat:@"%d",(int)self.feedModel.feedComments.count];
        cell.likeButton.label.text = [NSString stringWithFormat:@"%@" ,self.feedModel.noOfLikes];
        cell.likeButton.image.image = [UIImage imageNamed:LIKE_IMAGE];
        cell.commentButton.image.image = [UIImage imageNamed:COMMENT_IMAGE];
//        cell.bottomView.hidden = YES;
        cell.commentButton.button.enabled = NO;
        
        // Get user Avatar image
        NSString *action = [NSString stringWithFormat:@"%@/%d/%d", GET_USER_AVATAR, App_Delegate.appfilter.industryId.intValue, self.feedModel.createdBy.userID.intValue];

        // Convert server UTC time to local time
        NSDate *localTime = [Helper convertUTCTimeToLocalDate:self.feedModel.createdOn];
        NSInteger days = [Helper daysBetweenDate:localTime andDate:[NSDate date]];
        if (days == 0) {
            cell.daysLabel.text = [NSString stringWithFormat:@"0h"];
            NSInteger hours = [Helper hoursBetweenDate:[NSDate date] andDateTime:localTime];
            cell.daysLabel.text = [NSString stringWithFormat:@"%dh", (int)hours];
        } else {
            cell.daysLabel.text = [NSString stringWithFormat:@"%dd", (int)days];
        }
        
        NSString *imageURLString = [ServiceUtil addHeaderParameters:action withParams:nil];
        [cell.userImage sd_setImageWithURL:[NSURL URLWithString:imageURLString] placeholderImage:[UIImage imageNamed:@"defaultimg"] options:SDWebImageRefreshCached];
        
        cell.descLabel.text = [Helper removeWhiteSpaceAndNewLineCharacter:self.feedModel.feedDescription];
        
        // Load charrts
        NSString* encodedUrl = [self.urlChartString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:encodedUrl]];
        
        if (self.isReload) {
            if (!cell.chartControl.isLoading) {
                [cell.chartControl loadRequest:theRequest];                
            }
        }        
        
        cell.likeButton.button.tag = indexPath.row;
        [cell.likeButton.button addTarget:self action:@selector(likeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        if (self.feedModel.isLiked.boolValue) {
            [cell.likeButton.button setSelected:YES];
            cell.likeButton.image.image = [UIImage imageNamed:LIKED_IMAGE];
            cell.likeButton.image.tintColor = GREEN_COLOR;
        } else {
            [cell.likeButton.button setSelected:NO];
            cell.likeButton.image.image = [UIImage imageNamed:LIKE_IMAGE];
            cell.likeButton.image.tintColor = SOCIAL_BUTTON_COLOR;
        }
        
        collectionViewCell = cell;
        
    } else {
        NSString *identifier = [NSString stringWithFormat:@"CommentCell_%d-%d-%d", (int)indexPath.section, (int)indexPath.row, (int)indexPath.item];

        [collectionView registerClass:[CommentCell class] forCellWithReuseIdentifier:identifier];
        CommentCell *cell = (CommentCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        NSDictionary *commenterDict = [self.feedModel.feedComments objectAtIndex:indexPath.row-1];
        
         cell.userLabel.text = [Helper removeWhiteSpaceAndNewLineCharacter:commenterDict[@"user"][@"name"]];
         cell.commentLabel.text = [Helper removeWhiteSpaceAndNewLineCharacter:commenterDict[@"text"]];

        // Get user Avatar image
        NSString *action = [NSString stringWithFormat:@"%@/%d/%d", GET_USER_AVATAR, App_Delegate.appfilter.industryId.intValue,[commenterDict[@"user"][@"id"] intValue]];
        
        NSInteger days = [Helper daysBetweenDate:[Helper convertUTCTimeToLocalDate:commenterDict[@"createdOn"]] andDate:[NSDate date]];
        if (days == 0) {
            cell.daysLabel.text = [NSString stringWithFormat:@"0h"];
        } else {
            cell.daysLabel.text = [NSString stringWithFormat:@"%dd", (int)days];
        }
        
        NSString *imageURLString = [ServiceUtil addHeaderParameters:action withParams:nil];
        [cell.userImage sd_setImageWithURL:[NSURL URLWithString:imageURLString] placeholderImage:[UIImage imageNamed:@"defaultimg"] options:SDWebImageRefreshCached];
        
        collectionViewCell = cell;
        
    }
    return collectionViewCell;
}

//---------------------------------------------------------------

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    //You may want to create a divider to scale the size by the way..
    return CGSizeMake(self.view.frame.size.width - 16, 100);
}

//---------------------------------------------------------------

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeMake(self.view.frame.size.width, 40);
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITextField methods

//---------------------------------------------------------------

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *trimString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSString *trunctString = [Helper removeWhiteSpaceAndNewLineCharacter:trimString];
    
    
    if (trunctString.length >= COMMENT_MAX_LENGTH) {
        return NO;
    } else {
        self.btnSendMsg.enabled = (trunctString.length > 0) ? YES : NO;
        return YES;
    }
}

//---------------------------------------------------------------

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.textField resignFirstResponder];
    return YES;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WKNavigationDelegate methods

//---------------------------------------------------------------

- (void) webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    NSString *javascript = @"var meta = document.createElement('meta');meta.setAttribute('name', 'viewport');meta.setAttribute('content', 'width=device-width, initial-scale=0.0, maximum-scale=0.0, user-scalable=no');document.getElementsByTagName('head')[0].appendChild(meta);";
    [webView evaluateJavaScript:javascript completionHandler:nil];
}

//---------------------------------------------------------------

- (void) webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
    UIView *contentView = webView.superview;
    FeedsCell *cell = (FeedsCell *)contentView.superview.superview;
    DLog(@"#### URL :%@", webView.URL.absoluteString);
    
    [webView evaluateJavaScript:@"document.getElementsByClassName('x_panel')[0].offsetHeight;" completionHandler:^(id response, NSError * _Nullable error) {
        CGFloat height = [NSString stringWithFormat:@"%@", response].floatValue;
        cell.chartControl.hidden = NO;
        self.isReload = NO;
        [cell.activity stopAnimating];
        
        [UIView animateWithDuration:0.5 animations:^{
            cell.webViewHeightConstraint.constant = height;
            [cell.chartControl layoutIfNeeded];
            // Reload cell
            [self.collectionView reloadData];
        }];
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View LifeCycle methods

//---------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.isReload = YES;
    self.btnSendMsg.enabled = NO;
    
    // Do any additional setup after loading the view.
    [self.collectionView setBackgroundColor:[UIColor clearColor]];
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:FeedsCommentCellId];
    
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    flowLayout.estimatedItemSize = CGSizeMake(self.view.frame.size.width, 100);
    
    BABFrameObservingInputAccessoryView *inputView = [[BABFrameObservingInputAccessoryView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 0)];
    inputView.userInteractionEnabled = NO;
    self.textField.inputAccessoryView = inputView;
    
    __weak typeof(self)weakSelf = self;
    inputView.keyboardFrameChangedBlock = ^(BOOL keyboardVisible, CGRect keyboardFrame) {
        [weakSelf.view layoutIfNeeded];
    };
}

//---------------------------------------------------------------

@end
