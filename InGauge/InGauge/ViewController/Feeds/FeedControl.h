//
//  FeedControl.h
//  IN-Gauge
//
//  Created by Mehul Patel on 11/08/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LIRButtonView.h"
#import "FeedModel.h"
#import "Helper.h"

@import WebKit;

@interface FeedControl : UIView <UIScrollViewDelegate, WKNavigationDelegate>

@property (nonatomic, strong) UILabel               *userLabel;
@property (nonatomic, strong) UILabel               *daysLabel;
@property (nonatomic, strong) UILabel               *dateLabel;
@property (nonatomic, strong) UILabel               *descLabel;
@property (nonatomic, strong) UIImageView           *userImage;
@property (nonatomic, strong) WKWebView             *chartControl;
@property (nonatomic, strong) UIActivityIndicatorView      *activity;
@property (nonatomic, strong) UIView                *cardView;
@property (nonatomic, strong) UIView                *bottomView;
@property (nonatomic, strong) LIRButtonView         *shareButton;
@property (nonatomic, strong) LIRButtonView         *commentButton;
@property (nonatomic, strong) LIRButtonView         *likeButton;

@property (nonatomic, strong) NSLayoutConstraint    *webViewHeightConstraint;
@property (nonatomic, strong) NSLayoutConstraint    *bottomViewHeightConstraint;
@property (nonatomic, strong) NSString                      *chartUrlString;

- (void) setFeedDetails:(FeedModel *)model;
@end
