//
//  FeedControl.m
//  IN-Gauge
//
//  Created by Mehul Patel on 11/08/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "FeedControl.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ServiceUtil.h"

@implementation FeedControl

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc {
    // Remove delegates, mutable array and observers here!
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) setFeedDetails:(FeedModel *)model {
    
    UIColor *imageGrayColor = SOCIAL_BUTTON_COLOR;
    self.userLabel.text = [Helper removeWhiteSpaceAndNewLineCharacter:model.createdBy.name];
    self.dateLabel.text = model.createdBy.jobTitle;
    self.likeButton.label.text = [NSString stringWithFormat:@"%@", model.noOfLikes];
    self.likeButton.image.image = [UIImage imageNamed:LIKE_IMAGE];
    
    self.commentButton.label.text = [NSString stringWithFormat:@"%@",model.noOfComments];
    self.commentButton.image.image = [UIImage imageNamed:COMMENT_IMAGE];
    self.commentButton.image.tintColor = imageGrayColor;
    
    // Get user Avatar image
    NSString *action = [NSString stringWithFormat:@"%@/%d/%d", GET_USER_AVATAR, App_Delegate.appfilter.industryId.intValue, model.createdBy.userID.intValue];
    
    // Convert server UTC time to local time
    NSDate *localTime = [Helper convertUTCTimeToLocalDate:model.createdOn];
    NSInteger days = [Helper daysBetweenDate:localTime andDate:[NSDate date]];
    if (days == 0) {
        self.daysLabel.text = [NSString stringWithFormat:@"0h"];
        NSInteger hours = [Helper hoursBetweenDate:[NSDate date] andDateTime:localTime];
        self.daysLabel.text = [NSString stringWithFormat:@"%dh", (int)hours];
    } else {
        self.daysLabel.text = [NSString stringWithFormat:@"%dd", (int)days];
    }
    
    // Avtar image
    NSString *imageURLString = [ServiceUtil addHeaderParameters:action withParams:nil];
    [self.userImage sd_setImageWithURL:[NSURL URLWithString:imageURLString] placeholderImage:[UIImage imageNamed:@"defaultimg"] options:SDWebImageRefreshCached];
    
    // Like
    if (model.isLiked.boolValue) {
        self.likeButton.image.image = [UIImage imageNamed:LIKED_IMAGE];
        [self.likeButton.button setSelected:YES];
        self.likeButton.image.tintColor = GREEN_COLOR;
    } else {
        self.likeButton.image.image = [UIImage imageNamed:LIKE_IMAGE];
        [self.likeButton.button setSelected:NO];
        self.likeButton.image.tintColor = imageGrayColor;
    }

    // Description / Comments / Share report for
    self.descLabel.text = [Helper removeWhiteSpaceAndNewLineCharacter:model.feedDescription];
//    if ([[model.reportDetails allKeys] containsObject:@"subType"]) {
//        NSNumber *subType = [model.reportDetails valueForKey:@"subType"];
//        if (subType.intValue == 1) {
//            // Clear comment text if subType = 1 (Share by comment option)
//            // Meaning - if report is shared using comment(from Share page), it will show comment in a box
//            // As comment is inline with report box, this comment text does not need to display!
//            self.descLabel.text = @"";
//        }
//    }
}

//---------------------------------------------------------------

- (id) init {
    self = [super init];
    
    self.backgroundColor = BACK_COLOR;
    
    _cardView = [[UIView alloc] init];
    self.cardView.translatesAutoresizingMaskIntoConstraints = NO;
    self.cardView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.cardView];
    
    // Card view constraints
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cardView]|" options: 0 metrics:nil views:@{@"cardView" : self.cardView}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[cardView]|" options:0 metrics:nil views:@{@"cardView" : self.cardView}]];
    
    // User label
    _userLabel = [[UILabel alloc] init];
    self.userLabel.font = [UIFont fontWithName:AVENIR_FONT_HEAVY size:14];
    self.userLabel.numberOfLines = 0;
    self.userLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.userLabel.textAlignment = NSTextAlignmentLeft;
    [self.userLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.cardView addSubview:self.userLabel];
    
    // Days label
    _daysLabel = [[UILabel alloc] init];
    self.daysLabel.font = [UIFont fontWithName:AVENIR_FONT size:12];
    self.daysLabel.numberOfLines = 0;
    self.daysLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.daysLabel.textColor = [UIColor lightGrayColor];
    self.daysLabel.textAlignment = NSTextAlignmentRight;
    [self.daysLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.cardView addSubview:self.daysLabel];
    
    // Date label
    _dateLabel = [[UILabel alloc] init];
    self.dateLabel.font = [UIFont fontWithName:AVENIR_FONT size:12];
    self.dateLabel.numberOfLines = 0;
    self.dateLabel.textColor = [UIColor lightGrayColor];
    self.dateLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.dateLabel.textAlignment = NSTextAlignmentLeft;
    [self.dateLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.cardView addSubview:self.dateLabel];
    
    // Description label
    _descLabel = [[UILabel alloc] init];
    self.descLabel.textColor = [UIColor lightGrayColor];
    self.descLabel.font = [UIFont fontWithName:AVENIR_FONT size:12];
    self.descLabel.numberOfLines = 0;
    self.descLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.descLabel.textAlignment = NSTextAlignmentLeft;
    [self.descLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.cardView addSubview:self.descLabel];
    
    // user image
    _userImage = [[UIImageView alloc] init];
    self.userImage.translatesAutoresizingMaskIntoConstraints = NO;
    [self.cardView addSubview:self.userImage];
    
    self.userImage.image = [UIImage imageNamed:@""];
    self.userImage.layer.cornerRadius = 25;
    self.userImage.clipsToBounds = YES;
    self.userImage.layer.masksToBounds = YES;
    
    // Chart
    _chartControl = [[WKWebView alloc] init];
    [self.chartControl.scrollView setScrollEnabled:NO];
    [self.chartControl.scrollView setBounces:NO];
//    [self.chartControl.scrollView setBouncesZoom:NO];
    self.chartControl.navigationDelegate = self;
    self.chartControl.translatesAutoresizingMaskIntoConstraints = NO;
    [self.cardView addSubview:self.chartControl];
    self.chartControl.hidden = YES;
    
    // BottomView
    _bottomView = [[UIControl alloc] init];
    self.bottomView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.cardView addSubview:self.bottomView];
    
    _activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [self.activity setColor:[UIColor grayColor]];
    self.activity.translatesAutoresizingMaskIntoConstraints = NO;
    [self.cardView addSubview:self.activity];
    [self.activity startAnimating];
    
    self.daysLabel.text = @"10d";
    //        self.userImage.backgroundColor     = [UIColor yellowColor];
//            self.userLabel.backgroundColor     = [UIColor redColor];
//            self.dateLabel.backgroundColor     = [UIColor brownColor];
//            self.daysLabel.backgroundColor     = [UIColor blueColor];
//            self.descLabel.backgroundColor     = [UIColor greenColor];
    
    // Constraints
    NSDictionary *views = @{@"userImage" : self.userImage, @"userLabel" : self.userLabel, @"daysLabel" : self.daysLabel, @"dateLabel" : self.dateLabel, @"descLabel" : self.descLabel, @"chartControl" : self.chartControl, @"bottomView" : self.bottomView, @"activity" : self.activity};
    
    // Horizontal constraints
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[userImage(50)]" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[daysLabel]-10-|" options: 0 metrics:nil views:views]];

    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-70-[userLabel]" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-70-[dateLabel]" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[descLabel]-10-|" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[chartControl]-10-|" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[bottomView]-5-|" options: 0 metrics:nil views:views]];
    
    // user label width
    [self.cardView addConstraint:[NSLayoutConstraint constraintWithItem:self.userLabel
                                                              attribute:NSLayoutAttributeWidth
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.cardView
                                                              attribute:NSLayoutAttributeWidth
                                                             multiplier:0.7
                                                               constant:1]];

    
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[userImage(50)]" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[userLabel]-0-[dateLabel]" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[daysLabel]" options: 0 metrics:nil views:views]];
    
    
    // Description label
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-70-[descLabel]-5-[chartControl]-45-|" options: 0 metrics:nil views:views]];
    
    // Bottom control
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[bottomView(44)]|" options: 0 metrics:nil views:views]];
    
    // WebView height constraints = this will allow dynamic height for chart
    _webViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.chartControl
                                                            attribute:NSLayoutAttributeHeight
                                                            relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                               toItem:nil
                                                            attribute:NSLayoutAttributeNotAnAttribute
                                                           multiplier:1.0
                                                             constant:60];
    
    self.webViewHeightConstraint.priority = 999;
    [self.cardView addConstraint:self.webViewHeightConstraint];
    
    // Activity indicator constraints
    [self.cardView addConstraint:[NSLayoutConstraint constraintWithItem:self.activity
                                                              attribute:NSLayoutAttributeCenterX
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.cardView
                                                              attribute:NSLayoutAttributeCenterX
                                                             multiplier:1.0
                                                               constant:0]];
    
    [self.cardView addConstraint:[NSLayoutConstraint constraintWithItem:self.activity
                                                              attribute:NSLayoutAttributeCenterY
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.cardView
                                                              attribute:NSLayoutAttributeCenterY
                                                             multiplier:1.0
                                                               constant:0]];
    // Create bottomview
    [self createBottomView];
    
    // Drop shadow
    self.cardView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    self.cardView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    self.cardView.layer.shadowRadius = 0.5f;
    self.cardView.layer.shadowOpacity = 2.0f;
    
    return self;
}

//---------------------------------------------------------------

- (void) layoutSubviews {
    [super layoutSubviews];
    [self layoutIfNeeded];
}

//---------------------------------------------------------------

- (void) createBottomView {
    _commentButton = [[LIRButtonView alloc] init];
    self.commentButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.bottomView addSubview:self.commentButton];
    
    _likeButton = [[LIRButtonView alloc] init];
    self.likeButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.bottomView addSubview:self.likeButton];
    
    // Constraints
    NSDictionary *views = @{ @"commentButton" : self.commentButton, @"likeButton" : self.likeButton};
    
    [self.bottomView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[likeButton]" options: 0 metrics:nil views:views]];
    [self.bottomView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[commentButton]-|" options: 0 metrics:nil views:views]];
    
    [self.bottomView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[commentButton]|" options: 0 metrics:nil views:views]];
    [self.bottomView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[likeButton]|" options: 0 metrics:nil views:views]];
    [self addTopLine:self.bottomView];
}

//---------------------------------------------------------------

- (void) addTopLine:(UIView *)headerView {
    // Add line
    UIView *lineView = [[UIView alloc] init];
    [lineView setBackgroundColor:TABLE_SEPERATOR_COLOR];
    [lineView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [headerView addSubview:lineView];
    
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[lineView]|" options: 0 metrics:nil views:@{@"lineView" : lineView}]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[lineView(1)]" options: 0 metrics:nil views:@{@"lineView" : lineView}]];
}

//---------------------------------------------------------------

- (void) addBottomLine:(UIView *)headerView {
    // Add line
    UIView *lineView = [[UIView alloc] init];
    [lineView setBackgroundColor:TABLE_SEPERATOR_COLOR];
    [lineView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [headerView addSubview:lineView];
    
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[lineView]|" options: 0 metrics:nil views:@{@"lineView" : lineView}]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[lineView(1)]|" options: 0 metrics:nil views:@{@"lineView" : lineView}]];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WKNavigationDelegate methods

//---------------------------------------------------------------

- (void) webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    NSString *javascript = @"var meta = document.createElement('meta');meta.setAttribute('name', 'viewport');meta.setAttribute('content', 'width=device-width, initial-scale=0.0, maximum-scale=0.0, user-scalable=no');document.getElementsByTagName('head')[0].appendChild(meta);";
    [webView evaluateJavaScript:javascript completionHandler:nil];
}

//---------------------------------------------------------------

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [webView evaluateJavaScript:@"document.getElementsByClassName('x_panel')[0].offsetHeight;" completionHandler:^(id response, NSError * _Nullable error) {
        CGFloat height = [NSString stringWithFormat:@"%@", response].floatValue;
        
        [UIView animateWithDuration:0.5 animations:^{
            self.chartControl.hidden = NO;
            [self.activity stopAnimating];
 
            self.webViewHeightConstraint.constant = height;
            [self.chartControl layoutIfNeeded];
            [self.chartControl setNeedsUpdateConstraints];
            [self.chartControl setNeedsLayout];
        }];
        [self layoutIfNeeded];
        [self setNeedsUpdateConstraints];                
    }];
}

//---------------------------------------------------------------

@end
