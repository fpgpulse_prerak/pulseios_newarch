//
//  FeedsCell.h
//  InGauge
//
//  Created by Mehul Patel on 24/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LIRButtonView.h"

@import WebKit;

@interface FeedsCell : UICollectionViewCell <UIScrollViewDelegate>

@property (nonatomic, strong) UILabel               *userLabel;
@property (nonatomic, strong) UILabel               *daysLabel;
@property (nonatomic, strong) UILabel               *dateLabel;
@property (nonatomic, strong) UILabel               *descLabel;
@property (nonatomic, strong) UIImageView           *userImage;
@property (nonatomic, strong) WKWebView             *chartControl;
@property (nonatomic, strong) UIActivityIndicatorView      *activity;
@property (nonatomic, strong) UIView                *cardView;
@property (nonatomic, strong) UIView                *bottomView;
@property (nonatomic, strong) LIRButtonView         *shareButton;
@property (nonatomic, strong) LIRButtonView         *commentButton;
@property (nonatomic, strong) LIRButtonView         *likeButton;

@property (nonatomic, strong) NSLayoutConstraint    *webViewHeightConstraint;
@property (nonatomic, strong) NSLayoutConstraint    *bottomViewHeightConstraint;

@end
