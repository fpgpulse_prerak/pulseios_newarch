//
//  FeedsCell.m
//  InGauge
//
//  Created by Mehul Patel on 24/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "FeedsCell.h"
#import "Helper.h"

@implementation FeedsCell

// Initialize the collection cell based on the nscoder and add the tap gesture recognizer for custom animation
-(id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    return self;
}

// Initialize the collectiion cell and add the tap gesture recognizer for custom animation
-(id) init {
    self = [super init];
    return self;
}

//---------------------------------------------------------------

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    self.backgroundColor = BACK_COLOR;
    
    _cardView = [[UIView alloc] init];
    self.cardView.translatesAutoresizingMaskIntoConstraints = NO;
    self.cardView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.cardView];
    
    // Card view constraints
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cardView]|" options: 0 metrics:nil views:@{@"cardView" : self.cardView}]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[cardView]|" options:0 metrics:nil views:@{@"cardView" : self.cardView}]];
    
    // User label
    _userLabel = [[UILabel alloc] init];
    self.userLabel.font = [UIFont fontWithName:AVENIR_FONT_HEAVY size:14];
    self.userLabel.numberOfLines = 0;
    self.userLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.userLabel.textAlignment = NSTextAlignmentLeft;
    [self.userLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.cardView addSubview:self.userLabel];
    
    // Days label
    _daysLabel = [[UILabel alloc] init];
    self.daysLabel.font = [UIFont fontWithName:AVENIR_FONT size:12];
    self.daysLabel.numberOfLines = 0;
    self.daysLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.daysLabel.textColor = [UIColor lightGrayColor];
    self.daysLabel.textAlignment = NSTextAlignmentRight;
    [self.daysLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.cardView addSubview:self.daysLabel];
    
    // Date label
    _dateLabel = [[UILabel alloc] init];
    self.dateLabel.font = [UIFont fontWithName:AVENIR_FONT size:12];
    self.dateLabel.numberOfLines = 0;
    self.dateLabel.textColor = [UIColor lightGrayColor];
    self.dateLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.dateLabel.textAlignment = NSTextAlignmentLeft;
    [self.dateLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.cardView addSubview:self.dateLabel];
    
    // Description label
    _descLabel = [[UILabel alloc] init];
    self.descLabel.textColor = [UIColor lightGrayColor];
    self.descLabel.font = [UIFont fontWithName:AVENIR_FONT size:12];
    self.descLabel.numberOfLines = 0;
    self.descLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.descLabel.textAlignment = NSTextAlignmentLeft;
    [self.descLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.cardView addSubview:self.descLabel];
    
    // user image
    _userImage = [[UIImageView alloc] init];
    self.userImage.translatesAutoresizingMaskIntoConstraints = NO;
    [self.cardView addSubview:self.userImage];
    
    self.userImage.image = [UIImage imageNamed:@""];
    self.userImage.layer.cornerRadius = 25;
    self.userImage.clipsToBounds = YES;
    self.userImage.layer.masksToBounds = YES;
    
    // Chart
    _chartControl = [[WKWebView alloc] init];
    [self.chartControl.scrollView setScrollEnabled:NO];
    [self.chartControl.scrollView setBounces:NO];
    [self.chartControl.scrollView setBouncesZoom:NO];
    self.chartControl.translatesAutoresizingMaskIntoConstraints = NO;
    [self.cardView addSubview:self.chartControl];
    self.chartControl.hidden = YES;
    
    // BottomView
    _bottomView = [[UIControl alloc] init];
    self.bottomView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.cardView addSubview:self.bottomView];
    
    _activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [self.activity setColor:[UIColor grayColor]];
    self.activity.translatesAutoresizingMaskIntoConstraints = NO;
    [self.cardView addSubview:self.activity];
    [self.activity startAnimating];
    
    self.daysLabel.text = @"10d";
    //        self.userImage.backgroundColor     = [UIColor yellowColor];
    //        self.userLabel.backgroundColor     = [UIColor redColor];
    //        self.dateLabel.backgroundColor     = [UIColor brownColor];
    //        self.descLabel.backgroundColor     = [UIColor greenColor];
    
    // Constraints
    NSDictionary *views = @{@"userImage" : self.userImage, @"userLabel" : self.userLabel, @"daysLabel" : self.daysLabel, @"dateLabel" : self.dateLabel, @"descLabel" : self.descLabel, @"chartControl" : self.chartControl, @"bottomView" : self.bottomView, @"activity" : self.activity};
    
    // User label & user image
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[userImage(50)]" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[userLabel]-0-[dateLabel]" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[daysLabel]" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[userImage(50)]-10-[userLabel]-[daysLabel]-10-|" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[userImage(50)]-10-[dateLabel]" options: 0 metrics:nil views:views]];
    
    
    // Description label
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-70-[descLabel]-5-[chartControl]-1-[bottomView]-0-|" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[descLabel]-10-|" options: 0 metrics:nil views:views]];
    
    
    // Chart control
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[chartControl]-10-|" options: 0 metrics:nil views:views]];
    
    // Bottom control
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[bottomView]-0-|" options: 0 metrics:nil views:views]];
    
    // Priorities constraints
    [self.descLabel setContentHuggingPriority:250 forAxis:UILayoutConstraintAxisVertical];
    [self.bottomView setContentHuggingPriority:250 forAxis:UILayoutConstraintAxisVertical];
    
    // WebView height constraints = this will allow dynamic height for chart
    _webViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.chartControl
                                                            attribute:NSLayoutAttributeHeight
                                                            relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                               toItem:nil
                                                            attribute:NSLayoutAttributeNotAnAttribute
                                                           multiplier:1.0
                                                             constant:60];
    
    self.webViewHeightConstraint.priority = 999;
    [self.cardView addConstraint:self.webViewHeightConstraint];
    
    CGFloat widthConstant = self.frame.size.width - 20;
    NSLayoutConstraint *widthConstraint = [NSLayoutConstraint constraintWithItem:self.chartControl
                                                                       attribute:NSLayoutAttributeWidth
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:nil
                                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                                      multiplier:1.0
                                                                        constant:widthConstant];
    [self.cardView addConstraint:widthConstraint];
    
    
    // Activity indicator constraints
    [self.cardView addConstraint:[NSLayoutConstraint constraintWithItem:self.activity
                                                              attribute:NSLayoutAttributeCenterX
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.cardView
                                                              attribute:NSLayoutAttributeCenterX
                                                             multiplier:1.0
                                                               constant:0]];
    
    [self.cardView addConstraint:[NSLayoutConstraint constraintWithItem:self.activity
                                                              attribute:NSLayoutAttributeCenterY
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.cardView
                                                              attribute:NSLayoutAttributeCenterY
                                                             multiplier:1.0
                                                               constant:0]];
    // Create bottomview
    [self createBottomView];
    
    // Drop shadow
    self.cardView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    self.cardView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    self.cardView.layer.shadowRadius = 0.5f;
    self.cardView.layer.shadowOpacity = 2.0f;
    
    return self;
}

//---------------------------------------------------------------

- (void) layoutSubviews {
    [super layoutSubviews];
    [self.contentView layoutIfNeeded];
}

//---------------------------------------------------------------

- (void) createBottomView {
    _commentButton = [[LIRButtonView alloc] init];
    self.commentButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.bottomView addSubview:self.commentButton];
    self.commentButton.image.tintColor = SOCIAL_BUTTON_COLOR;
    
    _likeButton = [[LIRButtonView alloc] init];
    self.likeButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.bottomView addSubview:self.likeButton];

    // Constraints
    NSDictionary *views = @{ @"commentButton" : self.commentButton, @"likeButton" : self.likeButton};
    
    [self.bottomView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[likeButton]" options: 0 metrics:nil views:views]];
    [self.bottomView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[commentButton]-|" options: 0 metrics:nil views:views]];
    
    [self.bottomView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[commentButton]|" options: 0 metrics:nil views:views]];
    [self.bottomView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[likeButton]|" options: 0 metrics:nil views:views]];
    
    // Top line
    [self addTopLine:self.bottomView];
}

//---------------------------------------------------------------

- (void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    self.contentView.frame = bounds;
}

//---------------------------------------------------------------

- (void) addTopLine:(UIView *)headerView {
    // Add line
    UIView *lineView = [[UIView alloc] init];
    [lineView setBackgroundColor:TABLE_SEPERATOR_COLOR];
    [lineView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [headerView addSubview:lineView];
    
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[lineView]|" options: 0 metrics:nil views:@{@"lineView" : lineView}]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[lineView(1)]" options: 0 metrics:nil views:@{@"lineView" : lineView}]];
}

//---------------------------------------------------------------

- (void) addBottomLine:(UIView *)headerView {
    // Add line
    UIView *lineView = [[UIView alloc] init];
    [lineView setBackgroundColor:TABLE_SEPERATOR_COLOR];
    [lineView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [headerView addSubview:lineView];
    
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[lineView]|" options: 0 metrics:nil views:@{@"lineView" : lineView}]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[lineView(1)]|" options: 0 metrics:nil views:@{@"lineView" : lineView}]];
}

//---------------------------------------------------------------

@end
