	//
//  FeedsViewController.m
//  InGauge
//
//  Created by Mehul Patel on 24/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "FeedsViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CommentViewController.h"
#import "FeedControl.h"
#import "LGRefreshView.h"
#import "AnalyticsLogger.h"
#import "MainDashboard.h"

#define COMMENT_TAG 10000
#define LIKE_TAG    20000
#define CHART_TAG   30000

#define LOAD_MORE_TAG   99999
#define ITEM_PER_VIEW   4

@interface FeedsViewController () <CommentDelegate, NSURLSessionDelegate>

@property (nonatomic, strong) UIScrollView                  *scrollView;
@property (nonatomic, strong) UIButton                      *loadMoreButton;
@property (nonatomic, strong) UILabel                       *navigationLabel;
@property (nonatomic, strong) UILabel                       *topLabel;
@property (nonatomic, strong) UIView                        *noRecordsView;
@property (nonatomic, strong) LGRefreshView                 *refreshView;
@property (nonatomic, strong) FeedList                      *feedList;
@property (nonatomic, strong) FeedModel                     *feedModel;
@property (nonatomic, assign) FeedControl                   *lastView;
@property (nonatomic, strong) NSArray                       *dataArray;
@property (nonatomic, strong) NSArray                       *remainingArray;
@property (nonatomic, strong) NSArray                       *arrListGraph;
@property (nonatomic, strong) NSArray                       *chartArray;
@property (nonatomic, strong) NSIndexPath                   *selectedIndex;
@property (nonatomic, assign) NSInteger                     selectedTag;
@property (nonatomic, assign) NSInteger                     pageNo;

@end

@implementation FeedsViewController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) tenantChangeButtonTapped:(id)sender {
    [self changeTenantAndIndustry];
}

//---------------------------------------------------------------

- (void) commentButtonClicked:(UIButton *)sender {
    
    NSInteger index = sender.tag - COMMENT_TAG;
    self.selectedTag = index + CHART_TAG;
    self.selectedIndex = [NSIndexPath indexPathForRow:index inSection:0];
    self.feedModel = [self.dataArray objectAtIndex:index];
    [self performSegueWithIdentifier:@"CommentViewController" sender:sender];
}

//---------------------------------------------------------------

- (void) likeButtonClicked:(UIButton *)sender {
    
    if (![Helper checkForConnection:self]) {
        return;
    }
    NSInteger index = sender.tag - LIKE_TAG;
    
    LIRButtonView *likeButton = (LIRButtonView *)sender.superview;
    FeedModel *feedData = [self.dataArray objectAtIndex:index];
    
    long likecount = [Helper getNumbersFromString:likeButton.label.text];
    BOOL isLike = NO;
    if (sender.isSelected) {
        [sender setSelected:NO];
        likeButton.image.image = [UIImage imageNamed:LIKE_IMAGE];
        likeButton.image.tintColor = SOCIAL_BUTTON_COLOR;
        isLike = NO;
        if (likecount > 0)
            likecount -= 1;
    } else {
        [sender setSelected:YES];
        likeButton.image.image = [UIImage imageNamed:LIKED_IMAGE];
        likeButton.image.tintColor = GREEN_COLOR;
        likecount += 1;
        isLike = YES;
    }
    
    //Analytics
    [AnalyticsLogger logFeedsLikeEvent:feedData.feedId.stringValue isLike:[NSNumber numberWithBool:!sender.isSelected]];
    
    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.dataArray];
    if (feedData) {
        feedData.noOfLikes = [NSString stringWithFormat:@"%ld",likecount];
        feedData.isLiked = [NSNumber numberWithBool:isLike];
        [tempArray replaceObjectAtIndex:index withObject:feedData];
    }
    self.dataArray = [NSArray arrayWithArray:tempArray];
    tempArray = nil;
    
    likeButton.label.text = [NSString stringWithFormat:@"%ld",likecount];
    NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@",USERFEEDLIKE,feedData.feedId, App_Delegate.appfilter.loginUserId];
    [ServiceManager getWebServiceCall:urlString parameters:nil whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        if (response) {
            @try {
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
            }
        }
    }];
}

//---------------------------------------------------------------

- (void) openTenantTapped:(UIButton *)sender {
    [self tenantChangeButtonTapped:nil];
}

//---------------------------------------------------------------

- (void) loadMoreButtonTapped:(UIButton *)sender {
    [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {        
    }];
    
    self.pageNo = self.pageNo + 1;
    
    // Add more from array
    NSArray *itemsForView = [NSArray new];
    NSInteger limit = self.dataArray.count + 4;
    if (limit >= self.remainingArray.count) {
        NSInteger remainingCount = self.remainingArray.count - self.dataArray.count;
        itemsForView = [self.remainingArray subarrayWithRange:NSMakeRange(self.dataArray.count, remainingCount)];
    } else {
        itemsForView = [self.remainingArray subarrayWithRange:NSMakeRange(self.dataArray.count, ITEM_PER_VIEW)];
    }
    
    // add more array
    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.dataArray];
    [tempArray addObjectsFromArray:itemsForView];
    self.dataArray = [NSArray arrayWithArray:tempArray];
    tempArray = nil;
    // Create chart URL strings
    [self createChartURLString:self.dataArray];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WEBSERVICE methods

//---------------------------------------------------------------

- (void) callWebServiceToGetFeedsList {
    // Check for internet connnection
    if (![Helper checkForConnection:self]) {
        return;
    }
    
    MBProgressHUD *hud = [Helper showLoading:self.view];
    DLog(@"\n\n\n\n==========>>> Feeds - Service Call Begin <<<==========\n\n\n\n");
    NSDictionary *params = [App_Delegate.appfilter serializeFiltersForFeedsService];
    [ServiceManager getWebServiceCall:GET_FEEDS_ACTION parameters:params whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
    DLog(@"\n\n\n\n==========>>> Feeds - Service Call finish <<<==========\n\n\n\n");    
        [Helper hideLoading:hud];
        
        UINavigationController *navigationController =  (UINavigationController *)self.parentViewController;
        MainDashboard *tabBar = (MainDashboard*)navigationController.parentViewController;
        tabBar.feeds = response;
        [self updateUI:response];
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) updateUI:(NSDictionary *)response {
    _feedList = [FeedList new];
    
    self.pageNo = 0;
    self.dataArray = nil;
    self.dataArray = [NSArray new];
    
    // Remove previous views
    [self removeSubViewsFromScrollView];
    
    if (response) {
        @try {
            [HKJsonUtils updatePropertiesWithData:response forObject:self.feedList];
            self.remainingArray = self.feedList.feedList;
            
            if (self.remainingArray.count > 4) {
                NSArray *itemsForView = [self.remainingArray subarrayWithRange: NSMakeRange(self.pageNo, ITEM_PER_VIEW)];
                self.dataArray = [NSMutableArray arrayWithArray:itemsForView];
            } else {
                self.dataArray = [NSMutableArray arrayWithArray:self.remainingArray];
            }
            
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
        }
    }
    
    if (self.feedList != nil && self.feedList.feedList.count > 0) {
        // Create chart URL strings
        [self createChartURLString:self.dataArray];
        [self.noRecordsView setHidden:YES];
    } else {
        [self.noRecordsView setHidden:NO];
    }
}

//---------------------------------------------------------------

- (void) createPullToRefresh {
    __weak typeof(self) wself = self;
    _refreshView = [LGRefreshView refreshViewWithScrollView:self.scrollView refreshHandler:^(LGRefreshView *refreshView) {
        if (wself) {
            __strong typeof(wself) self = wself;
            //TODO: Call API
            [self callWebServiceToGetFeedsList];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void) {
                [self.refreshView endRefreshing];
            });
        }
    }];
    _refreshView.tintColor = APP_COLOR;
}

//---------------------------------------------------------------

- (void) createChartURLString:(NSArray *)reportIds {
    
    // Clear previous records
    self.chartArray = nil;

    NSMutableArray *tempArray = [NSMutableArray new];
    NSString *baseUrlString = [NSString stringWithFormat:@"%@/feeds", FEEDS_CHART_URL];
    
    for (FeedModel *model in reportIds) {
        NSString *urlString = [NSString stringWithFormat:@"%@/%d", baseUrlString, model.feedId.intValue];
        NSString *chartUrlString = [ServiceUtil addHeaderParametersWithOutBaseURL:urlString withParams:nil];
        [tempArray addObject:chartUrlString];
    }
    
    self.chartArray = [NSArray arrayWithArray:tempArray];
    tempArray = nil;
    
    [self createChartControl];
}

//---------------------------------------------------------------

- (void) setTopNavBarTitle {
    CGFloat width = self.view.frame.size.width - 100;
    UIView *navBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-15, 44)];
    _navigationLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, width, 20)];
    self.navigationLabel.text = [App_Delegate.keyMapping getKeyValue:@"KEY_FEEDS"];
    self.navigationLabel.textAlignment = NSTextAlignmentCenter;
    self.navigationLabel.font = [UIFont fontWithName:AVENIR_FONT size:14.0f];
    self.navigationLabel.textColor = [UIColor whiteColor];
    [navBarView addSubview:self.navigationLabel];
    
    _topLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 25, width, 15)];
    self.topLabel.textAlignment = NSTextAlignmentCenter;
    self.topLabel.font = [UIFont fontWithName:AVENIR_FONT size:12.0f];
    self.topLabel.textColor = [UIColor whiteColor];
    [navBarView addSubview:self.topLabel];
    
    // Add button to open filter
    UIButton *openFilterButton = [UIButton buttonWithType:UIButtonTypeSystem];
    openFilterButton.frame = navBarView.frame;
    [openFilterButton addTarget:self action:@selector(openTenantTapped:) forControlEvents:UIControlEventTouchUpInside];
    [navBarView addSubview:openFilterButton];
    self.navigationItem.titleView = navBarView;
}

//---------------------------------------------------------------

- (void) updateTopLabelWidth {
    float width = self.topLabel.intrinsicContentSize.width;
    CGRect frame = self.topLabel.frame;
    frame.size.width = width;
    
    // Calculate center position
    CGFloat originX = self.navigationLabel.center.x - (width/2);
    frame.origin.x = originX;
    self.topLabel.frame = frame;
    [self.topLabel sizeToFit];
}

//---------------------------------------------------------------

- (void) createChartControl {

    NSInteger index = self.pageNo * ITEM_PER_VIEW;
    BOOL isFirstView = YES;

    if (index == 0) {
        self.lastView = nil;
    }
    
    for (;index < self.chartArray.count; index++) {
        FeedControl *chartControl = [[FeedControl alloc] init];
        chartControl.translatesAutoresizingMaskIntoConstraints = NO;
        [self.scrollView addSubview:chartControl];
//        [self.scrollView insertSubview:chartControl atIndex:0];
        chartControl.tag = index + CHART_TAG;
        
        chartControl.commentButton.button.tag = COMMENT_TAG + index;
        chartControl.likeButton.button.tag = LIKE_TAG + index;
        [chartControl.commentButton.button addTarget:self action:@selector(commentButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [chartControl.likeButton.button addTarget:self action:@selector(likeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];

        // Set url string to load later
        chartControl.chartUrlString = [self.chartArray objectAtIndex:index];
        
        // Set Feed details
        [chartControl setFeedDetails:[self.dataArray objectAtIndex:index]];
        
        NSDictionary *views = @{@"chartControl" : chartControl, @"loadMoreButton" : self.loadMoreButton};
        NSDictionary *metrics = @{@"width" : [NSNumber numberWithFloat:self.view.bounds.size.width]};
        
        [self.scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[chartControl(width)]|" options:0 metrics:metrics views:views]];
        
        //--> If view is first then pin the leading edge to main ScrollView otherwise to the last View.
        if (self.lastView == nil) {
            [self.scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[chartControl]" options:0 metrics:nil views:views]];
            isFirstView = NO;
        } else {
            [self.scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[lastView]-10-[chartControl]" options:0 metrics:nil views:@{@"chartControl" : chartControl, @"lastView": self.lastView}]];
        }
        
        //--> If View is last then pin the trailing edge to mainScrollView trailing edge.
        if (index == self.chartArray.count - 1) {
            
            // Show/hide load more button
            if (self.chartArray.count >= self.remainingArray.count) {
                [self.loadMoreButton removeFromSuperview];
                [self.scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[chartControl]-10-|" options:0 metrics:nil views:views]];
            } else {
                // add button at the end
                UIButton *button = [self.scrollView viewWithTag:LOAD_MORE_TAG];
                if (button) {
                    [button removeFromSuperview];
                }
                [self.scrollView addSubview:self.loadMoreButton];
                
                [self.scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[chartControl]-15-[loadMoreButton(40)]-15-|" options:0 metrics:nil views:views]];
                [self.scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[loadMoreButton(200)]" options:0 metrics:nil views:views]];
                
                [self.scrollView addConstraint:[NSLayoutConstraint constraintWithItem:self.loadMoreButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.scrollView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
            }
        }
        //--> Assign the current View as last view to keep the reference for next View.
        self.lastView = chartControl;
    }
    [self loadCharts];
//    [self performSelectorInBackground:@selector(loadCharts) withObject:self];
}

//---------------------------------------------------------------

- (void) loadCharts {
    for (UIView *subView in self.scrollView.subviews) {
        if ([subView isKindOfClass:[FeedControl class]]) {
            
            // Background task
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                FeedControl *chartControl = (FeedControl *)subView;
                NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:chartControl.chartUrlString]];
                NSURL *url = [NSURL URLWithString:chartControl.chartUrlString];
                NSData *data = [self sendSynchronousRequest:theRequest returningResponse:nil error:nil];
                // Main thread
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (data) {
                        [chartControl.chartControl loadData:data MIMEType:@"text/html" characterEncodingName:@"NSUTF8StringEncoding" baseURL:url];
                    } else {
                        [chartControl.activity stopAnimating];
                    }
                });
            });
        }
    }
//    [self.scrollView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        if ([obj isKindOfClass:[FeedControl class]]) {
//            FeedControl *chartControl = (FeedControl *)obj;
//            NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:chartControl.chartUrlString]];
//            NSURL *url = [NSURL URLWithString:chartControl.chartUrlString];
//            NSData *data = [self sendSynchronousRequest:theRequest returningResponse:nil error:nil];
//            if (data) {
//                [chartControl.chartControl loadData:data MIMEType:@"text/html" characterEncodingName:@"NSUTF8StringEncoding" baseURL:url];
//            } else {
//                [chartControl.activity stopAnimating];
//            }
//        }
//    }];
}

//---------------------------------------------------------------

- (NSData *)sendSynchronousRequest:(NSURLRequest *)request returningResponse:(__autoreleasing NSURLResponse **)responsePtr
                             error:(__autoreleasing NSError **)errorPtr {
    dispatch_semaphore_t    sem;
    __block NSData *        result;
    
    result = nil;
    
    sem = dispatch_semaphore_create(0);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    sessionConfig.timeoutIntervalForRequest = 10;
    sessionConfig.timeoutIntervalForResource = 60.0;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:nil];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (errorPtr != NULL) {
            *errorPtr = error;
        }
        if (responsePtr != NULL) {
            *responsePtr = response;
        }
        if (error == nil) {
            result = data;
        }
        dispatch_semaphore_signal(sem);
    }] resume];
    
    dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    
    return result;
}

//---------------------------------------------------------------

- (void) createInnerViews {
    
    // Load more button
    _loadMoreButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.loadMoreButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.loadMoreButton setTitle:@"More Feeds" forState:UIControlStateNormal];
    [self.loadMoreButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [self.view addSubview:self.loadMoreButton];
    self.loadMoreButton.backgroundColor = APP_COLOR;
    self.loadMoreButton.layer.cornerRadius = 10;
    self.loadMoreButton.tag = LOAD_MORE_TAG;
    [self.loadMoreButton addTarget:self action:@selector(loadMoreButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    // ScrollView
    _scrollView = [UIScrollView new];
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.scrollView];
    [self.scrollView showsVerticalScrollIndicator];
    self.scrollView.alwaysBounceVertical = YES;

//    NSDictionary *views = @{@"scrollView" : self.scrollView, @"loadMoreButton" : self.loadMoreButton};
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[scrollView]|" options:0 metrics:nil views:views]];
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[loadMoreButton(200)]-|" options:0 metrics:nil views:views]];
//
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[scrollView][loadMoreButton(40)]-5-|" options:0 metrics:nil views:views]];
    
    NSDictionary *views = @{@"scrollView" : self.scrollView};
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[scrollView]|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[scrollView]|" options:0 metrics:nil views:views]];
    [self.scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height)];
}

//---------------------------------------------------------------

- (void) removeSubViewsFromScrollView {
    // reset scrollView
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, 0);
    self.scrollView.contentOffset = CGPointMake(0, 0);
    
    // clear previous content
    for (UIView *view in self.scrollView.subviews) {
        // Remove only chart controls
        if ([view isKindOfClass:[FeedControl class]]) {
            [view removeFromSuperview];
        }
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark CommentDelegate methods

//---------------------------------------------------------------

- (void) updateCommentCount:(NSInteger)tag count:(NSInteger)count {
    @try {
        // Update one view only
        FeedControl *control = [self.scrollView viewWithTag:tag];
        if (control) {
            // Update button title
            control.commentButton.label.text = [NSString stringWithFormat:@"%d", (int)count];
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void) updateLikeCount:(NSInteger)tag count:(FeedModel *)model {
    @try {
        
        FeedControl *control = [self.scrollView viewWithTag:tag];
        
        if (control) {
            // Update button title
            
            UIButton *sender = control.likeButton.button;
            NSInteger likecount = model.noOfLikes.integerValue;
            if (sender.isSelected) {
                [sender setSelected:NO];
                control.likeButton.image.tintColor = SOCIAL_BUTTON_COLOR;
                control.likeButton.image.image = [UIImage imageNamed:LIKE_IMAGE];
            } else {
                [sender setSelected:YES];
                control.likeButton.image.tintColor = GREEN_COLOR;
                control.likeButton.image.image = [UIImage imageNamed:LIKED_IMAGE];
            }
            control.likeButton.label.text = [NSString stringWithFormat:@"%d",(int)likecount];
            
            // update data model
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.feedId.intValue = %d", model.feedId.intValue];
            NSArray *filters = [self.dataArray filteredArrayUsingPredicate:predicate];

            NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.dataArray];
            if (filters.count > 0) {
                FeedModel *feedData = filters[0];
                NSInteger index = [self.dataArray indexOfObject:feedData];
                [tempArray replaceObjectAtIndex:index withObject:model];
            }
            self.dataArray = [NSArray arrayWithArray:tempArray];
            tempArray = nil;
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];

    [self setTopNavBarTitle];
    
    // ScrollView setup
    [self createInnerViews];
    
    _noRecordsView = [Helper createNoAccessView:self.scrollView withMessage:[App_Delegate.keyMapping getKeyValue:@"KEY_NO_DATA_FEEDS"]];
    
    // Pull to refresh ----
    [self createPullToRefresh];
    
    // Get feeds
    // Check if already got the feeds
    UINavigationController *navigationController =  (UINavigationController *)self.parentViewController;
    MainDashboard *tabBar = (MainDashboard*)navigationController.parentViewController;
    if (tabBar.feeds) {
        [self updateUI:tabBar.feeds];
    } else {
        // Call service
        [self callWebServiceToGetFeedsList];
    }
}

//---------------------------------------------------------------

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.topLabel.text = [NSString stringWithFormat:@"%@", App_Delegate.appfilter.tenantName];
    [self updateTopLabelWidth];
}

//---------------------------------------------------------------

 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     if ([[segue identifier] isEqualToString:@"CommentViewController"]) {
         CommentViewController *comment = [segue destinationViewController];
         comment.delegate = self;
         comment.feedModel = self.feedModel;
         comment.selectedTag = self.selectedTag;
         comment.urlChartString = [self.chartArray objectAtIndex:self.selectedIndex.row];
     }
 }

//---------------------------------------------------------------

@end
