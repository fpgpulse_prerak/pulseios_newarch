//
//  FilterCell.h
//  IN-Gauge
//
//  Created by Mehul Patel on 05/10/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"

@interface FilterCell : UITableViewCell

@property (nonatomic, strong) UILabel               *itemLabel;
@property (nonatomic, strong) UIImageView           *arrowImage;
@property (nonatomic, strong) UIView                *cardView;

@end
