//
//  FilterCell.m
//  IN-Gauge
//
//  Created by Mehul Patel on 05/10/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "FilterCell.h"

@implementation FilterCell

//---------------------------------------------------------------

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor whiteColor];
    self.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tick"]];
    
    _cardView = [[UIView alloc] init];
    self.cardView.translatesAutoresizingMaskIntoConstraints = NO;
    self.cardView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.cardView];
    
    // Card view constraints
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cardView]|" options: 0 metrics:nil views:@{@"cardView" : self.cardView}]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cardView]|" options: 0 metrics:nil views:@{@"cardView" : self.cardView}]];
    
    // User label
    _itemLabel = [[UILabel alloc] init];
    self.itemLabel.font = [UIFont fontWithName:AVENIR_FONT size:14];
    self.itemLabel.numberOfLines = 0;
    self.itemLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.itemLabel.textAlignment = NSTextAlignmentLeft;
    [self.itemLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.cardView addSubview:self.itemLabel];
    
    // user image
    _arrowImage = [[UIImageView alloc] init];
    self.arrowImage.alpha = 0.8;
    self.arrowImage.translatesAutoresizingMaskIntoConstraints = NO;
    self.arrowImage.image = [UIImage imageNamed:RIGHT_ARROW];
    [self.cardView addSubview:self.arrowImage];
    
    // Constraints
    NSDictionary *views = @{@"itemLabel" : self.itemLabel, @"arrowImage" : self.arrowImage};
    
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[itemLabel][arrowImage(12)]-10-|" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-6-[itemLabel]-6-|" options: 0 metrics:nil views:views]];
    
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[arrowImage(12)]" options:0 metrics:nil views:views]];
    
    [self.cardView addConstraint:[NSLayoutConstraint constraintWithItem:self.arrowImage
                                                              attribute:NSLayoutAttributeCenterY
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.cardView
                                                              attribute:NSLayoutAttributeCenterY
                                                             multiplier:1
                                                               constant:0]];
    
    [self.cardView addConstraint:[NSLayoutConstraint constraintWithItem:self.itemLabel
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                             multiplier:1
                                                               constant:50]];
//    [self addDashedBottomBorder];
    return self;
}

//---------------------------------------------------------------

- (void) addDashedBottomBorder {
    
    UIColor *color = [UIColor lightGrayColor];
    
    CAShapeLayer *shapeLayer = [CAShapeLayer new];
    CGSize frameSize = self.frame.size;
    CGRect shapeRect = CGRectMake(0, 0, frameSize.width, 0);
    shapeLayer.bounds = shapeRect;
    shapeLayer.position = CGPointMake(frameSize.width/2, frameSize.height);
    shapeLayer.fillColor = [UIColor clearColor].CGColor;
    shapeLayer.strokeColor = color.CGColor;
    shapeLayer.lineWidth = 2.0;
    shapeLayer.lineJoin = kCALineJoinRound;
    shapeLayer.lineDashPattern = @[@9, @6];
    shapeLayer.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, shapeRect.size.height, shapeRect.size.width, 0) cornerRadius:0].CGPath;
    
    [self.layer addSublayer:shapeLayer];
}

//---------------------------------------------------------------

- (void) layoutSubviews {
    [super layoutSubviews];
}

//---------------------------------------------------------------

@end
