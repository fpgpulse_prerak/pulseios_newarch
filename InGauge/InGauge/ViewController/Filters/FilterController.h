//
//  FilterController.h
//  IN-Gauge
//
//  Created by Mehul Patel on 05/10/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

typedef enum : NSUInteger {
    OpenFromDashboard   = 1,
    OpenFromPreference  = 2,
    OpenFromGoal        = 3,
    OpenFromSurveyList  = 4
} OpenFrom;

@protocol FilterDelegate <NSObject>

@optional
- (void) reloadItemsBasedOnFilters:(Filters *)filter withFilterItems:(NSArray *)filterItems;
- (void) updateFilterArray:(NSArray *)updatedArray forFilterType:(FilterType)filterType;

@end

@interface FilterController : BaseViewController

@property (nonatomic, weak) id <FilterDelegate>     delegate;
@property (nonatomic, strong) Filters               *filter;
@property (nonatomic, strong) NSArray               *filterItems; //RefineItem
//@property (nonatomic, assign) NSArray               *dimensionArray;
@property (nonatomic, assign) OpenFrom              openFrom;
@property (nonatomic, assign) BOOL                  isShowGoalProducts;

@end
