//
//  FilterController.m
//  IN-Gauge
//
//  Created by Mehul Patel on 05/10/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "FilterController.h"
#import "FilterDetailView.h"

@interface FilterController () <UITableViewDataSource, UITableViewDelegate, FilterDetailDelegate>

@property (nonatomic, weak) IBOutlet UIView                 *topView;
@property (nonatomic, weak) IBOutlet UIView                 *bottomView;
@property (nonatomic, weak) IBOutlet UILabel                *titleLabel;
@property (nonatomic, weak) IBOutlet UIButton               *clearAllButton;
@property (nonatomic, weak) IBOutlet UIButton               *applyButton;
@property (nonatomic, weak) IBOutlet UITableView            *tableView;
@property (nonatomic, weak) IBOutlet FilterDetailView       *filterDetailView;

@property (nonatomic, strong) NSIndexPath                   *prevIndexPath;

@end

@implementation FilterController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
    self.prevIndexPath = nil;
    self.filterDetailView.delegate = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) closeButtonTapped:(id)sender {
    
    // close all operations before leaving filter control
    [self.filterDetailView closeAllOperations];
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

//---------------------------------------------------------------

- (IBAction) clearAllButtonTapped:(UIButton *)sender {
    [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];
    
    // Clear filters for Dashboard screen
    if (self.openFrom == OpenFromDashboard) {
        
        // Call service based on dynamic filters
        if (self.filter.regionId) {
            RefineItem *refineItem = [self.filterItems objectAtIndex:0];
            self.filter.geoItem = refineItem.subItems[0];
            
            // Clear selected array first!
            self.filter.selectedRegions = nil;
            [self geographyChange];
            [self selectFirstCell];
        } else {
            // Get locations based on tenant
            [self getTenantLocations:^(BOOL isFinish) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self selectFirstCell];
            }];
        }
    }
    
    // Clear filters for Goal screen
    if (self.openFrom == OpenFromGoal) {
        [self getTenantLocations:^(BOOL isFinish) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self selectFirstCell];
        }];
    }
    
    // Clear filters for Survey list
    if (self.openFrom == OpenFromSurveyList) {
        
        // Select first location
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.filterType = %d", LocationFilter];
        NSArray *items = [self.filterItems filteredArrayUsingPredicate:predicate];
        if (items.count > 0) {
            RefineItem *item = (RefineItem *)[items objectAtIndex:0];
            Location *location = [item.subItems objectAtIndex:0];
            self.filter.locationId = location.locationId;
            self.filter.locationName = location.name;
            [self updateFilterItem:self.filter.locationId displayName:self.filter.locationName filterType:LocationFilter];
        }
        
        // Select first observation category
        predicate = [NSPredicate predicateWithFormat:@"SELF.filterType = %d", ObservationCategory];
        items = [self.filterItems filteredArrayUsingPredicate:predicate];
        if (items.count > 0) {
            RefineItem *item = (RefineItem *)[items objectAtIndex:0];
            OBCategory *category = [item.subItems objectAtIndex:0];
            self.filter.categoryId = category.categoryId;
            self.filter.categoryName = category.name;
            [self updateFilterItem:self.filter.categoryId displayName:self.filter.categoryName filterType:ObservationCategory];
        }
        
        // Select first survey status
        predicate = [NSPredicate predicateWithFormat:@"SELF.filterType = %d", ObservationStatus];
        items = [self.filterItems filteredArrayUsingPredicate:predicate];
        if (items.count > 0) {
            RefineItem *item = (RefineItem *)[items objectAtIndex:0];
            self.filter.surveyStatus = [item.subItems objectAtIndex:0];
            [self updateFilterItem:nil displayName:self.filter.surveyStatus filterType:ObservationStatus];
        }
        [self.tableView reloadData];
        
        [self selectFirstCell];
    }
}

//---------------------------------------------------------------

- (IBAction) applyButtonTapped:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:^{}];

    [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];
    [self.delegate reloadItemsBasedOnFilters:self.filter withFilterItems:self.filterItems];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WEB SERVICE methods

//---------------------------------------------------------------

- (void) getTenantLocations:(void(^)(BOOL isFinish))block {
    // Get locations
    
    MBProgressHUD *hud = [Helper showLoading:self.view];
    NSDictionary *details = [self.filter serializeFiltersForLocationService];
    if (self.openFrom == OpenFromDashboard) {
        details = [self.filter serializeFiltersForLocationService];
    } else if (self.openFrom == OpenFromGoal) {
        details = [self.filter serializeFiltersGoalSettingLocationService];
    } else if (self.openFrom == OpenFromSurveyList) {
        details = [self.filter serializeFiltersSurveyLocationService];
    }
    
    [ServiceManager callWebServiceToGetLocations:details usingBlock:^(LocationList *locationList) {
        [Helper hideLoading:hud];
        [self.delegate updateFilterArray:locationList.locationList forFilterType:LocationFilter];
        
        // Remove filter
        self.filter.locationId = nil;
        self.filter.locationName = @"";

        // Get location group
        if (locationList.locationList.count > 0) {
            // First location
            Location *location = [locationList.locationList objectAtIndex:0];
            self.filter.locationId = location.locationId;
            self.filter.locationName = location.name;
            self.filter.metricTypeName = [Helper getMetricType:location.hotelMetricsDataType];
            
            // Update metric type name here...
             [self updateFilterItem:nil displayName:self.filter.metricTypeName filterType:MetricTypeFilter];
            
            // Check if location group filter is available
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.filterType = %d", LocationGroupFilter];
            NSArray *items = [self.filterItems filteredArrayUsingPredicate:predicate];
            
            if (items.count > 0) {
                // Call service to get location groups
                [self getLocationGroups:^(BOOL isFinish) {}];
            } else {
                self.filter.locationGroupId = nil;
                self.filter.locationGroupName = @"";
            }
        }
        
        // Update FilterItem array
        [self updateFilterItemsArray:locationList.locationList itemId:self.filter.locationId displayName:self.filter.locationName filterType:LocationFilter];
        block(YES);
    }];
}

//---------------------------------------------------------------

- (void) getLocationGroups:(void(^)(BOOL isFinish))block {
    MBProgressHUD *hud = [Helper showLoading:self.view];
    [ServiceManager callWebServiceToGetLocationGroup:self.filter.locationId usingBlock:^(LocationGroupList *locationGroupList) {
        [Helper hideLoading:hud];
        [self.delegate updateFilterArray:locationGroupList.locationGroupList forFilterType:LocationGroupFilter];
        
        // Remove filter
        self.filter.locationGroupId = nil;
        self.filter.locationGroupName = @"";

        // Get location group
        if (locationGroupList.locationGroupList.count > 0) {
            // First location group
            LocationGroup *locationGroup = [locationGroupList.locationGroupList objectAtIndex:0];
            self.filter.locationGroupId = locationGroup.locationGroupID;
            self.filter.locationGroupName = locationGroup.locationGroupName;
            
            
            // Check if product filter is available
            NSPredicate *predicate_prod = [NSPredicate predicateWithFormat:@"SELF.filterType = %d", ProductFilter];
            NSArray *filterProducts = [self.filterItems filteredArrayUsingPredicate:predicate_prod];
            if (filterProducts.count > 0) {
                // Call service to get location groups
                if (self.openFrom == OpenFromGoal) {
                    [self getProductsForGoalSettings:^(BOOL isFinish) {}];
                } else {
                    [self getProducts:^(BOOL isFinish) {}];
                }
            } else {
                self.filter.productId = nil;
                self.filter.productName = @"";
            }
            
            
            // Check if user filter is available
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.filterType = %d", UserFilter];
            NSArray *items = [self.filterItems filteredArrayUsingPredicate:predicate];
            
            if (items.count > 0) {
                [self getUsers:^(BOOL isFinish) {}];
            } else {
                self.filter.userId = nil;
                self.filter.userName = @"";
            }
        }
        
        // Update FilterItem array
        [self updateFilterItemsArray:locationGroupList.locationGroupList itemId:self.filter.locationGroupId displayName:self.filter.locationGroupName filterType:LocationGroupFilter];
        block(YES);
    }];
}

//---------------------------------------------------------------

- (void) getProducts:(void(^)(BOOL isFinish))block {
    MBProgressHUD *hud = [Helper showLoading:self.view];
    [ServiceManager callWebServiceToGetProducts:self.filter usingBlock:^(ProductList *productList) {
        [Helper hideLoading:hud];
        [self.delegate updateFilterArray:productList.productList forFilterType:ProductFilter];
        
        // If there is CustomGoalFilter than show "Incremental Revenue" and "Upsell products"
        if (self.isShowGoalProducts) {
            NSMutableArray *tempArray = [NSMutableArray arrayWithArray:productList.productList];
            
            //***NOTE: Add static product "Incremental Revenue" to get all details
            Product *incrementalRevenueProduct = [Product new];
            incrementalRevenueProduct.tenantProductName = [App_Delegate.keyMapping getKeyValue:@"KEY_TITLE_ARPD"];                    incrementalRevenueProduct.productId = [NSNumber numberWithInt:-1];
            [tempArray insertObject:incrementalRevenueProduct atIndex:0];
            
            // Only for Hospitality industry
            if (App_Delegate.appfilter.industryId.integerValue == 1) {
                //UPDATE:08 Sep 2017 - Add Second static product "Upsell Products" (https://fpgpulse.atlassian.net/browse/PA-126)
                Product *upsellProduct = [Product new];
                upsellProduct.tenantProductName = [App_Delegate.keyMapping getKeyValue:@"KEY_UPSELL_PRODUCTS"];
                upsellProduct.productId = [NSNumber numberWithInt:-2];
                [tempArray insertObject:upsellProduct atIndex:1];
            }
            
            productList.productList = [NSArray arrayWithArray:tempArray];
            tempArray = nil;
        }
        
        // Remove filter
        self.filter.productId = nil;
        self.filter.productName = @"";
        
        if (productList.productList.count > 0) {
            Product *product = [productList.productList objectAtIndex:0];
            self.filter.productId = product.productId;
            self.filter.productName = product.tenantProductName;
        }
        // Update FilterItem array
        [self updateFilterItemsArray:productList.productList itemId:self.filter.productId displayName:self.filter.productName filterType:ProductFilter];
        block(YES);
    }];
}

//---------------------------------------------------------------

- (void) getProductsForGoalSettings:(void(^)(BOOL isFinish))block {
    
    MBProgressHUD *hud = [Helper showLoading:self.view];
    [ServiceManager callWebServiceToGetProducts:self.filter usingBlock:^(ProductList *productList) {
        [Helper hideLoading:hud];
        
        NSMutableArray *tempArray = [NSMutableArray arrayWithArray:productList.productList];
        //***NOTE: Add static product "Incremental Revenue" to get all details
        Product *incrementalRevenueProduct = [Product new];
        incrementalRevenueProduct.tenantProductName = [App_Delegate.keyMapping getKeyValue:@"KEY_TITLE_ARPD"];                    incrementalRevenueProduct.productId = [NSNumber numberWithInt:-1];
        [tempArray insertObject:incrementalRevenueProduct atIndex:0];
        
        // Only for Hospitality industry
        if (App_Delegate.appfilter.industryId.integerValue == 1) {
            //UPDATE:08 Sep 2017 - Add Second static product "Upsell Products" (https://fpgpulse.atlassian.net/browse/PA-126)
            Product *upsellProduct = [Product new];
            upsellProduct.tenantProductName = [App_Delegate.keyMapping getKeyValue:@"KEY_UPSELL_PRODUCTS"];
            upsellProduct.productId = [NSNumber numberWithInt:-2];
            [tempArray insertObject:upsellProduct atIndex:1];
        }
        productList.productList = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
        
        [self.delegate updateFilterArray:productList.productList forFilterType:ProductFilter];
        
        // Remove filter
        self.filter.productId = nil;
        self.filter.productName = @"";
        
        if (productList.productList.count > 0) {
            Product *product = [productList.productList objectAtIndex:0];
            self.filter.productId = product.productId;
            self.filter.productName = product.tenantProductName;
        }
        // Update FilterItem array
        [self updateFilterItemsArray:productList.productList itemId:self.filter.productId displayName:self.filter.productName filterType:ProductFilter];
        block(YES);
    }];
}

//---------------------------------------------------------------

- (void) getUsers:(void(^)(BOOL isFinish))block {
    MBProgressHUD *hud = [Helper showLoading:self.view];
    // Get Users list
    
    NSDictionary *details = nil;
    if (self.openFrom == OpenFromDashboard) {

        [ServiceManager callWebServiceToGetAvailabelUsers:self.filter usingBlock:^(UserList *userList) {
            [Helper hideLoading:hud];
            [self.delegate updateFilterArray:userList.userList forFilterType:UserFilter];
            
            // Remove filter
            self.filter.userId = nil;
            self.filter.userName = @"";
            
            if (userList.userList.count > 0) {
                User *user = [userList.userList objectAtIndex:0];
                self.filter.userId = user.userID;
                self.filter.userName = user.name;
            }
            
            // Update FilterItem array
            [self updateFilterItemsArray:userList.userList itemId:self.filter.userId displayName:self.filter.userName filterType:UserFilter];
            block(YES);
        }];
    }
    
    else if (self.openFrom == OpenFromGoal) {
        details = [self.filter serializeFiltersGoalProgressUsersService];
        
        [ServiceManager callWebServiceToGetUsers:self.filter withDetails:details usingBlock:^(UserList *userList) {
            [Helper hideLoading:hud];
            [self.delegate updateFilterArray:userList.userList forFilterType:UserFilter];
            
            // Remove filter
            self.filter.userId = nil;
            self.filter.userName = @"";
            
            if (userList.userList.count > 0) {
                User *user = [userList.userList objectAtIndex:0];
                self.filter.userId = user.userID;
                self.filter.userName = user.name;
            }
            
            // Update FilterItem array
            [self updateFilterItemsArray:userList.userList itemId:self.filter.userId displayName:self.filter.userName filterType:UserFilter];
            block(YES);
        }];
    }
}

//---------------------------------------------------------------

- (void) getDashboardUsers {
    
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) getRegionTypeIdFromGeography:(GeographyModel *)model {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.title = %@", model.regionType];
    NSArray *items = [self.filterItems filteredArrayUsingPredicate:predicate];
    if (items.count > 0) {
        RefineItem *item = (RefineItem *)items[0];
        self.filter.regionTypeId = item.regionTypeId;
        self.filter.regionTypeName = item.title;
    }
}

//---------------------------------------------------------------

- (void) updateFilterItem:(NSNumber *)itemId displayName:(NSString *)displayName filterType:(FilterType)filterType {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.filterType = %d", filterType];
    NSArray *items = [self.filterItems filteredArrayUsingPredicate:predicate];
    if (items.count > 0) {
        NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.filterItems];
        RefineItem *item = (RefineItem *)[items objectAtIndex:0];
        item.itemId = itemId;
        item.displayName = displayName;
        
        NSInteger index = [self.filterItems indexOfObject:item];
        [tempArray replaceObjectAtIndex:index withObject:item];
        self.filterItems = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
    }
}

//---------------------------------------------------------------

- (void) updateFilterItemsArray:(NSArray *)subItems itemId:(NSNumber *)itemId displayName:(NSString *)displayName filterType:(FilterType)filterType {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.filterType = %d", filterType];
    NSArray *items = [self.filterItems filteredArrayUsingPredicate:predicate];
    if (items.count > 0) {
        NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.filterItems];
        RefineItem *item = (RefineItem *)[items objectAtIndex:0];
        item.itemId = itemId;
        item.displayName = displayName;
        item.subItems = subItems;
        
        NSInteger index = [self.filterItems indexOfObject:item];
        [tempArray replaceObjectAtIndex:index withObject:item];
        self.filterItems = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
        [self.tableView reloadData];
    }
}

//---------------------------------------------------------------

- (void) selectFirstCell {
    // Select first row
    @try {
        self.prevIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:self.prevIndexPath];
        cell.textLabel.textColor = MATERIAL_PINK_COLOR;
        cell.detailTextLabel.textColor = [UIColor blackColor];
        cell.backgroundColor = [UIColor whiteColor];
        
        RefineItem *item = [self.filterItems objectAtIndex:self.prevIndexPath.row];
        self.filterDetailView.filter = self.filter;
        [self.filterDetailView reloadFilterData:item];
        
        // For Geography types - load lower level based on top geography
        // If lower level is there
        if (item.filterType == GeographyType) {
            if (self.filterItems.count > 1) {
                
                for (int index = 0; index < self.filter.selectedRegions.count; index++) {
                    int nextIndex = index + 1;
                    GeographyModel *model = self.filter.selectedRegions[index];
                    RefineItem *nextItem = [self.filterItems objectAtIndex:nextIndex];
                    // break the loop!
                    if (nextItem.filterType != GeographyType) {
                        break;
                    }

                    // Get subItems
                    nextItem.subItems = [App_Delegate.appfilter getGeoSubItems:model.region.regionId tolevel:nextIndex];
                    // update
                    [self updateItem:nextItem];
                }
            }
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void) updateItem:(id)object {
    // Update
    NSInteger index = [self.filterItems indexOfObject:object];
    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.filterItems];
    [tempArray replaceObjectAtIndex:index withObject:object];
    self.filterItems = [NSArray arrayWithArray:tempArray];
    tempArray = nil;
}

//---------------------------------------------------------------

- (void) getSubRegionGeographyFilters {

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.filterType = %d", GeographyType];
    NSArray *items = [self.filterItems filteredArrayUsingPredicate:predicate];
    GeographyModel *model = self.filter.geoItem;
    
    int nextLevel = model.level.intValue + 1;
    for (int i = nextLevel; i < items.count; i++) {
        RefineItem *innerItem = (RefineItem *)[items objectAtIndex:i];
        innerItem.itemId = nil;
        innerItem.displayName = nil;
        innerItem.subItems = [App_Delegate.appfilter getGeoSubItems:model.region.regionId tolevel:i];
        // Enable next row selection
        if (i == 0) {
            innerItem.isEnable = YES;
        } else {
            innerItem.isEnable = NO;
        }
        
        // update
        [self updateItem:innerItem];
    }
}

//---------------------------------------------------------------

- (void) resetSubRegionGeographyFilters {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.filterType = %d", GeographyType];
    NSArray *items = [self.filterItems filteredArrayUsingPredicate:predicate];
    
    for (RefineItem *innerItem in items) {
        // Get an index
        NSInteger subItemIndex = [self.filterItems indexOfObject:innerItem];
        innerItem.subItems = [App_Delegate.appfilter.geoItems objectAtIndex:subItemIndex];
        [self updateItem:innerItem];
    }
}

//---------------------------------------------------------------

- (void) geographyChange {
    @try {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.filterType = %d", GeographyType];
        NSArray *items = [self.filterItems filteredArrayUsingPredicate:predicate];
        if (items.count > 0) {
            GeographyModel *model = self.filter.geoItem;
            
            //------- Steps:
            // 1. Reset other geo filters if level-0 filter is selected
            if (model.level.integerValue == 0) {
                [self resetSubRegionGeographyFilters];
            }
            
            // 2. Update current selected item
            RefineItem *currentItem = (RefineItem *)[items objectAtIndex:model.level.integerValue];
            currentItem.itemId = model.region.regionId;
            currentItem.displayName = model.region.name;
            [self updateItem:currentItem];
            
            // 3. Update higher level filters
            [self getSubRegionGeographyFilters];
            
            // 4. Enable next item
            NSInteger limit = model.level.integerValue + 1;
            if (limit < items.count) {
                RefineItem *nextItem = (RefineItem *)[items objectAtIndex:limit];
                nextItem.isEnable = YES;
                [self updateItem:nextItem];
            }

            //------- Steps to update filters
            // ~~~> 1. Update regionTypeId
            [self getRegionTypeIdFromGeography:model];

            // ~~~> 2. Update regionId
            self.filter.regionId = model.region.regionId;
            self.filter.regionName = model.region.name;
            
            // ~~~> 3. Update selected regions array
            NSMutableArray *tempArray = self.filter.selectedRegions ? [NSMutableArray arrayWithArray:self.filter.selectedRegions] : [NSMutableArray new];
            if (self.filter.selectedRegions.count <= model.level.intValue) {
                // Add new column
                [tempArray addObject:model];
            } else {
                // Replace existing column!
                [tempArray replaceObjectAtIndex:model.level.intValue withObject:model];
            }
            
            // Remove all sublevel if level = 0
            if (model.level.integerValue == 0) {
                self.filter.selectedRegions = [NSArray arrayWithObject:[tempArray objectAtIndex:0]];
            } else {
                self.filter.selectedRegions = [NSArray arrayWithArray:tempArray];
            }
            tempArray = nil;
            
            
            // ------ Call location service
            // Check if location filter is available
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.filterType = %d", LocationFilter];
            NSArray *items = [self.filterItems filteredArrayUsingPredicate:predicate];
            
            // Set default metric type - when locations are not available
//            self.filter.metricTypeName = [Helper getMetricType:@"Arrival"];
            
            if (items.count > 0) {
                // Call service to get locations
                [self getTenantLocations:^(BOOL isFinish) {}];
            } else {
                self.filter.locationId = nil;
                self.filter.locationName = @"";
                [self.tableView reloadData];
            }
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.filterItems.count;
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *formCellIdentifier = @"formCellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:formCellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:formCellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [UIFont fontWithName:AVENIR_FONT size:14.0];
    cell.detailTextLabel.font = [UIFont fontWithName:AVENIR_FONT size:12.0];
    
    //colors
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.detailTextLabel.textColor = [UIColor lightGrayColor];

    RefineItem *item = [self.filterItems objectAtIndex:indexPath.row];
    cell.textLabel.text = item.title;
    cell.detailTextLabel.text = item.displayName;
    
    // disable for geography
    if (item.filterType == GeographyType) {
        cell.textLabel.textColor = item.isEnable ? [UIColor blackColor] : [UIColor lightGrayColor];
        cell.userInteractionEnabled = item.isEnable;
    } else {
        cell.userInteractionEnabled = YES;
    }
    
    if (self.prevIndexPath == indexPath) {
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = MATERIAL_PINK_COLOR;
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    return cell;
}

//---------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Deselect previous row
    if (self.prevIndexPath) {
        [self tableView:self.tableView didDeselectRowAtIndexPath:self.prevIndexPath];
    }
    self.prevIndexPath = indexPath;
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.textColor = MATERIAL_PINK_COLOR;
    cell.detailTextLabel.textColor = [UIColor blackColor];
    cell.backgroundColor = [UIColor whiteColor];
    RefineItem *item = [self.filterItems objectAtIndex:indexPath.row];
    
    // Update details
    self.filterDetailView.filter = self.filter;
    [self.filterDetailView reloadFilterData:item];
}

//---------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    cell.backgroundColor = [UIColor clearColor];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark FilterDetailDelegate methods

//---------------------------------------------------------------

- (void) dismissController {
    [self dismissViewControllerAnimated:YES completion:^{        
    }];
}

//---------------------------------------------------------------

- (void) filterItemSelected:(Filters *)filter forFilterType:(FilterType)filterType {
    
    self.filter = filter;
    
    switch (filterType) {
        case GeographyType: {
            [self geographyChange];
            [self.tableView reloadData];
            break;
        }
        case LocationFilter: {
            // Update location name
            [self updateFilterItem:self.filter.locationId displayName:self.filter.locationName filterType:LocationFilter];
            
            // Update metric type name here...
            [self updateFilterItem:nil displayName:self.filter.metricTypeName filterType:MetricTypeFilter];

            // Check if location group filter is available
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.filterType = %d", LocationGroupFilter];
            NSArray *items = [self.filterItems filteredArrayUsingPredicate:predicate];
            
            if (items.count > 0) {
                // Not call other services if open from Survey
                if (self.openFrom != OpenFromSurveyList) {
                    [self getLocationGroups:^(BOOL isFinish) {}];
                }
            } else {
                self.filter.locationGroupId = nil;
                self.filter.locationGroupName = @"";
            }
            
            [self.tableView reloadData];
            break;
        }
        case LocationGroupFilter: {
            // Update location group name
            [self updateFilterItem:self.filter.locationGroupId displayName:self.filter.locationGroupName filterType:LocationGroupFilter];
            
            // Check if product filter is available
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.filterType = %d", ProductFilter];
            NSArray *items = [self.filterItems filteredArrayUsingPredicate:predicate];
            
            if (items.count > 0) {
                if (self.openFrom == OpenFromGoal) {
                    [self getProductsForGoalSettings:^(BOOL isFinish) {}];
                } else {
                    [self getProducts:^(BOOL isFinish) {}];
                }
            } else {
                self.filter.productId = nil;
                self.filter.productName = @"";
            }
            
            // Check if user filter is available
            NSPredicate *userPredicate = [NSPredicate predicateWithFormat:@"SELF.filterType = %d", UserFilter];
            NSArray *users = [self.filterItems filteredArrayUsingPredicate:userPredicate];
            if (users.count > 0) {
                [self getUsers:^(BOOL isFinish) {}];
            } else {
                self.filter.userId = nil;
                self.filter.userName = @"";
            }
            [self.tableView reloadData];

            break;
        }
        case ProductFilter: {
            // Update product name
            [self updateFilterItem:self.filter.productId displayName:self.filter.productName filterType:ProductFilter];
            [self.tableView reloadData];
            break;
        }
        case UserFilter: {
            // Update user name
            [self updateFilterItem:self.filter.userId displayName:self.filter.userName filterType:UserFilter];
            [self.tableView reloadData];
            break;
        }
            
        case MetricTypeFilter: {
            // Update metric type
            [self updateFilterItem:self.filter.userId displayName:self.filter.metricTypeName filterType:MetricTypeFilter];
            [self.tableView reloadData];
            break;
        }
            
        case ObservationCategory: {
            // Update metric type
            [self updateFilterItem:self.filter.categoryId displayName:self.filter.categoryName filterType:ObservationCategory];
            [self.tableView reloadData];
            break;
        }
            
        case ObservationStatus: {
            // Update metric type
            [self updateFilterItem:nil displayName:self.filter.surveyStatus filterType:ObservationStatus];
            [self.tableView reloadData];
            break;
        }
            
        default:
            break;
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    
    // Table settings
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    // Drop line for topView
//    [Helper addBottomLine:self.topView withColor:[UIColor grayColor]];
    
    // Top Shadow for bottomView
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.bottomView.bounds];
    self.bottomView.layer.masksToBounds = NO;
    self.bottomView.layer.shadowColor = [UIColor grayColor].CGColor;
    self.bottomView.layer.shadowOffset = CGSizeMake(0.0f, -0.1f);
    self.bottomView.layer.shadowOpacity = 0.5f;
    self.bottomView.layer.shadowPath = shadowPath.CGPath;
    
    // Left side border
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 0.5;
    border.borderColor = [UIColor lightGrayColor].CGColor;
    border.frame = CGRectMake(0, 0, borderWidth, self.filterDetailView.frame.size.height);
    border.borderWidth = borderWidth;
    [self.filterDetailView.layer addSublayer:border];
    self.filterDetailView.layer.masksToBounds = YES;
    self.filterDetailView.delegate = self;
    
    self.clearAllButton.enabled = YES;
    // Disable clear all for Inustry/Tenant change
    if (self.openFrom == OpenFromPreference) {
        self.clearAllButton.enabled = NO;
        [self.clearAllButton setTitleColor:TABLE_SEPERATOR_COLOR forState:UIControlStateNormal];
    }
}

//---------------------------------------------------------------

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.titleLabel.text = @"FILTER BY";
    if (self.openFrom == OpenFromPreference) {
        self.titleLabel.text = @"CHANGE INDUSTRY & TENANT";
    }    
    [self selectFirstCell];
}

//---------------------------------------------------------------

@end
