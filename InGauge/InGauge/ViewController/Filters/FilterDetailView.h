//
//  FilterDetailView.h
//  IN-Gauge
//
//  Created by Mehul Patel on 05/10/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "ServiceManager.h"

@protocol FilterDetailDelegate <NSObject>

@optional
- (void) dismissController;
- (void) filterItemSelected:(Filters *)filter forFilterType:(FilterType)filterType;

@end

@interface FilterDetailView : UIView

@property (nonatomic, weak) id <FilterDetailDelegate>       delegate;
@property (nonatomic, strong) Filters                       *filter;

- (void) reloadFilterData:(RefineItem *)item;
- (void) closeAllOperations;

@end
