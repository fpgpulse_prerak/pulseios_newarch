//
//  FilterDetailView.m
//  IN-Gauge
//
//  Created by Mehul Patel on 05/10/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "FilterDetailView.h"
#import "FilterCell.h"

#define kHeaderHeight 50

@interface FilterDetailView() <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

@property (nonatomic, weak) IBOutlet UITableView        *tableView;
@property (nonatomic, weak) IBOutlet UISearchBar        *searchBar;

@property (nonatomic, strong) UIView                    *noRecordsView;
@property (nonatomic, strong) RefineItem                *filterItem;
@property (nonatomic, strong) NSArray                   *allItems;
@property (nonatomic, strong) NSArray                   *dataArray;
@property (nonatomic, strong) NSArray                   *searchArray;
@property (nonatomic, strong) NSArray                   *selectionArray;
@property (nonatomic, strong) NSIndexPath               *prevIndexPath;

@end

@implementation FilterDetailView

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void)dealloc {
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
    self.searchBar.delegate = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) setupViews {
    // Table settings
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 50;
    
    // Search Bar UI
    self.searchBar.delegate = self;
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setFont:[UIFont fontWithName:AVENIR_FONT size:12]];
    [self.searchBar setReturnKeyType:UIReturnKeyDone];
//    [self.searchBar setKeyboardAppearance:UIKeyboardAppearanceDark];
    
    _noRecordsView = [Helper createNoAccessView:self withMessage:[App_Delegate.keyMapping getKeyValue:@"KEY_NO_DATA"]];
}

//---------------------------------------------------------------

- (NSPredicate *) getPredicateFormat {
    NSPredicate *predicate = nil;
    NSString *name = self.filterItem.displayName;
    // Early exit if no name exist!
    if (!name) {
        return nil;
    }
        
    switch (self.filterItem.filterType) {
        case IndustryFilter: {
            predicate = [NSPredicate predicateWithFormat:@"SELF.industryDescription = %@", name];
            break;
        }
            
        case TenantFilter: {
            predicate = [NSPredicate predicateWithFormat:@"SELF.tenantName = %@", name];
            break;
        }
            
        case GeographyType: {
            predicate = [NSPredicate predicateWithFormat:@"SELF.region.name = %@", name];
            break;
        }
            
        case RegionTypeFilter: {
            predicate = [NSPredicate predicateWithFormat:@"SELF.name = %@", name];
            break;
        }
        case RegionFilter: {
            predicate = [NSPredicate predicateWithFormat:@"SELF.completeName = %@", name];
            break;
        }
        case LocationFilter: {
            predicate = [NSPredicate predicateWithFormat:@"SELF.name = %@", name];
            break;
        }
        case LocationGroupFilter: {
            predicate = [NSPredicate predicateWithFormat:@"SELF.locationGroupName = %@", name];
            break;
        }
        case ProductFilter: {
            predicate = [NSPredicate predicateWithFormat:@"SELF.tenantProductName = %@", name];
            break;
        }
        case UserFilter: {
            predicate = [NSPredicate predicateWithFormat:@"SELF.name = %@", name];
            break;
        }
        case MetricTypeFilter: {
            predicate = [NSPredicate predicateWithFormat:@"SELF = %@", name];
            break;
        }
            // Observation
        case PerformanceLeaderFilter: {
            predicate = [NSPredicate predicateWithFormat:@"SELF.name = %@", name];
            break;
        }
        case AgentFilter: {
            predicate = [NSPredicate predicateWithFormat:@"SELF.name = %@", name];
            break;
        }
        case ObservationTypeFilter: {
//            ObservationType *obType = (ObservationType *)object;
//            NSString *sureveyCategoryName = obType.surveyCategory.name;
//            sureveyCategoryName = sureveyCategoryName.length > 0 ? [NSString stringWithFormat:@"(%@)", sureveyCategoryName] : @"";
//            NSString *observationType = @"";
//            if (obType.name.length > 0) {
//                observationType = [NSString stringWithFormat:@"%@ %@", obType.name, sureveyCategoryName];
//            } else {
//                observationType = [NSString stringWithFormat:@"%@", sureveyCategoryName];
//            }
//            filterName = observationType;
            predicate = nil;
            break;
        }
        case ObservationCategory: {
            predicate = [NSPredicate predicateWithFormat:@"SELF.name = %@", name];
            break;
        }
        case ObservationStatus: {
            predicate = [NSPredicate predicateWithFormat:@"SELF = %@", name];
            break;
        }
        default: {
            predicate = [NSPredicate predicateWithFormat:@"SELF = %@", name];
            break;
        }
    }
    return predicate;
}

//---------------------------------------------------------------

- (void) reloadFilterData:(RefineItem *)item {
    self.selectionArray = nil;
    // Update inner controls
//    self.prevIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    self.searchBar.text = @"";
    [self.searchBar resignFirstResponder];
    
    self.filterItem = item;
    self.dataArray = item.subItems;
    self.allItems = self.dataArray;
    
    // Check that selected item is there or not?
    // Show selected item at top of list
    NSPredicate *predicate = [self getPredicateFormat];
    if (predicate) {
        NSArray *filterArray = [self.dataArray filteredArrayUsingPredicate:predicate];
        
        // No selection if there is only one item!
        if (filterArray.count == self.dataArray.count) {
            // there is no need of selection array
        } else {
            if (filterArray.count) {
                // Add selected item
                self.selectionArray = @[filterArray[0]];
                
                // Remove selected object from Main list
                NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.dataArray];
                [tempArray removeObject:filterArray[0]];
                self.dataArray = [NSArray arrayWithArray:tempArray];
                tempArray = nil;
            }
        }
    }
    
    [self.tableView reloadData];
    
    if (self.dataArray.count > 0) {
        // Scroll table to first position
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView scrollToRowAtIndexPath:indexPath
                              atScrollPosition:UITableViewScrollPositionTop
                                      animated:YES];
        [self.noRecordsView setHidden:YES];
        [self.tableView setHidden:NO];
    } else {
        [self.noRecordsView setHidden:NO];
        [self.tableView setHidden:YES];
    }
}

//---------------------------------------------------------------

- (NSDictionary *) isSelected:(id)object {
    NSString *filterName = @"";
    switch (self.filterItem.filterType) {
        case IndustryFilter: {
            Industry *type = (Industry *)object;
            filterName = type.industryDescription;
            break;
        }
            
        case TenantFilter: {
            TenantRoleModel *type = (TenantRoleModel *)object;
            filterName = type.tenantName;
            break;
        }
            
        case GeographyType: {
            GeographyModel *type = (GeographyModel *)object;
            filterName = type.region.name;
            break;
        }
            
        case RegionTypeFilter: {
            RegionType *type = (RegionType *)object;
            filterName = type.name;
            break;
        }
        case RegionFilter: {
            Region *region = (Region *)object;
            filterName = region.completeName;
            break;
        }
        case LocationFilter: {
            Location *location = (Location *)object;
            filterName = location.name;
            break;
        }
        case LocationGroupFilter: {
            LocationGroup *group = (LocationGroup *)object;
            filterName = group.locationGroupName;
            break;
        }
        case ProductFilter: {
            Product *product = (Product *)object;
            filterName = product.tenantProductName;
            break;
        }
        case UserFilter: {
            User *user = (User *)object;
            filterName = user.name;
            break;
        }
        case MetricTypeFilter: {
            filterName = (NSString *)object;
            break;
        }
            // Observation
        case PerformanceLeaderFilter: {
            User *user = (User *)object;
            filterName = user.name;
            break;
        }
        case AgentFilter: {
            User *user = (User *)object;
            filterName = user.name;
            break;
        }
        case ObservationTypeFilter: {
            ObservationType *obType = (ObservationType *)object;
            NSString *sureveyCategoryName = obType.surveyCategory.name;
            sureveyCategoryName = sureveyCategoryName.length > 0 ? [NSString stringWithFormat:@"(%@)", sureveyCategoryName] : @"";
            NSString *observationType = @"";
            if (obType.name.length > 0) {
                observationType = [NSString stringWithFormat:@"%@ %@", obType.name, sureveyCategoryName];
            } else {
                observationType = [NSString stringWithFormat:@"%@", sureveyCategoryName];
            }
            filterName = observationType;
            break;
        }
        case ObservationCategory: {
            OBCategory *category = (OBCategory *)object;
            filterName = category.name;
            break;
        }
        case ObservationStatus: {
            filterName = (NSString *)object;
            break;
        }
        default: {
            filterName = (NSString *)object;
            break;
        }
    }

    return @{@"name" : [Helper removeWhiteSpaceAndNewLineCharacter:filterName], @"isSelected" : [NSNumber numberWithBool:[filterName isEqualToString:self.filterItem.displayName]]};
}

//---------------------------------------------------------------

- (void) closeAllOperations {
    self.searchBar.text = @"";
    [self.searchBar resignFirstResponder];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

//---------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return (self.selectionArray.count > 0) ? kHeaderHeight : 0;
}

//---------------------------------------------------------------

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Previous code
    NSString *searchText = [Helper removeWhiteSpaceAndNewLineCharacter:self.searchBar.text];
    return (searchText.length > 0) ? self.searchArray.count : self.dataArray.count;

//    switch (section) {
//        case 0: { return self.selectionArray.count; break;}
//        case 1: {
//            NSString *searchText = [Helper removeWhiteSpaceAndNewLineCharacter:self.searchBar.text];
//            return (searchText.length > 0) ? self.searchArray.count : self.dataArray.count;
//            break;
//        }
//        default: return 0; break;
//    }
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"FilterCell%d%d", (int)indexPath.section, (int)indexPath.row];
    FilterCell *cell = (FilterCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[FilterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSArray *dataSource = dataSource = self.dataArray;
    NSString *searchText = [Helper removeWhiteSpaceAndNewLineCharacter:self.searchBar.text];
    if (searchText.length > 0) {
        dataSource = self.searchArray;
    }
    
    NSDictionary *details = [self isSelected:[dataSource objectAtIndex:indexPath.row]];
    cell.itemLabel.text = [[details allKeys] containsObject:@"name"] ? [details objectForKey:@"name"] : @"";
    cell.arrowImage.hidden = YES;
    
    // Show selected object
    NSNumber *isSelected = [details objectForKey:@"isSelected"];
    if (isSelected.boolValue) {
        cell.accessoryView.hidden = NO;
        self.prevIndexPath = indexPath;
    } else {
        cell.accessoryView.hidden = YES;
    }
    return cell;
}

//---------------------------------------------------------------

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Deselect previous row
    if (self.prevIndexPath) {
        [self tableView:self.tableView didDeselectRowAtIndexPath:self.prevIndexPath];
    }
    self.prevIndexPath = indexPath;
    FilterCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryView.hidden = NO;
    
    NSArray *dataSource = self.dataArray;
    NSString *searchText = [Helper removeWhiteSpaceAndNewLineCharacter:self.searchBar.text];
    if (searchText.length > 0) {
        dataSource = self.searchArray;
    }
    
    // Selected row can not be select again
    if (dataSource.count == 0) {
        return;
    }
    
    // for industry and tenant change use different approach
    if (self.filterItem.filterType == IndustryFilter) {

        App_Delegate.globalHud = [Helper showLoading:App_Delegate.window];
        App_Delegate.globalHud.label.text = @"Loading...";

        [self.delegate dismissController];
        
        Industry *industryModel = [dataSource objectAtIndex:indexPath.row];
        [App_Delegate industryChanged:industryModel];
        return;
    }
    
    if (self.filterItem.filterType == TenantFilter) {
        App_Delegate.globalHud = [Helper showLoading:App_Delegate.window];
        App_Delegate.globalHud.label.text = @"Loading...";
        
        [self.delegate dismissController];
        TenantRoleModel *tenantModel = [dataSource objectAtIndex:indexPath.row];
        [App_Delegate tenantChanged:tenantModel];
        return;
    }
    
    switch (self.filterItem.filterType) {
            
        case GeographyType: {
            GeographyModel *type = [dataSource objectAtIndex:indexPath.row];
            self.filter.geoItem = type;
            break;
        }
            
        case RegionFilter: {
            Region *region = [dataSource objectAtIndex:indexPath.row];
            self.filter.regionName = region.completeName;
            self.filter.regionId = region.regionId;
            break;
        }
        case LocationFilter: {
            Location *location = [dataSource objectAtIndex:indexPath.row];
            self.filter.locationName = location.name;
            self.filter.locationId = location.locationId;
            self.filter.metricTypeName = [Helper getMetricType:location.hotelMetricsDataType];
            break;
        }
        case LocationGroupFilter: {
            LocationGroup *group = [dataSource objectAtIndex:indexPath.row];
            self.filter.locationGroupName = group.locationGroupName;
            self.filter.locationGroupId = group.locationGroupID;
            break;
        }
        case ProductFilter: {
            Product *product = [dataSource objectAtIndex:indexPath.row];
            self.filter.productName = product.tenantProductName;
            self.filter.productId = product.productId;
            break;
        }
        case UserFilter: {
            User *user = [dataSource objectAtIndex:indexPath.row];
            self.filter.userName = user.name;
            self.filter.userId = user.userID;
            break;
        }
        case MetricTypeFilter: {
            NSString *metricType = [dataSource objectAtIndex:indexPath.row];
            self.filter.metricTypeName = metricType;
            break;
        }
            
            // Observation
        case PerformanceLeaderFilter: {
            User *user = [dataSource objectAtIndex:indexPath.row];
            self.filter.performanceLeaderId = user.userID;
            break;
        }
        case AgentFilter: {
            User *user = [dataSource objectAtIndex:indexPath.row];
            self.filter.agentId = user.userID;
            break;
        }
        case ObservationTypeFilter: {
            ObservationType *obType = [dataSource objectAtIndex:indexPath.row];
            self.filter.obTypeId = obType.observationTypeId;
            break;
        }
        case ObservationCategory: {
            OBCategory *category = [dataSource objectAtIndex:indexPath.row];
            self.filter.categoryName = category.name;
            self.filter.categoryId = category.categoryId;
            break;
        }
        case ObservationStatus: {
            NSString *status = [dataSource objectAtIndex:indexPath.row];
            self.filter.surveyStatus = status;
            break;
        }
        default: {
            break;
        }
    }
    
    NSMutableArray *searchTempArray = [NSMutableArray new];
    NSMutableArray *dataTempArray = [NSMutableArray new];
    
    id currentObject = [dataSource objectAtIndex:indexPath.row];
    self.selectionArray = @[currentObject];
    
    for (id object in self.allItems) {
        
        if (searchText) {
            if ([self.searchArray containsObject:object]) {
                [searchTempArray addObject:object];
            }
        }
        
        if ([self.dataArray containsObject:object]) {
            [dataTempArray addObject:object];
        } else {
            [dataTempArray addObject:object];
        }
    }
    // remove current object from arrays
    [searchTempArray removeObject:currentObject];
    [dataTempArray removeObject:currentObject];
    
    self.searchArray = [NSArray arrayWithArray:searchTempArray];
    self.dataArray = [NSArray arrayWithArray:dataTempArray];
    searchTempArray = nil;
    dataTempArray = nil;
    
    NSRange range = NSMakeRange(0, 1);
    NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
    [self.tableView reloadSections:section withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView reloadData];
    [self.delegate filterItemSelected:self.filter forFilterType:self.filterItem.filterType];
}

//---------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    FilterCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryView.hidden = YES;
}

//---------------------------------------------------------------

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kHeaderHeight)];
    headerView.backgroundColor = [UIColor whiteColor];
    headerView.tag = section;

    // Drop shadow
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:headerView.bounds];
    headerView.layer.masksToBounds = NO;
    headerView.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    headerView.layer.shadowOffset = CGSizeMake(0.0f, 0.1f);
    headerView.layer.shadowOpacity = 0.5f;
    headerView.layer.shadowPath = shadowPath.CGPath;


    UIView *cardView = [[UIView alloc] init];
    cardView.translatesAutoresizingMaskIntoConstraints = NO;
    cardView.backgroundColor = [UIColor clearColor];
    [headerView addSubview:cardView];
    
    // Card view constraints
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cardView]|" options: 0 metrics:nil views:@{@"cardView" : cardView}]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cardView]-1-|" options: 0 metrics:nil views:@{@"cardView" : cardView}]];
    
    // User label
    UILabel *itemLabel = [[UILabel alloc] init];
    itemLabel.font = [UIFont fontWithName:AVENIR_FONT size:14];
    itemLabel.numberOfLines = 0;
    itemLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    itemLabel.textAlignment = NSTextAlignmentLeft;
    [itemLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [cardView addSubview:itemLabel];
    
    // User image
    UIImageView *arrowImage = [[UIImageView alloc] init];
    arrowImage.translatesAutoresizingMaskIntoConstraints = NO;
    arrowImage.image = [UIImage imageNamed:@"tick"];
    [cardView addSubview:arrowImage];
    
    NSDictionary *details = [self isSelected:[self.selectionArray objectAtIndex:section]];
    itemLabel.text = [[details allKeys] containsObject:@"name"] ? [details objectForKey:@"name"] : @"";
    arrowImage.hidden = NO;

    // Constraints
    NSDictionary *views = @{@"itemLabel" : itemLabel, @"arrowImage" : arrowImage};
    
    [cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[itemLabel][arrowImage(12)]-10-|" options: 0 metrics:nil views:views]];
    [cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[itemLabel]|" options: 0 metrics:nil views:views]];
    [cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[arrowImage(12)]" options:0 metrics:nil views:views]];
    
    [cardView addConstraint:[NSLayoutConstraint constraintWithItem:arrowImage
                                                              attribute:NSLayoutAttributeCenterY
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:cardView
                                                              attribute:NSLayoutAttributeCenterY
                                                             multiplier:1
                                                          constant:0]];
    return headerView;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UISearch delegate methods

//---------------------------------------------------------------

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    return YES;
}

//---------------------------------------------------------------

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return YES;
}

//---------------------------------------------------------------

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    @try {
        if (isEmptyString(searchText)) {
            self.searchArray = self.dataArray;
        } else {
            searchText = [[searchText trim] lowercaseString];
            NSMutableArray *tempArray = [NSMutableArray array];
            for (id option in self.dataArray) {
                
                NSString *optionText = @"";
                switch (self.filterItem.filterType) {
                    case IndustryFilter: {
                        Industry *type = (Industry *)option;
                        optionText = type.industryDescription;
                        break;
                    }
                    case TenantFilter: {
                        TenantRoleModel *type = (TenantRoleModel *)option;
                        optionText = type.tenantName;
                        break;
                    }
                    case RegionTypeFilter: {
                        RegionType *type = (RegionType *)option;
                        optionText = type.name;
                        break;
                    }
                    case RegionFilter: {
                        Region *region = (Region *)option;
                        optionText = region.completeName;
                        break;
                    }
                    case LocationFilter: {
                        Location *location = (Location *)option;
                        optionText = location.name;
                        break;
                    }
                    case LocationGroupFilter: {
                        LocationGroup *group = (LocationGroup *)option;
                        optionText = group.locationGroupName;
                        break;
                    }
                    case ProductFilter: {
                        Product *product = (Product *)option;
                        optionText = product.tenantProductName;
                        break;
                    }
                    case UserFilter: {
                        User *user = (User *)option;
                        optionText = user.name;
                        break;
                    }
                    case MetricTypeFilter: {
                        optionText = option;
                        break;
                    }
                        // Observation
                    case PerformanceLeaderFilter: {
                        User *user = (User *)option;
                        optionText = user.name;
                        break;
                    }
                    case AgentFilter: {
                        User *user = (User *)option;
                        optionText = user.name;
                        break;
                    }
                    case ObservationTypeFilter: {
                        ObservationType *obType = (ObservationType *)option;
                        optionText = obType.name;
                        break;
                    }
                        
                    case ObservationCategory: {
                        OBCategory *category = (OBCategory *)option;
                        optionText = category.name;
                        break;
                    }
                    case ObservationStatus: {
                        optionText = option;
                        break;
                    }
                        
                    case GeographyType: {
                        GeographyModel *model = (GeographyModel *)option;
                        optionText = model.region.name;
                        break;
                    }
                
                    default: {
                        optionText = option;
                        break;
                    }
                }
                                
                if ([[optionText lowercaseString] myContainsString:searchText]) {
                    [tempArray addObject:option];
                }
            }
            self.searchArray = [NSArray arrayWithArray:tempArray];
            tempArray = nil;
        }
        [self.tableView reloadData];
    }
    @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

//---------------------------------------------------------------

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    return YES;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Init methods

//---------------------------------------------------------------

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setupViews];
    }
    return self;
}

//---------------------------------------------------------------

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupViews];
    }
    return self;
}

//---------------------------------------------------------------

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViews];
    }
    return self;
}

//---------------------------------------------------------------

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupViews];
}

//---------------------------------------------------------------

@end
