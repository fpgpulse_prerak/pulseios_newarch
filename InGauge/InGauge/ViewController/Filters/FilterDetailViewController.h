//
//  FilterDetailViewController.h
//  InGauge
//
//  Created by Mehul Patel on 25/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ServiceManager.h"

@protocol FilterDetailDelegate <NSObject>

- (void) reloadFilters:(FilterType)filterType withFilter:(Filters *)filter;

@end

@interface FilterDetailViewController : BaseViewController

@property (nonatomic, weak) id <FilterDetailDelegate>   delegate;

@property (nonatomic, strong) RegionTypeList            *regionTypeList;
@property (nonatomic, strong) RegionList                *regionList;
@property (nonatomic, strong) LocationList              *locationList;
@property (nonatomic, strong) LocationGroupList         *locationGroupList;
@property (nonatomic, strong) ProductList               *productList;
@property (nonatomic, strong) UserList                  *userList;
@property (nonatomic, strong) NSArray                   *metricArray;

@property (nonatomic, strong) UserList                  *performanceLeaderList;
@property (nonatomic, strong) UserList                  *agentList;
@property (nonatomic, strong) OBTypeList                *obTypeList;
@property (nonatomic, strong) OBCategoryList            *categoryList;
@property (nonatomic, strong) NSArray                   *statusArray;

@property (nonatomic, assign) FilterType                itemId;
@property (nonatomic, strong) Filters                   *filter;

@end
