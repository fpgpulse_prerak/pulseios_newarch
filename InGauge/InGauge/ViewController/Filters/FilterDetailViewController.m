//
//  FilterDetailViewController.m
//  InGauge
//
//  Created by Mehul Patel on 25/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "FilterDetailViewController.h"
#import "FilterItemCell.h"

#define kHeaderHeight 50

@interface FilterDetailViewController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>
;
@property (nonatomic, weak) IBOutlet UITableView        *tableView;
@property (nonatomic, weak) IBOutlet UISearchBar        *searchBar;

@property (nonatomic, strong) UIView                    *noRecordsView;
@property (nonatomic, strong) NSArray                   *dataArray;
@property (nonatomic, strong) NSArray                   *searchArray;
@property (nonatomic, strong) NSArray                   *selectionArray;

@end

@implementation FilterDetailViewController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------


//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (NSDictionary *) isSelected:(id)object {
    BOOL isSelected = NO;
    NSString *filterName = @"";
    switch (self.itemId) {
        case RegionTypeFilter: {
            RegionType *type = object;
            filterName = [Helper removeWhiteSpaceAndNewLineCharacter:type.name];
            isSelected = (type.regionTypeId.intValue == self.filter.regionTypeId.intValue) ? YES : NO;
            break;
        }
        case RegionFilter: {
            Region *region = object;
            filterName = [Helper removeWhiteSpaceAndNewLineCharacter:region.completeName];
            isSelected = (region.regionId.intValue == self.filter.regionId.intValue) ? YES : NO;
            break;
        }
        case LocationFilter: {
            Location *location = object;
            filterName = [Helper removeWhiteSpaceAndNewLineCharacter:location.name];
            isSelected = (location.locationId.intValue == self.filter.locationId.intValue) ? YES : NO;
            break;
        }
        case LocationGroupFilter: {
            LocationGroup *group = object;
            filterName = [Helper removeWhiteSpaceAndNewLineCharacter:group.locationGroupName];
            isSelected = (group.locationGroupID.intValue == self.filter.locationGroupId.intValue) ? YES : NO;
            break;
        }
        case ProductFilter: {
            Product *product = object;
            filterName = [Helper removeWhiteSpaceAndNewLineCharacter:product.tenantProductName];
            isSelected = (product.productId.intValue == self.filter.productId.intValue) ? YES : NO;
            break;
        }
        case UserFilter: {
            User *user = object;
            filterName = [Helper removeWhiteSpaceAndNewLineCharacter:user.name];
            isSelected = (user.userID.intValue == self.filter.userId.intValue) ? YES : NO;
            break;
        }
        case MetricTypeFilter: {
            NSString *metricType = object;
            filterName = [Helper removeWhiteSpaceAndNewLineCharacter:metricType];
            isSelected = [metricType isEqualToString:self.filter.metricTypeName] ? YES : NO;
            break;
        }
            // Observation
        case PerformanceLeaderFilter: {
            User *user = object;
            filterName = [Helper removeWhiteSpaceAndNewLineCharacter:user.name];
            isSelected = (user.userID.intValue == self.filter.performanceLeaderId.intValue) ? YES : NO;
            break;
        }
        case AgentFilter: {
            User *user = object;
            filterName = [Helper removeWhiteSpaceAndNewLineCharacter:user.name];
            isSelected = (user.userID.intValue == self.filter.agentId.intValue) ? YES : NO;
            break;
        }
        case ObservationTypeFilter: {
            ObservationType *obType = object;
            NSString *sureveyCategoryName = [Helper removeWhiteSpaceAndNewLineCharacter:obType.surveyCategory.name];
            sureveyCategoryName = sureveyCategoryName.length > 0 ? [NSString stringWithFormat:@"(%@)", sureveyCategoryName] : @"";
            NSString *observationType = @"";
            if (obType.name.length > 0) {
                observationType = [NSString stringWithFormat:@"%@ %@", obType.name, sureveyCategoryName];
            } else {
                observationType = [NSString stringWithFormat:@"%@", sureveyCategoryName];
            }
            filterName = [Helper removeWhiteSpaceAndNewLineCharacter:observationType];
            isSelected = (obType.observationTypeId.intValue == self.filter.obTypeId.intValue) ? YES : NO;
            break;
        }
        case ObservationCategory: {
            OBCategory *category = object;
            filterName = [Helper removeWhiteSpaceAndNewLineCharacter:category.name];
            isSelected = (category.categoryId.intValue == self.filter.categoryId.intValue) ? YES : NO;
            break;
        }
        case ObservationStatus: {
            NSString *status = object;
            filterName = [Helper removeWhiteSpaceAndNewLineCharacter:status];
            isSelected = [status isEqualToString:self.filter.surveyStatus] ? YES : NO;
            break;
        }
        default: {
            filterName = object;
            isSelected = NO;
            break;
        }
    }
    return @{@"name" : [Helper removeWhiteSpaceAndNewLineCharacter:filterName], @"isSelected" : [NSNumber numberWithBool:isSelected]};
}

//---------------------------------------------------------------

- (NSPredicate *) getPredicateFormat {
    NSPredicate *predicate = nil;
//    NSString *name = self.filterItem.displayName;
    // Early exit if no name exist!
//    if (!name) {
//        return nil;
//    }

    switch (self.itemId) {
//        case IndustryFilter: {
//            predicate = [NSPredicate predicateWithFormat:@"SELF.industryDescription = %@", name];
//            break;
//        }
//            
//        case TenantFilter: {
//            predicate = [NSPredicate predicateWithFormat:@"SELF.tenantName = %@", name];
//            break;
//        }
//            
//        case GeographyType: {
//            predicate = [NSPredicate predicateWithFormat:@"SELF.region.name = %@", name];
//            break;
//        }
            
//        case RegionTypeFilter: {
//            predicate = [NSPredicate predicateWithFormat:@"SELF.name = %@", self.filter];
//            break;
//        }
//        case RegionFilter: {
//            predicate = [NSPredicate predicateWithFormat:@"SELF.completeName = %@", name];
//            break;
//        }
        case LocationFilter: {
            predicate = [NSPredicate predicateWithFormat:@"SELF.locationId.intValue = %d", self.filter.locationId.intValue];
            break;
        }
        case LocationGroupFilter: {
            predicate = [NSPredicate predicateWithFormat:@"SELF.locationGroupId.intValue = %d", self.filter.locationGroupId.intValue];
            break;
        }
        case ProductFilter: {
            predicate = [NSPredicate predicateWithFormat:@"SELF.productId.intValue = %d", self.filter.productId.intValue];
            break;
        }
        case UserFilter: {
            predicate = [NSPredicate predicateWithFormat:@"SELF.userID.intValue = %d", self.filter.userId.intValue];
            break;
        }
        case MetricTypeFilter: {
            predicate = nil;
            break;
        }
            // Observation
        case PerformanceLeaderFilter: {
            predicate = [NSPredicate predicateWithFormat:@"SELF.userID.intValue = %d", self.filter.performanceLeaderId.intValue];
            break;
        }
        case AgentFilter: {
            predicate = [NSPredicate predicateWithFormat:@"SELF.userID.intValue = %d", self.filter.agentId.intValue];
            break;
        }
        case ObservationTypeFilter: {
            predicate = [NSPredicate predicateWithFormat:@"SELF.observationTypeId.intValue = %d", self.filter.obTypeId.intValue];
            break;
        }
        case ObservationCategory: {
            predicate = [NSPredicate predicateWithFormat:@"SELF.categoryId.intValue = %d", self.filter.categoryId.intValue];
            break;
        }
        case ObservationStatus: {
            predicate = nil;
            break;
        }
        default: {
            predicate = nil;
            break;
        }
    }
    return predicate;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *searchText = [Helper removeWhiteSpaceAndNewLineCharacter:self.searchBar.text];
    if (searchText.length > 0) {
        return self.searchArray.count;
    } else {
        return self.dataArray.count;
    }
}

//---------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return (self.selectionArray.count > 0) ? kHeaderHeight : 0;
}

//---------------------------------------------------------------

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"FilterItemCell%d%d", (int)indexPath.section, (int)indexPath.row];
    
    FilterItemCell *cell = (FilterItemCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[FilterItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSArray *dataSource = self.dataArray;
    NSString *searchText = [Helper removeWhiteSpaceAndNewLineCharacter:self.searchBar.text];
    if (searchText.length > 0) {
        dataSource = self.searchArray;
    }
    
    cell.arrowImage.hidden = YES;
    cell.selectedItemLabel.text = @"";
    cell.selectedItemLabel.hidden = YES;
    NSDictionary *details = [self isSelected:[dataSource objectAtIndex:indexPath.row]];
    cell.itemLabel.text = [[details allKeys] containsObject:@"name"] ? [details objectForKey:@"name"] : @"";

    NSNumber *selected = [details objectForKey:@"isSelected"];
    cell.accessoryView = selected.boolValue ? [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tick"]] : nil;
    return cell;
}

//---------------------------------------------------------------

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *dataSource = self.dataArray;
    NSString *searchText = [Helper removeWhiteSpaceAndNewLineCharacter:self.searchBar.text];
    if (searchText.length > 0) {
        dataSource = self.searchArray;
    }
    switch (self.itemId) {
        case RegionTypeFilter: {
            RegionType *type = [dataSource objectAtIndex:indexPath.row];
            self.filter.regionTypeName = type.name;
            self.filter.regionTypeId = type.regionTypeId;
            break;
        }
        case RegionFilter: {
            Region *region = [dataSource objectAtIndex:indexPath.row];
            self.filter.regionName = region.completeName;
            self.filter.regionId = region.regionId;
            break;
        }
        case LocationFilter: {
            Location *location = [dataSource objectAtIndex:indexPath.row];
            self.filter.locationName = location.name;        
            self.filter.locationId = location.locationId;
            self.filter.metricTypeName = [Helper getMetricType:location.hotelMetricsDataType];
            break;
        }
        case LocationGroupFilter: {
            LocationGroup *group = [dataSource objectAtIndex:indexPath.row];
            self.filter.locationGroupName = group.locationGroupName;
            self.filter.locationGroupId = group.locationGroupID;
            break;
        }
        case ProductFilter: {
            Product *product = [dataSource objectAtIndex:indexPath.row];
            self.filter.productName = product.tenantProductName;
            self.filter.productId = product.productId;
            break;
        }
        case UserFilter: {
            User *user = [dataSource objectAtIndex:indexPath.row];
            self.filter.userName = user.name;
            self.filter.userId = user.userID;
            break;
        }
        case MetricTypeFilter: {
            NSString *metricType = [dataSource objectAtIndex:indexPath.row];
            self.filter.metricTypeName = metricType;
            break;
        }

        // Observation
        case PerformanceLeaderFilter: {
            User *user = [dataSource objectAtIndex:indexPath.row];
            self.filter.performanceLeaderId = user.userID;
            break;
        }
        case AgentFilter: {
            User *user = [dataSource objectAtIndex:indexPath.row];
            self.filter.agentId = user.userID;
            break;
        }
        case ObservationTypeFilter: {
            ObservationType *obType = [dataSource objectAtIndex:indexPath.row];
            self.filter.obTypeId = obType.observationTypeId;
            break;
        }
        case ObservationCategory: {
            OBCategory *category = [dataSource objectAtIndex:indexPath.row];
            self.filter.categoryName = category.name;
            self.filter.categoryId = category.categoryId;
            break;
        }
        case ObservationStatus: {
            NSString *status = [dataSource objectAtIndex:indexPath.row];
            self.filter.surveyStatus = status;
            break;
        }
        default: {
            break;
        }
    }
    [self.delegate reloadFilters:self.itemId withFilter:self.filter];
    [self.navigationController popViewControllerAnimated:YES];
}

//---------------------------------------------------------------

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, kHeaderHeight)];
    headerView.backgroundColor = [UIColor whiteColor];
    headerView.tag = section;
    
    // Drop shadow
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:headerView.bounds];
    headerView.layer.masksToBounds = NO;
    headerView.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    headerView.layer.shadowOffset = CGSizeMake(0.0f, 0.1f);
    headerView.layer.shadowOpacity = 0.5f;
    headerView.layer.shadowPath = shadowPath.CGPath;
    
    
    UIView *cardView = [[UIView alloc] init];
    cardView.translatesAutoresizingMaskIntoConstraints = NO;
    cardView.backgroundColor = [UIColor clearColor];
    [headerView addSubview:cardView];
    
    // Card view constraints
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cardView]|" options: 0 metrics:nil views:@{@"cardView" : cardView}]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cardView]-1-|" options: 0 metrics:nil views:@{@"cardView" : cardView}]];
    
    // User label
    UILabel *itemLabel = [[UILabel alloc] init];
    itemLabel.font = [UIFont fontWithName:AVENIR_FONT size:14];
    itemLabel.numberOfLines = 0;
    itemLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    itemLabel.textAlignment = NSTextAlignmentLeft;
    [itemLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [cardView addSubview:itemLabel];
    
    // User image
    UIImageView *arrowImage = [[UIImageView alloc] init];
    arrowImage.translatesAutoresizingMaskIntoConstraints = NO;
    arrowImage.image = [UIImage imageNamed:@"tick"];
    [cardView addSubview:arrowImage];
    
    NSDictionary *details = [self isSelected:[self.selectionArray objectAtIndex:section]];
    itemLabel.text = [[details allKeys] containsObject:@"name"] ? [details objectForKey:@"name"] : @"";
    arrowImage.hidden = NO;
    
    // Constraints
    NSDictionary *views = @{@"itemLabel" : itemLabel, @"arrowImage" : arrowImage};
    
    [cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[itemLabel][arrowImage(12)]-10-|" options: 0 metrics:nil views:views]];
    [cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[itemLabel]|" options: 0 metrics:nil views:views]];
    [cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[arrowImage(12)]" options:0 metrics:nil views:views]];
    
    [cardView addConstraint:[NSLayoutConstraint constraintWithItem:arrowImage
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:cardView
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1
                                                          constant:0]];
    return headerView;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UISearch delegate methods

//---------------------------------------------------------------

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    return YES;
}

//---------------------------------------------------------------

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return YES;
}

//---------------------------------------------------------------

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    @try {
        if (isEmptyString(searchText)) {
            self.searchArray = self.dataArray;
        } else {
            searchText = [[searchText trim] lowercaseString];
            NSMutableArray *tempArray = [NSMutableArray array];
            for (id option in self.dataArray) {
                
                NSString *optionText = @"";
                switch (self.itemId) {
                    case RegionTypeFilter: {
                        RegionType *type = (RegionType *)option;
                        optionText = type.name;
                        break;
                    }
                    case RegionFilter: {
                        Region *region = (Region *)option;
                        optionText = region.completeName;
                        break;
                    }
                    case LocationFilter: {
                        Location *location = (Location *)option;
                        optionText = location.name;
                        break;
                    }
                    case LocationGroupFilter: {
                        LocationGroup *group = (LocationGroup *)option;
                        optionText = group.locationGroupName;
                        break;
                    }
                    case ProductFilter: {
                        Product *product = (Product *)option;
                        optionText = product.tenantProductName;
                        break;
                    }
                    case UserFilter: {
                        User *user = (User *)option;
                        optionText = user.name;
                        break;
                    }
                    case MetricTypeFilter: {
                        optionText = option;
                        break;
                    }
                        // Observation
                    case PerformanceLeaderFilter: {
                        User *user = (User *)option;
                        optionText = user.name;
                        break;
                    }
                    case AgentFilter: {
                        User *user = (User *)option;
                        optionText = user.name;
                        break;
                    }
                    case ObservationTypeFilter: {
                        ObservationType *obType = (ObservationType *)option;
                        optionText = obType.name;
                        break;
                    }
                        
                    case ObservationCategory: {
                        OBCategory *category = (OBCategory *)option;
                        optionText = category.name;
                        break;
                    }
                    case ObservationStatus: {
                        optionText = option;
                        break;
                    }
                        
                    default: {
                        optionText = option;
                        break;
                    }
                }
                
                if ([[optionText lowercaseString] myContainsString:searchText]) {
                    [tempArray addObject:option];
                }
            }
            self.searchArray = [NSArray arrayWithArray:tempArray];
            tempArray = nil;
        }
        [self.tableView reloadData];
    }
    @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

//---------------------------------------------------------------

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    return YES;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    
    // Search Bar UI
    self.searchBar.delegate = self;
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setFont:[UIFont fontWithName:AVENIR_FONT size:12]];
    [self.searchBar setReturnKeyType:UIReturnKeyDone];
    [self.searchBar setKeyboardAppearance:UIKeyboardAppearanceDefault];

    // TableView settings
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    //    self.tableView.estimatedRowHeight = UITableViewAutomaticDimension;
    //    self.tableView.estimatedRowHeight = 140;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    _noRecordsView = [Helper createNoAccessView:self.view withMessage:[App_Delegate.keyMapping getKeyValue:@"KEY_NO_DATA"]];
    
    switch (self.itemId) {
        case RegionTypeFilter: self.dataArray = self.regionTypeList.regionTypeList; break;
        case RegionFilter: self.dataArray = self.regionList.regionList; break;
        case LocationFilter: self.dataArray = self.locationList.locationList; break;
        case LocationGroupFilter: self.dataArray = self.locationGroupList.locationGroupList; break;
        case ProductFilter: self.dataArray = self.productList.productList; break;
        case UserFilter: self.dataArray = self.userList.userList; break;
        case MetricTypeFilter: self.dataArray = self.metricArray; break;
            // Observation
        case PerformanceLeaderFilter: self.dataArray = self.performanceLeaderList.userList; break;
        case AgentFilter: self.dataArray = self.agentList.userList; break;
        case ObservationTypeFilter: self.dataArray = self.obTypeList.obList; break;
        case ObservationCategory: self.dataArray = self.categoryList.categoryList; break;
        case ObservationStatus: self.dataArray = self.statusArray; break;
        default:
            break;
    }
    
    if (self.dataArray.count > 0) {
        [self.noRecordsView setHidden:YES];
        [self.tableView setHidden:NO];
        
        // Check that selected item is there or not?
        // Show selected item at top of list
        NSPredicate *predicate = [self getPredicateFormat];
        if (predicate) {
            NSArray *filterArray = [self.dataArray filteredArrayUsingPredicate:predicate];
            
            // No selection if there is only one item!
            if (filterArray.count == self.dataArray.count) {
                // there is no need of selection array
            } else {
                if (filterArray.count) {
                    // Add selected item
                    self.selectionArray = @[filterArray[0]];
                    
                    // Remove selected object from Main list
                    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.dataArray];
                    [tempArray removeObject:filterArray[0]];
                    self.dataArray = [NSArray arrayWithArray:tempArray];
                    tempArray = nil;
                }
            }
        }
        [self.tableView reloadData];
        
    } else {
        [self.noRecordsView setHidden:NO];
        [self.tableView setHidden:YES];
    }
}

//---------------------------------------------------------------

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

//---------------------------------------------------------------

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

//---------------------------------------------------------------

@end
