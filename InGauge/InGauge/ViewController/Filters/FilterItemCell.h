//
//  FilterCell.h
//  InGauge
//
//  Created by Mehul Patel on 25/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterItemCell : UITableViewCell

@property (nonatomic, strong) UILabel               *itemLabel;
@property (nonatomic, strong) UILabel               *selectedItemLabel;
@property (nonatomic, strong) UIImageView           *arrowImage;
@property (nonatomic, strong) UIView                *cardView;

@end
