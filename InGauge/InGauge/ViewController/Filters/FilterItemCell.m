//
//  FilterCell.m
//  InGauge
//
//  Created by Mehul Patel on 25/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "FilterItemCell.h"
#import "Helper.h"

@implementation FilterItemCell

//---------------------------------------------------------------

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor whiteColor];
    
    _cardView = [[UIView alloc] init];
    self.cardView.translatesAutoresizingMaskIntoConstraints = NO;
    self.cardView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.cardView];
    
    // Card view constraints
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cardView]|" options: 0 metrics:nil views:@{@"cardView" : self.cardView}]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cardView]|" options: 0 metrics:nil views:@{@"cardView" : self.cardView}]];
    
    
    // User label
    _itemLabel = [[UILabel alloc] init];
    self.itemLabel.font = [UIFont fontWithName:AVENIR_FONT size:14];
    self.itemLabel.numberOfLines = 0;
    self.itemLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.itemLabel.textAlignment = NSTextAlignmentLeft;
    [self.itemLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.cardView addSubview:self.itemLabel];
    
    // User label
    _selectedItemLabel = [[UILabel alloc] init];
    self.selectedItemLabel.font = [UIFont fontWithName:AVENIR_FONT_LIGHT size:11];
    self.selectedItemLabel.numberOfLines = 0;
    self.selectedItemLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.selectedItemLabel.textAlignment = NSTextAlignmentLeft;
    [self.selectedItemLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.cardView addSubview:self.selectedItemLabel];
    
    // user image
    _arrowImage = [[UIImageView alloc] init];
    self.arrowImage.alpha = 0.8;
    self.arrowImage.translatesAutoresizingMaskIntoConstraints = NO;
    self.arrowImage.image = [UIImage imageNamed:RIGHT_ARROW];
    [self.cardView addSubview:self.arrowImage];
    
    self.selectedItemLabel.text = @"Detail label";
    
    // Constraints
    NSDictionary *views = @{@"itemLabel" : self.itemLabel, @"selectedItemLabel" : self.selectedItemLabel, @"arrowImage" : self.arrowImage};
    
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[itemLabel][arrowImage(12)]-10-|" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[selectedItemLabel][arrowImage(12)]-10-|" options: 0 metrics:nil views:views]];

    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-6-[itemLabel][selectedItemLabel]-6-|" options: 0 metrics:nil views:views]];

    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[arrowImage(12)]" options:0 metrics:nil views:views]];
    
    [self.cardView addConstraint:[NSLayoutConstraint constraintWithItem:self.arrowImage
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.cardView
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1
                                                      constant:0]];

    return self;
}

//---------------------------------------------------------------

- (void) layoutSubviews {
    [super layoutSubviews];
}

@end
