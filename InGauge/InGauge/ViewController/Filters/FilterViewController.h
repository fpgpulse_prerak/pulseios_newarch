//
//  FilterViewController.h
//  InGauge
//
//  Created by Mehul Patel on 25/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ServiceManager.h"

@protocol FilterViewDelegate <NSObject>

- (void) reloadDashboardDetails:(Filters *)filter;
- (void) updateFilterArray:(NSArray *)updatedArray forFilterType:(FilterType)filterType;

@end

@interface FilterViewController : BaseViewController

@property (nonatomic, weak) id <FilterViewDelegate>         delegate;

@property (nonatomic, strong) RegionTypeList            *regionTypeList;
@property (nonatomic, strong) RegionList                *regionList;
@property (nonatomic, strong) LocationList              *locationList;
@property (nonatomic, strong) LocationGroupList         *locationGroupList;
@property (nonatomic, strong) ProductList               *productList;
@property (nonatomic, strong) UserList                  *userList;
@property (nonatomic, strong) NSArray                   *metricArray;

@property (nonatomic, strong) NSString                  *SelectedList;
@property (nonatomic, strong) Filters                   *filter;

@end
