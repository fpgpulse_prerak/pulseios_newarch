//
//  FilterViewController.m
//  InGauge
//
//  Created by Mehul Patel on 25/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "FilterViewController.h"
#import "FilterItemCell.h"
#import "FilterDetailViewController.h"
#import "AppDelegate.h"

@interface FilterViewController () <UITableViewDataSource, UITableViewDelegate, FilterDetailDelegate>

@property (nonatomic, weak) IBOutlet UITableView        *tableView;
@property (nonatomic, weak) IBOutlet UIButton           *applyButton;
@property (nonatomic, weak) IBOutlet UIButton           *clearButton;

@property (nonatomic, strong) NSArray                   *dataArray;

@end

@implementation FilterViewController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) applyButtonTapped:(id)sender {
    [self.delegate reloadDashboardDetails:self.filter];
    [self.navigationController popViewControllerAnimated:YES];
}

//---------------------------------------------------------------

- (IBAction) clearButtonTapped:(id)sender {
    
    [self getRegions:^(BOOL isFinish) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.tableView reloadData];
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WEBSERVICE methods

//---------------------------------------------------------------

- (void) getRegions:(void(^)(BOOL isFinish))block {
    // Show loading indicator
    MBProgressHUD *hud = [Helper showLoading:self.view];
    // Get regions
    
    [ServiceManager callWebServiceToGetRegions:self.filter usingBlock:^(RegionList *regionList) {
        [Helper hideLoading:hud];
        
        self.regionList = regionList;
//        [self.delegate updateFilterArray:self.regionList.regionList forFilterType:RegionFilter];
        
        self.filter.regionName = @"";
        
        // Get locations
        if (self.regionList.regionList.count > 0) {
            // First region
            Region *region = [self.regionList.regionList objectAtIndex:0];
            self.filter.regionName = region.completeName;
            self.filter.orderBy = KEY_ORDER_BY_VALUE;
            self.filter.sort = KEY_SORT_BY_ASC_VALUE;
            self.filter.regionId = region.regionId;
            self.filter.activeStatus = KEY_PARAM_CONST_ACTIVE_STATUS;
            [self getTenantLocations:^(BOOL isFinish) {
                block(isFinish);
            }];
        } else {
            // Remove filter
            self.filter.regionId = [NSNumber numberWithInt:0];
        }
        [self.tableView reloadData];

    }];
}

//---------------------------------------------------------------

- (void) getTenantLocations:(void(^)(BOOL isFinish))block {
    // Get locations
    
    MBProgressHUD *hud = [Helper showLoading:self.view];
    NSDictionary *details = [self.filter serializeFiltersForLocationService];
    [ServiceManager callWebServiceToGetLocations:details usingBlock:^(LocationList *locationList) {
        [Helper hideLoading:hud];
        self.locationList = locationList;
        [self.delegate updateFilterArray:self.locationList.locationList forFilterType:LocationFilter];
        
        self.filter.locationName = @"";
        // Get location group
        if (self.locationList.locationList.count > 0) {
            
            // First location
            Location *location = [self.locationList.locationList objectAtIndex:0];
            self.filter.locationId = location.locationId;
            self.filter.locationName = location.name;
            self.filter.metricTypeName = [Helper getMetricType:location.hotelMetricsDataType];
            [self getLocationGroups:^(BOOL isFinish) {
                block(isFinish);
            }];
        } else {
            // Remove filter
            self.filter.locationId = [NSNumber numberWithInt:0];
        }
        [self.tableView reloadData];

    }];
}

//---------------------------------------------------------------

- (void) getLocationGroups:(void(^)(BOOL isFinish))block {
    MBProgressHUD *hud = [Helper showLoading:self.view];
    [ServiceManager callWebServiceToGetLocationGroup:self.filter.locationId usingBlock:^(LocationGroupList *locationGroupList) {
        [Helper hideLoading:hud];
        
        self.locationGroupList = locationGroupList;
        [self.delegate updateFilterArray:self.locationGroupList.locationGroupList forFilterType:LocationGroupFilter];
        
        // Get location group
        self.filter.locationGroupName = @"";
        if (self.locationGroupList.locationGroupList.count > 0) {
            // First location group
            LocationGroup *locationGroup = [self.locationGroupList.locationGroupList objectAtIndex:0];
            self.filter.locationGroupId = locationGroup.locationGroupID;
            self.filter.locationGroupName = locationGroup.locationGroupName;
            [self getProducts:^(BOOL isFinish) {
                block(isFinish);
            }];
            [self getUsers:^(BOOL isFinish) {
                block(isFinish);
            }];
        } else {
            // Remove filter
            self.filter.locationGroupId = [NSNumber numberWithInt:0];
        }
        [self.tableView reloadData];

    }];
}

//---------------------------------------------------------------

- (void) getProducts:(void(^)(BOOL isFinish))block {
    MBProgressHUD *hud = [Helper showLoading:self.view];
    [ServiceManager callWebServiceToGetProducts:self.filter usingBlock:^(ProductList *productList) {
        [Helper hideLoading:hud];
        self.productList = productList;
        [self.delegate updateFilterArray:self.productList.productList forFilterType:ProductFilter];
        
        self.filter.productName = @"";
        if (self.productList.productList.count > 0) {
            Product *product = [self.productList.productList objectAtIndex:0];
            self.filter.productId = product.productId;            
            self.filter.productName = product.tenantProductName;
        }
        else {
            // Remove filter
            self.filter.productId = [NSNumber numberWithInt:0];
        }
        [self.tableView reloadData];
        block(YES);
    }];
}

//---------------------------------------------------------------

- (void) getUsers:(void(^)(BOOL isFinish))block {
    MBProgressHUD *hud = [Helper showLoading:self.view];
    // Get Users list
    NSDictionary *details = [self.filter serializeDashboardFiltersForUsersService];
    [ServiceManager callWebServiceToGetUsers:self.filter withDetails:details usingBlock:^(UserList *userList) {
        [Helper hideLoading:hud];
        self.userList = userList;
        [self.delegate updateFilterArray:self.userList.userList forFilterType:UserFilter];
        self.filter.userName = @"";
        
        if (self.userList.userList.count > 0) {
            User *user = [self.userList.userList objectAtIndex:0];
            self.filter.userId = user.userID;            
            self.filter.userName = user.name;
        }
        else {
            // Remove filter
            self.filter.userId = [NSNumber numberWithInt:0];
        }    
        [self.tableView reloadData];
        block(YES);
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------


//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

//---------------------------------------------------------------

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"FilterItemCell%d%d", (int)indexPath.section, (int)indexPath.row];
    
    FilterItemCell *cell = (FilterItemCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[FilterItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }    
    cell.itemLabel.text = [self.dataArray objectAtIndex:indexPath.row];

    switch (indexPath.row) {
        case RegionTypeFilter: {
            cell.selectedItemLabel.text = self.filter.regionTypeName;
            break;
        }
        case RegionFilter: {
            cell.selectedItemLabel.text = self.filter.regionName;
            break;
        }
        case LocationFilter: {
            cell.selectedItemLabel.text = self.filter.locationName;
            break;
        }
        case LocationGroupFilter: {
            cell.selectedItemLabel.text = self.filter.locationGroupName;
            break;
        }
        case ProductFilter: {
            cell.selectedItemLabel.text = self.filter.productName;
            break;
        }
        case UserFilter: {
            cell.selectedItemLabel.text = self.filter.userName;
            break;
        }
        case MetricTypeFilter: {
            cell.selectedItemLabel.text = self.filter.metricTypeName;
            break;
        }
        default:
            cell.selectedItemLabel.text = @"";
            break;
    }
    return cell;
}

//---------------------------------------------------------------

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FilterDetailViewController *controller = [App_Delegate.mainStoryboard instantiateViewControllerWithIdentifier:@"FilterDetailViewController"];
    controller.filter = self.filter;
    controller.delegate = self;
    controller.itemId = indexPath.row;
    controller.regionTypeList = self.regionTypeList;
    controller.regionList = self.regionList;
    controller.locationList = self.locationList;
    controller.locationGroupList = self.locationGroupList;
    controller.productList = self.productList;
    controller.userList = self.userList;
    controller.metricArray = self.metricArray;
    [self.navigationController pushViewController:controller animated:YES];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark FilterDetailView delegate methods

//---------------------------------------------------------------

- (void) reloadFilters:(FilterType)filterType withFilter:(Filters *)filter {
    self.filter = filter;
    switch (filterType) {
        case RegionTypeFilter: {
            [self getRegions:^(BOOL isFinish) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self.tableView reloadData];
            }];
            break;
        }
        case RegionFilter: {
            [self getTenantLocations:^(BOOL isFinish) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self.tableView reloadData];
            }];
            break;
        }
        case LocationFilter: {
            [self getLocationGroups:^(BOOL isFinish) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self.tableView reloadData];
            }];
            break;
        }
        case LocationGroupFilter: {
            [self getProducts:^(BOOL isFinish) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self.tableView reloadData];
            }];
            [self getUsers:^(BOOL isFinish) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self.tableView reloadData];
            }];
            break;
        }
        case ProductFilter: {
//            [self get]
            break;
        }
        case UserFilter: {
            break;
        }
        default:
            break;
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    
    self.title = [App_Delegate.keyMapping getKeyValue:@"KEY_FILTER"];
    
    // TableView settings
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
//    self.tableView.estimatedRowHeight = UITableViewAutomaticDimension;
//    self.tableView.estimatedRowHeight = 140;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    // Button border
    //self.applyButton.layer.cornerRadius = 6.0f;
    //self.applyButton.layer.masksToBounds = YES;
    self.applyButton.backgroundColor = APP_COLOR;   

    //self.clearButton.layer.cornerRadius = 6.0f;
    //self.clearButton.layer.masksToBounds = YES;
    self.clearButton.backgroundColor = APP_COLOR;
    
    //TODO: Temporary data - Region need to add
    self.dataArray = @[[App_Delegate.keyMapping getKeyValue:@"KEY_REGION_TYPE"], [App_Delegate.keyMapping getKeyValue:@"KEY_REGION"], [App_Delegate.keyMapping getKeyValue:@"KEY_LOCATION"],[App_Delegate.keyMapping getKeyValue:@"KEY_LOCATION_GROUP"], [App_Delegate.keyMapping getKeyValue:@"KEY_PRODUCT"], [App_Delegate.keyMapping getKeyValue:@"KEY_USER"],[App_Delegate.keyMapping getKeyValue:@"KEY_METRIC_TYPE"]];
    
    [self.applyButton setTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_APPLY"] forState:UIControlStateNormal];
    [self.clearButton setTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_CLEAR"] forState:UIControlStateNormal];
    
}

//---------------------------------------------------------------

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

//---------------------------------------------------------------

@end
