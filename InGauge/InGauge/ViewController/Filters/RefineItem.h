//
//  RefineItem.h
//  IN-Gauge
//
//  Created by Mehul Patel on 05/10/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"

@interface RefineItem : NSObject <NSCopying>

@property (nonatomic, strong) NSNumber          *itemId;
@property (nonatomic, strong) NSNumber          *regionTypeId;
@property (nonatomic, strong) NSString          *title;
@property (nonatomic, strong) NSString          *displayName;
@property (nonatomic, strong) NSArray           *subItems;
@property (nonatomic, strong) NSArray           *selectedItems;
@property (nonatomic, assign) BOOL              isEnable;
@property (nonatomic, assign) FilterType        filterType;

@property (nonatomic, strong) GeographyModel    *selectedGeoModel;
@end
