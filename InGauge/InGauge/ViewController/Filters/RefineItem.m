//
//  RefineItem.m
//  IN-Gauge
//
//  Created by Mehul Patel on 05/10/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "RefineItem.h"

@implementation RefineItem


//---------------------------------------------------------------

- (id)copyWithZone:(NSZone *)zone {
    RefineItem *item = [RefineItem new];
    item.itemId = self.itemId;
    item.title = self.title;
    item.regionTypeId = self.regionTypeId;
    item.displayName = self.displayName;
    item.subItems = self.subItems;
    item.isEnable = self.isEnable;
    item.filterType = self.filterType;
    return item;
}

@end
