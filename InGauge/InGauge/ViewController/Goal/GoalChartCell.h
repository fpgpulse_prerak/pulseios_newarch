//
//  GoalChartCell.h
//  IN-Gauge
//
//  Created by Mehul Patel on 20/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "OffSetLabel.h"
#import <Highcharts/Highcharts.h>
#import "GoalModel.h"

@interface GoalChartCell : UITableViewCell

@property (nonatomic, strong) UILabel               *titleLabel;
@property (nonatomic, strong) OffSetLabel           *chartValueLabel;
@property (nonatomic, strong) UILabel               *goalLabel;

@property (nonatomic, strong) HIChartView           *highChart;
@property (nonatomic, strong) HIPie                 *pieChart;
@property (nonatomic, strong) GoalModel             *goalModel;
@property (nonatomic, strong) NSIndexPath           *indexPath;

- (instancetype) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withGoalModel:(GoalModel *)model forIndexPath:(NSIndexPath *)indexPath;

@end
