//
//  GoalChartCell.m
//  IN-Gauge
//
//  Created by Mehul Patel on 20/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "GoalChartCell.h"

@implementation GoalChartCell

//---------------------------------------------------------------

- (instancetype) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withGoalModel:(GoalModel *)model forIndexPath:(NSIndexPath *)indexPath {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    self.goalModel = model;
    self.indexPath = indexPath;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = BACK_COLOR;
    
    // UIView - just for background view
    UIView *backView = [[UIView alloc] init];
    [backView setBackgroundColor:[UIColor whiteColor]];
    [backView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.contentView addSubview:backView];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[backView]-10-|" options: 0 metrics:nil views:@{@"backView" : backView}]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[backView]-10-|" options: 0 metrics:nil views:@{@"backView" : backView}]];

    // Chart label
    _chartValueLabel = [[OffSetLabel alloc] init];
    self.chartValueLabel.font = [UIFont fontWithName:AVENIR_FONT_HEAVY size:20];
    self.chartValueLabel.numberOfLines = 0;
    self.chartValueLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.chartValueLabel.textAlignment = NSTextAlignmentCenter;
    self.chartValueLabel.textColor = APP_COLOR;
    [self.chartValueLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.contentView addSubview:self.chartValueLabel];
    self.chartValueLabel.backgroundColor = [UIColor clearColor];
    self.chartValueLabel.edgeInsets = UIEdgeInsetsMake(-10, 0, 0, 0);
    
    // Title label
    _titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont fontWithName:AVENIR_FONT_HEAVY size:12];
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.textColor = [UIColor whiteColor];
    [self.titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.contentView addSubview:self.titleLabel];
    self.titleLabel.backgroundColor = APP_COLOR;
    
    // Title label
    _goalLabel = [[UILabel alloc] init];
    self.goalLabel.font = [UIFont fontWithName:AVENIR_NEXT_FONT size:40.0f];
    self.goalLabel.numberOfLines = 0;
    self.goalLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.goalLabel.textAlignment = NSTextAlignmentCenter;
    self.goalLabel.textColor = APP_COLOR;
    [self.goalLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.contentView addSubview:self.goalLabel];

    self.goalLabel.text = @"$0.00";
    
    // Constraints
    NSDictionary *views = @{@"titleLabel" : self.titleLabel, @"chartValueLabel" : self.chartValueLabel, @"goalLabel" : self.goalLabel};

    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[chartValueLabel]-10-|" options: 0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[titleLabel]-10-|" options: 0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[goalLabel]-10-|" options: 0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[goalLabel]-50-|" options: 0 metrics:nil views:views]];

    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[chartValueLabel(35)]-0-[titleLabel(45)]-5-|" options: 0 metrics:nil views:views]];

    // Create highchart view
    [self createChartView];
    return self;
}

//---------------------------------------------------------------

- (void) layoutSubviews {
    [super layoutSubviews];
    // High Chart frame
    CGFloat width = self.frame.size.width - 20;
    CGRect rect = self.highChart.frame;
    rect.size.width = width;
    self.highChart.frame = rect;
}

//---------------------------------------------------------------

- (void) createChartView {
    CGFloat width = self.frame.size.width - 10;
    _highChart = [[HIChartView alloc] initWithFrame:CGRectMake(10, 5, width, 115)];
    
    HIOptions *options = [[HIOptions alloc]init];
    HIChart *chart = [[HIChart alloc]init];
    chart.plotBackgroundColor = [[HIColor alloc]init];
    chart.plotBorderWidth = [[NSNumber alloc]init];
    chart.plotShadow = @NO;
    chart.type = @"pie";
    chart.margin = @[@0, @0, @0, @0]; // margin: (Top, right, bottom, left)
    
    HITitle *title = [[HITitle alloc]init];
    title.text = @"";
    
    HITooltip *tooltip = [[HITooltip alloc]init];
    tooltip.pointFormat = @"{series.name}: <b>{point.percentage:.1f}%</b>";
    
    HIPlotOptions *plotoptions = [[HIPlotOptions alloc]init];
    plotoptions.pie = [[HIPlotOptionsPie alloc]init];
    plotoptions.pie.allowPointSelect = @NO;
    plotoptions.pie.cursor = @"pointer";
    plotoptions.pie.dataLabels = [[HIPlotOptionsPieDataLabels alloc]init];
    plotoptions.pie.dataLabels.enabled = @NO;
    plotoptions.pie.dataLabels.format = @"<b>{point.name}</b>: {point.percentage:.1f} %";
    plotoptions.pie.dataLabels.style = @{@"color": @"black"};
    plotoptions.pie.borderWidth = @0;
    
    _pieChart = [[HIPie alloc]init];
    self.pieChart.name = @"";
    self.pieChart.enableMouseTracking = @NO;
    
    NSNumber *pieValue = @0;
    NSNumber *differeanceValue = @0;
    HIColor *firstSliceColor = [[HIColor alloc]initWithRGB:52 green:109 blue:164];
    
    // Calcualte difference
    CGFloat diffValue = 100 - pieValue.floatValue;
    differeanceValue = [NSNumber numberWithDouble:diffValue];
    
    // Check that PieValue is nil or not?
    pieValue = pieValue ? pieValue : @0;
    
    if (App_Delegate.appfilter.industryId.integerValue == 2) {
        switch (self.indexPath.row) {
                // Charts Settings
            case 3: firstSliceColor = [[HIColor alloc]initWithRGB:50 green:143 blue:149]; break;
            case 4: firstSliceColor = [[HIColor alloc]initWithRGB:62 green:149 blue:20]; break;
            case 5: firstSliceColor = [[HIColor alloc]initWithRGB:217 green:0 blue:18]; break;
            default: break;
        }
    } else {
        switch (self.indexPath.row) {
            case 2: firstSliceColor = [[HIColor alloc]initWithRGB:50 green:143 blue:149]; break;
            case 3: firstSliceColor = [[HIColor alloc]initWithRGB:62 green:149 blue:20]; break;
            case 4: firstSliceColor = [[HIColor alloc]initWithRGB:217 green:0 blue:18]; break;
            default: break;
        }
    }
    
    // Pass data to chart
    self.pieChart.data = [NSMutableArray arrayWithObjects:
                          @{
                                @"name": @"Goal",
                                @"y": pieValue
                           },
                          @{
                                @"name": @"Remaining Goal",
                                @"y": differeanceValue,
                           }, nil];

    // Chart value label
    self.chartValueLabel.text = [NSString stringWithFormat:@"%.2f%%", pieValue.doubleValue];
    
    // Disable export button
    HIExporting *exporting = [[HIExporting alloc] init];
    exporting.enabled = @NO;
    
    // Disable credit link
    HICredits *credits = [[HICredits alloc] init];
    credits.enabled = @NO;
    
    NSArray *colors = @[
                            firstSliceColor,
                            [[HIColor alloc]initWithRGB:220 green:220 blue:220],
                        ];
    options.exporting   = exporting;
    options.credits     = credits;
    options.chart       = chart;
    options.colors      = colors;
    options.title       = title;
    options.tooltip     = tooltip;
    options.plotOptions = plotoptions;
    options.series = [NSMutableArray arrayWithObjects:self.pieChart, nil];
    self.highChart.options = options;
    [self.contentView addSubview:self.highChart];
}

//---------------------------------------------------------------

@end
