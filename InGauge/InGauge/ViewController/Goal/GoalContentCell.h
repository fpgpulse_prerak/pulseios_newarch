//
//  GoalContentCell.h
//  IN-Gauge
//
//  Created by Mehul Patel on 21/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"

@interface GoalContentCell : UITableViewCell

@property (nonatomic, strong) UILabel           *metricLabel;
@property (nonatomic, strong) UILabel           *valueLabel;

@end
