//
//  GoalContentCell.m
//  IN-Gauge
//
//  Created by Mehul Patel on 21/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "GoalContentCell.h"

@implementation GoalContentCell

//---------------------------------------------------------------

- (instancetype) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor colorWithWhite:1 alpha:0.6];
    
    // User label
    _metricLabel = [[UILabel alloc] init];
    self.metricLabel.font = [UIFont fontWithName:AVENIR_FONT size:12];
    self.metricLabel.numberOfLines = 0;
    self.metricLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.metricLabel.textAlignment = NSTextAlignmentLeft;
    [self.metricLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.contentView addSubview:self.metricLabel];
    
    // User label
    _valueLabel = [[UILabel alloc] init];
    self.valueLabel.font = [UIFont fontWithName:AVENIR_FONT size:12];
    self.valueLabel.numberOfLines = 0;
    self.valueLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.valueLabel.textAlignment = NSTextAlignmentRight;
    [self.valueLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.contentView addSubview:self.valueLabel];
    
//    self.metricLabel.backgroundColor = [UIColor redColor];
//    self.valueLabel.backgroundColor = [UIColor yellowColor];
    
    // Constraints
    NSDictionary *views = @{@"metricLabel" : self.metricLabel, @"valueLabel" : self.valueLabel};
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[metricLabel]-[valueLabel]-10-|" options: 0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[metricLabel]|" options: 0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[valueLabel]|" options: 0 metrics:nil views:views]];
    [Helper addBottomLine:self.contentView withColor:TABLE_SEPERATOR_COLOR];
    return self;
}

//---------------------------------------------------------------

- (void) layoutSubviews {
    [super layoutSubviews];
}

//---------------------------------------------------------------

@end
