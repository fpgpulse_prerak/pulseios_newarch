//
//  GoalViewController.h
//  IN-Gauge
//
//  Created by Mehul Patel on 20/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface GoalViewController : BaseViewController

@property (nonatomic, strong) Filters                   *filter;

@end
