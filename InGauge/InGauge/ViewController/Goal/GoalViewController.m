//
//  GoalViewController.m
//  IN-Gauge
//
//  Created by Mehul Patel on 20/06/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "GoalViewController.h"
#import "NTMonthYearPicker.h"
#import "GoalChartCell.h"
#import "GoalContentCell.h"
#import "SLButton.h"
#import "AnalyticsLogger.h"
#import "FilterController.h"

#define HEADER_HEIGHT 50
#define TOTAL_SECTION 7

@interface GoalViewController () <UITableViewDelegate, UITableViewDataSource, FilterDelegate>

@property (nonatomic, weak) IBOutlet UITableView        *tableView;
@property (nonatomic, weak) IBOutlet SLButton           *filterButton;
@property (nonatomic, weak) IBOutlet UIButton           *selectionButton;
@property (nonatomic, weak) IBOutlet UILabel            *monthLabel;
@property (nonatomic, weak) IBOutlet UILabel            *yearLabel;
@property (nonatomic, weak) IBOutlet UIView             *filterView;

@property (nonatomic, strong) NTMonthYearPicker         *datePickerView;
@property (nonatomic, strong) UILabel                   *navigationLabel;
@property (nonatomic, strong) UILabel                   *topLabel;
@property (nonatomic, strong) NSArray                   *chartsArray;
@property (nonatomic, strong) NSArray                   *weeklyGoalArray;
@property (nonatomic, strong) NSArray                   *goalArray;
@property (nonatomic, strong) NSArray                   *revenueArray;
@property (nonatomic, strong) NSArray                   *goalStatusArray;
@property (nonatomic, strong) NSArray                   *effortArray;
@property (nonatomic, strong) NSArray                   *verdictArray;
@property (nonatomic, strong) NSMutableArray            *arrSelectedSectionIndex;
@property (nonatomic, strong) UIView                    *noRecordsView;

@property (nonatomic, strong) GoalModel                 *goalModel;
@property (nonatomic, strong) LocationList              *locationList;
@property (nonatomic, strong) LocationGroupList         *locationGroupList;
@property (nonatomic, strong) ProductList               *productList;
@property (nonatomic, strong) UserList                  *userList;

@end

@implementation GoalViewController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
    self.arrSelectedSectionIndex = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) monthAndYearSelectionButtonTapped:(id)sender {
    [self showDatePicker];
}

//---------------------------------------------------------------

- (void) headerTapped:(UIControl *)sender {
    NSNumber *isExpand = [self.arrSelectedSectionIndex objectAtIndex:sender.tag];
    [self.arrSelectedSectionIndex replaceObjectAtIndex:sender.tag withObject:[NSNumber numberWithBool:!isExpand.boolValue]];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:sender.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
}

//---------------------------------------------------------------

- (IBAction) filterButtonTapped:(UIButton *)sender {
    if (self.filterButton.isEnabled) {
//        [self performSegueWithIdentifier:@"showFilterViewFromGoal" sender:self];
        
        // Location
        RefineItem *item1 = [RefineItem new];
        item1.itemId = self.filter.locationId;
        item1.title = [App_Delegate.keyMapping getKeyValue:@"KEY_LOCATION"];
        item1.displayName = self.filter.locationName;
        item1.subItems = self.locationList.locationList;
        item1.filterType = LocationFilter;
        
        // Location Group
        RefineItem *item2 = [RefineItem new];
        item2.itemId = self.filter.locationGroupId;
        item2.title = [App_Delegate.keyMapping getKeyValue:@"KEY_LOCATION_GROUP"];
        item2.displayName = self.filter.locationGroupName;
        item2.subItems = self.locationGroupList.locationGroupList;
        item2.filterType = LocationGroupFilter;
        
        // Product
        RefineItem *item3 = [RefineItem new];
        item3.itemId = self.filter.productId;
        item3.title = [App_Delegate.keyMapping getKeyValue:@"KEY_PRODUCT"];
        item3.displayName = self.filter.productName;
        item3.subItems = self.productList.productList;
        item3.filterType = ProductFilter;
        
        // User
        RefineItem *item4 = [RefineItem new];
        item4.itemId = self.filter.userId;
        item4.title = [App_Delegate.keyMapping getKeyValue:@"KEY_USER"];
        item4.displayName = self.filter.userName;
        item4.subItems = self.userList.userList;
        item4.filterType = UserFilter;
        
        FilterController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterController"];
        controller.delegate = self;
        controller.filterItems = @[item1, item2, item3, item4];
        controller.openFrom = OpenFromGoal;
        Filters *tempFilters = self.filter.copy;
        controller.filter = tempFilters;
        [self presentViewController:controller animated:YES completion:^{
        }];
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WEB SERVICE methods

//---------------------------------------------------------------

- (void) callFilterWebServicesInBackground {
    // Show loading on filter button
    [self.filterButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [self.filterButton showLoading];
    self.filter.accessible = [NSNumber numberWithBool:NO];
    self.filterButton.enabled = NO;
    
    // Reset all filters
    self.filter.regionTypeId    = [NSNumber numberWithInt:0];
    self.filter.regionId        = [NSNumber numberWithInt:0];
    self.filter.locationId      = [NSNumber numberWithInt:0];
    self.filter.locationGroupId = [NSNumber numberWithInt:0];
    self.filter.productId       = [NSNumber numberWithInt:0];
    self.filter.userId          = [NSNumber numberWithInt:0];
    self.filter.orderBy         = KEY_ORDER_BY_VALUE;
    self.filter.sort            = KEY_SORT_BY_ASC_VALUE;
    self.filter.activeStatus    = KEY_PARAM_CONST_ACTIVE_STATUS;
    
    // Reset default names
    self.filter.locationName    = @"";
    self.filter.locationGroupName = @"";
    self.filter.productName     = @"";
    self.filter.userName        = @"";
    
    // Call service to get locations
    NSDictionary *details = [self.filter serializeFiltersGoalSettingLocationService];
    [ServiceManager callWebServiceToGetLocations:details usingBlock:^(LocationList *locationList) {
        self.locationList = locationList;
        
        // Get location group
        if (self.locationList.locationList.count > 0) {
            // First location
            Location *location = [self.locationList.locationList objectAtIndex:0];
            self.filter.locationId = location.locationId;
            self.filter.locationName = location.name;
            [self updateTopLabelWidth];
        }

        //Update table to reflect currency sign
        [self.tableView reloadData];
        
        // Call service to get location group
        [ServiceManager callWebServiceToGetLocationGroup:self.filter.locationId usingBlock:^(LocationGroupList *locationGroupList) {
            
            self.locationGroupList = locationGroupList;
            // Get location group
            if (self.locationGroupList.locationGroupList.count > 0) {
                
                // First location group
                LocationGroup *locationGroup = [self.locationGroupList.locationGroupList objectAtIndex:0];
                self.filter.locationGroupId = locationGroup.locationGroupID;
                self.filter.getContributorsOnly = [NSNumber numberWithBool:YES];
                self.filter.locationGroupName = locationGroup.locationGroupName;
            }
            
            // Get Product list
            [ServiceManager callWebServiceToGetProducts:self.filter usingBlock:^(ProductList *productList) {
                self.productList = productList;
                if (self.productList.productList.count > 0) {

                    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.productList.productList];

                    //***NOTE: Add static product "Incremental Revenue" to get all details
                    Product *incrementalRevenueProduct = [Product new];
                    incrementalRevenueProduct.tenantProductName = [App_Delegate.keyMapping getKeyValue:@"KEY_TITLE_ARPD"];                    incrementalRevenueProduct.productId = [NSNumber numberWithInt:-1];
                    [tempArray insertObject:incrementalRevenueProduct atIndex:0];

                    // Only for Hospitality industry
                    if (App_Delegate.appfilter.industryId.integerValue == 1) {
                        //UPDATE:08 Sep 2017 - Add Second static product "Upsell Products" (https://fpgpulse.atlassian.net/browse/PA-126)
                        Product *upsellProduct = [Product new];
                        upsellProduct.tenantProductName = [App_Delegate.keyMapping getKeyValue:@"KEY_UPSELL_PRODUCTS"];
                        upsellProduct.productId = [NSNumber numberWithInt:-2];
                        [tempArray insertObject:upsellProduct atIndex:1];
                    }
                    
                    self.productList.productList = [NSArray arrayWithArray:tempArray];
                    tempArray = nil;
                    
                    // Update filters
                    Product *product = [self.productList.productList objectAtIndex:0];
                    self.filter.productId = product.productId;
                    self.filter.productName = product.tenantProductName;
                }
            }];
            
            // Call user service
            [self callWebServiceToGetUsers:^(BOOL finish) {
                [self.filterButton setImage:[UIImage imageNamed:@"filter"] forState:UIControlStateNormal];
                [self.filterButton hideLoading];
                [self callWebServiceToGetGoalProgress];
            }];
        }];
    }];
}

//---------------------------------------------------------------

- (void) callWebServiceToGetUsers:(void(^)(BOOL finish))block {
    
    // UPDATE: if login user is contributor than dont load other users
    if ([User signedInUser].isFrontlineAssociate.boolValue) {
        User *user = [User signedInUser];
        user.name = [user getFullName];
        self.userList = [UserList new];
        self.userList.userList = @[user];
        self.filter.userId = user.userID;
        self.filter.userName = user.name;

        [self updateTopLabelWidth];
        block(YES);
    } else {
        // Get Users list
        NSDictionary *details = [self.filter serializeFiltersGoalProgressUsersService];
        [ServiceManager callWebServiceToGetUsers:self.filter withDetails:details usingBlock:^(UserList *userList) {
            self.userList = userList;
            // First User
            if (self.userList.userList.count > 0) {
                User *user = [self.userList.userList objectAtIndex:0];
                
                // If previous/saved user is in list
                User *filterUser = [self.userList getUser:self.filter.userId];
                if (filterUser) {
                    user = filterUser;
                }
                
                self.filter.userId = user.userID;
                self.filter.userName = user.name;
            }
            [self updateTopLabelWidth];
            block(YES);
        }];
    }
}

//---------------------------------------------------------------

- (void) callWebServiceToGetGoalProgress {
    // Check for internet connnection
    if (![Helper checkForConnection:self]) {
        return;
    }
    
    MBProgressHUD *hud = [Helper showLoading:self.view];
    NSString *urlString = [NSString stringWithFormat:@"%@", GET_GOAL_DETAILS];
    NSDictionary *params = [self.filter serializeFiltersForGoalProgress];
    DLog(@"\n\n\n\n==========>>> Goal Progress - Service Call Begin <<<==========\n\n\n\n");

    [ServiceManager getWebServiceCall:urlString parameters:params whenFinish:^(NSString *errorMessage, NSDictionary *response, NSNumber *status) {
        [Helper hideLoading:hud];
        DLog(@"\n\n\n\n==========>>> Goal Progress - Service Call finish <<<==========\n\n\n\n");
        
        _goalModel = [GoalModel new];
        if (response) {
            @try {
                NSDictionary *details = [response objectForKey:MAIN_BODY];
                [HKJsonUtils updatePropertiesWithData:details forObject:self.goalModel];
            } @catch (NSException *exception) {
                DLog(@"Exception :%@", exception.debugDescription);
            }
        }        
        
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        [self updateUI];
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) showDatePicker {
    if (!self.datePickerView) {
        _datePickerView = [[NTMonthYearPicker alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 240)];
    }
    
    NSDate *dateFromMonthAndYear = [Helper getDateFromMonth:self.filter.month andYear:self.filter.year];
    self.datePickerView.date = dateFromMonthAndYear;
    
    NSString *title = UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation) ? @"\n\n\n\n\n\n\n\n\n" : @"\n\n\n\n\n\n\n\n\n\n\n\n" ;
        
    UIAlertController* datePickerContainer = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [datePickerContainer.view addSubview:self.datePickerView];
    
    [datePickerContainer addAction:[UIAlertAction actionWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_TITLE_CANCEL"] style:UIAlertActionStyleCancel handler:^(UIAlertAction* action) {
    }]];
    
    [datePickerContainer addAction:[UIAlertAction actionWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_SELECT"] style:UIAlertActionStyleDefault handler:^(UIAlertAction* action) {
        self.monthLabel.text = [NSString stringWithFormat:@"%@: %@",[App_Delegate.keyMapping getKeyValue:@"KEY_TITLE_MONTH"], [Helper monthNameString:self.datePickerView.date]];
        self.yearLabel.text = [NSString stringWithFormat:@"%@: %@",[App_Delegate.keyMapping getKeyValue:@"KEY_TITLE_YEAR"] ,[Helper yearString:self.datePickerView.date]];
        
        // Change month and year filter
        self.filter.month = [NSNumber numberWithInteger:[Helper getMonthFromDate:self.datePickerView.date]];
        self.filter.year = [NSNumber numberWithInteger:[Helper getYearFromDate:self.datePickerView.date]];
        
        //UPDATE: 28-sep-2017 ~ As user now changes with month and years please call user service first!
        // Call user service
        [self callWebServiceToGetUsers:^(BOOL finish) {
            [self callWebServiceToGetGoalProgress];
            // Update filters
//            [ServicePreference saveCustomObject:self.filter key:KEY_GOAL_FILTERS];
        }];
        
        //Analytics
        [AnalyticsLogger logGoalProgressFilterChangeEvent:self];
        
        //Analytics
        [AnalyticsLogger logGoalProgressDateChangeEvent:self];
    }]];
    
    [self presentViewController:datePickerContainer animated:YES completion:nil];
}

//---------------------------------------------------------------

- (void) setTopNavBarTitle {
    UIView *navBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - 140, 44)];
    _navigationLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, navBarView.frame.size.width, 20)];
    self.navigationLabel.text = [App_Delegate.keyMapping getKeyValue:@"KEY_GOAL_SETTING"];
    self.navigationLabel.textAlignment = NSTextAlignmentCenter;
    self.navigationLabel.font = [UIFont fontWithName:AVENIR_FONT size:14.0f];
    self.navigationLabel.textColor = [UIColor whiteColor];
    [navBarView addSubview:self.navigationLabel];
    
    _topLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 25, navBarView.frame.size.width, 15)];
    self.topLabel.textAlignment = NSTextAlignmentCenter;
    self.topLabel.font = [UIFont fontWithName:AVENIR_FONT size:12.0f];
    self.topLabel.textColor = [UIColor whiteColor];
    [navBarView addSubview:self.topLabel];
    
    // Add button to open filter
    UIButton *openFilterButton = [UIButton buttonWithType:UIButtonTypeSystem];
    openFilterButton.frame = CGRectMake(60, 0, self.view.frame.size.width - 150, 44);
    [openFilterButton addTarget:self action:@selector(filterButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    //    [openFilterButton setBackgroundColor:[UIColor redColor]];
    [navBarView addSubview:openFilterButton];
    self.navigationItem.titleView = navBarView;
}

//---------------------------------------------------------------

- (void) updateTopLabelWidth {
    
    NSString *locationName = [Helper removeWhiteSpaceAndNewLineCharacter: self.filter.locationName];
    NSString *userName = [Helper removeWhiteSpaceAndNewLineCharacter: self.filter.userName];

//    Filters *filter = [ServicePreference customObjectWithKey:KEY_GOAL_FILTERS];
//    if (filter != nil) {
//        locationName = [Helper removeWhiteSpaceAndNewLineCharacter: filter.locationName];
//        userName = [Helper removeWhiteSpaceAndNewLineCharacter: filter.userName];
//    }
    
    if (locationName.length > 0) {
        userName = userName.length > 0 ? [NSString stringWithFormat:@"- %@", userName] : @"";
    }
    self.topLabel.text = [NSString stringWithFormat:@"%@ %@", locationName, userName];

    float width = self.topLabel.intrinsicContentSize.width;
    CGRect frame = self.topLabel.frame;
    frame.size.width = width;
    
    // Calculate center position
    CGFloat originX = self.navigationLabel.center.x - (width/2);
    frame.origin.x = originX;
    self.topLabel.frame = frame;
    [self.topLabel sizeToFit];
}

//---------------------------------------------------------------

- (void) updateUI {
    [self createWeeklyGoalArray];
    [self createGoalMetricesArray];

    [self createRevenueMetircesArray];
    // For Car Rental industry
    if (App_Delegate.appfilter.industryId.integerValue == 2) {
        [self createRevenueMetircesArrayForCarRental];
    }
    
    [self createGoalRatioMetricesArray];
    [self createEffortMetricsArray];
    [self createVerdictArray];
    
    // Reload details
    [self.tableView reloadData];
    [self.tableView setContentOffset:CGPointZero animated:YES];
}

//---------------------------------------------------------------

- (GoalChartCell *) createGoalChartCell:(GoalChartCell *)cell forIndexPath:(NSIndexPath *) indexPath {
    cell.titleLabel.text = [self.chartsArray objectAtIndex:indexPath.row];
    
    NSNumber *pieValue = @0;
    NSNumber *differeanceValue = @0;
    HIColor *firstSliceColor = [[HIColor alloc]initWithRGB:52 green:109 blue:164];
    
    // Set counting label format
    Location *selectedLocation = [self.locationList getLocation:self.filter.locationId];
    NSString *currency = [Helper getCurrencySign:selectedLocation.regionCurrency];
    
    NSString *goalValue = @"−";
    if (App_Delegate.appfilter.industryId.integerValue == 2) {
        
        if (indexPath.row > 2) {
            cell.highChart.hidden = NO;
            cell.chartValueLabel.hidden = NO;
        } else {
            cell.highChart.hidden = YES;
            cell.chartValueLabel.hidden = YES;
        }
        
        switch (indexPath.row) {
            case 0: {
                goalValue = [NSString stringWithFormat:@"%@%@", currency, [Helper getDecimalString:self.goalModel.FpgGoalProgress.productGoal]];
                break;
            }
            case 1: {
                goalValue = [NSString stringWithFormat:@"%@%@", currency, [Helper getDecimalString:self.goalModel.SelfGoalProgress.productGoal]];
                break;
            }
            case 2: {
                goalValue = [NSString stringWithFormat:@"%@%@", currency, [Helper getDecimalString:self.goalModel.FpgGoalProgress.productARPDThisMonth]];
                break;
            }
                
                // Charts Settings
            case 3: {
                pieValue = self.goalModel.FpgGoalProgress.targetAchievedPer;
                firstSliceColor = [[HIColor alloc]initWithRGB:50 green:143 blue:149]; //50 143 149
                break;
            }
            case 4: {
                pieValue = self.goalModel.SelfGoalProgress.targetAchievedPer;
                firstSliceColor = [[HIColor alloc]initWithRGB:62 green:149 blue:20]; //62 149 20
                break;
            }
            case 5: {
                pieValue = self.goalModel.FpgGoalProgress.timeElapsed;
                firstSliceColor = [[HIColor alloc]initWithRGB:217 green:0 blue:18]; //217 0 18
                break;
            }
            default:
                pieValue = @0;
                break;
        }
    } else {
        
        // Hide My Goals for Hospitality industry
        if (indexPath.row > 1) {
            cell.highChart.hidden = NO;
            cell.chartValueLabel.hidden = NO;
        } else {
            cell.highChart.hidden = YES;
            cell.chartValueLabel.hidden = YES;
        }
        
        switch (indexPath.row) {
            case 0: {
                goalValue = [NSString stringWithFormat:@"%@%@", currency, [Helper getDecimalString:self.goalModel.FpgGoalProgress.productGoal]];
                break;
            }
                
            case 1: {
                goalValue = [NSString stringWithFormat:@"%@%@", currency, [Helper getDecimalString:self.goalModel.FpgGoalProgress.productIRThisMonth]];
                break;
            }
                
            // Charts Settings
            case 2: {
                pieValue = self.goalModel.FpgGoalProgress.targetAchievedPer;
                firstSliceColor = [[HIColor alloc]initWithRGB:50 green:143 blue:149]; //50 143 149
                break;
            }
            case 3: {
                pieValue = self.goalModel.FpgGoalProgress.timeElapsed;
                firstSliceColor = [[HIColor alloc]initWithRGB:217 green:0 blue:18]; //217 0 18
                break;
            }
            default:
                pieValue = @0;
                break;
        }
    }
    
    NSArray *colors = @[
                        firstSliceColor,
                        [[HIColor alloc]initWithRGB:220 green:220 blue:220],
                        ];
    cell.highChart.options.colors = colors;
    cell.goalLabel.text = goalValue;
    
    // Calcualte difference
    CGFloat diffValue = 100 - pieValue.floatValue;
    differeanceValue = [NSNumber numberWithDouble:diffValue];
    
    // Check that PieValue is nil or not?
    pieValue = pieValue ? pieValue : @0;
    
    // Pass data to chart
    cell.pieChart.data = [NSMutableArray arrayWithObjects:
                          @{
                                @"name": @"Goal",
                                @"y": pieValue
                            },
                          @{
                                @"name": @"Remaining Goal",
                                @"y": differeanceValue,
                            },
                          nil];
    
    HIOptions *options = cell.highChart.options;
    cell.highChart.options = options;
    
    // Chart value label
    cell.chartValueLabel.text = [NSString stringWithFormat:@"%.2f%%", pieValue.doubleValue];
    return cell;
}

//---------------------------------------------------------------
#pragma mark — Metric Array
//---------------------------------------------------------------

- (void) createWeeklyGoalArray {
    @try {
        NSArray *titles = @[
                                [App_Delegate.keyMapping getKeyValue:@"KEY_WEEKLY_GOAL"],
                                [App_Delegate.keyMapping getKeyValue:@"KEY_THIS_WEEK"]
                            ];
        // Weekly goal
        NSString *weeklyGoalString = @"−";
        NSNumber *isShowGoalAchieved = @0;
        weeklyGoalString = [Helper getDecimalString:self.goalModel.weeklyGoalPrediction.reqRevWeeklyFPG];
        isShowGoalAchieved = self.goalModel.weeklyGoalPrediction.weeklyGoalAcheivedFPG;
        
        Location *selectedLocation = [self.locationList getLocation:self.filter.locationId];
        NSString *currencySign = [Helper getCurrencySign:selectedLocation.regionCurrency];    
        NSString *weeklyAchievedGoal = [NSString stringWithFormat:@"%@%@", currencySign, weeklyGoalString];
        
        // This week
        NSString *thisWeekGoalString = @"−";
        thisWeekGoalString = [Helper getDecimalString:self.goalModel.weeklyGoalPrediction.earnedRevenueWeekly];
        NSString *thisWeekGoal = [NSString stringWithFormat:@"%@%@", currencySign, thisWeekGoalString];
        
        NSMutableArray *tempArray = [NSMutableArray new];
        NSDictionary *weeklyGoalDetails = @{
                                  KEY_TITLE : [titles objectAtIndex:0],
                                  KEY_VALUE : weeklyAchievedGoal,
                                  };
        [tempArray addObject:weeklyGoalDetails];
        
        NSDictionary *thisWeekGoalDetails = @{
                                  KEY_TITLE : [titles objectAtIndex:1],
                                  KEY_VALUE : thisWeekGoal,
                                  };
        [tempArray addObject:thisWeekGoalDetails];
        self.weeklyGoalArray = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void) createGoalMetricesArray {
    @try {
        Location *selectedLocation = [self.locationList getLocation:self.filter.locationId];
        NSString *currencySign = [Helper getCurrencySign:selectedLocation.regionCurrency];
        
        // If default selected product id is -1 then it is "Incremental Revenue"
        // Change title according to product
        Product *selectedProduct = [self.productList getProduct:self.filter.productId];
        NSString *productName = [Helper removeWhiteSpaceAndNewLineCharacter:selectedProduct.tenantProductName];
        if (selectedProduct.productId.integerValue == -1) {
            productName = [App_Delegate.keyMapping getKeyValue:@"KEY_ALL_PRODUCTS"];
        }
        
        NSArray *titles = @[
                            [NSString stringWithFormat:@"%@ %@", [App_Delegate.keyMapping getKeyValue:@"KEY_GOAL_DETAILS_VALUE1"], productName],
                            [NSString stringWithFormat:@"%@ %@", [App_Delegate.keyMapping getKeyValue:@"KEY_GOAL_DETAILS_VALUE2"], productName],
                            [App_Delegate.keyMapping getKeyValue:@"KEY_GOAL_DETAILS_VALUE3"],
                            [App_Delegate.keyMapping getKeyValue:@"KEY_GOAL_DETAILS_VALUE4"]
                            ];
        if (App_Delegate.appfilter.industryId.integerValue == 2) {
            titles = @[
                       [NSString stringWithFormat:@"%@ %@", [App_Delegate.keyMapping getKeyValue:@"KEY_GOAL_DETAILS_VALUE1"], productName],
                       [NSString stringWithFormat:@"%@ %@", [App_Delegate.keyMapping getKeyValue:@"KEY_GOAL_DETAILS_VALUE2"], productName],
                       [App_Delegate.keyMapping getKeyValue:@"KEY_GOAL_DETAILS_VALUE3"],
                       [App_Delegate.keyMapping getKeyValue:@"KEY_GOAL_DETAILS_VALUE4"]
                       ];
        }
        
        NSString *totalGoalForProd = [NSString stringWithFormat:@"%@%@", currencySign, [Helper getDecimalString:self.goalModel.FpgGoalProgress.productGoal]];
        NSString *incrRevForProd = [NSString stringWithFormat:@"%@%@", currencySign, [Helper getDecimalString:self.goalModel.FpgGoalProgress.productIRThisMonth]];
        NSString *targetAchived = [NSString stringWithFormat:@"%@%%", [Helper getDecimalString:self.goalModel.FpgGoalProgress.targetAchievedPer]];
        NSString *timeElapsed = [NSString stringWithFormat:@"%@%%", [Helper getDecimalString:self.goalModel.FpgGoalProgress.timeElapsed]];
        
        //Use "productARPDThisMonth" parameter for Car Rental industry
        if (App_Delegate.appfilter.industryId.integerValue == 2) {
            incrRevForProd = [NSString stringWithFormat:@"%@%@", currencySign, [Helper getDecimalString:self.goalModel.FpgGoalProgress.productARPDThisMonth]];
        }

        NSMutableArray *tempArray = [NSMutableArray new];
        // Total goal
        NSDictionary *totalGoalDetails = @{
                                  KEY_TITLE : [titles objectAtIndex:0],
                                  KEY_VALUE : totalGoalForProd,
                                  };
        [tempArray addObject:totalGoalDetails];
        
        // Incremental Revneue
        NSDictionary *incRevenueDetails = @{
                                  KEY_TITLE : [titles objectAtIndex:1],
                                  KEY_VALUE : incrRevForProd,
                                  };
        [tempArray addObject:incRevenueDetails];
        
        // Target Achieved
        NSDictionary *targetAchievedDetails = @{
                                  KEY_TITLE : [titles objectAtIndex:2],
                                  KEY_VALUE : targetAchived,
                                  };
        [tempArray addObject:targetAchievedDetails];
        
        // Time Elapsed
        NSDictionary *timeElapsedDetails = @{
                                  KEY_TITLE : [titles objectAtIndex:3],
                                  KEY_VALUE : timeElapsed,
                                  };
        [tempArray addObject:timeElapsedDetails];
        
        self.goalArray = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void) createRevenueMetircesArray {
    @try {
        Location *selectedLocation = [self.locationList getLocation:self.filter.locationId];
        NSString *currencySign = [Helper getCurrencySign:selectedLocation.regionCurrency];
        
        NSArray *titles = @[
                            [App_Delegate.keyMapping getKeyValue:@"KEY_REVENUE_DETAILS_VALUE1"],
                            [App_Delegate.keyMapping getKeyValue:@"KEY_REVENUE_DETAILS_VALUE2"],
                            [App_Delegate.keyMapping getKeyValue:@"KEY_REVENUE_DETAILS_VALUE3"],
                            [App_Delegate.keyMapping getKeyValue:@"KEY_REVENUE_DETAILS_VALUE4"],
                            [App_Delegate.keyMapping getKeyValue:@"KEY_REVENUE_DETAILS_VALUE5"],
                            ];
        
        NSString *revToHitGoal      = [NSString stringWithFormat:@"%@%@", currencySign, [Helper getDecimalString:self.goalModel.FpgGoalProgress.revenuetoHitGoal]];
        NSString *avgRevThisMonth   = [NSString stringWithFormat:@"%@%@", currencySign, [Helper getDecimalString:self.goalModel.FpgGoalProgress.averageRevenueMTD]];
        NSString *avgRevPerProd     = [NSString stringWithFormat:@"%@%@", currencySign, [Helper getDecimalString:self.goalModel.FpgGoalProgress.avgRevenuPerUnit]];
        NSString *projRevEOM        = [NSString stringWithFormat:@"%@%@", currencySign, [Helper getDecimalString:self.goalModel.FpgGoalProgress.projectedRevenueEOM]];;
        NSString *revNextInctTier   = [NSString stringWithFormat:@"%@%@", currencySign, [Helper getDecimalString:self.goalModel.FpgGoalProgress.revenueToHitNextTier]];
        
        NSMutableArray *tempArray = [NSMutableArray new];
        // Revenue To Hit Goal
        NSDictionary *revHitDetails = @{
                                  KEY_TITLE : [titles objectAtIndex:0],
                                  KEY_VALUE : revToHitGoal
                                  };
        [tempArray addObject:revHitDetails];
        
        // Average revenue to this month
        NSDictionary *avgRevenueDetails = @{
                                  KEY_TITLE : [titles objectAtIndex:1],
                                  KEY_VALUE : avgRevThisMonth
                                  };
        [tempArray addObject:avgRevenueDetails];
        
        // Average revenue per product
        NSDictionary *avgRevPerProductDetails = @{
                                  KEY_TITLE : [titles objectAtIndex:2],
                                  KEY_VALUE : avgRevPerProd
                                  };
        [tempArray addObject:avgRevPerProductDetails];
        
        // Projected EOM Incremental Revenue
        NSDictionary *projEOMDetails = @{
                                  KEY_TITLE : [titles objectAtIndex:3],
                                  KEY_VALUE : projRevEOM
                                  };
        [tempArray addObject:projEOMDetails];
        
        // Incremental Revenue to hit next incentive tier
        NSDictionary *revIncTierDetails = @{
                                  KEY_TITLE : [titles objectAtIndex:4],
                                  KEY_VALUE : revNextInctTier
                                  };
        [tempArray addObject:revIncTierDetails];
        
        self.revenueArray = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void) createRevenueMetircesArrayForCarRental {
    @try {
        Location *selectedLocation = [self.locationList getLocation:self.filter.locationId];
        NSString *currencySign = [Helper getCurrencySign:selectedLocation.regionCurrency];

        // If default selected product id is -1 then it is "Incremental Revenue"
        // Change title according to product
        Product *selectedProduct = [self.productList getProduct:self.filter.productId];
        NSString *productName = [Helper removeWhiteSpaceAndNewLineCharacter:selectedProduct.tenantProductName];
        
        if (selectedProduct.productId.integerValue == -1) {
            productName = [App_Delegate.keyMapping getKeyValue:@"KEY_ALL_PRODUCTS"];
        }

        NSArray *titles = @[
                            [NSString stringWithFormat:@"%@ %@", [App_Delegate.keyMapping getKeyValue:@"KEY_MTD_REVENUE"], productName],
                            [App_Delegate.keyMapping getKeyValue:@"KEY_PROJECTED_INC_REVENUE"],
                            [NSString stringWithFormat:@"%@ %@", [App_Delegate.keyMapping getKeyValue:@"KEY_YTD_REVENUE"], productName]
                            ];
        
        NSString *mtdRev    = [NSString stringWithFormat:@"%@%@", currencySign, [Helper getDecimalString:self.goalModel.FpgGoalProgress.productIRThisMonth]];
        NSString *eomRev    = [NSString stringWithFormat:@"%@ %@", currencySign, [Helper getDecimalString:self.goalModel.FpgGoalProgress.projectedRevenueEOM]];
        NSString *ytdRev    = [NSString stringWithFormat:@"%@%@", currencySign, [Helper getDecimalString:self.goalModel.FpgGoalProgress.productYTDIncRevenue]];
        
        NSMutableArray *tempArray = [NSMutableArray new];
        NSDictionary *detail1 = @{
                                  KEY_TITLE : [titles objectAtIndex:0],
                                  KEY_VALUE : mtdRev
                                  };
        [tempArray addObject:detail1];
        
        NSDictionary *detail2 = @{
                                  KEY_TITLE : [titles objectAtIndex:1],
                                  KEY_VALUE : eomRev
                                  };
        [tempArray addObject:detail2];
        
        NSDictionary *detail3 = @{
                                  KEY_TITLE : [titles objectAtIndex:2],
                                  KEY_VALUE : ytdRev
                                  };
        [tempArray addObject:detail3];
        
        self.revenueArray = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void) createGoalRatioMetricesArray {
    @try {
        
        Location *selectedLocation = [self.locationList getLocation:self.filter.locationId];
        NSString *currencySign = [Helper getCurrencySign:selectedLocation.regionCurrency];
        
        NSMutableArray *tempProductsArray = [NSMutableArray new];
        
        for (Product *product in self.goalModel.curMonthGoalMap) {
            NSString *productName = [Helper removeWhiteSpaceAndNewLineCharacter:product.locationGroupProductName];
            NSString *achievedString = product.achieved.boolValue ? [App_Delegate.keyMapping getKeyValue:@"KEY_ACHIEVED"] : [App_Delegate.keyMapping getKeyValue:@"KEY_NOT_ACHIEVED"];

            NSString *goal = @"−";
            
            if (product.fpgGoal.doubleValue > 0) {
                goal =[NSString stringWithFormat:@"%@ %@ (%@)", currencySign, [Helper getDecimalString:product.fpgGoal], achievedString];
            }
            
            NSDictionary *detail = @{
                                        KEY_TITLE : productName,
                                        KEY_VALUE : goal
                                     };
            [tempProductsArray addObject:detail];
        }
        self.goalStatusArray = [NSArray arrayWithArray:tempProductsArray];
        tempProductsArray = nil;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void) createEffortMetricsArray {
    @try {
        
        NSArray *titles = @[
                            [App_Delegate.keyMapping getKeyValue:@"KEY_EFFORT_DETAILS_VALUE1"],
                            [App_Delegate.keyMapping getKeyValue:@"KEY_EFFORT_DETAILS_VALUE2"],
                            [App_Delegate.keyMapping getKeyValue:@"KEY_EFFORT_DETAILS_VALUE3"]
                            ];
        
        NSString *noOfProdAchiveGoal    = [NSString stringWithFormat:@"%@", [Helper getDecimalString:self.goalModel.FpgGoalProgress.requiredNumberOfUnitsPerDay]];
        NSString *noOfProdNextInctTier  = [NSString stringWithFormat:@"%@", [Helper getDecimalString:self.goalModel.FpgGoalProgress.numberOfUnitsToHitNextTier]];

        //Notes
        NSString *notes = [Helper removeWhiteSpaceAndNewLineCharacter:self.goalModel.FpgGoalProgress.notes];
        NSMutableArray *tempArray = [NSMutableArray new];
        
        NSDictionary *achieveGoalDetails = @{
                                  KEY_TITLE : [titles objectAtIndex:0],
                                  KEY_VALUE : noOfProdAchiveGoal
                                  };
        
        NSDictionary *inctTierDetails = @{
                                  KEY_TITLE : [titles objectAtIndex:1],
                                  KEY_VALUE : noOfProdNextInctTier
                                  };
        
        NSDictionary *notesDetails = @{
                                  KEY_TITLE : [titles objectAtIndex:2],
                                  KEY_VALUE : notes.length > 0 ? notes : @"−"
                                  };
        
        // change effort array if industry = CarRental
        if (App_Delegate.appfilter.industryId.integerValue == 2) {
            [tempArray addObject:notesDetails];
        } else {
            [tempArray addObject:achieveGoalDetails];
            [tempArray addObject:inctTierDetails];
            [tempArray addObject:notesDetails];
        }
        self.effortArray = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void) createVerdictArray {
    @try {
        NSMutableArray *tempArray = [NSMutableArray new];
        NSString *verdictString = @"−";
        if (self.goalModel.FpgGoalProgress.verdict) {
            verdictString = self.goalModel.FpgGoalProgress.verdict;
        }
        
        NSDictionary *detail = @{
                                  KEY_TITLE : @"",
                                  KEY_VALUE : verdictString
                                  };
        [tempArray addObject:detail];
        self.verdictArray = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }

}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return TOTAL_SECTION;
}

//---------------------------------------------------------------

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.chartsArray.count;
    }
    
    NSNumber *isExpand = [self.arrSelectedSectionIndex objectAtIndex:section];
    if (isExpand.boolValue) {
        switch (section) {
            case 1: return self.weeklyGoalArray.count; break;
            case 2: return self.goalArray.count; break;
            case 3: return self.revenueArray.count; break;
            case 4: return self.goalStatusArray.count; break;
            case 5: return self.effortArray.count; break;
            case 6: return self.verdictArray.count; break;
            default: return 0; break;
        }
    } else {
        return 0;
    }
}

//---------------------------------------------------------------

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return (section == 0) ? 1 : HEADER_HEIGHT;
}

//---------------------------------------------------------------

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return (indexPath.section == 0) ? 210 : 50;
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *mainCell = [tableView dequeueReusableCellWithIdentifier:@"mainCell"];
    @try {
        if (indexPath.section == 0) {
            
            NSString *cellIdentifier = [NSString stringWithFormat:@"GoalChartCell"];
            GoalChartCell *cell = (GoalChartCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                cell = [[GoalChartCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier withGoalModel:self.goalModel forIndexPath:indexPath];
            }
            mainCell = [self createGoalChartCell:cell forIndexPath:indexPath];
        } else {
            
            NSString *cellIdentifier = [NSString stringWithFormat:@"GoalContentCell%d%d", (int)indexPath.section, (int)indexPath.row];
            GoalContentCell *cell = (GoalContentCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                cell = [[GoalContentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            NSArray *dataArray = nil;
            switch (indexPath.section) {
                case 1: dataArray = self.weeklyGoalArray; break;
                case 2: dataArray = self.goalArray; break;
                case 3: dataArray = self.revenueArray; break;
                case 4: dataArray = self.goalStatusArray; break;
                case 5: dataArray = self.effortArray; break;
                case 6: dataArray = self.verdictArray; break;
                default: break;
            }
            if (dataArray.count > 0) {
                NSDictionary *details = [dataArray objectAtIndex:indexPath.row];
                cell.metricLabel.text = [details objectForKey:KEY_TITLE];
                cell.valueLabel.text = [details objectForKey:KEY_VALUE];
            }
            mainCell = cell;
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception);
    }
    return mainCell;
}

//---------------------------------------------------------------

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    // For Top blocks we will return section with height 1
    if (section == 0) {
        return [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 1)];
    }
    
    UIControl *headerView = [[UIControl alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, HEADER_HEIGHT)];
    [headerView setBackgroundColor:[UIColor whiteColor]];
    headerView.tag = section;
    
    [Helper addBottomLine:headerView withColor:TABLE_SEPERATOR_COLOR];
    // Drop shadow
    headerView.layer.shadowColor = TABLE_SEPERATOR_COLOR.CGColor;
    headerView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    headerView.layer.shadowRadius = 0.5f;
    headerView.layer.shadowOpacity = 2.0f;

    // User label
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont fontWithName:AVENIR_FONT_HEAVY size:12];
    titleLabel.numberOfLines = 0;
    titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    [titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [headerView addSubview:titleLabel];
    [headerView addTarget:self action:@selector(headerTapped:) forControlEvents:UIControlEventTouchUpInside];

    // Arrow Image
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.translatesAutoresizingMaskIntoConstraints = NO;
    [imageView setBackgroundColor:[UIColor clearColor]];
    imageView.tintColor = [UIColor blackColor];
    imageView.alpha = 0.8;
    [headerView addSubview:imageView];
    
    // Constraints
    NSDictionary *views = @{@"titleLabel" : titleLabel, @"imageView" : imageView};
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[titleLabel]-[imageView(12)]-10-|" options: 0 metrics:nil views:views]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[titleLabel]|" options: 0 metrics:nil views:views]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[imageView(12)]" options: 0 metrics:nil views:views]];
    
    NSString *titleString = @"";
    switch (section) {
        case 1: titleString = [App_Delegate.keyMapping getKeyValue:@"KEY_WEEKLY_GOAL"]; break;
        case 2: titleString = [App_Delegate.keyMapping getKeyValue:@"KEY_GOAL_DETAILS"]; break;
        case 3: titleString = [App_Delegate.keyMapping getKeyValue:@"KEY_REVENUE_DETAILS"]; break;
        case 4: titleString = [App_Delegate.keyMapping getKeyValue:@"KEY_PRODUCT_GOAL_STATUS"]; break;
        case 5: titleString = [App_Delegate.keyMapping getKeyValue:@"KEY_EFFORT_DETAILS"]; break;
        case 6: titleString = [App_Delegate.keyMapping getKeyValue:@"KEY_EFFORT_DETAILS_VALUE4"]; break;
        default: break;
    }
    titleLabel.text = titleString;
    
    NSNumber *isExpand = [self.arrSelectedSectionIndex objectAtIndex:section];
    imageView.image = (isExpand.boolValue) ? [UIImage imageNamed:DOWN_ARROW] : [UIImage imageNamed:RIGHT_ARROW];
    return headerView;
}

//---------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section != 0) {
        /*************** Close the section, once the data is selected ***********************************/
        NSNumber *isExpand = [self.arrSelectedSectionIndex objectAtIndex:indexPath.section];
        [self.arrSelectedSectionIndex replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:!isExpand.boolValue]];
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark GoalFilter delegate methods

//---------------------------------------------------------------

//- (void) reloadGoalDetails {
//    // Save filters
////    [ServicePreference saveCustomObject:self.filter key:KEY_GOAL_FILTERS];
//
//    if (self.filter.userId) {
//        [self updateTopLabelWidth];
//    }
//    [self callWebServiceToGetGoalProgress];
//    
//    //Analytics
//    [AnalyticsLogger logGoalProgressFilterChangeEvent:self];
//}

//---------------------------------------------------------------

- (void) reloadItemsBasedOnFilters:(Filters *)filter withFilterItems:(NSArray *)filterItems {
    self.filter = filter;
//    if (self.filter.userId) {
//        [self updateTopLabelWidth];
//    }
    [self updateTopLabelWidth];

    [self callWebServiceToGetGoalProgress];
    
    //Analytics
    [AnalyticsLogger logGoalProgressFilterChangeEvent:self];
}

//---------------------------------------------------------------

//- (void) updateFilterArray:(NSArray *)updatedArray forFilterType:(FilterType)filterType withFilter:(Filters *)filter {
//    // Update filters and array
//    self.filter = filter;
//    switch (filterType) {
//        case LocationFilter: self.locationList.locationList = updatedArray; break;
//        case LocationGroupFilter: self.locationGroupList.locationGroupList = updatedArray; break;
//        case ProductFilter: self.productList.productList = updatedArray; break;
//        case UserFilter: self.userList.userList = updatedArray; break;
//        default: break;
//    }
//}

//---------------------------------------------------------------

- (void) updateFilterArray:(NSArray *)updatedArray forFilterType:(FilterType)filterType {
    switch (filterType) {
        case LocationFilter: self.locationList.locationList = updatedArray; break;
        case LocationGroupFilter: self.locationGroupList.locationGroupList = updatedArray; break;
        case ProductFilter: self.productList.productList = updatedArray; break;
        case UserFilter: self.userList.userList = updatedArray; break;
        default: break;
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    
    [self setTopNavBarTitle];
    
    // Get filters from stored
//    Filters *storedFilters = [ServicePreference customObjectWithKey:KEY_GOAL_FILTERS];
//    self.filter = (storedFilters) ? storedFilters : [Filters new];

    self.filter = [Filters new];
    _noRecordsView = [Helper createNoAccessView:self.navigationController.view withMessage:@"No goal set for this user!"];

    [Helper addBottomLine:self.filterView withColor:TABLE_SEPERATOR_COLOR];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    // 2 - CarRental
    if (App_Delegate.appfilter.industryId.integerValue == 2) {
        self.chartsArray = @[
                             [App_Delegate.keyMapping getKeyValue:@"KEY_IN_GOAL"],
                             [App_Delegate.keyMapping getKeyValue:@"KEY_MY_IN"],
                             [App_Delegate.keyMapping getKeyValue:@"KEY_TITLE_ARPD"],
                             [App_Delegate.keyMapping getKeyValue:@"KEY_GOAL_ACHIEVED"],
                             [App_Delegate.keyMapping getKeyValue:@"KEY_MY_GOAL_ACHIEVED"],
                             [App_Delegate.keyMapping getKeyValue:@"KEY_GOAL_DETAILS_VALUE4"],
                             ];
    } else {
        self.chartsArray = @[
                             [App_Delegate.keyMapping getKeyValue:@"KEY_IN_GOAL"],
                             [App_Delegate.keyMapping getKeyValue:@"KEY_TITLE_ARPD"],
                             [App_Delegate.keyMapping getKeyValue:@"KEY_GOAL_ACHIEVED"],
                             [App_Delegate.keyMapping getKeyValue:@"KEY_GOAL_DETAILS_VALUE4"],
                             ];
    }
    
    _arrSelectedSectionIndex = [[NSMutableArray alloc] init];
    for (int index = 0; index < TOTAL_SECTION; index++) {
        [self.arrSelectedSectionIndex addObject:[NSNumber numberWithBool:NO]];
    }

    // Check if default is set or not?
    // If default is already there then do not call filter services
    // Call webservice to get filter
    if (self.filter.month && self.filter.year) {
        // Call goal progress service
        [self callWebServiceToGetGoalProgress];
    } else {
        // Get month and year
        NSDate *currentDate = [NSDate date];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:currentDate];
        
        self.filter.month = [NSNumber numberWithInteger:components.month];
        self.filter.year = [NSNumber numberWithInteger:components.year];
    }
    
    self.monthLabel.text = [NSString stringWithFormat:@"%@ :%@",[App_Delegate.keyMapping getKeyValue:@"KEY_TITLE_MONTH"] ,[Helper getMonthNameFromMonthNumber:self.filter.month.integerValue]];
    self.yearLabel.text = [NSString stringWithFormat:@"%@ :%@",[App_Delegate.keyMapping getKeyValue:@"KEY_TITLE_YEAR"] ,self.filter.year];
    
    // Background service call
    [self callFilterWebServicesInBackground];
}

//---------------------------------------------------------------
// In a Storyboard-based application, you will often want to do a little preparation before navigation
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    if ([[segue identifier] isEqualToString:@"showFilterViewFromGoal"]) {
//        GoalFilterViewController *controller = (GoalFilterViewController *)[segue destinationViewController];
////        // Get filters from stored
////        Filters *filter = [ServicePreference customObjectWithKey:KEY_GOAL_FILTERS];
////        if (filter != nil) {
////            self.filter = filter;
////        }
//        controller.filter = self.filter;
//        controller.delegate = self;
//        controller.locationList = self.locationList;
//        controller.locationGroupList = self.locationGroupList;
//        controller.productList = self.productList;
//        controller.userList = self.userList;
//    }
}

//---------------------------------------------------------------

@end
