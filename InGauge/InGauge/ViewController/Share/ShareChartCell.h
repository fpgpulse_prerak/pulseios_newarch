//
//  ShareChartCell.h
//  IN-Gauge
//
//  Created by Mehul Patel on 01/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareChartCell : UITableViewCell

@property (nonatomic, strong) UIView            *cardView;
@property (nonatomic, strong) UIImageView       *chartImage;

@end
