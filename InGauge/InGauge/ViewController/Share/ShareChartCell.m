//
//  ShareChartCell.m
//  IN-Gauge
//
//  Created by Mehul Patel on 01/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "ShareChartCell.h"

@implementation ShareChartCell

//---------------------------------------------------------------

#pragma mark
#pragma mark Init methods

//---------------------------------------------------------------

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    self.backgroundColor = [UIColor whiteColor];
    
    _cardView = [[UIView alloc] init];
    self.cardView.translatesAutoresizingMaskIntoConstraints = NO;
    self.cardView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.cardView];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cardView]|" options: 0 metrics:nil views:@{@"cardView" : self.cardView}]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cardView]|" options: 0 metrics:nil views:@{@"cardView" : self.cardView}]];
    
    _chartImage = [[UIImageView alloc] init];
    self.chartImage.translatesAutoresizingMaskIntoConstraints = NO;
    [self.cardView addSubview:self.chartImage];
    
    NSDictionary *views = @{@"chartImage" : self.chartImage};
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[chartImage]|" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[chartImage]|" options: 0 metrics:nil views:views]];
    return self;
}

//---------------------------------------------------------------

- (void)layoutSubviews {
    [super layoutSubviews];
}

//---------------------------------------------------------------

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

//---------------------------------------------------------------

@end
