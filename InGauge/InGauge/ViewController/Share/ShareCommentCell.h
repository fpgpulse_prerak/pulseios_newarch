//
//  ShareCommentCell.h
//  IN-Gauge
//
//  Created by Mehul Patel on 01/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UITextView+Placeholder.h>
#import "Helper.h"

@interface ShareCommentCell : UITableViewCell

@property (nonatomic, strong) UIView                *cardView;
@property (nonatomic, strong) UILabel               *titleLabel;
@property (nonatomic, strong) UITextView            *textView;
@property (nonatomic, strong) UIView                *bottomSpace;

@end
