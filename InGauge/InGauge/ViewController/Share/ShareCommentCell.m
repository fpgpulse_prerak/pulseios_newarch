//
//  ShareCommentCell.m
//  IN-Gauge
//
//  Created by Mehul Patel on 01/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "ShareCommentCell.h"

@implementation ShareCommentCell

//---------------------------------------------------------------

#pragma mark
#pragma mark Init methods

//---------------------------------------------------------------

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    self.backgroundColor = [UIColor clearColor];
    
    _cardView = [[UIView alloc] init];
    self.cardView.translatesAutoresizingMaskIntoConstraints = NO;
    self.cardView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.cardView];
    
    _bottomSpace = [[UIView alloc] init];
    self.bottomSpace.translatesAutoresizingMaskIntoConstraints = NO;
    self.bottomSpace.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.bottomSpace];
    
    _titleLabel = [[UILabel alloc] init];
    self.titleLabel.textColor = [UIColor blackColor];
    self.titleLabel.font = [UIFont fontWithName:AVENIR_FONT size:15];
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.contentView addSubview:self.titleLabel];
 
    self.titleLabel.text = @"Your comment";
    
    _textView = [[UITextView alloc] init];
    self.textView.translatesAutoresizingMaskIntoConstraints = NO;
    self.textView.placeholder = @"What is the report about?";
    self.textView.font = [UIFont fontWithName:AVENIR_FONT size:14];
    self.textView.textColor = [UIColor blackColor];
    [self.cardView addSubview:self.textView];
    
    NSDictionary *views = @{@"titleLabel" : self.titleLabel, @"textView" : self.textView, @"cardView" : self.cardView, @"bottomSpace" : self.bottomSpace};
    
    // CardView, Label and BottomViews are in contentView
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cardView]|" options: 0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[bottomSpace]|" options: 0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[titleLabel]-15-|" options: 0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[titleLabel]-5-[cardView(60)]-5-[bottomSpace(40)]-10-|" options: 0 metrics:nil views:views]];

    // TextView will be added to cardView
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-11-[textView]-15-|" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[textView(60)]|" options: 0 metrics:nil views:views]];
    return self;
}

//---------------------------------------------------------------

@end
