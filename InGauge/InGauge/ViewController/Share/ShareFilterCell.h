//
//  ShareFilterCell.h
//  IN-Gauge
//
//  Created by Mehul Patel on 02/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"

@interface ShareFilterCell : UITableViewCell

@property (nonatomic, strong) UILabel               *itemNameLabel;
@property (nonatomic, strong) UILabel               *countLabel;

@end
