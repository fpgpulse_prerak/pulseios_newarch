//
//  ShareFilterCell.m
//  IN-Gauge
//
//  Created by Mehul Patel on 02/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "ShareFilterCell.h"

@implementation ShareFilterCell

//---------------------------------------------------------------

#pragma mark
#pragma mark Init methods

//---------------------------------------------------------------

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    //self.backgroundColor = [UIColor whiteColor];
    
    _itemNameLabel = [[UILabel alloc] init];
    self.itemNameLabel.textColor = [UIColor blackColor];
    self.itemNameLabel.font = [UIFont fontWithName:AVENIR_FONT size:14];
    self.itemNameLabel.numberOfLines = 0;
    self.itemNameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.itemNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.itemNameLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.contentView addSubview:self.itemNameLabel];
    
    _countLabel = [[UILabel alloc] init];
    self.countLabel.textColor = MATERIAL_PINK_COLOR;
    self.countLabel.font = [UIFont fontWithName:AVENIR_NEXT_DEMI_BOLD_FONT size:11];
    self.countLabel.numberOfLines = 0;
    self.countLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.countLabel.textAlignment = NSTextAlignmentLeft;
    [self.countLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.contentView addSubview:self.countLabel];
    
    NSDictionary *views = @{@"itemNameLabel" : self.itemNameLabel, @"countLabel" : self.countLabel};
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[itemNameLabel]-2-[countLabel]-5-|" options: 0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[itemNameLabel]|" options: 0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[itemNameLabel]|" options: 0 metrics:nil views:views]];
    
//    [self.itemNameLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];
    [self.countLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    [self.countLabel setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.countLabel
                                                              attribute:NSLayoutAttributeCenterY
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.contentView
                                                              attribute:NSLayoutAttributeCenterY
                                                             multiplier:1
                                                               constant:0]];
    return self;
}

//---------------------------------------------------------------

- (void)layoutSubviews {
    [super layoutSubviews];
}

//---------------------------------------------------------------

@end
