//
//  ShareFilterDetailView.h
//  IN-Gauge
//
//  Created by Mehul Patel on 02/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "ServiceManager.h"

@protocol ShareFilterDetailDelegate <NSObject>

@optional
- (void) updateItem:(RefineItem *)refineItem withSubItem:(NSArray *)subItems;

@end

@interface ShareFilterDetailView : UIView

@property (nonatomic, weak) id <ShareFilterDetailDelegate>       delegate;
@property (nonatomic, strong) NSArray                           *multiSelectionArray;

- (void) reloadFilterData:(RefineItem *)item;
- (void) closeAllOperations;

@end
