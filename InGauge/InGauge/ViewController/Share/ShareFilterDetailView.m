//
//  ShareFilterDetailView.m
//  IN-Gauge
//
//  Created by Mehul Patel on 02/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "ShareFilterDetailView.h"
#import "FilterItemCell.h"

#define HEADER_HEIGHT 30

@interface ShareFilterDetailView() <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

@property (nonatomic, weak) IBOutlet UITableView        *tableView;
@property (nonatomic, weak) IBOutlet UISearchBar        *searchBar;

@property (nonatomic, strong) UIView                    *noRecordsView;
@property (nonatomic, strong) RefineItem                *filterItem;
@property (nonatomic, strong) NSArray                   *dataArray;
@property (nonatomic, strong) NSArray                   *searchArray;

@property (nonatomic, strong) NSIndexPath               *prevIndexPath;

@end

@implementation ShareFilterDetailView

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc {
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
    self.searchBar.delegate = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WEBSERVICE methods

//---------------------------------------------------------------

- (void) getLocationGroups:(void(^)(BOOL isFinish))block {
    MBProgressHUD *hud = [Helper showLoading:self];
    [ServiceManager callWebServiceToGetLocationGroup:self.filterItem.itemId usingBlock:^(LocationGroupList *locationGroupList) {
        [Helper hideLoading:hud];
        
        @try {
            // Get location group
            if (locationGroupList.locationGroupList.count > 0) {
                self.filterItem.subItems = locationGroupList.locationGroupList;
                NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.filterItem.selectedItems];
                for (LocationGroup *group in locationGroupList.locationGroupList) {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.locationGroupID.intValue = %d", group.locationGroupID.intValue];
                    NSArray *filterGroups = [self.filterItem.selectedItems filteredArrayUsingPredicate:predicate];
                    if (filterGroups.count > 0) {
                        LocationGroup *filterGroup = filterGroups[0];
                        NSInteger index = [tempArray indexOfObject:filterGroup];
                        [tempArray replaceObjectAtIndex:index withObject:group];
                    }
                }
                
                self.filterItem.selectedItems = [NSArray arrayWithArray:tempArray];
                tempArray = nil;
                
                [self updateUI:self.filterItem];
                [self.delegate updateItem:self.filterItem withSubItem:self.multiSelectionArray];
                block(YES);
            }
        } @catch (NSException *exception) {
            DLog(@"Exception :%@", exception.debugDescription);
            block(YES);
        }
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) setupViews {
    // Table settings
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 50;
    
    // Search Bar UI
    self.searchBar.delegate = self;
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setFont:[UIFont fontWithName:AVENIR_FONT size:12]];
    [self.searchBar setReturnKeyType:UIReturnKeyDone];
    //    [self.searchBar setKeyboardAppearance:UIKeyboardAppearanceDark];
    
    _noRecordsView = [Helper createNoAccessView:self withMessage:[App_Delegate.keyMapping getKeyValue:@"KEY_NO_DATA"]];
    _multiSelectionArray = [NSMutableArray new];
}

//---------------------------------------------------------------

- (void) reloadFilterData:(RefineItem *)item {

    // Update inner controls
    self.searchBar.text = @"";
    [self.searchBar resignFirstResponder];
    
    self.filterItem = item;
    
    // call service only when there are not subitems
    if (!item.subItems) {
        [self getLocationGroups:^(BOOL isFinish) {
        }];
    } else {
        [self updateUI:item];
    }
}

//---------------------------------------------------------------

- (void) updateUI:(RefineItem *)item {
    
    self.dataArray = item.subItems;
    self.multiSelectionArray = item.selectedItems;
    [self.tableView reloadData];

    if (self.dataArray.count > 0) {
        // Scroll table to first position
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            [self.tableView scrollToRowAtIndexPath:indexPath
                                  atScrollPosition:UITableViewScrollPositionTop
                                          animated:YES];
        [self.noRecordsView setHidden:YES];
        [self.tableView setHidden:NO];
    } else {
        [self.noRecordsView setHidden:NO];
        [self.tableView setHidden:YES];
    }
}

//---------------------------------------------------------------

- (BOOL) isSelected:(LocationGroup *)locationGroup {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.locationGroupID.intValue = %d", locationGroup.locationGroupID.intValue];
    NSArray *tags = [self.multiSelectionArray filteredArrayUsingPredicate:predicate];
    if (tags.count > 0) {
        return YES;
    } else
        return NO;
}

//---------------------------------------------------------------

- (void) closeAllOperations {
    self.searchBar.text = @"";
    [self.searchBar resignFirstResponder];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *searchText = [Helper removeWhiteSpaceAndNewLineCharacter:self.searchBar.text];
    if (searchText.length > 0) {
        return self.searchArray.count;
    } else {
        return self.dataArray.count;
    }
}

//---------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return HEADER_HEIGHT;
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"FilterItemCell%d%d", (int)indexPath.section, (int)indexPath.row];
    FilterItemCell *cell = (FilterItemCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[FilterItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.arrowImage.hidden = YES;
    cell.selectedItemLabel.text = @"";
    cell.selectedItemLabel.hidden = YES;

    NSArray *dataSource = self.dataArray;
    NSString *searchText = [Helper removeWhiteSpaceAndNewLineCharacter:self.searchBar.text];
    if (searchText.length > 0) {
        dataSource = self.searchArray;
    }
    
    LocationGroup *group = [dataSource objectAtIndex:indexPath.row];
    cell.itemLabel.text = [Helper removeWhiteSpaceAndNewLineCharacter:group.locationGroupName];
    
    // Show selected object
    if ([self isSelected:group]) {
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tick"]];
    } else {
        cell.accessoryView = nil;
    }
    return cell;
}

//---------------------------------------------------------------

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIControl alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, HEADER_HEIGHT)];
    headerView.tag = section;
    headerView.backgroundColor = BACK_COLOR;
    
    // User label
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont fontWithName:AVENIR_NEXT_DEMI_BOLD_FONT size:15];
    titleLabel.numberOfLines = 0;
    titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.textColor = [UIColor blackColor];
    [titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [headerView addSubview:titleLabel];
    
    titleLabel.text = @"Location Groups";
    
    NSDictionary *views = @{@"titleLabel" : titleLabel};
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[titleLabel]-10-|" options: 0 metrics:nil views:views]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[titleLabel]|" options: 0 metrics:nil views:views]];
    
    [Helper addBottomLine:headerView withColor:TABLE_SEPERATOR_COLOR];
    return headerView;
}

//---------------------------------------------------------------

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    NSArray *dataSource = self.dataArray;
    NSString *searchText = [Helper removeWhiteSpaceAndNewLineCharacter:self.searchBar.text];
    if (searchText.length > 0) {
        dataSource = self.searchArray;
    }
    
    @try {
        LocationGroup *object = (LocationGroup *)[dataSource objectAtIndex:indexPath.row];
        NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.multiSelectionArray];
        // Update selection
        if (cell.accessoryView != nil) {
            cell.accessoryView = nil;
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.locationGroupID.intValue = %d", object.locationGroupID.intValue];
            NSArray *filterArray = [tempArray filteredArrayUsingPredicate:predicate];
            if (filterArray.count > 0) {
                LocationGroup *removeGroup = filterArray[0];
                [tempArray removeObject:removeGroup];
            }
        } else {
            cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tick"]];
            object.locationName = self.filterItem.title;
            [tempArray addObject:object];
        }
        
        self.multiSelectionArray = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
        // Update top label
//        [self updateTopLabel];

        // Update parent item
        [self.delegate updateItem:self.filterItem withSubItem:self.multiSelectionArray];

    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }

}

//---------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    FilterItemCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryView.hidden = YES;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UISearch delegate methods

//---------------------------------------------------------------

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    return YES;
}

//---------------------------------------------------------------

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return YES;
}

//---------------------------------------------------------------

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    @try {
        if (isEmptyString(searchText)) {
            self.searchArray = self.dataArray;
        } else {
            searchText = [[searchText trim] lowercaseString];
            NSMutableArray *tempArray = [NSMutableArray array];
            for (id option in self.dataArray) {
                
                NSString *optionText = @"";
                switch (self.filterItem.filterType) {
                    
                    case LocationFilter: {
                        LocationGroup *group = (LocationGroup *)option;
                        optionText = group.locationGroupName;
                        break;
                    }
                    default: {
                        optionText = option;
                        break;
                    }
                }
                
                if ([[optionText lowercaseString] myContainsString:searchText]) {
                    [tempArray addObject:option];
                }
            }
            self.searchArray = [NSArray arrayWithArray:tempArray];
            tempArray = nil;
        }
        [self.tableView reloadData];
    }
    @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

//---------------------------------------------------------------

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    return YES;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Init methods

//---------------------------------------------------------------

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setupViews];
    }
    return self;
}

//---------------------------------------------------------------

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupViews];
    }
    return self;
}

//---------------------------------------------------------------

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViews];
    }
    return self;
}

//---------------------------------------------------------------

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupViews];
}

//---------------------------------------------------------------

@end
