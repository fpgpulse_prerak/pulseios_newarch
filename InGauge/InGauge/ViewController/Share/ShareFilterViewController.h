//
//  ShareFilterViewController.h
//  IN-Gauge
//
//  Created by Mehul Patel on 02/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ServiceManager.h"

@protocol ShareFilterViewDelegate <NSObject>

- (void) selectedLocationGroups:(NSArray *)items;

@end

@interface ShareFilterViewController : BaseViewController

@property (nonatomic, weak) id <ShareFilterViewDelegate>    delegate;
@property (nonatomic, strong) NSArray                       *locationArray;
@property (nonatomic, strong) NSArray                       *filterItems; //RefineItem

@end
