//
//  ShareFilterViewController.m
//  IN-Gauge
//
//  Created by Mehul Patel on 02/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "ShareFilterViewController.h"
#import "ShareFilterDetailView.h"
#import "ShareFilterCell.h"

#define HEADER_HEIGHT 30

@interface ShareFilterViewController () <UITableViewDataSource, UITableViewDelegate, ShareFilterDetailDelegate>

@property (nonatomic, weak) IBOutlet UITableView            *tableView;
@property (nonatomic, weak) IBOutlet ShareFilterDetailView  *filterDetailView;

@property (nonatomic, strong) UILabel                       *navigationLabel;
@property (nonatomic, strong) UIView                        *noRecordsView;
@property (nonatomic, strong) NSIndexPath                   *prevIndexPath;
@property (nonatomic, strong) NSArray                       *selectedGroups;

@end

@implementation ShareFilterViewController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) selectAllButtonTapped:(id)sender {
    // Select all items
    if (self.prevIndexPath) {
        RefineItem *item = [self.filterItems objectAtIndex:self.prevIndexPath.row];
        if (item) {
            [self updateItem:item withSubItem:item.subItems];
            [self.filterDetailView reloadFilterData:item];
        }
    }
    [self updateTopLabel];
    [self.tableView reloadData];
}

//---------------------------------------------------------------

- (IBAction) clearAllButtonTapped:(id)sender {
    // Deselect all items
    if (self.prevIndexPath) {
        RefineItem *item = [self.filterItems objectAtIndex:self.prevIndexPath.row];
        if (item) {
            [self updateItem:item withSubItem:[NSArray new]];
            [self.filterDetailView reloadFilterData:item];
        }
    }
    [self updateTopLabel];
    [self.tableView reloadData];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) setTopNavBarTitle {
    //    CGFloat width = self.view.frame.size.width - 100;
    _navigationLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.navigationItem.titleView.frame.size.width, 20)];
    self.navigationLabel.textAlignment = NSTextAlignmentCenter;
    self.navigationLabel.font = [UIFont fontWithName:AVENIR_NEXT_FONT size:14];
    self.navigationLabel.textColor = [UIColor whiteColor];
    self.navigationLabel.adjustsFontSizeToFitWidth = YES;
    self.navigationItem.titleView = self.navigationLabel;
    [self updateTopLabel];
}

//---------------------------------------------------------------

- (void) updateTopLabel {    
    NSInteger selectedCount = 0;
    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.selectedGroups];
    if (self.filterItems) {
        for (RefineItem *subItem in self.filterItems) {
            selectedCount += subItem.selectedItems.count;
            [tempArray addObjectsFromArray:subItem.selectedItems];
        }
    }
    self.navigationLabel.text = [NSString stringWithFormat:@"%d Selected", (int)selectedCount];

    // update selected groups
    self.selectedGroups = [NSArray arrayWithArray:tempArray];
    tempArray = nil;
}

//---------------------------------------------------------------

- (void) selectFirstCell {
    // Select first row
    @try {
        self.prevIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:self.prevIndexPath];
        cell.textLabel.textColor = MATERIAL_PINK_COLOR;
        cell.detailTextLabel.textColor = [UIColor blackColor];
        cell.backgroundColor = [UIColor whiteColor];
        
        RefineItem *item = [self.filterItems objectAtIndex:self.prevIndexPath.row];
        [self.filterDetailView reloadFilterData:item];
        
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void) updateItem:(id)object {
    // Update
    NSInteger index = [self.filterItems indexOfObject:object];
    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.filterItems];
    [tempArray replaceObjectAtIndex:index withObject:object];
    self.filterItems = [NSArray arrayWithArray:tempArray];
    tempArray = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.filterItems.count;
}

//---------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return HEADER_HEIGHT;
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"ShareFilterCell%d%d", (int)indexPath.section, (int)indexPath.row];
    ShareFilterCell *cell = (ShareFilterCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[ShareFilterCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //colors
    cell.backgroundColor = [UIColor clearColor];
    cell.itemNameLabel.textColor = [UIColor blackColor];
    
    RefineItem *item = [self.filterItems objectAtIndex:indexPath.row];
    cell.itemNameLabel.text = item.title;
    cell.countLabel.text = (item.selectedItems.count > 0) ? [NSString stringWithFormat:@"%d", (int)item.selectedItems.count] : @"";
    
    if (self.prevIndexPath == indexPath) {
        cell.backgroundColor = [UIColor whiteColor];
        cell.itemNameLabel.textColor = MATERIAL_PINK_COLOR;
//        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    
    return cell;
}

//---------------------------------------------------------------

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
     UIView *headerView = [[UIControl alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, HEADER_HEIGHT)];
     headerView.tag = section;
     headerView.backgroundColor = [UIColor whiteColor];
     
     // User label
     UILabel *titleLabel = [[UILabel alloc] init];
     titleLabel.font = [UIFont fontWithName:AVENIR_NEXT_DEMI_BOLD_FONT size:15];
     titleLabel.numberOfLines = 0;
     titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
     titleLabel.textAlignment = NSTextAlignmentLeft;
     titleLabel.textColor = [UIColor blackColor];
     [titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
     [headerView addSubview:titleLabel];
    
     titleLabel.text = @"Locations";
     
     NSDictionary *views = @{@"titleLabel" : titleLabel};
     [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[titleLabel]-10-|" options: 0 metrics:nil views:views]];
     [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[titleLabel]|" options: 0 metrics:nil views:views]];
    
    [Helper addBottomLine:headerView withColor:TABLE_SEPERATOR_COLOR];
     return headerView;
}

//---------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Deselect previous row
    if (self.prevIndexPath) {
        [self tableView:self.tableView didDeselectRowAtIndexPath:self.prevIndexPath];
    }
    self.prevIndexPath = indexPath;
    
    ShareFilterCell *cell = (ShareFilterCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.itemNameLabel.textColor = MATERIAL_PINK_COLOR;
//    cell.detailTextLabel.textColor = [UIColor blackColor];
    cell.backgroundColor = [UIColor whiteColor];
    RefineItem *item = [self.filterItems objectAtIndex:indexPath.row];
    [self.filterDetailView reloadFilterData:item];
}

//---------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    ShareFilterCell *cell = (ShareFilterCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.itemNameLabel.textColor = [UIColor blackColor];
//    cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    cell.backgroundColor = [UIColor clearColor];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark ShareFilterDetailDelegate methods

//---------------------------------------------------------------

- (void) updateItem:(RefineItem *)refineItem withSubItem:(NSArray *)selectedItems {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.itemId.intValue = %d", refineItem.itemId.intValue];
    NSArray *items = [self.filterItems filteredArrayUsingPredicate:predicate];
    if (items.count > 0) {
        NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.filterItems];
        RefineItem *item = (RefineItem *)[items objectAtIndex:0];
        item.selectedItems = selectedItems;
        
        NSInteger index = [self.filterItems indexOfObject:item];
        [tempArray replaceObjectAtIndex:index withObject:item];
        self.filterItems = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
        
        [self updateTopLabel];
        [self.tableView reloadData];
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];

    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:nil];
    self.navigationItem.backBarButtonItem = backItem;

    // TableView settings
    // Table settings
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _noRecordsView = [Helper createNoAccessView:self.view withMessage:[App_Delegate.keyMapping getKeyValue:@"KEY_NO_DATA"]];
        
    if (self.filterItems.count > 0) {
        [self.noRecordsView setHidden:YES];
        [self.tableView setHidden:NO];
    } else {
        [self.noRecordsView setHidden:NO];
        [self.tableView setHidden:YES];
    }
    
    // Left side border
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 0.5;
    border.borderColor = [UIColor lightGrayColor].CGColor;
    border.frame = CGRectMake(0, 0, borderWidth, self.filterDetailView.frame.size.height);
    border.borderWidth = borderWidth;
    [self.filterDetailView.layer addSublayer:border];
    self.filterDetailView.layer.masksToBounds = YES;
    self.filterDetailView.delegate = self;
    
    // Setup navigation title
    [self setTopNavBarTitle];
}


//---------------------------------------------------------------

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self selectFirstCell];
}

//---------------------------------------------------------------

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.delegate selectedLocationGroups:self.filterItems];
}

//---------------------------------------------------------------

@end
