//
//  ShareMultiSelectController.h
//  IN-Gauge
//
//  Created by Mehul Patel on 02/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol ShareMultiSelectDelegate <NSObject>

- (void) selectedItems:(NSArray *)items forType:(FilterType)filterType;
- (void) updateUserArray:(NSArray *)userArray;
- (void) updateLocationArray:(NSArray *)locationArray;

@end

@interface ShareMultiSelectController : BaseViewController

@property (nonatomic, weak) id <ShareMultiSelectDelegate>   delegate;
@property (nonatomic, strong) NSArray                           *multiSelectionArray;
@property (nonatomic, strong) Filters                           *filter;
@property (nonatomic, assign) FilterType                        filterType;
@property (nonatomic, strong) NSArray                           *dataArray;

@end
