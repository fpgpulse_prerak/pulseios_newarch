//
//  ShareMultiSelectController.m
//  IN-Gauge
//
//  Created by Mehul Patel on 02/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "ShareMultiSelectController.h"
#import "FilterItemCell.h"
#import "UserMailListModel.h"

//#define HEADER_HEIGHT 30

@interface ShareMultiSelectController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (nonatomic, weak) IBOutlet UISearchBar        *searchBar;
@property (nonatomic, weak) IBOutlet UITableView        *tableView;

@property (nonatomic, strong) UIView                    *noRecordsView;
@property (nonatomic, strong) UILabel                   *navigationLabel;
@property (nonatomic, strong) NSArray                   *searchArray;

@end

@implementation ShareMultiSelectController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) selectAllButtonTapped:(id)sender {
    // Select all items
    self.multiSelectionArray = [NSArray arrayWithArray:self.dataArray];
    [self updateTopLabel];
    [self.tableView reloadData];
}

//---------------------------------------------------------------

- (IBAction) clearAllButtonTapped:(id)sender {
    // Deselect all items
    self.multiSelectionArray = [NSMutableArray new];
    [self updateTopLabel];
    [self.tableView reloadData];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WEBSERVICE methods

//---------------------------------------------------------------

- (void) callWebServiceToGetUserList {
    // Check for internet
    if (![Helper checkForConnection:self]) {
        return;
    }
    
    MBProgressHUD *hud = [Helper showLoading:self.view];
    [ServiceManager getAllUsersOfTenant:^(UserList *userList) {
        [Helper hideLoading:hud];

        self.dataArray = userList.userList;
        [self.delegate updateUserArray:self.dataArray];
        [self.tableView reloadData];
        [self showHideTable];
    }];
}

//---------------------------------------------------------------

- (void) callWebServiceToGetLocations {
    MBProgressHUD *hud = [Helper showLoading:self.view];
    NSDictionary *details = @{
                              @"orderBy" : @"name",
                              @"sort" : @"ASC",
                              @"activeStatus" : @"Normal"
                              };
    [ServiceManager callWebServiceToGetLocations:details usingBlock:^(LocationList *locationList) {
        [Helper hideLoading:hud];
        self.dataArray = locationList.locationList;
        [self.delegate updateLocationArray:self.dataArray];
        [self.tableView reloadData];
        [self showHideTable];
    }];
}


//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) showHideTable {
    if (self.dataArray.count > 0) {
        [self.noRecordsView setHidden:YES];
        [self.tableView setHidden:NO];
    } else {
        [self.noRecordsView setHidden:NO];
        [self.tableView setHidden:YES];
    }
}

//---------------------------------------------------------------

- (BOOL) isSelected:(id)object {
    NSPredicate *predicate = [NSPredicate new];
    switch (self.filterType) {
        case LocationFilter: {
            Location *location = (Location *)object;
            predicate = [NSPredicate predicateWithFormat:@"SELF.locationId.intValue = %d", location.locationId.intValue];
            break;
        }
           
        case UserFilter: {
            User *user = (User *)object;
            predicate = [NSPredicate predicateWithFormat:@"SELF.userID.intValue = %d", user.userID.intValue];
            break;
        }
        default: return NO; break;
    }
    
    NSArray *tags = [self.multiSelectionArray filteredArrayUsingPredicate:predicate];
    return (tags.count > 0) ? YES : NO;
}

//---------------------------------------------------------------

- (void) setTopNavBarTitle {
//    CGFloat width = self.view.frame.size.width - 100;
    _navigationLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.navigationItem.titleView.frame.size.width, 20)];
    self.navigationLabel.textAlignment = NSTextAlignmentCenter;
    self.navigationLabel.font = [UIFont fontWithName:AVENIR_NEXT_FONT size:14];
    self.navigationLabel.textColor = [UIColor whiteColor];
    self.navigationLabel.adjustsFontSizeToFitWidth = YES;
    self.navigationItem.titleView = self.navigationLabel;
    [self updateTopLabel];
}

//---------------------------------------------------------------

- (void) updateTopLabel {    
//    self.navigationLabel.text = [NSString stringWithFormat:@"%d selected out of %d", (int)self.multiSelectionArray.count, (int)(self.dataArray.count)];
    self.navigationLabel.text = [NSString stringWithFormat:@"%d Selected", (int)self.multiSelectionArray.count];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *searchText = [Helper removeWhiteSpaceAndNewLineCharacter:self.searchBar.text];
    if (searchText.length > 0) {
        return self.searchArray.count;
    } else {
        return self.dataArray.count;
    }
}

//---------------------------------------------------------------

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"FilterItemCell%d%d", (int)indexPath.section, (int)indexPath.row];
    
    FilterItemCell *cell = (FilterItemCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[FilterItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }

    NSArray *dataSource = self.dataArray;
    NSString *searchText = [Helper removeWhiteSpaceAndNewLineCharacter:self.searchBar.text];
    if (searchText.length > 0) {
        dataSource = self.searchArray;
    }
    
    NSString *name = @"";
    switch (self.filterType) {
        case LocationFilter: {
            Location *location = (Location *)[dataSource objectAtIndex:indexPath.row];
            name = location.name;
            break;
        }
        case UserFilter: {
            User *user = (User *)[dataSource objectAtIndex:indexPath.row];
            name = user.name;
            break;
        }
        default:
            break;
    }
    cell.itemLabel.text = name;
    cell.arrowImage.hidden = YES;
    cell.selectedItemLabel.text = @"";
    cell.selectedItemLabel.hidden = YES;
    
    if ([self isSelected:[dataSource objectAtIndex:indexPath.row]]) {
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tick"]];
    } else {
        cell.accessoryView = nil;
    }
    return cell;
}

//---------------------------------------------------------------

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    NSArray *dataSource = self.dataArray;
    NSString *searchText = [Helper removeWhiteSpaceAndNewLineCharacter:self.searchBar.text];
    if (searchText.length > 0) {
        dataSource = self.searchArray;
    }
    
    @try {
        Location *object = (Location *)[dataSource objectAtIndex:indexPath.row];
        NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.multiSelectionArray];
        
        NSPredicate *predicate = [NSPredicate new];
        switch (self.filterType) {
            case LocationFilter: {
                Location *location = (Location *)object;
                predicate = [NSPredicate predicateWithFormat:@"SELF.locationId.intValue = %d", location.locationId.intValue];
                break;
            }
                
            case UserFilter: {
                User *user = (User *)object;
                predicate = [NSPredicate predicateWithFormat:@"SELF.userID.intValue = %d", user.userID.intValue];
                break;
            }
            default:; break;
        }
        
        // Update selection
        if (cell.accessoryView != nil) {
            cell.accessoryView = nil;
            NSArray *filterArray = [tempArray filteredArrayUsingPredicate:predicate];
            if (filterArray.count > 0) {
//                Location *removeLocation = filterArray[0];
                [tempArray removeObject:filterArray[0]];
            }
        } else {
            cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tick"]];
            [tempArray addObject:object];
        }
        
        self.multiSelectionArray = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
        // Update top label
        [self updateTopLabel];
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UISearch delegate methods

//---------------------------------------------------------------

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    return YES;
}

//---------------------------------------------------------------

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return YES;
}

//---------------------------------------------------------------

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    @try {
        if (isEmptyString(searchText)) {
            self.searchArray = self.dataArray;
        } else {
            
            searchText = [[searchText trim] lowercaseString];
            NSMutableArray *tempArray = [NSMutableArray array];
            for (id option in self.dataArray) {
                NSString *name = @"";
                switch (self.filterType) {
                    case LocationFilter: {
                        Location *location = (Location *)option;
                        name = location.name;
                        break;
                    }
                        
                    case UserFilter: {
                        User *user = (User *)option;
                        name = user.name;
                        break;
                    }
                    default:break;
                }
                if ([[name lowercaseString] myContainsString:searchText]) {
                    [tempArray addObject:option];
                }
            }
            self.searchArray = [NSArray arrayWithArray:tempArray];
            tempArray = nil;
        }
        [self.tableView reloadData];
    }
    @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

//---------------------------------------------------------------

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    return YES;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];

    // Search Bar UI
    self.searchBar.delegate = self;
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setFont:[UIFont fontWithName:AVENIR_FONT size:12]];
    [self.searchBar setReturnKeyType:UIReturnKeyDone];
    [self.searchBar setKeyboardAppearance:UIKeyboardAppearanceDefault];
    
    // TableView settings
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.allowsMultipleSelection = YES;
    
    _noRecordsView = [Helper createNoAccessView:self.view withMessage:[App_Delegate.keyMapping getKeyValue:@"KEY_NO_DATA"]];
    
    switch (self.filterType) {
        case UserFilter: {
            (self.dataArray.count == 0) ? [self callWebServiceToGetUserList] : [self showHideTable];
            self.navigationItem.rightBarButtonItem = nil;
            break;
        }
        case LocationFilter: {
            (self.dataArray.count == 0) ? [self callWebServiceToGetLocations] : [self showHideTable];
            break;
        }
        default: break;
    }
    
    // Setup navigation title
    [self setTopNavBarTitle];
    
}

//---------------------------------------------------------------

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

//---------------------------------------------------------------

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.delegate selectedItems:self.multiSelectionArray forType:self.filterType];
}

//---------------------------------------------------------------

@end
