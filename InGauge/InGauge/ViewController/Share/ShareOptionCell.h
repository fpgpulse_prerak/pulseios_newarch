//
//  ShareOptionCell.h
//  IN-Gauge
//
//  Created by Mehul Patel on 01/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "ShareOptionCollectionCell.h"

@interface ShareCollectionView : UICollectionView

@property (nonatomic, strong) NSIndexPath *indexPath;

@end

static NSString *ShareOptionCollectionViewCellIdentifier = @"ShareOptionCollectionViewCellIdentifier";

@interface ShareOptionCell : UITableViewCell

@property (nonatomic, strong) UILabel                   *titleLabel;
@property (nonatomic, strong) ShareCollectionView       *collectionView;

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath;

@end
