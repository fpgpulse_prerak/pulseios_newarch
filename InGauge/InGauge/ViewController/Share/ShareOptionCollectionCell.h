//
//  ShareOptionCollectionCell.h
//  IN-Gauge
//
//  Created by Mehul Patel on 01/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"

@interface ShareOptionCollectionCell : UICollectionViewCell

@property (nonatomic, strong) UILabel                   *itemLabel;
@property (nonatomic, strong) UIButton                  *itemButton;

@end
