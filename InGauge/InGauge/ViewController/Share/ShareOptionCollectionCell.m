//
//  ShareOptionCollectionCell.m
//  IN-Gauge
//
//  Created by Mehul Patel on 01/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "ShareOptionCollectionCell.h"

@implementation ShareOptionCollectionCell

//---------------------------------------------------------------

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    return self;
}

//---------------------------------------------------------------

- (id) init {
    self = [super init];
    return self;
}

//---------------------------------------------------------------

- (id) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor clearColor];
    
    _itemLabel = [[UILabel alloc] init];
    self.itemLabel.font = [UIFont fontWithName:AVENIR_FONT_HEAVY size:12];
    self.itemLabel.numberOfLines = 0;
    self.itemLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.itemLabel.textAlignment = NSTextAlignmentCenter;
    [self.itemLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.contentView addSubview:self.itemLabel];
    
    _itemButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.itemButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.itemButton.userInteractionEnabled = NO;
    [self.contentView addSubview:self.itemButton];
    
    self.itemButton.layer.cornerRadius = 25;
    self.itemButton.layer.borderWidth = 2;
    self.itemButton.backgroundColor = [UIColor whiteColor];
    self.itemLabel.text = @"Tenant";
    
    NSDictionary *views = @{@"itemLabel" : self.itemLabel, @"itemButton" : self.itemButton};
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[itemButton(50)]" options: 0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[itemLabel]" options: 0 metrics:nil views:views]];

    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[itemButton(50)]-2-[itemLabel(20)]" options: 0 metrics:nil views:views]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.itemButton
                                                                 attribute:NSLayoutAttributeCenterX
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeCenterX
                                                                multiplier:1
                                                                  constant:0]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.itemLabel
                                                                 attribute:NSLayoutAttributeCenterX
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.itemButton
                                                                 attribute:NSLayoutAttributeCenterX
                                                                multiplier:1
                                                                  constant:0]];
    return self;
}

//---------------------------------------------------------------

- (void) layoutSubviews {
    [super layoutSubviews];
    [self.contentView layoutIfNeeded];
}

//---------------------------------------------------------------

@end
