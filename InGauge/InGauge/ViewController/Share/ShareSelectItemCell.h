//
//  ShareSelectItemCell.h
//  IN-Gauge
//
//  Created by Mehul Patel on 01/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"

@interface ShareSelectItemCell : UITableViewCell

@property (nonatomic, strong) UIView                *cardView;
@property (nonatomic, strong) UILabel               *itemNameLabel;
@property (nonatomic, strong) UILabel               *selectedItemsLabel;
@property (nonatomic, strong) UILabel               *moreLabel;
@property (nonatomic, strong) UILabel               *noteLabel;
@property (nonatomic, strong) UIImageView           *arrowImage;

@property (nonatomic, assign) FilterType            filterType;

@end
