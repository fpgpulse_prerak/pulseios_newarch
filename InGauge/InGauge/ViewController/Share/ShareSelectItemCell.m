//
//  ShareSelectItemCell.m
//  IN-Gauge
//
//  Created by Mehul Patel on 01/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "ShareSelectItemCell.h"

@implementation ShareSelectItemCell

//---------------------------------------------------------------

#pragma mark
#pragma mark Init methods

//---------------------------------------------------------------

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    self.backgroundColor = [UIColor whiteColor];
    
    _cardView = [[UIView alloc] init];
    self.cardView.translatesAutoresizingMaskIntoConstraints = NO;
    self.cardView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.cardView];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cardView]|" options: 0 metrics:nil views:@{@"cardView" : self.cardView}]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cardView]|" options: 0 metrics:nil views:@{@"cardView" : self.cardView}]];
    
    _itemNameLabel = [[UILabel alloc] init];
    self.itemNameLabel.textColor = APP_COLOR;
    self.itemNameLabel.font = [UIFont fontWithName:AVENIR_NEXT_DEMI_BOLD_FONT size:15];
    self.itemNameLabel.numberOfLines = 0;
    self.itemNameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.itemNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.itemNameLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.cardView addSubview:self.itemNameLabel];
    
    _selectedItemsLabel = [[UILabel alloc] init];
    self.selectedItemsLabel.textColor = [UIColor darkGrayColor];
    self.selectedItemsLabel.font = [UIFont fontWithName:AVENIR_FONT size:12];
    self.selectedItemsLabel.numberOfLines = 1;
    self.selectedItemsLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.selectedItemsLabel.textAlignment = NSTextAlignmentLeft;
    [self.selectedItemsLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.cardView addSubview:self.selectedItemsLabel];
    
    _moreLabel = [[UILabel alloc] init];
    self.moreLabel.textColor = [UIColor darkGrayColor];
    self.moreLabel.font = [UIFont fontWithName:AVENIR_FONT size:12];
    self.moreLabel.numberOfLines = 1;
    self.moreLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.moreLabel.textAlignment = NSTextAlignmentLeft;
    [self.moreLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.cardView addSubview:self.moreLabel];
    
    _noteLabel = [[UILabel alloc] init];
    self.noteLabel.textColor = [UIColor blackColor];
    self.noteLabel.font = [UIFont fontWithName:AVENIR_FONT size:11];
    self.noteLabel.numberOfLines = 0;
    self.noteLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.noteLabel.textAlignment = NSTextAlignmentLeft;
    [self.noteLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.cardView addSubview:self.noteLabel];
    
    // arrow image
    _arrowImage = [[UIImageView alloc] init];
    self.arrowImage.alpha = 1;
    self.arrowImage.translatesAutoresizingMaskIntoConstraints = NO;
    self.arrowImage.image = [UIImage imageNamed:RIGHT_ARROW];
    [self.cardView addSubview:self.arrowImage];
    
//    self.itemNameLabel.text = @"Tenant";
//    self.selectedItemsLabel.text = @"Hilton Worldwide, Hilton Worldwide, Hilton Worldwide, Hilton";
//    self.noteLabel.text = @"Note: This feed will be shared with all users of this tenant.";
//    self.moreLabel.text = @" & 3000 more";
    
//    self.selectedItemsLabel.backgroundColor = [UIColor yellowColor];
//    self.moreLabel.backgroundColor = [UIColor redColor];
    
    
    NSDictionary *views = @{@"itemNameLabel" : self.itemNameLabel, @"selectedItemsLabel" : self.selectedItemsLabel, @"moreLabel" : self.moreLabel, @"noteLabel" : self.noteLabel, @"arrowImage" : self.arrowImage};
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[itemNameLabel]-30-|" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[selectedItemsLabel][moreLabel]-30-|" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[noteLabel]-30-|" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[itemNameLabel]-0-[selectedItemsLabel]-0-[noteLabel]-5-|" options: 0 metrics:nil views:views]];
    
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[arrowImage(12)]-10-|" options: 0 metrics:nil views:views]];
    
    [self.cardView addConstraint:[NSLayoutConstraint constraintWithItem:self.moreLabel
                                                              attribute:NSLayoutAttributeTop
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.selectedItemsLabel
                                                              attribute:NSLayoutAttributeTop
                                                             multiplier:1
                                                               constant:0]];
    
    // Below constraint will remove extra trailling padding of more label
    [self.moreLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    
    // Arrow image constraints
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[arrowImage(12)]" options:0 metrics:nil views:views]];
    
    [self.cardView addConstraint:[NSLayoutConstraint constraintWithItem:self.arrowImage
                                                              attribute:NSLayoutAttributeCenterY
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.cardView
                                                              attribute:NSLayoutAttributeCenterY
                                                             multiplier:1
                                                               constant:0]];
    return self;
}

//---------------------------------------------------------------

- (void)layoutSubviews {
    [super layoutSubviews];    
}

//---------------------------------------------------------------


@end
