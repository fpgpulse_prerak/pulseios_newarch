//
//  ShareViewController.h
//  IN-Gauge
//
//  Created by Mehul Patel on 30/10/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewController.h"

typedef enum : NSUInteger {
    ShareReportForLike = 0,
    ShareReportForComment = 1,
    ShareReportForShare = 3,
} ShareReportFor;

@interface ShareViewController : BaseTableViewController

@property (nonatomic, strong) Filters           *filter;
@property (nonatomic, strong) CustomReport      *report;
@property (nonatomic, strong) UIImage           *chartImage;
@property (nonatomic, strong) NSString          *dashboardName;
@property (nonatomic, assign) ShareReportFor    shareReportFor;

@end
