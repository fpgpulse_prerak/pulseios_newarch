//
//  ShareViewController.m
//  IN-Gauge
//
//  Created by Mehul Patel on 30/10/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "ShareViewController.h"
#import "ShareChartCell.h"
#import "ShareOptionCell.h"
#import "ShareSelectItemCell.h"
#import "ShareCommentCell.h"
#import "ShareMultiSelectController.h"
#import "ShareFilterViewController.h"
#import "AnalyticsLogger.h"

#define LOCATION_CHARS_LIMIT 55

@interface ShareViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UITextViewDelegate, ShareMultiSelectDelegate, ShareFilterViewDelegate>

@property (nonatomic, strong) UIButton                  *flotingButton;

@property (nonatomic, strong) NSMutableDictionary       *contentOffsetDictionary;
@property (nonatomic, strong) NSArray                   *userArray;
@property (nonatomic, strong) NSArray                   *locationArray;
// selected location
@property (nonatomic, strong) NSArray                   *selectedLocations;
@property (nonatomic, strong) NSString                  *selectedLocationString;
@property (nonatomic, strong) NSString                  *locationMoreString;
// selected location group
@property (nonatomic, strong) NSArray                   *selectedGroupItems;
@property (nonatomic, strong) NSArray                   *selectedGroups;
@property (nonatomic, strong) NSString                  *selectedGroupString;
@property (nonatomic, strong) NSString                  *groupMoreString;
// selected user
@property (nonatomic, strong) NSArray                   *selectedUsers;
@property (nonatomic, strong) NSString                  *selectedUserString;
@property (nonatomic, strong) NSString                  *userMoreString;
@property (nonatomic, strong) NSString                  *commentString;

@property (nonatomic, assign) NSInteger                 selectedIndex;

@end

@implementation ShareViewController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) shareButtonTapped:(id)sender {
//    [Helper tapOnControlAnimation:button forScale:0.5 forCompletionBlock:^(BOOL finished) {
//    }];
    // Validations
    // 1. Check if any item selected or not?
    
    [self.view endEditing:YES];
    NSString *message = nil;
    NSArray *selectedIds = nil;
    
    switch (self.selectedIndex) {
        // Share to tenant
        case 0: {
            selectedIds = @[App_Delegate.appfilter.tenantId];
            NSDictionary *details = @{
                                      ENTITY_TYPE : @0,
                                      ENTITY_ID : App_Delegate.appfilter.tenantId,
                                      KEY_INDUSTRY_ID : App_Delegate.appfilter.industryId,
                                      };
            selectedIds = @[details];
            break;
        }
        // Share to locations
        case 1: {
            NSMutableArray *tempArray = [NSMutableArray new];
            for (Location *location in self.selectedLocations) {
                NSDictionary *details = @{
                                          ENTITY_TYPE : @1,
                                          ENTITY_ID : location.locationId,
                                          KEY_INDUSTRY_ID : App_Delegate.appfilter.industryId,
                                          };
                [tempArray addObject:details];
            }
            selectedIds = [NSArray arrayWithArray:tempArray];
            tempArray = nil;
            message = @"You need to select atleast one location to share!";
            break;
        }
            
        // Share to location groups
        case 2: {
            NSMutableArray *tempArray = [NSMutableArray new];
            for (LocationGroup *group in self.selectedGroups) {
                NSDictionary *details = @{
                                          ENTITY_TYPE : @2,
                                          ENTITY_ID : group.locationGroupID,
                                          KEY_INDUSTRY_ID : App_Delegate.appfilter.industryId,
                                          };
                [tempArray addObject:details];
            }
            selectedIds = [NSArray arrayWithArray:tempArray];
            tempArray = nil;
            message = @"You need to select atleast one location group to share!";
            break;
        }
            
            // Share to users
        case 3: {
            NSMutableArray *tempArray = [NSMutableArray new];
            for (User *user in self.selectedUsers) {
                NSDictionary *details = @{
                                          ENTITY_TYPE : @3,
                                          ENTITY_ID : user.userID,
                                          KEY_INDUSTRY_ID : App_Delegate.appfilter.industryId,
                                          };
                [tempArray addObject:details];
                
            }
            selectedIds = [NSArray arrayWithArray:tempArray];
            tempArray = nil;
            message = @"You need to select atleast one colleague to share!";
            break;
        }
        default:
            break;
    }
    
    if (selectedIds.count == 0) {
        // show an alert
        [Helper showAlertWithTitle:@"Required!" message:message forController:self];
        return;
    }
    
    //description = comment added by user
    //feedShareWithDTOs = list of items(location, group or user) selected
    //subType = like(0), comment(1), share(3)
    NSDictionary *params = @{
                                @"description" : self.commentString ? self.commentString : @"",
                                @"feedShareWithDTOs" : selectedIds,
                                @"subType": [NSNumber numberWithInteger:self.shareReportFor].stringValue
                             };
    // Call webservice
    [self callWebServiceToShareFeed:params];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WEBSERVICE methods

//---------------------------------------------------------------

- (void) callWebServiceToGetLocations:(void(^)(BOOL finish))block {
    
    NSDictionary *details = @{
                                @"orderBy" : @"name",
                                @"sort" : @"ASC",
                                @"activeStatus" : @"Normal"
                              };
    MBProgressHUD *hud = [Helper showLoading:self.view];
    [ServiceManager callWebServiceToGetLocations:details usingBlock:^(LocationList *locationList) {
        [Helper hideLoading:hud];
        
        self.locationArray = locationList.locationList;
        [self updateLocationArray:self.locationArray];
        block(YES);
    }];
}

//---------------------------------------------------------------

- (void) callWebServiceToShareFeed:(NSDictionary *)details {

    NSMutableDictionary *filterParams = [[self.filter serializeFiltersForChartURL] mutableCopy];
    User *user = [User signedInUser];
    if (!isEmptyString(user.accessToken)) {
        NSString *authorisationBearer = [NSString stringWithFormat:@"%@", user.accessToken];
        [filterParams setObject:[NSString stringWithFormat:@"%@",authorisationBearer] forKey:@"authorizationId"];
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/shareReport/%d", DASHBOARD_CHART_URL, self.report.customReportId.intValue];
    NSString *chartUrlString = [ServiceUtil addHeaderParametersWithOutBaseURL:urlString withParams:filterParams];
    NSString* encodedUrl = [chartUrlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    // Analytics
    [AnalyticsLogger logDashboardChartShareEvent:self reportId:self.report.customReportId.stringValue comment:self.commentString];
    
    // Start progress
    MBProgressHUD *hud = [Helper showLoading:self.view];
    [ServiceManager callWebServiceToShareFeeds:encodedUrl params:details usingBlock:^(NSDictionary *response, NSError *error) {
        [Helper hideLoading:hud];
        if (response != nil) {
            if ([response[@"statusCode"] integerValue] == 200) {
                [Helper showAlertWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_DB_FEED_SHARE_TITLE"] message:response[MAIN_BODY] forController:self];
            } else {
                [Helper showAlertWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_OB_SAVE_ERROR_TITLE"] message:[App_Delegate.keyMapping getKeyValue:@"KEY_DB_FEED_ERROR"] forController:self];
            }
        } else {
            [Helper showAlertWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_OB_SAVE_ERROR_TITLE"] message:[App_Delegate.keyMapping getKeyValue:@"KEY_DB_FEED_ERROR"] forController:self];
        }
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) createFloatingShareButton {
    _flotingButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 60, self.view.frame.size.height - 120, 50, 50)];
    [self.flotingButton setImage:[UIImage imageNamed:SEND_IMAGE] forState:UIControlStateNormal];
    [self.flotingButton setBackgroundColor:APP_COLOR];
    self.flotingButton.tintColor = [UIColor whiteColor];
    [self.flotingButton setImageEdgeInsets:UIEdgeInsetsMake(15, 16, 15, 14)]; //top,left,bottom,right
    [self.flotingButton setContentMode:UIViewContentModeCenter];
    self.flotingButton.layer.cornerRadius = 25;
    self.flotingButton.hidden = NO;
    self.flotingButton.layer.masksToBounds = YES;
    [self.flotingButton addTarget:self action:@selector(shareButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.flotingButton];
    [self.view bringSubviewToFront:self.flotingButton];
}

//---------------------------------------------------------------

- (ShareSelectItemCell *) setupShareSelectItemCell:(ShareSelectItemCell *)cell indexPath:(NSIndexPath *)indexPath {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    // Set label based on share option selected
    NSString *regionType = @"";
    NSString *selectedItemString = @"";
    NSString *moreString = @"";
    NSString *noteString = @"";
    
    cell.arrowImage.hidden = NO;
    
    switch (self.selectedIndex) {
        // Tenant selected
        case 0: {
            cell.arrowImage.hidden = YES;
            regionType = @"Tenant";
            selectedItemString = App_Delegate.appfilter.tenantName;
            noteString = @"Note: This feed will be shared with all users of this tenant.";
            moreString = @"";
            cell.filterType = TenantFilter;
            break;
        }
         
        // Location selected
        case 1: {
            regionType = @"Location";
            selectedItemString = self.selectedLocationString;
            moreString = self.locationMoreString;
            noteString = @"Note : This feed will be shared with all users of the selected location(s).";
            cell.filterType = LocationFilter;
            break;
        }
            
        // Location Group selected
        case 2: {
            regionType = @"Location Group";
            selectedItemString = self.selectedGroupString;
            moreString = self.groupMoreString;
            noteString = @"Note : This feed will be shared with all users of the selected group(s).";
            cell.filterType = LocationGroupFilter;
            break;
        }
            
        // User selected
        case 3: {
            regionType = @"Colleague";
            selectedItemString = self.selectedUserString;
            moreString = self.userMoreString;
            noteString = @"Note : This feed will be shared with selected colleague(s).";
            cell.filterType = UserFilter;
            break;
        }
        default: break;
    }
    
    cell.itemNameLabel.text = regionType;
    cell.selectedItemsLabel.text = selectedItemString;
    cell.noteLabel.text = noteString;
    cell.moreLabel.text = moreString;
    cell.moreLabel.hidden = (moreString.length > 0) ? NO : YES;
    
    // Drop shadow
    cell.layer.shadowColor = APP_COLOR.CGColor;
    cell.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    cell.layer.shadowRadius = 1.0;
    cell.layer.shadowOpacity = 2.0f;
    return cell;
}

//---------------------------------------------------------------

- (void) createSelectedItemStrings:(NSArray *)items withFilterType:(FilterType)filterType {
    @try {
        NSMutableString *tempString = [NSMutableString new];
        NSInteger count = 0;
        for (id object in items) {
            
            NSString *name = @"";
            switch (filterType) {
                case LocationFilter: {
                    Location *location = (Location *)object;
                    name = location.name;
                    break;
                }
                    
                case LocationGroupFilter: {
                    LocationGroup *group = (LocationGroup *)object;
                    name = [NSString stringWithFormat:@"%@ [%@]", group.locationGroupName, group.locationName];
                    break;
                }
                    
                case UserFilter: {
                    User *user = (User *)object;
                    name = user.name;
                    break;
                }
                default: break;
            }
            // Append string to show on board
            [tempString appendFormat:@"%@, ", name];
            
            // Limit condition for character counts
            NSInteger actualLength = tempString.length - 2; // Here we subtract -2 because we added two extra characters comma and space in above string.
            count++;
            
            if (actualLength >= LOCATION_CHARS_LIMIT) {
                break;
            }
        }
        
        NSString *itemNameString = @"";
        NSString *loadMoreString = @"";
        // remove extra characters for last item
        if ([tempString length] > 0) {
            itemNameString = [tempString substringToIndex:[tempString length] - 2];
        }
        
        NSInteger extraLocation = items.count - count;
        loadMoreString = (extraLocation > 0) ? [NSString stringWithFormat:@" & %d more", (int)extraLocation] : @"";
        tempString = nil;

        
        switch (filterType) {
            case LocationFilter: {
                self.selectedLocationString = itemNameString;
                self.locationMoreString = loadMoreString;
                break;
            }
                
            case LocationGroupFilter: {
                self.selectedGroupString = itemNameString;
                self.groupMoreString = loadMoreString;
                break;
            }
            case UserFilter: {
                self.selectedUserString = itemNameString;
                self.userMoreString = loadMoreString;
                break;
            }
            default: break;
        }
        
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *mainCell = [tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
    if (mainCell == nil) {
        mainCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellIdentifier"];
        [mainCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    switch (indexPath.row) {
        case 0: {
            static NSString *shareViewCellIdentifier = @"ShareViewCellIdentifier";

            ShareChartCell *shareCell = (ShareChartCell *)[tableView dequeueReusableCellWithIdentifier:shareViewCellIdentifier];
            if (shareCell == nil) {
                shareCell = [[ShareChartCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:shareViewCellIdentifier];
            }
            [shareCell setSelectionStyle:UITableViewCellSelectionStyleNone];

            shareCell.chartImage.image = self.chartImage;
            mainCell = shareCell;
            break;
        }
            
        case 1 : {
            static NSString *shareOptionCellIdentifier = @"ShareOptionCellIdentifier";
            
            ShareOptionCell *shareOptionCell = (ShareOptionCell *)[tableView dequeueReusableCellWithIdentifier:shareOptionCellIdentifier];
            if (!shareOptionCell) {
                shareOptionCell = [[ShareOptionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:shareOptionCellIdentifier];
            }
            [shareOptionCell setSelectionStyle:UITableViewCellSelectionStyleNone];
            mainCell = shareOptionCell;
            break;
        }
            
        case 2 : {
            static NSString *shareSelectItemCellIdentifier = @"ShareSelectItemCellIdentifier";
            
            ShareSelectItemCell *shareSelectCell = (ShareSelectItemCell *)[tableView dequeueReusableCellWithIdentifier:shareSelectItemCellIdentifier];
            if (!shareSelectCell) {
                shareSelectCell = [[ShareSelectItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:shareSelectItemCellIdentifier];
            }
            mainCell = [self setupShareSelectItemCell:shareSelectCell indexPath:indexPath];
            break;
        }
            
        case 3 : {
            static NSString *shareCommentCellCellIdentifier = @"ShareCommentCellIdentifier";
            
            ShareCommentCell *shareCommentCell = (ShareCommentCell *)[tableView dequeueReusableCellWithIdentifier:shareCommentCellCellIdentifier];
            if (!shareCommentCell) {
                shareCommentCell = [[ShareCommentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:shareCommentCellCellIdentifier];
            }
            [shareCommentCell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            shareCommentCell.titleLabel.hidden = (self.shareReportFor == ShareReportForLike);
            shareCommentCell.cardView.hidden = (self.shareReportFor == ShareReportForLike);
            shareCommentCell.titleLabel.text = (self.shareReportFor == ShareReportForComment) ? @"Your comment" : @"Share your report";
            shareCommentCell.textView.delegate = self;
            
            // Drop shadow
            shareCommentCell.cardView.layer.shadowColor = APP_COLOR.CGColor;
            shareCommentCell.cardView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
            shareCommentCell.cardView.layer.shadowRadius = 1.0;
            shareCommentCell.cardView.layer.shadowOpacity = 2.0f;
            mainCell = shareCommentCell;
            break;
        }
        default: break;
    }
    return mainCell;
}

//---------------------------------------------------------------

- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case 1: {
            ShareOptionCell *shareCell = (ShareOptionCell *)cell;
            [shareCell setCollectionViewDataSourceDelegate:self indexPath:indexPath];
            NSInteger index = shareCell.collectionView.indexPath.row;
            
            CGFloat horizontalOffset = [self.contentOffsetDictionary[[@(index) stringValue]] floatValue];
            [shareCell.collectionView setContentOffset:CGPointMake(horizontalOffset, 0)];
            break;
        }
        default: break;
    }
}

//---------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0: case 1: case 3: return; break;
        default: break;
    }
    
    if (indexPath.row == 2) {
        ShareSelectItemCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if (cell.filterType == TenantFilter) {
            return;
        }
        
        // Open location selection
        if (cell.filterType == LocationFilter) {
            ShareMultiSelectController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareMultiSelectController"];
            controller.delegate = self;            
            controller.multiSelectionArray = self.selectedLocations;
            controller.filterType = LocationFilter;
            controller.filter = self.filter;
            controller.dataArray = self.locationArray;
            [self.navigationController pushViewController:controller animated:YES];
        }
        
        // Open location group selection
        if (cell.filterType == LocationGroupFilter) {
            // Open selection view / for location group it will be same like filters view with multiple selection
            ShareFilterViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareFilterViewController"];
            // if there is no location then call webservice first
            if (self.locationArray.count == 0) {
                [self callWebServiceToGetLocations:^(BOOL finish) {
                    controller.delegate = self;
                    controller.filterItems = self.selectedGroupItems;
                    controller.locationArray = self.locationArray;
                    [self.navigationController pushViewController:controller animated:YES];
                }];
            } else {
                controller.delegate = self;
                controller.filterItems = self.selectedGroupItems;
                controller.locationArray = self.locationArray;
                [self.navigationController pushViewController:controller animated:YES];
            }
        }
        
        // Open user selection
        if (cell.filterType == UserFilter) {
            ShareMultiSelectController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareMultiSelectController"];
            controller.delegate = self;
            controller.multiSelectionArray = self.selectedUsers;
            controller.filterType = UserFilter;
            controller.filter = self.filter;
            controller.dataArray = self.userArray;
            [self.navigationController pushViewController:controller animated:YES];
        }
    }    
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UICollectionViewDataSource methods

//---------------------------------------------------------------

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 4;
}

//---------------------------------------------------------------

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ShareOptionCollectionCell *cell = (ShareOptionCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:ShareOptionCollectionViewCellIdentifier forIndexPath:indexPath];
    
    NSString *imageName = @"";
    NSString *itemName = @"";
    
    switch (indexPath.row) {
        case 0: {
            imageName = @"tenant";
            itemName = @"Tenant";
            break;
        }
        case 1: {
            imageName = @"pin";
            itemName = @"Location";
            break;
        }
        case 2: {
            imageName = @"location-group";
            itemName = @"Location Group";
            break;
        }
        case 3: {
            imageName = @"user";
            itemName = @"Colleague";
            break;
        }
        default: break;
    }
    
    [cell.itemButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    cell.itemLabel.text = itemName;
    

    // Change selection color
    if (self.selectedIndex == indexPath.row) {
        cell.itemButton.tintColor = MATERIAL_PINK_COLOR;
        cell.itemButton.layer.borderColor = MATERIAL_PINK_COLOR.CGColor;
        cell.itemLabel.textColor = [UIColor blackColor];
    } else {
        cell.itemButton.tintColor = SOCIAL_BUTTON_COLOR;
        cell.itemButton.layer.borderColor = SOCIAL_BUTTON_COLOR.CGColor;
        cell.itemLabel.textColor = SOCIAL_BUTTON_COLOR;
    }
    
    return cell;
}

//---------------------------------------------------------------

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    // Deselect previous cell
    ShareOptionCollectionCell *previousCell = (ShareOptionCollectionCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:self.selectedIndex inSection:0]];
    previousCell.itemButton.tintColor = SOCIAL_BUTTON_COLOR;
    previousCell.itemButton.layer.borderColor = SOCIAL_BUTTON_COLOR.CGColor;
    previousCell.itemLabel.textColor = SOCIAL_BUTTON_COLOR;

    // Select current cell
    ShareOptionCollectionCell *cell = (ShareOptionCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [Helper tapOnControlAnimation:cell forScale:0.5 forCompletionBlock:^(BOOL finished) {
    }];
    cell.itemButton.tintColor = MATERIAL_PINK_COLOR;
    cell.itemButton.layer.borderColor = MATERIAL_PINK_COLOR.CGColor;
    cell.itemLabel.textColor = [UIColor blackColor];
    self.selectedIndex = indexPath.row;
    
    // Reload only one row
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UIScrollViewDelegate methods

//---------------------------------------------------------------

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    if (![scrollView isKindOfClass:[UICollectionView class]]) return;
    
    CGFloat horizontalOffset = scrollView.contentOffset.x;
    
    ShareCollectionView *collectionView = (ShareCollectionView *)scrollView;
    NSInteger index = collectionView.indexPath.row;
    self.contentOffsetDictionary[[@(index) stringValue]] = @(horizontalOffset);
}

//---------------------------------------------------------------

#pragma mark
#pragma mark MultiSelectionDelegate delegate methods

//---------------------------------------------------------------

- (void) selectedItems:(NSArray *)items forType:(FilterType)filterType {
    if (filterType == LocationFilter) {
        self.selectedLocations = items;
    } else if (filterType == UserFilter) {
        self.selectedUsers = items;
    }
    
    [self createSelectedItemStrings:items withFilterType:filterType];
    [self.tableView reloadData];
}

//---------------------------------------------------------------

- (void) updateUserArray:(NSArray *)userArray {
    self.userArray = userArray;
}

//---------------------------------------------------------------

- (void) updateLocationArray:(NSArray *)locationArray {
    self.locationArray = locationArray;
    
    // create items
    NSMutableArray *tempArray = [NSMutableArray new];
    for (Location *location in self.locationArray) {
        RefineItem *item = [RefineItem new];
        item.itemId = location.locationId;
        item.title = location.name;
        item.subItems = nil;
        item.filterType = LocationFilter;
        [tempArray addObject:item];
    }
    self.selectedGroupItems = [NSArray arrayWithArray:tempArray];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark ShareFilterViewDelegate methods

//---------------------------------------------------------------

- (void) selectedLocationGroups:(NSArray *)items {
    @try {
        self.selectedGroupItems = items;
        
        NSMutableArray *tempArray = [NSMutableArray new];
        for (RefineItem *item in items) {
            if (item.selectedItems.count > 0) {
                
                for (LocationGroup *group in item.selectedItems) {
                    group.locationName = item.title;
                    [tempArray addObject:group];
                }
            }
        }
        
        self.selectedGroups = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
        
        [self createSelectedItemStrings:self.selectedGroups withFilterType:LocationGroupFilter];
        [self.tableView reloadData];
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark  methods

//---------------------------------------------------------------

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    self.commentString = [Helper removeWhiteSpaceAndNewLineCharacter:textView.text];
    return YES;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"SHARE TO FEEDS";
    _contentOffsetDictionary = [NSMutableDictionary dictionary];
    self.selectedIndex = 0;
    
    // table settings
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 50;
    
//    [self createFloatingShareButton];
    
    // create location group items
    // create items
    NSMutableArray *tempArray = [NSMutableArray new];
    for (Location *location in self.locationArray) {
        RefineItem *item = [RefineItem new];
        item.itemId = location.locationId;
        item.title = location.name;
        item.subItems = nil;
        item.filterType = LocationFilter;
        [tempArray addObject:item];
    }
    self.selectedGroupItems = [NSArray arrayWithArray:tempArray];
}

//---------------------------------------------------------------

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.title = @"SHARE TO FEEDS";
}

//---------------------------------------------------------------

@end
