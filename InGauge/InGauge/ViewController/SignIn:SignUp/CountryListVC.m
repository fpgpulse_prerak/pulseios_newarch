//
//  ContryListVC.m
//  IN-Gauge
//
//  Created by Bharat Chandera on 04/08/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "CountryListVC.h"
#import "NSLocale+TTEmojiFlagString.h"

@interface CountryListVC () <UISearchBarDelegate>

@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) NSArray *arrCountryList;

@end

@implementation CountryListVC

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc {
    // Remove delegates, mutable array and observers here!
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (NSArray *) countryListJson {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"CountryList" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    return [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

//---------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrCountryList.count;
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tick"]];

    NSDictionary *dict = self.arrCountryList[indexPath.row];
    NSString *countryFlg = [NSLocale emojiFlagForISOCountryCode:[NSString stringWithFormat:@"%@",dict[@"code"]]];
   
    UIFont *font = [UIFont fontWithName:AVENIR_FONT size:14];
    cell.textLabel.font = font;
    cell.detailTextLabel.font = font;
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",countryFlg,dict[@"name"]];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",dict[@"dial_code"]];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedCountryCode"] isEqual:dict[@"code"]]) {
        cell.accessoryView.hidden = NO;
    } else {
        cell.accessoryView.hidden = YES;
    }
    return cell;
}

//---------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dict = self.arrCountryList[indexPath.row];
    
    NSString* countryFlg = [NSLocale emojiFlagForISOCountryCode:[NSString stringWithFormat:@"%@",dict[@"code"]]];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@ %@",countryFlg,dict[@"dial_code"]] forKey:@"SelectedCountry"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",dict[@"dial_code"]] forKey:@"SelectedCountryCode"];
    [[NSUserDefaults standardUserDefaults]  synchronize];
    [self.navigationController popViewControllerAnimated:YES];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UISearchBar delegate methods

//---------------------------------------------------------------

- (void) searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText {
    if (searchText.length < 1) {
        //reload with full array
        return;
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(name CONTAINS [cd] %@)",searchText];
    NSArray *filterdZones = [[self countryListJson] filteredArrayUsingPredicate:predicate];
    self.arrCountryList = [NSArray arrayWithArray:filterdZones];
    [self.tableView reloadData];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View LifeCycle methods

//---------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    _arrCountryList = [self countryListJson];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setDefaultTextAttributes:@{ NSFontAttributeName: [UIFont fontWithName:AVENIR_FONT size:14],}];
}

//---------------------------------------------------------------

@end
