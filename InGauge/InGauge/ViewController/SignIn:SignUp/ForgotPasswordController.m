    //
//  ForgotPasswordController.m
//  InGauge
//
//  Created by Mehul Patel on 10/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "ForgotPasswordController.h"
#import "AppDelegate.h"
#import "AnalyticsLogger.h"

@interface ForgotPasswordController () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField        *emailText;
@property (nonatomic, weak) IBOutlet UIButton           *getPasswordButton;

@end

@implementation ForgotPasswordController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
    self.emailText.delegate = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) getPasswordButtonTapped:(id)sender {
    // Animation
    [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];

    // Resign all responder
    [self.emailText resignFirstResponder];
    
    // Validation
    NSString *emailString = [Helper removeWhiteSpaceAndNewLineCharacter:self.emailText.text];
    
    if (emailString.length == 0) {
        [self.emailText becomeFirstResponder];
        [Helper showAlertWithTitle:@"" message:@"Please enter your email!" forController:self];
        return;
    }
    
    if (![Helper validateEmail:emailString]) {
        [self.emailText becomeFirstResponder];
        [Helper showAlertWithTitle:@"" message:@"Invalid email address!" forController:self];
        return;
    }
    
    // Web Service call
    [ServiceManager callForgotUpWebService:@{@"email":emailString} whenFinish:^(NSString *errorMessage, NSDictionary *response) {
        if ([[response valueForKey:@"statusCode"] integerValue] == 200) {
            [self dismissViewControllerAnimated:YES completion:^{
    
                [Helper showAlertWithTitle:@"Email was sent successfully!" message:[NSString stringWithFormat:@"You requested to reset the password for account %@, and an email was sent to this mailbox, please follow the email to reset your password.", emailString] forController:App_Delegate.topMostController];
                
                // FireBase - Analytics
                [AnalyticsLogger logForgotPasswordEvent:emailString];
            }];
        } else {
            NSDictionary *dict = response[@"data"][0];
            NSString *str = dict[@"message"];
            [Helper showAlertWithTitle:@"Email!" message:str forController:App_Delegate.topMostController];
        }
    }];
}

//---------------------------------------------------------------

- (IBAction) backButtonTapped:(id)sender {
    [self.emailText resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
}

//---------------------------------------------------------------


#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------


//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    // FireBase - Analytics
    [AnalyticsLogger logEventWithName:@"forgot_password"];

    // Delegation
    self.emailText.delegate = self;
    
    // Button border
    self.getPasswordButton.layer.cornerRadius = 6.0f;
    self.getPasswordButton.layer.masksToBounds = YES;
    self.getPasswordButton.backgroundColor = APP_COLOR;
}

//---------------------------------------------------------------

@end
