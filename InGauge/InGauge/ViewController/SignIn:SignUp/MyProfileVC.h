//
//  MyProfileVC.h
//  InGauge
//
//  Created by Mehul Patel on 18/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewController.h"

@interface MyProfileVC : BaseTableViewController {
    
    IBOutlet UIImageView *userProfileImg;
    IBOutlet UITextField *txtFName;
    IBOutlet UITextField *txtLName;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtRole;
    IBOutlet UITextField *txtPhoneCode;
    IBOutlet UITextField *txtPhone;
    IBOutlet UITextField *txtEmpID;
    IBOutlet UITextField *txtHireDate;
    IBOutlet UITextField *txtFpgLevel;
    
    IBOutlet UIButton *btnGengerMale;
    IBOutlet UIButton *btnGengerFemale;
    IBOutlet UIButton *btnEditPhoto;
    
    IBOutlet UIButton *btnSaveProfile;
    
    IBOutlet UILabel *lblGenderM;
    IBOutlet UILabel *lblGenderF;
    IBOutlet UILabel *lblGender;

}

@end
