//
//  MyProfileVC.m
//  InGauge
//
//  Created by Mehul Patel on 18/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "MyProfileVC.h"
#import "User.h"
#import "UIViewController+LangExt.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "AnalyticsLogger.h"

@interface MyProfileVC () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, weak) IBOutlet UIImageView        *userImage;
@property (nonatomic, strong) User                      *user;
@property (nonatomic, strong) UIImage                   *userAvatar;

@end

@implementation MyProfileVC


//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc {
    // Remove delegates, mutable array and observers here!
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) changeAvatar:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
//    NSString *cancelTitleString = [AppDelegate.keyMapping getKeyValue:@"KEY_ALERT_TITLE_CANCEL"];
//    NSString *cameraTitleString = [AppDelegate.keyMapping getKeyValue:@"KEY_ALERT_TITLE_CAMERA"];
//    NSString *galleryTitleString = [AppDelegate.keyMapping getKeyValue:@"KEY_ALERT_TITLE_GALLERY"];
    
    // Cancel Option
    [actionSheet addAction:[UIAlertAction actionWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_TITLE_CANCEL"] style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    // Take from Camera Option
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take Picture" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
        if (hasCamera) {
            [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
        } else {
            [Helper showAlertWithTitle:@"" message:@"Camera is not available." forController:self];
        }
    }]];
    
    // Take from Gallery option
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Select Picture" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WEBSERVICE methods

//---------------------------------------------------------------

- (void) callWebServiceToGetUserAvatar {
    // Check for internet connnection
    if (![Helper checkForConnection:self]) {
        return;
    }
    
    User *user = [User signedInUser];
    NSString *action = [NSString stringWithFormat:@"%@/%d/%d", GET_USER_AVATAR, App_Delegate.appfilter.industryId.intValue, user.userID.intValue];
    
    NSString *urlString = [ServiceUtil addHeaderParameters:action withParams:nil];
    [self.userImage sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"defaultimg"] options:SDWebImageRefreshCached];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UIImagePickerController delegate methods

//---------------------------------------------------------------

- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    imagePickerController.sourceType = sourceType;
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    
    if (sourceType == UIImagePickerControllerSourceTypeCamera) {
        imagePickerController.showsCameraControls = YES;
    }
    
    UIViewController *topC = App_Delegate.window.rootViewController.topViewController;
    [topC presentViewController:imagePickerController animated:YES completion:nil];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) userSetup {
    self.user = [User signedInUser];
    txtFName.text = self.user.firstName;
    txtLName.text = self.user.lastName;
    txtEmail.text = self.user.email;
    //TODO: Role is missing
    txtPhone.text = self.user.phoneNumber;
    //TODO: Country code is missing in response
    //TODO: Employee Id is missing in response
    
    [btnGengerMale setBackgroundImage:[UIImage imageNamed:@"uncheck"] forState:UIControlStateNormal];
    [btnGengerFemale setBackgroundImage:[UIImage imageNamed:@"uncheck"] forState:UIControlStateNormal];
    
    // Male-Female condition based image
    [self.user.gender isEqualToString:@"Male"] ? [btnGengerMale setBackgroundImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal] : [btnGengerMale setBackgroundImage:[UIImage imageNamed:@"uncheck"] forState:UIControlStateNormal];
    txtHireDate.text = [self.user.hiredDate toNSString:APP_DISPLAY_DATE_TIME_FORMAT];
    txtFpgLevel.text = self.user.fpgLevel;
    txtEmpID.text = self.user.number;
    txtRole.text = App_Delegate.appfilter.userRole;
    
    lblGenderF.text = [App_Delegate.keyMapping getKeyValue:@"KEY_FEMALE_TITLE"];
    lblGenderM.text = [App_Delegate.keyMapping getKeyValue:@"KEY_MALE_TITLE"];
    lblGender.text = [App_Delegate.keyMapping getKeyValue:@"KEY_GENDER_TITLE"];
    
    [self callWebServiceToGetUserAvatar];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

//---------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 12;
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    [cell setUserInteractionEnabled:NO];
    return cell;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UIImagePickerControllerDelegate methods

//---------------------------------------------------------------

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    self.userAvatar = info[UIImagePickerControllerOriginalImage];
    UIImage *editedImage = info[UIImagePickerControllerEditedImage];
    if (editedImage != nil) {
        self.userAvatar = editedImage;
    }
    
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    if (UTTypeConformsTo(kUTTypeImage, (__bridge CFStringRef)mediaType)) {
        [picker dismissViewControllerAnimated:YES completion:nil];
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View LifeCycle methods

//---------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Analytics
    [AnalyticsLogger logEventWithName:@"check_profile"];
    
    self.title = [App_Delegate.keyMapping getKeyValue:@"KEY_MY_PROFILE"];
    
    btnSaveProfile.layer.cornerRadius = 6.0f;
    btnSaveProfile.layer.masksToBounds = YES;
    btnSaveProfile.backgroundColor = APP_COLOR;    
    
    userProfileImg.image = [UIImage imageNamed:@""];
    userProfileImg.layer.cornerRadius = userProfileImg.frame.size.width/2;
    userProfileImg.layer.masksToBounds = YES;
    
    btnEditPhoto.layer.cornerRadius = btnEditPhoto.frame.size.width/2;
    btnEditPhoto.layer.borderColor = [UIColor lightGrayColor].CGColor;
    btnEditPhoto.layer.borderWidth = 1.0;
    btnEditPhoto.layer.masksToBounds = YES;
    btnSaveProfile.hidden = YES;
    
    [self userSetup];
}

//---------------------------------------------------------------

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

//---------------------------------------------------------------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//---------------------------------------------------------------

@end
