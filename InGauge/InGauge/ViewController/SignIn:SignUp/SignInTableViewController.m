//
//  SignInTableViewController.m
//  InGauge
//
//  Created by Mehul Patel on 24/04/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "SignInTableViewController.h"
#import "AppDelegate.h"
#import "NSString+LangExt.h"
#import "AnalyticsLogger.h"

@interface SignInTableViewController () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UIButton           *signInButton;
@property (nonatomic, weak) IBOutlet UITextField        *userNameText;
@property (nonatomic, weak) IBOutlet UITextField        *passwordText;

@property (nonatomic, weak) IBOutlet UIView                 *carRentalView;
@property (nonatomic, weak) IBOutlet UIView                 *hotelView;

@end

@implementation SignInTableViewController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
    self.userNameText.delegate = nil;
    self.passwordText.delegate = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) signInButtonTapped:(UIButton *)sender {
    // Animation
    [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];
    [self validateDetails];
}

//---------------------------------------------------------------

- (IBAction) showPassButtonTapped:(UIButton *)sender  {
    self.passwordText.secureTextEntry = !self.passwordText.secureTextEntry;
}

//---------------------------------------------------------------

- (IBAction) hotelButtonTapped:(id)sender {
    
    self.carRentalView.layer.borderWidth = 0;
    // Animation start
    self.hotelView.transform = CGAffineTransformMakeScale(0.2, 0.2);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.hotelView.transform = CGAffineTransformIdentity;
        self.hotelView.layer.borderWidth = 1;
    } completion:^(BOOL finished) {
        // Set default URL to Europ car
        [Helper loginToAdmin];
        //Analytics
        [AnalyticsLogger logSignInChangeIndustryEvent:@"Hospitality"];
    }];}

//---------------------------------------------------------------

- (IBAction) carButtonTapped:(id)sender {
    
    self.hotelView.layer.borderWidth = 0;
    
    // Animation start
    self.carRentalView.transform = CGAffineTransformMakeScale(0.2, 0.2);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.carRentalView.transform = CGAffineTransformIdentity;
        self.carRentalView.layer.borderWidth = 1;
    } completion:^(BOOL finished) {
        // Set default URL to Europ car
        [Helper loginToEuropCar];
        //Analytics
        [AnalyticsLogger logSignInChangeIndustryEvent:@"Car Rental"];
    }];
}

//---------------------------------------------------------------


#pragma mark
#pragma mark WEBSERVICE methods

//---------------------------------------------------------------

- (void) callAllServices:(User *)user {
    // Show loading indicator
    App_Delegate.globalHud = [Helper showLoading:App_Delegate.window];
    App_Delegate.globalHud.label.text = @"Signing...";
    
//    // Call service to get all keys
//    [ServiceManager getServerMappingKeyList:nil usingBlock:^(NSDictionary *keys) {
        // Login Service call
        [ServiceManager callLoginWebService:user usingBlock:^(NSString *error, NSDictionary *respose) {
            if (error == nil) {
                [Helper hideLoading:App_Delegate.globalHud];
                // Analytics
                [AnalyticsLogger logSignInEvent:user withSuccess:@YES];
            } else {
                // Analytics
                [AnalyticsLogger logSignInEvent:user withSuccess:@NO];
            }
        }];
//    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) validateDetails {
    // Resign all responder
    [self.userNameText resignFirstResponder];
    [self.passwordText resignFirstResponder];
    
    // Validation
    NSString *userNameString = [Helper removeWhiteSpaceAndNewLineCharacter:self.userNameText.text];
    NSString *passwordString = [Helper removeWhiteSpaceAndNewLineCharacter:self.passwordText.text];
    
    if (userNameString.length == 0) {
        [self.userNameText becomeFirstResponder];
        [Helper showAlertWithTitle:@"" message:@"Please enter your email!" forController:self];
        return;
    }
    
    if (passwordString.length == 0) {
        [self.passwordText becomeFirstResponder];
        [Helper showAlertWithTitle:@"" message:@"Please enter your password!" forController:self];
        return;
    }
    
    if (![Helper validateEmail:userNameString]) {
        [self.userNameText becomeFirstResponder];
        [Helper showAlertWithTitle:@"" message:@"Invalid email address!" forController:self];
        return;
    }
    
    __block User * user = [User new];
    user.email = [Helper removeWhiteSpaceAndNewLineCharacter:self.userNameText.text];
    // MD5 encrypt password
    user.password = [self.passwordText.text md5EncryptedStringWithEncoding:NSUTF8StringEncoding];
    
    [self callAllServices:user];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITextField methods

//---------------------------------------------------------------

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.userNameText) {
        [self.passwordText becomeFirstResponder];
    } else if (textField == self.passwordText) {
        [self.passwordText resignFirstResponder];
        [self validateDetails];
    }
    return YES;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    
    self.userNameText.delegate = self;
    self.passwordText.delegate = self;

    // Button border
    self.signInButton.layer.cornerRadius = 6.0f;
    self.signInButton.layer.masksToBounds = YES;
    self.signInButton.backgroundColor = APP_COLOR;
    
    self.hotelView.backgroundColor = [UIColor whiteColor];
    self.carRentalView.backgroundColor = [UIColor whiteColor];
    
    self.hotelView.layer.borderWidth = 0;
    self.hotelView.layer.borderColor = APP_COLOR.CGColor;
    self.hotelView.layer.cornerRadius = 8.0f;
    self.hotelView.layer.masksToBounds = YES;
    
    self.carRentalView.layer.borderWidth = 0;
    self.carRentalView.layer.borderColor = APP_COLOR.CGColor;   
    self.carRentalView.layer.cornerRadius = 8.0f;
    self.carRentalView.layer.masksToBounds = YES;
    
    // Check default domain
    if ([Helper isHotelDomain]) {
        self.hotelView.layer.borderWidth = 1;
    } else {
        self.carRentalView.layer.borderWidth = 1;
    }
}

//---------------------------------------------------------------

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBarTintColor:APP_COLOR];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{
       NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:AVENIR_FONT size:14]
       }
     ];
}

//---------------------------------------------------------------

@end
