//
//  SignUpController.m
//  InGauge
//
//  Created by Mehul Patel on 10/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "SignUpController.h"
#import "NSString+LangExt.h"
#import <RMPickerViewController/RMPickerViewController.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import "NSLocale+TTEmojiFlagString.h"
#import "AnalyticsLogger.h"

#define PHONE_MAX_LENGTH 15
#define PASS_MIN_LENGHT 6

@interface SignUpController () <UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, weak) IBOutlet UITextField        *firstNameText;
@property (nonatomic, weak) IBOutlet UITextField        *lastNameText;
@property (nonatomic, weak) IBOutlet UITextField        *countryCodeText;
@property (nonatomic, weak) IBOutlet UITextField        *phoneText;
@property (nonatomic, weak) IBOutlet UITextField        *empIdText;
@property (nonatomic, weak) IBOutlet UITextField        *jobTitleText;
@property (nonatomic, weak) IBOutlet UITextField        *organizationText;
@property (nonatomic, weak) IBOutlet UITextField        *emailText;
@property (nonatomic, weak) IBOutlet UITextField        *passwordText;
@property (nonatomic, weak) IBOutlet UITextField        *confirmPassText;
@property (nonatomic, weak) IBOutlet UITextField        *selectDomainText;
@property (nonatomic, weak) IBOutlet UIButton           *createAccountButton;

@property (nonatomic, strong) UITextField               *selectedTextField;
@property (nonatomic, strong) NSString                  *countryCodeStr;

@end

@implementation SignUpController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
    self.firstNameText.delegate     = nil;
    self.lastNameText.delegate      = nil;
    self.countryCodeText.delegate   = nil;
    self.phoneText.delegate         = nil;
    self.empIdText.delegate         = nil;
    self.jobTitleText.delegate      = nil;
    self.organizationText.delegate  = nil;
    self.emailText.delegate         = nil;
    self.passwordText.delegate      = nil;
    self.confirmPassText.delegate   = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) showPassButtonTapped:(UIButton *)sender  {
    self.confirmPassText.secureTextEntry = !self.confirmPassText.secureTextEntry;
}

//---------------------------------------------------------------

- (IBAction) backButtonTapped:(id)sender {
    [self.emailText resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
}

//---------------------------------------------------------------

- (IBAction) selectButtonTapped:(id)sender   {
    RMAction<UIPickerView *> *selectAction = [RMAction<UIPickerView *> actionWithTitle:@"Select" style:RMActionStyleDone andHandler:^(RMActionController<UIPickerView *> *controller) {
        
        NSNumber *selectedIndex = @([controller.contentView selectedRowInComponent:0]);
        if ([selectedIndex integerValue] == 0) {
            self.selectDomainText.text = @"Hospitality";
        } else {
            self.selectDomainText.text = @"Car Rental";
        }
    }];
    
    RMAction<UIPickerView *> *cancelAction = [RMAction<UIPickerView *> actionWithTitle:@"Cancel" style:RMActionStyleCancel andHandler:^(RMActionController<UIPickerView *> *controller) {
    }];
    
    RMPickerViewController *pickerController = [RMPickerViewController actionControllerWithStyle:RMActionControllerStyleWhite title:@"Select Domian" message:@"Which domain you want to sign up." selectAction:selectAction andCancelAction:cancelAction];
    pickerController.picker.dataSource = self;
    pickerController.picker.delegate = self;
    
    pickerController.disableBlurEffectsForContentView = YES;
    pickerController.disableBlurEffects = YES;
    [self presentViewController:pickerController animated:YES completion:nil];
}

//---------------------------------------------------------------

- (IBAction) createAccountButtonTapped:(id)sender {
    
    [self.selectedTextField resignFirstResponder];
    
    // Animation
    [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];
    
    // *** 1. Empty field validation
    if (![self validateTextFieldsForEmptyString]) {
        return;
    }
    
    // *** 2. Email validation
    NSString *emailString = [Helper removeWhiteSpaceAndNewLineCharacter:self.emailText.text];
    if (![Helper validateEmail:emailString]) {
        [self.emailText becomeFirstResponder];
        [Helper showAlertWithTitle:@"" message:NSLocalizedStringWithFormat(@"signup.alert.invalidemail") forController:self];
        return;
    }
    
    // *** 3. Password validation - Minimum lenght must be 8 characters
    NSString *passwordString = self.passwordText.text;
    if (passwordString.length < PASS_MIN_LENGHT) {
        [Helper showAlertWithTitle:@"" message:NSLocalizedStringWithFormat(@"signup.alert.passoword.minlength") forController:self];
        return;
    }
    
    // *** 4. Password comparison
    NSString *confirmPassString = self.confirmPassText.text;
    if (![passwordString isEqualToString:confirmPassString]) {
        [Helper showAlertWithTitle:@"" message:NSLocalizedStringWithFormat(@"signup.alert.passwordnotmatch") forController:self];
        return;
    }
    
    // Register account to server
    
    __block User *user = [User new];
    user.email = [Helper removeWhiteSpaceAndNewLineCharacter:self.emailText.text];
    user.firstName = [Helper removeWhiteSpaceAndNewLineCharacter:self.firstNameText.text];
    user.lastName = [Helper removeWhiteSpaceAndNewLineCharacter:self.lastNameText.text];
    user.number = [Helper removeWhiteSpaceAndNewLineCharacter:self.empIdText.text];
    user.phoneNumber = [Helper removeWhiteSpaceAndNewLineCharacter:self.phoneText.text];
    user.jobTitle = [Helper removeWhiteSpaceAndNewLineCharacter:self.jobTitleText.text];
    user.desiredLocation = [Helper removeWhiteSpaceAndNewLineCharacter:self.organizationText.text];
    user.countryCode = self.countryCodeStr;
    
    // MD5 encrypt password
    user.password = [self.passwordText.text md5EncryptedStringWithEncoding:NSUTF8StringEncoding];
    
    if ([self.selectDomainText.text isEqualToString:@"Hospitality"]) {
        // Set default URL to Hospitality
        [Helper loginToAdmin];
    } else {
        // Set default URL to Europ car
        [Helper loginToEuropCar];
    }
    
    [self callAllServices:user];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WEBSERVICE methods

//---------------------------------------------------------------

- (void) callAllServices:(User *)user {
    [ServiceManager callSignUpWebService:user whenFinish:^(NSString *errorMessage, NSDictionary *response) {
        if (errorMessage == nil) {
            [self dismissViewControllerAnimated:YES completion:^{
                [Helper showAlertWithTitle:@"Success!" message:@"Thank you for registering for IN-Gauge. The account is in pending status and will be activated by your System administrator" forController:App_Delegate.topMostController];
                
                // FireBase - Analytics
                [AnalyticsLogger logRegisterUserEvent:user withSuccess:@YES];
            }];
        } else {
            NSDictionary *dict = response[@"data"][0];
            NSString *str = dict[@"message"];
            [Helper showAlertWithTitle:@"SignUp!" message:str forController:App_Delegate.topMostController];
            // FireBase - Analytics
            [AnalyticsLogger logRegisterUserEvent:user withSuccess:@NO];            
        }
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (NSString *) setDefaultCountryCode {
    
    NSString *countryIdentifier = [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(code CONTAINS [cd] %@)",countryIdentifier];//if ur array is only strings then change format to @"(self CONTAINS [cd] %@)"
    NSArray *filterdZones = [[self countryListJson] filteredArrayUsingPredicate:predicate];
    
    NSDictionary *dict = filterdZones[0];
    NSString *countryFlg = [NSLocale emojiFlagForISOCountryCode:[NSString stringWithFormat:@"%@",dict[@"code"]]];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@ %@",countryFlg,dict[@"dial_code"]] forKey:@"SelectedCountry"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",dict[@"dial_code"]] forKey:@"SelectedCountryCode"];
    [[NSUserDefaults standardUserDefaults]  synchronize];
    
    self.countryCodeStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedCountryCode"];
    [self.countryCodeStr stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    return  [NSString stringWithFormat:@"%@ %@",countryFlg,dict[@"dial_code"]];
}

//---------------------------------------------------------------

- (NSArray *) countryListJson {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"CountryList" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    return [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
}

//---------------------------------------------------------------

- (BOOL) validateTextFieldsForEmptyString {
    
    NSString *emailString = [Helper removeWhiteSpaceAndNewLineCharacter:self.emailText.text];
    NSString *passString = [Helper removeWhiteSpaceAndNewLineCharacter:self.passwordText.text];
    NSString *confirmPassString = [Helper removeWhiteSpaceAndNewLineCharacter:self.confirmPassText.text];
    NSString *firstNameString = [Helper removeWhiteSpaceAndNewLineCharacter:self.firstNameText.text];
    NSString *lastNameString = [Helper removeWhiteSpaceAndNewLineCharacter:self.lastNameText.text];
    NSString *phoneString = [Helper removeWhiteSpaceAndNewLineCharacter:self.phoneText.text];
    NSString *jobTitleString = [Helper removeWhiteSpaceAndNewLineCharacter:self.jobTitleText.text];
    NSString *empIdString = [Helper removeWhiteSpaceAndNewLineCharacter:self.empIdText.text];
    NSString *organizationString = [Helper removeWhiteSpaceAndNewLineCharacter:self.organizationText.text];
    
    NSString *message = nil;
    BOOL isValid = YES;
    // First Name
    if (firstNameString.length == 0) {
        message = NSLocalizedStringWithFormat(@"signup.firstname.required");
        [self.firstNameText becomeFirstResponder];
        isValid = NO;
    }
    // Last Name
    else if (lastNameString.length == 0) {
        message = NSLocalizedStringWithFormat(@"signup.lastname.required");
        [self.lastNameText becomeFirstResponder];
        isValid = NO;
    }
    
    // Cell/Mobile
    else if (phoneString.length == 0) {
        message = NSLocalizedStringWithFormat(@"signup.phone.required");
        [self.phoneText becomeFirstResponder];
        isValid = NO;
    }
    
    // Employee ID
    else if (empIdString.length == 0) {
        message = NSLocalizedStringWithFormat(@"signup.empid.required");
        [self.empIdText becomeFirstResponder];
        isValid = NO;
    }
    
    // Job Title
    else if (jobTitleString.length == 0) {
        message = NSLocalizedStringWithFormat(@"signup.jobtitle.required");
        [self.jobTitleText becomeFirstResponder];
        isValid = NO;
    }
    
    // Organization
    else if (organizationString.length == 0) {
        message = NSLocalizedStringWithFormat(@"signup.organization.required");
        [self.organizationText becomeFirstResponder];
        isValid = NO;
    }
    
    // Email
    else if (emailString.length == 0) {
        message = NSLocalizedStringWithFormat(@"signup.email.required");
        [self.emailText becomeFirstResponder];
        isValid = NO;
    }
    
    // Password
    else if (passString.length == 0) {
        message = NSLocalizedStringWithFormat(@"signup.password.required");
        [self.passwordText becomeFirstResponder];
        isValid = NO;
    }
    
    // Confirm Password
    else if (confirmPassString.length == 0) {
        message = NSLocalizedStringWithFormat(@"signup.repassword.required");
        [self.confirmPassText becomeFirstResponder];
        isValid = NO;
    }
    
    if (message) {
        [Helper showAlertWithTitle:@"" message:message forController:self];
    }
    
    return isValid;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark RMPickerViewController Delegates methods

//---------------------------------------------------------------

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

//---------------------------------------------------------------

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 2;
}

//---------------------------------------------------------------

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return (row == 0) ? @"Hospitality" :  @"Car Rental";
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITextField delegate methods

//---------------------------------------------------------------

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    self.selectedTextField = textField;
    return YES;
}

//---------------------------------------------------------------

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];

    // FireBase - Analytics
    [AnalyticsLogger logEventWithName:@"sign_up"];
    
    // Delegates
    self.firstNameText.delegate     = self;
    self.lastNameText.delegate      = self;
    self.countryCodeText.delegate   = self;
    self.phoneText.delegate         = self;
    self.empIdText.delegate         = self;
    self.jobTitleText.delegate      = self;
    self.organizationText.delegate  = self;
    self.emailText.delegate         = self;
    self.passwordText.delegate      = self;
    self.confirmPassText.delegate   = self;
    
    self.selectDomainText.userInteractionEnabled = NO;
    
    // Button border
    self.createAccountButton.layer.cornerRadius = 6.0f;
    self.createAccountButton.layer.masksToBounds = YES;
    self.createAccountButton.backgroundColor = APP_COLOR;
}

//---------------------------------------------------------------

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];

    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedCountry"]) {
        self.countryCodeText.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedCountry"]];
        self.countryCodeStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedCountryCode"];
        [self.countryCodeStr stringByReplacingOccurrencesOfString:@"+" withString:@""];
    } else {
        self.countryCodeText.text = [NSString stringWithFormat:@"%@",[self setDefaultCountryCode]];
    }
}

//---------------------------------------------------------------

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.selectedTextField resignFirstResponder];
}

//---------------------------------------------------------------

@end

