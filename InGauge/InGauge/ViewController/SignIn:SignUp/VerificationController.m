//
//  VerificationController.m
//  pulse
//
//  Created by Mehul Patel on 28/04/16.
//  Copyright © 2016 frontline performance group. All rights reserved.
//

#import "VerificationController.h"

@interface VerificationController() <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField            *codeText;
@property (nonatomic, weak) IBOutlet UILabel                *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel                *infoLabel;
@property (nonatomic, weak) IBOutlet UILabel                *resendLabel;
@property (nonatomic, weak) IBOutlet UIButton               *nextButton;
@property (nonatomic, weak) IBOutlet UIButton               *resendButton;

@end

@implementation VerificationController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
    self.codeText.delegate = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) nextButtonTapped:(UIButton *)sender {
    
    // Button pressed animation
    sender.transform = CGAffineTransformMakeScale(0.8, 0.8);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        sender.transform = CGAffineTransformIdentity;
        [self callWebServiceToVerifyCode];
    } completion:^(BOOL finished) {}];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WEBSERVICE methods

//---------------------------------------------------------------

- (void) callWebServiceToVerifyCode {
    // Validation...
    // 1. Empty textfield
    NSString *otpCode = [Helper removeWhiteSpaceAndNewLineCharacter:self.codeText.text];
    if (otpCode.length == 0) {
        [Helper showAlertWithTitle:@"" message:(@"signup.optcode.required") forController:self];
        return;
    }
    
    // Call service
    [ServiceManager callWebServiceToVerify2FACode:otpCode usingBlock:^(NSString *error, NSDictionary *respose) {
        
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

//---------------------------------------------------------------

#pragma mark
#pragma mark UITextField delegate methods

//---------------------------------------------------------------

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    // Max number
    self.nextButton.enabled = (newLength < 1) ?  NO : YES;
    return YES;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    self.title = @"OTP Verfiy";
    
    // Button settings
    self.nextButton.layer.cornerRadius  = 16.0f;
    self.nextButton.layer.borderColor   = [UIColor whiteColor].CGColor;
    self.nextButton.layer.borderWidth   = 1.0;
    self.nextButton.layer.masksToBounds = YES;

    self.resendLabel.hidden     = YES;
    self.resendButton.hidden    = YES;
    
    self.titleLabel.text = @"IN-Gauge Secure Access";
    self.infoLabel.text = @"Please enter the code received via text message";
    
    // Text field settings
    self.codeText.delegate = self;
    [self.codeText setKeyboardType:UIKeyboardTypeNumberPad];
}

//---------------------------------------------------------------

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:NO];
    
    // Show line dash pattern
    CAShapeLayer *border = [CAShapeLayer layer];
    border.strokeColor = [UIColor lightGrayColor].CGColor;
    border.fillColor = nil;
    border.lineDashPattern = @[@4, @2];
    [self.codeText.layer addSublayer:border];
    
    border.path = [UIBezierPath bezierPathWithRect:self.codeText.bounds].CGPath;
    border.frame = self.codeText.bounds;
}

//---------------------------------------------------------------

@end
