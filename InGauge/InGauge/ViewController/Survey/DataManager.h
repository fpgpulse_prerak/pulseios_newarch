//
//  DataManager.h
//  IN-Gauge
//
//  Created by Mehul Patel on 11/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helper.h"
#import "ServiceManager.h"
#import "SurveyHelper.h"

@interface DataManager : NSObject

@property (nonatomic, strong) Event             *event;

@property (nonatomic, strong) NSArray           *sectionArray;
@property (nonatomic, strong) NSArray           *symptomArray;
@property (nonatomic, strong) NSArray           *strengthArray;
@property (nonatomic, strong) NSDictionary      *answerList;
@property (nonatomic, strong) NSString          *surveyEntityTypeString;

@property (nonatomic, assign) EVENT_TYPE        eventType;
@property (nonatomic, assign) SURVEY_TYPE       surveyType;
@property (nonatomic, assign) BOOL              isCompleted;

- (void) calculateAgentScore;

@end
