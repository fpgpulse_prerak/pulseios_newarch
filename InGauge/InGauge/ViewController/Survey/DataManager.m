//
//  DataManager.m
//  IN-Gauge
//
//  Created by Mehul Patel on 11/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "DataManager.h"

#define AnswerScore_YES 5
#define AnswerScore_NO  1
#define AnswerScore_NA  0

@implementation DataManager


//---------------------------------------------------------------

#pragma mark
#pragma mark Score calculation methods

//---------------------------------------------------------------

- (void) calculateAgentScore {
    /*   Formulas
     **** For all questions per section
     1. Answer score (AS) => YES = 5, NO = 1, NA = 0
     2. Question weight (QW) => From server response/ 0 for NA answer
     3. Question score            (QS)  = AS * QW
     4. Question Weight Total     (QWT) = Sum (QW)
     5. Answer Score Total        (AST) = Sum (AS)
     6. Question Score Total      (QST) = Sum (QS)
     7. Total possible score      (TPS) = QWT * valueOf('YES') (i.e. 5 for now)
     8. section Percentage        (SP)  = (QST / TPS) * 100
     
     **** For all sections
     9. Degree of importance (DOI) => Get it from response
     10.Total percentage (TP)      => totalPercentage += SP * DOI
     11.Total DOI                  => totalDOI += DOI
     12.Final Score                => totalPercentage / totalDOI
     
     ---------------------------------------------------------------
     ********Start calculation
     */
    [MBProgressHUD showHUDAddedTo:App_Delegate.topMostController.view animated:YES];
    
    // 10. Total percentage (TP)   => totalPercentage += SP * DOI
    double totalPercentage = 0.0f;
    // 11.Total DOI                => totalDOI += DOI
    double totalDOI = 0.0f;
    
    for (QuestionSection *section in self.sectionArray) {
        // get section percentage
        
        // 9. Degree of importance
        double DOI = section.degreeOfImportance.doubleValue;
        double segPer = [self getsectionPercentage:section];
        
        //NOTE: This is new change from server
        // If section percentage is zero than do not consider it in calculation
        if (segPer != 0) {
            totalPercentage += [self getsectionPercentage:section] * DOI;
            totalDOI += DOI;
        }
    }
    
    // 12. Final Score = totalPercentage / totalDOI
    double finalScore = totalPercentage / totalDOI;
    double validateScore = [Helper validateInfinte:finalScore];
    
    self.event.scoreVal = [NSNumber numberWithFloat:validateScore];
    
    // Create Opportunities array
    [self createOpportunitiesTextArray];
    
    runOnMainQueueWithoutDeadlocking(^{
        [MBProgressHUD hideHUDForView:App_Delegate.topMostController.view animated:YES];
    });
    
    // Update coaching score label
    if ([NSNotification notificationWithName:kNotificationUpdateScoreOnCoaching object:nil]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationUpdateScoreOnCoaching object:nil];
    }
}

//---------------------------------------------------------------

- (double) getsectionPercentage:(QuestionSection *)section {
    // 4. Question Weight Total (QWT) = Sum(QW)
    double queWeightTotal = 0.0f;
    
    // 5. Answer Score Total (AST) = Sum (AS)
    double ansScoreTotal = 0.0f;
    
    // 6. Question Score Total (QST) = Sum(QS)
    double queScoreTotal = 0.0f;
    
    for (Question *question in section.questions) {
        NSString *questionId = question.questionId.stringValue;
        NSString *answerString = [self.answerList valueForKey:questionId];
        
        /* For NA */
        double weight = question.weight.doubleValue;
        if ([answerString isEqualToString:ANSWER_NA]) {
            weight = 0;
        }
        
        queWeightTotal += weight;
        ansScoreTotal  += [self getAnswerScore:question];
        queScoreTotal  += [self getQuestionScore:question];
    }
    
    // 7. Total possible score (TPS) = QWT * valueOf('YES')
    double totalPossibleScore = queWeightTotal * AnswerScore_YES;
    
    // 8. section Percentage (SP) = (QST / TPS) * 100
    double sectionPercentage = 0.0f;
    if (totalPossibleScore > 0) {
        sectionPercentage = (queScoreTotal / totalPossibleScore) * 100;
    }
    return sectionPercentage;
}

//---------------------------------------------------------------

- (double) getAnswerScore:(Question *)question {
    // 1. Answer score (AS) => YES = 5, NO = 1, NA = 0
    double answerScore = 0.0f;
    
    NSString *questionId = question.questionId.stringValue;
    NSString *answerString = [self.answerList valueForKey:questionId];
    
    if ([answerString isEqualToString:ANSWER_YES]) {
        answerScore = AnswerScore_YES;
    } else if ([answerString isEqualToString:ANSWER_NO]) {
        answerScore = AnswerScore_NO;
    } else {
        answerScore = AnswerScore_NA;
    }
    return answerScore;
}

//---------------------------------------------------------------

- (double) getQuestionScore:(Question *)question {
    
    // 2. Question weight (QW) => From server response/ 0 for NA answer
    double questionWeight = question.weight.floatValue;
    
    // 3. Question score (QS)  = AS * QW
    double questionScore = [self getAnswerScore:question] * questionWeight;
    return questionScore;
}

//---------------------------------------------------------------

- (void) createOpportunitiesTextArray {
    NSMutableArray *tempArray = [NSMutableArray new];
    
    for (QuestionSection *section in self.sectionArray) {
        for (Question *question in section.questions) {
            NSString *questionId = question.questionId.stringValue;
            NSString *answerString = [self.answerList valueForKey:questionId];
            
            if ([answerString isEqualToString:ANSWER_NO]) {
                NSString *symptomString = question.symptom ? question.symptom : @"";
                [tempArray addObject:symptomString];
            }
        }
    }
    self.symptomArray = [NSArray arrayWithArray:tempArray];
    tempArray = nil;
}

//---------------------------------------------------------------

- (void) createStrengthTextArray {
    NSMutableArray *tempArray = [NSMutableArray new];
    
    for (QuestionSection *section in self.sectionArray) {
        for (Question *question in section.questions) {
            NSString *questionId = question.questionId.stringValue;
            NSString *answerString = [self.answerList valueForKey:questionId];
            
            if ([answerString isEqualToString:ANSWER_YES]) {
                NSString *symptomString = question.symptom ? question.symptom : @"";
                [tempArray addObject:symptomString];
            }
        }
    }
    self.strengthArray = [NSArray arrayWithArray:tempArray];
    tempArray = nil;
}

//---------------------------------------------------------------

@end
