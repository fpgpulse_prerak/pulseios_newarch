//
//  EventManager.m
//  pulse
//
//  Created by Mehul Patel on 15/12/15.
//  Copyright © 2015 frontline performance group. All rights reserved.
//

#import "EventManager.h"
#import <EventKit/EventKit.h>
#import "SurveyHelper.h"

#define ALARAM_TIME 15

@interface EventManager()

@end

@implementation EventManager

//---------------------------------------------------------------

#pragma mark
#pragma mark EKEventStore methods

//---------------------------------------------------------------

- (instancetype) init {
    self = [super init];
    if (self) {
        self.eventStore = [[EKEventStore alloc] init];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        // Check if the access granted value for the events exists in the user defaults dictionary.
        if ([userDefaults valueForKey:@"eventkit_events_access_granted"] != nil) {
            // The value exists, so assign it to the property.
            self.eventsAccessGranted = [[userDefaults valueForKey:@"eventkit_events_access_granted"] intValue];
        }
        else{
            // Set the default value.
            self.eventsAccessGranted = NO;
        }
    }
    return self;
}

//---------------------------------------------------------------

- (void) setEventsAccessGranted:(BOOL)eventsAccessGranted {
    _eventsAccessGranted = eventsAccessGranted;
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:eventsAccessGranted] forKey:@"eventkit_events_access_granted"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

//---------------------------------------------------------------

- (void) saveAllEvents:(NSArray *)events withBlock:(void(^)(BOOL isFinish))block {
    
    @try {
        for (Event *event in events) {
            if (event) {
                [self synchroniseEventsWithLocalCalendar:event];
            }
        }
        block(YES);
    } @catch (NSException *exception) {
            DLog(@"exception :%@",exception.debugDescription);
            block(NO);
    }
}

//---------------------------------------------------------------

- (void) removeEvent:(Event *)event {
    @try {
        // Add an alarm before 15 mins to an actual event
        NSPredicate *predicateForEvents = [self.eventStore predicateForEventsWithStartDate:self.startDate endDate:self.endDate calendars:[NSArray arrayWithObject:[self.eventStore defaultCalendarForNewEvents]]];
        //set predicate to search for an event of the calendar(you can set the startdate, enddate and check in the calendars other than the default Calendar)
        
        NSArray *events_Array = [self.eventStore eventsMatchingPredicate: predicateForEvents];
        //get array of events from the eventStore
        
        for (EKEvent *eventToCheck in events_Array) {
            if ([eventToCheck.title isEqualToString:event.questionnaireName]) {
                NSError *error;
                [self.eventStore removeEvent:eventToCheck span:EKSpanThisEvent error:&error];
                if (error) {
                    DLog(@"exception :%@",error.debugDescription);
                }
                break;
            }
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (void) removeAllEvents:(NSArray *)events withBlock:(void(^)(BOOL isFinish))block {
    @try {
        for (Event *event in events) {
            [self removeEvent:event];
        }
        block(YES);
    }
    @catch (NSException *exception) {
        DLog(@"exception :%@",exception.debugDescription);
        block(NO);
    }
}

//---------------------------------------------------------------

- (void) synchroniseEventsWithLocalCalendar:(Event *)serverEvent {

    // removes an event if it is already there to update it
    if (serverEvent.eventId == nil) {
        return;
    }

    // Create a new event object.
    EKEvent *event = [EKEvent eventWithEventStore:self.eventStore];
    event.calendar = self.eventStore.defaultCalendarForNewEvents;
    
    // Add an alarm before 15 mins to an actual event
    NSTimeInterval interval = -(NSTimeInterval)((u_int32_t)ALARAM_TIME * 60);

    EKAlarm *alarm = [EKAlarm alarmWithRelativeOffset:interval];
    [event addAlarm:alarm];
    
    // IF date is not there return
    if (serverEvent.startDate == nil) {
        return;
    }

    NSDate *startDate = [SurveyHelper getLocalDateConvertFromUtcTimeStamp:serverEvent.startDate.doubleValue];    
    NSDate *alramEndDate = startDate;
    NSDate *alramStartDate = [startDate dateByAddingTimeInterval:-(NSTimeInterval)((u_int32_t)ALARAM_TIME * 60)];
    
    //Title
    if (serverEvent.questionnaireName == nil) {
        return;
    }

    //event title
    event.title = serverEvent.questionnaireName;
    //start date
    event.startDate = alramStartDate;
    event.endDate = alramEndDate;    
    event.notes = @"";
    
    // Save and commit the event.
    NSError *error;
    if ([self.eventStore saveEvent:event span:EKSpanFutureEvents commit:YES error:&error]) {
    } else {
        DLog(@"%@", [error localizedDescription]);
    }
}

//---------------------------------------------------------------

@end
