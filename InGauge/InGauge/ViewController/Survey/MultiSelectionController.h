//
//  MultiSelectionController.h
//  IN-Gauge
//
//  Created by Mehul Patel on 13/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "DataManager.h"

@protocol MultiSelectionDelegate <NSObject>

- (void) selectedItems:(NSArray *)items;
//- (BOOL) shouldDisableOption:(id)option;

@end

@interface MultiSelectionController : BaseViewController

@property (nonatomic, weak) id <MultiSelectionDelegate>   delegate;

@property (nonatomic, strong) GreatJobList              *jobList;

@property (nonatomic, assign) FilterType                itemId;
@property (nonatomic, strong) Filters                   *filter;
@property (nonatomic, strong) DataManager               *dataManager;
@property (nonatomic, strong) NSArray                   *multiSelectionArray;

@end
