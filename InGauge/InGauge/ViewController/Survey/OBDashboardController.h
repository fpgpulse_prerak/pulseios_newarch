//
//  OBDashboardController.h
//  IN-Gauge
//
//  Created by Mehul Patel on 14/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewController.h"

@interface OBDashboardController : BaseViewController

@property (nonatomic, assign) SURVEY_TYPE               surveyType;

@end
