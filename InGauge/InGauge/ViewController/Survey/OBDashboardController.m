//
//  OBDashboardController.m
//  IN-Gauge
//
//  Created by Mehul Patel on 14/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "OBDashboardController.h"
#import "SLButton.h"
#import "SurveyDateFilter.h"
#import "OBFilterController.h"
#import "OBDashboardCell.h"
#import "ObserveAgentController.h"
#import "LGRefreshView.h"
#import "UITableView+DragLoad.h"
#import "EventManager.h"

#define REMOVE_BUTTON_TAG   1000
#define EDIT_BUTTON_TAG     5000
#define VIEW_BUTTON_TAG     10000

#define TEXT_STATUS_COMPLETED   @"COMPLETED"
#define TEXT_STATUS_PENDING     @"PENDING"
#define TEXT_STATUS_CANCELLED   @"CANCELLED"

@interface OBDashboardController() <UITableViewDelegate, UITableViewDataSource, OBFilterDelegate, DateFilterDelegate, UITableViewDragLoadDelegate>

@property (nonatomic, weak) IBOutlet UITableView        *tableView;
@property (nonatomic, weak) IBOutlet SLButton           *filterButton;
@property (nonatomic, weak) IBOutlet UIButton           *selectionButton;
@property (nonatomic, weak) IBOutlet UILabel            *dateLabel;
@property (nonatomic, weak) IBOutlet UIView             *filterView;
@property (nonatomic, weak) IBOutlet UIBarButtonItem    *takeObButton;

@property (nonatomic, strong) UIView                    *noRecordsView;
@property (nonatomic, strong) LGRefreshView             *refreshView;
@property (nonatomic, strong) LocationList              *locationList;
@property (nonatomic, strong) OBCategoryList            *categoryList;
@property (nonatomic, strong) Filters                   *filter;
@property (nonatomic, strong) EventList                 *eventList;
@property (nonatomic, strong) EventManager              *eventManager;

@property (nonatomic, strong) NSArray                   *statusArray;
@property (nonatomic, strong) NSArray                   *dataArray;
@property (nonatomic, strong) NSIndexPath               *selectedIndexPath;

@property (nonatomic) NSInteger                         pageNo;
@property (nonatomic) NSInteger                         pageSize;
@property (nonatomic) BOOL                              isFirstLoad;
@property (nonatomic) BOOL                              isCellExpand;

@end

@implementation OBDashboardController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUpdateObservationDashboard object:nil];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

////---------------------------------------------------------------
//
//- (IBAction) selectDateRangeButtonTapped:(id)sender {
//    SurveyDateFilter *controller = [App_Delegate.mainStoryboard instantiateViewControllerWithIdentifier:@"SurveyDateFilter"];
//    controller.isShowCompareDate = NO;
//    controller.delegate = self;
//    controller.removeDateLimit = YES;
//    [self.navigationController pushViewController:controller animated:YES];
//}

//---------------------------------------------------------------

- (void) removeButtonTapped:(UIButton *)sender {
    @try {
        [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];
        
        NSInteger index = sender.tag - REMOVE_BUTTON_TAG;
        Event *event = (Event *)[self.dataArray objectAtIndex:index];
        NSString *title = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_DELETE_TITLE"];
        NSString *message = @"";
        
        if (self.surveyType == OBSERVATION) {
            message = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_DELETE_MESSAGE"];
        } else {
            message = [App_Delegate.keyMapping getKeyValue:@"KEY_CC_DELETE_MESSAGE"];
        }
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *yesAction = [UIAlertAction actionWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_YES"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            // Call service to remove observation
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            [ServiceManager callWebServiceToDeleteSingleObservation:event.eventId usingBlock:^(NSDictionary *response) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if (response) {
                    
                    // Remove event from calendar
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self removeEventFromCalendar:event];
                    });
                    
                    // Remove cell from local after service call using animation
                    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.dataArray];
                    [tempArray removeObjectAtIndex:index];
                    self.dataArray = [NSArray arrayWithArray:tempArray];
                    tempArray = nil;
                    
                    NSIndexPath *removeIndexPath = [NSIndexPath indexPathForRow:index inSection:0];
                    [self.tableView beginUpdates];
                    [self.tableView deleteRowsAtIndexPaths:@[removeIndexPath] withRowAnimation:UITableViewRowAnimationBottom];
                    [self.tableView endUpdates];
                }
            }];
        }];
        [alertController addAction:yesAction];
        
        UIAlertAction *noAction = [UIAlertAction actionWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_NO"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alertController addAction:noAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------
    
- (void) editButtonTapped:(UIButton *)sender {
    @try {
        [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];
        
        NSInteger index = sender.tag - EDIT_BUTTON_TAG;
        Event *event = [self.dataArray objectAtIndex:index];
        
        int status = event.surveyStatus.intValue;
        EVENT_TYPE eventType = OB_COMPLETED_EVENT;
        if (status == PENDING) {
            eventType = OB_PENDING_EVENT;
        }
        
        // Open container view
        UIStoryboard *observationStoryboard = [UIStoryboard storyboardWithName:KEY_OBSERVATION_STORYBOARD_NAME bundle:[NSBundle mainBundle]];
        ObserveAgentController *controller = [observationStoryboard instantiateViewControllerWithIdentifier:@"ObserveAgentController"];
        controller.eventType = eventType;
        controller.event = event;
        controller.surveyType = self.surveyType;
        [self.navigationController pushViewController:controller animated:YES];
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------
        
- (void) viewButtonTapped:(UIButton *)sender {
    @try {
        [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];
        
        NSInteger index = sender.tag - VIEW_BUTTON_TAG;
        Event *event = [self.dataArray objectAtIndex:index];
        // Open container view
        UIStoryboard *observationStoryboard = [UIStoryboard storyboardWithName:KEY_OBSERVATION_STORYBOARD_NAME bundle:[NSBundle mainBundle]];
        ObserveAgentController *controller = [observationStoryboard instantiateViewControllerWithIdentifier:@"ObserveAgentController"];
        controller.eventType = OB_COMPLETED_EVENT_VIEW;
        controller.event = event;
        controller.surveyType = self.surveyType;
        [self.navigationController pushViewController:controller animated:YES];
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WEB SERVICE methods

//---------------------------------------------------------------

- (void) callFilterWebServicesInBackground {
    
    // Show loading on filter button
    [self.filterButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [self.filterButton showLoading];
    self.filter.accessible      = [NSNumber numberWithBool:NO];
    self.filterButton.enabled   = NO;
    
    // Reset all filters
    self.filter.locationId      = [NSNumber numberWithInt:0];
    self.filter.orderBy         = KEY_ORDER_BY_VALUE;
    self.filter.sort            = KEY_SORT_BY_VALUE;
    self.filter.activeStatus    = KEY_PARAM_CONST_ACTIVE_STATUS;
    self.filter.locationName    = @"";
    
    // Call service to get locations
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSDictionary *details = [self.filter serializeFiltersGoalSettingLocationService];
    [ServiceManager callWebServiceToGetLocations:self.filter withDetails:details usingBlock:^(LocationList *locationList) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        self.locationList = locationList;
        
        //***NOTE: Add static location "All locations" to get all details
        Location *allLocations = [Location new];
        allLocations.name = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_ALL_LOCATION"];
        allLocations.locationId = [NSNumber numberWithInt:-1];
        
        NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.locationList.locationList];
        [tempArray insertObject:allLocations atIndex:0];
        self.locationList.locationList = [NSArray arrayWithArray:tempArray];
        tempArray = nil;

        if (self.locationList.locationList.count > 0) {
            // First location
            Location *location = [self.locationList.locationList objectAtIndex:0];
            self.filter.locationId = location.locationId;
            self.filter.locationName = location.name;
        }
                
        // Call service to get Observation categories
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [ServiceManager callWebServiceToGetObservationCategories:self.filter usingBlock:^(OBCategoryList *categoryList) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            self.categoryList = categoryList;
            
            //***NOTE: Add static category "All Categories" to get all details
            OBCategory *category = [OBCategory new];
            category.name = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_ALL_CATEGORIES"];
            category.categoryId = [NSNumber numberWithInt:-1];
            
            NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.categoryList.obCategoryList];
            [tempArray insertObject:category atIndex:0];
            self.categoryList.obCategoryList = [NSArray arrayWithArray:tempArray];
            tempArray = nil;
            
            if (self.categoryList.obCategoryList.count > 0) {
                // First location group
                OBCategory *category = [self.categoryList.obCategoryList objectAtIndex:0];
                self.filter.categoryId = category.categoryId;
                self.filter.categoryName = category.name;
            }
            
            // Enable filterbutton
            [self.filterButton setImage:[UIImage imageNamed:@"filter"] forState:UIControlStateNormal];
            [self.filterButton hideLoading];
            
            // Call Observation list service
            [self callWebServiceToGetObservationList];
        }];
    }];
}

//---------------------------------------------------------------

- (void) callWebServiceToGetObservationList {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSDictionary *params = [self.filter serializeFiltersForObservationListService];
    
    // Body details are static for now, it will be useful in future for pagination and searching
    NSDictionary *paginationDetails = @{
                                          @"first" : [NSNumber numberWithInteger:self.pageNo],
                                          @"total" : [NSNumber numberWithInteger:self.pageSize],
                                          @"sortBy" : @[],
                                          @"sortOrder" : @[],
                                          @"searchText" : @""
                                  };
    
    NSMutableDictionary *bodyDetails = [NSMutableDictionary dictionaryWithDictionary:params];
    [bodyDetails setObject:paginationDetails forKey:@"solrInput"];
    
    params = [NSDictionary dictionaryWithDictionary:bodyDetails];
    bodyDetails = nil;
    
    [ServiceManager callWebServiceToGetListOfObservation:params usingBlock:^(EventList *eventList) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        self.eventList = eventList;
        
        // Add Events to device calendar
        dispatch_async(dispatch_get_main_queue(), ^{
            [self storeEventsToLocalCalendar];
        });
        
        NSMutableArray *tempArray = [NSMutableArray new];
        [tempArray addObjectsFromArray:self.dataArray];
        [tempArray addObjectsFromArray:self.eventList.eventList];
        
        self.dataArray = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
        [self.tableView reloadData];
        
        if (self.dataArray.count > 0) {
            self.noRecordsView.hidden = YES;
        } else {
            self.noRecordsView.hidden = NO;
        }

        // Hide load more if you get all records
        if (self.dataArray.count == self.eventList.total.intValue) {
            self.tableView.showLoadMoreView = NO;
        } else {
            self.tableView.showLoadMoreView = YES;
        }
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) dateSettings {
    // Default date if network is not there
    NSDate *endDate = [[NSDate alloc] init];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comp = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:endDate];
    [comp setDay:1];
    NSDate *startDate = [gregorian dateFromComponents:comp];
    
    self.filter.startDate = startDate;
    self.filter.endDate = endDate;
    
    // Set dates from Calendar syncronization
    self.eventManager.startDate = startDate;
    self.eventManager.endDate = endDate;
    
    NSString *startDateString =  [self.filter.startDate toNSString:APP_DISPLAY_DATE_TIME_FORMAT];
    NSString *endDateString =  [self.filter.endDate toNSString:APP_DISPLAY_DATE_TIME_FORMAT];
    self.dateLabel.text = [NSString stringWithFormat:@"%@ ~ %@", startDateString, endDateString];
    
    [ServicePreference setObject:startDateString forKey:SURVEY_START_DATE];
    [ServicePreference setObject:endDateString forKey:SURVEY_END_DATE];
}

//---------------------------------------------------------------

- (void) createPullToRefresh {
    // Pull to refresh ----
    __weak typeof(self) wself = self;
    _refreshView = [LGRefreshView refreshViewWithScrollView:self.tableView refreshHandler:^(LGRefreshView *refreshView) {
        if (wself) {
            __strong typeof(wself) self = wself;
            
            // Call Dashboard chart service
            [self reloadDashboardDetails];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void) {
                [self.refreshView endRefreshing];
            });
        }
    }];
    _refreshView.tintColor = APP_COLOR;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

//---------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"OBDashboardCell%d%d",(int)indexPath.section, (int)indexPath.row];
    
    OBDashboardCell *cell = (OBDashboardCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[OBDashboardCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    [cell.removeButton addTarget:self action:@selector(removeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [cell.editButton addTarget:self action:@selector(editButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [cell.viewButton addTarget:self action:@selector(viewButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    // Define tags
    [cell.removeButton setTag:REMOVE_BUTTON_TAG + indexPath.row];
    [cell.editButton setTag:EDIT_BUTTON_TAG + indexPath.row];
    [cell.viewButton setTag:VIEW_BUTTON_TAG + indexPath.row];
    [cell.removeButton setHidden:NO];
    [cell.viewButton setHidden:NO];
    
    Event *event = [self.dataArray objectAtIndex:indexPath.row];
    
    if (event) {
        int status = event.surveyStatus.intValue;

        // DO NOT SHOW VIEW BUTTON IN PENDING EVENTS
        if (status == PENDING) {
            [cell.viewButton setHidden:YES];
        }
        
        //Status line color
        UIColor *color = [UIColor grayColor];
        NSString *statusText = TEXT_STATUS_COMPLETED;
        if (status == PENDING) {
            color = OB_ORANGE_COLOR;
            statusText = TEXT_STATUS_PENDING;
        } else if (status == COMPLETED) {
            color = OB_GREEN_COLOR;
            statusText = TEXT_STATUS_COMPLETED;
        } else if (status == CANCELLED) {
            color = OB_GREY_COLOR;
            statusText = TEXT_STATUS_CANCELLED;
        }
        cell.cardView.statusLabel.backgroundColor = color;
        
        // Status
        cell.cardView.statusLabel.text = statusText;
        
        // Agent
        cell.cardView.agentNameLabel.text = event.respondentName;
        
        // Performance leader
        cell.cardView.performanceLeaderLabel.text = event.surveyorName;
        
        // Observation type
        NSString *sureveyCategoryName = [Helper removeWhiteSpaceAndNewLineCharacter:event.questionnaireSurveyCategoryName];
        sureveyCategoryName = sureveyCategoryName.length > 0 ? [NSString stringWithFormat:@"(%@)", sureveyCategoryName] : @"";
        NSString *observationType = @"";
        if (event.questionnaireName.length > 0) {
            observationType = [NSString stringWithFormat:@"%@ %@", event.questionnaireName, sureveyCategoryName];
        } else {
            observationType = [NSString stringWithFormat:@"%@", sureveyCategoryName];
        }
        cell.cardView.observationTypeLabel.text = observationType;
        
        // Score
        if (event.scoreVal) {
            cell.cardView.scoreLabel.text = [NSString stringWithFormat:@"%0.2f %%", event.scoreVal.floatValue];
        } else {
            cell.cardView.scoreLabel.text = @"";
        }
        
        // location
        // Get organization name from ID.
        cell.cardView.locationLabel.text = event.tenantLocationName;
        
        // scheduled on - Start Date
        cell.cardView.scheduledOnLabel.text = [SurveyHelper getLocalDateInStringConvertFromUtcTimeStamp:event.startDate.doubleValue];
        
        // VALIDATIONS:
        // Check that user allow to edit observation
        cell.editButton.hidden = NO;
        if (self.surveyType == OBSERVATION) {
            if (!App_Delegate.userPermissions.canAccessEditObservation.boolValue) {
                cell.editButton.hidden = YES;
            }
        } else {
            if (!App_Delegate.userPermissions.canAccessEditCounterCoaching.boolValue) {
                cell.editButton.hidden = YES;
            }
        }
    }
    
    return cell;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark  Drag delegate methods

//---------------------------------------------------------------

- (void)finishLoadMore {
    [self.tableView finishLoadMore];
    self.pageNo = self.pageNo + self.pageSize;
    [self callWebServiceToGetObservationList];
}

//---------------------------------------------------------------

- (void)dragTableDidTriggerLoadMore:(UITableView *)tableView {
    //send load more request(generally network request) here        
    [self performSelector:@selector(finishLoadMore) withObject:nil afterDelay:2];
}

//---------------------------------------------------------------

- (void)dragTableLoadMoreCanceled:(UITableView *)tableView {
    //cancel load more request(generally network request) here
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(finishLoadMore) object:nil];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark OBFilterDelegate methods

//---------------------------------------------------------------

- (void) reloadDashboardDetails:(Filters *)filter {
    self.filter = filter;
    [self reloadDashboardDetails];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark DateFilter methods

//---------------------------------------------------------------

- (void) reloadDashboardDetails {
    
    NSString *startDateString = [ServicePreference objectForKey:SURVEY_START_DATE];
    NSString *endDateString = [ServicePreference objectForKey:SURVEY_END_DATE];
    
    NSDate *startDate = [NSDate dateFromNSString:startDateString withFormat:APP_DISPLAY_DATE_TIME_FORMAT];
    NSDate *endDate = [NSDate dateFromNSString:endDateString withFormat:APP_DISPLAY_DATE_TIME_FORMAT];
    
    NSString *strFilterStartDate = [startDate toNSString:SERVICE_DATE_TIME_SHORT_FORMAT];
    NSString *strFilterEndDate = [endDate toNSString:SERVICE_DATE_TIME_SHORT_FORMAT];
    
    self.filter.startDate = [NSDate dateFromNSString:strFilterStartDate withFormat:SERVICE_DATE_TIME_SHORT_FORMAT];
    self.filter.endDate = [NSDate dateFromNSString:strFilterEndDate withFormat:SERVICE_DATE_TIME_SHORT_FORMAT];
    
    // Set dates from Calendar syncronization
    self.eventManager.startDate = self.filter.startDate;
    self.eventManager.endDate = self.filter.endDate;
    
    // Update date label
    self.dateLabel.text = [NSString stringWithFormat:@"%@ ~ %@", startDateString, endDateString];
    
    // Update page number and remove old data
    self.pageNo = 0;
    self.dataArray = nil;
    self.dataArray = [NSArray new];
    
    // Call service to get observation list
    [self callWebServiceToGetObservationList];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark EKEvent & EKCalendar methods (Device calendar)

//---------------------------------------------------------------

- (void) requestAccessToEvents {
    [self.eventManager.eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (error == nil) {
            // Store the returned granted value.
            self.eventManager.eventsAccessGranted = granted;
        }
        else{
            // In case of error, just log its description to the debugger.
            DLog(@"%@", [error localizedDescription]);
        }
    }];
}

//---------------------------------------------------------------

- (void) storeEventsToLocalCalendar {
    
    // Check that user allowed to access calendar
    if (self.eventManager.eventsAccessGranted) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        // Remove all events first
        [self.eventManager removeAllEvents:self.eventList.eventList withBlock:^(BOOL isFinish) {
            // Start synchronization of events.
            [self.eventManager saveAllEvents:self.eventList.eventList withBlock:^(BOOL isFinish) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            }];
        }];
    }
}

//---------------------------------------------------------------

- (void) removeEventFromCalendar:(Event *)event {
    // Check that user allowed to access calendar
    if (self.eventManager.eventsAccessGranted) {
        [self.eventManager removeEvent:event];
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:nil];
    self.navigationItem.backBarButtonItem = backItem;
    
    // Table Settings
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 180.0;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.showLoadMoreView = YES;
    [self.tableView setDragDelegate:self refreshDatePermanentKey:@"ObservationList"];
    self.tableView.footerPullUpText = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_LOAD_MORE"];
    self.tableView.footerReleaseText = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_RELEASE_MORE"];
    self.tableView.footerLoadingText = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_LOADING"];
    
    // Pagination settings
    self.pageNo = 0;
    self.pageSize = 20;
    self.isFirstLoad = YES;
    self.isCellExpand = NO;
    _selectedIndexPath = [NSIndexPath indexPathForRow:-1 inSection:-1];

    _noRecordsView = [Helper createNoAccessView:self.tableView withMessage:[App_Delegate.keyMapping getKeyValue:@"KEY_OB_NO_SURVEY"]];

    _eventManager = [[EventManager alloc] init];
    // Request access to events.
    [self performSelector:@selector(requestAccessToEvents) withObject:nil afterDelay:0.4];
    
    // Get filters from stored
    _filter = [Filters new];
    
    // surveyEntityType = "0" | "1" : "Observation" | "Counter Coaching"
    
    if (self.surveyType == OBSERVATION) {
        self.filter.surveyEntityType = @"0"; // This parameter will be used in get observation list service
        self.filter.surveyEntityTypeString = KEY_PARAM_CONST_OBSERVATION; // This will be used in get categories list service
        self.title = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_DASHBOARD_TITLE"];
        // Check user permission to take observation
        if (!App_Delegate.userPermissions.canAccessTakeObservation.boolValue) {
            [self.navigationItem setRightBarButtonItem:nil animated:YES];
        }
    } else {
        self.filter.surveyEntityType = @"1";
        self.filter.surveyEntityTypeString = KEY_PARAM_CONST_COACHING;
        self.title = [App_Delegate.keyMapping getKeyValue:@"KEY_CC_DASHBOARD_TITLE"];
        // Check user permission to take observation
        if (!App_Delegate.userPermissions.canAccessTakeCounterCoaching.boolValue) {
            [self.navigationItem setRightBarButtonItem:nil animated:YES];
        }
    }
    
    // Create PULL-TO-REFRESH CONTROL
    [self createPullToRefresh];
    
    // Create Status array
    self.statusArray = @[
                         [App_Delegate.keyMapping getKeyValue:@"KEY_OB_STATUS_ALL"],
                         [App_Delegate.keyMapping getKeyValue:@"KEY_OB_STATUS_COMPLETED"],
                         [App_Delegate.keyMapping getKeyValue:@"KEY_OB_STATUS_PENDING"],
                         [App_Delegate.keyMapping getKeyValue:@"KEY_OB_STATUS_CANCELLED"],
                         ];
    self.filter.surveyStatus = [self.statusArray objectAtIndex:0];
    [Helper addBottomLine:self.filterView withColor:TABLE_SEPERATOR_COLOR];

    // Date settings
    [self dateSettings];
    
    // Background service call
    [self callFilterWebServicesInBackground];
    
    // Add notification observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadDashboardDetails) name:kUpdateObservationDashboard object:nil];
}

//---------------------------------------------------------------
// In a Storyboard-based application, you will often want to do a little preparation before navigation
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showObserveAgent"]) {
        // Go to Observation Storyboard
        ObserveAgentController *controller = (ObserveAgentController *)[segue destinationViewController];
        controller.eventType = OB_NEW_EVENT;
        controller.surveyType = self.surveyType;
    }

    if ([[segue identifier] isEqualToString:@"showFilterViewFromObservation"]) {
        OBFilterController *controller = (OBFilterController *)[segue destinationViewController];
        controller.filter = self.filter;
        controller.delegate = self;
        controller.locationList = self.locationList;
        controller.categoryList = self.categoryList;
        controller.statusArray = self.statusArray;
    }
    
    // Date filter
    if ([segue.identifier isEqualToString:@"showSurveyDateFilter"]) {
        SurveyDateFilter *controller = (SurveyDateFilter *)[segue destinationViewController];
        controller.isShowCompareDate = NO;
        controller.delegate = self;
        controller.removeDateLimit = YES;
    }
}

//---------------------------------------------------------------

@end
