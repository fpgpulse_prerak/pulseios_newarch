//
//  OBFilterController.h
//  IN-Gauge
//
//  Created by Mehul Patel on 14/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ServiceManager.h"

@protocol  OBFilterDelegate <NSObject>

- (void) reloadDashboardDetails:(Filters *)filter;
//- (void) updateFilterArray:(NSArray *)updatedArray forFilterType:(FilterType)filterType;

@end

@interface OBFilterController : BaseViewController

@property (nonatomic, weak) id <OBFilterDelegate>         delegate;

@property (nonatomic, strong) LocationList              *locationList;
@property (nonatomic, strong) OBCategoryList            *categoryList;
@property (nonatomic, strong) NSArray                   *statusArray;
@property (nonatomic, strong) Filters                   *filter;

@end
