//
//  OBFilterController.m
//  IN-Gauge
//
//  Created by Mehul Patel on 14/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "OBFilterController.h"
#import "FilterCell.h"
#import "FilterDetailViewController.h"
#import "AppDelegate.h"


@interface OBFilterController () <UITableViewDataSource, UITableViewDelegate, FilterDetailDelegate>

@property (nonatomic, weak) IBOutlet UITableView        *tableView;
@property (nonatomic, weak) IBOutlet UIButton           *applyButton;
@property (nonatomic, weak) IBOutlet UIButton           *clearButton;

@property (nonatomic, strong) NSArray                   *dataArray;

@end

@implementation OBFilterController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) applyButtonTapped:(id)sender {
    [self.delegate reloadDashboardDetails:self.filter];
    [self.navigationController popViewControllerAnimated:YES];
}

//---------------------------------------------------------------

- (IBAction) clearButtonTapped:(id)sender {
    
    if (self.locationList.locationList.count > 0) {
        // First location
        Location *location = [self.locationList.locationList objectAtIndex:0];
        self.filter.locationId = location.locationId;
        self.filter.locationName = location.name;
    }
    
    if (self.categoryList.obCategoryList.count > 0) {
        // First Category
        OBCategory *category = [self.categoryList.obCategoryList objectAtIndex:0];
        self.filter.categoryId = category.categoryId;
        self.filter.categoryName = category.name;
    }
    
    self.filter.surveyStatus = [self.statusArray objectAtIndex:0];
    
    [self.tableView reloadData];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WEBSERVICE methods

////---------------------------------------------------------------
//
//- (void) getTenantLocations:(void(^)(BOOL isFinish))block {
//    // Get locations
//    
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    NSDictionary *details = [self.filter serializeFiltersForLocationService];
//    [ServiceManager callWebServiceToGetLocations:self.filter withDetails:details usingBlock:^(LocationList *locationList) {
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
//        self.locationList = locationList;
//        [self.delegate updateFilterArray:self.locationList.locationList forFilterType:LocationFilter];
//        
//        self.filter.locationName = @"";
//        // Get location group
//        if (self.locationList.locationList.count > 0) {
//            
//            // First location
//            Location *location = [self.locationList.locationList objectAtIndex:0];
//            self.filter.locationId = location.locationId;
//            self.filter.locationName = location.name;
//        } else {
//            // Remove filter
//            self.filter.locationId = [NSNumber numberWithInt:0];
//        }
//        [self.tableView reloadData];
//        
//    }];
//}
//
////---------------------------------------------------------------
//
//- (void) getObservationCategory:(void(^)(BOOL isFinish))block {
//    // Get Category
//    
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    [ServiceManager callWebServiceToGetObservationCategories:self.filter usingBlock:^(OBCategoryList *categoryList) {
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
//        self.categoryList = categoryList;
//        [self.delegate updateFilterArray:self.categoryList.obCategoryList forFilterType:ObservationCategory];
//        
//        self.filter.categoryName = @"";
//        // Get location group
//        if (self.categoryList.obCategoryList.count > 0) {
//            
//            // First Category
//            OBCategory *category = [self.categoryList.obCategoryList objectAtIndex:0];
//            self.filter.categoryId = category.categoryId;
//            self.filter.categoryName = category.name;
//        } else {
//            // Remove filter
//            self.filter.categoryId = [NSNumber numberWithInt:0];
//        }
//        [self.tableView reloadData];
//        
//    }];
//}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------


//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

//---------------------------------------------------------------

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"FilterCell%d%d", (int)indexPath.section, (int)indexPath.row];
    
    FilterCell *cell = (FilterCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[FilterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.itemLabel.text = [self.dataArray objectAtIndex:indexPath.row];
    
    switch (indexPath.row) {
        case 0: {
            cell.selectedItemLabel.text = self.filter.locationName;
            break;
        }
        case 1: {
            cell.selectedItemLabel.text = self.filter.categoryName;
            break;
        }
        case 2: {
            cell.selectedItemLabel.text = self.filter.surveyStatus;
            break;
        }
        default:
            cell.selectedItemLabel.text = @"";
            break;
    }
    return cell;
}

//---------------------------------------------------------------

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FilterDetailViewController *controller = [App_Delegate.mainStoryboard instantiateViewControllerWithIdentifier:@"FilterDetailViewController"];
    controller.filter = self.filter;
    controller.delegate = self;
    
    switch (indexPath.row) {
        case 0:
            controller.itemId = LocationFilter;
            break;
        case 1:
            controller.itemId = ObservationCategory;
            break;
        case 2:
            controller.itemId = ObservationStatus;
            break;
        default:
            break;
    }
    
    controller.locationList = self.locationList;
    controller.categoryList = self.categoryList;
    controller.statusArray = self.statusArray;
    
    [self.navigationController pushViewController:controller animated:YES];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark FilterDetailView delegate methods

//---------------------------------------------------------------

- (void) reloadFilters:(FilterType)filterType withFilter:(Filters *)filter {
    self.filter = filter;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    
    self.title = [App_Delegate.keyMapping getKeyValue:@"KEY_FILTER"];
    
    // TableView settings
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    // Titles
    self.dataArray = @[
                        [App_Delegate.keyMapping getKeyValue:@"KEY_LOCATION"],
                        @"Category", //[App_Delegate.keyMapping getKeyValue:@"KEY_LOCATION_GROUP"],
                        @"Status", //[App_Delegate.keyMapping getKeyValue:@"KEY_PRODUCT"],
                        ];
    
    [self.applyButton setTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_APPLY"] forState:UIControlStateNormal];
    [self.clearButton setTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_CLEAR"] forState:UIControlStateNormal];
    self.applyButton.backgroundColor = APP_COLOR;
    self.clearButton.backgroundColor = APP_COLOR;
}

//---------------------------------------------------------------

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

//---------------------------------------------------------------

@end
