//
//  OBQuestionAnswerController.m
//  IN-Gauge
//
//  Created by Mehul Patel on 11/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "OBQuestionAnswerController.h"
#import "AnalyticsLogger.h"

@interface OBQuestionAnswerController ()

@property (nonatomic, weak) IBOutlet UIView             *cardView;
@property (nonatomic, weak) IBOutlet UIView             *sectionView;
@property (nonatomic, weak) IBOutlet UIView             *quetsionView;
@property (nonatomic, weak) IBOutlet UILabel            *sectionLabel;
@property (nonatomic, weak) IBOutlet UILabel            *questionLabel;
@property (nonatomic, weak) IBOutlet UIButton           *yesButton;
@property (nonatomic, weak) IBOutlet UIButton           *naButton;
@property (nonatomic, weak) IBOutlet UIButton           *skipButton;

@end

@implementation OBQuestionAnswerController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) yesButtonTapped:(UIButton *)sender {
    [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];
    
    [self.yesButton setBackgroundColor:APP_COLOR];
    [self.yesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.naButton setTitleColor:APP_COLOR forState:UIControlStateNormal];
    [self.naButton setBackgroundColor:[UIColor whiteColor]];

    [self.questionAnswerDelegate nextQuestion:ANSWER_YES currentQueId:self.question.questionId];
    
    //Analytics
    [AnalyticsLogger logSurveyAnswerSelectEvent:self answer:ANSWER_YES];
}

//---------------------------------------------------------------

- (IBAction) naButtonTapped:(UIButton *)sender {
    [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];

    [self.yesButton setTitleColor:APP_COLOR forState:UIControlStateNormal];
    [self.yesButton setBackgroundColor:[UIColor whiteColor]];

    [self.naButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.naButton setBackgroundColor:APP_COLOR];

    [self.questionAnswerDelegate nextQuestion:ANSWER_NA currentQueId:self.question.questionId];
}

//---------------------------------------------------------------

- (IBAction) skipButtonTapped:(UIButton *)sender {
    [Helper tapOnControlAnimation:sender forScale:0.5 forCompletionBlock:^(BOOL finished) {}];
    
    [self.yesButton setTitleColor:APP_COLOR forState:UIControlStateNormal];
    [self.yesButton setBackgroundColor:[UIColor whiteColor]];

    [self.naButton setTitleColor:APP_COLOR forState:UIControlStateNormal];
    [self.naButton setBackgroundColor:[UIColor whiteColor]];
    
    [self.questionAnswerDelegate nextQuestion:ANSWER_NO currentQueId:self.question.questionId];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) addBottomLine:(UIView *)view withColor:(UIColor *)color {
    // Add line
    UIView *lineView = [[UIView alloc] init];
    [lineView setBackgroundColor:color];
    [lineView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [view addSubview:lineView];
    
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[lineView]|" options: 0 metrics:nil views:@{@"lineView" : lineView}]];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[lineView(0.5)]|" options: 0 metrics:nil views:@{@"lineView" : lineView}]];
}

//---------------------------------------------------------------

- (void) addTopLine:(UIView *)view withColor:(UIColor *)color {
    // Add line
    UIView *lineView = [[UIView alloc] init];
    [lineView setBackgroundColor:color];
    [lineView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [view addSubview:lineView];
    
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[lineView]|" options: 0 metrics:nil views:@{@"lineView" : lineView}]];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[lineView(0.5)]" options: 0 metrics:nil views:@{@"lineView" : lineView}]];
}

//---------------------------------------------------------------

- (void) buttonSettings:(UIButton *)button {
    [button setTitleColor:APP_COLOR forState:UIControlStateNormal];
    button.backgroundColor = [UIColor clearColor];
    button.layer.cornerRadius = 18.0f;
    button.layer.masksToBounds = YES;
    button.layer.borderColor = APP_COLOR.CGColor;
    button.layer.borderWidth = 1.1f;
}
//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    
    // Drop shadow
    self.cardView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    self.cardView.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    self.cardView.layer.shadowRadius = 1;
    self.cardView.layer.shadowOpacity = 2.0f;
    
    // Button settings
    [self.yesButton setTitle:ANSWER_YES forState:UIControlStateNormal];
    [self.naButton setTitle:ANSWER_NA forState:UIControlStateNormal];
    [self.skipButton setTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_OB_ANS_SKIP"] forState:UIControlStateNormal];
    
    [self buttonSettings:self.yesButton];
    [self buttonSettings:self.naButton];
    
    self.sectionView.backgroundColor = [UIColor colorWithRed:247/255.0 green:247/255.0 blue:247/255.0 alpha:1];
    UIColor *lineColor = [UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1];
    [self addTopLine:self.sectionView withColor:lineColor];
    [self addBottomLine:self.sectionView withColor:lineColor];
    
    NSString *segmentIndex = [NSString stringWithFormat:@"%@ %@:", [App_Delegate.keyMapping getKeyValue:@"KEY_OB_QUE_SECTION"], self.segIndexString];
    NSString *segmentText = [Helper removeWhiteSpaceAndNewLineCharacter:self.sectionText];
    
    self.sectionLabel.text = [NSString stringWithFormat:@"%@ %@",segmentIndex, [Helper decodeStringForHtmlCharacters:segmentText]];
    self.questionLabel.text = [NSString stringWithFormat:@"%@. %@", self.queIndexString, [Helper decodeStringForHtmlCharacters:self.question.questionText]];

    // Show/hide NA button
    if ([self.question.type isEqualToString:@"YESNONA"]) {
        [self.naButton setHidden:NO];
    } else {
        [self.naButton setHidden:YES];
    }
    
    if ([self.answerString isEqualToString:ANSWER_YES]) {
        [self.yesButton setBackgroundColor:APP_COLOR];
        [self.yesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.naButton setTitleColor:APP_COLOR forState:UIControlStateNormal];
        [self.naButton setBackgroundColor:[UIColor whiteColor]];
        
    } else if ([self.answerString isEqualToString:ANSWER_NA]) {
        [self.yesButton setTitleColor:APP_COLOR forState:UIControlStateNormal];
        [self.yesButton setBackgroundColor:[UIColor whiteColor]];
        [self.naButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.naButton setBackgroundColor:APP_COLOR];
        
    } else if ([self.answerString isEqualToString:ANSWER_NO])  {
        [self.yesButton setTitleColor:APP_COLOR forState:UIControlStateNormal];
        [self.yesButton setBackgroundColor:[UIColor whiteColor]];
        [self.naButton setTitleColor:APP_COLOR forState:UIControlStateNormal];
        [self.naButton setBackgroundColor:[UIColor whiteColor]];
    }
    
    // Show answer text only for view events
    if (self.dataManager.eventType == OB_COMPLETED_EVENT_VIEW) {
        // Hide other buttons
        [self.yesButton setHidden:YES];
        [self.skipButton setHidden:YES];
        
        [self.naButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.naButton setBackgroundColor:APP_COLOR];
        [self.naButton setTitle:self.answerString forState:UIControlStateNormal];
        self.naButton.userInteractionEnabled = NO;
    }
}

//---------------------------------------------------------------

@end
