//
//  OBQuestionContainer.h
//  IN-Gauge
//
//  Created by Mehul Patel on 11/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "DataManager.h"

@interface OBQuestionContainer : BaseViewController

@property (nonatomic, strong) Filters                   *filter;
@property (nonatomic, strong) DataManager               *dataManager;

@end
