//
//  OBQuestionController.m
//  IN-Gauge
//
//  Created by Mehul Patel on 11/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "OBQuestionContainer.h"
#import "OBQuestionAnswerController.h"
#import "DataManager.h"
#import "ObservationTabController.h"

@interface OBQuestionContainer () <UIPageViewControllerDataSource, QuestionAnswerDelegate>

@property (strong, nonatomic) UIPageViewController      *pageController;
@property (nonatomic, strong) UILabel                   *scoreLabel;

@property (nonatomic, strong) UIView                    *noRecordsView;
@property (nonatomic, strong) Questionnaire             *questionnaire;
@property (nonatomic, strong) NSArray                   *controllerArray;
@property (nonatomic, strong) NSMutableDictionary       *answerList;
@property (nonatomic, strong) NSString                  *agentScoreTitle;
@property (nonatomic) NSInteger                         segementCount;
@property (nonatomic) NSInteger                         pageCount;

@end

@implementation OBQuestionContainer

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------


//---------------------------------------------------------------

#pragma mark
#pragma mark WEBSERVICE methods

//---------------------------------------------------------------

- (void) callWebserviceToGetQuestionnaire {
    // Get questionnaire
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ServiceManager callWebServiceToGetQuestionnaire:self.filter usingBlock:^(Questionnaire *questionnaire) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (questionnaire == nil || questionnaire.questionSections.count == 0) {
            [Helper showAlertWithTitle:@"" message:[App_Delegate.keyMapping getKeyValue:@"KEY_OB_NO_QUES"] forController:self];
            [self.navigationController popViewControllerAnimated:YES];
        }
        self.questionnaire = questionnaire;
        [self updateUI];
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) updateUI {
    // Show information alert
    [SurveyHelper showAlert:self.dataManager.eventType forController:self];
    
    // Questionnaire
    if (self.questionnaire) {
        //             Check for question segments
        if (self.questionnaire.questionSections && self.questionnaire.questionSections.count > 0) {
            self.dataManager.sectionArray = self.questionnaire.questionSections;
        } else {
            self.dataManager.sectionArray = nil;
        }
    }
    
    if (self.answerList.count == 0) {
        self.pageCount = 0;
        for (QuestionSection *section in self.questionnaire.questionSections) {
            if (section && section.questions.count > 0) {
                self.pageCount = self.pageCount + section.questions.count;
                
                for (Question *question in section.questions) {
                    NSString *questionIdStringValue = question.questionId.stringValue;
                    if (questionIdStringValue) {
                        
                        if (self.dataManager.eventType == OB_NEW_EVENT || self.dataManager.eventType == OB_PENDING_EVENT) {
                            // Set 'No' for answers for new event
                            [self.answerList setObject:ANSWER_NO forKey:questionIdStringValue];
                        } else {
                            QuestionAnswer *queAnswer = [self.dataManager.event getQuestionAnswer:question.questionId];
                            if (queAnswer) {
                             [self.answerList setObject:queAnswer.answer forKey:questionIdStringValue];
                            } else {
                                [self.answerList setObject:ANSWER_NO forKey:questionIdStringValue];
                            }
                        }
                        self.dataManager.answerList = [NSDictionary dictionaryWithDictionary:self.answerList];
                    }
                }
            }
        }
    }
    // Calculate score
    [self.dataManager calculateAgentScore];
    [self initialSetup];
}

//---------------------------------------------------------------

- (void) initialSetup {
    @try {
        // Check for questions
        self.noRecordsView.hidden = YES;
        if (self.pageCount == 0) {
            for (id view in self.view.subviews) {
                [(UIView *)view removeFromSuperview];
            }
            
            [self.pageController removeFromParentViewController];
            [[self.pageController view] removeFromSuperview];
            self.pageController.dataSource = nil;
            self.pageController = nil;
            self.noRecordsView.hidden = NO;
            return;
        }
        
        NSMutableArray *tempArray = [NSMutableArray new];
        // Create controller array:
        for (int i = 0; i < self.pageCount; i++) {
            [tempArray addObject:[self createController:i]];
        }
        self.controllerArray = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
        
        _pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
        self.pageController.dataSource = self;
        self.pageController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);

        OBQuestionAnswerController *initialViewController = [self viewControllerAtIndex:0];
        if (initialViewController) {
            NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
            
            __weak UIPageViewController* pvcw = self.pageController;
            [self.pageController setViewControllers:viewControllers
                                          direction:UIPageViewControllerNavigationDirectionForward
                                           animated:NO completion:^(BOOL finished) {
                                               UIPageViewController* pvcs = pvcw;
                                               if (!pvcs) return;
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   [pvcs setViewControllers:viewControllers
                                                                  direction:UIPageViewControllerNavigationDirectionForward
                                                                   animated:NO completion:nil];
                                               });
                                           }];
            

            [self addChildViewController:self.pageController];
            [[self view] addSubview:[self.pageController view]];
            [self.pageController didMoveToParentViewController:self];
        }
    }
    @catch (NSException *exception) {
        DLog(@"exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (OBQuestionAnswerController *) createController:(NSInteger)index {
    UIStoryboard *observationStoryboard = [UIStoryboard storyboardWithName:KEY_OBSERVATION_STORYBOARD_NAME bundle:[NSBundle mainBundle]];
    OBQuestionAnswerController *childViewController = (OBQuestionAnswerController *)[observationStoryboard instantiateViewControllerWithIdentifier:@"OBQuestionAnswerController"];
    childViewController.questionAnswerDelegate = self;
    childViewController.index = index;
    childViewController.dataManager = self.dataManager;
    return childViewController;
}

//---------------------------------------------------------------

- (OBQuestionAnswerController *)viewControllerAtIndex:(NSUInteger)index {
    @try {
        
        if (self.controllerArray.count > 0) {
            
            // Question Details view controller
            OBQuestionAnswerController *childViewController = [self.controllerArray objectAtIndex:index];
            childViewController.index = index;
            childViewController.dataManager = self.dataManager;
            
            // Get Question text for current index
            NSDictionary *details = [self getQuestionFromIndex:index];
            QuestionSection *section = [details objectForKey:@"section"];
            Question *question = [details objectForKey:@"question"];
            
            // Create segment and question index strings
            childViewController.segIndexString = [[details objectForKey:@"segmentIndex"] stringValue];
            childViewController.queIndexString = [[details objectForKey:@"questionIndex"] stringValue];
            
            // Pass segment text and question text to details view controller
            childViewController.sectionText = section.name;
            childViewController.question = question;
            childViewController.dataManager = self.dataManager;
            
            // Get answer string for current question
            NSString *answerString = @"";
            if ([self.answerList objectForKey:question.questionId.stringValue]) {
                answerString = [self.answerList objectForKey:question.questionId.stringValue];
            }

            if ([answerString isEqualToString:ANSWER_NO]) {
                answerString = @"−";
            }
            childViewController.answerString = answerString;
            return childViewController;
        }
    }
    @catch (NSException *exception) {
        DLog(@"exception :%@",exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (NSDictionary *) getQuestionFromIndex:(NSInteger) index {
    NSInteger count = 0;
    NSInteger sectionIndex = 1;
    for (QuestionSection *section in self.questionnaire.questionSections) {
        
        count = count + section.questions.count;
        if (section.questions.count == 0) {
            sectionIndex = sectionIndex - 1;
        }
        if (index < count) {
            NSInteger difference = count - index;
            NSInteger validIndex = section.questions.count - difference;
            Question *question = [section.questions objectAtIndex:validIndex];
            return @{@"section" : section, @"question" : question, @"segmentIndex" : [NSNumber numberWithInteger:sectionIndex], @"questionIndex" : [NSNumber numberWithInteger:validIndex+1]};
        }
        sectionIndex++;
    }
    return nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UIPageViewController delegate methods

//---------------------------------------------------------------

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    // [self.nextButton setEnabled:YES];
    
    OBQuestionAnswerController *detailsViewController = (OBQuestionAnswerController *)viewController;
    NSUInteger index = [detailsViewController index];
    if (index == 0) {
        return nil;
    }
    
    // Decrease the index by 1 to return
    index--;
    return [self viewControllerAtIndex:index];
    
}

//---------------------------------------------------------------

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    OBQuestionAnswerController *detailsViewController = (OBQuestionAnswerController *)viewController;
    
    NSUInteger index = [detailsViewController index];
    index++;
    if (index == self.pageCount) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
    
}

//---------------------------------------------------------------

#pragma mark
#pragma mark QuestionAnswerDelegate methods

//---------------------------------------------------------------

- (void) nextQuestion:(NSString *)answer currentQueId:(NSNumber *)questionId {
    
    [self.answerList setObject:answer forKey:questionId.stringValue];
    // Calculate score
    self.dataManager.answerList = [NSDictionary dictionaryWithDictionary:self.answerList];
    [self.dataManager calculateAgentScore];
    
    OBQuestionAnswerController *viewController = [self.pageController.viewControllers lastObject];
    NSInteger currentIndex = [self.controllerArray indexOfObject:viewController];
    currentIndex++;
    if (currentIndex == self.pageCount) {
        //        [self.delegate moveSliderToCoachingScreen];
        
        // Switch tab as this is last question
        [self.tabBarController setSelectedIndex:1];        
        return;
    }
    
    OBQuestionAnswerController *initialViewController = [self viewControllerAtIndex:currentIndex];
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
    __weak UIPageViewController* pvcw = self.pageController;
    [self.pageController setViewControllers:viewControllers
                                  direction:UIPageViewControllerNavigationDirectionForward
                                   animated:YES completion:^(BOOL finished) {
                                       UIPageViewController* pvcs = pvcw;
                                       if (!pvcs) return;
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           [pvcs setViewControllers:viewControllers
                                                          direction:UIPageViewControllerNavigationDirectionForward
                                                           animated:YES completion:nil];
                                       });
                                   }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Notification methods

//---------------------------------------------------------------

- (void) updateScoreLabel {
    self.scoreLabel.text = [NSString stringWithFormat:@"%@: %@%%", self.agentScoreTitle, [Helper getDecimalWithRemovingExtraZero:self.dataManager.event.scoreVal]];
    
    // Update tabbar datamanager
    ObservationTabController *tabBar = (ObservationTabController*)self.parentViewController;
    tabBar.dataManager = self.dataManager;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    self.pageCount = 0;
    
    _answerList = [NSMutableDictionary new];
    
    _noRecordsView = [Helper createNoAccessView:self.view withMessage:[App_Delegate.keyMapping getKeyValue:@"KEY_OB_NO_QUES"]];
    self.noRecordsView.hidden = YES;
    
    if (self.dataManager.surveyType == OBSERVATION) {
        self.agentScoreTitle = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_AGENT_SCORE"];
    } else {
        self.agentScoreTitle = [App_Delegate.keyMapping getKeyValue:@"KEY_CC_TEAM_MEMBER_SCORE"];
    }
    
    // Show agent label at right-top
    _scoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 20)];
    self.scoreLabel.font = [UIFont fontWithName:AVENIR_FONT size:12];
    self.scoreLabel.textColor = [UIColor whiteColor];
    self.scoreLabel.textAlignment = NSTextAlignmentRight;
    self.scoreLabel.text = [NSString stringWithFormat:@"%@: 0%%", self.agentScoreTitle];
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:self.scoreLabel];
    self.tabBarController.navigationItem.rightBarButtonItem = rightButton;
    
    // Add notification to get actual score
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateScoreLabel) name:kNotificationUpdateScoreOnCoaching object:nil];
    
    // call webservice
    if (self.dataManager.eventType == OB_NEW_EVENT || self.dataManager.eventType == OB_PENDING_EVENT) {
        [self callWebserviceToGetQuestionnaire];
    } else {
        _questionnaire = [Questionnaire new];
        self.questionnaire.questionSections = self.dataManager.event.questionnaireQuestionSections;
        [self updateUI];
    }
}

//---------------------------------------------------------------

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:self.scoreLabel];
    self.tabBarController.navigationItem.rightBarButtonItem = rightButton;
}

//---------------------------------------------------------------

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.tabBarController.navigationItem.rightBarButtonItem = nil;
}

//---------------------------------------------------------------

@end
