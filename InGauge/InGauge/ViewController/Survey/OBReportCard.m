//
//  OBDashboardCell.h
//  IN-Gauge
//
//  Created by Mehul Patel on 14/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "OBReportCard.h"

#define PROFILE_IMAGE_HEIGHT        30
#define LOCATION_IMAGE_HEIGHT       15
#define SCHEDULED_IMAGE_HEIGHT      15

#define AGENT_LABEL_WIDTH           0.72
#define AGENT_LABEL_HEIGHT          0.15

#define PL_LABEL_WIDTH              0.72
#define PL_LABEL_HEIGHT             0.1

#define OB_TYPE_LABEL_WIDTH         0.95
#define OB_TYPE_LABEL_HEIGHT        0.1

#define CATEGORY_LABEL_WIDTH        0.2
#define CATEGORY_LABEL_HEIGHT       0.1

#define LOCATION_LABEL_WIDTH        0.42
#define LOCATION_LABEL_HEIGHT       0.1

#define SCHEDULED_LABEL_WIDTH       0.40
#define SCHEDULED_LABEL_HEIGHT      0.1

@interface OBReportCard()

@property (nonatomic) CGFloat   radius;

@end

@implementation OBReportCard

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc {
    // Remove delegates, mutable array and observers here!
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) createControls {
        
    _locationImageView = [[UIImageView alloc] init];
    [self.locationImageView setBackgroundColor:[UIColor clearColor]];
    [self.locationImageView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.locationImageView setImage:[UIImage imageNamed:@"location"]];
    [self addSubview:self.locationImageView];
    
    _timerImageView = [[UIImageView alloc] init];
    [self.timerImageView setBackgroundColor:[UIColor clearColor]];
    [self.timerImageView setImage:[UIImage imageNamed:@"clock"]];
    [self.timerImageView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:self.timerImageView];
    
    // Create labels

    // Status label
    _statusLabel = [[UILabel alloc] init];
    self.statusLabel.font = [UIFont fontWithName:AVENIR_FONT_HEAVY size:7];
    self.statusLabel.textColor = [UIColor whiteColor];
    self.statusLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.statusLabel.textAlignment = NSTextAlignmentCenter;
    [self.statusLabel setBackgroundColor:[UIColor clearColor]];
    [self.statusLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:self.statusLabel];
    
    self.statusLabel.layer.masksToBounds = YES;
    self.statusLabel.layer.cornerRadius = 6.0;
    
    // Agent label
    _agentNameLabel = [[UILabel alloc] init];
    self.agentNameLabel.font = [UIFont fontWithName:AVENIR_FONT size:16];
    self.agentNameLabel.textColor = [UIColor blackColor];
    self.agentNameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.agentNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.agentNameLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:self.agentNameLabel];
    
    UIColor *sublabelColor = [UIColor blackColor];
    // Performance Leader label
    _performanceLeaderLabel = [[UILabel alloc] init];
    self.performanceLeaderLabel.font = [UIFont fontWithName:AVENIR_FONT size:12];
    self.performanceLeaderLabel.textColor = sublabelColor;
    [self.performanceLeaderLabel setNumberOfLines:2];
    self.performanceLeaderLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.performanceLeaderLabel.textAlignment = NSTextAlignmentLeft;
    [self.performanceLeaderLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:self.performanceLeaderLabel];
    
    // observationTypeLabel
    _observationTypeLabel = [[UILabel alloc] init];
    self.observationTypeLabel.font = [UIFont fontWithName:AVENIR_FONT size:12];
    self.observationTypeLabel.textColor = sublabelColor;
    self.observationTypeLabel.numberOfLines = 2;
    self.observationTypeLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.observationTypeLabel.textAlignment = NSTextAlignmentLeft;
    [self.observationTypeLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:self.observationTypeLabel];
    
    // Score label
    _scoreLabel = [[UILabel alloc] init];
    self.scoreLabel.font = [UIFont fontWithName:AVENIR_FONT size:12];
    self.scoreLabel.textColor = sublabelColor;
    self.scoreLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.scoreLabel.textAlignment = NSTextAlignmentRight;
    [self.scoreLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:self.scoreLabel];
    
    // Seperation line label
    _seperationLine = [[UILabel alloc] init];
    self.seperationLine.font = [UIFont fontWithName:AVENIR_FONT size:12];
    self.seperationLine.backgroundColor = TABLE_SEPERATOR_COLOR;
    self.seperationLine.lineBreakMode = NSLineBreakByWordWrapping;
    self.seperationLine.textAlignment = NSTextAlignmentRight;
    [self.seperationLine setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:self.seperationLine];
    
    // locationLabel
    _locationLabel = [[UILabel alloc] init];
    self.locationLabel.font = [UIFont fontWithName:AVENIR_FONT size:9];
    self.locationLabel.textColor = sublabelColor;
    self.locationLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.locationLabel.textAlignment = NSTextAlignmentLeft;
    [self.locationLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:self.locationLabel];
    
    // scheduledOnLabel
    _scheduledOnLabel = [[UILabel alloc] init];
    self.scheduledOnLabel.font = [UIFont fontWithName:AVENIR_FONT size:9];
    self.scheduledOnLabel.textColor = sublabelColor;
    self.scheduledOnLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.scheduledOnLabel.textAlignment = NSTextAlignmentLeft;
    [self.scheduledOnLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:self.scheduledOnLabel];
    
    self.statusLabel.text = @"COMPLETED";
    self.agentNameLabel.text = @"Annie Romero";
    self.performanceLeaderLabel.text = @"Cherie Graefenstein";
    self.observationTypeLabel.text = @"Observation (Worldwide)";
    self.locationLabel.text = @"Hilton Worldwide";
    self.scheduledOnLabel.text = @"10-07-2017 04:50:05 PM";
    self.scoreLabel.text = @"63.64%";
    
    self.statusLabel.backgroundColor = OB_GREEN_COLOR;
    
//    [self.statusLabel setBackgroundColor:[UIColor orangeColor]];
//    [self.agentNameLabel setBackgroundColor:[UIColor grayColor]];
//    [self.performanceLeaderLabel setBackgroundColor:[UIColor yellowColor]];
//    [self.observationTypeLabel setBackgroundColor:[UIColor blueColor]];
//    [self.locationLabel setBackgroundColor:[UIColor redColor]];
//    [self.scheduledOnLabel setBackgroundColor:[UIColor magentaColor]];
//    [self.scoreLabel setBackgroundColor:[UIColor orangeColor]];

    [self createViewConstraints];
}

//---------------------------------------------------------------

- (void) createViewConstraints {
    // Constraints
    NSDictionary *metrics = @{
                                @"profileImageHeight": [NSNumber numberWithFloat:PROFILE_IMAGE_HEIGHT],
                                @"locationImageHeight": [NSNumber numberWithFloat:LOCATION_IMAGE_HEIGHT],
                                @"scheduledImageHeight": [NSNumber numberWithFloat:SCHEDULED_IMAGE_HEIGHT],
                            };
    
    NSDictionary *views = @{
                                @"statusLabel" : self.statusLabel,
                                @"agentNameLabel" : self.agentNameLabel,
                                @"performanceLeaderLabel" : self.performanceLeaderLabel,
                                @"observationTypeLabel" : self.observationTypeLabel,
                                @"locationLabel" : self.locationLabel,
                                @"scheduledOnLabel" : self.scheduledOnLabel,
                                @"scoreLabel" : self.scoreLabel,
                                @"seperationLine" : self.seperationLine,
                                @"locationImageView" : self.locationImageView,
                                @"timerImageView" : self.timerImageView
                            };
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[statusLabel(50)]" options:0 metrics:metrics views:views]];

    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[performanceLeaderLabel]" options:0 metrics:metrics views:views]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[seperationLine]-10-|" options:0 metrics:metrics views:views]];

    
    //VERTICAL ALIGN: All left side controls
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-15-[statusLabel(12)]-10-[agentNameLabel]-0-[performanceLeaderLabel]-0-[observationTypeLabel]-5-[seperationLine(0.5)]" options:0 metrics:metrics views:views]];

    //HORIZONTAL ALIGN: [from left side with superview - "|" ]: Agent label  & score
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[agentNameLabel]-3-[scoreLabel]" options:0 metrics:metrics views:views]];
    
    //VERTICAL ALIGN: All right side controls
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-40-[scoreLabel]" options:0 metrics:metrics views:views]];
    
    //HORIZONTAL ALIGN: Observation type
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[observationTypeLabel]" options:0 metrics:metrics views:views]];
    
    //VERTICAL ALIGN: Seperation line & location image
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[seperationLine(0.5)]-8-[locationImageView(locationImageHeight)]" options:0 metrics:metrics views:views]];

    //VERTICAL ALIGN: Seperation line & scheduled image
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[seperationLine(0.5)]-8-[timerImageView(scheduledImageHeight)]" options:0 metrics:metrics views:views]];
    
    //VERTICAL ALIGN: Seperation line & location label
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[seperationLine(0.5)]-7-[locationLabel]" options:0 metrics:metrics views:views]];

    //VERTICAL ALIGN: Seperation line & scheduled label
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[seperationLine(0.5)]-7-[scheduledOnLabel]" options:0 metrics:metrics views:views]];

    //HORIZONTAL ALIGN: Location image & location label
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[locationImageView(locationImageHeight)]-5-[locationLabel]-5-[timerImageView(scheduledImageHeight)]-5-[scheduledOnLabel]" options:0 metrics:metrics views:views]];
    
    // Width constraints
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.agentNameLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:AGENT_LABEL_WIDTH constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.performanceLeaderLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:PL_LABEL_WIDTH constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.observationTypeLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:OB_TYPE_LABEL_WIDTH constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.scoreLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:CATEGORY_LABEL_WIDTH constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.locationLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:LOCATION_LABEL_WIDTH constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.scheduledOnLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:SCHEDULED_LABEL_WIDTH constant:0]];

    // Height constraints
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.agentNameLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeHeight multiplier:AGENT_LABEL_HEIGHT constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.performanceLeaderLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeHeight multiplier:PL_LABEL_HEIGHT constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.observationTypeLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeHeight multiplier:OB_TYPE_LABEL_HEIGHT constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.scoreLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeHeight multiplier:CATEGORY_LABEL_HEIGHT constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.locationLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeHeight multiplier:LOCATION_LABEL_HEIGHT constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.scheduledOnLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeHeight multiplier:SCHEDULED_LABEL_HEIGHT constant:0]];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Init methods

//---------------------------------------------------------------

- (instancetype) init {
    self = [super init];
    return self;
}

//---------------------------------------------------------------

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    self.frame = frame;
    [self createControls];
    return self;
}

//---------------------------------------------------------------

- (void) layoutSubviews {
    [super layoutSubviews];
}

//---------------------------------------------------------------

@end
