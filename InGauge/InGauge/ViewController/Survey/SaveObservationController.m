//
//  SaveObservationController.m
//  IN-Gauge
//
//  Created by Mehul Patel on 12/07/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "SaveObservationController.h"
#import "AHTagTableViewCell.h"
#import "MultiSelectionController.h"
#import "ObservationTabController.h"
#import "ObserveAgentController.h"
#import "OBDashboardController.h"
#import "MoreViewController.h"
#import "AnalyticsLogger.h"

@interface SaveObservationController () <MultiSelectionDelegate, AHTagCellDelegate, UITextViewDelegate>

@property (nonatomic, weak) IBOutlet UILabel            *scoreTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel            *scoreValueLabel;
@property (nonatomic, weak) IBOutlet UILabel            *jobTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel            *areaLabel;
@property (nonatomic, weak) IBOutlet UILabel            *publishLabel;
@property (nonatomic, weak) IBOutlet UILabel            *mailLabel;
@property (nonatomic, weak) IBOutlet UISwitch           *publishSwitch;
@property (nonatomic, weak) IBOutlet UISwitch           *mailSwitch;

@property (nonatomic, weak) IBOutlet UITextView         *areaTextView;
@property (nonatomic, weak) IBOutlet UIButton           *saveButton;
@property (nonatomic, weak) IBOutlet UIImageView        *arrowImage;

@property (nonatomic, strong) GreatJobList              *jobList;
@property (nonatomic, strong) NSArray                   *selectedJobs;
@property (nonatomic, strong) NSArray                   *multiSelection;

@end

@implementation SaveObservationController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
    self.areaTextView.delegate = nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) saveButtonTapped:(UIButton *)saveButton {
    // Resign textView
    [self.areaTextView resignFirstResponder];

    // Perform Any validation here...
    
    // Save event, it can be new or edited
    [self saveEvent];
}

//---------------------------------------------------------------

- (IBAction) publishSwitchValueChanged:(UISwitch *)sender {
    // Update value
    self.dataManager.event.donotDisplayToAgent = [NSNumber numberWithBool:!sender.isOn];
}

//---------------------------------------------------------------

- (IBAction) mailSwitchValueChanged:(UISwitch *)sender {
    // Update value
    self.dataManager.event.donotMailToAgent = [NSNumber numberWithBool:!sender.isOn];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WEBSERVICE methods

//---------------------------------------------------------------

- (void) callWebServiceToGetGreatJobOn {
    // Get locations
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ServiceManager callWebServiceToGetGreatJobOn:^(GreatJobList *jobList) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        self.jobList = jobList;
        [self.tableView reloadData];
    }];
}

//---------------------------------------------------------------

- (void) callWebServiceToCreateSaveEvent {

    [MBProgressHUD showHUDAddedTo:App_Delegate.window animated:YES];
    [ServiceManager callWebServiceSaveEvent:self.dataManager.event usingBlock:^(NSString *errorMessage,NSDictionary *details) {
        [MBProgressHUD hideHUDForView:App_Delegate.window animated:YES];
        @try {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *alertTitle = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_SAVE_ERROR_TITLE"];
                NSString *alertMessage = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_SAVE_ERROR_MSG"];

                if (self.dataManager.surveyType == OBSERVATION) {
                    alertTitle = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_SAVE_ERROR_TITLE"];
                    alertMessage = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_SAVE_ERROR_MSG"];
                } else {
                    alertTitle = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_SAVE_ERROR_TITLE"];
                    alertMessage = [App_Delegate.keyMapping getKeyValue:@"KEY_CC_SAVE_ERROR_MSG"];
                }
                
                NSNumber *statusCode = nil;
                if ([details objectForKey:@"statusCode"]) {
                    
                    statusCode = [details objectForKey:@"statusCode"];
                    if (statusCode.integerValue == 200) {
                        if (self.dataManager.surveyType == OBSERVATION) {
                            alertTitle = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_SAVE_SUCCESS_TITLE"];
                            alertMessage = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_SAVE_SUCCESS_MSG"];
                        } else {
                            alertTitle = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_SAVE_SUCCESS_TITLE"];
                            alertMessage = [App_Delegate.keyMapping getKeyValue:@"KEY_CC_SAVE_SUCCESS_MSG"];
                        }
                        
                        // Post notification - to reload dashboard details if any
                        if ([NSNotification notificationWithName:kUpdateObservationDashboard object:nil]) {
                            [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateObservationDashboard object:nil];
                        }
                    } else {
                        alertTitle = alertTitle;
                        alertMessage = errorMessage;
                    }
                }
                
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertTitle message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_TITLE_OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    if (statusCode.integerValue == 200) {
                        [self backToRootViewController];
                    }
                }];
                
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
           });
                           
        } @catch (NSException *exception) {
            DLog(@"Exception :%@",exception.debugDescription);
        }
        [self.tableView reloadData];
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void)configureCell:(id)object atIndexPath:(NSIndexPath *)indexPath {
    if (![object isKindOfClass:[AHTagTableViewCell class]]) {
        return;
    }
    AHTagTableViewCell *cell = (AHTagTableViewCell *)object;
    cell.delegate = self;
    AHTag *tag1 = [AHTag new];
    tag1.title = @"Tag1";
    tag1.color = [UIColor colorWithRed:217/255.0 green:0/255.0 blue:18/255.0 alpha:1];
    tag1.enabled = [NSNumber numberWithBool:YES];
    
    AHTag *tag2 = [AHTag new];
    tag2.title = @"Tag2";
    tag2.color = [UIColor colorWithRed:217/255.0 green:0/255.0 blue:18/255.0 alpha:1];
    tag2.enabled = [NSNumber numberWithBool:YES];
//    cell.label.tags = @[tag1, tag2, tag1, tag2, tag1, tag2, tag1, tag2, tag1, tag2, tag1, tag2, tag1, tag2, tag1, tag2];
    cell.label.tags = self.selectedJobs;
    
    // Hide remove button if event is view only
    if (self.dataManager.eventType == OB_COMPLETED_EVENT_VIEW) {
        cell.label.isShow = NO;
    } else {
        cell.label.isShow = YES;
    }
}

//---------------------------------------------------------------

- (AHTag *) createTag:(GreatJob *)job {
    AHTag *tag = [AHTag new];
    tag.tagId = job.greatJobId;
    tag.title = job.name;
    tag.color = [UIColor colorWithRed:50/255.0 green:143/255.0 blue:149/255.0 alpha:1];
    tag.enabled = [NSNumber numberWithBool:YES];
    return tag;
}

//---------------------------------------------------------------

- (void) createAvailableTags {
    
    NSMutableArray *tempArray = [NSMutableArray new];
    NSMutableArray *selectionArray = [NSMutableArray new];
    
    for (GreatJob *model in self.dataManager.event.strengths) {
        [selectionArray addObject:model];
        AHTag *tag = [self createTag:model];
        [tempArray addObject:tag];
    }
    
    self.selectedJobs = [NSArray arrayWithArray:tempArray];
    tempArray = nil;
    self.multiSelection = [NSArray arrayWithArray:selectionArray];
    selectionArray = nil;
}

//---------------------------------------------------------------

- (void) saveEvent {
    // Tenant Location Id
    self.dataManager.event.tenantLocationId = self.filter.locationId;
    
    // Respondent Id - Agent
    // Respondent Id - Team Member [Counter coaching]
    self.dataManager.event.respondentId = self.filter.agentId;
    
    // Surveyor Id - Performance leader
    // Surveyor Id - Coach [Counter coaching]
    self.dataManager.event.surveyorId = self.filter.performanceLeaderId;
    
    // Questionnaire Id - Observation Type Id
    self.dataManager.event.questionnaireId = self.filter.obTypeId;
    
    // OneToOneIds Array
    if (self.selectedJobs && self.selectedJobs.count > 0) {
        
        NSMutableArray *tempArray = [NSMutableArray new];
        for (AHTag *tag in self.selectedJobs) {
            [tempArray addObject:tag.tagId];
        }
        NSArray *oneToOneId = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
        self.dataManager.event.oneToOneIds = oneToOneId ? oneToOneId : [NSArray new];
    } else {
        [Helper showAlertWithTitle:@"" message:[App_Delegate.keyMapping getKeyValue:@"KEY_OB_SELECT_GREAT_JOB"] forController:self];
        return;
    }
    
    // Note
    NSString *note = [Helper removeWhiteSpaceAndNewLineCharacter:self.areaTextView.text];
    if (note && note.length > 0) {
        self.dataManager.event.note = note;
    }
    
    // Do not Display agent - Only for counter coaching
    self.dataManager.event.donotDisplayToAgent = [NSNumber numberWithBool:NO];
    
    // Do not Mail agent - Only for counter coaching
    self.dataManager.event.donotMailToAgent = [NSNumber numberWithBool:NO];
    
    // For edit event and coaching
    if (self.dataManager.surveyType == COACHING) {
        self.dataManager.event.donotDisplayToAgent = [NSNumber numberWithBool:!self.publishSwitch.isOn];
        self.dataManager.event.donotMailToAgent = [NSNumber numberWithBool:!self.mailSwitch.isOn];
    }
    
    // Start Date in interval
    //** It is already set in 'ObserveAgentController.m' class
    
    // Agent Score
    //*** It is already calculated */
    
    // Question and Answer
    NSMutableArray *tempArray = [NSMutableArray new];
    for (NSString *key in self.dataManager.answerList.allKeys) {
        QuestionAnswer *model = [QuestionAnswer new];
        model.questionId = [NSNumber numberWithInt:key.intValue];
        model.answer = [self.dataManager.answerList objectForKey:key];
        NSDictionary *details = [model searialize];
        [tempArray addObject:details];
    }
    self.dataManager.event.questionAnswer = [NSArray arrayWithArray:tempArray];
    tempArray = nil;
    
    // MapTo
    self.dataManager.event.mapTo = KEY_PARAM_CONST_SURVEY;
    
    // Symptom Text - Opportunities
    if (self.dataManager.symptomArray.count > 0) {
        self.dataManager.event.symptomText = [self createOpportunitiesString:self.dataManager.symptomArray];
    } else {
        self.dataManager.event.symptomText = @"";
    }
    
    // Strength Text
    if (self.dataManager.strengthArray.count > 0) {
        self.dataManager.event.strengthText = [self createOpportunitiesString:self.dataManager.strengthArray];
    } else {
        self.dataManager.event.strengthText = @"";
    }
    
    // Industry Id
    self.dataManager.event.industryId = App_Delegate.appfilter.industryId;
    
    // Survey Type - It is already set from pervious class
    self.dataManager.event.surveyEntityType = self.filter.surveyEntityTypeString;
    
    // Survey Status
    self.dataManager.event.surveyStatus = KEY_PARAM_CONST_COMPLETED;
    
    // Active Status
    self.dataManager.event.activeStatus = KEY_PARAM_CONST_ACTIVE_STATUS;
    
    // End Date
    if (!self.dataManager.event.endDate) {
        // End date will only not available for new event or pending event
        long long utcTimeInMilliSeconds = [Helper utcTimeIntervalForDate:[NSDate date]];
        self.dataManager.event.endDate = [NSString stringWithFormat:@"%lld", utcTimeInMilliSeconds];;
    }
    
    // Call webservice
    [self callWebServiceToCreateSaveEvent];
    
    // Analytics
    [AnalyticsLogger logSurveySaveEvent:self];
}

//---------------------------------------------------------------

- (NSString *) createOpportunitiesString:(NSArray *)array {
    // Symptomtext - Opportunities
    NSMutableString *appendString = [NSMutableString new];
    for (NSString *string in array) {
        if (string.length > 0) {
            [appendString appendString:string];
            [appendString appendString:@" &&& "];
        }
    }
    NSString *truncatedString = @"";
    NSString *fullString = [NSString stringWithString:appendString];
    if (fullString.length > 0) {
        truncatedString = [fullString substringToIndex:[fullString length] - 5];
    }
    appendString = nil;
    return truncatedString;
}

//---------------------------------------------------------------

- (void) backToRootViewController {
    NSArray *array = [self.parentViewController.navigationController viewControllers];
    BaseViewController *baseController = nil;
    
    for (id controller in array) {
        // Check that Dashboard is in list?
        if ([controller isKindOfClass:[OBDashboardController class]]) {
            baseController = [array objectAtIndex:1];
            break;
        }
        
        if ([controller isKindOfClass:[ObserveAgentController class]]) {
            baseController = [array objectAtIndex:0];
            break;
        }
    }
    if (baseController) {
        [self.navigationController popToViewController:baseController animated:YES];
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if (self.dataManager.surveyType == OBSERVATION) {
        if (indexPath.row == 3 || indexPath.row == 4) {
            return 0;
        }
    }
    return UITableViewAutomaticDimension;
}

//---------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    if (self.dataManager.eventType == OB_COMPLETED_EVENT_VIEW) {
        cell.userInteractionEnabled = NO;
    }
    switch (indexPath.row) {
        case 2:
            [self configureCell:cell atIndexPath:indexPath];
            break;
            
        default:
            break;
    }
    return cell;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark MultiSelectionDelegate delegate methods

//---------------------------------------------------------------

- (void) selectedItems:(NSArray *)items {
    self.multiSelection = items;
    self.selectedJobs = nil;
    for (GreatJob *job in items) {
        if (job) {
            NSMutableArray *tempArray = [NSMutableArray new];
            tempArray = [NSMutableArray arrayWithArray:self.selectedJobs];
            AHTag *tag = [self createTag:job];
            [tempArray addObject:tag];
            self.selectedJobs = [NSArray arrayWithArray:tempArray];
            tempArray = nil;
        }
    }
    [self.tableView reloadData];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark AHTagCellDelegate methods

//---------------------------------------------------------------

- (void)tapOnTag:(AHTag *)tag {
    // Remove tag from datasource
    @try {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.tagId.intValue = %d", tag.tagId.intValue];
        NSArray *tags = [self.selectedJobs filteredArrayUsingPredicate:predicate];
        if (tags.count > 0) {
            AHTag *tag = [tags objectAtIndex:0];
            
            // Remove tag if it's found
            NSMutableArray *tempArray = [NSMutableArray new];
            tempArray = [NSMutableArray arrayWithArray:self.selectedJobs];
            [tempArray removeObject:tag];
            self.selectedJobs = [NSArray arrayWithArray:tempArray];
            tempArray = nil;
            
            // Remove multiselection
            [self.tableView reloadData];
        }
        
        // Remove multiselection
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"SELF.greatJobId.intValue = %d", tag.tagId.intValue];
        NSArray *selected = [self.multiSelection filteredArrayUsingPredicate:predicate2];
        if (selected.count > 0) {
            GreatJob *job = [selected objectAtIndex:0];
            
            // Remove job if it's found
            NSMutableArray *tempArray = [NSMutableArray new];
            tempArray = [NSMutableArray arrayWithArray:self.multiSelection];
            [tempArray removeObject:job];
            self.multiSelection = [NSArray arrayWithArray:tempArray];
            tempArray = nil;
        }
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription)
    }
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITextView delegate methods

//---------------------------------------------------------------

- (void) textViewDidChange:(UITextView *)textView {
    self.dataManager.event.note = [Helper removeWhiteSpaceAndNewLineCharacter:textView.text];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    
    // Get filters from Parent Controller (TabBarCoontroller)
    ObservationTabController *tabBar = (ObservationTabController*)self.parentViewController;
    self.filter = tabBar.filter;
    self.dataManager = tabBar.dataManager;

    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 50;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    // Titles
    self.jobTitleLabel.text = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_GREAT_JOB_ON"];
    self.areaLabel.text = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_AREA_OF_IMPROV"];
    self.publishLabel.text = [App_Delegate.keyMapping getKeyValue:@"KEY_CC_DISPLAY_DASHBOARD"];
    self.mailLabel.text = [App_Delegate.keyMapping getKeyValue:@"KEY_CC_SEND_EMAIL"];
    if (self.dataManager.surveyType == OBSERVATION) {
        self.scoreTitleLabel.text = [App_Delegate.keyMapping getKeyValue:@"KEY_OB_AGENT_SCORE"];
    } else {
        self.scoreTitleLabel.text = [App_Delegate.keyMapping getKeyValue:@"KEY_CC_TEAM_MEMBER_SCORE"];
    }
    
    [self.saveButton setTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_OB_SAVE_TITLE"] forState:UIControlStateNormal];
    
    [self.publishSwitch setOn:!self.dataManager.event.donotDisplayToAgent.boolValue animated:YES];
    [self.mailSwitch setOn:!self.dataManager.event.donotMailToAgent.boolValue animated:YES];
    
    self.areaTextView.delegate = self;
    self.areaTextView.layer.cornerRadius = 4;
    self.areaTextView.layer.borderColor = TABLE_SEPERATOR_COLOR.CGColor;
    self.areaTextView.layer.borderWidth = 1;
    
    UINib *nib = [UINib nibWithNibName:@"AHTagTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"cell"];
    
    // Get available job list
    [self createAvailableTags];

    // Show area of imporvment
    self.areaTextView.text = self.dataManager.event.note;
    
    // Call service to get Job list
    [self callWebServiceToGetGreatJobOn];
}

//---------------------------------------------------------------

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Get filters from Parent Controller (TabBarCoontroller)
    ObservationTabController *tabBar = (ObservationTabController*)self.parentViewController;
    self.filter = tabBar.filter;
    self.dataManager = tabBar.dataManager;
    
    // Set agent score
    self.scoreValueLabel.text = [NSString stringWithFormat:@"%@%%", [Helper getDecimalWithRemovingExtraZero:self.dataManager.event.scoreVal]];
    
    // Disable views for view event
    if (self.dataManager.eventType == OB_COMPLETED_EVENT_VIEW) {
        self.areaTextView.editable = NO;
        self.saveButton.hidden = YES;
        self.arrowImage.hidden = YES;
    }
}

//---------------------------------------------------------------
// In a Storyboard-based application, you will often want to do a little preparation before navigation
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showMultiSelectionView"]) {
        // Show list of great jobs on
        UINavigationController *navController =  (UINavigationController *)[segue destinationViewController];
        MultiSelectionController *controller = (MultiSelectionController *)[navController.viewControllers objectAtIndex:0];
        controller.filter = self.filter;
        controller.delegate = self;
        controller.itemId = GreatJobFilter;
        controller.jobList = self.jobList;
        controller.multiSelectionArray = self.multiSelection;
    }
}

//---------------------------------------------------------------

- (BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"showMultiSelectionView"]) {
        // User can only select 3 types
        if (self.multiSelection.count >= 3) {
            // Show alert for information
            [Helper showAlertWithTitle:[App_Delegate.keyMapping getKeyValue:@"KEY_OB_SELECT_TYPE_INFO"] message:[App_Delegate.keyMapping getKeyValue:@"KEY_OB_SELECT_TYPE_MSG"] forController:self];
            return NO;
        } else {
            return YES;
        }
    }
    return YES;
}

//---------------------------------------------------------------

@end
