//
//  VideoCell.h
//  IN-Gauge
//
//  Created by Mehul Patel on 30/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"

@interface VideoCell : UITableViewCell

@property (nonatomic, strong) UIStackView           *stackView;
@property (nonatomic, strong) UIView                *cardView;
@property (nonatomic, strong) UILabel               *titleLabel;
@property (nonatomic, strong) UILabel               *durationLabel;
@property (nonatomic, strong) UIImageView           *videoImage;
@property (nonatomic, strong) UIImageView           *noThumbImage;

@end
