//
//  VideoCell.m
//  IN-Gauge
//
//  Created by Mehul Patel on 30/11/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "VideoCell.h"

@implementation VideoCell

//---------------------------------------------------------------

#pragma mark -
#pragma mark Init methods

//---------------------------------------------------------------

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (!(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) return nil;
    self.selectionStyle = UITableViewCellSelectionStyleNone;

    _cardView = [[UIView alloc] init];
    self.cardView.translatesAutoresizingMaskIntoConstraints = NO;
    self.cardView.backgroundColor = [UIColor clearColor];
    self.cardView.clipsToBounds = YES;
    [self.contentView addSubview:self.cardView];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cardView]|" options: 0 metrics:nil views:@{@"cardView" : self.cardView}]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cardView]|" options: 0 metrics:nil views:@{@"cardView" : self.cardView}]];
    
    // Video image - Add it to Card View
    _videoImage = [[UIImageView alloc] init];
    self.videoImage.translatesAutoresizingMaskIntoConstraints = NO;
    self.videoImage.contentMode = UIViewContentModeScaleToFill;
    self.videoImage.backgroundColor = BACK_COLOR;
    [self.cardView addSubview:self.videoImage];

    // Drop down shadow
    self.videoImage.layer.masksToBounds = NO;
    self.videoImage.layer.shadowColor = BACK_COLOR.CGColor;
    self.videoImage.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    self.videoImage.layer.shadowRadius = 0.5f;
    self.videoImage.layer.shadowOpacity = 2.0f;
    
    // No thumb image
    _noThumbImage = [[UIImageView alloc] init];
    self.noThumbImage.translatesAutoresizingMaskIntoConstraints = NO;
    self.noThumbImage.contentMode = UIViewContentModeScaleToFill;
    self.noThumbImage.clipsToBounds = YES;
    self.noThumbImage.hidden = YES;
    self.noThumbImage.tintColor = [UIColor grayColor];
    self.noThumbImage.image = [UIImage imageNamed:@"no-thumb"];
    [self.cardView addSubview:self.noThumbImage];

    // Video title - Add it to StackView
    _titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont fontWithName:AVENIR_NEXT_FONT size:12];
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.titleLabel.numberOfLines = 3;
    
    // Video duration - Add it to StackView
    _durationLabel = [[UILabel alloc] init];
    self.durationLabel.font = [UIFont fontWithName:AVENIR_NEXT_FONT size:11];
    self.durationLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.durationLabel.textAlignment = NSTextAlignmentLeft;
    self.durationLabel.textColor = [UIColor grayColor];
    self.durationLabel.numberOfLines = 0;
    
    UIView *extraSpace = [[UIView alloc] init];
    extraSpace.translatesAutoresizingMaskIntoConstraints = NO;
    extraSpace.backgroundColor = [UIColor clearColor];
    
    // Stack View - Add it to CardView
    _stackView = [[UIStackView alloc] init];
    self.stackView.translatesAutoresizingMaskIntoConstraints = NO;
    self.stackView.axis = UILayoutConstraintAxisVertical;
    self.stackView.distribution = UIStackViewDistributionFill;
    self.stackView.alignment = UIStackViewAlignmentFill;
    self.stackView.spacing = 5;
    
    [self.stackView addArrangedSubview:self.titleLabel];
    [self.stackView addArrangedSubview:self.durationLabel];
    [self.stackView addArrangedSubview:extraSpace];
    [self.cardView addSubview:self.stackView];
    
//    self.titleLabel.backgroundColor = [UIColor yellowColor];
//    self.durationLabel.backgroundColor = [UIColor redColor];
        
    NSDictionary *views = @{@"stackView" : self.stackView, @"videoImage" : self.videoImage, @"noThumbImage" : self.noThumbImage};
    // Layout for Stack View
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[videoImage(150)]-10-[stackView]-5-|" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-15-[stackView]|" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-15-[videoImage(79)]" options: 0 metrics:nil views:views]];

    // Content Hugging priority - This will remove extra padding (height) of label
    [self.titleLabel setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisVertical];
    // Content Hugging priority - This will remove extra padding (height) of label
    [self.durationLabel setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisVertical];

    // No thumbnail image
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[noThumbImage(30)]" options: 0 metrics:nil views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[noThumbImage(30)]" options: 0 metrics:nil views:views]];
    [self.cardView addConstraint:[NSLayoutConstraint constraintWithItem:self.noThumbImage
                                                              attribute:NSLayoutAttributeCenterX
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.videoImage
                                                              attribute:NSLayoutAttributeCenterX
                                                             multiplier:1
                                                               constant:0]];
    [self.cardView addConstraint:[NSLayoutConstraint constraintWithItem:self.noThumbImage
                                                              attribute:NSLayoutAttributeCenterY
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.videoImage
                                                              attribute:NSLayoutAttributeCenterY
                                                             multiplier:1
                                                               constant:0]];
    return self;
}

//---------------------------------------------------------------

- (void) layoutSubviews {
    [super layoutSubviews];    
}

//---------------------------------------------------------------

@end
