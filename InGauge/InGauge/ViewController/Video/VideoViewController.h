//
//  VideoViewController.h
//  InGauge
//
//  Created by Mehul Patel on 24/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewController.h"

@interface VideoViewController : BaseTableViewController

@end
