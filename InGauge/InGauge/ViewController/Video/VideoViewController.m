//
//  VideoViewController.m
//  InGauge
//
//  Created by Mehul Patel on 24/05/17.
//  Copyright © 2017 FPG Pulse. All rights reserved.
//

#import "VideoViewController.h"
#import "LGFilterView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "YBHud.h"
#import "VideoCell.h"
#import "HTHorizontalSelectionList.h"
#import "LGRefreshView.h"
#import <JavaScriptCore/JavaScriptCore.h>

#define kTopHeaderViewHeight    200
#define kHeaderHeight           30
#define ITEM_PER_VIEW           10

typedef enum : NSUInteger {
    TABLE_ANIMATION_NONE,
    TABLE_ANIMATION_TOP,
    TABLE_ANIMATION_LEFT,
    TABLE_ANIMATION_RIGHT,
    TABLE_ANIMATION_BOTTOM
} TABLE_ANIMATION;

static NSString *optionTitle = @"Select an Option";

@import WebKit;

@interface VideoViewController () <UIScrollViewDelegate, WKNavigationDelegate, WKScriptMessageHandler, HTHorizontalSelectionListDataSource, HTHorizontalSelectionListDelegate>

@property (nonatomic, weak) IBOutlet UIView                 *headerView;
@property (nonatomic, weak) IBOutlet UIView                 *topView;
@property (nonatomic, weak) IBOutlet UILabel                *videoTitle;

@property (nonatomic, strong) LGFilterView                  *filterView;
@property (nonatomic, strong) YBHud                         *videoLoadingHud;
@property (nonatomic, strong) HTHorizontalSelectionList     *textSelectionList;
@property (nonatomic, strong) LGRefreshView                 *refreshView;
@property (nonatomic, strong) WKWebView                     *wkWebView;
@property (nonatomic, strong) UILabel                       *navigationLabel;
@property (nonatomic, strong) UILabel                       *topLabel;
@property (nonatomic, strong) UIImageView                   *arrowImage;
@property (nonatomic, strong) UIView                        *navBarView;
@property (nonatomic, strong) UIView                        *noRecordsView;

@property (nonatomic, strong) VideoCategoryModel            *categoryModel;
@property (nonatomic, strong) NSMutableDictionary           *contentOffsetDictionary;
@property (nonatomic, strong) NSArray                       *dataArray;
@property (nonatomic, strong) NSArray                       *mostWatchVideos;
@property (nonatomic, strong) NSArray                       *remainingArray;
@property (nonatomic, strong) NSArray                       *segmentTitles;
@property (nonatomic, assign) NSInteger                     pageNo;
@property (nonatomic, assign) NSInteger                     segmentPrevIndex;
@property (nonatomic, assign) TABLE_ANIMATION               tableAnimation;

@end

@implementation VideoViewController

//---------------------------------------------------------------

#pragma mark
#pragma mark Memory management methods

//---------------------------------------------------------------

- (void) dealloc  {
    // Remove delegates, mutable array and observers here!
    self.contentOffsetDictionary = nil;
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark Action methods

//---------------------------------------------------------------

- (IBAction) tenantChangeButtonTapped:(id)sender {
    [self changeTenantAndIndustry];
}

//---------------------------------------------------------------

- (void) selectCategoryButtonTapped:(UIButton *)sender {
    [Helper tapOnControlAnimation:self.navBarView forScale:0.5 forCompletionBlock:^(BOOL finished) {}];
    [self.tableView setContentOffset:CGPointZero animated:YES];
    
    if ([self.filterView isShowing]) {
        self.arrowImage.image = [UIImage imageNamed:DOWN_ARROW];
        [self.filterView dismissAnimated:YES completionHandler:nil];
    } else {
        // Patch if table is scrolled
        self.arrowImage.image = (self.tableView.contentOffset.y != 0) ? [UIImage imageNamed:DOWN_ARROW] :  [UIImage imageNamed:UP_ARROW];
        [self.filterView showInView:self.view animated:YES completionHandler:nil];
    }
}

//---------------------------------------------------------------

- (IBAction) reloadWebView:(id)sender {
    [self.videoLoadingHud showInView:self.wkWebView];
    [self.wkWebView reload];
}

//---------------------------------------------------------------

- (void) rightSwipeHandler:(UIGestureRecognizer *)gestureRecognizer {
    NSInteger index = self.textSelectionList.selectedButtonIndex;
    // Reached to first index
    if (index == 0) {
        return;
    }
    [self.textSelectionList setSelectedButtonIndex:index-1 animated:YES];
    self.tableAnimation = TABLE_ANIMATION_LEFT;
    [self.tableView reloadData];
}

//---------------------------------------------------------------

- (void) leftSwipeHandler:(UIGestureRecognizer *)gestureRecognizer {
    NSInteger index = self.textSelectionList.selectedButtonIndex;
    // Reached to last index
    index = index + 1;
    if (index == self.segmentTitles.count) {
        return;
    }
    [self.textSelectionList setSelectedButtonIndex:index animated:YES];
    self.tableAnimation = TABLE_ANIMATION_RIGHT;
    [self.tableView reloadData];
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark WEBSERVICE methods

//---------------------------------------------------------------

- (void) callWebServiceToGetCategoryList {

    // initialise
    self.pageNo = 0;
    
    NSString *type = [User signedInUser].isSuperAdmin.boolValue ? @"roleAndPermission" : @"sectionWiseMedia";
    NSDictionary *params = @{
                                @"userId" : [User signedInUser].userID,
                                @"roleId" : App_Delegate.appfilter.userRoleId,
                                @"type" : type
                             };
    
    MBProgressHUD *hud = [Helper showLoading:self.navigationController.view];
    [ServiceManager callWebServiceToGetVideoCategoryList:params usingBlock:^(VideoCategoryModel *model, NSString *error) {
        [Helper hideLoading:hud];
        self.categoryModel = model;
        
        // Get video list from category
        [self getVideosFromCategory];
        
        // Check for data
        if (self.categoryModel.allVideos.count == 0) {
            self.noRecordsView.hidden = NO;
            self.headerView.hidden = YES;
            self.arrowImage.hidden = YES;
            self.topLabel.hidden = YES;
            self.textSelectionList.hidden = YES;
        } else {
            
            // Create filterView
            [self createFilterView];
            
            self.noRecordsView.hidden = YES;
            self.headerView.hidden = NO;
            self.arrowImage.hidden = NO;
            self.topLabel.hidden = NO;
            self.textSelectionList.hidden = NO;
        }
        
        self.tableAnimation = TABLE_ANIMATION_BOTTOM;
        [self.tableView reloadData];
        
        // Load WebView
        if (self.dataArray.count > 0) {
            [self loadVideo:self.dataArray[0]];
        }
    }];
}

//---------------------------------------------------------------

- (void) callWebServiceToGetMostWatchVideos {
    NSString *type = [User signedInUser].isSuperAdmin.boolValue ? @"mostWatchedSuperAdmin" : @"mostWatchedList";
    NSDictionary *params = @{
                                @"userId" : [User signedInUser].userID,
                                @"roleId" : App_Delegate.appfilter.userRoleId,
                                @"type" : type
                             };
    [ServiceManager callWebServiceToGetVideoCategoryList:params usingBlock:^(VideoCategoryModel *model, NSString *error) {
        // By default show all videos
        self.mostWatchVideos = model.allVideos;
        
        // Create filterView
        [self createFilterView];
    }];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark Custom methods

//---------------------------------------------------------------

- (void) loadVideo:(VideoCategory *)video {
    // Dismiss previous hud
    [self.videoLoadingHud dismiss];
    self.videoTitle.text = video.name;
    
    // Show loading indicator
    [self.videoLoadingHud showInView:self.wkWebView];
    
    NSString *baseUrlString = [NSString stringWithFormat:@"%@/vtPlayer", PLAY_VIDEO_URL];
    // Set IndustryId
    NSString *industryId = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_INDUSTRY_ID];
    if (!industryId) {
        industryId = KEY_INDUSTRY_ID_VALUE;
    }
    NSDictionary *params = @{
                                @"email" : [User signedInUser].email,
                                @"userId" : [User signedInUser].userID,
                                @"mediaHasId" : video.mediaId,
                                KEY_INDUSTRY_ID : industryId
                                };
    
    NSMutableDictionary *filterParams = [params mutableCopy];
    User *user = [User signedInUser];
    if (!isEmptyString(user.accessToken)) {
        NSString *authorisationBearer = [NSString stringWithFormat:@"%@", user.accessToken];
        [filterParams setObject:[NSString stringWithFormat:@"%@",authorisationBearer] forKey:@"authorizationId"];
    }
    NSString *chartUrlString = [ServiceUtil addHeaderParametersWithOutBaseURL:baseUrlString withParams:filterParams];
    NSString *encodedUrl = [chartUrlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:encodedUrl]];
    DLog(@"\n\n\n\n *** Video URL :%@", encodedUrl);
    [self.wkWebView loadRequest:theRequest];
}

//---------------------------------------------------------------

- (void) createTopThumbnailView {    
    _wkWebView = [[WKWebView alloc] init];
    [self.wkWebView.scrollView setScrollEnabled:NO];
    [self.wkWebView.scrollView setBounces:NO];
    self.wkWebView.navigationDelegate = self;
    self.wkWebView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.topView addSubview:self.wkWebView];
    self.wkWebView.backgroundColor = [UIColor blackColor];
    self.wkWebView.opaque = NO;
    
    [self.topView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[wkWebView]|" options: 0 metrics:nil views:@{@"wkWebView" : self.wkWebView}]];
    [self.topView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[wkWebView]|" options: 0 metrics:nil views:@{@"wkWebView" : self.wkWebView}]];
    
    // loading indicator
    _videoLoadingHud = [[YBHud alloc] initWithHudType:DGActivityIndicatorAnimationTypeBallClipRotatePulse andText:@""];
    self.videoLoadingHud.tintColor = [UIColor whiteColor];
    self.videoLoadingHud.dimAmount = 0;
}

//---------------------------------------------------------------

- (void) createFilterView {
    self.filterView = nil;
    
    if (self.categoryModel.categoryNames.count > 0) {
        __weak typeof(self) wself = self;
        _filterView = [[LGFilterView alloc] initWithTitles:self.categoryModel.categoryNames actionHandler:^(LGFilterView *filterView, NSString *title, NSUInteger index) {
            if (wself) {
                __strong typeof(wself) self = wself;
                self.topLabel.text = title;
                // Update arrow frame
                self.arrowImage.image = [UIImage imageNamed:DOWN_ARROW];
                [self updateToLabel];
                
                // Get video list from category
                [self getVideosFromCategory];
                
                // Update table
                self.tableAnimation = TABLE_ANIMATION_BOTTOM;
                [self.tableView reloadData];
                // Load WebView
                if (self.dataArray.count > 0) {
                    [self loadVideo:self.dataArray[0]];
                }
            }
        } cancelHandler:nil];
        
        // Apperance
        self.filterView.transitionStyle = LGFilterViewTransitionStyleTop;
        self.filterView.numberOfLines = 0;
        self.filterView.font = [UIFont fontWithName:AVENIR_NEXT_FONT size:14];
        self.filterView.titleColor = [UIColor blackColor];
        self.filterView.titleColorSelected = MATERIAL_PINK_COLOR;
        self.filterView.backgroundColorSelected = [UIColor clearColor];
    }
}

//---------------------------------------------------------------

- (void) setTopNavBarTitle {
    CGFloat width = self.view.frame.size.width - 100;
    _navBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - 15, 44)];
    _navigationLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, width, 20)];
    self.navigationLabel.text = [App_Delegate.keyMapping getKeyValue:@"KEY_VIDEO"];
    self.navigationLabel.textAlignment = NSTextAlignmentCenter;
    self.navigationLabel.font = [UIFont fontWithName:AVENIR_FONT size:14.0f];
    self.navigationLabel.textColor = [UIColor whiteColor];
    [self.navBarView addSubview:self.navigationLabel];
    
    _topLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, width, 15)];
    self.topLabel.textAlignment = NSTextAlignmentCenter;
    self.topLabel.font = [UIFont fontWithName:AVENIR_FONT size:12.0f];
    self.topLabel.textColor = [UIColor whiteColor];
    self.topLabel.text = @"All Videos";
    [self.topLabel sizeToFit];
    [self.navBarView addSubview:self.topLabel];

    width = self.topLabel.intrinsicContentSize.width;
    CGRect frame = self.topLabel.frame;
    frame.size.width = width;
    
    // Calculate center position
    CGFloat originX = self.navigationLabel.center.x - (width/2);
    frame.origin.x = originX - 10;
    self.topLabel.frame = frame;
    [self.topLabel sizeToFit];
    
    // Arrow image
    CGFloat marginX = self.topLabel.frame.origin.x + self.topLabel.frame.size.width + 5;
    CGFloat marginY = self.topLabel.frame.origin.y;
    _arrowImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:DOWN_ARROW]];
    self.arrowImage.frame = CGRectMake(marginX, marginY, 12, 15);
    self.arrowImage.tintColor = [UIColor whiteColor];
    [self.navBarView addSubview:self.arrowImage];
    
    // Add button to open filter
    UIButton *openFilterButton = [UIButton buttonWithType:UIButtonTypeSystem];
    openFilterButton.frame = CGRectMake(0, 0, self.view.frame.size.width - 70, 44);
    [openFilterButton addTarget:self action:@selector(selectCategoryButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.navBarView addSubview:openFilterButton];
    self.navigationItem.titleView = self.navBarView;
}

//---------------------------------------------------------------

- (void) updateToLabel {
    float width = self.topLabel.intrinsicContentSize.width;
    CGRect frame = self.topLabel.frame;
    frame.size.width = width;
    
    // Calculate center position
    CGFloat originX = self.navigationLabel.center.x - (width/2);
    frame.origin.x = originX - 10;
    self.topLabel.frame = frame;
    [self.topLabel sizeToFit];
    
    CGFloat marginX = self.topLabel.frame.origin.x + self.topLabel.frame.size.width + 5;
    frame = self.arrowImage.frame;
    frame.origin.x = marginX;
    self.arrowImage.frame = frame;
}

//---------------------------------------------------------------

- (void) createSegmentView {
    _textSelectionList = [[HTHorizontalSelectionList alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    self.textSelectionList.hidden = YES;
    self.textSelectionList.delegate = self;
    self.textSelectionList.dataSource = self;
    
    self.textSelectionList.selectionIndicatorAnimationMode = HTHorizontalSelectionIndicatorAnimationModeHeavyBounce;
    self.textSelectionList.showsEdgeFadeEffect = YES;
    
    self.textSelectionList.selectionIndicatorColor = [UIColor redColor];
    self.textSelectionList.bottomTrimColor = BACK_COLOR;
    [self.textSelectionList setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.textSelectionList setTitleColor:MATERIAL_PINK_COLOR forState:UIControlStateSelected];
    [self.textSelectionList setTitleFont:[UIFont fontWithName:AVENIR_NEXT_FONT size:12] forState:UIControlStateNormal];
    [self.textSelectionList setTitleFont:[UIFont fontWithName:AVENIR_NEXT_DEMI_BOLD_FONT size:12] forState:UIControlStateSelected];
    self.textSelectionList.selectionIndicatorColor = MATERIAL_PINK_COLOR;
    
    // Drop shadow
//    self.textSelectionList.layer.shadowColor = [UIColor grayColor].CGColor;
//    self.textSelectionList.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
//    self.textSelectionList.layer.shadowRadius = 0.5;
//    self.textSelectionList.layer.shadowOpacity = 2.0f;
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.textSelectionList.bounds];
    self.textSelectionList.layer.masksToBounds = NO;
    self.textSelectionList.layer.shadowColor = [UIColor grayColor].CGColor;
    self.textSelectionList.layer.shadowOffset = CGSizeMake(0.0f, 4.0);
    self.textSelectionList.layer.shadowOpacity = 0.5f;
    self.textSelectionList.layer.shadowPath = shadowPath.CGPath;

}

//---------------------------------------------------------------

- (void) createPullToRefresh {
    __weak typeof(self) wself = self;
    _refreshView = [LGRefreshView refreshViewWithScrollView:self.tableView refreshHandler:^(LGRefreshView *refreshView) {
        if (wself) {
            __strong typeof(wself) self = wself;
            //TODO: Call API
            [self callWebServiceToGetCategoryList];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void) {
                [self.refreshView endRefreshing];
            });
        }
    }];
    _refreshView.tintColor = APP_COLOR;
}

//---------------------------------------------------------------

- (void) loadMoreVideos {
    
    self.pageNo = self.pageNo + 1;
    
    // Add more from array
    NSArray *itemsForView = [NSArray new];
    NSInteger limit = self.dataArray.count + ITEM_PER_VIEW;
    
    NSInteger remainingCount = ITEM_PER_VIEW;
    if (limit >= self.remainingArray.count) {
        remainingCount = self.remainingArray.count - self.dataArray.count;
    }
    
    itemsForView = [self.remainingArray subarrayWithRange:NSMakeRange(self.dataArray.count, remainingCount)];
    
    // Early exit!
    if (itemsForView.count == 0) {
        return;
    }
    
    //***NOTE: Create index array before adding items to dataArray, other wise you will not get old count
    NSMutableArray *tempIndexArray = [NSMutableArray new];
    NSInteger index = self.dataArray.count;
    while (remainingCount > 0) {
        [tempIndexArray addObject:[NSIndexPath indexPathForRow:index inSection:0]];
        index++;
        remainingCount--;
    }
    NSArray *indexArray = [NSArray arrayWithArray:tempIndexArray];
    tempIndexArray = nil;

    // add more array
    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.dataArray];
    [tempArray addObjectsFromArray:itemsForView];
    self.dataArray = [NSArray arrayWithArray:tempArray];
    tempArray = nil;
    
    // Insert row at bottom - Table animation
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationRight];
    [self.tableView endUpdates];
}

//---------------------------------------------------------------

- (void) getVideosFromCategory {
    if ([self.topLabel.text isEqualToString:@"All Videos"] || [self.topLabel.text isEqualToString:optionTitle]) {
        self.dataArray = self.categoryModel.allVideos;
    } else {
        // Change video list
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.categoryName = %@", self.topLabel.text];
        NSArray *filterArray = [self.categoryModel.categoryItems filteredArrayUsingPredicate:predicate];
        self.dataArray = nil;
        if (filterArray.count > 0) {
            VideoCategoryList *list = filterArray[0];
            self.dataArray = list.categoryList;
        }
    }
}

//---------------------------------------------------------------

- (void) addSwipeGestures {
    UISwipeGestureRecognizer *rightGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeHandler:)];
    [rightGestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:rightGestureRecognizer];

    UISwipeGestureRecognizer *leftGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeHandler:)];
    [leftGestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [self.view addGestureRecognizer:leftGestureRecognizer];
}

//---------------------------------------------------------------

- (void) showVideoLoadinIndicator:(NSInteger)tag {
    YBHud *hud = [[YBHud alloc] initWithHudType:DGActivityIndicatorAnimationTypeBallClipRotatePulse andText:@""];    
    hud.tintColor = [UIColor whiteColor];
    hud.dimAmount = 0;
    hud.blockView.tag = tag;
    [hud showInView:self.wkWebView];
}

//---------------------------------------------------------------

#pragma mark
#pragma mark UITableView datasource methods

//---------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 95;
}

//---------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

//---------------------------------------------------------------

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (self.textSelectionList.selectedButtonIndex) {
        case 0: return self.dataArray.count; break;
        case 1: return self.mostWatchVideos.count; break;
        default: return 0; break;
    }
}

//---------------------------------------------------------------

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    @try {
        // By default show from left
        CGAffineTransform transform = CGAffineTransformMakeTranslation(0, 0.f);
        switch (self.tableAnimation) {
            case TABLE_ANIMATION_NONE   : transform = CGAffineTransformMakeTranslation(0, 0); break;
            case TABLE_ANIMATION_TOP    : transform = CGAffineTransformMakeTranslation(0, 0); break;
            case TABLE_ANIMATION_BOTTOM : transform = CGAffineTransformMakeTranslation(0, self.view.frame.size.height); break;
            case TABLE_ANIMATION_LEFT   : transform = CGAffineTransformMakeTranslation(-self.view.frame.size.width, 0); break;
            case TABLE_ANIMATION_RIGHT  : transform = CGAffineTransformMakeTranslation(self.view.frame.size.width, 0); break;
            default: break;
        }
        
        cell.transform = transform;
        cell.layer.shadowColor = [[UIColor blackColor]CGColor];
        cell.layer.shadowOffset = CGSizeMake(10, 10);
//        cell.alpha = 0;
        
        //2. Define the final state (After the animation) and commit the animation
        [UIView beginAnimations:@"rotation" context:NULL];
        [UIView setAnimationDuration:0.3];
        cell.transform = CGAffineTransformMakeTranslation(0, 0);
        cell.alpha = 1;
        cell.layer.shadowOffset = CGSizeMake(0, 0);
        [UIView commitAnimations];
    } @catch (NSException *exception) {
        DLog(@"Exception :%@", exception.debugDescription);
    }
}

//---------------------------------------------------------------

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"VideoCell%d%d", (int)indexPath.section, (int)indexPath.row];
    
    VideoCell *cell = (VideoCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[VideoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    VideoCategory *video = nil;
    switch (self.textSelectionList.selectedButtonIndex) {
        case 0: { video = [self.dataArray objectAtIndex:indexPath.row]; break; }
        case 1: { video = [self.mostWatchVideos objectAtIndex:indexPath.row]; break; }
        default: break;
    }
    
    cell.noThumbImage.hidden = NO;
    // Download video thumbnail image
    [cell.videoImage sd_setImageWithURL:[NSURL URLWithString:video.thumbnailURL] placeholderImage:nil options:SDWebImageContinueInBackground completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        // If image not found show custom placeholder image
        cell.noThumbImage.hidden = image ? YES : NO;                
    }];
    cell.titleLabel.text = video.name;
    cell.durationLabel.text = video.duration;
    return cell;
}

//---------------------------------------------------------------

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return self.textSelectionList;
}

//---------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // select animation
    VideoCell *cell = (VideoCell *)[tableView cellForRowAtIndexPath:indexPath];
    [Helper tapOnControlAnimation:cell forScale:0.6 forCompletionBlock:^(BOOL finished) {}];

    VideoCategory *video = nil;
    switch (self.textSelectionList.selectedButtonIndex) {
        case 0: { video = [self.dataArray objectAtIndex:indexPath.row]; break; }
        case 1: { video = [self.mostWatchVideos objectAtIndex:indexPath.row]; break; }
        default: break;
    }
    [self.tableView setContentOffset:CGPointZero animated:YES];
    
    [self.wkWebView stopLoading];
    [self loadVideo:video];
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark UIScrollViewDelegate methods

//---------------------------------------------------------------

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    // Remove animation
    self.tableAnimation = TABLE_ANIMATION_NONE;

    // Hide filterView if its open!
    if ([self.filterView isShowing]) {
        [self.filterView dismissAnimated:YES completionHandler:nil];   
    }
    
//    // Auto load more records
//    if (([scrollView contentOffset].y + scrollView.frame.size.height) >= [scrollView contentSize].height) {
//        [self loadMoreVideos];
////        DLog(@"Load more records....");
//    }
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark HTHorizontalSelectionListDataSource methods

//---------------------------------------------------------------

- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.segmentTitles.count;
}

//---------------------------------------------------------------

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
    return self.segmentTitles[index];
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark HTHorizontalSelectionListDelegate methods

//---------------------------------------------------------------

- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
    // update the view for the corresponding index
    if (self.segmentPrevIndex < index) {
        self.tableAnimation = TABLE_ANIMATION_RIGHT;
    } else {
        self.tableAnimation = TABLE_ANIMATION_LEFT;
    }
    self.segmentPrevIndex = index;
    [self.tableView reloadData];
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark WebView delegate methods

//---------------------------------------------------------------

- (UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return nil;
}

//---------------------------------------------------------------

#pragma mark
#pragma mark WKNavigationDelegate methods

//---------------------------------------------------------------

- (void) webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    //    NSString *javascript = @"var meta = document.createElement('meta');meta.setAttribute('name', 'viewport');meta.setAttribute('content', 'width=device-width, initial-scale=0.0, maximum-scale=0.0, user-scalable=no');document.getElementsByTagName('head')[0].appendChild(meta);";
    //    [webView evaluateJavaScript:javascript completionHandler:nil];
    DLog(@"\n\n\n\n\n\n\n ***** Commit navigation");
}

//---------------------------------------------------------------

- (void) webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
//    
//    JSContext *ctx = [webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
//    ctx[@"console"][@"log"] = ^(JSValue * msg) {
//        NSLog(@"JavaScript %@ log message: %@", [JSContext currentContext], msg);
//    };

//    [webView evaluateJavaScript:@"document.getElementsByTagName('html')[0].offsetHeight;" completionHandler:^(id response, NSError * _Nullable error) {
//    }];
    DLog(@"\n\n\n\n\n\n\n ***** Finish navigation");
    [self.videoLoadingHud dismiss];
}

//---------------------------------------------------------------

- (void) webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    DLog(@"\n\n\n\n\n\n\n ***** Failed navigation");
    [self.videoLoadingHud dismiss];
}

//---------------------------------------------------------------

- (void)webViewWebContentProcessDidTerminate:(WKWebView *)webView {
    DLog(@"\n\n\n\n\n\n\n ***** Terminate navigation");
    [self.videoLoadingHud dismiss];
}

//---------------------------------------------------------------

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    DLog(@"\n\n\n\n\n\n\n ***** Fail provisional navigation");
    [self.videoLoadingHud dismiss];
}

//---------------------------------------------------------------

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    DLog(@"message :%@", message);
}

//---------------------------------------------------------------

#pragma mark -
#pragma mark View lifeCycle methods

//---------------------------------------------------------------

- (void) viewDidLoad {
    [super viewDidLoad];
    
    self.segmentPrevIndex = 0;
    self.headerView.hidden = YES;
    self.tableAnimation = TABLE_ANIMATION_BOTTOM;
    
    _noRecordsView = [Helper createNoAccessView:self.tableView withMessage:@"There are no videos available for this tenant."];

    self.segmentTitles = @[@"ALL VIDEOS", @"MOST WATCHED VIDEOS"];
    [Helper addBottomLine:self.headerView withColor:BACK_COLOR];
    self.headerView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.7];
    
    // Call service to get categories
    [self callWebServiceToGetCategoryList];

    // Call service to get most watched Videos
    [self callWebServiceToGetMostWatchVideos];
    
    // Create Navigation labels
    [self setTopNavBarTitle];
    
    // Create Top Video view
    [self createTopThumbnailView];
    
    // Create Segement View
    [self createSegmentView];
    
    // Create Pull to refresh
    [self createPullToRefresh];
    
    // Add swipe gestures
    [self addSwipeGestures];
    
    // TableView settings
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.estimatedRowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 140;

    UIView *dummyFooter = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 30)];
    dummyFooter.backgroundColor = [UIColor whiteColor];
    self.tableView.tableFooterView = dummyFooter;
    self.contentOffsetDictionary = [NSMutableDictionary dictionary];
}

//---------------------------------------------------------------

@end
